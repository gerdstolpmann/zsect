Zset:

Encoded sets store sets of integers in memory and on disk. The integers
have a range 1 .. 2^48-1. The integers are logically stored as pairs
(start,len) meaning the set { i | start <= i < start+len }. The pairs
are sorted by start.

The encoded set is stored in an array of 62 bit integers (actually laid
out as 64 bit ints but we use only up to 62 bits).

The 6 most significant bits store the type of the word. The 56 lower
significant bits store data that depends on the type.

Type=0:
    data[0-47] store an integer x meaning the set { x }

Type=1:
    data[0-47] store an integer x meaning the set { i | FIRST <= i <= x }.
    (FIRST is the implicitly known first i.)

Type=2:
    data[0-47] store an integer x meaning the set { i | x <= i <= LAST }
    (LAST is the implicitly known last i.)

    Note that an array with one word of type 2 and x=1 denotes the set
    of all integers when LAST=SPACE.

Type=3:
    May be used as a guard.

Type=4:
    With x=data[0-27] and y=data[28-55]: the set is
    { i | PREV+x <= i < PREV+x+y }

    PREV is the last integer encoded by the previous word (which must exist)

Type=5:
   With x1=data[0-13], y1=data[14-27], x2=data[28-41], y2=data[42-55]:
   the set is
     { i | PREV+x1 <= i < PREV+x1+y1  or 
           PREV+x1+y1+x2 <= i < PREV+x1+y1+x2+y2 }

Type=6:
   With x1=data[0-8], y1=data[9-17], x2=data[18-26], y2=data[27-35],
        x3=data[36-44], y3=data[45-53]:
   the set is
     { i | PREV+x1 <= i < PREV+x1+y1  or 
           PREV+x1+y1+x2 <= i < PREV+x1+y1+x2+y2 or
           PREV+x1+y1+x2+y2+x3 <= i < PREV+x1+y1+x2+y2+x3+y3 }
Type=7:
   With x1=data[0-6], y1=data[7-13], x2=data[14-20], y2=data[21-27],
        x3=data[28-34], y3=data[35-41], x4=data[42-48], y5=data[49-55]:
   the set is
     { i | PREV+x1 <= i < PREV+x1+y1  or 
           PREV+x1+y1+x2 <= i < PREV+x1+y1+x2+y2 or
           PREV+x1+y1+x2+y2+x3 <= i < PREV+x1+y1+x2+y2+x3+y3 or
           PREV+x1+y1+x2+y2+x3+y3+x4 <= i < PREV+x1+y1+x2+y2+x3+y3+x4+y4 }

Type=8:
  With x1=data[0-27] and x2=data[28-55]: the set is
  { i | i = PREV+x1  or  i = PREV+x1+x2 }

Type=9:
  With x1=data[0-17], x2=data[18-35], x3=data[36-53]: the set is
  { i | i = PREV+x1  or  i = PREV+x1+x2  or  i = PREV+x1+x2+x3 }

Type=10:
   With x1=data[0-13], x2=data[14-27], x3=data[28-41], x4=data[42-55]:
   the set is
  { i | i = PREV+x1  or  i = PREV+x1+x2  or  i = PREV+x1+x2+x3  or
        i = PREV+x1+x2+x3+x4 }

Type=11:
  With x1=data[0-9], x2=data[10-19], x3=data[20-29], x4=data[30-39],
       x5=data[40-49]:
  { i | i = PREV+x1  or  i = PREV+x1+x2  or  i = PREV+x1+x2+x3  or
        i = PREV+x1+x2+x3+x4   or  i = PREV+x1+x2+x3+x4+x5 }

Type=12:
   With x1=data[0-8], x2=data[9-17], x3=data[18-26], x4=data[27-35],
        x5=data[36-44], x6=data[45-53]:
  { i | i = PREV+x1  or  i = PREV+x1+x2  or  i = PREV+x1+x2+x3  or
        i = PREV+x1+x2+x3+x4   or  i = PREV+x1+x2+x3+x4+x5  or
        i = PREV+x1+x2+x3+x4+x5+x6 }

Type=13:
   With x1=data[0-7], x2=data[8-15], x3=data[16-23], x4=data[24-31],
        x5=data[32-39], x6=data[40-47], x7=data[48-55]:
  { i | i = PREV+x1  or  i = PREV+x1+x2  or  i = PREV+x1+x2+x3  or
        i = PREV+x1+x2+x3+x4   or  i = PREV+x1+x2+x3+x4+x5  or
        i = PREV+x1+x2+x3+x4+x5+x6   or  i = PREV+x1+x2+x3+x4+x5+x6+x7 }

Structural:

Every 256-th word (including the very first one) must have one of the
types 0, 1 or 2.

A word of type 1 must be the very first one.

A word of type 2 must be the very last one.

The empty set is represented by the empty array.

The word of type 3 MAY be used in internal representations as a guard,
i.e. it is appended to the end of the array.

