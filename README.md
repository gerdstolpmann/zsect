# ZSect

Defines a column-oriented database format (Zcontainer), and provides
an execution engine for queries on such databases. The format is
specially designed for analytical purposes. There is now also a
machine learner (Zmola).

This project is in a very early stage.

License: LGPL with linking exception.

