#use "topfind";;
#require "zcontainer";;
#load "learner.cma";;

open Zcontainer;;
open L_types;;

module Target =
  struct
    type t = string
    let compare = String.compare
    let equal x y = (x = y)
    let hash x = Hashtbl.hash x
  end


module In =
  struct
    module Target = Target
    let ztab =
      Ztable.from_file Zrepr.stdrepr "playtennis.inv"
    let features =
      [ "humidity"; "outlook"; "temperature"; "wind" ]
    let target_col =
      Ztable.get_zcol ztab Ztypes.Zstring "playtennis"
    let target_get id =
      Zcol.value_of_id target_col id
    let debug = true
  end;;


module Train = L_id3.Train(In)


(* ~/pub/invtables/src/zcontainer/tools/zdigest -out playtennis.inv playtennis.csv *)
