open Zcontainer
open L_types
open Printf


let log2 = log 2.0

module Train(In:TRAIN_INPUT) =
  struct
    module Ht = Hashtbl.Make(In.Target)

    let count_values trainset =
      let ht = Ht.create 41 in
      Zset.iter
        (fun start len ->
           for id = start to start+len-1 do
             let x = In.target_get id in
             try
               let n = Ht.find ht x in
               incr n
             with Not_found ->
               Ht.replace ht x (ref 1)
           done
        )
        trainset;
      ht

    let has_single_partition trainset =
      (* Check whether all training samples have the same target value *)
      let some_id = Zset.first trainset in
      let some_val = In.target_get some_id in
      let target_cmp = In.Target.compare in
      L_util.zset_for_all
        (fun id -> target_cmp (In.target_get id) some_val = 0)
        trainset

    let best_target_value trainset =
      let ht = count_values trainset in
      match
        Ht.fold
          (fun x n acc ->
             match acc with
               | None ->
                   Some(x,!n)
               | Some(best_x, best_n) ->
                   if !n > best_n then
                     Some(x, !n)
                   else
                     acc
          )
          ht
          None
      with
        | None -> assert false
        | Some(x,n) -> (x,n)


    let entropy trainset =
      let ht = count_values trainset in
      let total = Zset.count trainset in
      let sum =
        Ht.fold
          (fun x n acc ->
            let p = float !n /. float total in
            acc  -.  p *. log p
          )
          ht
          0.0 in
      sum /. log2
      

    let gain_cat ft trainset e_trainset =
      (* Get the column: *)
      let Ztypes.ZcolBox(_,ty,repr,order) = Ztable.get_zcolbox In.ztab ft in
      let col = Ztable.get_zcol In.ztab ty ft in
      (* Partition trainset by ft: *)
      let icol = Zcol.invert col trainset in
      let iter = Zcol.iter_values icol in
      iter # go_beginning();
      let e = ref e_trainset in
      let n_trainset = Zset.count trainset in
      while not iter#beyond do
        let part_set = iter#current_zset Zset.default_pool in
        let n = Zset.count part_set in
        e := !e -. (float n /. float n_trainset) *. entropy part_set;
        iter # next()
      done;
      !e

    let rec bisection_extract_candidates l =
      match l with
        | (_,x1,t1) :: ((_,x2,t2) :: _ as l') ->
            if x1 <> x2 && not (In.Target.equal t1 t2) then
              0.5 *. (x1 +. x2) :: bisection_extract_candidates l'
            else
              bisection_extract_candidates l'
        | [_] ->
            []
        | [] ->
            []

    let bisection_candidates ft trainset =
      (* Get the column: *)
      let Ztypes.ZcolBox(_,ty,repr,order) = Ztable.get_zcolbox In.ztab ft in
      match Ztypes.same_ztype ty Ztypes.Zfloat with
        | Ztypes.Equal ->
            let col = Ztable.get_zcol In.ztab ty ft in
            let get_value = Zcol.value_of_id col in
            let values = ref [] in
            Zset.iter
              (fun start len ->
                 for id = start to start+len-1 do
                   values := (id, get_value id, In.target_get id) :: !values
                 done
              )
              trainset;
            values := 
              List.sort
                (fun (id1,x1,t1) (id2,x2,t2) ->
                   let p = compare x1 x2 in
                   if p <> 0 then p else id1-id2
                )
                !values;
            bisection_extract_candidates !values
        | Ztypes.Not_equal ->
            assert false

    let gain_bisection ft split_value trainset e_trainset =
      (* Get the column: *)
      let Ztypes.ZcolBox(_,ty,repr,order) = Ztable.get_zcolbox In.ztab ft in
      match Ztypes.same_ztype ty Ztypes.Zfloat with
        | Ztypes.Equal ->
            let col = Ztable.get_zcol In.ztab Ztypes.Zfloat ft in
            let get_value = Zcol.value_of_id col in
            let set1, set2 =
              L_util.zset_partition
                (fun id ->
                   get_value id < split_value
                )
                (Ztable.space In.ztab)
                trainset in
            let n1 = Zset.count set1 in
            let n2 = Zset.count set2 in
            let n_trainset = Zset.count trainset in
            (e_trainset -. 
               (float n1 /. float n_trainset) *. entropy set1 -.
               (float n2 /. float n_trainset) *. entropy set2,
             set1,
             set2
            )
        | Ztypes.Not_equal ->
            assert false


    let maxarg l =
      match l with
        | (arg1,value1) :: l' ->
            fst
              (List.fold_left
                 (fun (maxarg,maxval) (arg,value) ->
                  if value > maxval then
                    (arg,value)
                  else
                    (maxarg,maxval)
                 )
                 (arg1,value1)
                 l'
              )
        | [] ->
            invalid_arg "maxarg"

    type split_type =
      | Categorical of string            (* feature name *)
      | Real of string * float * Zset.zset * Zset.zset
           (* feature name, split value, left set, right set *)


    let rec extend path cat_features real_features trainset =
      (* cat_features: categorical features
         real_features: real-valued
       *)
      let path_str =
        String.concat
          "/" (List.rev (List.map (fun (n,v) -> sprintf "%s %s" n v) path)) in
      if In.debug then
        printf "count(%s) = %d\n%!" path_str (Zset.count trainset);
      (* Stop criterion: (so far build complete trees) *)
      if cat_features = [] && real_features = [] then (
        let (target,_) = best_target_value trainset in
        if In.debug then
          printf "  leaf: %s\n%!" path_str;
        Tree(Leaf target)
      )
      else if has_single_partition trainset then (
        let some_id = Zset.first trainset in
        let target = In.target_get some_id in
        if In.debug then
          printf "  leaf: %s\n%!" path_str;
        Tree(Leaf target)
      )
      else (
        let (best_target,_) = best_target_value trainset in
        (* For every remaining feature compute the entropy gain: *)
        let e_trainset = entropy trainset in
        if In.debug then
          printf "entropy(%s) = %.3f\n%!" path_str e_trainset;
        let gain_by_cat_ft =
          List.map
            (fun colname ->
               let g = gain_cat colname trainset e_trainset in
               if In.debug then
                 printf "gain_cat(%s,%s) = %.3f\n%!" path_str colname g;
               (Categorical colname, g)
            )
            cat_features in
        (* Same for real-valued features: *)
        let gain_by_real_ft =
          List.flatten
            (List.map
               (fun colname ->
                  let split_cands = bisection_candidates colname trainset in
                  List.map
                    (fun split_cand ->
                       let g, set1, set2 =
                         gain_bisection colname split_cand trainset e_trainset in
                       if In.debug then
                         printf "gain_bi(%s,%s,%.3f) = %.3f\n%!"
                                path_str colname split_cand g;
                       (Real(colname,split_cand,set1,set2), g)
                    )
                    split_cands
               )
               real_features
            ) in
        (* Get the feature with the maximum gain: *)
        let gain_all = gain_by_cat_ft @ gain_by_real_ft in
        if gain_all = [] then (
          if In.debug then
            printf "  leaf: %s\n%!" path_str;
          Tree(Leaf best_target)
        )
        else
          let split = maxarg gain_all in
          match split with
            | Categorical ft ->
                let remaining =
                  List.filter (fun name -> name <> ft) cat_features in
                (* Get the column: *)
                let Ztypes.ZcolBox(_,ty,repr,order) =
                  Ztable.get_zcolbox In.ztab ft in
                let col = Ztable.get_zcol In.ztab ty ft in
                let coldescr = Ztypes.ZcolDescr(ft,ty,repr,order) in
                (* Partition trainset by ft: *)
                let icol = Zcol.invert col trainset in
                let iter = Zcol.iter_values icol in
                iter # go_beginning();
                let children = ref [] in
                while not iter#beyond do
                  let _, part_val = iter#current_value in
                  let part_set = iter#current_zset Zset.default_pool in
                  let path' = 
                    (ft, "= " ^ Ztypes.debug_string_of_value ty part_val)
                    :: path in
                  let sub = extend path' remaining real_features part_set in
                  let decision_test = ColtestBox(coldescr, Ztypes.EQ, part_val) in
                  (* omit leaves returning [best_target]: *)
                  let omit =
                    match sub with
                      | Tree(Leaf t) ->
                          In.Target.equal t best_target
                      | _ ->
                          false in
                  if not omit then
                    children := (decision_test, sub) :: !children;
                  iter # next()
                done;
                children := (TrueBox, Tree(Leaf best_target)) :: !children;
                Tree(Inner (List.rev !children))
            | Real(ft,sv,set1,set2) ->
                let Ztypes.ZcolBox(_,ty,repr,order) =
                  Ztable.get_zcolbox In.ztab ft in
                let coldescr = Ztypes.ZcolDescr(ft,ty,repr,order) in
                ( match Ztypes.same_ztype ty Ztypes.Zfloat with
                    | Ztypes.Equal ->
                        let path1' = 
                          (ft, sprintf " < %.3f" sv) :: path in
                        let path2' = 
                          (ft, sprintf " > %.3f" sv) :: path in
                        let sub1 = extend path1' cat_features real_features set1 in
                        let sub2 = extend path2' cat_features real_features set2 in
                        Tree(Inner [ ColtestBox(coldescr, Ztypes.LT, sv), sub1;
                                     TrueBox, sub2
                                   ]
                            )
                    | Ztypes.Not_equal ->
                        assert false
                )
      )

    let partition_features ftlist =
      List.partition
        (fun ft ->
           let Ztypes.ZcolBox(_,ty,repr,order) =
             Ztable.get_zcolbox In.ztab ft in
           match ty with
             | Ztypes.Zfloat -> false
             | _ -> true
        )
        ftlist


    let run_for zset =
      let cat_features, real_features = partition_features In.features in
      extend [] cat_features real_features zset


    let run ?select () =
      let all = Zset.all Zset.default_pool (Ztable.space In.ztab) in
      assert(not (Zset.is_empty all));
      let zset =
        match select with
          | None -> all
          | Some f ->
              L_util.zset_filter f (Ztable.space In.ztab) all in
      if Zset.is_empty zset then
        failwith "L_id3.run: nothing selected";
      run_for zset
end
