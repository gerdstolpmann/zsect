open L_types
open Zcontainer
open Printf


let eval_cmp cmp =
  let open Ztypes in
  match cmp with
    | EQ -> (fun n -> n=0)
    | NE -> (fun n -> n<>0)
    | LT -> (fun n -> n<0)
    | LE -> (fun n -> n<=0)
    | GT -> (fun n -> n>0)
    | GE -> (fun n -> n>=0)


let rec eval (ftvalues : (string * valuebox) list)
             (tree : 'v tree) : 'v option =
  match tree with
    | Tree(Inner decisions) ->
        eval_run_decisions ftvalues decisions
    | Tree(Leaf v) ->
        Some v

and eval_run_decisions ftvalues decisions =
  match decisions with
    | (ColtestBox(Ztypes.ZcolDescr(name,ty,_,ord),cmp,cmp_v), sub) :: dec' ->
        let ok =
          try
            let ftvbox = List.assoc name ftvalues in   (* or Not_found *)
            let ValueBox(ftty,ftval) = ftvbox in
              match Ztypes.same_ztype ftty ty with
                | Ztypes.Equal ->
                    eval_cmp
                      cmp
                      Ztypes.(ord.ord_cmp ftval cmp_v)
                | Ztypes.Not_equal ->
                    failwith "Type mismatch"
          with
            | Not_found ->
                false in
        if ok then
          eval ftvalues sub
        else
          eval_run_decisions ftvalues dec'
    | (TrueBox, sub) :: _ ->
        eval ftvalues sub
    | [] ->
        None


let rec quick_eval (tree : ('u,'v) quicktree)
                   (id : 'u) : 'v option =
  match tree with
    | QTree(Inner decisions) ->
        quick_eval_run_decisions decisions id
    | QTree(Leaf v) ->
        Some v

and quick_eval_run_decisions decisions id =
  match decisions with
    | (_, (test, sub)) :: dec' ->
        if test id then
          quick_eval sub id
        else
          quick_eval_run_decisions dec' id
    | [] ->
        None


let rec quick_tree_for_ztable (tree : 'v tree) ztab =
  match tree with
    | Tree(Inner decisions) ->
        QTree(Inner (List.map (quick_tree_map_decision ztab) decisions))
    | Tree(Leaf v) ->
        QTree(Leaf v)

and quick_tree_map_decision ztab dec =
  match dec with
    | (ColtestBox(Ztypes.ZcolDescr(name,ty,_,ord),cmp,cmp_v) as dbox, sub) ->
        let zcol = Ztable.get_zcol ztab ty name in
        let sub' = quick_tree_for_ztable sub ztab in
        let get_value = Zcol.value_of_id zcol  in
        let e_cmp = eval_cmp cmp in
        let test id =
          let v = get_value id in
          e_cmp Ztypes.(ord.ord_cmp_asc v cmp_v) in
        (dbox, (test, sub'))
    | (TrueBox, sub) ->
        let sub' = quick_tree_for_ztable sub ztab in
        (TrueBox, ((fun _ -> true), sub'))


module Accuracy(In:TRAIN_INPUT) =
  struct

    let accuracy_for (tree : 'v tree) zset =
      (* Check the accuracy of the tree by counting how well the sample
         from zset is predicted
       *)
      let qtree = quick_tree_for_ztable tree In.ztab in
      let n = ref 0 in
      Zset.iter
        (fun start len ->
           for id = start to start+len-1 do
             try
               let v_predicted =
                 match quick_eval qtree id with
                   | Some v -> v
                   | None -> (* "don't know" *) raise Not_found in
               let v_actually = In.target_get id in
               if In.Target.equal v_predicted v_actually then
                 incr n
             with
               | Not_found -> ()
           done
        )
        zset;

      float !n /. float (Zset.count zset)

    let accuracy ?select tree =
      let all = Zset.all Zset.default_pool (Ztable.space In.ztab) in
      assert(not (Zset.is_empty all));
      let zset =
        match select with
          | None -> all
          | Some f ->
              L_util.zset_filter f (Ztable.space In.ztab) all in
      if Zset.is_empty zset then
        failwith "L_id3.run: nothing selected";
      accuracy_for tree zset

  end
