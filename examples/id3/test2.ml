#use "topfind";;
#require "zcontainer";;
#load "learner.cma";;

open Zcontainer;;
open L_types;;

module Target =
  struct
    type t = bool
    let compare = Pervasives.compare
    let equal x y = (x = y)
    let hash x = if x then 1 else 0
  end


module In =
  struct
    module Target = Target
    let ztab =
      Ztable.from_file Zrepr.stdrepr "ACCIDENT2007/selectedfields.zcont"
    let features =
      [ "COUNTY"; "DAY"; "DAY_WEEK"; "HOUR"; "MONTH"; "NO_LANES"; "PERSONS";
        "SP_LIMIT"
      ]
    let target_col =
      Ztable.get_zcol ztab Ztypes.Zint64 "DRUNK_DR"
    let target_get id =
      Zcol.value_of_id target_col id > 0L
    let debug = true
  end;;


module Train = L_id3.Train(In)
module A = L_evaluate.Accuracy(In);;

(*
let t = Train.run ~select:(fun id -> id mod 3 < 2) ();;
A.accuracy ~select:(fun id -> id mod 3 = 2) t;;


 *)


(* ~/pub/invtables/src/zcontainer/tools/zdigest selectedfields.csv -out selectedfields.zcont -repr DRUNK_DR=DF64/int64 -order DRUNK_DR=int64_asc -repr PERSONS=DF64/float -order PERSONS=float_asc *)

