open Zcontainer

module type TARGET =
  sig
    type t
    val compare : t -> t -> int
    val equal : t -> t -> bool
    val hash : t -> int
  end

module type TRAIN_INPUT = 
  sig
    module Target : TARGET
    val ztab : Ztable.ztable
    val features : string list
    val target_get : int -> Target.t
    val debug : bool
  end


type _ decision =
  | Coltest : 'a Ztypes.zcoldescr * Ztypes.comparison * 'a -> 'a decision
      (** Coltest(col,cmp,value): how the value of col compares to value
          with comparison cmp
       *)
  | True : unit decision
      (** Always true *)

type decisionbox =
  | ColtestBox : 'a Ztypes.zcoldescr * Ztypes.comparison * 'a -> decisionbox
  | TrueBox : decisionbox

type ('a, 'v) node =
  | Inner of (decisionbox * 'a) list
  | Leaf of 'v
  
type 'v tree =
  | Tree of ('v tree,'v) node

type ('u,'v) quicktree =
  | QTree of (('u -> bool) * ('u,'v) quicktree, 'v) node

type _ value =
  | Value : 'a Ztypes.ztype * 'a -> 'a value

type valuebox =
  | ValueBox : 'a Ztypes.ztype * 'a -> valuebox


(*
type child_id = Child of int
type node_id = Node of int

module NodeT = struct ... end

module NodeMap = Map.Make(NodeT)

type 'v revtree =
  { mutable rev_node : (child_id,'v) node NodeMap.t;
    mutable rev_parent : node_id NodeMap.t;
  }
 *)
