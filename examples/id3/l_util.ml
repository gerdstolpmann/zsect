open L_types
open Zcontainer
open Printf


let zset_for_all f zset =
  try
    Zset.iter
      (fun start len ->
         for id = start to start+len-1 do
           if not (f id) then raise Exit
         done
      )
      zset;
    true
  with Exit -> false


let zset_filter f space zset =
  let b = Zset.build_begin Zset.default_pool 100 space in
  Zset.iter
    (fun start len ->
       for id = start to start+len-1 do
         if f id then
           Zset.build_add b id
       done
    )
    zset;
  Zset.build_end b


let zset_partition f space zset =
  let b1 = Zset.build_begin Zset.default_pool 100 space in
  let b2 = Zset.build_begin Zset.default_pool 100 space in
  Zset.iter
    (fun start len ->
       for id = start to start+len-1 do
         if f id then
           Zset.build_add b1 id
         else
           Zset.build_add b2 id
       done
    )
    zset;
  (Zset.build_end b1, Zset.build_end b2)


let print_tree ch string_of_leaf t =
  let string_of_path path =
    String.concat
      "."
      (List.map
         (sprintf "%02d")
         (List.rev path)
      ) in

  let string_of_cmp =
    function
    | Ztypes.EQ -> "="
    | Ztypes.NE -> "<>"
    | Ztypes.LT -> "<"
    | Ztypes.LE -> "<="
    | Ztypes.GT -> ">"
    | Ztypes.GE -> ">=" in

  let string_of_dc =
    function
    | ColtestBox(zd, cmp, value) ->
        let Ztypes.ZcolDescr(colname,zt,_,_) = zd in
        sprintf
          "%s %s %s"
          colname
          (string_of_cmp cmp)
          (Ztypes.debug_string_of_value zt value)
    | TrueBox ->
        "true" in

  let rec print path t =
    match t with
      | Tree (Leaf x) ->
          fprintf
            ch
            "%s        Leaf: %s\n"
            (string_of_path path)
            (string_of_leaf x)
      | Tree (Inner decisions) ->
          List.iteri
            (fun i (dbox,sub) ->
               let subpath = (i+1)::path in
               fprintf
                 ch
                 "%s        Test: %s\n"
                 (string_of_path subpath)
                 (string_of_dc dbox);
               print subpath sub
            )
            decisions
  in
  print [] t


let write_tree string_of_leaf file t =
  let ch = open_out file in
  print_tree ch string_of_leaf t;
  close_out ch

  
