open Zmola
open Zmola.Types
open Zcontainer
open Printf

let cols =
  [| ( "x",
       Zdigester.ParserBox(Ztypes.Zint, int_of_string),
       "IM/int",
       "int_asc"
     );
     ( "y",
       Zdigester.ParserBox(Ztypes.Zfloat, float_of_string),
       "DF64/float",
       "float_asc"
     );
    |]


let pi = 3.141592

let error() =
  (* some normal distributed error using Box-Muller *)
  let var = 0.01 in
  let u = Random.float 1.0 in
  let v = Random.float 1.0 in
  sqrt(var) *. sqrt(-2.0 *. log u) *. cos(2.0 *. pi *. v)

let create_sine_data f ~enable_noise ~rounds ~size =
  for r = 1 to rounds do
    for i = 0 to size-1 do
      let k = i - size/2 in
      let x = float k *. pi /. 100.0 in
      let y = sin x +. (if enable_noise then error() else 0.0) in
      f k x y
    done;
  done

let create_parabola_data f ~enable_noise ~rounds ~size =
  for r = 1 to rounds do
    for i = 0 to size-1 do
      let x = 2.0 *. float i /. float size in
      let y = x *. x +. (if enable_noise then error() else 0.0) in
      f i x y
    done;
  done

let create_data =
  function
  | `Sine -> create_sine_data
  | `Parabola -> create_parabola_data
    

let create_table ~enable_noise ~rounds ~size ~curve =
  let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
  create_data
    curve
    (fun k x y ->
       let dg_vec = [| string_of_int k; string_of_float y |] in
       Zdigester.build_add dg dg_vec;
    )
    ~enable_noise ~rounds ~size;
  Zdigester.build_end dg


module Learn(L:LOSS) =
  struct
    module DS = L.DS

    (* base learner: *)
    module BL_step = Blearner_sqe_bisection.Step(DS)
    module BL_config = struct let max_depth = 3 end
    module BL = Blearner.Tree_learner(BL_config)(BL_step)

    (* ensemble learner: *)
    module EL = Booster.Tree_Gradient_Booster(L)(BL)

    (* folding helper *)
    module F = Folder.Data_folds(DS)

    (* Boosting helper *)
    module Loop = Booster_loop.Loop(EL)

    let init_draw size =
      Graphics.open_graph "";
      Graphics.resize_window size 200

    let draw f x_col set size =
      Graphics.clear_graph();
      Zset.iter
        (fun start len ->
           for k = start to start+len-1 do
             let x = Zcol.value_of_id x_col k in
             let y = f k x in
             Graphics.plot (x+size/2) (truncate (100.0 *. y +. 100.0));
           done
        )
        set

    let learn ds ~enable_graph ~size ~rate ~folds ~stages =
      let ztab = DS.ztable ds in
      let pool = Zset.create_pool ~enctype:Zset.Array_enc 1000 100 in
      if enable_graph then init_draw size;
      let fld = F.create pool folds ds in
      let Ztypes.Space n = Ztable.space ztab in
      let x_col = Ztable.get_zcol ztab Ztypes.Zint "x" in
      let total_model = ref [] in
      for d = 0 to folds-1 do
        let trainset = F.get_fold fld d in
        let valset = F.get_fold_negation fld d in
        let loopstate = Loop.create ds pool trainset valset rate in
        printf "fold %d iter %d loss_train=%e loss_val=%e\n%!"
               d
               (Loop.stage_count loopstate)
               (Loop.loss_trainset loopstate /. float n)
               (Loop.loss_valset loopstate /. float n);
        while Loop.stage_count loopstate < stages do
          Loop.next loopstate;
          printf "fold %d iter %d loss_train=%e loss_val=%e\n%!"
                 d
                 (Loop.stage_count loopstate)
                 (Loop.loss_trainset loopstate /. float n)
                 (Loop.loss_valset loopstate /. float n);
          (* Loop.check loopstate; *)
          if enable_graph then (
            let s = Loop.stage loopstate in
            draw
              (fun k x ->
                 EL.approximation s k
              )
              x_col
              trainset
              size
          )
        done;
        (* Add the models in m to the total list *)
        let m = Loop.ensemble_model loopstate in
        let m2 = List.map (fun t -> Tree.multiply (1.0 /. float folds) t) m in
        total_model := m2 @ !total_model
      done;
      printf "number of trees: %d\n" (List.length !total_model);
      if enable_graph then (
        let all = Zset.all pool (Ztable.space ztab) in
        let eval = Tree.eval_list ztab !total_model in
        draw
          (fun k x ->
             eval k
          )
          x_col
          all
          size;
        printf "[press enter]%!";
        ignore(input_line stdin)
      )
end


module type MAKE_LOSS =
  functor(DS:DATASET) -> LOSS with module DS = DS

let () =
  (* let monotonic = ref None in *)

  let module DS =
    struct
      type dataset = Ztable.ztable
      type col_hint = { col_ordinal : bool;
                        (* col_monotonic : [`Increasing|`Decreasing] option; *)
                      }
      let ztable ztab = ztab
      let x_columns _ = [ "x", {col_ordinal=true;
                                (* col_monotonic= !monotonic; *)
                               };
                        ]
      let y_column _ = "y"
    end in

  let curve = ref `Sine in
  let enable_noise = ref false in
  let enable_graph = ref false in
  let rounds = ref 2 in
  let size = ref 1000 in
  let rate = ref 1.0 in
  let folds = ref 2 in
  let stages = ref 50 in
  let loss_mod = ref (module Loss.Squared_error : MAKE_LOSS) in
  let write_csv = ref None in
  Arg.parse
    [ "-sine", Arg.Unit (fun () -> curve := `Sine),
      "  approximate a sine curve";

      "-parabola", Arg.Unit (fun () -> curve := `Parabola),
      "  approximate a parabola curve";

      "-mono-parabola", Arg.Unit (fun () -> curve := `Parabola;
                                 (* monotonic := Some `Increasing *)
                                 ),
      "  approximate a parabola curve";
      (* hint: use smaller -rate, e.g. 0.1 *)

      "-enable-noise", Arg.Set enable_noise,
      "  add some normal-distributed noise to the data";
      
      "-sampling-factor", Arg.Set_int rounds,
      "<n>  how many data points to generate for every x value (default: 2)";

      "-size", Arg.Set_int size,
      "<n>  the number of x values (the width of the graph) (default: 1000)";

      "-enable-graph", Arg.Set enable_graph,
      "  draw a graph in a window";

      "-rate", Arg.Set_float rate,
      "<fraction>  set the learning rate (default: 1.0)";

      "-folds", Arg.Set_int folds,
      "<n>  set the number of folds (for cross-validation and bagging) (default: 2)";
      
      "-stages", Arg.Set_int stages,
      "<n>   set the number of boosting stages (default: 50)";

      "-loss", Arg.String
                 (function
                   | "squared" ->
                       loss_mod := (module Loss.Squared_error)
                   | "absdev" ->
                       loss_mod := (module Loss.Absolute_deviation)
                   | "huber" ->
                       let module HC = struct let alpha = 0.2 end in
                       loss_mod := (module Loss.Huber(HC))
                   | s ->
                       raise(Arg.Bad("unknown loss module: " ^ s))
                 ),
      "(squared|absdev|huber)  set the loss module";

      "-write-csv", Arg.String (fun s -> write_csv := Some s),
      "<file>   write the data to this CSV file (istead of learning)";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    "usage: sine [options]";

  let module L = (val !loss_mod : MAKE_LOSS) in
  let module Lrn = Learn(L(DS)) in
  match !write_csv with
    | None ->
        let ds =
          create_table
            ~enable_noise:!enable_noise
            ~rounds:!rounds
            ~size:!size
            ~curve:!curve in
        Lrn.learn
          ds
          ~enable_graph:!enable_graph
          ~size:!size
          ~rate:!rate
          ~folds:!folds
          ~stages:!stages
    | Some file ->
        let f = open_out file in
        fprintf f "x,y\n";
        create_data
          !curve
          (fun k x y ->
             fprintf f "%d,%f\n" k y
          )
          ~enable_noise:!enable_noise
          ~rounds:!rounds
          ~size:!size;
        close_out f

          
