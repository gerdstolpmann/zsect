open Zmola
open Zmola.Types
open Zcontainer
open Printf


let cols =
  [| ( "x",
       Zdigester.ParserBox(Ztypes.Zint, int_of_string),
       "IM/int",
       "int_asc"
     );
     ( "y",
       Zdigester.ParserBox(Ztypes.Zint, int_of_string),
       "IM/int",
       "int_asc"
     );
     ( "z",
       Zdigester.ParserBox(Ztypes.Zint, int_of_string),
       "DF64/int",
       "int_asc"
     );
    |]


let pi = 3.141592

let error() =
  (* some normal distributed error using Box-Muller *)
  let var = 1.0 in
  let u = Random.float 1.0 in
  let v = Random.float 1.0 in
  sqrt(var) *. sqrt(-2.0 *. log u) *. cos(2.0 *. pi *. v)

let round x =
  int_of_float(floor(x +. 0.5))


let create_square ~enable_noise ~rounds ~size f =
  let half = size/2 in
  let limit = half*3/4 in
  for r = 1 to rounds do
    for x = -half to half do
      for y = -half to half do
        let c =
          if enable_noise then
            round (float limit +. error())
          else
            limit in
        let z_bool =
          ((x = (-c) || x = c) && y >= (-limit) && y <= limit) ||
            ((y = (-c) || y = c) && x >= (-limit) && x <= limit) in
        let z = if z_bool then 1 else 0 in
        f x y z
      done
    done
  done

let create_cross ~enable_noise ~rounds ~size f =
  let half = size/2 in
  let limit = half*3/4 in
  for r = 1 to rounds do
    for x = -half to half do
      for y = -half to half do
        let c =
          if enable_noise then
            round (float x +. error())
          else
            x in
        let z_bool =
          x >= (-limit) && x <= limit && y >= (-limit) && y <= limit && 
            (y = c || y = -c) in
        let z = if z_bool then 1 else 0 in
        f x y z
      done
    done
  done

let create_table create_data =
  let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
  create_data
    (fun x y z ->
       let dg_vec = [| string_of_int x; string_of_int y; string_of_int z |] in
       Zdigester.build_add dg dg_vec;
    );
  Zdigester.build_end dg


module Learn(L:LOSS with type p = float) =
  struct
    module DS = L.DS

    (* base learner: *)
    module BL_step = Blearner_sqe_bisection.Step(DS)
    module BL_config = struct let max_depth = 3 end
    module BL = Blearner.Tree_learner(BL_config)(BL_step)

    (* ensemble learner: *)
    module EL = Booster.Tree_Gradient_Booster(L)(BL)

    (* folding helper *)
    module F = Folder.Data_folds(DS)

    (* Boosting helper *)
    module Loop = Booster_loop.Loop(EL)

    let target_size = 500

    let window_size size =
      let w1 = max size target_size in
      if size < w1 then
        target_size / size * size
      else
        size

    let init_draw size =
      Graphics.open_graph "";
      let winsize = window_size size in
      Graphics.resize_window winsize winsize

    let draw ?(overlay=false) size x_col y_col f drawset =
      let winsize = window_size size in
      let offset = winsize/2 in
      let factor = winsize/size in
      let half = size/2 in
      let sum = Array.make_matrix (size+1) (size+1) 0.0 in
      let count = Array.make_matrix (size+1) (size+1) 0 in
      Graphics.clear_graph();
      Zset.iter
        (fun start len ->
           for k = start to start+len-1 do
             let x = Zcol.value_of_id x_col k in
             let y = Zcol.value_of_id y_col k in
             let dup = count.(x+half).(y+half) > 0 in
             if not dup || overlay then (
               let z = f k in
               let s = sum.(x+half).(y+half) +. z in
               let n = count.(x+half).(y+half) + 1 in
               sum.(x+half).(y+half) <- s;
               count.(x+half).(y+half) <- n;
               let eff_z = s /. float n in
               let z256 = 255 - int_of_float(eff_z *. 255.0) in
               let z256 = min 255 (max 0 z256) in
               Graphics.set_color (Graphics.rgb z256 z256 z256);
               Graphics.fill_rect (factor*x+offset) (factor*y+offset) factor factor;
             )
           done;
           Graphics.set_color Graphics.foreground;
        )
        drawset

    let show ds ~size =
      init_draw size;
      let pool = Zset.create_pool ~enctype:Zset.Array_enc 1000 100 in
      let ztab = DS.ztable ds in
      let all = Zset.all pool (Ztable.space ztab) in
      let x_col = Ztable.get_zcol ztab Ztypes.Zint "x" in
      let y_col = Ztable.get_zcol ztab Ztypes.Zint "y" in
      let z_col = Ztable.get_zcol ztab Ztypes.Zint "z" in
      draw size x_col y_col (fun k -> float(Zcol.value_of_id z_col k)) all
      

    let learn ds ~enable_graph ~size ~rate ~folds ~stages =
      let ztab = DS.ztable ds in
      let pool = Zset.create_pool ~enctype:Zset.Array_enc 1000 100 in
      if enable_graph then init_draw size;
      let fld = F.create pool folds ds in
      let Ztypes.Space n = Ztable.space ztab in
      let x_col = Ztable.get_zcol ztab Ztypes.Zint "x" in
      let y_col = Ztable.get_zcol ztab Ztypes.Zint "y" in
      let total_model = ref [] in
      for d = 0 to folds-1 do
        let trainset = F.get_fold fld d in
        let valset = F.get_fold_negation fld d in
        let loopstate = Loop.create ds pool trainset valset rate in
        printf "fold %d iter %d loss_train=%e loss_val=%e rate=%.2f\n%!"
               d
               (Loop.stage_count loopstate)
               (Loop.loss_trainset loopstate /. float n)
               (Loop.loss_valset loopstate /. float n)
               (Loop.learning_rate loopstate);
        while Loop.stage_count loopstate < stages do
          Loop.next loopstate;
          printf "fold %d iter %d loss_train=%e loss_val=%e rate=%.2f\n%!"
                 d
                 (Loop.stage_count loopstate)
                 (Loop.loss_trainset loopstate /. float n)
                 (Loop.loss_valset loopstate /. float n)
                 (Loop.learning_rate loopstate);
          (* Loop.check loopstate; *)
          if enable_graph then (
            let s = Loop.stage loopstate in
            draw ~overlay:true size x_col y_col 
                 (fun id -> L.output (EL.approximation s id))
                 trainset
          );
        done;
        (* Add the models in m to the total list *)
        let m = Loop.ensemble_model loopstate in
        let m2 = List.map (fun t -> Tree.multiply (1.0 /. float folds) t) m in
        total_model := m2 @ !total_model
      done;
      printf "number of trees: %d\n" (List.length !total_model);
      if enable_graph then (
        let all = Zset.all pool (Ztable.space ztab) in
        let eval id = L.output (Tree.eval_list ztab !total_model id) in
        draw size x_col y_col eval all;
        printf "[press enter]%!";
        ignore(input_line stdin)
      )
  end



module type MAKE_LOSS =
  functor(DS:DATASET) -> LOSS with module DS = DS and type p = float

module BC = struct let alpha = 0.0 end

let () =
  let module DS =
    struct
      type dataset = Ztable.ztable
      type col_hint = { col_ordinal : bool;
                        (* col_monotonic : [`Increasing|`Decreasing] option; *)
                      }
      let ztable ztab = ztab
      let x_columns _ = [ "x", {col_ordinal=true; (*col_monotonic=None*)};
                          "y", {col_ordinal=true; (*col_monotonic=None*)};
                        ]
      let y_column _ = "z"
    end in

  let enable_noise = ref false in
  let enable_graph = ref false in
  let rounds = ref 2 in
  let size = ref 50 in
  let rate = ref 1.0 in
  let folds = ref 2 in
  let stages = ref 50 in
  let loss_mod = ref (module Loss.Bilog(BC) : MAKE_LOSS) in
  let write_csv = ref None in
  let shape = ref create_square in
  let show_target = ref false in
  let alpha = ref 0.0 in
  Arg.parse
    [ "-enable-noise", Arg.Set enable_noise,
      "  add some normal-distributed noise to the data";
      
      "-sampling-factor", Arg.Set_int rounds,
      "<n>  how many data points to generate for every x value (default: 2)";

      "-size", Arg.Set_int size,
      "<n>  the number of x values (the width of the graph) (default: 1000)";

      "-enable-graph", Arg.Set enable_graph,
      "  draw a graph in a window";

      "-show-target", Arg.Set show_target,
      "  show the target shape in the graph before learning";

      "-rate", Arg.Set_float rate,
      "<fraction>  set the learning rate (default: 1.0)";

      "-folds", Arg.Set_int folds,
      "<n>  set the number of folds (for cross-validation and bagging) (default: 2)";
      
      "-stages", Arg.Set_int stages,
      "<n>   set the number of boosting stages (default: 50)";

      "-alpha", Arg.Set_float alpha,
      "<num>   set the alpha constant for influence trimming. This option must precede -loss";

      "-loss", Arg.String
                 (function
                   | "bilog" ->
                       let module BC = struct let alpha = !alpha end in
                       loss_mod := (module Loss.Bilog(BC))
                   | s ->
                       raise(Arg.Bad("unknown loss module: " ^ s))
                 ),
      "(bilog)  set the loss module";

      "-shape", Arg.String
                  (function
                    | "square" ->
                        shape := create_square
                    | "cross" ->
                        shape := create_cross
                    | s ->
                       raise(Arg.Bad("unknown shape: " ^ s))
                 ),
      "(square|cross)  set the shape";

      "-write-csv", Arg.String (fun s -> write_csv := Some s),
      "<file>   write the data to this CSV file (istead of learning)";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    "usage: shape [options]";

  let module L = (val !loss_mod : MAKE_LOSS) in
  let module Lrn = Learn(L(DS)) in
  match !write_csv with
    | None ->
        let ds =
          create_table
            (!shape
               ~enable_noise:!enable_noise
               ~rounds:!rounds
               ~size:!size
            ) in
        if !enable_graph && !show_target then (
          Lrn.show
            ds
            ~size:!size;
          printf "[press enter]%!";
          ignore(input_line stdin);
        );

        Lrn.learn
          ds
          ~enable_graph:!enable_graph
          ~size:!size
          ~rate:!rate
          ~folds:!folds
          ~stages:!stages
    | Some file ->
        let f = open_out file in
        fprintf f "x,y,z\n";
        !shape
          (fun x y z ->
             fprintf f "%d,%d,%d\n" x y z
          )
          ~enable_noise:!enable_noise
          ~rounds:!rounds
          ~size:!size;
        close_out f

          
