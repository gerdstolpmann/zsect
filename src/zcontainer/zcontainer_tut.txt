Tutorial for zcontainer

{2 Introduction}

{!Zcontainer} is a file format for column-oriented datasets. In this
section let me try to explain in which respect these datasets are
different from usual ones.

{b Storage layout}

The first apparent difference is the storage layout. Traditionally,
databases store data row by row, i.e. all fields of a row are packed
into some contiguous area on disk. We do it here differently, and pack
the data column by column.

In the simplest version, this means a column is just an array of the
values the rows have for the column. There are more advanced storage
formats, though, see the next paragraph.

{b Inverted columns}

Columns cannot only be stored like arrays, but there is an alternate
format where we first extract which values occur in a column, and then
mainly store a mapping from the values to the IDs of the rows having
the values.

Inverted columns allow that the data can be highly compressed. There
is even an extension for sparse data: In a sparse column, one of the
values isn't stored at all but simply considered as the default value.

{b Radical preference for read access}

The file format is made so that read accesses a maximally cheap, and
write accesses are complicated.

At the moment, write accesses are only possible via a procedure called
"digestion". We read some new rows in (chunk by chunk), convert these
rows in-memory into the zcontainer format, and merge the new data with
the existing table, resulting into a new table.

In the future, there will be more ways of writing data, in particular
via joins (create a new table by joining some existing tables).

{b Storage in simple files}

A table is just a directory on disk. A column is represented by one or
more files in this directory.

{b Memory-mapped files}

The files are memory-mapped into the process accessing them. If there is
enough RAM, the database is as fast as a pure in-memory database. However,
you can also access databases that are much larger than RAM is available.

{b 64 bit only}

Any support for 32 bit platforms is dropped, and thus we avoid any
painful code paths and emulations that would be needed for these platforms.


{2 The tools}

At the moment, there are four command-line tools:

 - [zdigest] digests CSV data and produces a zcontainer table
 - [zdump] reads a zcontainer table and outputs the data row by row
 - [ztool] has a couple of management functions, allowing you to:
   {ul
     {li inspect what is in a zcontainer}
     {li copy columns from one table to another}
     {li remove columns from a table}
     {li merge tables}
   }
 - [zdiscretize] can create histogram columns, permitting basic
   discretization of data

For example,

{[ zdigest -out my.zcont file.csv ]}

reads [file.csv] where the first row is taken as a header row giving the
columns names. The data is stored in a zcontainer table in the new directory
my.zcont. By default, [zdigest] assumes the fields are all strings, and
stores these in inverted columns. You can change that with additional
options:

{[ zdigest -out my.zcont file.csv -repr col=DF64/int ]}

This requests that the column "col" is to be stored in the "DF64/int"
format. "int" is the data type. "DF64" is a representation prefix requesting
to store the ints in an array of 64 bit words. See {!Zcontainer.Zrepr} for more
documentation which formats are available.

You can also invoke the digester from the {!Zcontainer} library with the
{!Zcontainer.Zdigester} module.


{2 Some concepts}

As a database is limited to a single table anyway, we use the terms
"table" and "database" interchangably. Of course, you can have several
tables, but that just means that they are stored in independent
directories.

A table has columns, identified by names. You can add new column and
remove columns, but you cannot change a column. A column is read-only
after it has been created.

The rows of a table are numbered from 1 to N. We call this range also
the {i space} of the table. The number of a row is also called its (row)
ID.

The format of the columns is specified by the triple (type,
representation, ordering). The type is a tiny subset of the
OCaml type system (see {!Zcontainer.Ztypes.ztype}, effectively we have
strings, integers, floats, options and pairs). The representation says
how the column is stored on disk. There are two major ways:

 - {i Direct columns} store the values like an array where the indices
   are the row IDs
 - {i Inverted columns} only store which values occur, and for every
   value the set of row IDs

For both representation methods there are a couple of concrete
representations, see {!Zcontainer.Zrepr} for details.

Note that the zcontainer library abstracts over the representation.
You can access an inverted column also like an array, and you can
walk over the values occurring in a direct column as if it were
inverted. Of course, these "wrong" access methods are more expensive
than the methods the representation can support best.

Finally, the ordering says in which order the values of the column are
iterated by default if you want to iterate over the
values. Essentially, this determines in which order the values of an
inverted column are stored on disk. For direct columns, there is no
influence of the ordering on the disk format, but the ordering is kept
for symmetry with the inverted columns.


{2 The {!Zcontainer} library}

The library consists of a number of submodules, in particular

 - {!Zcontainer.Ztable} allows you to open a table, and to add/remove
   columns
 - {!Zcontainer.Zcol} allows you to access a single column
 - {!Zcontainer.Zset} represents sets of row IDs
 - {!Zcontainer.Zfilter} contains functions to filter columns by expressions
 - {!Zcontainer.Zaggreg} provides folding functions
 - {!Zcontainer.Zgrouping} is about grouping data by keys

and a couple of lower-level modules.

{2 Load a table}

If the table exists already on disk you can simply open it:

{[
let ztab = Zcontainer.Ztable.from_file Zcontainer.Zrepr.stdrepr "my.zcont"
]}

Now, the data has been memory-mapped into RAM. The [stdrepr] parameter
selects that the built-in encodings for the data types should be assumed.

There are now a number of access methods:

{[
let space = Zcontainer.Ztable.space ztab
let columns = Zcontainer.Ztable.columns ztab
]}

As already mentioned, the [space] is the range of row IDs. Effectively,
the highest ID is always equal to the number of rows [n]:

{[
let Zcontainer.Ztypes.Space n = space
]}

The [columns] are just the column names. If you know the name and type
of a column, you can get a handle for it with

{[
let zcol = Zcontainer.Ztable.get_zcol ztab Zcontaier.Ztypes.Zint "k"
]}

(here we assume the column "k" is of type int, and [zcol] has type
[int Zcontainer.Zcol.zcol]). If you don't know the type and want to
write generic code, you need to recourse to [get_zcolbox]:

{[
let Zcontainer.Ztypes.ZcolBox(name,ty,repr,order) =
  Zcontainer.Ztable.get_zcolbox ztab "k"
]}

Note that [ty] ia a GADT describing the type. You can match on it:

{[
match ty with
 | Zcontaier.Ztypes.Zint -> ...
 | Zcontaier.Ztypes.Zfloat -> ...
 | ...
]}

(Beginners of OCaml should take into account that GADTs are a
notoriously difficult concept, and try to avoid matching on types!).

Note that there is also [ZcolDescr] which is like [ZcolBox] but
exposes the type parameter.

{b Advanced}

In {!Zcontainer.Ztable} you also find:

 - Functions for creating columns ([build_zinvcol] and [build_zdircol])
 - [merge] merges several columns into an output column (this merge
   works like concatenating the rows)
 - [add_vector] adds a bigarray as a named column to the table. The
   bigarray is not copied and also not stored on disk. This is mainly
   intended for attaching intermediate results to tables.
 - [create_memory_ztable] creates a table that only exists in memory.
   Any columns must be explicitly filled with values (using
   [build_zinvcol], [build_zdircol], or [add_vector]). Memory tables
   can also overlay disk tables. This means that the columns of the
   disk table are visible, but any new column only exists in memory.


{2 Access data by ID}

Let's assume that the column "x" stores pairs [(int * string)]. We first
get a handle [zcol]:

{[
let ty = Zcontainer.Ztypes.(Zpair(Zint,Zstring))
let zcol = Zcontainer.Ztable.get_zcol ztab ty "x"
]}

Now let's get the pair at ID 34:

{[
let (i,s) = Zcontainer.Zcol.value_of_id zcol 34
]}

Note that you get the pair directly - there's no wrapper around it
that you would have to match against.

If you want to loop over IDs, it is advisable to partially evaluate
[value_of_id] because this factors some of the access work out of the
loop:

{[
let get_sum zcol =
  let Zcontainer.Ztypes.Space n = Zcontainer.Zcol.space zcol in
  let sum = ref 0 in
  let get = Zcontainer.Zcol.value_of_id zcol in
  for id = 1 to n do
    let (i,_) = get id in
    sum := !sum + i
  done;
  !sum
]}

(NB. In the {!Zcontainer.Zaggreg} module you find a number of
prefabricated reductions over columns.)

{b Performance:} Remember that this access method is only fast for
direct columns. For inverted columns it is slower by a considerable
but constant factor.


{2 Sets}

The data type {!Zcontainer.Zset.zset} represents sets of positive
integers.  This type is mainly intended to be used for row ID sets.

Like for columns, there are several representations for sets. These
have in common that sets are mainly considered as sorted lists of ranges
[(from,to]):

 - The {i packed} representation is the default one, and the only one that
   can be used for on-disk columns. Here, several ranges are packed into
   64 bit words (if possible). Additionally, some tricks are used to
   compress these ranges, and there is an additional index for quickly
   skipping over ranges in large sets. Decoding packed sets is pretty
   fast, but encoding takes some additional time.
 - The {i array} representation is a non-compressing variant of the
   packed representation. While the decoding speed is not that much
   higher than that of packed sets, the encoding speed is much better.
   Array sets are for in-memory use only.
 - The {i bitset} representation is a bitset with a skip array as
   accelerator. Bitsets take a lot of memory, but are a very fast basis
   for set intersections. If you need to intersect a given set [b] with
   many other sets, it is often a good idea to convert [b] to bitset
   representation first.

Before starting to create a set you first need a {i pool}. Pools are
both factories for sets and helper data structures for the memory
management details:

 - {!Zcontainer.Zset.create_pool} creates a pool in any of the three
   representations.
 - {!Zcontainer.Zset.default_pool} is a ready-to-use pool for packed
   sets. (Note that you shouldn't use this in multi-threaded programs,
   because two threads cannot safely access the same pool concurrently.)
 - {!Zcontainer.Zset.bitset_pool} is a pool for bitsets

You can create then sets manually, e.g.

{[
open Zcontainer.Zset
let pool = default_pool
let builder = build_begin pool 100
build_add builder 12
build_add builder 16
build_add_range builder 20 29
let zset = buid_end builder
]}

The builder data structure is here a helper buffer that accumulates
the integers and/or integer ranges (which must already be sorted).

Like columns sets are read-only after they have been built.

Functions for sets include:

 - [empty]: the empty set
 - [all]: the set [{1...n}] for a given space [n]
 - [iter]: iterate over the ranges in a set
 - [mem]: check whether an ID is member of a set
 - [filter]: subset by filtering on IDs
 - [isect]: intersect two sets
 - [diff]: set difference
 - [union]: set union
 - [union_many]: union of many sets (faster than uniting set by set)


{2 Iterate over data by value}

Before you can iterate over the values occurring in a column, you need
to invert the column. For columns that are already stored in inverted
format this is actually a no-op, but for direct columns the inversion
is quite costly:

{[
let zset = Zcontainer.Zset.all pool (Zcontainer.Zcol.space zcol)
let icol = Zcontainer.Zcol.invert zcol zset
]}

Here, [zset] is a filter for the rows that are covered by the iteration.
In this example we set it to the set of all rows. Small filter sets can
speed up the iteration enormously.

Now:

{[
let itr = Zcontainer.Zcol.iter_values icol
]}

This gets you an iterator [itr] (see {!classtype:Zcontainer.Zinvcol.ziterator}).
The iterator is like a cursor that can move over the values (in the
order specified by the column), and for every cursor position you can
get the value as such, and the set of rows with the value (expressed
as [zset]):

{[
while not itr#beyond do
  let h, v = itr#current_value in
  let zset = itr#current_zset pool in
  ...
  itr#next()
done
]}

Here, [v] is the value. The number [h] is the so-called "hash". This is
not to be confused with the hash values used for hash table or even with
crypographic hashes. It is simply a number extracted from the value, and
the library ensures that it respects the sorting order (i.e. for sorting
the values, it is possible to first sort by [h] and resort only to [v]
for equal hashes).

The iterator can also be walked in backward direction (with the
methods [go_end], followed by [prev] to go backwards).

{b Performance:} Remember that this access method is only fast for
inverted columns. For direct columns it is much slower, sometimes
painfully slow, because the data has first to be sorted by the values.


{2 Aggregations}

Because it depends on the representation which access method is fast
it is often advisable to use one of the prefabricated reductions in
{!Zcontainer.Zaggreg}. Internally these functions are backed by
several implementations where reasonable to always ensure best speed.

For example, the loop we already saw

{[
let get_sum zcol =
  let Zcontainer.Ztypes.Space n = Zcontainer.Zcol.space zcol in
  let sum = ref 0 in
  let get = Zcontainer.Zcol.value_of_id zcol in
  for id = 1 to n do
    let (i,_) = get id in
    sum := !sum + i
  done;
  !sum
]}

can also be expressed with the [fold] function:

{[
let sum =
  let zset = Zcontainer.Zset.all pool (Zcontainer.Zcol.space zcol) in
  Zcontainer.Zaggreg.fold
    (fun id (i,_) acc -> i + acc)
    zcol
    zset
    0
]}

For a direct column [zcol] this function uses a loop similar to that
of [get_sum], For inverted columns, though, it takes advantage of that
and first iterates over the values, then over the rows with a given
value.

This module also includes functions for a number of statistics:
 - minimum
 - maximum
 - sum
 - arithmetic mean
 - (standard) variance
 - n-th smallest value
 - median
 - any quantile


{2 Filtering data}

Filtering rows can be done by intersecting sets with filter sets.
Many of the zcontainer functions already accept a filter set as input
(e.g. {!Zcontainer.Zaggreg.fold} takes the set of rows to iterate
over), so often the intersection doesn't even need to be computed.
The remaining question is how to create the filter sets. Here,
{!Zcontainer.Zfilter} helps.

For example, let's get the set of all rows where the float column
[x] is positive. We first have to create a filter expression:

{[
let x = Zcontainer.(Ztable.get_zcol ztab Ztypes.Zfloat "x")
let expr = Zcontainer.(Zfilter.colcmp_flt x Ztypes.GT 0.0)
]}

Here, [colcmp_flt] constructs the filter expression "x > 0.0".

Now, we only need to evaluate the expression:

{[
let zset = Zcontainer.Zfilter.zset pool ztab expr
]}

This call computes the set in the fastest way that is possible given
the representation of "x". (NB. This is only true so far we don't take
any knowledge about value distributions into account. Often there are
several ways to build filter sets, e.g. you can additively accumulate
positive values or you can start with the set of all rows and subtract
any non-positive values. In order to profit from such alternatives
zcontainers would have to collect statistical data of value distributions.
This is not yet done.)

You can also create expressions by combining them with AND, OR, NOT.

Let me point out one interesting detail. The evaluator for filters
assumes 3-valued Kleene logic like SQL does. This logic is different
from the usual Boolean logic in the presence of unknown values
(expressed as NULLs in the case of SQL). As we also can represent
optional values in zcontainer, there actually is the possibility of
unknowns. For example, if "y" is a column of type [int option], we
can run the filter

{[
let y = Zcontainer.(Ztable.get_zcol ztab (Ztypes.Zoption Ztypes.Zint) "y")
let expr = Zcontainer.(Zfilter.colcmp_some_flt y Ztypes.GT 0)
]}

Note that we used a different function, [colcmp_some_flt] instead of
[colcmp_flt]. This function interprets [None] values as unknowns
according to Kleene. Of course, "y > 0" only selects the positive
ints, and not the [None]s. The difference to the Boolean interpretation
gets apparent when you look at the negation:

{[
let nexpr = Zcontainer.(Zfilter.not_flt (Zfilter.colcmp_some_flt y Ztypes.GT 0))
]}

This gets us the rows with "y <= 0", exluding the [None] values. For
a Boolean interpretation the negation would also select the [None]
values.

{b Performance:} The speed of creating filter sets is hard to predict.
There is no general preference regarding the column representation: Both
direct and inverted columns can work good or bad for a given filter
expression.

{2 Grouping}

The module {!Zcontainer.Zgrouping} supports n-dimensional grouping.
This means the rows that have the same values for a column or a tuple
of columns are grouped together.

Let's assume "year" and "month" are both string-valued columns. We want
to group the data by pairs (year,month):

{[
let year = Zcontainer.(Ztable.get_zcol ztab Ztypes.Zstring "year")
let month = Zcontainer.(Ztable.get_zcol ztab Ztypes.Zstring "month")
let gspec =
  Zcontainer.(
    Zgrouping.also_by month Ztypes.ASC
      (Zgrouping.group_by year Ztypes.ASC)
  )
]}

We can specify the order of the groups: both years and months should
be returned in an ascending way.

Now get the groups:

{[
let groups = Zcontainer.Zgrouping.list pool ztab gspec
]}

This is a list of [(string,string) group] objects. For every group
object we can get the value and the set (i.e. the rows of that group):

{[
List.iter
  (fun group ->
    let (year,month) = Zcontainer.Zgrouping.group_value group in
    let set = Zcontainer.Zgrouping.group_set group in
    ...
  )
  groups
]}

Grouping has a number of options, e.g. you can directly specify a
filter expression, or you can limit the number of groups returned
(only get the first n groups).

{b Performance:} Grouping requires inverted columns for good
performance.
