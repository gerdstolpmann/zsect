open Zcontainer
open Zcontainer.Ztypes
open Printf

let cmd_schema() =
  let zcont = ref "" in
  Arg.parse
    [ "-in", Arg.Set_string zcont,
      "<filename>  Set the container to use";
    ]
    (fun arg -> raise(Arg.Bad("Unexpected arg: " ^ arg)))
    "usage: ztool schema -in <cont>";
  if !zcont = "" then
    failwith "No container";
  let tab = Ztable.from_file Zrepr.stdrepr !zcont in
  let cols = Ztable.columns tab in
  let cols = List.sort compare cols in
  printf "TABLE %s (\n" (Filename.basename !zcont);
  List.iter
    (fun colname ->
       let ZcolBox(_,_,repr,ord) = Ztable.get_zcolbox tab colname in
       printf "  %s : %s" colname (string_of_zrepr repr);
       printf " ORDER %s;\n" (string_of_zorder ord)
    )
    cols;
  printf ")\n"

let cmd_numrows() =
  let zcont = ref "" in
  Arg.parse
    [ "-in", Arg.Set_string zcont,
      "<filename>  Set the container to use";
    ]
    (fun arg -> raise(Arg.Bad("Unexpected arg: " ^ arg)))
    "usage: ztool numrows -in <cont>";
  if !zcont = "" then
    failwith "No container";
  let tab = Ztable.from_file Zrepr.stdrepr !zcont in
  let Space n = Ztable.space tab in
  printf "%d\n" n

let cmd_stats() =
  let zcont = ref "" in
  Arg.parse
    [ "-in", Arg.Set_string zcont,
      "<filename>  Set the container to use";
    ]
    (fun arg -> raise(Arg.Bad("Unexpected arg: " ^ arg)))
    "usage: ztool stats -in <cont>";
  if !zcont = "" then
    failwith "No container";
  let tab = Ztable.from_file Zrepr.stdrepr !zcont in
  let Space n = Ztable.space tab in
  let cols = Ztable.columns tab in
  let cols = List.sort compare cols in
  printf "%-20s I/D %9s %12s %s\n"
         "Column"
         "#Values"
         "Size"
         "Bytes/row";
  List.iter
    (fun colname ->
       let ZcolBox(_,ty,_,_) = Ztable.get_zcolbox tab colname in
       let col = Ztable.get_zcol tab ty colname in
       match Zcol.content col with
         | Zcol.Dir dcol ->
             let s = Zdircol.storage_size dcol in
             printf "%-20s   D %9s %12d %.3f\n"
                    colname
                    "n/a"
                    s
                    (float s /. float n)
         | Zcol.Inv icol ->
             let s = Zinvcol.storage_size icol in
             let l = Zinvcol.length icol in
             printf "%-20s I   %9d %12d %.3f\n"
                    colname
                    l
                    s
                    (float s /. float n)
    )
    cols
 

let cmd_rmcol() =
  let zcont = ref "" in
  let cols = ref [] in
  Arg.parse
    [ "-inout", Arg.Set_string zcont,
      "<filename>  Set the container to modify";
    ]
    (fun arg -> cols := arg :: !cols)
    "usage: ztool rmcol -inout <cont> column...";
  if !zcont = "" then
    failwith "No container";
  let tab = Ztable.from_file Zrepr.stdrepr !zcont in
  cols := List.rev !cols;
  let error = ref false in
  List.iter
    (fun colname ->
       if Ztable.contains_zcol_name colname tab then (
         Ztable.remove_column ~force:true tab colname;
         printf "Removed: %s\n" colname
       )
       else (
         printf "No such column: %s\n" colname;
         error := true
       )
    )
    !cols

let eq_re = Str.regexp {|^\([^=]+\)=\(.*\)$|}

let cmd_cpcol() =
  let zcont_in = ref "" in
  let zcont_out = ref "" in
  let cols = ref [] in
  Arg.parse
    [ "-out", Arg.Set_string zcont_out,
      "<filename>  Set the container to modify";

      "-in", Arg.Set_string zcont_in,
      "<filename>  Set the container to read";
    ]
    (fun arg -> cols := arg :: !cols)
    "usage: ztool cpcol -out <cont> -in <cont> [column=]column...";
  if !zcont_out = "" then
    failwith "No output container";
  if !zcont_in = "" then
    failwith "No input container";
  let tab_out = Ztable.from_file Zrepr.stdrepr !zcont_out in
  let tab_in = Ztable.from_file Zrepr.stdrepr !zcont_in in
  let Space n_out = Ztable.space tab_out in
  let Space n_in = Ztable.space tab_in in
  if n_out <> n_in then
    failwith (sprintf "Mismatch in the number of rows: output has %d rows, input has %d rows" n_out n_in);
  List.iter
    (fun cpspec ->
       let colname_out, colname_in =
         if Str.string_match eq_re cpspec 0 then
           Str.matched_group 1 cpspec, Str.matched_group 2 cpspec
         else
           cpspec, cpspec in
       let ZcolBox(_,ty,repr,ord) = Ztable.get_zcolbox tab_in colname_in in
       let repr_str = string_of_zrepr repr in
       let ord_str = string_of_zorder ord in
       let col_in = Ztable.get_zcol tab_in ty colname_in in
       match Zcol.content col_in with
         | Zcol.Dir dcol_in ->
             let b =
               Ztable.build_zdircol tab_out colname_out ty repr_str ord_str in
             ( try
                 Zdircol.merge b [| dcol_in |];
                 ignore(Zdircol.build_end b)
               with error ->
                 Zdircol.build_abort b;
                 raise error
             )
         | Zcol.Inv icol_in ->
             let b =
               Ztable.build_zinvcol tab_out colname_out ty repr_str ord_str in
             ( try
                 Zinvcol.merge b [| icol_in |];
                 ignore(Zinvcol.build_end b)
               with error ->
                 Zinvcol.build_abort b;
                 raise error
             )
    )
    !cols

let cmd_merge() =
  let zcont_in = ref [] in
  let zcont_out = ref "" in
  let cols = ref [] in
  Arg.parse
    [ "-out", Arg.Set_string zcont_out,
      "<filename>  Set the container to create";

      "-in", Arg.String (fun s -> zcont_in := s :: !zcont_in),
      "<filename>  Set the container to merge in (several possible)";
    ]
    (fun arg -> cols := arg :: !cols)
    "usage: ztool cpcol -out <cont> -in <cont1> -in <cont2> ... ";
  if !zcont_out = "" then
    failwith "No output container";
  if !zcont_in = [] then
    failwith "No input container";
  if Sys.file_exists !zcont_out then
    failwith (sprintf "Already exists: %s" !zcont_out);
  let zcont_in = Array.of_list (List.rev !zcont_in) in
  let tab_in = Array.map (Ztable.from_file Zrepr.stdrepr) zcont_in in
  let n_total =
    Array.fold_left
      (fun acc tab ->
         let Space n = Ztable.space tab in
         acc + n
      )
      0
      tab_in in
  let cols = Ztable.columns tab_in.(0) in
  let cols = List.sort compare cols in
  Array.iteri
    (fun i tab ->
       let tcols = Ztable.columns tab in
       let tcols = List.sort compare tcols in
       if tcols <> cols then
         failwith(sprintf "Column mismatch: input 0 has (%s) but input %d has (%s)"
                          (String.concat "," cols)
                          i
                          (String.concat "," tcols));
       List.iter
         (fun colname ->
            let ZcolBox(_,ty0,repr0,ord0) =
              Ztable.get_zcolbox tab_in.(0) colname in
            let ZcolBox(_,ty,repr,ord) =
              Ztable.get_zcolbox tab colname in
            match same_ztype ty0 ty with
              | Equal ->
                  if string_of_zrepr repr0 <> string_of_zrepr repr then
                    failwith (sprintf "Column representation mismatch for column %s: input 0 declares it as %s but input %d as %s"
                                    colname
                                    (string_of_zrepr repr0)
                                    i
                                    (string_of_zrepr repr));
                  if string_of_zorder ord0 <> string_of_zorder ord then
                    failwith (sprintf "Column ordering mismatch for column %s: input 0 declares it as %s but input %d as %s"
                                    colname
                                    (string_of_zorder ord0)
                                    i
                                    (string_of_zorder ord));
              | Not_equal ->
                  failwith (sprintf "Column type mismatch for column %s: input 0 declares it with type %s but input %d with type %s"
                                    colname
                                    (string_of_ztype ty0)
                                    i
                                    (string_of_ztype ty))
         )
         cols
    )
    tab_in;
  let coldecl =
    List.map
      (fun colname ->
         let ZcolBox(_,ty0,repr0,ord0) =
           Ztable.get_zcolbox tab_in.(0) colname in
         (colname,
          ZtypeBox ty0,
          string_of_zrepr repr0,
          string_of_zorder ord0
         )
      )
      cols in
  let tab_out =
    Ztable.create_file_ztable Zrepr.stdrepr (create_space n_total) !zcont_out in
  Ztable.merge
    tab_out
    coldecl
    tab_in

let help ch =
  output_string ch {|
ztool <command> <args>

Commands:
 - schema: show schema
 - numrows: show number of rows
 - stats: show storage stats
 - rmcol: remove column(s)
 - cpcol: copy columns over from a second container with same number of rows
 - merge: merge rows from containers with the same columns

Run "ztool <command> -help" to get help per command.
|};
  ()


let main() =
  match Array.to_list Sys.argv with
    | _ :: ("-help"|"--help"|"-h") :: _ ->
        help stdout; exit 0
    | _ :: "schema" :: _ ->
        incr Arg.current;
        cmd_schema()
    | _ :: "numrows" :: _ ->
        incr Arg.current;
        cmd_numrows()
    | _ :: "stats" :: _ ->
        incr Arg.current;
        cmd_stats()
    | _ :: "rmcol" :: _ ->
        incr Arg.current;
        cmd_rmcol()
    | _ :: "cpcol" :: _ ->
        incr Arg.current;
        cmd_cpcol()
    | _ :: "merge" :: _ ->
        incr Arg.current;
        cmd_merge()
    | _ ->
        prerr_endline "Bad arguments.";
        help stderr;
        exit 2


let () =
  Printexc.record_backtrace true;
  try
    main()
  with
    | error ->
        let bt = Printexc.get_backtrace() in
        eprintf "Exception: %s\n" (Printexc.to_string error);
        eprintf "Backtrace: %s\n%!" bt;
        exit 2

