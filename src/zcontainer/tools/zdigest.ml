open Zcontainer
open Zcontainer.Ztypes
open Printf

let parse_name_value s =
  try
    let k = String.index s '=' in
    let name = String.sub s 0 k in
    let value = String.sub s (k+1) (String.length s - k - 1) in
    (name,value)
  with
    | Not_found ->
        failwith("Cannot parse: " ^ s)

let main() =
  let default_repr = ref "IM/string" in
  let default_order = ref "asc" in
  let default_anon = ref "" in
  let cols = ref [] in
  let col_repr = Hashtbl.create 5 in
  let col_order = Hashtbl.create 5 in
  let col_anon = Hashtbl.create 5 in
  let stage_size = ref 1_000_000 in
  let out = ref "out.zcont" in
  let inp = ref [] in

  let set_col_repr s =
    let (name,value) = parse_name_value s in
    Hashtbl.replace col_repr name value in

  let set_col_order s =
    let (name,value) = parse_name_value s in
    Hashtbl.replace col_order name value in

  let set_col_anon s =
    let (name,value) = parse_name_value s in
    Hashtbl.replace col_anon name value in

  Arg.parse
    [ "-default-repr", Arg.Set_string default_repr,
      "<repr>  Set the default representation";

      "-default-order", Arg.Set_string default_order,
      "<order>  Set the default order";

      "-default-anon", Arg.Set_string default_anon,
      "<value>   Set the default of which values are considered anonymous";

      "-repr", Arg.String set_col_repr,
      "<column>=<repr>  Set the representation of <column> to <repr>";

      "-order", Arg.String set_col_order,
      "<column>=<order>  Set the order of <column> to <order>";
  
      "-anon", Arg.String set_col_anon,
      "<column>=<value>  Set the anonymous value for this sparse column";

      "-col", Arg.String (fun s -> cols := s :: !cols),
      "<column>   Set the name of the next column";

      "-stage-size", Arg.Set_int stage_size,
      "<n>   Set the size of each stage (in number of rows)";

      "-out", Arg.Set_string out,
      "<filename>  Set the name of the output zcontainer (suffix .zcont)";
    ]
    (fun s -> inp := s :: !inp)
    "usage: zdigest [options] file.csv ...";

  inp := List.rev !inp;
  if !inp = [] then
    failwith "No inputs";

  cols := List.rev !cols;

  let cur = ref (Csv.of_channel (open_in (List.hd !inp))) in
  
  if !cols = [] then (
    let head = Csv.next !cur in
    cols := head
  );

  let col_descr =
    List.map
      (fun name ->
         let repr_name =
           try Hashtbl.find col_repr name
           with Not_found -> !default_repr in
         let order_name =
           try Hashtbl.find col_order name
           with Not_found -> !default_order in
         let anon_value =
           try Hashtbl.find col_anon name
           with Not_found -> !default_anon in
         let zreprbox =
           zreprbox_of_string Zrepr.stdrepr repr_name in
         let ZreprBox(ty1, repr) = zreprbox in
         ignore(zorder_of_string Zrepr.stdrepr ty1 order_name);
         let p = Zdigester.parser_of_type ty1 anon_value in
         (name, Zdigester.ParserBox(ty1,p), repr_name, order_name)
      )
      !cols in

  let dg =
    Zdigester.create_file_zdigester 
      ~max_stage_rows:!stage_size
      Zrepr.stdrepr
      (Array.of_list col_descr)
      !out in

  while !inp <> [] do
    try
      while true do
        let row = Csv.next_a !cur in
        Zdigester.build_add dg row
      done
    with End_of_file ->
      Csv.close_in !cur;
      inp := List.tl !inp;
      match !inp with
        | next :: _ ->
            cur := Csv.of_channel (open_in next)
        | [] ->
            ()
  done;

  let zt = Zdigester.build_end dg in
  let Space n = Ztable.space zt in
  eprintf "Digested %d rows.\n%!" n


let () =
  Printexc.record_backtrace true;
  try
    main()
  with
    | error ->
        let bt = Printexc.get_backtrace() in
        eprintf "Exception: %s\n" (Printexc.to_string error);
        eprintf "Backtrace: %s\n%!" bt;
        exit 2

