open Zcontainer
open Printf

let main() =
  let ztab_name = ref "" in
  let columns = ref [] in
  let method1 = ref None in
  let saved = ref None in
  let f_diff = ref (fun x -> x) in

  Arg.parse
    [ "-inout",
      Arg.Set_string ztab_name,
      "<filename>   Analyze this zcontainer and append new columns to it";

      "-equal-width-histogram",
      ( let offset = ref 0.0 in
        Arg.Tuple
          [ Arg.Set_float offset;
            Arg.Float (fun x -> method1 := Some (`Eqwidth_histo(!offset, x)))
          ]
      ),
      "<offset> <width>   Create histogram anchored at <offset> with fixed <width>";

      "-log-histogram",
      ( let offset = ref 0.0 in
        Arg.Tuple
          [ Arg.Set_float offset;
            Arg.Int (fun x -> method1 := Some (`Log_histo(!offset, x)))
          ]
      ),
      "<offset> <factor>   Create log histogram anchored at <offset> with <factor> buckets per magnitude";

      "-maxdiff-histogram",
      Arg.Int (fun n -> method1 := Some(`Maxdiff_histo n)),
      "<n>   Create histogram with <n> buckets and boundaries at biggest gaps";

      "-log",
      Arg.Unit (fun () -> f_diff := log),
      "    Take the log of the gap size";
    ]
    (fun name ->
       match !saved with
         | None -> saved := Some name
         | Some name2 -> columns := (name2, name) :: !columns; saved := None
    )
    "usage: zdiscretize [options] { inputcol outputcol }...";

  if !saved <> None then
    failwith "Even number of arguments expected (see zdiscretize -help)";
  
  if !columns = [] then
    failwith "No columns to process";

  if !ztab_name = "" then
    failwith "No zcontainer specified (-inout)";

  if !method1 = None then
    failwith "No method specified";

  let ztab = Ztable.from_file Zrepr.stdrepr !ztab_name in
  
  List.iter
    (fun (incol_name,outcol_name) ->
       match !method1 with
         | Some(`Eqwidth_histo(offset,width)) ->
             let incol = Ztable.get_zcol ztab Ztypes.Zfloat incol_name in
             ignore(Zcontainer.Zdiscretize.equal_width_histogram
                      ~ztable:ztab ~name:outcol_name ~offset ~width incol)
         | Some(`Log_histo(offset,factor)) ->
             let incol = Ztable.get_zcol ztab Ztypes.Zfloat incol_name in
             ignore(Zcontainer.Zdiscretize.logarithmic_histogram
                      ~ztable:ztab ~name:outcol_name ~offset ~factor incol)
         | Some(`Maxdiff_histo number) ->
             let incol = Ztable.get_zcol ztab Ztypes.Zfloat incol_name in
             ignore(Zcontainer.Zdiscretize.maxdiff_histogram
                      ~f_diff:!f_diff
                      ~ztable:ztab ~name:outcol_name ~number incol)
         | None ->
             assert false
    )
    !columns


let () =
  Printexc.record_backtrace true;
  try
    main()
  with
    | error ->
        let bt = Printexc.get_backtrace() in
        eprintf "Exception: %s\n" (Printexc.to_string error);
        eprintf "Backtrace: %s\n%!" bt;
        exit 2

