open Zcontainer
open Ztypes
open Printf

let rec printer_of_type : type t . t ztype -> t -> string =
  fun ty ->
    match ty with
      | Zstring -> sprintf "%S"
      | Zint64 -> Int64.to_string
      | Zint -> string_of_int
      | Zfloat -> string_of_float
      | Zbool -> string_of_bool
      | Zoption ty1 ->
          let sub_printer = printer_of_type ty1 in
          (function
            | None -> "\\N"
            | Some s -> sub_printer s
          )
      | Zpair(ty1,ty2) ->
          let sub_printer1 = printer_of_type ty1 in
          let sub_printer2 = printer_of_type ty2 in
          (fun (x1,x2) ->
             sprintf "(%s,%s)" (sub_printer1 x1) (sub_printer2 x2)
          )
      | Zlist ty1 ->
          let sub_printer1 = printer_of_type ty1 in
          (fun l -> "[" ^ String.concat ";" (List.map sub_printer1 l) ^ "]")
      | Zbundle ty1 ->
          printer_of_type (Zlist(Zpair(Zint,ty1)))

let row_printers zt col_names =
  List.map
    (fun col_name ->
       let ZcolBox(_,ty,_,_) = Ztable.get_zcolbox zt col_name in
       let col = Ztable.get_zcol zt ty col_name in
       let p = printer_of_type ty in
       (fun id ->
          let v = Zcol.value_of_id col id in
          p v
       )
    )
    col_names

let main() =
  let mode = ref `Table in
  let inp = ref [] in
  let cols = ref [] in
  let counts = ref false in
  let row_ids = ref false in
  Arg.parse
    [ "-in", Arg.String (fun s -> inp := s :: !inp),
      "<filename>   Dump this zcontainer";
      
      "-table", Arg.Unit (fun () -> mode := `Table),
      "  Mode: Dump as a table. This is the default";

      "-inverted", Arg.Unit (fun () -> mode := `Inverted),
      "  Mode: Dump as inverted table";

      "-inv-counts", Arg.Set counts,
      "  for -inverted: print counts per value";
      
      "-inv-row-ids", Arg.Set row_ids,
      "  for -inverted: print row IDs per value";
    ]
    (fun s -> cols := s :: !cols)
    "usage: zdump [options] [ column ... ]";

  inp := List.rev !inp;
  cols := List.rev !cols;

  List.iter
    (fun file ->
       let zt = Ztable.from_file Zrepr.stdrepr file in
       let Space n = Ztable.space zt in
       let all_zset = Zset.all Zset.default_pool (Ztable.space zt) in
       let all_cols = Ztable.columns zt in
       let sel_cols =
         if !cols = [] then
           List.sort String.compare all_cols
         else
           !cols in
       match !mode with
         | `Table ->
             let printers = row_printers zt sel_cols in
             print_endline (String.concat "," sel_cols);
             for id = 1 to n do
               let strings = List.map (fun p -> p id) printers in
               print_endline (String.concat "," strings)
             done
         | `Inverted ->
             List.iter
               (fun col_name ->
                  let ZcolBox(_,ty,_,_) = Ztable.get_zcolbox zt col_name in
                  let col = Ztable.get_zcol zt ty col_name in
                  let p = printer_of_type ty in
                  let inv = Zcol.invert col all_zset in
                  let itr = Zcol.iter_values inv in
                  printf "Column %S:\n%!" col_name;
                  while not itr#beyond do
                    let _, v = itr#current_value in
                    print_string (p v); flush stdout;
                    if !counts || !row_ids then (
                      let zset = itr#current_zset Zset.default_pool in
                      if !counts then
                        printf ":%d" (Zset.count zset);
                      if !row_ids then (
                        print_string "=";
                        let first = ref true in
                        Zset.iter
                          (fun start len ->
                             if not !first then print_char ',';
                             if len = 1 then
                               printf "%d" start
                             else
                               printf "%d-%d" start (start+len-1);
                             first := false
                          )
                          zset
                      )
                    );
                    print_newline();
                    itr#next()
                  done;
                  print_newline();
                  flush stdout;
               )
               sel_cols
    )
    !inp

let () =
  Printexc.record_backtrace true;
  try
    main()
  with
    | error ->
        let bt = Printexc.get_backtrace() in
        eprintf "Exception: %s\n" (Printexc.to_string error);
        eprintf "Backtrace: %s\n%!" bt;
        exit 2


