(* Bigarray pools *)

module type POOL_CONFIG =
  sig
    type entry
    type data_type
    type data_repr

    val bigarray_kind : (data_type, data_repr) Bigarray.kind
    val bigarray_element_size : int

    val get_bigarray :
         entry -> (data_type, data_repr, Bigarray.c_layout) Bigarray.Array1.t

    val set_bigarray :
         entry -> (data_type, data_repr, Bigarray.c_layout) Bigarray.Array1.t ->
         unit
  end


module Pool(PC:POOL_CONFIG) = struct
  type data =
      (PC.data_type, PC.data_repr, Bigarray.c_layout) Bigarray.Array1.t

  type pool =
    { mutable pool_fd : Unix.file_descr option;
      mutable pool_data : data;
      mutable pool_data_end : int;
      mutable pool_entries : PC.entry Weak.t;
      mutable pool_entries_end : int;
      mutable pool_last : (PC.entry * int) option;
      pool_resize : pool -> int -> int -> unit;
    }

  let size_of_pool p =
    let size = ref 0 in
    let entries = ref 0 in
    for k = 0 to p.pool_entries_end - 1 do
      match Weak.get p.pool_entries k with
        | None -> ()
        | Some entry ->
            size := !size + Bigarray.Array1.dim (PC.get_bigarray entry);
            incr entries
  done;
  (!size, !entries)


  let copy_pool p q =
    (* Copy everything from p to q, and skip unused zsets. q must be large
       enough
     *)
    q.pool_last <- None;
    for k = 0 to p.pool_entries_end - 1 do
      match Weak.get p.pool_entries k with
        | None -> ()
        | Some entry ->
            let data = PC.get_bigarray entry in
            let size = Bigarray.Array1.dim data in
            let offset = q.pool_data_end in
            let new_data =
              Bigarray.Array1.sub q.pool_data offset size in
            Bigarray.Array1.blit data new_data;
            PC.set_bigarray entry new_data;
            q.pool_data_end <- offset + size;
            Weak.set q.pool_entries q.pool_entries_end (Some entry);
            q.pool_entries_end <- q.pool_entries_end + 1;
            q.pool_last <- Some(entry, offset)
    done

  let create_pool_1 pool_resize n_data n_entries =
    { pool_fd = None;
      pool_data =
        Bigarray.Array1.create PC.bigarray_kind Bigarray.c_layout n_data;
      pool_data_end = 0;
      pool_entries = Weak.create n_entries;
      pool_entries_end = 0;
      pool_resize;
      pool_last = None;
  }

  let resize_pool p n_data n_entries =
    let q = create_pool_1 p.pool_resize n_data n_entries in
    copy_pool p q;
    p.pool_data <- q.pool_data;
    p.pool_data_end <- q.pool_data_end;
    p.pool_entries <- q.pool_entries;
    p.pool_entries_end <- q.pool_entries_end;
    p.pool_last <- q.pool_last

  let create_pool =
    create_pool_1 resize_pool

  let map_pool_1 fd pool_resize n_data n_entries =
    { pool_fd = Some fd;
      pool_data =
        Bigarray.Array1.map_file
          fd PC.bigarray_kind Bigarray.c_layout true n_data;
      pool_data_end = 0;
      pool_entries = Weak.create n_entries;
      pool_entries_end = 0;
      pool_resize;
      pool_last = None;
  }

  let resize_mapped_pool p n_data n_entries =
    (* No garbage collection in this case! Only enlarge! *)
    match p.pool_fd with
      | Some fd ->
          (* remap the pool. TODO: use remap() syscall *)
          let n = max n_data p.pool_data_end in
          if n < Bigarray.Array1.dim p.pool_data then
            Unix.LargeFile.ftruncate
              fd (Int64.of_int (n * PC.bigarray_element_size));
          let data =
            Bigarray.Array1.map_file
              fd PC.bigarray_kind Bigarray.c_layout true n in
          p.pool_data <- data;
          let ne = max n_entries p.pool_entries_end in
          let new_entries = Weak.create ne in
          Weak.blit p.pool_entries 0 new_entries 0 ne;
          p.pool_entries <- new_entries
      | None ->
          assert false


  let map_pool fd =
    map_pool_1 fd resize_mapped_pool

  let require_pool p n_data n_entries =
    (* Ensure that the pool is large enough for n_entries additional entries and
       n_data additional data elements
     *)
    if p.pool_entries_end + n_entries >= Weak.length p.pool_entries ||
         p.pool_data_end + n_data > Bigarray.Array1.dim p.pool_data then (
      let p_size, p_entries = size_of_pool p in
      let new_n_data =
        if n_data = 0 then p_size
        else (p_size + n_data) + (p_size + n_data)/2 in
      let new_n_entries =
        if n_entries = 0 then p_entries
        else (p_entries + n_entries) + (p_entries + n_entries)/2 in
      p.pool_resize p new_n_data new_n_entries
    )


  let alloc_entry alloc p size =
    require_pool p size 1;
    let offset = p.pool_data_end in
    let data = Bigarray.Array1.sub p.pool_data offset size in
    p.pool_data_end <- offset + size;
    let entry = alloc data in
    Weak.set p.pool_entries p.pool_entries_end (Some entry);
    p.pool_entries_end <- p.pool_entries_end + 1;
    p.pool_last <- Some(entry,offset);
    entry


  let resize_entry ?(assert_last=false) p entry size =
    (* the precondition is that entry was allocated on p *)
    let data = PC.get_bigarray entry in
    if size <= Bigarray.Array1.dim data then (
      (* shrink *)
      ( match p.pool_last with
          | Some(entry',offset) when entry == entry' ->
              p.pool_data_end <- offset + size
          | _ ->
              ()
      );
      let new_data = Bigarray.Array1.sub data 0 size in
      PC.set_bigarray entry new_data
    )
    else
      (* enlarge *)
      let dspace = Bigarray.Array1.dim p.pool_data in
      match p.pool_last with
        | Some(entry',offset) when entry == entry' && offset+size <= dspace ->
            (* The entry is the last one of the pool. Simply reslice zs_data *)
            let new_data =
              Bigarray.Array1.sub p.pool_data offset size in
            p.pool_data_end <- offset + size;
            PC.set_bigarray entry new_data
        | Some(entry',_) when entry == entry' ->
            (* The entry is the last one, but we need to enlarge the pool *)
            require_pool p size 0;
            ( match p.pool_last with
                | Some(entry'',offset) ->
                    assert(entry' == entry'');
                    let new_data =
                      Bigarray.Array1.sub p.pool_data offset size in
                    p.pool_data_end <- offset + size;
                    PC.set_bigarray entry new_data
                | None ->
                    assert false
            )
        | _ ->
            assert(not assert_last);
            require_pool p size 0;
            let offset = p.pool_data_end in
            let new_data = Bigarray.Array1.sub p.pool_data offset size in
            p.pool_data_end <- offset + size;
            Bigarray.Array1.blit
              data
              (Bigarray.Array1.sub new_data 0 (Bigarray.Array1.dim data));
            p.pool_last <- Some(entry,offset);
            PC.set_bigarray entry new_data

  let truncate p =
    match p.pool_fd with
      | None -> ()
      | Some fd ->
          let new_data = Bigarray.Array1.sub p.pool_data 0 p.pool_data_end in
          Unix.LargeFile.ftruncate
            fd 
            (Int64.of_int (p.pool_data_end * PC.bigarray_element_size));
          p.pool_data <- new_data

  let close p =
    match p.pool_fd with
      | None -> ()
      | Some fd ->
          Unix.close fd;
          p.pool_fd <- None

          (* TODO: msync *)

end
