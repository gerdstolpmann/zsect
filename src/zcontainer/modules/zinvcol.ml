(* Inverted columns *)

open Ztypes
open Zutil
open Printf

module Int =
  struct type t = int let compare : int -> int -> int = compare end
module IntSet =
  Set.Make(Int)

exception Anonymous

class type ['a] ziterator =
  object
    method current_pos : int
    method current_value : int * 'a
    method current_value_lz : (int * 'a) Lazy.t
    method current_zset : Zset.pool -> Zset.zset
    method current_zset_lz : Zset.pool -> Zset.zset Lazy.t
    method beyond : bool
    method next : unit -> unit
    method prev : unit -> unit
    method go_beginning : unit -> unit
    method go_end : unit -> unit
    method go : int -> unit
    method find : comparison -> 'a -> unit
    method length : int
  end

let iter_empty() : _ ziterator =
  object
    method current_pos = 0
    method current_value = raise Not_found
    method current_value_lz = lazy(raise Not_found)
    method current_zset _ = raise Not_found
    method current_zset_lz _ = lazy(raise Not_found)
    method beyond = true
    method next() = ()
    method prev() = ()
    method go_beginning() = ()
    method go_end() = ()
    method go k = ()
    method find _ _ = raise Not_found
    method length = 0
  end

let find_ins_pos : 'a zorder -> 'a ziterator -> 'a -> int * bool =
  fun ord itr default ->
    itr # go_beginning();
    let h_default = ord.ord_hash default in
    let lt (h,v) =
      h < h_default || (h = h_default && ord.ord_cmp v default < 0) in
    let eq (h,v) =
      h = h_default && ord.ord_cmp v default = 0 in
    let k = ref 0 in
    while not(itr # beyond) && lt (itr # current_value) do
      itr # next();
      incr k;
    done;
    (!k, not(itr # beyond) && eq (itr # current_value))

let compare_hv ord ((h1:int),x1) cmp (h2,x2) =
  let test = compare_test cmp in
  let d1 = Pervasives.compare h1 h2 in
  if d1=0 then
    test(ord.ord_cmp x1 x2)
  else
    test d1


(*
let iter_with_default : 'a zorder -> 'a ziterator -> 'a -> Zset.zset ->
                        'a ziterator =
  fun ord itr default all ->
    let h_default = ord.ord_hash default in
    let pos_default, exists_default = find_ins_pos ord itr default in
    itr # go_beginning();
    let cur = ref 0 in
    let len = if exists_default then itr#length else itr#length+1 in
    let at_ins_pos() =
      !cur = pos_default && not exists_default in
    let reconcile_cur() =
      let p = itr # current_pos in
      cur := if not exists_default && p >= pos_default then p+1 else p in
    let get_non_default_set pool =
      let p = itr # current_pos in
      itr # go_beginning();
      let l = ref [] in
      while not itr # beyond do
        if not exists_default || itr # current_pos <> pos_default then
          l := itr # current_zset pool :: !l;
        itr # next();
      done;
      itr # go p;
      let s = Zset.union_many pool !l in
      s in
    let non_default_set_lz pool = lazy(get_non_default_set pool) in
    ( object(self)
        method current_pos = !cur
        method current_value =
          if at_ins_pos() then
            (h_default, default)
          else
            itr # current_value
        method current_value_lz =
          if at_ins_pos() then
            lazy(h_default, default)
          else
            itr # current_value_lz

        method current_zset pool =
          if !cur = pos_default then
            Zset.diff pool all (Lazy.force (non_default_set_lz pool))
          else
            itr # current_zset pool

        method current_zset_lz pool =
          if !cur = pos_default then
            lazy(Zset.diff pool all (Lazy.force (non_default_set_lz pool)))
          else
            itr # current_zset_lz pool

        method beyond = !cur < 0 || !cur >= len

        method length = len

        method next() =
          if at_ins_pos() then
            itr # go pos_default
          else
            itr # next();
          incr cur

        method prev() =
          if at_ins_pos() then
            itr # go (pos_default-1)
          else
            itr # prev();
          decr cur

        method go_beginning() =
          cur := 0;
          itr # go_beginning()

        method go_end() =
          cur := len;
          itr # go_end();

        method go k =
          cur := k;
          let k' =
            if exists_default || k <= pos_default then k else k-1 in
          itr # go k'

        method find cmp x =
          let h = ord.ord_hash x in
          try
            itr # find cmp x;  (* or Not_found *)
            let (h_inner,inner) = itr # current_value in
            if cmp <> NE &&
                 compare_hv ord (h_default,default) cmp (h,x) &&
                   compare_hv ord (h_inner,inner) cmp (h_default,default)
            then
              self # go pos_default
            else
              reconcile_cur()
          with
            | Not_found when compare_hv ord (h_default,default) cmp (h,x) ->
                self # go pos_default

      end
    )
 *)

let iter_isect ?(all_values=false) itr fltset =
  if all_values then
    ( object
        method current_pos = itr#current_pos
        method current_value = itr#current_value
        method current_value_lz = itr#current_value_lz
        method current_zset pool =
          let s = itr # current_zset pool in
          Zset.isect pool fltset s
        method current_zset_lz pool =
          let s_lz = itr # current_zset_lz pool in
          lazy (
              let s = Lazy.force s_lz in
              Zset.isect pool fltset s
            )
        method beyond = itr#beyond
        method next = itr#next
        method prev = itr#prev
        method go_beginning = itr#go_beginning
        method go_end = itr#go_end
        method go = itr#go
        method find = itr#find
        method length = itr#length
      end
    )
  else
    (* TODO: avoid that the sets are all intersected with fltset at the
       beginning of this function. Instead, do this only as far needed.
     *)
    let len = itr#length in
    let non_empty = Array.make len false in
    let le = Array.make len (-1) in
    let ge = Array.make len (-1) in
    itr # go_beginning();
    let pool = Zset.create_pool 100 1000 in
    let k = ref 0 in
    let n = ref 0 in
    while not (itr#beyond) do
      let s = itr # current_zset pool in
      let ne = Zset.isect_non_empty s fltset in
      non_empty.(!k) <- ne;
      incr k;
      if ne then incr n;
      itr # next();
    done;
    let m = Array.make !n 0 in
    k := 0;
    for j = 0 to len-1 do
      if non_empty.(j) then (
        m.(!k) <- j;
        le.(j) <- !k;
        ge.(j) <- !k;
        incr k
      ) else
        if j > 0 then
          le.(j) <- le.(j-1)
    done;
    for j = len-2 downto 0 do
      if ge.(j) = (-1) then
        ge.(j) <- ge.(j+1)
    done;
    let cur = ref 0 in
    if !n > 0 then
      itr # go m.(0);
    ( object(self)
        method current_pos = !cur
        method current_value =
          if !cur < 0 || !cur >= !n then raise Not_found;
          itr # current_value
        method current_value_lz =
          if !cur < 0 || !cur >= !n then raise Not_found;
          itr # current_value_lz
        method current_zset pool =
          if !cur < 0 || !cur >= !n then raise Not_found;
          let s = itr # current_zset pool in
          Zset.isect pool s fltset
        method current_zset_lz pool =
          if !cur < 0 || !cur >= !n then raise Not_found;
          let s_lz = itr # current_zset_lz pool in
          lazy (
              Zset.isect pool (Lazy.force s_lz) fltset
            )
        method beyond =
          !cur < 0 || !cur >= !n
        method next() =
          incr cur;
          if !cur >= 0 && !cur < !n then
            itr # go m.(!cur)
        method prev() =
          decr cur;
          if !cur >= 0 && !cur < !n then
            itr # go m.(!cur)
        method go_beginning() =
          cur := 0;
          if !n > 0 then
            itr # go m.(0);
        method go_end() =
          cur := !n;
          itr # go_end()
        method go k =
          cur := k;
          if k >= 0 && k < !n then
            itr # go m.(k)
        method find cmp x =
          let p = itr # current_pos in
          let not_found() =
            itr # go p; raise Not_found in
          match cmp with
            | LT | LE ->
                itr # find cmp x;
                let k = le.(itr#current_pos) in
                if k = (-1) then not_found();
                self # go k
            | GT | GE ->
                itr # find cmp x;
                let k = ge.(itr#current_pos) in
                if k = (-1) then not_found();
                self # go k
            | EQ ->
                itr # find cmp x;
                let k1 = le.(itr#current_pos) in
                let k2 = ge.(itr#current_pos) in
                if k1 = (-1) || k2 = (-1) || k1 <> k2 then not_found();
                self # go k1
            | NE ->
                itr # find cmp x;
                let k1 = le.(itr#current_pos) in
                let k2 = ge.(itr#current_pos) in
                if k1 = (-1) then (
                  if k2 = (-1) then not_found();
                  self # go k2
                ) else
                  self # go k1
        method length =
          !n
      end
    )

let remove ?(force=false) dir col_name =
  let name = Filename.concat dir (Zutil.FileUtil.encode_name col_name) in
  Zutil.FileUtil.rm ~force (name ^ ".inv");
  Zutil.FileUtil.rm ~force (name ^ ".idt");
  Zutil.FileUtil.rm ~force (name ^ ".idx");
  Zutil.FileUtil.rm ~force (name ^ ".idtmem");
  Zutil.FileUtil.rm ~force (name ^ ".invmem")

            
module N = struct
  type inv_data =
      (int64, Bigarray.int64_elt, Bigarray.c_layout) Bigarray.Array1.t

  type idt_data =
      (int, Bigarray.int_elt, Bigarray.c_layout) Bigarray.Array1.t

  type idt_char_data =
      (char, Bigarray.int8_unsigned_elt, Bigarray.c_layout) Bigarray.Array1.t

  type idx_data =
    | I2 of (int, Bigarray.int8_signed_elt, Bigarray.c_layout) Bigarray.Array1.t
    | I4 of (int, Bigarray.int8_signed_elt, Bigarray.c_layout) Bigarray.Array1.t
    | I8 of (int, Bigarray.int8_signed_elt, Bigarray.c_layout) Bigarray.Array1.t
    | I16 of (int, Bigarray.int16_signed_elt, Bigarray.c_layout) Bigarray.Array1.t
    | I32 of (int32, Bigarray.int32_elt, Bigarray.c_layout) Bigarray.Array1.t
    | I64 of (int64, Bigarray.int64_elt, Bigarray.c_layout) Bigarray.Array1.t

  type idx_kind =
    | I2_kind | I4_kind | I8_kind | I16_kind | I32_kind | I64_kind

  type zinvdata =
      { mutable ic_inv_data : inv_data;
        mutable ic_idt_data : idt_data;
        mutable ic_idt_char_data : idt_char_data;
        mutable ic_idx_data : idx_data;
        mutable ic_sparse : (int * int) option;   (* used/total *)
      }

  type 'a zinvcol =
      { ic_space : space;
        ic_type : 'a zcoldescr;
        ic_data : zinvdata;
      }

  let kind_of_idx_data =
    function
    | I2 _ -> I2_kind
    | I4 _ -> I4_kind
    | I8 _ -> I8_kind
    | I16 _ -> I16_kind
    | I32 _ -> I32_kind
    | I64 _ -> I64_kind

  let space ic = ic.ic_space
  let descr ic = ic.ic_type

  let kind_of_space (Space sp) =
    if sp <= 2 then
      I2_kind
    else if sp <= 14 then
      I4_kind
    else if sp <= 127 then
      I8_kind
    else if sp <= 32767 then
      I16_kind
    else if sp <= 2147483647 then
      I32_kind
    else
      I64_kind

  let kind_max =
    function
      | I2_kind -> 2
      | I4_kind -> 14
      | I8_kind -> 127
      | I16_kind -> 32767
      | I32_kind -> 2147483647
      | I64_kind -> max_int

  let kind_bits =
    function
      | I2_kind -> 2
      | I4_kind -> 4
      | I8_kind -> 8
      | I16_kind -> 16
      | I32_kind -> 32
      | I64_kind -> 64

  let kind_ht_fixup =
    (* hash tables for sparse indexes are at least 8 bits wide *)
    function
    | I2_kind
    | I4_kind -> I8_kind
    | k -> k


  let storage_size icol =
    let d = icol.ic_data in
    8 * Bigarray.Array1.dim d.ic_inv_data +
      8 * Bigarray.Array1.dim d.ic_idt_data +
      Bigarray.Array1.dim d.ic_idt_char_data +
      ( match d.ic_idx_data with
          | I2 a
          | I4 a
          | I8 a -> Bigarray.Array1.dim a
          | I16 a -> 2 * Bigarray.Array1.dim a
          | I32 a -> 4 * Bigarray.Array1.dim a
          | I64 a -> 8 * Bigarray.Array1.dim a
      )

  let create_I8 n =
    Bigarray.Array1.create Bigarray.int8_signed Bigarray.c_layout n

  let create_idx_data kind n =
    match kind with
      | I2_kind ->
          let n = if n=0 then 0 else (n-1) / 4 + 1 in
          I2(create_I8 n)
      | I4_kind ->
          let n = if n=0 then 0 else (n-1) / 2 + 1 in
          I4(create_I8 n)
      | I8_kind ->
          I8(create_I8 n)
      | I16_kind ->
          I16(Bigarray.Array1.create Bigarray.int16_signed Bigarray.c_layout n)
      | I32_kind ->
          I32(Bigarray.Array1.create Bigarray.int32 Bigarray.c_layout n)
      | I64_kind ->
          I64(Bigarray.Array1.create Bigarray.int64 Bigarray.c_layout n)

  let map_I8 fd shared n =
    Bigarray.Array1.map_file
      fd Bigarray.int8_signed Bigarray.c_layout shared n

  let map_idx_data fd shared kind n =
    match kind with
    | I2_kind ->
        let n = if n=0 then 0 else (n-1) / 4 + 1 in
        I2(map_I8 fd shared n)
    | I4_kind ->
        let n = if n=0 then 0 else (n-1) / 2 + 1 in
        I4(map_I8 fd shared n)
    | I8_kind ->
        I8(map_I8 fd shared n)
    | I16_kind ->
        I16(Bigarray.Array1.map_file
              fd Bigarray.int16_signed Bigarray.c_layout shared n)
    | I32_kind ->
        I32(Bigarray.Array1.map_file
              fd Bigarray.int32 Bigarray.c_layout shared n)
    | I64_kind ->
        I64(Bigarray.Array1.map_file
              fd Bigarray.int64 Bigarray.c_layout shared n)

  let truncate_idx_data fd kind n =
    let bits = kind_bits kind in
    let bytes =
      if bits >= 8 then
        n * (bits/8)
      else
        if n=0 then
          0
        else
          (n-1) / bits + 1 in
    Unix.LargeFile.ftruncate fd (Int64.of_int bytes)


  let fill_idx_data =
    function
    | I2 ba
    | I4 ba
    | I8 ba ->
        Bigarray.Array1.fill ba (-1)
    | I16 ba ->
        Bigarray.Array1.fill ba (-1)
    | I32 ba ->
        Bigarray.Array1.fill ba (-1l)
    | I64 ba ->
        Bigarray.Array1.fill ba (-1L)

  let set_idx_bits ba shift entries_per_byte bits_per_entry x_max start len x =
    let x = if x < 0 then x_max else x land x_max in
    let start_by = start asr shift in
    let start_bi = start land (entries_per_byte-1) in
    let last_by = (start+len-1) asr shift in
    let last_bi = (start+len-1) land (entries_per_byte-1) in
    let x_mask = (-1) lxor (x_max lsl 8) in
    if start_by = last_by then (
      let x_start = ref ba.{start_by} in
      let offs = ref (start_bi * bits_per_entry) in
      for bi = start_bi to last_bi do
        let m = x_mask asr (8 - !offs) in
        x_start := (!x_start land m) lor (x lsl !offs);
        offs := !offs + bits_per_entry
      done;
      ba.{start_by} <- !x_start;
    )
    else if start_by < last_by then (
      let x_start = ref ba.{start_by} in
      let offs = ref (start_bi * bits_per_entry) in
      for bi = start_bi to entries_per_byte-1 do
        let m = x_mask asr (8 - !offs) in
        x_start := (!x_start land m) lor (x lsl !offs);
        offs := !offs + bits_per_entry
      done;
      ba.{start_by} <- !x_start;
      let x_repl = ref 0 in
      for k = 0 to entries_per_byte-1 do
        x_repl := !x_repl lor (x lsl (k*bits_per_entry))
      done;
      for by = start_by+1 to last_by-1 do
        ba.{ by } <- !x_repl
      done;
      let x_last = ref ba.{last_by} in
      offs := 0;
      for bi = 0 to last_bi do
        let m = x_mask asr (8 - !offs) in
        x_last := (!x_last land m) lor (x lsl !offs);
        offs := !offs + bits_per_entry
      done;
      ba.{last_by} <- !x_last
    )


  let set_idx_data start len x =
    function
    | I2 ba ->
        set_idx_bits ba 2 4 2 3 start len x
    | I4 ba ->
        set_idx_bits ba 1 2 4 15 start len x
    | I8 ba ->
        for k = 0 to len-1 do ba.{ start+k } <- x done
    | I16 ba ->
        for k = 0 to len-1 do ba.{ start+k } <- x done
    | I32 ba ->
        let xl = Int32.of_int x in
        for k = 0 to len-1 do ba.{ start+k } <- xl done
    | I64 ba ->
        let xL = Int64.of_int x in
        for k = 0 to len-1 do ba.{ start+k } <- xL done


  let get_idx_bits ba shift entries_per_byte bits_per_entry x_max k =
    let k_by = k asr shift in
    let k_bi = k land (entries_per_byte-1) in
    let x_k = ba.{k_by} in
    let offs = k_bi * bits_per_entry in
    let x = (x_k lsr offs) land x_max in
    if x = x_max then (-1) else x

  let get_idx_data k =
    function
    | I2 ba ->
        get_idx_bits ba 2 4 2 3 k
    | I4 ba ->
        get_idx_bits ba 1 2 4 15 k
    | I8 ba ->
        ba.{ k }
    | I16 ba ->
        ba.{ k }
    | I32 ba ->
        Int32.to_int ba.{ k }
    | I64 ba ->
        Int64.to_int ba.{ k }


  module Pool_config_inv = struct
    type entry = zinvdata
    type data_type = int64
    type data_repr = Bigarray.int64_elt
    let bigarray_kind = Bigarray.int64
    let bigarray_element_size = 8
    let get_bigarray ic = ic.ic_inv_data
    let set_bigarray ic data = ic.ic_inv_data <- data
  end

  module P_inv = Zpool.Pool(Pool_config_inv)

  module Pool_config_idt = struct
    type entry = zinvdata
    type data_type = int
    type data_repr = Bigarray.int_elt
    let bigarray_kind = Bigarray.int
    let bigarray_element_size = 8
    let get_bigarray ic = ic.ic_idt_data
    let set_bigarray ic data =
      ic.ic_idt_data <- data;
      ic.ic_idt_char_data <- Netsys_mem.memory_of_bigarray_1 data
  end

  module P_idt = Zpool.Pool(Pool_config_idt)

  type 'a zinvcol_builder =
      { mutable b_open : bool;
        b_fsname : (string * string) option;
        b_inv_pool : P_inv.pool;
        b_idt_pool : P_idt.pool;
        b_ic : 'a zinvcol;
        mutable b_inv_end : int;
        mutable b_idt_end : int;
        b_num_records : int;
        b_mb : 'a multibyte;
        b_order : 'a zorder;
        mutable b_count : int;
        mutable b_last_hash : int;
        mutable b_last_val : 'a option;
        mutable b_done : 'a zinvcol -> unit;
        b_is_anon : 'a -> bool;
        b_no_index : bool;
      }

  let alloc_zinvcol_builder ?(no_index=false)
                            fd_triple_opt inv_size idt_size ic_space ic_type =
    let Space sp = ic_space in
    if inv_size < 1 || idt_size < 1 || sp < 1 then
      invalid_arg "Zcontainer.Zinvcol.alloc_zinvcol_builder";
    let inv_size = (inv_size + 1) * 4 in
    let ic_sparse =
      match ic_type with
        | ZcolDescr(_,_,repr,zo) ->
            ( match repr with
                | SInv_multibyte _ -> Some(0,97)
                | _ -> None
            ) in
    let alloc b_inv_pool b_idt_pool establish_idx_data =
      let b_data =
        P_inv.alloc_entry
          (fun ic_inv_data ->
             P_idt.alloc_entry
               (fun ic_idt_data ->
                  let ic_idt_char_data =
                    Netsys_mem.memory_of_bigarray_1 ic_idt_data in
                  let ic_idx_data =
                    if no_index then
                      let kind = kind_of_space ic_space in
                      create_idx_data kind 0
                    else
                      match ic_sparse with
                        | None ->
                            let kind = kind_of_space ic_space in
                            establish_idx_data kind sp
                        | Some(_,n) ->
                            let kind = kind_ht_fixup (kind_of_space ic_space) in
                            establish_idx_data kind (2*n) in
                  fill_idx_data ic_idx_data;
                  { ic_inv_data;
                    ic_idt_data;
                    ic_idt_char_data;
                    ic_idx_data;
                    ic_sparse;
                  }
               )
               b_idt_pool
               idt_size
          )
          b_inv_pool
          inv_size in
      let mb, zo, is_anon =
        match ic_type with
          | ZcolDescr(_,_,repr,zo) ->
              ( match repr with
                  | Inv_multibyte mb ->
                      (mb, zo, (fun _ -> false))
                  | SInv_multibyte(mb,_,is_anon) ->
                      (mb, zo, is_anon)
                  | _ ->
                      failwith
                        "Zcontainer.Zinvcol: bad representation for zinvcol"
              ) in
      let b_ic =
        { ic_space;
          ic_type;
          ic_data = b_data
        } in
      { b_open = true;
        b_inv_pool;
        b_idt_pool;
        b_ic;
        b_inv_end = 4;
        b_idt_end = 0;
        b_num_records = sp;
        b_order = zo;
        b_mb = mb;
        b_count = 0;
        b_last_hash = 0;
        b_last_val = None;
        b_done = (fun _ -> ());
        b_fsname = None;
        b_is_anon = is_anon;
        b_no_index = no_index;
      } in
    match fd_triple_opt with
      | Some(fd_inv, fd_idt, fd_idx) ->
          let b_inv_pool = P_inv.map_pool fd_inv inv_size 1 in
          let b_idt_pool = P_idt.map_pool fd_idt idt_size 1 in
          if not no_index then (
            match ic_sparse with
              | None ->
                  let kind = kind_of_space ic_space in
                  truncate_idx_data fd_idx kind sp
              | Some(_,n) ->
                  let kind = kind_ht_fixup(kind_of_space ic_space) in
                  truncate_idx_data fd_idx kind (2*n)
          );
          alloc b_inv_pool b_idt_pool (map_idx_data fd_idx true)
      | None ->
          let b_inv_pool = P_inv.create_pool inv_size 1 in
          let b_idt_pool = P_idt.create_pool idt_size 1 in
          alloc b_inv_pool b_idt_pool create_idx_data

  let create_memory_zinvcol_builder ?no_index arg =
    alloc_zinvcol_builder ?no_index None arg

  let create_file full_name f =
    let fd =
      Unix.openfile
        full_name
        [ Unix.O_RDWR; Unix.O_CREAT; Unix.O_TRUNC; Unix.O_CLOEXEC ]
        0o666 in
    try
      f fd
    with
      | error ->
          Unix.close fd;
          ( try Unix.unlink full_name with _ -> ());
          raise error

  let create_file_zinvcol_builder ?(no_index=false) ?(mem_suffix=false)
                                  dir inv_size idt_size ic_space ic_type =
    let col_name =
      match ic_type with
        | ZcolDescr(n,_,_,_) -> n in
    let name = Filename.concat dir (Zutil.FileUtil.encode_name col_name) in
    let inv_suffix = if mem_suffix then ".invmem" else ".inv" in
    let idt_suffix = if mem_suffix then ".idtmem" else ".idt" in
    let idx_suffix = ".idx" in
    create_file
      (name ^ inv_suffix)
      (fun fd_inv ->
         create_file
           (name ^ idt_suffix)
           (fun fd_idt ->
              let create_idx =
                if no_index then
                  (fun f -> f fd_idt)
                    (* fd will be ignored by alloc_zinvcol_builder! *)
                else
                  create_file (name ^ idx_suffix) in
              create_idx
                (fun fd_idx ->
                   let b =
                     alloc_zinvcol_builder
                       ~no_index
                       (Some(fd_inv, fd_idt, fd_idx))
                       inv_size idt_size ic_space ic_type in
                   let b =
                     { b with b_fsname = Some(dir,col_name) } in
                   if not no_index then Unix.close fd_idx;
                   (* NB. keep fd_inv and fd_idt open. These descriptors are
                      managed now by the pools, and will be closed when the
                      pools are closed.
                    *)
                   b
                )
           )
      )

  let create_new_sparse_idx b n =
    let ic_space = b.b_ic.ic_space in
    let kind = kind_ht_fixup(kind_of_space ic_space) in
    let ic_idx_data =
      match b.b_fsname with
        | None ->
            create_idx_data kind (2*n)
        | Some(dir,col_name) ->
            let name =
              Filename.concat dir (Zutil.FileUtil.encode_name col_name) in
            create_file
              (name ^ ".idxnew")
              (fun fd_idx ->
                 truncate_idx_data fd_idx kind (2*n);
                 let data = map_idx_data fd_idx true kind (2*n) in
                 Unix.close fd_idx;
                 data
              ) in
    fill_idx_data ic_idx_data;
    ic_idx_data


  let commit_new_sparse_idx b n_used n ic_idx_data =
    ( match b.b_fsname with
        | None -> ()
        | Some(dir,col_name) ->
            let name =
              Filename.concat dir (Zutil.FileUtil.encode_name col_name) in
            Unix.rename (name ^ ".idxnew") (name ^ ".idx")
    );
    b.b_ic.ic_data.ic_idx_data <- ic_idx_data;
    b.b_ic.ic_data.ic_sparse <- Some(n_used,n)


  let ht_hash = Hashtbl.seeded_hash 1234

  let sparse_add_to_idx_1 b n idx_data =
    let n2 = 2*n in
    let rec probe p n id k_idx =
      if get_idx_data p idx_data = (-1) then (
        set_idx_data p 1 id idx_data;
        set_idx_data (p+1) 1 k_idx idx_data
      )
      else
        let p' = if p+2 >= n2 then 0 else p+2 in
        probe p' n id k_idx in
    fun id k_idx ->
      probe (((ht_hash id) mod n) * 2) n id k_idx


  (* TODO: if the sparse idx gets larger than the non-sparse index, downgrade to
     non-sparse format
   *)

  let sparse_idx_rebuild b =
    let old_idx_data = b.b_ic.ic_data.ic_idx_data in
    let old_n =
      match b.b_ic.ic_data.ic_sparse with
        | None -> assert false
        | Some(_,n) -> n in
    let new_n = old_n * 2 in
    let new_idx_data = create_new_sparse_idx b new_n in
    let add = sparse_add_to_idx_1 b new_n new_idx_data in
    let used = ref 0 in
    for i = 0 to old_n-1 do
      let i2 = i lsl 1 in
      let id = get_idx_data i2 old_idx_data in
      if id >= 1 then (
        incr used;
        let k_idx = get_idx_data (i2+1) old_idx_data in
        add id k_idx
      )
    done;
    commit_new_sparse_idx b !used new_n new_idx_data

  let rec sparse_add_to_idx b id k_idx =
    let ic = b.b_ic in
    let ic_data = ic.ic_data in
    match ic_data.ic_sparse with
      | None ->
          assert false
      | Some(used,n) ->
          if 2*used <= n then
            let idx_data = b.b_ic.ic_data.ic_idx_data in
            sparse_add_to_idx_1 b n idx_data id k_idx;
            ic_data.ic_sparse <- Some (used+1,n)
          else (
            sparse_idx_rebuild b;
            sparse_add_to_idx b id k_idx
          )


  let padding = String.make 8 '\000'

  let build_add_hv b hash value_enc zset =
    if hash < 0 then
      invalid_arg "Zcontainer.Zinvcol.build_add";
    ( match zset with
        | Zset.Zs_one(Space sp,_) ->
            if sp > b.b_num_records then
              invalid_arg "Zcontainer.Zinvcol.build_add"
        | Zset.Zs_enc zset ->
            let Space sp = Zset.(zset.zs_space) in
            if sp > b.b_num_records then
              invalid_arg "Zcontainer.Zinvcol.build_add"
        | Zset.Zs_bitset(_,_,_,bitset) ->
            let sp = Bitset.length bitset in
            if sp > b.b_num_records then
              invalid_arg "Zcontainer.Zinvcol.build_add"
        | Zset.Zs_empty ->
            ()
    );
    let ic = b.b_ic in
    let d = ic.ic_data in
    (* Extend inv: *)
    if b.b_inv_end + 3 >= Bigarray.Array1.dim d.ic_inv_data then (
      let new_size = 2 * b.b_inv_end + 1 in
      P_inv.resize_entry ~assert_last:true b.b_inv_pool d new_size;
      assert( b.b_inv_end + 3 < Bigarray.Array1.dim d.ic_inv_data);
    );
    let zset_count = Zset.count zset in
    let zset_first = if zset_count = 0 then 0 else Zset.first zset in
    let zset_last = if zset_count = 0 then 0 else Zset.last zset in
    let ptr = b.b_idt_end lsl 3 in
    let i1 =
      Int64.logor
        (Int64.of_int ptr)
        (Int64.shift_left (Int64.of_int (zset_last land 0xffff)) 48) in
    let i2 =
      Int64.logor
        (Int64.of_int zset_count)
        (Int64.shift_left (Int64.of_int ((zset_last lsr 16) land 0xffff)) 48) in
    let i3 =
      Int64.logor
        (Int64.of_int zset_first)
        (Int64.shift_left (Int64.of_int ((zset_last lsr 32) land 0xffff)) 48) in
    let k_inv = b.b_inv_end in
    let k_idx = (k_inv lsr 2) - 1 in
    d.ic_inv_data.{ k_inv } <- Int64.of_int hash;
    d.ic_inv_data.{ k_inv + 1 } <- i1;
    d.ic_inv_data.{ k_inv + 2 } <- i2;
    d.ic_inv_data.{ k_inv + 3 } <- i3;
    b.b_inv_end <- k_inv + 4;
    (* Extend idt: *)
    let value_len = String.length value_enc in
    let set_len = Zset.byte_size zset in
    let idt_len1 =
      Zencoding.LenPre3.encode_length value_len in
    let idt_len2 =
      value_len  in
    let idt_len3 =
      Zencoding.LenPre3.encode_length set_len in
    let idt_len123 =
      idt_len1 + idt_len2 + idt_len3 in
    let idt_len4 =
      if idt_len123 land 0x7 = 0 then
        0
      else
        8 - (idt_len123 land 0x7) in
    let idt_len5 = set_len in
    let idt_total_len = idt_len123 + idt_len4 + idt_len5 in
    assert(idt_total_len land 7 = 0);
    let idt_total_wlen = idt_total_len lsr 3 in
    let k_idt = b.b_idt_end in
    let c_idt = k_idt lsl 3 in
    if k_idt + idt_total_wlen > Bigarray.Array1.dim d.ic_idt_data then (
      let new_size = 2 * (k_idt + idt_total_wlen) in
      P_idt.resize_entry ~assert_last:true b.b_idt_pool d new_size;
      assert(k_idt + idt_total_wlen <= Bigarray.Array1.dim d.ic_idt_data);
      assert(c_idt + idt_total_len <= Bigarray.Array1.dim d.ic_idt_char_data);
    );
    Zencoding.LenPre3.encode
      d.ic_idt_char_data c_idt idt_len1 value_len;
    let c_idt1 = c_idt + idt_len1 in
    Netsys_mem.blit_string_to_memory
      value_enc 0 d.ic_idt_char_data c_idt1 idt_len2;
    let c_idt2 = c_idt1 + idt_len2 in
    Zencoding.LenPre3.encode
      d.ic_idt_char_data c_idt2 idt_len3 set_len;
    let c_idt2 = c_idt2 + idt_len3 in
    Netsys_mem.blit_string_to_memory
      padding 0 d.ic_idt_char_data c_idt2 idt_len4;
    let c_idt3 = c_idt2 + idt_len4 in
    assert(c_idt3 land 7 = 0);
    let k_idt3 = c_idt3 lsr 3 in
    Zset.to_data d.ic_idt_data k_idt3 zset;
    b.b_idt_end <- k_idt + idt_total_wlen;
    (* Write idx: *)
    if not b.b_no_index then (
      match d.ic_sparse with
        | None ->
            let idx_data = d.ic_idx_data in
            Zset.iter
              (fun start len ->
                 (* TODO: Check that no ID is overwritten *)
                 set_idx_data (start-1) len k_idx idx_data
              )
              zset
        | Some _ ->
            Zset.iter
              (fun start len ->
                 for id = start to start+len-1 do
                   sparse_add_to_idx b id k_idx
                 done
              )
              zset
    )

  let build_add_1 b hash value zset =
    (* No checks here. The function is not exported *)
    b.b_last_hash <- hash;
    b.b_last_val <- Some value;
    b.b_count <- b.b_count + 1;
    let value_size = b.b_mb.mb_size value in
    let value_enc = Bytes.create value_size in
    b.b_mb.mb_encode value_enc 0 value;
    build_add_hv b hash value_enc zset
                               
  let build_add b value zset =
    if not b.b_open then
      failwith "Zcontainer.Zinvcol.build_add: already closed";
    let do_add = not (b.b_is_anon value) in
    if do_add then (
      let zo = b.b_order in
      let hash = zo.ord_hash value in
  (*
    let ZcolDescr(_,ty,_,_) = b.b_ic.ic_type in
    Printf.printf "build_add %s\n" (debug_string_of_value ty value);
   *)
      if b.b_count > 0 then (
        (* Printf.printf "  hash=%d last_hash=%d\n" hash b.b_last_hash; *)
        if hash < b.b_last_hash ||
           (hash = b.b_last_hash &&
              ( match b.b_last_val with
                  | None -> true
                  | Some lv -> zo.ord_cmp value lv <= 0
              )
           )
        then
          invalid_arg "Zcontainer.Zinvcol.build_add"
      );
      build_add_1 b hash value zset
    )


  let build_end b =
    if not b.b_open then
      failwith "Zcontainer.Zinvcol.build_end: already closed";
    b.b_open <- false;
    let d = b.b_ic.ic_data in
    P_inv.resize_entry ~assert_last:true b.b_inv_pool d b.b_inv_end;
    P_inv.truncate b.b_inv_pool;
    P_inv.close b.b_inv_pool;
    P_idt.resize_entry ~assert_last:true b.b_idt_pool d b.b_idt_end;
    P_idt.truncate b.b_idt_pool;
    P_idt.close b.b_idt_pool;
    (* TODO: check that all IDs are mapped! *)
    b.b_done b.b_ic;
    b.b_ic

  let build_abort b =
    if b.b_open then (
      b.b_open <- false;
      P_inv.close b.b_inv_pool;
      P_idt.close b.b_idt_pool;
      ( match b.b_fsname with
          | None -> ()
          | Some(dir,colname) -> remove ~force:true dir colname
      )
    )

  let from_file ?(no_index=false) ?(mem_suffix=false) dir ic_space ic_type =
    let Space sp = ic_space in
    let sparse_flag =
      match ic_type with
        | ZcolDescr(_,_,repr,zo) ->
            ( match repr with
                | SInv_multibyte _ -> true
                | _ -> false
            ) in
    let openfile full_name f =
      let fd =
        Unix.openfile
          full_name
          [ Unix.O_RDONLY; Unix.O_CLOEXEC ]
          0o666 in
      try
        f fd
      with
        | error ->
            Unix.close fd;
            raise error in
    let col_name =
      match ic_type with
        | ZcolDescr(n,_,_,_) -> n in
    let name = Filename.concat dir (Zutil.FileUtil.encode_name col_name) in
    let inv_suffix = if mem_suffix then ".invmem" else ".inv" in
    let idt_suffix = if mem_suffix then ".idtmem" else ".idt" in
    let idx_suffix = ".idx" in
    openfile
      (name ^ inv_suffix)
      (fun fd_inv ->
         openfile
           (name ^ idt_suffix)
           (fun fd_idt ->
              let openidx =
                if no_index then
                  (fun f -> f fd_idt)
                else
                  openfile (name ^ idx_suffix) in
              openidx
                (fun fd_idx ->
                   let ic_inv_data =
                     Bigarray.Array1.map_file
                       fd_inv Bigarray.int64 Bigarray.c_layout false (-1) in
                   let dim_inv =
                     Bigarray.Array1.dim ic_inv_data in
                   if dim_inv = 0 || dim_inv land 3 <> 0 then
                     failwith "Zcontainer.Zinvcol.from_file: format error (inv)";
                   let ic_idt_data =
                     Bigarray.Array1.map_file
                       fd_idt Bigarray.int Bigarray.c_layout false (-1) in
                   let ic_idt_char_data =
                     Netsys_mem.memory_of_bigarray_1 ic_idt_data in
                   let ic_idx_data, ic_sparse =
                     if no_index then
                       let kind = kind_of_space ic_space in
                       (create_idx_data kind 0, None)
                     else if sparse_flag then
                       let st_idx = Unix.fstat fd_idx in
                       let bits = kind_bits (kind_of_space ic_space) in
                       let factor = if bits >= 8 then bits/4 else 1 in
                       let n = Unix.(st_idx.st_size / factor) in
                       let kind = kind_ht_fixup(kind_of_space ic_space) in
                       map_idx_data fd_idx false kind (2*n), Some(0,n)
                     else
                       let kind = kind_of_space ic_space in
                       map_idx_data fd_idx false kind sp, None in
                   Unix.close fd_inv;
                   Unix.close fd_idt;
                   if not no_index then Unix.close fd_idx;
                   let ic_data =
                     { ic_inv_data; ic_idt_data; ic_idt_char_data; ic_idx_data;
                       ic_sparse
                     } in
                   { ic_space; ic_type; ic_data }
                )
           )
      )

  let array_length ic =
    Bigarray.Array1.dim ic.ic_data.ic_inv_data / 4 - 1

  let length ic =
    let is_sparse = ic.ic_data.ic_sparse <> None in
    let n = array_length ic in
    if is_sparse then n+1 else n

  let get_anon_hash_and_value ic =
    let ZcolDescr(_,_,repr,zo) = ic.ic_type in
    match repr with
      | SInv_multibyte(_,anon,_) -> (zo.ord_hash anon, anon)
      | _ -> assert false

  let get_hash ic k =
    let len = length ic in
    if k < 0 || k >= len then
      invalid_arg "Zcontainer.Zinvcol.get_hash";
    let is_sparse = ic.ic_data.ic_sparse <> None in
    if is_sparse && k = len-1 then
      fst(get_anon_hash_and_value ic)
    else
      Int64.to_int ic.ic_data.ic_inv_data.{ (k+1) lsl 2 }

  let b48_maxL =
    0xffff_ffff_ffffL

  let string_of_memory m =
    let n = Bigarray.Array1.dim m in
    let s = String.make n '\000' in
    Netsys_mem.blit_memory_to_string m 0 s 0 n;
    s

  let get_hash_and_encoded_value ic k =
    let len = array_length ic in
    if k < 0 || k >= len then
      invalid_arg "Zcontainer.Zinvcol.get_hash_and_value";
    let b =  (k+1) lsl 2 in
    let hash = Int64.to_int ic.ic_data.ic_inv_data.{b} in
    let ptr = Int64.to_int (Int64.logand ic.ic_data.ic_inv_data.{b+1} b48_maxL) in
    let idt = ic.ic_data.ic_idt_char_data in
    let idt_clen = Bigarray.Array1.dim idt in
    if ptr >= idt_clen then
      failwith "Zcontainer.Zinvcol: format error";
    let val_len_len = Zencoding.LenPre3.decode_length idt ptr in
    let val_len = Zencoding.LenPre3.decode idt ptr in
    if ptr + val_len_len + val_len > idt_clen then
      failwith "Zcontainer.Zinvcol: format error";
    let val_enc = string_of_memory
                    (Bigarray.Array1.sub idt (ptr + val_len_len) val_len) in
    (hash, val_enc)

  let get_hash_and_value ic k =
    let is_sparse = ic.ic_data.ic_sparse <> None in
    let is_anon =
      is_sparse && k = length ic - 1 in
    if is_anon then 
      get_anon_hash_and_value ic
    else
      let (hash, val_enc) = get_hash_and_encoded_value ic k in
      let mb =
        match ic.ic_type with
          | ZcolDescr(_,_,Inv_multibyte mb,_) -> mb
          | ZcolDescr(_,_,SInv_multibyte(mb,_,_),_) -> mb
          | _ -> assert false in
      (hash, mb.mb_decode val_enc 0 (Bytes.length val_enc))

  let get_value ic k =
    snd(get_hash_and_value ic k)


  let index_of_id_or_minus1 ic =
    (* returns -1 when there is no value *)
    match ic.ic_data.ic_sparse with
      | None ->
          fun id ->
            get_idx_data (id-1) ic.ic_data.ic_idx_data
      | Some(_,n) ->
          fun id ->
            let idx_data = ic.ic_data.ic_idx_data in
            let n2 = 2*n in
            let rec probe p =
              let k = get_idx_data p idx_data in
              if k = (-1) then
                k
              else if k = id then
                get_idx_data (p+1) idx_data
              else
                let p' = if p+2 >= n2 then 0 else p+2 in
                probe p' in
            probe (((ht_hash id) mod n) * 2)

  let index_of_id ic id =
    (* raises Not_found when there is no value *)
    let idx = index_of_id_or_minus1 ic id in
    if idx < 0 then raise Not_found;
    idx

  let value_of_id ic id =
    try
      let idx = index_of_id ic id in
      get_value ic idx
    with
      | Not_found as exn ->
          match ic.ic_type with
            | ZcolDescr(_,_,SInv_multibyte(_,anon,_),zo) ->
                anon
            | _ ->
                raise exn

  let value_array_of_zset ic zset =
    if not (Zset.space_less_or_equal_than zset ic.ic_space) then
      failwith "Zcontainer.Zinvcol.value_array_of_zset: \
                set exceeds space of column";
    if Zset.is_empty zset then
      [| |]
    else
      let x0 = value_of_id ic (Zset.first zset) in
      let va = Array.make (Zset.count zset) x0 in
      let k = ref 0 in
      let get = value_of_id ic in
      Zset.iter
        (fun start len ->
           for id = start to start+len-1 do
             va.( !k ) <- get id;
             incr k
           done
        )
        zset;
      va

  let repr_domain ic base pool =
    (* the representation domain: base ISECT the union of all zsets *)
    let repr_idx = index_of_id_or_minus1 ic in
    Zset.filter
      pool
      (fun id -> repr_idx id >= 0)
      base


  let nonanon_zset ic base pool =
    (* For sparse columns: the set of non-anonymous IDs.
       For other columns: the set of defined IDs
     *)
    match ic.ic_data.ic_sparse with
      | None ->
          repr_domain ic base pool
      | Some(_,n) ->
          let data = ic.ic_data.ic_idx_data in
          if Zset.count base < n/20 then (
            repr_domain ic base pool
          ) else (
            (* TODO: turn this into a Zset.unordered_builder functionality *)
            let bset = Zset.build_begin pool 100 ic.ic_space in
            let buf = Zutil.ArrayUtil.ext_create 100 0 in
            for k = 0 to n-1 do
              let id = get_idx_data (k lsl 1) data in
              if id >= 1 then
                Zutil.ArrayUtil.ext_add buf id
            done;
            let sub = Zutil.ArrayUtil.ext_contents buf in
            Array.sort (fun x y -> x-y) sub;
            let k = ref 0 in
            let len = Array.length sub in
            while !k < len do
              let start = sub.( !k ) in
              let num = ref 1 in
              incr k;
              while !k < len && sub.( !k ) = start + !num do
                incr k;
                incr num
              done;
              Zset.build_add_range bset start !num
            done;
            Zset.build_end bset
          )


  let anon_zset ic base pool =
    let dom = nonanon_zset ic base pool in
    Zset.diff pool base dom


  let get_zset_last ic k =
    if k < 0 || k >= array_length ic then
      invalid_arg "Zcontainer.Zinvcol.get_zset";
    let b =  (k+1) lsl 2 in
    let i1 = ic.ic_data.ic_inv_data.{b+1} in
    let i2 = ic.ic_data.ic_inv_data.{b+2} in
    let i3 = ic.ic_data.ic_inv_data.{b+3} in
    let last_1 = Int64.to_int (Int64.shift_right_logical i1 48) in
    let last_2 = Int64.to_int (Int64.shift_right_logical i2 48) in
    let last_3 = Int64.to_int (Int64.shift_right_logical i3 48) in
    let last = last_1 lor (last_2 lsl 16) lor (last_3 lsl 32) in
    last

  let is_anon ic k =
    let is_sparse = ic.ic_data.ic_sparse <> None in
    is_sparse && k = length ic - 1


  let get_zset ic k =
    if k < 0 || k >= length ic then
      invalid_arg "Zcontainer.Zinvcol.get_zset";
    let is_sparse = ic.ic_data.ic_sparse <> None in
    let is_anon =
      is_sparse && k = length ic - 1 in
    if is_anon then
      raise Anonymous
    else (
      let b =  (k+1) lsl 2 in
      let i1 = ic.ic_data.ic_inv_data.{b+1} in
      let i2 = ic.ic_data.ic_inv_data.{b+2} in
      let i3 = ic.ic_data.ic_inv_data.{b+3} in
      let ptr = Int64.to_int (Int64.logand i1 b48_maxL) in
      let count = Int64.to_int (Int64.logand i2 b48_maxL) in
      let first = Int64.to_int (Int64.logand i3 b48_maxL) in
      let last_1 = Int64.to_int (Int64.shift_right_logical i1 48) in
      let last_2 = Int64.to_int (Int64.shift_right_logical i2 48) in
      let last_3 = Int64.to_int (Int64.shift_right_logical i3 48) in
      let last = last_1 lor (last_2 lsl 16) lor (last_3 lsl 32) in
      let idt = ic.ic_data.ic_idt_char_data in
      let idt_clen = Bigarray.Array1.dim idt in
      if ptr >= idt_clen then
        failwith "Zcontainer.Zinvcol: format error";
      let val_len_len = Zencoding.LenPre3.decode_length idt ptr in
      let val_len = Zencoding.LenPre3.decode idt ptr in
      let ptr2 = ptr + val_len_len + val_len in
      if ptr2 > idt_clen then
        failwith "Zcontainer.Zinvcol: format error";
      let set_len_len = Zencoding.LenPre3.decode_length idt ptr2 in
      let set_len = Zencoding.LenPre3.decode idt ptr2 in
      let ptr3 = ptr2 + set_len_len in
      let padding = if ptr3 land 7 = 0 then 0 else 8 - (ptr3 land 7) in
      let ptr4 = ptr3 + padding in
      assert(ptr4 land 7 = 0);
      let ptr4w = ptr4 lsr 3 in
      let set_lenw = set_len lsr 3 in
      let idt = ic.ic_data.ic_idt_data in
      let idt_len = Bigarray.Array1.dim idt in
      if ptr4w + set_lenw > idt_len then
        failwith "Zcontainer.Zinvcol: format error";
      Zset.from_data idt ptr4w set_lenw ic.ic_space count first last
    )  

  let cmp_pair zo (h1, x1) (h2, x2) =
    if h1 < h2 then
      (-1)
    else
      if h1 > h2 then
        1
      else
        zo.ord_cmp x1 x2


  let find_1 : type s . ?hash:int -> ?first:int -> ?last:int ->
                    s zinvcol -> comparison -> s -> int =
    fun ?hash ?(first=0) ?last ic comp value ->
      let ZcolDescr(_,_,_,zo) = ic.ic_type in
      let hash =
        match hash with
          | Some h -> h
          | None -> zo.ord_hash value in
      let module HV = struct
        type t = int * s
        let compare hv1 hv2 =
          cmp_pair zo hv1 hv2
      end in
      let module OU = Zutil.OrdUtil(HV) in
      let get k =
        get_hash_and_value ic (k+first) in
      let len =
        match last with
          | None -> length ic - first
          | Some k -> k - first + 1 in
      first +
        OU.binary_search
          get
          len
          comp
          (hash,value)

  let find : type s . ?hash:int -> s zinvcol -> comparison -> s -> int =
    fun ?hash ic comp value ->
      find_1 ?hash ic comp value


  let cmp_pair_lfirst zo (h1, (l1,r1_opt)) (h2, (l2,r2_opt)) =
    (* (_,None) is here sorted before any (_,Some) *)
    if h1 < h2 then
      (-1)
    else
      if h1 > h2 then
        1
      else
        let dl = Pervasives.compare (l1:int) l2 in
        if dl <> 0 then
          dl
        else
          match r1_opt, r2_opt with
            | None, None -> 0
            | Some _, None -> 1
            | None, Some _ -> (-1)
            | Some x1, Some x2 ->
                zo.ord_cmp (l1,x1) (l2,x2)

  let cmp_pair_llast zo (h1, (l1,r1_opt)) (h2, (l2,r2_opt)) =
    (* (_,None) is here sorted after any (_,Some) *)
    if h1 < h2 then
      (-1)
    else
      if h1 > h2 then
        1
      else
        let dl = Pervasives.compare (l1:int) l2 in
        if dl <> 0 then
          dl
        else
          match r1_opt, r2_opt with
            | None, None -> 0
            | Some _, None -> (-1)
            | None, Some _ -> 1
            | Some x1, Some x2 ->
                zo.ord_cmp (l1,x1) (l2,x2)
                           
  let find_left_first : type s . ?hash:int -> (int*s) zinvcol -> int -> int =
    (* For a column of pairs find the first element where the left 
       component is equal to a value. Assumes standard ordering of pairs
       (Zrepr.pair_ord).
     *)
    fun ?hash ic lval ->
      let ZcolDescr(_,ty,_,zo) = ic.ic_type in
      let ty_r =
        match ty with
          | Zpair(_,ty_r) -> ty_r
          | _ -> assert false in
      let hash, shift =
        match hash with
          | Some h -> (h, 0)    (* assume Ord_pair case - CHECK *)
          | None ->
              ( match zo.ord_comp with
                  | Ord_pair ->
                      (* exploit here that only the left component goes into
                         the hash. See definition of pair_ord *)
                      (zo.ord_hash (lval,null_value ty_r), 0)
                  | Ord_bundle_pair ->
                      (* See definition of bundle_pair_ord *)
                      let h = zo.ord_hash (lval,null_value ty_r) in
                      (h, 48)
                  | _ ->
                      assert false
              ) in
      let module HV = struct
        type t = int * (int * s option)
        let compare hv1 hv2 =
          cmp_pair_lfirst zo hv1 hv2
      end in
      let module OU = Zutil.OrdUtil(HV) in
      let get k =
        let (h, (l,r)) = get_hash_and_value ic k in
        (h lsr shift, (l, Some r)) in
      let k =
        OU.binary_search
          get
          (length ic)
          GT
          (hash lsr shift,(lval,None)) in
      let (_, (l_k,_)) = get k in
      if l_k <> lval then raise Not_found;
      k

  let find_left_last : type s . ?hash:int -> (int*s) zinvcol -> int -> int =
    (* For a column of pairs find the last element where the left 
       component is equal to a value. Assumes standard ordering of pairs
       (Zrepr.pair_ord).
     *)
    fun ?hash ic lval ->
      let ZcolDescr(_,ty,_,zo) = ic.ic_type in
      let ty_r =
        match ty with
          | Zpair(_,ty_r) -> ty_r
          | _ -> assert false in
      let hash, shift =
        match hash with
          | Some h -> (h, 0)    (* assume Ord_pair case - CHECK *)
          | None ->
              ( match zo.ord_comp with
                  | Ord_pair ->
                      (* exploit here that only the left component goes into
                         the hash. See definition of pair_ord *)
                      (zo.ord_hash (lval,null_value ty_r), 0)
                  | Ord_bundle_pair ->
                      (* See definition of bundle_pair_ord *)
                      let h = zo.ord_hash (lval,null_value ty_r) in
                      (h, 48)
                  | _ ->
                      assert false
              ) in
      let module HV = struct
        type t = int * (int * s option)
        let compare hv1 hv2 =
          cmp_pair_llast zo hv1 hv2
      end in
      let module OU = Zutil.OrdUtil(HV) in
      let get k =
        let (h, (l,r)) = get_hash_and_value ic k in
        (h lsr shift, (l, Some r)) in
      let k =
        OU.binary_search
          get
          (length ic)
          LT
          (hash lsr shift,(lval,None)) in
      let (h_k, (l_k,_)) = get k in
      if l_k <> lval then raise Not_found;
      k
        
  let nonnone_zset ic base pool =
    (* The set of IDs where the value is defined and not "None" *)
    let ZcolDescr(_,_,repr,zo) = ic.ic_type in
    match find ic EQ None with
      | none_idx ->
          let get_idx = index_of_id_or_minus1 ic in
          ( match repr with
              | SInv_multibyte _ ->
                  (* None cannot be the anon element *)
                  Zset.filter
                    pool
                    (fun id ->
                       let idx = get_idx id in
                       idx <> none_idx
                    )
                    base
              | _ ->
                  Zset.filter
                    pool
                    (fun id ->
                       let idx = get_idx id in
                       idx >= 0 && idx <> none_idx
                    )
                    base
          )
      | exception Not_found ->
          (* For sparse columns None is often the anonymous value *)
          match repr with
            | SInv_multibyte(_,_,is_anon) ->
                if is_anon None then
                  nonanon_zset ic base pool
                else
                  Zset.all pool ic.ic_space
            | _ ->
                repr_domain ic base pool


  let dump3 conv_zset ic =
    (* testing *)
    let len = length ic in
    let l = ref [] in
    for k = len-1 downto 0 do
      let v = get_value ic k in
      let z = get_zset ic k in
      l := (v, conv_zset z) :: !l
    done;
    !l

  let dump ic = dump3 Zset.to_array ic
  let dump2 ic = dump3 Zset.dump ic


  let merge : type s .
                   ?clip_last_input:bool ->
                   ?mapval:(int -> (int * s) -> (int * s)) ->
                   ?mapset:((int * s) -> int list -> Zset.zset -> Zset.zset) ->
                   s zinvcol_builder -> (s zinvcol) array ->
                   int list array =
    fun ?(clip_last_input=false)
        ?(mapval = fun _ hv -> hv)
        ?(mapset = fun _ _ set -> set)
        b inputs ->
      let ZcolDescr(name,zt,zr,zo) = b.b_ic.ic_type in
      let module T = struct
        type t = int * s
        let compare ((h1,x1) : t) (h2,x2) = 
          if h1=h2 then
            zo.ord_cmp x1 x2
          else
            Pervasives.compare h1 h2
      end in
      let module TMap = Map.Make(T) in
      if b.b_count > 0 then
        failwith "Zcontainer.Zinvcol.merge: builder is not empty";
      let pool = Zset.create_pool 1000 1000 in
      let n = Array.length inputs in
      let Space out_sp = b.b_ic.ic_space in
      let offsets =
        Array.map (fun _ -> 0) inputs in
      Array.iteri
        (fun i input ->
           if not (Ztypes.same_descr b.b_ic.ic_type input.ic_type) then (
             eprintf "[DEBUG] type(b) = %s\n" (Ztypes.debug_string_of_descr b.b_ic.ic_type);
             eprintf "[DEBUG] type(input[%d]) = %s\n%!" i (Ztypes.debug_string_of_descr input.ic_type);
             failwith "Zcontainer.Zinvcol.merge: incompatible types";
           );
           let Space sp = input.ic_space in
           if i < Array.length inputs - 1 then
             offsets.(i+1) <- offsets.(i) + sp
        )
        inputs;
      let all_last_input =
        if clip_last_input then (
          let Space sp_last = inputs.(n-1).ic_space in
          let sp_clipped = Space(min sp_last (out_sp - offsets.(n-1))) in
          Zset.all pool sp_clipped
        )
        else Zset.empty in
      let lengths = Array.map (fun input -> array_length input) inputs in
      let current = Array.map (fun _ -> 0) inputs in
      let indexes = Array.mapi (fun i _ -> i) inputs in
      let m0 =
        Array.fold_left
          (fun acc i ->
             let inp = inputs.(i) in
             let len = lengths.(i) in
             let cur = current.(i) in
             if cur < len then
               let (h,v) = get_hash_and_value inp cur in
               let (h,v) = mapval i (h,v) in
               let l = try TMap.find (h,v) acc with Not_found -> [] in
               TMap.add (h,v) (i :: l) acc
             else
               acc
          )
          TMap.empty
          indexes in
      let m = ref m0 in
      let out = ref [] in
      while !m <> TMap.empty do
        let ((h,v),l) = TMap.min_binding !m in
        out := l :: !out;
        let zset =
          List.fold_left
            (fun acc i ->
               let inp = inputs.(i) in
               let cur = current.(i) in
               let ofs = offsets.(i) in
               let zi_1 = get_zset inp cur in
               let zi_2 =
                 if clip_last_input && i=n-1 then
                   Zset.isect pool zi_1 all_last_input
                 else
                   zi_1 in
               let zi_3 = Zset.shift_space pool b.b_ic.ic_space zi_2 ofs in
               Zset.union pool acc zi_3
            )
            Zset.empty
            l in
        let zset = mapset (h,v) l zset in
        build_add b v zset;
        m := TMap.remove (h,v) !m;
        List.iter
          (fun i ->
             let inp = inputs.(i) in
             let len = lengths.(i) in
             let cur = current.(i) + 1 in
             current.(i) <- cur;
             if cur < len then (
               let (h,v) = get_hash_and_value inp cur in
               let (h,v) = mapval i (h,v) in
               let l1 = try TMap.find (h,v) !m with Not_found -> [] in
               m := TMap.add (h,v) (i :: l1) !m
             )
          )
          l
      done;
      Array.iter
        (fun i ->
           let len = lengths.(i) in
           let cur = current.(i) in
           assert(cur = len)
        )
        indexes;
      Array.of_list (List.rev !out)


  let iter_all ?(first=0) ?last col : _ ziterator =
    let ZcolDescr(_,_,_,zo) = col.ic_type in
    let len = length col in
    let cur = ref first in
    let endpos =
      match last with
        | None -> len
        | Some n -> n + 1 in
    let anon_zset_lz =
      lazy
        ( let pool = Zset.create_pool 100 1000 in
          let all = Zset.all pool col.ic_space in
          anon_zset col all pool
        ) in
    ( object
        method current_pos =
          !cur - first
        method current_value =
          if !cur >= endpos then raise End_of_column;
          get_hash_and_value col !cur
        method current_value_lz =
          let cur = !cur in
          lazy 
            ( if cur >= endpos then raise End_of_column;
              get_hash_and_value col cur
            )
        method current_zset pool =
          if !cur >= endpos then raise End_of_column;
          try
            get_zset col !cur
          with
            | Anonymous ->
                Zset.copy pool (Lazy.force anon_zset_lz)
        method current_zset_lz pool =
          let cur = !cur in
          lazy
            ( if cur >= endpos then raise End_of_column;
              try
                get_zset col cur
              with
                | Anonymous ->
                    Zset.copy pool (Lazy.force anon_zset_lz)
            )
        method beyond =
          !cur < first || !cur >= endpos
        method next() =
          incr cur
        method prev() =
          decr cur
        method go_beginning() =
          cur := first
        method go_end() =
          cur := endpos
        method go k =
          cur := (k + first)
        method find comp x =
          let pos = find_1 ~first ~last:(endpos-1) col comp x in
          cur := pos
        method length =
          endpos - first
      end
    )


  let indices_of_zset col bases =
    (* TODO: the choice of data structure for eliminating duplicates is not
       really apparent. In particular, when [base] is very small, a hash table
       might be better (consumes less RAM).

       On the other hand, the bitset is good when most of the indices are
       actually selected (however, the [indices] array is a bad choice then).

       Idea: start with a hash table. When the number of elements grows beyond
       a number (say, len/128) switch to a bitset. Return either an array or
       the bitset.
     *)
    let collen = length col in
    let bitset = Bitset.create collen false in
    let indices = ref [] in
    List.iter
      (fun base ->
         Zset.iter
           (fun start len ->
              for j = 0 to len-1 do
                let id = start+j in
                let idx = index_of_id col id in
                assert(idx < collen);
                if not (Bitset.get bitset idx) then (
                  indices := idx :: !indices;
                  Bitset.set_true bitset idx
                )
              done
           )
           base
      )
      bases;
    let indices = Array.of_list !indices in
    Array.sort Pervasives.compare indices;
    indices


  (* TODO: another option is to gather the sets in the initial Zset loop,
     by putting the ID into the bucket for idx
   *)

  let iter_1 : type s . s zinvcol -> Zset.zset list -> _ -> s ziterator =
    fun col bases all_values_flag ->
      let anon_zset_lz =
        lazy
          ( let pool = Zset.create_pool 100 1000 in
            let all = Zset.all pool col.ic_space in
            anon_zset col all pool
          ) in
      let ZcolDescr(n,_,_,zo) = col.ic_type in
      let is_sparse = col.ic_data.ic_sparse <> None in
      let module HV = struct
        type t = int * s
        let compare hv1 hv2 =
          cmp_pair zo hv1 hv2
      end in
      let module OU = Zutil.OrdUtil(HV) in
      let indices =
        if all_values_flag then
          let n = length col - (if is_sparse then 1 else 0) in
          Array.init n (fun k -> k)
        else
          indices_of_zset col bases in
      (* For sparse columns, [indices] doesn't include the anon element *)
      let ilen = Array.length indices in
      let icur = ref 0 in

      let zset_at pool icur =
        if icur >= ilen then (
          if is_sparse && icur = ilen then
            Zset.copy pool (Lazy.force anon_zset_lz)
          else
            raise End_of_column;
        ) else
          let z = get_zset col indices.( icur ) in
          let l = List.map (Zset.isect pool z) bases in
          Zset.union_many pool l in
      ( object
          method current_pos =
            !icur
          method current_value =
            if !icur >= ilen then (
              if is_sparse && !icur = ilen then
                get_anon_hash_and_value col
              else
                raise End_of_column
            ) else
              get_hash_and_value col indices.( !icur )
          method current_value_lz =
            let icur = !icur in
            lazy
              ( if icur >= ilen then (
                  if is_sparse && icur = ilen then
                    get_anon_hash_and_value col
                  else
                    raise End_of_column
                ) else
                  get_hash_and_value col indices.( icur )
              )
          method current_zset pool =
            zset_at pool !icur
          method current_zset_lz pool =
            let icur = !icur in
            lazy (zset_at pool icur)
          method beyond =
            !icur < 0 || !icur >= ilen+1 || (!icur = ilen && not is_sparse)
          method next() =
            incr icur
          method prev() =
            decr icur
          method go_beginning() =
            icur := 0
          method go_end() =
            if is_sparse then icur := ilen else icur := ilen-1
          method go k =
            icur := k
          method find comp x =
            let tlen =
              if is_sparse then ilen+1 else ilen in
            icur :=
              OU.binary_search
                (fun p ->
                   if p < ilen then
                     let idx = indices.(p) in get_hash_and_value col idx
                   else
                     get_anon_hash_and_value col
                )
                tlen
                comp
                (zo.ord_hash x, x)
          method length =
            if is_sparse then ilen+1 else ilen
        end
      )

  let iter ?(all_values=false) col base =
    if Zset.is_all base then
      iter_all col
    else
      iter_1 col [base] all_values


  let iter_union ?(all_values=false) col bases =
    match bases with
      | [base] -> iter ~all_values col base
      | _ -> iter_1 col bases all_values


  let build_from_iterator b pool itr =
    try
      while not itr#beyond do
        let (h,v) = itr#current_value in
        let zset = itr#current_zset pool in
        build_add b v zset;
        itr # next();
      done;
      build_end b
    with
      | error ->
          build_abort b;
          raise error
end

module L = struct
  (* FIXME: we are assuming here that lists are sorted lexicographically.
     This is exploited in a number of ways:
       - unlist_ord construction
       - assuming that the passed list ordering is isomorphic to the ordering
         of a standard "int list"

     It is not even checked that the lexicograpic ordering is used (how?).
   *)

  type state =
    | Closed
    | Open
    | Adding
    | Merging

  type 'a zinvcol_builder =
    { mutable b_state : state;
      b_list : int list N.zinvcol_builder;
      b_mem : 'a N.zinvcol_builder;
      b_accu_list : ('a word list * Zset.zset) Queue.t;
      b_accu_mem : ('a, 'a word) Hashtbl.t;
      b_wordcount : int ref;
      b_descr : 'a list zcoldescr;
      b_order : 'a list zorder;
      b_bundle : bool;
      mutable b_done : 'a zinvcol -> unit;
      mutable b_count : int;
      mutable b_last_hash : int;
      mutable b_last_val : 'a list option;
      mutable b_bundle_indices : (int, unit) Hashtbl.t
    }

   and 'a word =
     { mutable w_index : int;
       w_data : 'a;
       mutable w_sets : Zset.zset list;
     }

  and 'a zinvcol =
    { col_list : int list N.zinvcol;
      col_mem : 'a N.zinvcol;
      col_descr : 'a list zcoldescr;
    }
       
  let unlist_mb mb =
    { mb_name = mb.mb_name ^ " unlist";
      mb_size = (fun x -> mb.mb_size [x]);
      mb_encode = (fun b pos x -> mb.mb_encode b pos [x]);
      mb_decode = (fun b pos len ->
                     match mb.mb_decode b pos len with
                       | [x] -> x
                       | _ -> failwith "Zcontainer.Zinvcol.L.unlist_mb"
                  );
    }

  let space ic =
    N.space ic.col_list

  let descr ic =
    ic.col_descr

  let unlist_repr =
    function
    | Inv_multibyte mb ->
        Inv_multibyte (unlist_mb mb)
    | SInv_multibyte _ ->
        failwith "Zcontainer.Zinvcol: \
                  sparse columns not supported for list type"
    | _ ->
        failwith "Zcontainer.Zinvcol: bad representation"


  let unlist_ord : type s . s ztype -> s list zorder -> s zorder =
    fun expected_ty ord ->
      match ord.ord_sub with
        | [| ZorderBox(inner_ty, inner_ord) |] ->
            ( match same_ztype inner_ty expected_ty with
                | Equal ->
                    inner_ord
                | Not_equal ->
                    assert false
            )
        | _ ->
            assert false

  let create_memory_zinvcol_builder
    : type s . _ -> _ -> _ -> s list zcoldescr -> _ -> s zinvcol_builder =
    fun inv_size idt_size space coltype bundle ->
      let ZcolDescr(name,ty,repr,ord) = coltype in
      let mem_ty =
        match ty with
          | Zlist ty1 -> ty1
          | _ -> assert false in
      let list_repr =
        Inv_multibyte (Zrepr.list_mb Zint Zrepr.int_mb) in
      let list_ord =
        (* Ztypes.list_ord Zint Zrepr.int_asc in *)
        Ztypes.nat_list_ord in
      let list_coltype =
        ZcolDescr(name, Zlist Zint, list_repr, list_ord) in
      let mem_coltype =
        ZcolDescr(name, mem_ty, unlist_repr repr, unlist_ord mem_ty ord) in
      let b_list =
        N.create_memory_zinvcol_builder inv_size idt_size space list_coltype in
      let b_mem =
        (* CHECK: same values for inv_size, idt_size? *)
        N.create_memory_zinvcol_builder
          ~no_index:true inv_size idt_size space mem_coltype in
      { b_state = Open;
        b_list;
        b_mem;
        b_accu_list = Queue.create();
        b_accu_mem = Hashtbl.create 137;
        b_wordcount = ref 0;
        b_order = ord;
        b_descr = coltype;
        b_bundle = bundle;
        b_done = (fun _ -> ());
        b_count = 0;
        b_last_hash = 0;
        b_last_val = None;
        b_bundle_indices = Hashtbl.create 13;
      }

  let create_file_zinvcol_builder
    : type s . _ -> _ -> _ -> _ -> s list zcoldescr -> _ -> s zinvcol_builder =
    fun dir inv_size idt_size space coltype bundle ->
      let ZcolDescr(name,ty,repr,ord) = coltype in
      let mem_ty =
        match ty with
          | Zlist ty1 -> ty1
          | _ -> assert false in
      let list_repr =
        Inv_multibyte (Zrepr.list_mb Zint Zrepr.int_mb) in
      let list_ord =
        Ztypes.nat_list_ord in
      let list_coltype =
        ZcolDescr(name, Zlist Zint, list_repr, list_ord) in
      let mem_coltype =
        ZcolDescr(name, mem_ty, unlist_repr repr, unlist_ord mem_ty ord) in
      let b_list =
        N.create_file_zinvcol_builder
          dir inv_size idt_size space list_coltype in
      let b_mem =
        (* CHECK: same values for inv_size, idt_size? *)
        N.create_file_zinvcol_builder
          ~no_index:true ~mem_suffix:true
          dir inv_size idt_size space mem_coltype in
      { b_state = Open;
        b_list;
        b_mem;
        b_accu_list = Queue.create();
        b_accu_mem = Hashtbl.create 137;
        b_wordcount = ref 0;
        b_order = ord;
        b_descr = coltype;
        b_bundle = bundle;
        b_done = (fun _ -> ());
        b_count = 0;
        b_last_hash = 0;
        b_last_val = None;
        b_bundle_indices = Hashtbl.create 13;
      }

  let build_add_check_bundle_value : type s . s list zcoldescr ->
                                          (int,unit) Hashtbl.t ->
                                           s list ->
                                          unit =
    fun coltype bundle_indices value ->
    (* Additional checks for bundles:
        - reject any words for default values
        - reject lists not sorted by left component (bundle index)
        - reject lists with duplicate words
       Also:
        - record all occurring bundle indices
     *)
    match coltype with
      | ZcolDescr(_, Zlist(Zpair(Zint,elemtype)), _, _) ->
          let default = null_value elemtype in
          let rec check first last_idx value =
            match value with
              | (idx, bval) :: value' ->
                  let ok =
                    (first || idx > last_idx) &&
                      bval <> default in
                  if not ok then
                    failwith "Zcontainer.Zinvcol.build_add: bad bundle value";
                  if not(Hashtbl.mem bundle_indices idx) then
                    Hashtbl.add bundle_indices idx ();
                  check false idx value'
              | [] ->
                  ()
          in
          check true 0 value
      | _ ->
          assert false

  let build_add b value zset =
    ( match b.b_state with
        | Closed ->
            failwith "Zcontainer.Zinvcol.build_add: already closed"
        | Merging ->
            failwith "Zcontainer.Zinvcol.build_add: cannot add after merge"
        | Open ->
            b.b_state <- Adding
        | Adding ->
            ()
    );
    let zo = b.b_order in
    let hash = zo.ord_hash value in
    if b.b_count > 0 then (
      if hash < b.b_last_hash ||
           (hash = b.b_last_hash &&
              ( match b.b_last_val with
                  | None -> true
                  | Some lv -> zo.ord_cmp value lv <= 0
              )
           )
      then
        invalid_arg "Zcontainer.Zinvcol.build_add"
    );
    if b.b_bundle then
      build_add_check_bundle_value b.b_descr b.b_bundle_indices value;
    let words_ht = Hashtbl.create 3 in
    let words =
      List.map
        (fun w_data ->
           let word =
             try Hashtbl.find b.b_accu_mem w_data
             with Not_found ->
               let w_index = !(b.b_wordcount) in
               incr b.b_wordcount;
               let word = {w_index; w_data; w_sets=[]} in
               Hashtbl.add b.b_accu_mem w_data word;
               word in
           Hashtbl.replace words_ht word.w_index word;
           word
        )
        value in
    Hashtbl.iter
      (fun _ word ->
         word.w_sets <- zset :: word.w_sets
      )
      words_ht;
    Queue.add (words, zset) b.b_accu_list;
    b.b_count <- b.b_count + 1;
    b.b_last_hash <- hash;
    b.b_last_val <- Some value

  let build_end_bundle_fixup : type s. s zinvcol_builder -> _ -> unit =
    fun b pool ->
      (* Only bundles
          - fix up the defaults. Iterate over the bundle indices, and
            sum up the set of rows where the bundle element is defined.
            Default = all - defined. Unless the default is empty, add
            it to b_accu_mem.
       *)
      match b.b_descr with
        | ZcolDescr(_, Zlist(Zpair(Zint,elemtype)), _, _) ->
            let default = null_value elemtype in
            let all = Zset.all pool N.(space b.b_mem.b_ic) in
            Hashtbl.iter
              (fun idx _ ->
                let defined_l =
                  Hashtbl.fold
                    (fun (i,_) word acc ->
                      if i=idx then (
                        let set = Zset.union_many pool word.w_sets in
                        word.w_sets <- [set];
                        set :: acc
                      ) else
                        acc
                    )
                    b.b_accu_mem
                    [] in
                let defined_s = Zset.union_many pool defined_l in
                let default_s = Zset.diff pool all defined_s in
                if not (Zset.is_empty default_s) then (
                  let word =
                    { w_index = 0;   (* will be overwritten anyway *)
                      w_data = (idx,default);
                      w_sets = [default_s]
                    } in
                  Hashtbl.add b.b_accu_mem word.w_data word;
                )
              )
              b.b_bundle_indices
        | _ ->
            assert false

  let build_end b =
    ( match b.b_state with
        | Closed ->
            failwith "Zcontainer.Zinvcol.build_end: already closed"
        | _ ->
            ()
    );
    let st = b.b_state in
    b.b_state <- Closed;
    try
      if st = Adding then (
        let ZcolDescr(_, _, _, mem_ord) = N.(b.b_mem.b_ic.ic_type) in
        let nwords = Hashtbl.length b.b_accu_mem in
        let pool =
          Zset.create_pool ~enctype:Zset.Packed_enc (10*nwords) nwords in
        if b.b_bundle then
          build_end_bundle_fixup b pool;
        let words =
          Hashtbl.fold (fun _ word acc -> word::acc) b.b_accu_mem [] in
        let words = Array.of_list words in
        let words = Array.map (fun w -> (mem_ord.ord_hash w.w_data, w)) words in
        Array.sort
          (fun (h1,w1) (h2,w2) ->
            if h1=h2 then
              mem_ord.ord_cmp w1.w_data w2.w_data
            else
              h1-h2
          )
          words;
        Array.iteri
          (fun i (_,w) -> w.w_index <- i)
          words;
        Array.iter
          (fun (_,w) ->
             let set = Zset.union_many pool w.w_sets in
             N.build_add b.b_mem w.w_data set
          )
          words;
        let ord_int_list = N.(b.b_list.b_order) in
        Queue.iter
          (fun (words, set) ->
           let addr = List.map (fun w -> w.w_index) words in
           let hash = ord_int_list.ord_hash addr in
           N.build_add_1 b.b_list hash addr set
              (* This build_add needs to be done in the order configured for
                 b_list which is the standard ordering for "int list". The queue
                 is filled, however, in the configured order for "t list".
                 Both orderings are isomorphic, because we are assigning these
                 integers in the order configured for the members t.
               *)
          )
          b.b_accu_list;
      );
      let col_mem = N.build_end b.b_mem in
      let col_list = N.build_end b.b_list in
      let col_descr = b.b_descr in
      let col = { col_mem; col_list; col_descr } in
      b.b_done col;
      col
    with error ->
      N.build_abort b.b_mem;
      N.build_abort b.b_list;
      raise error

  let build_abort b =
    b.b_state <- Closed;
    N.build_abort b.b_mem;
    N.build_abort b.b_list

  let build_from_iterator b pool itr =
    try
      while not itr#beyond do
        let (h,v) = itr#current_value in
        let zset = itr#current_zset pool in
        build_add b v zset;
        itr # next();
      done;
      build_end b
    with
      | error ->
          build_abort b;
          raise error
                  
  let set_b_done b f =
    b.b_done <- f

  let merge_bundle_mapset : type s .
                                 s zinvcol_builder -> (s zinvcol) array ->
                                 (int * s) -> _ -> _ -> _ =
    fun b inputs ->
      (* For bundles, there is a difficulty when we merge b_mem and one
         of the bundle indices does not occur at all in one input. In that
         case, we need to assume that this index has the default value
         everywhere.
       *)
      let out_space = N.(b.b_mem.b_ic.ic_space) in
      let Space out_sp = out_space in
      let pool = Zset.create_pool 10 100 in
      let n = Array.length inputs in
      let all_inputs =
        Array.fold_left
          (fun acc i -> IntSet.add i acc)
          IntSet.empty
          (Array.mapi (fun i _ -> i) inputs) in
      let offsets =
        Array.map (fun _ -> 0) inputs in
      Array.iteri
        (fun i input ->
           let Space sp = N.(input.col_mem.ic_space) in
           if i < Array.length inputs - 1 then
             offsets.(i+1) <- offsets.(i) + sp
        )
        inputs;
      let all_sets =
        Array.mapi
          (fun i ofs ->
            let Space sp_i = N.(inputs.(i).col_mem.ic_space) in
            let sp =
              if i=n-1 then
                Space (min sp_i (out_sp - ofs))
              else
                Space sp_i in
            let a = Zset.all pool sp in
            Zset.shift_space pool out_space a ofs
          )
          offsets in
      match N.(b.b_mem.b_ic.ic_type) with
        | ZcolDescr(_, Zpair(Zint,elemty), _, _) ->
            let default = null_value elemty in
            (fun (h,(_,v)) covered set ->
              if v = default && List.length covered < n then
                let uncovered_inputs =
                  List.fold_left
                    (fun acc i -> IntSet.remove i acc)
                    all_inputs
                    covered in
                let uncovered_sets =
                  IntSet.fold
                    (fun i acc ->
                      all_sets.(i) :: acc
                    )
                    uncovered_inputs
                    [] in
                (* TODO: it might be worth to cache this, in particular for
                   small n
                 *)
                Zset.union_many pool (set :: uncovered_sets)
              else
                set
            )
        | _ ->
            assert false


  let merge b inputs =
    ( match b.b_state with
        | Closed ->
            failwith "Zcontainer.zinvcol.merge: already closed"
        | Merging ->
            failwith "Zcontainer.Zinvcol.merge: cannot merge twice"
        | Open ->
            b.b_state <- Merging
        | Adding ->
            failwith "Zcontainer.Zinvcol.merge: cannot merge after build_add"
    );
    let mapset =
      if b.b_bundle then
        merge_bundle_mapset b inputs
      else
        (fun _ _ set -> set) in
    let merge_map =
      N.merge ~clip_last_input:b.b_bundle ~mapset
              b.b_mem (Array.map (fun ic -> ic.col_mem) inputs) in
    let key_offsets =
      Array.map
        (fun inp ->
           Array.make (N.length inp.col_mem) 0
        )
        inputs in
    let key_curofs = Array.map (fun _ -> 0) inputs in
    Array.iteri
      (fun k which_inputs ->
         List.iter
           (fun i ->
              let j = key_curofs.(i) in
              key_curofs.(i) <- j+1;
              key_offsets.(i).(j) <- k
           )
           which_inputs
      )
      merge_map;
    let ZcolDescr(_,_,_,ord_int_list) = N.(b.b_list.b_ic.ic_type) in
    let mapval i (h,int_list) =
      let int_list' =
        List.map
          (fun k -> key_offsets.(i).(k))
          int_list in
      let h' =
        ord_int_list.ord_hash int_list' in
      (h',int_list') in
    ignore(N.merge ~mapval b.b_list (Array.map (fun ic -> ic.col_list) inputs))

  let from_file dir space coltype =
    let ZcolDescr(name,ty,repr,ord) = coltype in
    let mem_ty =
      match ty with
        | Zlist ty1 -> ty1
        | _ -> assert false in
    let list_repr =
      Inv_multibyte (Zrepr.list_mb Zint Zrepr.int_mb) in
    let list_ord =
      Zrepr.list_ord Zint Zrepr.int_asc in
    let list_coltype =
      ZcolDescr(name, Zlist Zint, list_repr, list_ord) in
    let mem_coltype =
      ZcolDescr(name, mem_ty, unlist_repr repr, unlist_ord mem_ty ord) in
    let col_list =
      N.from_file dir space list_coltype in
    let col_mem =
      N.from_file ~no_index:true ~mem_suffix:true dir space mem_coltype in
    { col_list; col_mem; col_descr=coltype }
          
                  
  let length ic = N.length ic.col_list

  let decode_members ic int_list =
    (* eprintf "decode_members int_list=%s\n%!" (String.concat "," (List.map string_of_int int_list)); *)
    try
      List.map
        (fun k ->
           let _, x = N.get_hash_and_value ic.col_mem k in
           x
        )
        int_list
    with Not_found -> assert false

  let rec encode_members ic comp mem_list =
    (* Translates the member values to positions in col_mem. It is possible
       that member values do not occur in col_mem. The behavior depends then
       on comp:
        - comp=EQ: raise Not_found
        - comp=LE: return the next lower value instead, and stop the list
          translation.
        - comp=GE: return the next higher value instead, and stop the list
          translation.

       comp=LT behaves like comp=LE, and GT like GE. NE is not supported.

       Returns (int_list, precise). The precice bool is true when all members
       could be translated.
     *)
    match mem_list with
      | [] ->
          ([], true)
      | x :: l ->
          ( match
              N.find ic.col_mem EQ x
            with
              | k ->
                  let l', precise = encode_members ic comp l in
                  (k :: l', precise)
              | exception Not_found when comp=LE || comp=LT ->
                  ( try
                      ([ N.find ic.col_mem LT x;
                         N.length ic.col_mem
                       ], false)
                    with
                      | Not_found -> ([], false)
                  )
              | exception Not_found when comp=GE || comp=GT ->
                  ( try
                      ([ N.find ic.col_mem GT x ], false)
                    with
                      | Not_found -> ([ N.length ic.col_mem ], false)
                  )
          )

  let encode_existing ic mem_list =
    try fst(encode_members ic EQ mem_list)
    with Not_found -> assert false
            
  let get_value ic k =
    let int_list = N.get_value ic.col_list k in
    decode_members ic int_list

  let hash ic x =
    let ZcolDescr(_,_,_,ord) = ic.col_descr in
    ord.ord_hash x
                 
  let get_hash_and_value ic k =
    let x = get_value ic k in
    (hash ic x, x)

  let get_hash ic k =
    fst(get_hash_and_value ic k)

  let get_zset ic k =
    N.get_zset ic.col_list k

  let nonanon_zset ic base pool =
    N.nonanon_zset ic.col_list base pool

  let anon_zset ic base pool =
    N.anon_zset ic.col_list base pool

  let is_anon ic k =
    false  (* sparse columns not supported *)

  let reduce_find_to f_find f_any ic comp value =
    match comp with
      | EQ ->
          f_find EQ (fst (encode_members ic EQ value))
      | LE ->
          let int_list, precise = encode_members ic LE value in
          f_find LE int_list
      | LT ->
          let int_list, precise = encode_members ic LE value in
          f_find (if precise then LT else LE) int_list
      | GE ->
          let int_list, precise = encode_members ic GE value in
          f_find GE int_list
      | GT ->
          let int_list, precise = encode_members ic GE value in
          f_find (if precise then GT else GE) int_list
      | NE ->
          ( match encode_members ic EQ value with
              | int_list, _ ->
                  f_find NE int_list
              | exception Not_found ->
                  f_any()
          )

  let find ic comp value =
    let f_any() =
      if N.length ic.col_list > 0 then 0 else raise Not_found in
    reduce_find_to (N.find ic.col_list) f_any ic comp value
            
  let index_of_id ic id =
    N.index_of_id ic.col_list id

  let value_of_id ic id =
    decode_members ic (N.value_of_id ic.col_list id)

  let value_array_of_zset ic zset =
    let array = N.value_array_of_zset ic.col_list zset in
    Array.map (decode_members ic) array

  let dump3 conv_zset ic =
    let l = N.dump3 conv_zset ic.col_list in
    List.map (fun (v, x) -> (decode_members ic v, x)) l

  let dump ic = dump3 Zset.to_array ic
  let dump2 ic = dump3 Zset.dump ic

  let iterator (ic : 'a zinvcol) (itr : int list ziterator) : 'a list ziterator =
    object(self)
      method current_pos =
        itr # current_pos
      method current_value =
        let _, int_list = itr # current_value in
        let mem_list = decode_members ic int_list in
        (hash ic mem_list, mem_list)
      method current_value_lz =
        let pair_lz = itr # current_value_lz in
        lazy(
            let _, int_list = Lazy.force pair_lz in
            let mem_list = decode_members ic int_list in
            (hash ic mem_list, mem_list)
          )
      method current_zset =
        itr # current_zset
      method current_zset_lz =
        itr # current_zset_lz
      method beyond =
        itr # beyond
      method next =
        itr # next
      method prev =
        itr # prev
      method go_beginning =
        itr # go_beginning
      method go_end =
        itr # go_end
      method go =
        itr # go
      method find cmp value =
        reduce_find_to
          itr#find
          (fun () -> self # go_beginning())
          ic cmp value
      method length =
        itr # length
    end

  let iter_all ic =
    iterator ic (N.iter_all ic.col_list)

  let iter ?all_values ic base =
    iterator ic (N.iter ?all_values ic.col_list base)

  let iter_union ?all_values ic bases =
    iterator ic (N.iter_union ?all_values ic.col_list bases)

  let iter_mem_all ic =
    N.iter_all ic.col_mem

  let zorder_bundle : type t . (int * t) zinvcol -> t ztype * t zorder =
    fun ic ->
      let ZcolDescr(_,ty_mem,_,ord_mem) = N.descr ic.col_mem in
      let ty =
        match ty_mem with
          | Zpair(_,ty_r) -> ty_r
          | _ -> assert false in
      let ord =
        match ord_mem.ord_sub with
          | [| _; ZorderBox(ty_r, ord_right) |] ->
              ( match same_ztype ty ty_r with
                  | Equal -> (ord_right : t zorder)
                  | Not_equal -> assert false
              )
          | _ -> assert false in
      ty, ord

  let iter_bundle_element : type t. (int * t) zinvcol -> int ->
                                 t ziterator =
    fun ic lval ->
      try
        let first = N.find_left_first ic.col_mem lval in (* or Not_found *)
        let last = N.find_left_last ic.col_mem lval in (* or Not_found *)
        let itr = N.iter_all ~first ~last ic.col_mem in
        let _, ord = zorder_bundle ic in
        ( object(self)
            method current_pos = itr#current_pos
            method current_value =
              let _, (_, r) = itr # current_value in
              (ord.ord_hash r, r)
            method current_value_lz =
              let pair_lz = itr # current_value_lz in
              lazy(
                  let _, (_, r) = Lazy.force pair_lz in
                  (ord.ord_hash r, r)
                )
            method current_zset = itr#current_zset
            method current_zset_lz = itr#current_zset_lz
            method beyond = itr#beyond
            method next = itr#next
            method prev = itr#prev
            method go_beginning = itr#go_beginning
            method go_end = itr#go_end
            method go = itr#go
            method find cmp x = itr#find cmp (lval,x)
            method length = itr#length
          end
        )
      with
        | Not_found -> iter_empty()

  let indices ic =
    let l = ref [] in
    let itr = N.iter_all ic.col_mem in
    while not itr#beyond do
      let _, (idx,_) = itr#current_value in
      ( match !l with
          | [] -> l := [idx]
          | oldidx :: _ -> if idx <> oldidx then l := idx :: !l
      );
      itr # next();
    done;
    List.rev !l

  let storage_size ic =
    N.storage_size ic.col_list + N.storage_size ic.col_mem

  end

module B = struct
  (* View on bundle elements *)

  type 'a zinvcol =
    { element : int;
      bundle : (int * 'a) L.zinvcol;
      itr : 'a ziterator;
      default : 'a;
      ord : 'a zorder;
      ztype : 'a ztype;
      pool : Zset.pool;
    }

  let select : type s . (int * s) L.zinvcol -> int -> s zinvcol =
    fun ic element ->
      let ty, ord = L.zorder_bundle ic in
      let pool = Zset.create_pool 10 10 in
      let itr = L.iter_bundle_element ic element in
      let default = null_value ty in
      { element; bundle = ic; itr; default; ord; ztype=ty; pool }

  let space ic = L.space ic.bundle

  let descr ic =
    let ZcolDescr(n,_,_,_) = L.descr ic.bundle in
    let n = sprintf "%s[%d]" n ic.element in
    ZcolDescr(n,ic.ztype,Virtual_column,ic.ord)

  let length ic =
    ic.itr # length

  let get_value : type t . t zinvcol -> int -> t =
    fun ic k ->
      ic.itr # go k;
      let _, v = ic.itr # current_value in
      v

  let get_hash_and_value : type t . t zinvcol -> int -> (int * t) =
    fun ic k ->
      let v = get_value ic k in
      (ic.ord.ord_hash v, v)

  let get_hash ic k = fst(get_hash_and_value ic k)

  let get_zset ic k =
    ic.itr # go k;
    ic.itr # current_zset ic.pool

  let find ?hash ic comp x =
    ic.itr # find comp x;
    ic.itr # current_pos

  let value_of_id : type s . s zinvcol -> int -> s =
    fun ic id ->
      let v = L.value_of_id ic.bundle id in
      try List.assoc ic.element v
      with Not_found -> ic.default

  let index_of_id ic id =
    (* this is quite expensive! *)
    let v = value_of_id ic id in
    find ic EQ v

  let value_array_of_zset ic zset =
    Array.map (value_of_id ic) (Zset.to_array zset)

  let iter_all :  type t . t zinvcol -> t ziterator =
    fun ic ->
      L.iter_bundle_element ic.bundle ic.element

  let iter ?all_values ic zset =
    let itr1 = L.iter_bundle_element ic.bundle ic.element in
    iter_isect ?all_values itr1 zset

  let iter_union ?all_values ic zsets =
    let s = Zset.union_many ic.pool zsets in
    iter ?all_values ic s

  let nonanon_zset ic base pool =
    Zset.all ic.pool (L.space ic.bundle)

  let anon_zset ic base pool =
    Zset.empty

  let is_anon ic k =
    false  (* sparse columns not supported *)

  let nonnone_zset ic base pool =
    (* this is super-slow... *)
    let none_idx = find ic EQ None in
    Zset.filter
      pool
      (fun id ->
        try
          let idx = index_of_id ic id in
          idx <> none_idx
        with Not_found -> false
      )
      base

  let dump3 conv_zset ic =
    (* testing *)
    let len = length ic in
    let l = ref [] in
    for k = len-1 downto 0 do
      let v = get_value ic k in
      let z = get_zset ic k in
      l := (v, conv_zset z) :: !l
    done;
    !l

  let dump ic = dump3 Zset.to_array ic
  let dump2 ic = dump3 Zset.dump ic

end
             
             
type _ zinvcol =
  | N_col : 'a N.zinvcol -> 'a zinvcol       (* most types *)
  | L_col : 'a L.zinvcol -> 'a list zinvcol  (* lists *)
  | B_col : 'a B.zinvcol -> 'a zinvcol       (* bundle elements *)

type _ zinvcol_builder =
  | N_bld : 'a N.zinvcol_builder -> 'a zinvcol_builder
  | L_bld : 'a L.zinvcol_builder -> 'a list zinvcol_builder

      
let create_memory_zinvcol_builder
    : type t . _ -> _ -> _ -> t zcoldescr -> t zinvcol_builder =
  fun inv_size idt_size space coltype ->
    let ZcolDescr(n,ty,repr,ord) = coltype in
    match ty with
      | Zlist ty1 ->
          L_bld(L.create_memory_zinvcol_builder
                  inv_size idt_size space coltype false)
      | Zbundle ty1 ->
          let ty2 = Zlist(Zpair(Zint, ty1)) in
          let coltype2 = ZcolDescr(n,ty2,repr,ord) in
          L_bld(L.create_memory_zinvcol_builder
                  inv_size idt_size space coltype2 true)
      | _ ->
          N_bld(N.create_memory_zinvcol_builder inv_size idt_size space coltype)


let create_file_zinvcol_builder
    : type t . _ -> _ -> _ -> _ -> t zcoldescr -> t zinvcol_builder =
  fun dir inv_size idt_size space coltype ->
    let ZcolDescr(n,ty,repr,ord) = coltype in
    match ty with
      | Zlist _ ->
          L_bld(L.create_file_zinvcol_builder
                  dir inv_size idt_size space coltype false)
      | Zbundle ty1 ->
          let ty2 = Zlist(Zpair(Zint, ty1)) in
          let coltype2 = ZcolDescr(n,ty2,repr,ord) in
          L_bld(L.create_file_zinvcol_builder
                  dir inv_size idt_size space coltype2 true)
      | _ ->
          N_bld(N.create_file_zinvcol_builder
                  dir inv_size idt_size space coltype)
             
             
let build_add : type t . t zinvcol_builder -> t -> _ -> unit =
  fun bld x set ->
  match bld with
    | N_bld b ->
        N.build_add b x set
    | L_bld b ->
        L.build_add b x set


let build_end : type t . t zinvcol_builder -> t zinvcol =
  function
    | N_bld b ->
        N_col(N.build_end b)
    | L_bld b ->
        L_col(L.build_end b)
               
let build_abort : type t . t zinvcol_builder -> unit =
  function
    | N_bld b ->
        N.build_abort b
    | L_bld b ->
        L.build_abort b
               
let from_file : type t . _ -> _ -> t zcoldescr -> t zinvcol =
  fun dir space coltype ->
    let ZcolDescr(n,ty,repr,ord) = coltype in
    match ty with
      | Zlist _ ->
          L_col(L.from_file dir space coltype)
      | Zbundle ty1 ->
          let ty2 = Zlist(Zpair(Zint, ty1)) in
          let coltype2 = ZcolDescr(n,ty2,repr,ord) in
          L_col(L.from_file dir space coltype2)
      | _ ->
          N_col(N.from_file dir space coltype)

let length : type t . t zinvcol -> _ =
  function
  | L_col col -> L.length col
  | N_col col -> N.length col
  | B_col col -> B.length col

let get_hash : type t . t zinvcol -> _ =
  function
  | L_col col -> L.get_hash col
  | N_col col -> N.get_hash col
  | B_col col -> B.get_hash col

let get_hash_and_value : type t . t zinvcol -> int -> (int * t) =
  function
  | L_col col -> L.get_hash_and_value col
  | N_col col -> N.get_hash_and_value col
  | B_col col -> B.get_hash_and_value col

let get_value : type t . t zinvcol -> int -> t =
  function
  | L_col col -> L.get_value col
  | N_col col -> N.get_value col
  | B_col col -> B.get_value col

let get_zset : type t . t zinvcol -> int -> _ =
  function
  | L_col col -> L.get_zset col
  | N_col col -> N.get_zset col
  | B_col col -> B.get_zset col

let is_anon : type t . t zinvcol -> _ -> _ =
  function
  | L_col col -> L.is_anon col
  | N_col col -> N.is_anon col
  | B_col col -> B.is_anon col


let anon_zset : type t . t zinvcol -> _ -> _ -> _ =
  function
  | L_col col -> L.anon_zset col
  | N_col col -> N.anon_zset col
  | B_col col -> B.anon_zset col

let nonanon_zset : type t . t zinvcol -> _ -> _ -> _ =
  function
  | L_col col -> L.nonanon_zset col
  | N_col col -> N.nonanon_zset col
  | B_col col -> B.nonanon_zset col

let nonnone_zset : type t . t option zinvcol -> _ -> _ -> _ =
  function
  (* | L_col _ -> assert false -- type checker sees that this is not possible!*)
  | N_col col -> N.nonnone_zset col
  | B_col col -> B.nonnone_zset col

let find : type t . ?hash:int -> t zinvcol -> _ -> t -> _ =
  fun ?hash -> function
  | L_col col -> L.find col
  | N_col col -> N.find ?hash col
  | B_col col -> B.find ?hash col

let index_of_id : type t . t zinvcol -> _ -> _ =
  function
  | L_col col -> L.index_of_id col
  | N_col col -> N.index_of_id col
  | B_col col -> B.index_of_id col

let value_of_id : type t . t zinvcol -> _ -> t =
  fun c id ->
  match c with
    | L_col col -> L.value_of_id col id
    | N_col col -> N.value_of_id col id
    | B_col col -> B.value_of_id col id

let value_array_of_zset : type t . t zinvcol -> _ -> t array =
  function
  | L_col col -> L.value_array_of_zset col
  | N_col col -> N.value_array_of_zset col
  | B_col col -> B.value_array_of_zset col

let dump : type t . t zinvcol -> (t * int array) list =
  function
  | L_col col -> L.dump col
  | N_col col -> N.dump col
  | B_col col -> B.dump col

let dump2 : type t . t zinvcol -> (t * string) list =
  function
  | L_col col -> L.dump2 col
  | N_col col -> N.dump2 col
  | B_col col -> B.dump2 col

let dump3 : type t . _ -> t zinvcol -> (t * _) list =
  fun f -> function
  | L_col col -> L.dump3 f col
  | N_col col -> N.dump3 f col
  | B_col col -> B.dump3 f col

let merge : type t . t zinvcol_builder -> t zinvcol array -> unit =
  fun bld wrapped_cols ->
  match bld with
    | L_bld b ->
        let cols =
          Array.map
            (function
              | N_col _ | B_col _ -> assert false
              | L_col col -> col
            )
            wrapped_cols in
        L.merge b cols
    | N_bld b ->
        let cols =
          Array.map
            (function
              | N_col col -> col
              | _ -> assert false
            )
            wrapped_cols in
        ignore(N.merge b cols)
              
let iter_all : type t . t zinvcol -> t ziterator =
  function
  | L_col col -> L.iter_all col
  | N_col col -> N.iter_all col
  | B_col col -> B.iter_all col

let iter : type t . ?all_values:bool -> t zinvcol -> _ -> t ziterator =
  fun ?all_values -> function
  | L_col col -> L.iter ?all_values col
  | N_col col -> N.iter ?all_values col
  | B_col col -> B.iter ?all_values col

let iter_union : type t . ?all_values:bool -> t zinvcol -> _ -> t ziterator =
  fun ?all_values -> function
  | L_col col -> L.iter_union ?all_values col
  | N_col col -> N.iter_union ?all_values col
  | B_col col -> B.iter_union ?all_values col

let iter_mem_all =
  function
  | L_col col -> L.iter_mem_all col
  | _ -> assert false   (* not typable *)

let iter_mem_left =
  function
  | L_col col -> L.iter_bundle_element col
  | _ -> assert false   (* not typable *)
                
let build_from_iterator : type t . t zinvcol_builder -> _ -> t ziterator ->
                               t zinvcol =
  fun bld pool itr ->
  match bld with
    | L_bld b ->
        L_col (L.build_from_iterator b pool itr)
    | N_bld b ->
        N_col (N.build_from_iterator b pool itr)

let storage_size : type t . t zinvcol -> _ =
  function
  | L_col col -> L.storage_size col
  | N_col col -> N.storage_size col
  | B_col col -> 0

let space : type t . t zinvcol -> _ =
  function
  | L_col col -> L.space col
  | N_col col -> N.space col
  | B_col col -> B.space col

let descr : type t . t zinvcol -> t zcoldescr =
  function
  | L_col col -> L.descr col
  | N_col col -> N.descr col
  | B_col col -> B.descr col
                  
let set_b_done : type t . t zinvcol_builder -> (t zinvcol -> unit) -> unit =
  function
  | L_bld b -> (fun f -> L.set_b_done b (fun col -> f (L_col col)))
  | N_bld b -> (fun f -> N.(b.b_done <- (fun col -> f (N_col col))))

let select : type t . (int * t) list zinvcol -> int -> t zinvcol =
  fun col k ->
    match col with
      | L_col col ->
          B_col(B.select col k)
      | _ ->
          assert false

let indices col =
  match col with
    | L_col col -> L.indices col
    | _ -> assert false
