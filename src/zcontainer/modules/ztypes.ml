exception End_of_column

module StrMap = Map.Make(String)

type (_,_) type_eq =
  | Equal : ('a, 'a) type_eq
  | Not_equal

type space =
    (* private *)
    Space of int

let create_space n =
  if n < 1 then
    invalid_arg "Zcontainer.Ztypes.create_space";
  Space n

type _ ztype =
  | Zstring : string ztype
  | Zint64 : int64 ztype
  | Zint : int ztype
  | Zfloat : float ztype
  | Zbool : bool ztype
  | Zoption : 'a ztype -> 'a option ztype
  | Zpair : 'a ztype * 'b ztype -> ('a * 'b) ztype
  | Zlist : 'a ztype -> 'a list ztype
  | Zbundle : 'a ztype -> (int * 'a) list ztype
                                             
type ztypebox =
  | ZtypeBox : _ ztype -> ztypebox

type fixed64_data =
    (int64, Bigarray.int64_elt, Bigarray.c_layout) Bigarray.Array1.t

type 'a fixed64 =
    { f64_name : string;
      f64_size : int;
      f64_encode : fixed64_data -> int -> 'a -> unit;
      f64_decode : fixed64_data -> int -> 'a;
    }

(* e.g. [int64 fixed64], [float fixed64], [complex fixed64] *)

type fixed64box =
  | Fixed64Box : 'a ztype * 'a fixed64 -> fixed64box

type 'a multibyte =
    { mb_name : string;
      mb_size : 'a -> int;
      mb_encode : Bytes.t -> int -> 'a -> unit;
      mb_decode : Bytes.t -> int -> int -> 'a;
    }

type multibytebox =
  | MultibyteBox : 'a ztype * 'a multibyte -> multibytebox

type multibyteoption =
  { multibyteOption : 'a . ('a ztype -> 'a multibyte -> 'a option multibyte) }

type multibytelist =
  { multibyteList : 'a . ('a ztype -> 'a multibyte -> 'a list multibyte) }

type multibytebundle =
  { multibyteBundle : 'a . ('a ztype -> 'a multibyte ->
                            (int * 'a) list multibyte) }

type multibytepair =
  { multibytePair :
      'a 'b . 'a ztype -> 'a multibyte -> 'b ztype -> 'b multibyte ->
              ('a*'b) multibyte
  }

type 'a zrepr =
  | Inv_multibyte of 'a multibyte
  | SInv_multibyte of 'a multibyte * 'a * ('a -> bool)
      (* plus the anonymous element, and a test for anonymity *)
  | Dir_multibyte of 'a multibyte
  | Dir_fixed64 of 'a fixed64
  | Dir_bigarray : ('a,'b) Bigarray.kind -> 'a zrepr
  | Virtual_column
                                               
type zreprbox =
  | ZreprBox : 'a ztype * 'a zrepr -> zreprbox

type direction =
  | ASC | DESC

type ord_composition =
  | Ord_leaf
  | Ord_pair
  | Ord_option
  | Ord_list
  | Ord_bundle_pair
  | Ord_bundle_list
  | Ord_custom

type 'a zorder =
    { ord_name : string;
      ord_comp : ord_composition;
      ord_hash : 'a -> int;
      ord_cmp : 'a -> 'a -> int;
      ord_cmp_asc : 'a -> 'a -> int;
      ord_dir : direction option;
      ord_sub : zorderbox array;
    }

and zorderbox =
  | ZorderBox : 'a ztype * 'a zorder -> zorderbox

type zorderoption =
  { zorderOption : 'a . 'a ztype -> 'a zorder -> 'a option zorder }

type zorderlist =
  { zorderList : 'a . 'a ztype -> 'a zorder -> 'a list zorder }

type zorderpair =
  { zorderPair : 'a 'b . 'a ztype -> 'a zorder -> 'b ztype -> 'b zorder ->
                  ('a * 'b) zorder
  }

type zorderconv =
  { zorderConv : 'a 'b . 'a ztype -> 'b ztype -> 'b zorder -> 'a zorder option
  }
    
type zcolname = string

type _ zcoldescr =
  | ZcolDescr : zcolname * 'a ztype * 'a zrepr * 'a zorder -> 'a zcoldescr

type zcolbox =
  | ZcolBox : zcolname * 'a ztype * 'a zrepr * 'a zorder -> zcolbox

type stdrepr =
    { f64dict : fixed64box StrMap.t;
      mbdict : multibytebox StrMap.t;
      mboption : multibyteoption option;
      mbpair : multibytepair option;
      mblist : multibytelist option;
      mbbundle : multibytebundle option;
      orddict : zorderbox StrMap.t;
      ordoption : zorderoption option;
      (* ordlist : zorderlist option; *)
      (* ordbundle : zorderbundle option; *)
      ordpair : zorderpair option;
      ordconv : zorderconv;
    }

type comparison =
  | EQ | NE | LT | LE | GT | GE

let ord_custom = Ord_custom

let rec null_value : type t . t ztype -> t =
  function
  | Zstring -> ""
  | Zint64 -> 0L
  | Zint -> 0
  | Zfloat -> 0.0
  | Zoption _ -> None
  | Zpair(zt1,zt2) -> (null_value zt1, null_value zt2)
  | Zlist _ -> []
  | Zbool -> false
  | Zbundle _ -> []

let rec string_of_ztype : type t . t ztype -> string =
  function
  | Zstring -> "string"
  | Zint64 -> "int64"
  | Zint -> "int"
  | Zfloat -> "float"
  | Zoption zt -> string_of_ztype zt ^ " option"
  | Zpair(zt1,zt2) ->
      "(" ^ string_of_ztype zt1 ^ " * " ^ string_of_ztype zt2 ^ ")"
  | Zlist zt1 -> string_of_ztype zt1 ^ " list"
  | Zbool -> "bool"
  | Zbundle zt1 -> string_of_ztype zt1 ^ " bundle"

let parse_typename constr_leaf constr_option constr_pair constr_list
                   constr_bundle s =
  let lb = Lexing.from_string s in
  let rec parse tok =
    let ty =
      match tok with
        | Zlexer.Word w -> constr_leaf w
        | Zlexer.Lparen ->
            let ty, tok' = parse (Zlexer.type_token lb) in
            if tok' <> Zlexer.Rparen then
              failwith "Malformed type expression";
            ty
        | _ ->
            failwith "Malformed type expression" in
    parse_right (Zlexer.type_token lb) ty
  and parse_right tok ty1 =
    match tok with
      | Zlexer.Word "option" ->
          let ty' = constr_option ty1 in
          parse_right (Zlexer.type_token lb) ty'
      | Zlexer.Word "list" ->
          let ty' = constr_list ty1 in
          parse_right (Zlexer.type_token lb) ty'
      | Zlexer.Word "bundle" ->
          let ty' = constr_bundle ty1 in
          parse_right (Zlexer.type_token lb) ty'
      | Zlexer.Asterisk ->
          let ty2, tok2 = parse (Zlexer.type_token lb) in
          let ty' = constr_pair ty1 ty2 in
          parse_right tok2 ty'
      | _ ->
          (ty1, tok) in
  let ty, tok = parse (Zlexer.type_token lb) in
  if tok <> Zlexer.End then
    failwith "Malformed type expression";
  ty

let ztypebox_of_string s =
  let constr_leaf =
    function
    | "string" -> ZtypeBox Zstring
    | "int64" -> ZtypeBox Zint64
    | "int" -> ZtypeBox Zint
    | "float" -> ZtypeBox Zfloat
    | "bool" -> ZtypeBox Zbool
    | w -> failwith ("Unknown type: " ^ w) in
  let constr_option (ZtypeBox ty1) =
    ZtypeBox (Zoption ty1) in
  let constr_pair (ZtypeBox ty1) (ZtypeBox ty2) =
    ZtypeBox (Zpair(ty1,ty2)) in
  let constr_list (ZtypeBox ty1) =
    ZtypeBox (Zlist ty1) in
  let constr_bundle (ZtypeBox ty1) =
    ZtypeBox (Zbundle ty1) in
  parse_typename
    constr_leaf constr_option constr_pair constr_list constr_bundle s

let rec same_ztype : type s t . s ztype -> t ztype -> (s,t) type_eq =
  fun ty1 ty2 ->
    match ty1, ty2 with
      | Zstring, Zstring -> Equal
      | Zint64, Zint64 -> Equal
      | Zint, Zint -> Equal
      | Zfloat, Zfloat -> Equal
      | Zbool, Zbool -> Equal
      | Zoption ty11, Zoption ty21 ->
          ( match same_ztype ty11 ty21 with   (* needed for typing reasons *)
              | Equal -> Equal
              | Not_equal -> Not_equal
          )
      | Zpair(ty11,ty12), Zpair(ty21,ty22) ->
          ( match same_ztype ty11 ty21 with
              | Equal -> 
                  ( match same_ztype ty12 ty22 with
                      | Equal -> Equal
                      | Not_equal -> Not_equal
                  )
              | Not_equal -> Not_equal
          )
      | Zlist ty11, Zlist ty21 ->
          ( match same_ztype ty11 ty21 with
              | Equal -> Equal
              | Not_equal -> Not_equal
          )
      | Zbundle ty11, Zbundle ty21 ->
          ( match same_ztype ty11 ty21 with
              | Equal -> Equal
              | Not_equal -> Not_equal
          )
      | _ -> Not_equal

let string_of_zrepr =
  function
  | Inv_multibyte mb -> "IM/" ^ mb.mb_name
  | SInv_multibyte(mb,_,_) -> "SIM/" ^ mb.mb_name
  | Dir_multibyte mb -> "DM/" ^ mb.mb_name
  | Dir_fixed64 f64 -> "DF64/" ^ f64.f64_name
  | Dir_bigarray _ -> "DBA"
  | Virtual_column -> "VIRTUAL"

let zreprbox_of_string stdrepr s =
  (* This function doesn't support Dir_bigarray and Virtual_column! *)
  let mb_repr name =
    let constr_leaf name =
      try
        StrMap.find name stdrepr.mbdict
      with
        | Not_found -> failwith ("No such representation: " ^ name) in
    let constr_option (MultibyteBox(zt,mb)) =
      try
        let canonical_name = mb.mb_name ^ " option" in
        StrMap.find canonical_name stdrepr.mbdict
      with
        | Not_found ->
            match stdrepr.mboption with
              | Some lift ->
                  let mb' = lift.multibyteOption zt mb in
                  MultibyteBox(Zoption zt, mb')
              | None ->
                  failwith "Unsupported representation: _ option" in
    let constr_list (MultibyteBox(zt,mb)) =
      try
        let canonical_name = mb.mb_name ^ " list" in
        StrMap.find canonical_name stdrepr.mbdict
      with
        | Not_found ->
            match stdrepr.mblist with
              | Some lift ->
                  let mb' = lift.multibyteList zt mb in
                  MultibyteBox(Zlist zt, mb')
              | None ->
                  failwith "Unsupported representation: _ list" in
    let constr_pair (MultibyteBox(zt1,mb1)) (MultibyteBox(zt2,mb2)) =
      try
        let canonical_name = "(" ^ mb1.mb_name ^ " * " ^ mb2.mb_name ^ ")" in
        StrMap.find canonical_name stdrepr.mbdict
      with
        | Not_found ->
            match stdrepr.mbpair with
              | Some lift ->
                  let mb' = lift.multibytePair zt1 mb1 zt2 mb2 in
                  MultibyteBox(Zpair(zt1,zt2), mb')
              | None ->
                  failwith "Unsupported representation: _ * _" in
    let constr_bundle (MultibyteBox(zt,mb)) =
      try
        let canonical_name = mb.mb_name ^ " bundle" in
        StrMap.find canonical_name stdrepr.mbdict
      with
        | Not_found ->
            match stdrepr.mbbundle with
              | Some lift ->
                  let mb' = lift.multibyteBundle zt mb in
                  MultibyteBox(Zbundle zt, mb')
              | None ->
                  failwith "Unsupported representation: _ bundle" in
    parse_typename
      constr_leaf constr_option constr_pair constr_list constr_bundle name in
  try
    let p = String.index s '/' in
    let kind = String.sub s 0 p in
    let name = String.sub s (p+1) (String.length s - p - 1) in
    match kind with
      | "IM" ->
          let mbbox = mb_repr name in
          let MultibyteBox(ty,mb) = mbbox in
          ZreprBox(ty,Inv_multibyte mb)
      | "SIM" ->
          let mbbox = mb_repr name in
          let MultibyteBox(ty,mb) = mbbox in
          ( match ty with
              | Zoption ty' ->
                  let anon = None in
                  let is_anon = (function None -> true | _ -> false) in
                  ZreprBox(ty,SInv_multibyte(mb,anon,is_anon))
              | _ ->
                  failwith "SIM can only be used with option types"
          )
      | "DM" ->
          let mbbox = mb_repr name in
          let MultibyteBox(ty,mb) = mbbox in
          ZreprBox(ty,Dir_multibyte mb)
      | "DF64" ->
          let f64box = StrMap.find name stdrepr.f64dict in
          let Fixed64Box(ty,f64) = f64box in
          ZreprBox(ty,Dir_fixed64 f64)
      | _ ->
          raise Not_found
  with
    | Not_found ->
        failwith ("zreprbox_of_string: no such representation: " ^ s)


let string_of_zorder zo = zo.ord_name

(* The ordering of lists is hard-coded at the moment, because the Zinvcol
   module expects certain properties
 *)
let rec list_cmp cmp_base x y =
  match x, y with
    | [], [] -> 0
    | [], _ :: _ -> (-1)
    | _ :: _, [] -> 1
    | x1::xr, y1::yr ->
        let d = cmp_base x1 y1 in
        if d = 0 then
          list_cmp cmp_base xr yr
        else
          d

let list_ord : type t . t ztype -> t zorder -> t list zorder =
  fun zt zord ->
  { ord_name = zord.ord_name ^ " list";
    ord_comp = Ord_list;
    ord_hash = (function
                 | [] -> 0
                 | x :: _ -> zord.ord_hash x
               );
    ord_cmp = list_cmp zord.ord_cmp;
    ord_cmp_asc = list_cmp zord.ord_cmp_asc;
    ord_dir = None;
    ord_sub = [| ZorderBox(zt,zord) |]
  }


let pair_cmp cmp_base1 cmp_base2 (x1,y1) (x2,y2) =
  let d = cmp_base1 x1 x2 in
  if d <> 0 then
    d
  else
    cmp_base2 y1 y2


let pair_ord : type s t . s ztype -> s zorder -> t ztype -> t zorder ->
                    (s * t) zorder =
  fun zt1 zord1 zt2 zord2 ->
  { ord_name = "(" ^ zord1.ord_name ^ " * " ^ zord2.ord_name ^ ")";
    ord_comp = Ord_pair;
    ord_hash = (fun (x,y) ->
                  zord1.ord_hash x
               );
    ord_cmp = pair_cmp zord1.ord_cmp zord2.ord_cmp;
    ord_cmp_asc = pair_cmp zord1.ord_cmp_asc zord2.ord_cmp_asc;
    ord_dir = None;
    ord_sub = [| ZorderBox(zt1,zord1); ZorderBox(zt2,zord2) |];
  }
  (* Note that Zinvcol.find_left_first and find_left_last exploit this
     particular definition of ord_hash!
   *)

let int_hash x =
  (* this needs to be compatible with int64 *)
  ((x asr 1) lsr 1) lxor 0x2000_0000_0000_0000


let int_asc =
  { ord_name = "int_asc";
    ord_comp = Ord_leaf;
    ord_hash = int_hash;
    ord_cmp = (compare : int -> int -> int);
    ord_cmp_asc = (compare : int -> int -> int);
    ord_dir = Some ASC;
    ord_sub = [| |];
  }


let bundle_pair_ord : type t . t ztype -> t zorder -> (int * t) zorder =
  fun zt2 zord2 ->
  { ord_name = "( int_asc [BUNDLE]* " ^ zord2.ord_name ^ ")";
    ord_comp = Ord_bundle_pair;
    ord_hash = (fun (x,y) ->
                  (* the upper 14 bits are for x (an int), and the lower
                     48 bits are for y
                   *)
                  if x <= -8192 then
                    0
                  else if x >= 8191 then
                    0x3fff lsl 48
                  else
                    let x1 = ((x land 0x3fff) lxor 0x2000) lsl 48 in
                    let x2 = (zord2.ord_hash y) lsr 14 in
                    x1 lor x2
               );
    ord_cmp = pair_cmp int_asc.ord_cmp zord2.ord_cmp;
    ord_cmp_asc = pair_cmp int_asc.ord_cmp_asc zord2.ord_cmp_asc;
    ord_dir = None;
    ord_sub = [| ZorderBox(Zint,int_asc); ZorderBox(zt2,zord2) |];
  }
  (* Note that Zinvcol.find_left_first and find_left_last exploit this
     particular definition of ord_hash!
   *)

let nat_list_hash l =
  let x = ref 0 in
  let k = ref 62 in
  ( try
      List.iter
        (fun nat ->
          let n = Zencoding.LenPre3.encode_length nat in
          let v = Zencoding.LenPre3.encode_list n nat in
          List.iter
            (fun u ->
              if !k < 0 then raise Exit;
              let k' = !k - 8 in
              if k' >= 0 then
                x := !x lor (u lsl k')
              else
                x := !x lor (u lsr (-k'));
              k := k'
            )
            v
        )
        l
    with Exit -> ()
  );
  !x

let nat_list_ord =
  (* lists of natural numbers - needed for representing list columns *)
  { ord_name = "nat list";
    ord_comp = Ord_bundle_list;
    ord_hash = nat_list_hash;
    ord_cmp = list_cmp int_asc.ord_cmp;
    ord_cmp_asc = list_cmp int_asc.ord_cmp_asc;
    ord_dir = None;
    ord_sub = [| ZorderBox(Zint,int_asc) |]
  }


let bundle_ord : type t . t ztype -> t zorder -> (int * t) list zorder =
  fun ty zord ->
  (* Zbundle always uses the standard order for (int * _) list. We use
     a tweaked hash function, assuming that the bundle indices are
     in the range -8191 to +8190.
   *)
  let ty_list = Zpair(Zint,ty) in
  let ty1 =
    list_ord
      ty_list
      (* (pair_ord Zint int_asc ty zord) in *)
      (bundle_pair_ord ty zord) in
  { ty1 with
    ord_name = zord.ord_name ^ " bundle"
  }

let extract_ord_from_bundle : type t . t ztype ->
                                   (int * t) list zorder -> t zorder =
  fun ty ord ->
  match ord.ord_sub with
    | [| ZorderBox(_, { ord_sub = [| _; ZorderBox(ty_r, ord_r) |] } ) |] ->
        ( match same_ztype ty ty_r with
            | Equal -> ord_r
            | Not_equal -> assert false
        )
    | _ ->
        assert false

                           
let rec natural_zorder : type t . stdrepr -> t ztype -> direction -> t zorder =
  fun stdrepr ty dir ->
  let suff =
    match dir with
      | ASC -> "_asc"
      | DESC -> "_desc" in
  let leaf : type s . s ztype -> string -> s zorder = fun ty name ->
    try
      let ZorderBox(ty1,ord1) = StrMap.find name stdrepr.orddict in
      ( match same_ztype ty ty1 with
          | Equal -> ord1
          | Not_equal -> failwith ("Wrong type in stdrepr: " ^ name)
      )
    with
      | Not_found ->
          failwith ("No such order: " ^ name) in
  match ty with
    | Zstring -> leaf ty ("string" ^ suff)
    | Zint64 -> leaf ty ("int64" ^ suff)
    | Zint -> leaf ty ("int" ^ suff)
    | Zfloat -> leaf ty ("float" ^ suff)
    | Zbool -> leaf ty ("bool" ^ suff)
    | Zoption ty1 ->
        ( match stdrepr.ordoption with
            | Some lift ->
                let ord1 = natural_zorder stdrepr ty1 dir in
                lift.zorderOption ty1 ord1
            | None ->
                failwith "Unsupported order: _ option"
        )
    | Zpair(ty1,ty2) ->
        ( match stdrepr.ordpair with
            | Some lift ->
                let ord1 = natural_zorder stdrepr ty1 dir in
                let ord2 = natural_zorder stdrepr ty2 dir in
                lift.zorderPair ty1 ord1 ty2 ord2
            | None ->
                failwith "Unsupported order: _ * _"
        )
    | Zlist ty1 ->
        let ord1 = natural_zorder stdrepr ty1 dir in
        list_ord ty1 ord1
    | Zbundle ty1 ->
        let ord1 = natural_zorder stdrepr ty1 dir in
        bundle_ord ty1 ord1

let zorder_of_string stdrepr targetzt s =
  let rec build_zorder : type t . t ztype -> _ -> t zorder = fun zt expr ->
    match expr with
      | `Leaf name ->
          ( let zobox =
              try
                StrMap.find name stdrepr.orddict
              with
                | Not_found -> failwith ("No such order: " ^ name) in
            let ZorderBox(zozt,zo) = zobox in
            match same_ztype zt zozt with
              | Equal -> zo
              | Not_equal ->
                  ( match stdrepr.ordconv.zorderConv zt zozt zo with
                      | None -> 
                          failwith "Bad type of ordering, and no conversion possible"
                      | Some zo' -> zo'
                  )
          )
      | `Option sub ->
          ( match zt with
              | Zoption ztsub ->
                  ( let ord_sub = build_zorder ztsub sub in
                    try
                      let canonical_name = ord_sub.ord_name ^ " option" in
                      let ZorderBox(canon_zt,zo) =
                        StrMap.find canonical_name stdrepr.orddict in
                      ( match same_ztype zt canon_zt with
                          | Equal ->
                              zo
                          | _ ->
                              failwith "Bad stdrepr"
                      )
                    with
                      | Not_found ->
                          match stdrepr.ordoption with
                            | Some lift ->
                                lift.zorderOption ztsub ord_sub
                            | None ->
                                failwith "Unsupported order: _ option"
                  )
              | _ ->
                  failwith "Order structure is incompatible with type"
          )
      | `List sub ->
          ( match zt with
              | Zlist ztsub ->
                  ( let ord_sub = build_zorder ztsub sub in
                    list_ord ztsub ord_sub
                  )
              | _ ->
                  failwith "Order structure is incompatible with type"
          )
      | `Bundle sub ->
          ( match zt with
              | Zbundle ztsub ->
                  ( let ord_sub = build_zorder ztsub sub in
                    bundle_ord ztsub ord_sub
                  )
              | _ ->
                  failwith "Order structure is incompatible with type"
          )
      | `Pair(sub1,sub2) ->
          ( match zt with
              | Zpair(ztsub1,ztsub2) ->
                  let ord_sub1 = build_zorder ztsub1 sub1 in
                  let ord_sub2 = build_zorder ztsub2 sub2 in
                  ( try
                      let canonical_name =
                        "(" ^ ord_sub1.ord_name ^ " * " ^ ord_sub2.ord_name ^ ")" in
                      let ZorderBox(canon_zt,zo) =
                        StrMap.find canonical_name stdrepr.orddict in
                      ( match same_ztype zt canon_zt with
                          | Equal ->
                              zo
                          | _ ->
                              failwith "Bad stdrepr"
                      )
                    with
                      | Not_found ->
                          match stdrepr.ordpair with
                            | Some lift ->
                                lift.zorderPair ztsub1 ord_sub1 ztsub2 ord_sub2
                            | None ->
                                failwith "Unsupported order: _ * _"
                  )
              | _ ->
                  failwith "Order structure is incompatible with type"
          ) in
  if s = "asc" then
    natural_zorder stdrepr targetzt ASC
  else if s = "desc" then
    natural_zorder stdrepr targetzt DESC
  else
    let constr_leaf name = `Leaf name in
    let constr_option sub = `Option sub in
    let constr_list sub = `List sub in
    let constr_pair sub1 sub2 = `Pair(sub1,sub2) in
    let constr_bundle sub = `Bundle sub in
    let expr =
      parse_typename
        constr_leaf constr_option constr_pair constr_list constr_bundle s in
    build_zorder targetzt expr

let zorderbox_of_string stdrepr ztbox s =
  let ZtypeBox zt = ztbox in
  ZorderBox(zt, zorder_of_string stdrepr zt s)


let same_zrepr r1 r2 =
  match r1, r2 with
    | Inv_multibyte mb1, Inv_multibyte mb2 ->
        mb1.mb_name = mb2.mb_name
    | SInv_multibyte(mb1,_,_), SInv_multibyte(mb2,_,_) ->
        mb1.mb_name = mb2.mb_name   (* FIXME: compare anon elem *)
    | Dir_multibyte mb1, Dir_multibyte mb2 ->
        mb1.mb_name = mb2.mb_name
    | Dir_fixed64 f64_1, Dir_fixed64 f64_2 ->
        f64_1.f64_name = f64_2.f64_name
    | _ -> false

let same_descr d1 d2 =
  match d1, d2 with
    | ZcolDescr(_,_,r1,o1), ZcolDescr(_,_,r2,o2) ->
        same_zrepr r1 r2 && o1.ord_name = o2.ord_name

let debug_string_of_descr =
  function
  | ZcolDescr(name,ty,repr,zo) ->
      Printf.sprintf "%s<type:%s,repr:%s,order:%s>"
              name
              (string_of_ztype ty)
              (string_of_zrepr repr)
              (string_of_zorder zo)


let rec debug_string_of_value : type t . t ztype -> t -> string =
  function
  | Zstring ->
      Printf.sprintf "%S"
  | Zint64 ->
      Printf.sprintf "%Ld"
  | Zint ->
      Printf.sprintf "%d"
  | Zfloat ->
      string_of_float
  | Zbool ->
      string_of_bool
  | Zoption zt ->
      (function
        | None -> "None"
        | Some x ->
            Printf.sprintf "Some %s" (debug_string_of_value zt x)
      )
  | Zpair(zt1,zt2) ->
      (fun (x1,x2) ->
         Printf.sprintf "(%s,%s)"
                        (debug_string_of_value zt1 x1)
                        (debug_string_of_value zt2 x2)
      )
  | Zlist zt1 ->
      (fun l ->
         "[" ^ String.concat ";" (List.map (debug_string_of_value zt1) l) ^
         "]"
      )
  | Zbundle zt1 ->
      debug_string_of_value (Zlist(Zpair(Zint,zt1)))
        
let rec json_of_value : type t . t ztype -> t -> Yojson.Safe.json =
  fun ty value ->
  match ty with
    | Zstring -> `String value
    | Zint64 -> `Intlit (Int64.to_string value)
    | Zint -> `Int value
    | Zfloat -> `Float value
    | Zbool -> `Bool value
    | Zoption ty1 ->
        ( match value with
            | None -> `List []
            | Some x -> `List [json_of_value ty1 x]
        )
    | Zpair(ty1,ty2) ->
        `List [json_of_value ty1 (fst value);
               json_of_value ty2 (snd value);
              ]
    | Zlist ty1 ->
        `List (List.map (json_of_value ty1) value)
    | Zbundle ty1 ->
        json_of_value (Zlist(Zpair(Zint,ty1))) value

let rec value_of_json : type t . t ztype -> Yojson.Safe.json -> t =
  fun ty j ->
  match ty, j with
    | Zstring, `String value -> value
    | Zint64, `Int n -> Int64.of_int n
    | Zint64, `Intlit n -> Int64.of_string n
    | Zint, `Int n -> n
    | Zint, `Intlit n -> int_of_string n
    | Zfloat, `Float x -> x
    | Zbool, `Bool b -> b
    | Zoption _, `List [] -> None
    | Zoption ty1, `List [j1] -> Some(value_of_json ty1 j1)
    | Zpair(ty1,ty2), `List[j1;j2] ->
        (value_of_json ty1 j1, value_of_json ty2 j2)
    | Zlist ty1, `List l ->
        List.map (value_of_json ty1) l
    | Zbundle ty1, _ ->
        value_of_json (Zlist(Zpair(Zint,ty1))) j
    | _ ->
        failwith "Zcontainer.Ztypes.value_of_json: bad JSON value"
        
let json_of_zcolbox (ZcolBox(name,ty,repr,order)) =
  `List [ `String name;
          `String (string_of_ztype ty);
          `String (string_of_zrepr repr);
          `String (string_of_zorder order)
        ]

let zcolbox_of_json stdrepr j =
  match j with
    | `List [ `String name;
              `String ty_name;
              `String repr_name;
              `String order_name
            ] ->
        ( try
            let tybox = ztypebox_of_string ty_name in
            let ZtypeBox ty = tybox in
            match repr_name with
              | "VIRTUAL" ->
                  let repr = Virtual_column in
                  let order = zorder_of_string stdrepr ty order_name in
                  ZcolBox(name,ty,repr,order)
              | "DBA" ->
                  failwith "DBA not (yet) supported"
              | _ ->
                  let reprbox = zreprbox_of_string stdrepr repr_name in
                  let ZreprBox(rty, repr) = reprbox in
                  ( match same_ztype ty rty with
                      | Not_equal ->
                          failwith "type mismatch for representation"
                      | Equal ->
                          let order = zorder_of_string stdrepr ty order_name in
                          ZcolBox(name,ty,repr,order)
                  )
          with
            | Failure msg ->
                failwith("Zcontainer.Ztypes.zcolbox_of_json: bad JSON (" ^
                           msg ^ ")")
                         
        )
    | _ ->
        failwith "Zcontainer.Ztypes.zcolbox_of_json: bad JSON"
          
let compare_test cmp d =
  match cmp with
    | EQ -> d = 0
    | NE -> d <> 0
    | GT -> d > 0
    | GE -> d >= 0
    | LT -> d < 0
    | LE -> d <= 0
