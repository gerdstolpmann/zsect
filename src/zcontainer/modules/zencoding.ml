(* ok to compile with -unsafe *)

module LenPre3 = struct
  type data =
      (char,Bigarray.int8_unsigned_elt,Bigarray.c_layout) Bigarray.Array1.t

  let lenpre3_max = 1 lsl 61 - 1
  let lenpre3_max_7 = 1 lsl 53 - 1
  let lenpre3_max_6 = 1 lsl 45 - 1
  let lenpre3_max_5 = 1 lsl 37 - 1
  let lenpre3_max_4 = 1 lsl 29 - 1
  let lenpre3_max_3 = 1 lsl 21 - 1
  let lenpre3_max_2 = 1 lsl 13 - 1
  let lenpre3_max_1 = 1 lsl 5 - 1

  let decode_length (data:data) pos =
    if pos < 0 || pos >= Bigarray.Array1.dim data then
      invalid_arg "Zcontainer.Zencoding.LenPre3.decode_length";
    let w = Char.code data.{ pos } in
    w lsr 5 + 1

  let decode_bytes_length (data:Bytes.t) pos =
    if pos < 0 || pos >= Bytes.length data then
      invalid_arg "Zcontainer.Zencoding.LenPre3.decode_bytes_length";
    let w = Char.code (Bytes.get data pos) in
    w lsr 5 + 1

  let decode (data:data) pos =
    let n = decode_length data pos in
    if pos < 0 || pos >= Bigarray.Array1.dim data - n + 1 then
      invalid_arg "Zcontainer.Zencoding.LenPre3.decode";
    let posn = pos + n in
    let x0 =
      if n >= 2 then Char.code data.{ posn - 1 } else 0 in
    let x1 =
      if n >= 3 then (Char.code data.{ posn - 2 }) lsl 8 else 0 in
    let x2 =
      if n >= 4 then (Char.code data.{ posn - 3 }) lsl 16 else 0 in
    let x3 =
      if n >= 5 then (Char.code data.{ posn - 4 }) lsl 24 else 0 in
    let x4 =
      if n >= 6 then (Char.code data.{ posn - 5 }) lsl 32 else 0 in
    let x5 =
      if n >= 7 then (Char.code data.{ posn - 6 }) lsl 40 else 0 in
    let x6 =
      if n >= 8 then (Char.code data.{ posn - 7 }) lsl 48 else 0 in
    let x7 =
      ((Char.code data.{ pos }) land 0x1f) lsl ((n-1) lsl 3) in
    x0 lor x1 lor x2 lor x3 lor x4 lor x5 lor x6 lor x7

  let decode_bytes (data:Bytes.t) pos =
    let n = decode_bytes_length data pos in
    if pos < 0 || pos >= Bytes.length data - n + 1 then
      invalid_arg "Zcontainer.Zencoding.LenPre3.decode_bytes";
    let posn = pos + n in
    let x0 =
      if n >= 2 then Char.code (Bytes.get data (posn - 1)) else 0 in
    let x1 =
      if n >= 3 then (Char.code (Bytes.get data (posn - 2))) lsl 8 else 0 in
    let x2 =
      if n >= 4 then (Char.code (Bytes.get data (posn - 3))) lsl 16 else 0 in
    let x3 =
      if n >= 5 then (Char.code (Bytes.get data (posn - 4))) lsl 24 else 0 in
    let x4 =
      if n >= 6 then (Char.code (Bytes.get data (posn - 5))) lsl 32 else 0 in
    let x5 =
      if n >= 7 then (Char.code (Bytes.get data (posn - 6))) lsl 40 else 0 in
    let x6 =
      if n >= 8 then (Char.code (Bytes.get data (posn - 7))) lsl 48 else 0 in
    let x7 =
      ((Char.code (Bytes.get data pos)) land 0x1f) lsl ((n-1) lsl 3) in
    x0 lor x1 lor x2 lor x3 lor x4 lor x5 lor x6 lor x7

  let encode_length x =
    if x < 0 || x > lenpre3_max then
      invalid_arg "Zcontainer.Zencoding.LenPre3.encode_length";
    if x <= lenpre3_max_1 then 1
    else if x <= lenpre3_max_2 then 2
    else if x <= lenpre3_max_3 then 3
    else if x <= lenpre3_max_4 then 4
    else if x <= lenpre3_max_5 then 5
    else if x <= lenpre3_max_6 then 6
    else if x <= lenpre3_max_7 then 7
    else 8

  let encode (data:data) pos n x =
    (* n must have been obtained from encode_length *)
    if n < 1 || n > 8 || pos < 0 || pos >= Bigarray.Array1.dim data - n + 1 then
      invalid_arg "Zcontainer.Zencoding.LenPre3.encode";
    let posn = pos + n in
    if n >= 8 then
      data.{ posn - 7 } <- Char.chr ((x lsr 48) land 0xff);
    if n >= 7 then
      data.{ posn - 6 } <- Char.chr ((x lsr 40) land 0xff);
    if n >= 6 then
      data.{ posn - 5 } <- Char.chr ((x lsr 32) land 0xff);
    if n >= 5 then
      data.{ posn - 4 } <- Char.chr ((x lsr 24) land 0xff);
    if n >= 4 then
      data.{ posn - 3 } <- Char.chr ((x lsr 16) land 0xff);
    if n >= 3 then
      data.{ posn - 2 } <- Char.chr ((x lsr 8) land 0xff);
    if n >= 2 then
      data.{ posn - 1 } <- Char.chr (x land 0xff);
    let x1 = (x lsr ((n-1) lsl 3)) land 0x1f in
    let x2 = (n-1) lsl 5 in
    data.{ pos } <- Char.chr (x1 lor x2)

  let encode_bytes (data:Bytes.t) pos n x =
    (* n must have been obtained from encode_length *)
    if n < 1 || n > 8 || pos < 0 || pos >= Bytes.length data - n + 1 then
      invalid_arg "Zcontainer.Zencoding.LenPre3.encode_bytes";
    let posn = pos + n in
    if n >= 8 then
      Bytes.set data (posn - 7) (Char.chr ((x lsr 48) land 0xff));
    if n >= 7 then
      Bytes.set data (posn - 6) (Char.chr ((x lsr 40) land 0xff));
    if n >= 6 then
      Bytes.set data (posn - 5) (Char.chr ((x lsr 32) land 0xff));
    if n >= 5 then
      Bytes.set data (posn - 4) (Char.chr ((x lsr 24) land 0xff));
    if n >= 4 then
      Bytes.set data (posn - 3) (Char.chr ((x lsr 16) land 0xff));
    if n >= 3 then
      Bytes.set data (posn - 2) (Char.chr ((x lsr 8) land 0xff));
    if n >= 2 then
      Bytes.set data (posn - 1) (Char.chr (x land 0xff));
    let x1 = (x lsr ((n-1) lsl 3)) land 0x1f in
    let x2 = (n-1) lsl 5 in
    Bytes.set data pos (Char.chr (x1 lor x2))

  let encode_list n x =
    (* n must have been obtained from encode_length *)
    if n < 1 || n > 8 then
      invalid_arg "Zcontainer.Zencoding.LenPre3.encode_list";
    let rec recurse acc r x =
      if r=1 then
        let x1 = x land 0x1f in
        let x2 = (n-1) lsl 5 in
        (x1 lor x2) :: acc
      else
        recurse ((x land 0xff) :: acc) (r-1) (x lsr 8) in
    recurse [] n x


end
