(* Direct columns *)

open Ztypes
open Printf

type f64_data =
    (int64, Bigarray.int64_elt, Bigarray.c_layout) Bigarray.Array1.t

type char_data =
    (char, Bigarray.int8_unsigned_elt, Bigarray.c_layout) Bigarray.Array1.t

type zdirdata =
    { mutable dc_f64_data : f64_data;
      mutable dc_char_data : char_data option;
    }

type 'a zdirvector =
  | ZdirVec : ('a,'b,Bigarray.fortran_layout) Bigarray.Array1.t -> 'a zdirvector

type 'a zdircol =
    { dc_space : space;
      dc_type : 'a zcoldescr;
      dc_data : zdirdata;
      dc_vector : 'a zdirvector option;
    }

module Pool_config_f64 = struct
  type entry = zdirdata
  type data_type = int64
  type data_repr = Bigarray.int64_elt
  let bigarray_kind = Bigarray.int64
  let bigarray_element_size = 8
  let get_bigarray dc = dc.dc_f64_data
  let set_bigarray dc data = dc.dc_f64_data <- data
end

module P_f64 = Zpool.Pool(Pool_config_f64)

module Pool_config_char = struct
  type entry = zdirdata
  type data_type = char
  type data_repr = Bigarray.int8_unsigned_elt
  let bigarray_kind = Bigarray.char
  let bigarray_element_size = 1
  let get_bigarray dc =
    match dc.dc_char_data with
      | Some data -> data
      | None -> assert false
  let set_bigarray dc data = dc.dc_char_data <- Some data
end

module P_char = Zpool.Pool(Pool_config_char)

type 'a zdircol_builder =
    { mutable b_open : bool;
      b_fsname : (string * string) option;
      b_f64_pool : P_f64.pool;
      b_char_pool : P_char.pool option;
      b_dc : 'a zdircol;
      mutable b_f64_end : int;
      mutable b_char_end : int;
      b_num_records : int;
      b_repr : 'a zrepr;
      b_order : 'a zorder;
      mutable b_done : 'a zdircol -> unit;
    }

let alloc_zdircol_builder fd_f64_opt fd_char_opt f64_size char_size
                          dc_space dc_type =
  let Space sp = dc_space in
  let ZcolDescr(_,_,b_repr,b_order) = dc_type in
  if f64_size < 1 || char_size < 1 || sp < 1 then
    invalid_arg (sprintf "Zcontainer.Zdircol.alloc_zdircol_builder (f64_size=%d char_size=%d sp=%d" f64_size char_size sp);
  let alloc b_f64_pool b_char_pool =
    let dc_data =
      P_f64.alloc_entry
        (fun dc_f64_data ->
           match b_char_pool with
             | None ->
                 { dc_f64_data;
                   dc_char_data = None
                 }
             | Some p ->
                 P_char.alloc_entry
                   (fun data ->
                      { dc_f64_data;
                        dc_char_data = Some data;
                      }
                   )
                   p
                   char_size
        )
        b_f64_pool
        f64_size in
    let b_dc = 
      { dc_space;
        dc_type;
        dc_data;
        dc_vector = None;
      } in
    { b_open = true;
      b_fsname = None;
      b_f64_pool;
      b_char_pool;
      b_dc;
      b_f64_end = 0;
      b_char_end = 0;
      b_num_records = sp;
      b_repr;
      b_order;
      b_done = (fun _ -> ());
    } in
  match b_repr, fd_f64_opt, fd_char_opt with
    | Dir_fixed64 _, Some fd_f64, None ->
        let b_f64_pool = P_f64.map_pool fd_f64 f64_size 1 in
        alloc b_f64_pool None
    | Dir_multibyte _, Some fd_f64, Some fd_char ->
        let b_f64_pool = P_f64.map_pool fd_f64 f64_size 1 in
        let b_char_pool = P_char.map_pool fd_char char_size 1 in
        alloc b_f64_pool (Some b_char_pool)
    | Dir_fixed64 _, None, None ->
        let b_f64_pool = P_f64.create_pool f64_size 1 in
        alloc b_f64_pool None
    | Dir_multibyte _, None, None ->
        let b_f64_pool = P_f64.create_pool f64_size 1 in
        let b_char_pool = P_char.create_pool char_size 1 in
        alloc b_f64_pool (Some b_char_pool)
    | _ ->
        assert false


let create_memory_zdircol_builder f64_size char_size dc_space dc_type =
  let ZcolDescr(_,_,b_repr,b_order) = dc_type in
  match b_repr with
    | Dir_fixed64 _
    | Dir_multibyte _ ->
        alloc_zdircol_builder None None f64_size char_size dc_space dc_type
    | Dir_bigarray _ ->
        invalid_arg "Zcontainer.Zdircol.create_memory_zdircol_builder: \
                     this column type is not allowed here"        
    | SInv_multibyte _
    | Inv_multibyte _ ->
        invalid_arg "Zcontainer.Zdircol.create_memory_zdircol_builder: \
                     column type does not specify direct format"
    | Virtual_column ->
        invalid_arg "Zcontainer.Zdircol.create_memory_zdircol_builder: \
                     virtual columns not allowed"


let create_memory_zdircol_builder_1
    : type t . space -> stdrepr -> string -> t ztype -> string -> string ->
               t zdircol_builder =
  fun space stdrepr name ty1 repr_name zo_name ->
    let zreprbox = zreprbox_of_string stdrepr repr_name in
    let ZreprBox(ty2, repr) = zreprbox in
    let zo = zorder_of_string stdrepr ty2 zo_name in
    ( match same_ztype ty1 ty2 with
        | Equal ->
            let descr = ZcolDescr(name,ty1,repr,zo) in
            create_memory_zdircol_builder 1000 1000 space descr
        | Not_equal ->
            failwith "Zcontainer.Zdircol.create_memory_zdircol_builder_1: \
                      type/order mismatch"
    )


let create_file_zdircol_builder dir f64_size char_size dc_space dc_type =
  let create full_name f =
    let fd =
      Unix.openfile
        full_name
        [ Unix.O_RDWR; Unix.O_CREAT; Unix.O_TRUNC; Unix.O_CLOEXEC ]
        0o666 in
    try
      f fd
    with
      | error ->
          Unix.close fd;
          ( try Unix.unlink full_name with _ -> ());
          raise error in
  let ZcolDescr(col_name,_,b_repr,b_order) = dc_type in
  let name = Filename.concat dir (Zutil.FileUtil.encode_name col_name) in
  match b_repr with
    | Dir_fixed64 _ ->
        create
          (name ^ ".dir")
          (fun fd_f64 ->
             (* NB. Keep the file descr open. It is now managed by the pool
                and will be closed when the pool is closed
              *)
             let b = alloc_zdircol_builder
                       (Some fd_f64) None f64_size char_size dc_space dc_type in
             { b with b_fsname = Some(dir,col_name) }
          )
    | Dir_multibyte _ ->
        create
          (name ^ ".dir")
          (fun fd_f64 ->
             create
               (name ^ ".ddt")
               (fun fd_char ->
                  alloc_zdircol_builder
                    (Some fd_f64) (Some fd_char) f64_size char_size
                    dc_space dc_type
               )
          )
    | Dir_bigarray _ ->
        invalid_arg "Zcontainer.Zdircol.create_file_zdircol_builder: \
                     this column type is not allowed here"        
    | Inv_multibyte _
    | SInv_multibyte _ ->
        invalid_arg "Zcontainer.Zdircol.create_file_zdircol_builder: \
                     column type does not specify direct format"
    | Virtual_column ->
        invalid_arg "Zcontainer.Zdircol.create_file_zdircol_builder: \
                     virtual columns not allowed"

let remove ?(force=false) dir col_name =
  let name = Filename.concat dir (Zutil.FileUtil.encode_name col_name) in
  Zutil.FileUtil.rm ~force (name ^ ".dir");
  Zutil.FileUtil.rm ~force (name ^ ".ddt")



(*
let hexdigit_uc =
  [| '0'; '1'; '2'; '3'; '4'; '5'; '6'; '7';
     '8'; '9'; 'A'; 'B'; 'C'; 'D'; 'E'; 'F'; |]

let hexdigit_lc =
  [| '0'; '1'; '2'; '3'; '4'; '5'; '6'; '7';
     '8'; '9'; 'a'; 'b'; 'c'; 'd'; 'e'; 'f'; |]


let to_hex ?(lc=false) s =
  let hexdigit = if lc then hexdigit_lc else hexdigit_uc in
  let l = String.length s in
  let u = String.create (2*l) in
  for k = 0 to l-1 do
    let c = String.unsafe_get s k in
    let j = k lsl 1 in
    String.unsafe_set u j     hexdigit.(Char.code c lsr 4);
    String.unsafe_set u (j+1) hexdigit.(Char.code c land 15);
  done;
  u
 *)


let build_add : type t . t zdircol_builder -> t -> int =
  fun b value ->
    let dc = b.b_dc in
    let d = dc.dc_data in
    let extend_f64 by =
      if b.b_f64_end + by > Bigarray.Array1.dim d.dc_f64_data then (
        let new_size = max (2 * b.b_f64_end) (b.b_f64_end + by) in
        P_f64.resize_entry ~assert_last:true b.b_f64_pool d new_size;
        assert(b.b_f64_end+by <= Bigarray.Array1.dim d.dc_f64_data);
      ) in
    let extend_char by =
      match d.dc_char_data with
        | Some data ->
            if b.b_char_end + by > Bigarray.Array1.dim data then (
              match b.b_char_pool with
                | Some pool ->
                    let new_size = max (2 * b.b_char_end) (b.b_char_end + by) in
                    P_char.resize_entry ~assert_last:true pool d new_size;
                    ( match d.dc_char_data with
                        | Some data ->
                            assert(b.b_char_end+by <= Bigarray.Array1.dim data);
                            data
                        | None ->
                            assert false
                    )
                | None ->
                    assert false
            ) else
              data
        | None ->
            assert false in
    match b.b_repr with
      | Dir_fixed64 f ->
          let by = f.f64_size in
          extend_f64 by;
          f.f64_encode d.dc_f64_data b.b_f64_end value;
          let id = b.b_f64_end / by + 1 in
          b.b_f64_end <- b.b_f64_end + by;
          id
      | Dir_multibyte mb ->
          extend_f64 1;
          d.dc_f64_data.{ b.b_f64_end } <- Int64.of_int b.b_char_end;
          let id = b.b_f64_end + 1 in
          b.b_f64_end <- b.b_f64_end + 1;
          let s_len = mb.mb_size value in
          let s = Bytes.create s_len in
          mb.mb_encode s 0 value;
          let l_len = Zencoding.LenPre3.encode_length s_len in
          let by = s_len + l_len in
          let char_data = extend_char by in
          Zencoding.LenPre3.encode char_data b.b_char_end l_len s_len;
          Netsys_mem.blit_string_to_memory
            s 0 char_data (b.b_char_end+l_len) s_len;
          b.b_char_end <- b.b_char_end + by;
          id
      | Dir_bigarray _
      | Inv_multibyte _
      | SInv_multibyte _
      | Virtual_column ->
          assert false


let build_end b =
  if not b.b_open then
    failwith "Zcontainer.Zdircol.build_end: already closed";
  b.b_open <- false;
  let dc = b.b_dc in
  let d = dc.dc_data in
  P_f64.resize_entry ~assert_last:true b.b_f64_pool d b.b_f64_end;
  P_f64.truncate b.b_f64_pool;
  P_f64.close b.b_f64_pool;
  ( match b.b_char_pool with
      | Some p ->
          P_char.resize_entry ~assert_last:true p d b.b_char_end;
          P_char.truncate p;
          P_char.close p
      | None ->
          ()
  );
  b.b_done dc;
  dc


let build_abort b =
  if b.b_open then (
    b.b_open <- false;
    P_f64.close b.b_f64_pool;
    ( match b.b_char_pool with
        | Some p ->
            P_char.close p
        | None ->
            ()
    );
    ( match b.b_fsname with
        | None -> ()
        | Some(dir,colname) -> remove ~force:true dir colname
    )
  )

let from_file dir dc_space dc_type =
  let openfile full_name f =
    let fd =
      Unix.openfile
        full_name
        [ Unix.O_RDONLY; Unix.O_CLOEXEC ]
        0o666 in
    try
      f fd
    with
      | error ->
          Unix.close fd;
          raise error in
  let ZcolDescr(col_name,_,repr,_) = dc_type in
  let name = Filename.concat dir (Zutil.FileUtil.encode_name col_name) in
  openfile
    (name ^ ".dir")
    (fun fd_f64 ->
       let dc_f64_data =
         Bigarray.Array1.map_file
           fd_f64 Bigarray.int64 Bigarray.c_layout false (-1) in
       let dc_char_data =
         match repr with
           | Dir_multibyte _ ->
               openfile
                 (name ^ ".ddt")
                 (fun fd ->
                    let m = 
                      Bigarray.Array1.map_file
                        fd Bigarray.char Bigarray.c_layout false (-1) in
                    Unix.close fd;
                    Some m
                 )
           | _ ->
               None in
       Unix.close fd_f64;
       let dc_data =
         { dc_f64_data; dc_char_data } in
       { dc_space; dc_type; dc_data; dc_vector = None }
    )

let size_of_bigarray_kind : type s t . (s,t) Bigarray.kind -> int =
  function
  | Bigarray.Char -> 1
  | Bigarray.Int8_signed -> 1
  | Bigarray.Int8_unsigned -> 1
  | Bigarray.Int16_signed -> 2
  | Bigarray.Int16_unsigned -> 2
  | Bigarray.Float32 -> 4
  | Bigarray.Int32 -> 4
  | Bigarray.Float64 -> 8
  | Bigarray.Int -> 8
  | Bigarray.Int64 -> 8
  | Bigarray.Nativeint -> 8
  | Bigarray.Complex32 -> 8
  | Bigarray.Complex64 -> 16

let ztype_of_bigarray_kind : type s t . (s,t) Bigarray.kind -> s ztype =
  function
  | Bigarray.Float32 -> Zfloat
  | Bigarray.Float64 -> Zfloat
  | Bigarray.Int8_signed -> Zint
  | Bigarray.Int8_unsigned -> Zint
  | Bigarray.Int16_signed -> Zint
  | Bigarray.Int16_unsigned -> Zint
  | Bigarray.Int -> Zint
  | Bigarray.Int64 -> Zint64
  | Bigarray.Int32 ->
      failwith "Zcontainer.Zdircol.from_vector: int32 \
                bigarrays not supported"
  | Bigarray.Nativeint ->
      failwith "Zcontainer.Zdircol.from_vector: nativeint \
                bigarrays not supported"
  | Bigarray.Complex32 -> 
      failwith "Zcontainer.Zdircol.from_vector: complex \
                bigarrays not supported"
  | Bigarray.Complex64 ->
      failwith "Zcontainer.Zdircol.from_vector: complex \
                bigarrays not supported"
  | Bigarray.Char ->
      failwith "Zcontainer.Zdircol.from_vector: character \
                bigarrays not supported"

let ord_of_ty_vec : type s . s ztype -> direction -> s zorder =
  fun ty dir ->
    match ty, dir with
      | Zfloat, ASC -> Zrepr.float_asc
      | Zfloat, DESC -> Zrepr.float_desc
      | Zint, ASC -> Zrepr.int_asc
      | Zint, DESC -> Zrepr.int_desc
      | Zint64, ASC -> Zrepr.int64_asc
      | Zint64, DESC -> Zrepr.int64_desc
      | _ -> assert false

let from_vector : type s t . _ -> _ ->
                       (s, t, Bigarray.fortran_layout) Bigarray.Array1.t ->
                       s zdircol =
  fun name direction vec ->
    let sp = Space(Bigarray.Array1.dim vec) in
    let kind = Bigarray.Array1.kind vec in
    let repr = Dir_bigarray kind in
    let ty = ztype_of_bigarray_kind kind in
    let ord = ord_of_ty_vec ty direction in
    let descr = ZcolDescr(name, ty, repr, ord) in
    let data =
      { dc_f64_data = Bigarray.Array1.create Bigarray.Int64 Bigarray.c_layout 0;
        dc_char_data = None;
      } in
    { dc_space = sp;
      dc_type = descr;
      dc_data = data;
      dc_vector = Some(ZdirVec vec)
    }

let storage_size dc =
  let d = dc.dc_data in
  ( match d.dc_char_data with
      | None -> 0
      | Some cd -> Bigarray.Array1.dim cd
  ) +
    8 * Bigarray.Array1.dim d.dc_f64_data +
    ( match dc.dc_vector with
        | None -> 0
        | Some(ZdirVec vec) ->
            let dim = Bigarray.Array1.dim vec in
            let kind = Bigarray.Array1.kind vec in
            dim * size_of_bigarray_kind kind

    )

let length dc =
  let ZcolDescr(_,_,repr,_) = dc.dc_type in
  match repr with
    | Dir_fixed64 f ->
        let elem_size = f.f64_size in
        Bigarray.Array1.dim dc.dc_data.dc_f64_data / elem_size
    | Dir_multibyte _ ->
        Bigarray.Array1.dim dc.dc_data.dc_f64_data
    | Dir_bigarray _ ->
        ( match dc.dc_vector with
            | None -> assert false
            | Some (ZdirVec vec) -> Bigarray.Array1.dim vec
        )
    | _ ->
        assert false

let value_of_id_unsafe_vector : type s t u . (s,t,u) Bigarray.Array1.t -> _ -> 
                                     s =
  fun vec ->
  (* Note that the compiler specializes the bigarray accesses
     when it knows the kind
   *)
    match Bigarray.Array1.kind vec with
      | Bigarray.Float32 ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Float64 ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Int8_signed ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Int8_unsigned ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Int16_signed ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Int16_unsigned ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Int32 ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Int ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Int64 ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Nativeint ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Complex32 ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Complex64 ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)
      | Bigarray.Char ->
          (fun id -> Bigarray.Array1.unsafe_get vec id)



let value_of_id_unsafe dc =
  let ZcolDescr(_,ty,repr,_) = dc.dc_type in
  match repr with
    | Dir_fixed64 f ->
        if f.f64_size = 1 then
          (fun id ->
             let k = id-1 in
             f.f64_decode dc.dc_data.dc_f64_data k
          )
        else
          (fun id ->
             let k = (id-1) * f.f64_size in
             f.f64_decode dc.dc_data.dc_f64_data k
          )
    | Dir_multibyte mb ->
        ( match dc.dc_data.dc_char_data with
            | Some char ->
                (fun id ->
                   let pos = dc.dc_data.dc_f64_data.{ id-1 } in
                   if pos < 0L || pos >= Int64.of_int(Bigarray.Array1.dim char)
                   then
                     failwith "Zcontainer.Zdircol.value_of_id: format errro";
                   let pos = Int64.to_int pos in
                   let l_len = Zencoding.LenPre3.decode_length char pos in
                   let s_len = Zencoding.LenPre3.decode char pos in
                   let s = Bytes.make s_len '\000' in
                   Netsys_mem.blit_memory_to_string char (pos+l_len) s 0 s_len;
                   mb.mb_decode s 0 s_len
                )
            | None ->
                (fun _ -> assert false)
        )
    | Dir_bigarray kind ->
        ( match dc.dc_vector with
            | None -> assert false
            | Some(ZdirVec vec) ->
                let vec_kind = Bigarray.Array1.kind vec in
                let vec_ty = ztype_of_bigarray_kind vec_kind in
                ( match same_ztype ty vec_ty with
                    | Equal ->
                        value_of_id_unsafe_vector vec
                    | Not_equal ->
                        assert false
                )
        )
    | _ ->
        (fun _ -> assert false)

let float_of_id_unsafe dc id =
  (* assumes stdrepr for floats *)
  Int64.float_of_bits (Bigarray.Array1.unsafe_get dc.dc_data.dc_f64_data (id-1))


let value_of_id dc =
  let get = value_of_id_unsafe dc in
  let Space sp = dc.dc_space in
  let len = length dc in
  (fun id ->
     if id < 1 || id > len then (
       if id > len && id <= sp then
         raise Not_found
       else
         invalid_arg "Zcontainer.Zdircol.value_of_id"
     );
     get id
  )

let value_array_of_zset : type t . t zdircol -> _ -> t array =
  fun dc zset ->
    if not (Zset.space_less_or_equal_than zset dc.dc_space) then
      failwith "Zcontainer.Zdircol.value_array_of_zset: \
                set exceeds space of column";
    if Zset.is_empty zset then
      [| |]
    else
      let x0 = value_of_id dc (Zset.first zset) in
      let va = Array.make (Zset.count zset) x0 in
      let k = ref 0 in
      let ZcolDescr(_,ty,repr,_) = dc.dc_type in
      match ty with
        | Zfloat when Zrepr.is_std_float_f64 repr ->
            Zset.iter
              (fun start len ->
                 for id = start to start+len-1 do
                   va.( !k ) <- float_of_id_unsafe dc id;
                   incr k
                 done
              )
              zset;
            va
        | _ ->
            let get = value_of_id dc in
            Zset.iter
              (fun start len ->
                 for id = start to start+len-1 do
                   va.( !k ) <- get id;
                   incr k
                 done
              )
              zset;
            va

let iter_set dc f zset =
  let get = value_of_id_unsafe dc in
  Zset.iter
    (fun start len ->
       for id = start to start+len-1 do
         let x = get id in
         f id x
       done
    )
    zset

        
let merge b inputs =
  if b.b_f64_end > 0 then
    failwith "Zcontainer.Zdircol.merge: builder is not empty";
  if not b.b_open then
    failwith "Zcontainer.Zdircol.merge: already closed";
  let Space sp = b.b_dc.dc_space in
  let total = ref 0 in
  Array.iter
    (fun input ->
       let len = Bigarray.Array1.dim input.dc_data.dc_f64_data in
       total := !total + len;
       if not (Ztypes.same_descr b.b_dc.dc_type input.dc_type) then
         failwith "Zcontainer.Zdircol.merge: incompatible types";
    )
    inputs;
  if !total > sp then
    failwith "Zcontainer.Zdircol.merge: insufficient space";
  let dc = b.b_dc in
  let d = dc.dc_data in
  Array.iter
    (fun input ->
       let f64_offs = b.b_f64_end in
       let char_offs = Int64.of_int b.b_char_end in
       let f64_len = Bigarray.Array1.dim input.dc_data.dc_f64_data in
       if b.b_f64_end + f64_len > Bigarray.Array1.dim d.dc_f64_data then (
         let new_size = max (2 * b.b_f64_end) (b.b_f64_end + f64_len) in
         P_f64.resize_entry ~assert_last:true b.b_f64_pool d new_size;
         assert(b.b_f64_end+f64_len <= Bigarray.Array1.dim d.dc_f64_data);
       );
       Bigarray.Array1.blit
         input.dc_data.dc_f64_data
         (Bigarray.Array1.sub d.dc_f64_data b.b_f64_end f64_len);
       b.b_f64_end <- b.b_f64_end + f64_len;
       ( match d.dc_char_data, input.dc_data.dc_char_data with
           | Some dest_data, Some src_data ->
               let char_len = Bigarray.Array1.dim src_data in
               let dest_data =
                 if b.b_char_end + char_len > Bigarray.Array1.dim dest_data then (
                   match b.b_char_pool with
                     | Some pool ->
                         let new_size = 
                           max (2 * b.b_char_end) (b.b_char_end + char_len) in
                         P_char.resize_entry ~assert_last:true pool d new_size;
                         (match d.dc_char_data with
                            | Some dest_data ->
                                assert (b.b_char_end+char_len <=
                                          Bigarray.Array1.dim dest_data);
                                dest_data
                            | None ->
                                assert false
                         )
                   | None ->
                       assert false
               ) else
                   dest_data in
               Bigarray.Array1.blit
                 src_data
                 (Bigarray.Array1.sub dest_data b.b_char_end char_len);
               b.b_char_end <- b.b_char_end + char_len;
               for k = f64_offs to f64_offs + f64_len - 1 do
                 d.dc_f64_data.{ k } <- Int64.add d.dc_f64_data.{ k } char_offs
               done
           | None, None ->
               ()
           | _ ->
               assert false
       );
    )
    inputs


class type ['a] ziterator =
  object
    method current_pos : int
    method current_value : int * 'a
    method current_value_lz : (int * 'a) Lazy.t
    method current_zset : Zset.pool -> Zset.zset
    method current_zset_lz : Zset.pool -> Zset.zset Lazy.t
    method beyond : bool
    method next : unit -> unit
    method prev : unit -> unit
    method go_beginning : unit -> unit
    method go_end : unit -> unit
    method go : int -> unit
    method find : comparison -> 'a -> unit
    method length : int
  end


type ranges =
    (* Same as Zset.ranges, maybe move these functions there? *)
    { mutable rstart : int array;
      mutable rlen : int array;
      mutable rnum : int;
    }


let new_ranges() =
  { rstart = Array.make 1 0;
    rlen = Array.make 1 0;
    rnum = 0
  }

let ranges_add r id =
  let n = r.rnum in
  if n > 0 then (
    let e = r.rstart.(n-1) + r.rlen.(n-1) in
    if e = id then
      r.rlen.(n-1) <- r.rlen.(n-1) + 1
    else (
      if n >= Array.length r.rstart then (
        let new_start = Array.make (2*n) 0 in
        Array.blit r.rstart 0 new_start 0 n;
        let new_len = Array.make (2*n) 0 in
        Array.blit r.rlen 0 new_len 0 n;
        r.rstart <- new_start;
        r.rlen <- new_len
      );
      r.rstart.(n) <- id;
      r.rlen.(n) <- 1;
      r.rnum <- n+1
    )
  )
  else (
    r.rstart.(0) <- id;
    r.rlen.(0) <- 1;
    r.rnum <- 1
  )


class type ['a] accumulator =
  object
    method add : int -> 'a -> unit
    method finish : unit -> 'a ziterator
  end


let accumulator : type s . s zcoldescr ->  space -> s accumulator =
  fun descr space ->
    let ZcolDescr(_,ty,_,zo) = descr in
    let module V = struct
      type t = int * s
      let hash (h,_) = Hashtbl.hash h
      let equal (h1,x1 : t) (h2,x2) =
        h1 = h2 && zo.ord_cmp x1 x2 = 0
      let compare (h1,x1 : t) (h2,x2) =
        if h1 = h2 then
          zo.ord_cmp x1 x2
        else
          Pervasives.compare h1 h2
    end in
    let module H = Hashtbl.Make(V) in
    let module OU = Zutil.OrdUtil(V) in
    let h1 = H.create 432 in   (* maps value to ranges *)
    let h2 = H.create 432 in   (* maps value to first ID (for saving mem) *)
    let x0 = ref None in

    ( object
        method add id x =
          let v = (zo.ord_hash x, x) in
          try
            let r = H.find h1 v in
            ranges_add r id
          with Not_found ->
            try
              let old_id = H.find h2 v in
              let r = new_ranges() in
              ranges_add r old_id;
              ranges_add r id;
              H.add h1 v r;
            with Not_found ->
              H.add h2 v id;
              if !x0 = None then
                x0 := Some v

        method finish() =
          let len = H.length h2 in
          let keys =
            match !x0 with
              | None -> [| |]
              | Some x -> Array.make len x in
          let i = ref 0 in
          H.iter
            (fun x _ ->
               keys.( !i ) <- x;
               incr i
            )
            h2;
          Array.sort V.compare keys;
      (*
      for p = 0 to Array.length keys - 1 do
      print_endline (debug_string_of_value ty (snd keys.(p)))
      done;
       *)
          let cur = ref 0 in
          ( object
              method current_pos = !cur
              method current_value =
                if !cur >= len then raise End_of_column;
                keys.( !cur )
              method current_value_lz =
                let cur = !cur in
                lazy
                  ( if cur >= len then raise End_of_column;
                    keys.( cur )
                  )
              method current_zset pool =
                if !cur >= len then raise End_of_column;
                let v = keys.( !cur ) in
                try
                  let r = H.find h1 v in
                  let b = Zset.build_begin pool (4 * r.rnum) space in
                  for i = 0 to r.rnum - 1 do
                    Zset.build_add_range b r.rstart.(i) r.rlen.(i)
                  done;
                  Zset.build_end b
                with
                  | Not_found ->
                      let id = H.find h2 v in
                      Zset.Zs_one(space,id)
              method current_zset_lz pool =
                let cur = !cur in
                lazy
                  ( if cur >= len then raise End_of_column;
                    let v = keys.( cur ) in
                    try
                      let r = H.find h1 v in
                      let b = Zset.build_begin pool (4 * r.rnum) space in
                      for i = 0 to r.rnum - 1 do
                        Zset.build_add_range b r.rstart.(i) r.rlen.(i)
                      done;
                      Zset.build_end b
                    with
                      | Not_found ->
                          let id = H.find h2 v in
                          Zset.Zs_one(space,id)
                  )
              method beyond =
                !cur < 0 || !cur >= len
              method next() =
                incr cur
              method prev() =
                decr cur
              method go_beginning() =
                cur := 0
              method go_end() =
                cur := len - 1
              method go k =
                cur := k
              method find comp x =
                cur :=
                  OU.binary_search
                    (fun p -> keys.(p))
                    len
                    comp
                    (zo.ord_hash x, x)
              method length =
                len
            end
          )
      end
    )


let iter_1 : type s . s zdircol -> ((int -> int -> unit) -> unit) ->
                    s ziterator =
  fun col base_iter ->
  let acc = accumulator col.dc_type col.dc_space in
  let add = acc#add in
  let get = value_of_id col in
  ( try
      base_iter
        (fun start len ->
           for id = start to start+len-1 do
             let x = get id in
             add id x
           done
        )
    with
      | Not_found ->
          (* raised by value_of_id so far an ID is found outside the
             column but inside the space. We iterate in ascending order
             of IDs, so any further ID will also raise an exception.
           *)
          ()
  );
  acc#finish()


let iter col base =
  if not (Zset.space_less_or_equal_than base col.dc_space) then
    failwith "Zcontainer.Zdircol.iter: incompatible spaces";
  iter_1 col (fun f -> Zset.iter f base)

let iter_all col =
  let Space sp = col.dc_space in
  iter_1 col (fun f -> f 1 sp)

let iter_union col bases =
  let compatible =
    List.for_all
      (fun base -> Zset.space_less_or_equal_than base col.dc_space) bases in
  if not compatible then
    failwith "Zcontainer.Zdircol.iter_union: incompatible spaces";
  (* TODO: this can be implemented without storing the union *)
  let pool = Zset.create_pool 1 1000 in
  let union =
    match bases with
      | [] -> Zset.empty
      | [base] -> base
      | _ -> Zset.union_many pool bases in
  iter_1 col (fun f -> Zset.iter f union)

let space col = col.dc_space
let descr col = col.dc_type
                  
