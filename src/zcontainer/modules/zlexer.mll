{
  type token =
    | Word of string
    | Lparen
    | Rparen
    | Asterisk
    | End
}

rule type_token =
  parse
  | [ 'A'-'Z' 'a'-'z' '0'-'9' '_' ]+ as tok { Word tok }
  | "(" { Lparen }
  | ")" { Rparen }
  | "*" { Asterisk }
  | [ ' ' '\t' '\r' '\n' '~' ]+ { type_token lexbuf }
  | eof { End }
  | _ { failwith "lexer error" }
