(* Digester *)

open Ztypes
open Printf

type 'a parserfun =
    string -> 'a

type parserbox =
  | ParserBox : 'a ztype * 'a parserfun -> parserbox
  | NoParserBox : 'a Ztypes.ztype -> parserbox

type foodbox =
  | StringBox : string -> foodbox
  | ValueBox : 'a Ztypes.ztype * 'a -> foodbox
                                         
type col_stage =
  (* that's for a single column *)
  { cs_add : string -> unit;
    cs_add_food : foodbox -> unit;
    cs_finish : unit -> unit;
    cs_abort : unit -> unit;
    cs_time : float ref
  }

type stage =
    { s_tab : Ztable.ztable;
      mutable s_add : string array -> unit;
      mutable s_add_food : foodbox array -> unit;
      mutable s_finish : unit -> unit;
      mutable s_abort : unit -> unit;
      s_num : int ref;
    }

let tybox_of_parserbox =
  function
  | ParserBox(ty,_) -> ZtypeBox ty
  | NoParserBox ty -> ZtypeBox ty


let to_dir_repr stdrepr repr_name =
  let zreprbox = zreprbox_of_string stdrepr repr_name in
  let ZreprBox(ty, repr) = zreprbox in
  match repr with
    | Inv_multibyte mb
    | SInv_multibyte(mb,_,_) ->
        let repr_name' = string_of_zrepr(Dir_multibyte mb) in
        (true, repr_name')
    | _ ->
        (false, repr_name)


let invert zt pool name ty repr_name zo_name zdircol =
  let t0 = Unix.gettimeofday() in
  let itr = Zdircol.iter_all zdircol in
  let t1 = Unix.gettimeofday() in
  if !Zutil.debug then
    eprintf "[DEBUG] invert1 name=%s time=%.3fs\n%!" name (t1-.t0);
  let b = Ztable.build_zinvcol zt name ty repr_name zo_name in
  try
    while not itr#beyond do
      let (h,v) = itr#current_value in
      let zset = itr#current_zset pool in
      Zinvcol.build_add b v zset;
      itr # next();
    done;
    ignore(Zinvcol.build_end b);
    let t2 = Unix.gettimeofday() in
    if !Zutil.debug then
      eprintf "[DEBUG] invert2 name=%s time=%.3fs\n%!" name (t2-.t1)
  with
    | error ->
        Zinvcol.build_abort b;
        raise error

let bundle_fixup : type s . s ztype -> s -> s =
  fun ty ->
  match ty with
    | Zbundle ty1 ->
        let default = Ztypes.null_value ty1 in
        (fun vals1 ->
          let vals2 =
            List.filter (fun (idx,v) -> v <> default) vals1 in
          let vals3 =
            List.sort (fun (idx1,_) (idx2,_) -> compare idx1 idx2) vals2 in
          vals3
        )
    | _ ->
        (fun x -> x)


let create_col_stage pool zt (name,pbox,repr_name,zo_name) =
  let space = Ztable.(zt.zt_space) in
  let stdrepr = Ztable.(zt.zt_stdrepr) in
  let is_inv, dir_repr_name = to_dir_repr stdrepr repr_name in
  let time = ref 0.0 in
  let start ty =
    if is_inv then
      Zdircol.create_memory_zdircol_builder_1
        space stdrepr name ty dir_repr_name zo_name
    else
      Ztable.build_zdircol zt name ty dir_repr_name zo_name in
  let add b fixup parse s =
    let t0 =
      if !Zutil.debug then Unix.gettimeofday() else 0.0 in
    ignore(Zdircol.build_add b (fixup (parse s)));
    if !Zutil.debug then (
      let t1 = Unix.gettimeofday() in
      time := !time +. (t1 -. t0)
    ) in
  let add_food : type t . t Zdircol.zdircol_builder -> t ztype ->
                      (t -> t) ->
                      (string -> t) option -> _ -> unit =
    fun b ty fixup parse_opt food ->
      let t0 =
        if !Zutil.debug then Unix.gettimeofday() else 0.0 in
      ( match food with
          | StringBox s ->
              ( match parse_opt with
                  | None ->
                      failwith ("Zcontainer.Zdigester.build_add_food: \
                                 no parser defined for column: " ^ name)
                  | Some parse ->
                      ignore(Zdircol.build_add b (fixup (parse s)))
              )
          | ValueBox(foodty, foodval) ->
              ( match same_ztype ty foodty with
                  | Equal ->
                      ignore(Zdircol.build_add b (fixup foodval))
                  | Not_equal ->
                      failwith ("Zcontainer.Zdigester.build_add_food: \
                                 value is not of the expected type: " ^ name)
              )
      );
      if !Zutil.debug then (
        let t1 = Unix.gettimeofday() in
        time := !time +. (t1 -. t0)
      ) in
  let finish b ty () =
    let zdircol = Zdircol.build_end b in
    if !Zutil.debug then
      eprintf "[DEBUG] build name=%s time=%.3f\n%!" name !time;
    if is_inv then (
      invert zt pool name ty repr_name zo_name zdircol
    ) in
  let abort b () =
    Zdircol.build_abort b in
  match pbox with
    | ParserBox(ty,parse) ->
        let b = start ty in
        let fixup = bundle_fixup ty in
        { cs_add = add b fixup parse;
          cs_add_food = add_food b ty fixup (Some parse);
          cs_finish = finish b ty;
          cs_abort = abort b;
          cs_time = time
        }
    | NoParserBox ty ->
        let b = start ty in
        let fixup = bundle_fixup ty in
        { cs_add = (fun _ ->
                       failwith ("Zcontainer.Zdigester.build_add_food: \
                                  no parser defined for column: " ^ name)
                    );
          cs_add_food = add_food b ty fixup None;
          cs_finish = finish b ty;
          cs_abort = abort b;
          cs_time = time
        }

let create_col_stages zt cols =
  let pool = Zset.create_pool 1000 1000 in
  Array.map
    (fun colspec ->
       create_col_stage pool zt colspec
    )
    cols


let create_stage zt cols =
  let n = Array.length cols in
  let cstages = create_col_stages zt cols in
  let s_num = ref 0 in
  let s_add row =
    if Array.length row <> n then
      failwith "Zdigester: row has unexpected number of columns";
    Array.iteri
      (fun i cs ->
         let s = row.(i) in
         ignore(cs.cs_add s)
      )
      cstages;
    incr s_num in
  let s_add_food row =
    if Array.length row <> n then
      failwith "Zdigester: row has unexpected number of columns";
    Array.iteri
      (fun i cs ->
         let food = row.(i) in
         ignore(cs.cs_add_food food)
      )
      cstages;
    incr s_num in
  let s_finish() =
    Array.iter
      (fun cs -> cs.cs_finish())
      cstages in
  let s_abort() =
    Array.iter
      (fun cs -> cs.cs_abort())
      cstages in
  { s_tab = zt;
    s_add;
    s_add_food;
    s_finish;
    s_abort;
    s_num
  }

let stage_inactivate stage =
  stage.s_add <- (fun _ -> failwith "Zcontainer.Zdigester: inactive");
  stage.s_add_food <- (fun _ -> failwith "Zcontainer.Zdigester: inactive");
  stage.s_finish <- (fun _ -> failwith "Zcontainer.Zdigester: inactive");
  stage.s_abort <- (fun () -> ())

let stage_add stage row =
  stage.s_add row

let stage_add_food stage row =
  stage.s_add_food row

let stage_finish stage =
  stage.s_finish();
  stage_inactivate stage

let stage_abort stage =
  stage.s_abort();
  stage_inactivate stage

type zdigester =
    { dir : string option;
      stdrepr : stdrepr;
      cols : (string * parserbox * string * string) array;
      max_rows : int;
      mutable current : stage;
      mutable index : int;
      mutable tables : Ztable.ztable list;
      mutable total_rows : int;
    }

let create_stage_for_digester stdrepr space cols dir_opt index =
  match dir_opt with
    | None ->
        let zt = Ztable.create_memory_ztable stdrepr space in
        create_stage zt cols
    | Some dir ->
        let parent = Filename.dirname dir in
        let stage_dir = Filename.concat parent "tmp" in
        let zt_name = sprintf "%s_%06d" (Filename.basename dir) index in
        ( try Unix.mkdir stage_dir 0o777
          with Unix.Unix_error(Unix.EEXIST,_,_) -> ()
        );
        let zt_dir =
          Filename.concat stage_dir zt_name in
        let zt = Ztable.create_file_ztable stdrepr space zt_dir in
        create_stage zt cols

let next_stage dg =
  stage_finish dg.current;
  dg.tables <- dg.current.s_tab :: dg.tables;
  dg.index <- dg.index + 1;
  dg.total_rows <- dg.total_rows + !(dg.current.s_num);
  let space = Space dg.max_rows in
  let stage =
    create_stage_for_digester dg.stdrepr space dg.cols dg.dir dg.index in
  dg.current <- stage

let build_add dg row =
  let stage = dg.current in
  stage.s_add row;
  if !(stage.s_num) >= dg.max_rows then
    next_stage dg

let build_add_food dg row =
  let stage = dg.current in
  stage.s_add_food row;
  if !(stage.s_num) >= dg.max_rows then
    next_stage dg

let build_abort dg =
  stage_abort dg.current;
  Ztable.remove ~force:true dg.current.s_tab;
  List.iter (Ztable.remove ~force:true) dg.tables;
  dg.tables <- []

let build_end dg =
  stage_finish dg.current;
  dg.tables <- dg.current.s_tab :: dg.tables;
  dg.total_rows <- dg.total_rows + !(dg.current.s_num);
  if dg.total_rows = 0 then (
    build_abort dg;
    failwith "Zcontainer.Zdigester.build_end: nothing to digest";
  );
  let space = Space dg.total_rows in
  let zt =
    match dg.dir with
      | None ->
          Ztable.create_memory_ztable dg.stdrepr space
      | Some dir ->
          Ztable.create_file_ztable dg.stdrepr space dir in
  let cols' =
    Array.map
      (fun (name,pbox,repr_name,zo_name) ->
         let tybox = tybox_of_parserbox pbox in
         (name,tybox,repr_name,zo_name)
      )
      dg.cols in
  Ztable.merge zt (Array.to_list cols') (Array.of_list (List.rev dg.tables));
  List.iter (Ztable.remove ~force:true) dg.tables;
  dg.tables <- [];
  zt

let create_memory_zdigester ?(max_stage_rows = 1_000_000) stdrepr cols =
  let space = Space max_stage_rows in
  let current =
    create_stage_for_digester stdrepr space cols None 0 in
  { dir = None;
    stdrepr;
    cols;
    max_rows = max_stage_rows;
    current;
    index = 0;
    tables = [];
    total_rows = 0;
  }


let create_file_zdigester ?(max_stage_rows = 1_000_000) stdrepr cols dir =
  let space = Space max_stage_rows in
  let current =
    create_stage_for_digester stdrepr space cols (Some dir) 0 in
  { dir = Some dir;
    stdrepr;
    cols;
    max_rows = max_stage_rows;
    current;
    index = 0;
    tables = [];
    total_rows = 0;
  }

let rec parser_of_type : type t . t ztype -> string -> t parserfun =
  fun zt anon_value ->
  match zt with
  | Zstring ->
      (fun s -> s)
  | Zint64 ->
      Int64.of_string
  | Zint ->
      int_of_string
  | Zfloat ->
      float_of_string
  | Zbool ->
      (function
       | "0" | "f" | "F" | "false" | "FALSE" | "" -> false
       | "1" | "t" | "T" | "true" | "TRUE" -> true
       | s ->
           failwith ("Not a boolean value: " ^ s)
      )
  | Zoption zt ->
      let sub_parser = parser_of_type zt anon_value in
      (fun s -> if s=anon_value then None else Some(sub_parser s))
  | Zpair(zt1,zt2) ->
      let sub_parser1 = parser_of_type zt1 anon_value in
      let sub_parser2 = parser_of_type zt2 anon_value in
      (fun s ->
         try
           let k = String.index s ',' in
           (sub_parser1 (String.sub s 0 k),
            sub_parser2 (String.sub s (k+1) (String.length s - k - 1))
           )
         with
           | Not_found ->
               failwith "Cannot parse pair: no comma"
      )
  | Zlist zt1 ->
      let sub_parser1 = parser_of_type zt1 anon_value in
      let rec loop s i bracket =
        try
          let k = String.index_from s i ';' in
          sub_parser1 (String.sub s i (k-i)) :: loop s (k+1) bracket
        with
          | Not_found ->
              let l = String.length s in
              let m = if s.[l-1] = ']' then l-i-1 else l-i in
              [ sub_parser1 (String.sub s i m) ] in
      (fun s ->
        if s = "" || s = "[]" then [] else
          if s.[0] = '[' then
            loop s 1 true
          else
            loop s 0 false
      )
  | Zbundle zt1 ->
      parser_of_type (Zlist(Zpair(Zint,zt1))) anon_value

