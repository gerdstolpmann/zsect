open Ztypes
open Printf

let float_pair = Zpair(Zfloat,Zfloat)
let mb_repr_float_pair =
  Zrepr.pair_mb Zfloat Zrepr.float_mb Zfloat Zrepr.float_mb
let zo_float_pair =
  Zrepr.pair_ord Zfloat Zrepr.float_asc Zfloat Zrepr.float_asc


let equal_width_histogram ~ztable ~name ~offset ~width incol =
  let space = Ztable.space ztable in
  let Space n = space in
  let descr = Ztypes.ZcolDescr("histogram", float_pair,
                               Dir_multibyte mb_repr_float_pair,
                               zo_float_pair) in
  let acc = Zdircol.accumulator descr space in
  let add = acc#add in
  let value_of_id = Zcol.value_of_id incol in
  for id = 1 to n do
    let v = value_of_id id in
    let from = (floor ((v -. offset) /. width)) *. width +. offset in
    let cat = (from, from +. width) in
    add id cat
  done;
  let itr = acc # finish() in
  let pool = Zset.create_pool 1000 1000 in
  let binv =
    Ztable.build_zinvcol ztable name float_pair "IM/float*float" "asc" in
  ignore(Zinvcol.build_from_iterator binv pool itr);
  Ztable.get_zcol ztable float_pair name


let intlog x =
  (* floor(log10 x) *)
  let rec pos_intlog c k =
    if x < c then
      k
    else
      pos_intlog (10.0 *. c) (k+1) in
  let rec neg_intlog c k =
    if x >= c then
      k
    else
      neg_intlog (0.1 *. c) (k-1) in
  if x = 1.0 then
    0
  else if x > 1.0 then
    pos_intlog 10.0 0
  else
    neg_intlog 0.1 (-1)


let exp_table logmin logmax factor =
  (* returns table such that table.(k-logmin*factor) = 10^(k/factor) *)
  assert(logmin <= logmax);
  assert(factor >= 1);
  let n = (logmax-logmin) * factor + 1 in
  let tab = Array.make n 0.0 in
  let offset = logmin*factor in
  let r = 10.0 ** (1.0 /. float factor) in
  let p = ref 1.0 in
  for j = 0 to logmax-1 do
    if j >= logmin then (
      let q = ref !p in
      for i = 0 to factor-1 do
        tab.(j*factor - offset + i) <- !q;
        q := !q *. r
      done
    );
    p := 10.0 *. !p
  done;
  tab.(logmax*factor - offset) <- !p;
  p := 1.0;
  for j = 0 downto logmin do
    if j < logmax then (
      let q = ref !p in
      for i = 0 to factor-1 do
        tab.(j*factor - offset + i) <- !q;
        q := !q *. r
      done
    );
    p := 0.1 *. !p
  done;
  tab

let add_offset tab offset =
  for k = 0 to Array.length tab - 1 do
    tab.(k) <- tab.(k) +. offset
  done


module F =
  struct type t = float let compare (x:float) (y:float) = compare x y end
module FU = Zutil.OrdUtil(F)

let dec_const = log 10.0 /. log 2.0


let logarithmic_histogram ~ztable ~name ~offset ~factor incol =
  let space = Ztable.space ztable in
  let Space n = space in
  let descr = Ztypes.ZcolDescr("histogram", float_pair,
                               Dir_multibyte mb_repr_float_pair,
                               zo_float_pair) in
  let pool = Zset.create_pool 1000 1000 in
  let acc = Zdircol.accumulator descr space in
  let add = acc#add in
  let value_of_id = Zcol.value_of_id incol in
  if n > 0 then (
    let all = Zset.all pool space in
    let minval =
      Zaggreg.fold
        (fun id x m ->
           let xo = x +. offset in
           match classify_float xo with
             | FP_normal | FP_subnormal -> if xo > 0.0 then min xo m else m
             | _ -> m
        )
        incol
        all
        infinity in
    let minlog = intlog minval in
    let maxval =
      Zaggreg.fold
        (fun id x m ->
           let xo = x +. offset in
           match classify_float xo with
             | FP_normal | FP_subnormal -> if xo > 0.0 then max xo m else m
             | _ -> m
        )
        incol
        all 
        neg_infinity in
    let maxlog = intlog maxval + 1 in
    let tab = exp_table minlog maxlog factor in
    let tab_offset = minlog * factor in
    let tab_len = Array.length tab in
    for id = 1 to n do
      let x = value_of_id id in
      let xo = x +. offset in
      let cat =
        match classify_float xo with
          | FP_normal
          | FP_subnormal ->
              if xo > 0.0 then
                (* roughly guess where to search in tab: *)
                let _, exp2 = frexp xo in
                let exp10 = int_of_float (floor (float exp2 /. dec_const)) in
                let k_min = (exp10-1)*factor - tab_offset in
                let k_min = max 0 k_min in
                let k_max = (exp10+2)*factor - tab_offset in
                let k_max = min tab_len k_max in
                (* now we know table.(k_min) < xo < table.(k_max-1) *)
                let get k = tab.(k + k_min) in
                let len = k_max - k_min in
                match FU.binary_search get len LE xo with
                  | k ->
                      (tab.(k+k_min) -. offset, tab.(k+k_min+1) -. offset)
                  | exception Not_found ->
                      eprintf "[DEBUG] x=%e xo=%e exp2=%d exp10=%d k_min=%d k_max=%d\n%!"
                              x xo exp2 exp10 k_min k_max;
                      eprintf "[DEBUG] minval=%e minlog=%d maxval=%e maxlog=%d\n"
                              minval minlog maxval maxlog;
                      eprintf "[DEBUG] tab_offset=%d tab_len=%d tab.(0)=%e tab.(max)=%e\n"
                              tab_offset tab_len tab.(0) tab.(tab_len-1);
                      assert false
              else
                (neg_infinity, -.offset)
          | _ ->
              (neg_infinity, -.offset) in
      add id cat
    done;
  );
  let itr = acc # finish() in
  let binv =
    Ztable.build_zinvcol ztable name float_pair "IM/float*float" "asc" in
  ignore(Zinvcol.build_from_iterator binv pool itr);
  Ztable.get_zcol ztable float_pair name


module Diff = struct
  type t = int * float * float * float
  let compare (x1,y1,z1,u1) (x2,y2,z2,u2) = Pervasives.compare y1 y2
end

(* TODO:
   - support a base set
   - support a conversion from other types to float. Exception to skip IDs.
 *)

let second_smallest vector smallest =
  let n = Array.length vector in
  let i = ref 0 in
  while !i < n && snd(vector.( !i )) <= smallest &&
          (!i+1 >= n || snd(vector.( !i+1 )) < infinity)
    do incr i done;
  if !i < n then
    snd(vector.( !i ))
  else
    smallest


let second_biggest vector biggest =
  let n = Array.length vector in
  let i = ref (n-1) in
  while !i >= 0 && snd(vector.( !i )) >= biggest && 
          (!i = 0 || snd(vector.( !i-1 )) > neg_infinity)
    do decr i done;
  if !i >= 0 then
    snd(vector.( !i ))
  else
    biggest


let rec next_float x d =
  if x = x +. d then
    next_float x (2.0 *. d)
  else
    x +. d


let rec prev_float x d =
  if x = x -. d then
    prev_float x (2.0 *. d)
  else
    x -. d


let maxdiff_histogram ?(f_diff = fun x -> x) ~ztable ~name ~number incol =
  let space = Ztable.space ztable in
  let Space n = space in
  if n < 1 then
    failwith "Zcontainer.Zdiscretize.maxdiff_histogram: vector too short";
  (* Get the whole vector and sort it *)
  let vector = Array.make n (0, 0.0) in
  let value_of_id = Zcol.value_of_id incol in
  for id = 1 to n do
    let v = value_of_id id in
    vector.(id-1) <- (id, v)
  done;
  Array.sort (fun (_,v1) (_,v2) -> Pervasives.compare v1 v2) vector;
  let _, smallest0 = vector.(0) in
  let smallest =
    if smallest0 > neg_infinity then
      smallest0
    else
      second_smallest vector smallest0 in
  let snd_smallest = second_smallest vector smallest in
  let _, biggest0 = vector.(n-1) in
  let biggest =
    if biggest0 < infinity then
      biggest0
    else
      second_biggest vector biggest0 in
  let snd_biggest = second_biggest vector biggest in
(*
  eprintf "smallest: %f\n" smallest;
  eprintf "snd_smallest: %f\n" snd_smallest;
  eprintf "biggest: %f\n" biggest;
  eprintf "snd_biggest: %f\n" snd_biggest;
 *)
  (* Get the [number] biggest differences. Using a heap for managing that *)
  let module U = Zutil.OrdUtil(Diff) in   (* compare by (_,d,_,_) *)
  let heap = U.heap_create number (0, 0.0, 0.0, 0.0) in
  if n >= 2 then (
    let id_at_0, v_at_0 = vector.(0) in
    let id_at_1, v_at_1 = vector.(1) in
    let d0 = v_at_1 -. v_at_0 in
    let fd0 = f_diff v_at_1 -. f_diff v_at_0 in
    if d0 < infinity then
      U.heap_insert heap (id_at_0, fd0, d0, v_at_0);
    for k = 2 to Array.length vector - 1 do
      let id_curr, v_curr = vector.(k-1) in
      let id_next, v_next = vector.(k) in
      let d = v_next -. v_curr in
      let fd = f_diff v_next -. f_diff v_curr in
      let do_insert =
        d < infinity &&
          (U.heap_length heap < number-1 ||
             let (_, fd_min, _, _) = U.heap_min heap in
             compare fd fd_min > 0
          ) in
      let do_delete =
        do_insert && U.heap_length heap = number-1 in
      if do_delete then
        U.heap_delete_min heap;
      if do_insert then
        U.heap_insert heap (id_curr, fd, d, v_curr);
    done;
  );
  let a1 = U.heap_sort heap in
(*
  eprintf "HEAP:\n";
  Array.iter (fun (id, d, v) -> eprintf "(%d,%f,%f)\n" id d v) a1;
  eprintf "HEAP END\n%!";
 *)
  (* Skip over the beginning where the difference is 0.0 (or so close
     that we cannot construct a good boundary)
   *)
  let p = ref 0 in
  let q = Array.length a1 in
  while !p < q  && let (_,_,d,v) = a1.(!p) in v +. 0.25 *. d <= v do
    incr p
  done;
  let p = !p in
  (* only the array part from a1.(p) to a1.(q-1) is relevant.
     This slice may be empty
   *)
  (* Get the histogram array a2 *)
  let extra =
    if smallest = infinity || biggest = neg_infinity then
      [ neg_infinity; infinity ]
    else 
      let x0_m = smallest -. 0.25 *. (snd_smallest -. smallest) in
      let x0   = smallest -. 0.5 *. (snd_smallest -. smallest) in
      let x1_m = biggest +. 0.25 *. (biggest -. snd_biggest) in
      let x1   = biggest +. 0.5 *. (biggest -. snd_biggest) in
      [ (if x0_m < smallest then
           x0
         else   (* smallest and snd_smallest are too close *)
           prev_float smallest 1.0
        );
        ( if x1_m > biggest then
            x1
          else  (* biggest and snd_biggest are too close *)
            next_float biggest 1.0
        );
      ] @
        (if smallest0 <= neg_infinity then [neg_infinity] else []) @
          (if biggest0 >= infinity then [infinity] else []) in
  let n2 = q - p + List.length extra in
  let a2 = Array.make n2 0.0 in
  for k = p to q-1 do
    let _, _, d, x0 = a1.(k) in
    a2.(k-p) <- x0 +. 0.5 *. d
  done;
  List.iteri (fun i x -> a2.(q-p+i) <- x) extra;
  Array.sort Pervasives.compare a2;
(*
  eprintf "a2:\n";
  Array.iter (eprintf "%.20f\n") a2;
  eprintf "END a2\n%!";
 *)
  (* a2 contains now the histogram boundaries in asc order *)
  let binv =
    Ztable.build_zinvcol ztable name float_pair "IM/float*float" "asc" in
  let pool = Zset.create_pool 1000 1000 in
  let acc = ref [] in
  let save x =
    if !acc <> [] then (
      let b = Zset.build_begin pool 1000 space in
      List.iter
        (Zset.build_add b)
        (List.sort Pervasives.compare !acc);
      Zinvcol.build_add binv x (Zset.build_end b);
      acc := []
    ) in
  let slim = if a2.(n2-1) = infinity then n2-1 else n2 in
  ( try
      let s = ref 1 in
      for k = 0 to n-1 do
        let id, v = vector.(k) in
        while !s < slim && a2.( !s ) <= v do
          let x0 = a2.( !s-1 ) in
          let x1 = a2.( !s ) in
          save (x0,x1);
          incr s
        done;
        acc := id :: !acc
      done;
      if !s < n2 then (
        let x0 = a2.( !s-1 ) in
        let x1 = a2.( !s ) in
        save (x0,x1);
      )
    with Not_found ->
      assert false
  );
  ignore(Zinvcol.build_end binv);
  Ztable.get_zcol ztable float_pair name
