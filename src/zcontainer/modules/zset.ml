(* Encoded sets of integers *)

(* ok to compile with -unsafe *)


open Ztypes
open Zutil
open Printf

type zset =
  | Zs_empty
  | Zs_one of space * int
  | Zs_enc of zset_encoded
  | Zs_bitset of int * int * int * Bitset.bitset
      (* (count, first, last, bitset) *)

 and zset_encoded =
   { mutable zs_data : zset_data;
     mutable zs_index : int array;
     mutable zs_count : int;
     mutable zs_first : int;
     mutable zs_last : int;
     zs_space : space;
     zs_enctype : zset_enctype;
   }

 and zset_data =
   (int, Bigarray.int_elt, Bigarray.c_layout) Bigarray.Array1.t

 and zset_enctype =
   | Packed_enc
   | Array_enc
   | Bitset_enc

module Pool_config = struct
  type entry = zset_encoded
  type data_type = int
  type data_repr = Bigarray.int_elt
  let bigarray_kind = Bigarray.int
  let bigarray_element_size = 8
  let get_bigarray zset = zset.zs_data
  let set_bigarray zset data = zset.zs_data <- data
end


module P = Zpool.Pool(Pool_config)

type pool = 
  { pool : P.pool;
    pool_enctype : zset_enctype;
  }

type ranges =
    { mutable rstart : int array;
      mutable rlen : int array;
      mutable rnum : int;
    }

type zset_builder =
    { b_pool : P.pool;
      b_pool_enctype : zset_enctype;
      b_ranges : ranges;
      mutable b_first : int;
      mutable b_last : int;
      mutable b_count : int;
      b_zset : zset_encoded;
      mutable b_zset_end : int;
      mutable b_prev : int;
      b_bitset : Bitset.bitset;
    }

let alloc_zset p size zs_space zs_enctype =
  P.alloc_entry
    (fun zs_data ->
       { zs_data;
         zs_index = [| |];
         zs_count = 0;
         zs_first = 0;
         zs_last = 0;
         zs_space;
         zs_enctype
       }
    )
    p.pool size

let resize_zset p zset size =
  if zset.zs_enctype <> Bitset_enc then
    P.resize_entry p zset size

let dummy_array_zset =
  { zs_data = Bigarray.Array1.create Bigarray.int Bigarray.c_layout 0;
    zs_index = [| |];
    zs_count = 0;
    zs_first = 0;
    zs_last = 0;
    zs_space = Space 0;
    zs_enctype = Array_enc;
  }

let dummy_bitset =
  Bitset.create 0 false

let bitset_zset zs_space =
  (* no data here, but we still want count, first, last *)
  { zs_data = Bigarray.Array1.create Bigarray.int Bigarray.c_layout 0;
    zs_index = [| |];
    zs_count = 0;
    zs_first = 0;
    zs_last = 0;
    zs_space;
    zs_enctype = Bitset_enc;
  }

let dummy_bitset_zset = bitset_zset (Space 0)


let create_pool ?(enctype=Packed_enc) n_data n_entries =
  { pool = P.create_pool n_data n_entries;
    pool_enctype = enctype
  }

let default_pool =
  create_pool 1024 8

let bitset_pool =
  { pool = P.create_pool 1 1;
    pool_enctype = Bitset_enc
  }


exception Decode_error

let member_max = (1 lsl 48) - 1

let data_max = (1 lsl 56) - 1

let encode_type_0 ranges =
  let i = ranges.rstart.(0) in
  if i > member_max then failwith "Zcontainer.Zset: integer out of range";
  i

let encode_type_0_int i =
  if i > member_max then failwith "Zcontainer.Zset: integer out of range";
  i

let decode_type_0 _ _ _ ranges w =
  if w <= 0 || w > member_max then raise Decode_error;
  ranges.rstart.(0) <- w;
  ranges.rlen.(0) <- 1;
  ranges.rnum <- 1

let encode_type_1 ranges =
  let i = ranges.rstart.(0) + ranges.rlen.(0) - 1 in
  if i > member_max then failwith "Zcontainer.Zset: integer out of range";
  (1 lsl 56) lor i

let encode_type_1_int i =
  if i > member_max then failwith "Zcontainer.Zset: integer out of range";
  (1 lsl 56) lor i

let decode_type_1 first _ _ ranges w =
  let v = w land data_max in
  if v <= 0 || v > member_max || v < first then raise Decode_error;
  ranges.rstart.(0) <- first;
  ranges.rlen.(0) <- v-first+1;
  ranges.rnum <- 1
  
let encode_type_2 ranges =
  let i = ranges.rstart.(0) in
  if i > member_max then failwith "Zcontainer.Zset: integer out of range";
  (2 lsl 56) lor i

let encode_type_2_int i =
  if i > member_max then failwith "Zcontainer.Zset: integer out of range";
  (2 lsl 56) lor i

let decode_type_2 _ last _ ranges w =
  let v = w land data_max in
  if v <= 0 || v > member_max || v > last then raise Decode_error;
  ranges.rstart.(0) <- v;
  ranges.rlen.(0) <- (last - v) + 1;
  ranges.rnum <- 1

let decode_type_012_start first w =
  let ty = w lsr 56 in
  let v = w land data_max in
  if ty=0 then (
    assert(v >= 1 && v <= member_max);
    v
  ) else
    if ty=1 then (
      assert(v >= 1 && v <= member_max);
      first
    )
    else
      if ty=2 then (
        assert(v >= 1 && v <= member_max);
        v
      )
      else
        assert false

let type_4_max = (1 lsl 28) - 1

let check_type_4 prev ranges =
  ranges.rstart.(0) - prev <= type_4_max &&
    ranges.rlen.(0) <= type_4_max

let check_type_4_start prev ranges =
  ranges.rstart.(0) - prev <= type_4_max

let encode_type_4 prev ranges =
  (4 lsl 56) lor
    (ranges.rstart.(0) - prev) lor
      (ranges.rlen.(0) lsl 28)

let encode_type_4_with_max_len prev ranges =
  (4 lsl 56) lor
    (ranges.rstart.(0) - prev) lor
      (type_4_max lsl 28)

let decode_type_4 _ _ prev ranges w =
  ranges.rstart.(0) <- prev + (w land type_4_max);
  ranges.rlen.(0) <- (w lsr 28) land type_4_max;
  ranges.rnum <- 1

let type_5_max = (1 lsl 14) - 1

let check_type_5 prev ranges =
  ranges.rnum >= 2 &&
    let l0 = ranges.rlen.(0) in
    let l1 = ranges.rlen.(1) in
    let s0 = ranges.rstart.(0) in
    let s1 = ranges.rstart.(1) in
    (s0 - prev) lor l0 lor (s1 - s0 - l0) lor l1 <= type_5_max

let encode_type_5 prev ranges =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = rstart.(0) in
  let l0 = rlen.(0) in
  let s1 = rstart.(1) in
  let l1 = rlen.(1) in
  (5 lsl 56) lor
    (s0 - prev) lor
      (l0 lsl 14) lor
        ((s1 - s0 - l0) lsl 28) lor
          (l1 lsl 42)

let decode_type_5 _ _ prev ranges w =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = prev + (w land type_5_max) in
  rstart.(0) <- s0;
  let l0 = (w lsr 14) land type_5_max in
  rlen.(0) <- l0;
  let s1 = s0 + l0 + ((w lsr 28) land type_5_max) in
  rstart.(1) <- s1;
  let l1 = (w lsr 42) land type_5_max in
  rlen.(1) <- l1;
  ranges.rnum <- 2
  

let type_6_max = (1 lsl 9) - 1

let check_type_6 prev ranges =
  ranges.rnum >= 3 &&
    let l0 = ranges.rlen.(0) in
    let l1 = ranges.rlen.(1) in
    let l2 = ranges.rlen.(2) in
    let s0 = ranges.rstart.(0) in
    let s1 = ranges.rstart.(1) in
    let s2 = ranges.rstart.(2) in
    (s0 - prev) lor l0 lor (s1 - s0 - l0) lor l1 lor
      (s2 - s1 - l1) lor l2 <= type_6_max

let encode_type_6 prev ranges =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = rstart.(0) in
  let l0 = rlen.(0) in
  let s1 = rstart.(1) in
  let l1 = rlen.(1) in
  let s2 = rstart.(2) in
  let l2 = rlen.(2) in
  (6 lsl 56) lor
    (s0 - prev) lor
      (l0 lsl 9) lor
        ((s1 - s0 - l0) lsl 18) lor
          (l1 lsl 27) lor
            ((s2 - s1 - l1) lsl 36) lor
              (l2 lsl 45)

let decode_type_6 _ _ prev ranges w =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = prev + (w land type_6_max) in
  rstart.(0) <- s0;
  let l0 = (w lsr 9) land type_6_max in
  rlen.(0) <- l0;
  let s1 = s0 + l0 + ((w lsr 18) land type_6_max) in
  rstart.(1) <- s1;
  let l1 = (w lsr 27) land type_6_max in
  rlen.(1) <- l1;
  let s2 = s1 + l1 + ((w lsr 36) land type_6_max) in
  rstart.(2) <- s2;
  let l2 = (w lsr 45) land type_6_max in
  rlen.(2) <- l2;
  ranges.rnum <- 3


let type_7_max = (1 lsl 7) - 1

let check_type_7 prev ranges =
  ranges.rnum >= 4 &&
    let l0 = ranges.rlen.(0) in
    let l1 = ranges.rlen.(1) in
    let l2 = ranges.rlen.(2) in
    let l3 = ranges.rlen.(3) in
    let s0 = ranges.rstart.(0) in
    let s1 = ranges.rstart.(1) in
    let s2 = ranges.rstart.(2) in
    let s3 = ranges.rstart.(3) in
    (s0 - prev) lor l0 lor (s1 - s0 - l0) lor l1 lor 
      (s2 - s1 - l1) lor l2 lor (s3 - s2 - l2) lor l3 <= type_7_max

let encode_type_7 prev ranges =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = rstart.(0) in
  let l0 = rlen.(0) in
  let s1 = rstart.(1) in
  let l1 = rlen.(1) in
  let s2 = rstart.(2) in
  let l2 = rlen.(2) in
  let s3 = rstart.(3) in
  let l3 = rlen.(3) in
  (7 lsl 56) lor
    (s0 - prev) lor
      (l0 lsl 7) lor
        ((s1 - s0 - l0) lsl 14) lor
          (l1 lsl 21) lor
            ((s2 - s1 - l1) lsl 28) lor
              (l2 lsl 35) lor
                ((s3 - s2 - l2) lsl 42) lor
                  (l3 lsl 49)

let decode_type_7 _ _ prev ranges w =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = prev + (w land type_7_max) in
  rstart.(0) <- s0;
  let l0 = (w lsr 7) land type_7_max in
  rlen.(0) <- l0;
  let s1 = s0 + l0 + ((w lsr 14) land type_7_max) in
  rstart.(1) <- s1;
  let l1 = (w lsr 21) land type_7_max in
  rlen.(1) <- l1;
  let s2 = s1 + l1 + ((w lsr 28) land type_7_max) in
  rstart.(2) <- s2;
  let l2 = (w lsr 35) land type_7_max in
  rlen.(2) <- l2;
  let s3 = s2 + l2 + ((w lsr 42) land type_7_max) in
  rstart.(3) <- s3;
  let l3 = (w lsr 49) land type_7_max in
  rlen.(3) <- l3;
  ranges.rnum <- 4

let type_8_max = (1 lsl 28) - 1

let check_type_8 prev ranges =
  ranges.rnum >= 2 &&
    let l0 = ranges.rlen.(0) in
    let l1 = ranges.rlen.(1) in
    (l0 lor l1) = 1 &&
      let s0 = ranges.rstart.(0) in
      let s1 = ranges.rstart.(1) in
      (s0 - prev) lor (s1 - s0) <= type_8_max

let encode_type_8 prev ranges =
  let rstart = ranges.rstart in
  let s0 = rstart.(0) in
  let s1 = rstart.(1) in
  (8 lsl 56) lor
    (s0 - prev) lor
      ((s1 - s0) lsl 28)

let decode_type_8 _ _ prev ranges w =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = prev + (w land type_8_max) in
  rstart.(0) <- s0;
  let s1 = s0 + ((w lsr 28) land type_8_max) in
  rstart.(1) <- s1;
  rlen.(0) <- 1;
  rlen.(1) <- 1;
  ranges.rnum <- 2

let type_9_max = (1 lsl 18) - 1

let check_type_9 prev ranges =
  ranges.rnum >= 3 &&
    let l0 = ranges.rlen.(0) in
    let l1 = ranges.rlen.(1) in
    let l2 = ranges.rlen.(2) in
    l0 lor l1 lor l2 = 1 &&
      let s0 = ranges.rstart.(0) in
      let s1 = ranges.rstart.(1) in
      let s2 = ranges.rstart.(2) in
      (s0 - prev) lor (s1 - s0) lor (s2 - s1) <= type_9_max

let encode_type_9 prev ranges =
  let rstart = ranges.rstart in
  let s0 = rstart.(0) in
  let s1 = rstart.(1) in
  let s2 = rstart.(2) in
  (9 lsl 56) lor
    (s0 - prev) lor
      ((s1 - s0) lsl 18) lor
        ((s2 - s1) lsl 36)

let decode_type_9 _ _ prev ranges w =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = prev + (w land type_9_max) in
  rstart.(0) <- s0;
  let s1 = s0 + ((w lsr 18) land type_9_max) in
  rstart.(1) <- s1;
  let s2 = s1 + ((w lsr 36) land type_9_max) in
  rstart.(2) <- s2;
  rlen.(0) <- 1;
  rlen.(1) <- 1;
  rlen.(2) <- 1;
  ranges.rnum <- 3

let type_10_max = (1 lsl 14) - 1

let check_type_10 prev ranges =
  ranges.rnum >= 4 &&
    let l0 = ranges.rlen.(0) in
    let l1 = ranges.rlen.(1) in
    let l2 = ranges.rlen.(2) in
    let l3 = ranges.rlen.(3) in
    l0 lor l1 lor l2 lor l3 = 1 &&
      let s0 = ranges.rstart.(0) in
      let s1 = ranges.rstart.(1) in
      let s2 = ranges.rstart.(2) in
      let s3 = ranges.rstart.(3) in
      (s0 - prev) lor (s1 - s0) lor (s2 - s1) lor (s3 - s2) <= type_10_max

let encode_type_10 prev ranges =
  let rstart = ranges.rstart in
  let s0 = rstart.(0) in
  let s1 = rstart.(1) in
  let s2 = rstart.(2) in
  let s3 = rstart.(3) in
  (10 lsl 56) lor
    (s0 - prev) lor
      ((s1 - s0) lsl 14) lor
        ((s2 - s1) lsl 28) lor
          ((s3 - s2) lsl 42)

let decode_type_10 _ _ prev ranges w =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = prev + (w land type_10_max) in
  rstart.(0) <- s0;
  let s1 = s0 + ((w lsr 14) land type_10_max) in
  rstart.(1) <- s1;
  let s2 = s1 + ((w lsr 28) land type_10_max) in
  rstart.(2) <- s2;
  let s3 = s2 + ((w lsr 42) land type_10_max) in
  rstart.(3) <- s3;
  rlen.(0) <- 1;
  rlen.(1) <- 1;
  rlen.(2) <- 1;
  rlen.(3) <- 1;
  ranges.rnum <- 4

let type_11_max = (1 lsl 10) - 1

let check_type_11 prev ranges =
  ranges.rnum >= 5 &&
    let l0 = ranges.rlen.(0) in
    let l1 = ranges.rlen.(1) in
    let l2 = ranges.rlen.(2) in
    let l3 = ranges.rlen.(3) in
    let l4 = ranges.rlen.(4) in
    l0 lor l1 lor l2 lor l3 lor l4 = 1 &&
      let s0 = ranges.rstart.(0) in
      let s1 = ranges.rstart.(1) in
      let s2 = ranges.rstart.(2) in
      let s3 = ranges.rstart.(3) in
      let s4 = ranges.rstart.(4) in
      (s0 - prev) lor (s1 - s0) lor (s2 - s1) lor (s3 - s2) lor 
        (s4 - s3) <= type_11_max

let encode_type_11 prev ranges =
  let rstart = ranges.rstart in
  let s0 = rstart.(0) in
  let s1 = rstart.(1) in
  let s2 = rstart.(2) in
  let s3 = rstart.(3) in
  let s4 = rstart.(4) in
  (11 lsl 56) lor
    (s0 - prev) lor
      ((s1 - s0) lsl 10) lor
        ((s2 - s1) lsl 20) lor
          ((s3 - s2) lsl 30) lor
            ((s4 - s3) lsl 40)

let decode_type_11 _ _ prev ranges w =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = prev + (w land type_11_max) in
  rstart.(0) <- s0;
  let s1 = s0 + ((w lsr 10) land type_11_max) in
  rstart.(1) <- s1;
  let s2 = s1 + ((w lsr 20) land type_11_max) in
  rstart.(2) <- s2;
  let s3 = s2 + ((w lsr 30) land type_11_max) in
  rstart.(3) <- s3;
  let s4 = s3 + ((w lsr 40) land type_11_max) in
  rstart.(4) <- s4;
  rlen.(0) <- 1;
  rlen.(1) <- 1;
  rlen.(2) <- 1;
  rlen.(3) <- 1;
  rlen.(4) <- 1;
  ranges.rnum <- 5

let type_12_max = (1 lsl 9) - 1

let check_type_12 prev ranges =
  ranges.rnum >= 6 &&
    let l0 = ranges.rlen.(0) in
    let l1 = ranges.rlen.(1) in
    let l2 = ranges.rlen.(2) in
    let l3 = ranges.rlen.(3) in
    let l4 = ranges.rlen.(4) in
    let l5 = ranges.rlen.(5) in
    l0 lor l1 lor l2 lor l3 lor l4 lor l5 = 1 &&
      let s0 = ranges.rstart.(0) in
      let s1 = ranges.rstart.(1) in
      let s2 = ranges.rstart.(2) in
      let s3 = ranges.rstart.(3) in
      let s4 = ranges.rstart.(4) in
      let s5 = ranges.rstart.(5) in
      (s0 - prev) lor (s1 - s0) lor (s2 - s1) lor (s3 - s2) lor
        (s4 - s3) lor (s5 - s4) <= type_12_max

let encode_type_12 prev ranges =
  let rstart = ranges.rstart in
  let s0 = rstart.(0) in
  let s1 = rstart.(1) in
  let s2 = rstart.(2) in
  let s3 = rstart.(3) in
  let s4 = rstart.(4) in
  let s5 = rstart.(5) in
  (12 lsl 56) lor
    (s0 - prev) lor
      ((s1 - s0) lsl 9) lor
        ((s2 - s1) lsl 18) lor
          ((s3 - s2) lsl 27) lor
            ((s4 - s3) lsl 36) lor
              ((s5 - s4) lsl 45)

let decode_type_12 _ _ prev ranges w =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = prev + (w land type_12_max) in
  rstart.(0) <- s0;
  let s1 = s0 + ((w lsr 9) land type_12_max) in
  rstart.(1) <- s1;
  let s2 = s1 + ((w lsr 18) land type_12_max) in
  rstart.(2) <- s2;
  let s3 = s2 + ((w lsr 27) land type_12_max) in
  rstart.(3) <- s3;
  let s4 = s3 + ((w lsr 36) land type_12_max) in
  rstart.(4) <- s4;
  let s5 = s4 + ((w lsr 45) land type_12_max) in
  rstart.(5) <- s5;
  rlen.(0) <- 1;
  rlen.(1) <- 1;
  rlen.(2) <- 1;
  rlen.(3) <- 1;
  rlen.(4) <- 1;
  rlen.(5) <- 1;
  ranges.rnum <- 6

let type_13_max = (1 lsl 8) - 1

let check_type_13 prev ranges =
  ranges.rnum >= 7 &&
    let l0 = ranges.rlen.(0) in
    let l1 = ranges.rlen.(1) in
    let l2 = ranges.rlen.(2) in
    let l3 = ranges.rlen.(3) in
    let l4 = ranges.rlen.(4) in
    let l5 = ranges.rlen.(5) in
    let l6 = ranges.rlen.(6) in
    l0 lor l1 lor l2 lor l3 lor l4 lor l5 lor l6 = 1 &&
      let s0 = ranges.rstart.(0) in
      let s1 = ranges.rstart.(1) in
      let s2 = ranges.rstart.(2) in
      let s3 = ranges.rstart.(3) in
      let s4 = ranges.rstart.(4) in
      let s5 = ranges.rstart.(5) in
      let s6 = ranges.rstart.(6) in
      (s0 - prev) lor (s1 - s0) lor (s2 - s1) lor (s3 - s2) lor
        (s4 - s3) lor (s5 - s4) lor (s6 - s5) <= type_13_max

let encode_type_13 prev ranges =
  let rstart = ranges.rstart in
  let s0 = rstart.(0) in
  let s1 = rstart.(1) in
  let s2 = rstart.(2) in
  let s3 = rstart.(3) in
  let s4 = rstart.(4) in
  let s5 = rstart.(5) in
  let s6 = rstart.(6) in
  (13 lsl 56) lor
    (s0 - prev) lor
      ((s1 - s0) lsl 8) lor
        ((s2 - s1) lsl 16) lor
          ((s3 - s2) lsl 24) lor
            ((s4 - s3) lsl 32) lor
              ((s5 - s4) lsl 40) lor
                ((s6 - s5) lsl 48)

let decode_type_13 _ _ prev ranges w =
  let rstart = ranges.rstart in
  let rlen = ranges.rlen in
  let s0 = prev + (w land type_13_max) in
  rstart.(0) <- s0;
  let s1 = s0 + ((w lsr 8) land type_13_max) in
  rstart.(1) <- s1;
  let s2 = s1 + ((w lsr 16) land type_13_max) in
  rstart.(2) <- s2;
  let s3 = s2 + ((w lsr 24) land type_13_max) in
  rstart.(3) <- s3;
  let s4 = s3 + ((w lsr 32) land type_13_max) in
  rstart.(4) <- s4;
  let s5 = s4 + ((w lsr 40) land type_13_max) in
  rstart.(5) <- s5;
  let s6 = s5 + ((w lsr 48) land type_13_max) in
  rstart.(6) <- s6;
  rlen.(0) <- 1;
  rlen.(1) <- 1;
  rlen.(2) <- 1;
  rlen.(3) <- 1;
  rlen.(4) <- 1;
  rlen.(5) <- 1;
  rlen.(6) <- 1;
  ranges.rnum <- 7

let decoders =
  [| decode_type_0;
     decode_type_1;
     decode_type_2;
     (fun _ _ _ _ -> raise Decode_error);
     decode_type_4;
     decode_type_5;
     decode_type_6;
     decode_type_7;
     decode_type_8;
     decode_type_9;
     decode_type_10;
     decode_type_11;
     decode_type_12;
     decode_type_13;
    |]

let rnum_of_decoders =
  [| 1; 1; 1; 0;
     1; 2; 3; 4;
     2; 3; 4; 5;
     6; 7 |]

let decode first last prev ranges w =
  let ty = w lsr 56 in
  if ty > 13 then raise Decode_error;
  decoders.(ty) first last prev ranges w


let decode_rnum w =
  (* TODO: a better numbering of the types would be good here,
     permitting: ty land 7 *)
  let ty = w lsr 56 in
  if ty > 13 then raise Decode_error;
  rnum_of_decoders.(ty)


let shift_ranges r n =
  Array.blit r.rstart n r.rstart 0 (r.rnum - n);
  Array.blit r.rlen n r.rlen 0 (r.rnum - n);
  r.rnum <- r.rnum - n


let write b n w =
  b.b_zset.zs_data.{ b.b_zset_end } <- w;
  b.b_zset_end <- b.b_zset_end + 1;
  b.b_prev <- b.b_ranges.rstart.(n-1) + b.b_ranges.rlen.(n-1) - 1;
  shift_ranges b.b_ranges n


let write_type_4_to_7 b ranges other =
  let prev = b.b_prev in
  let rlen = ranges.rlen in
  let l0 = rlen.(0) in
  let l1 = rlen.(1) in
  let l2 = rlen.(2) in
  let l3 = rlen.(3) in
  let rstart = ranges.rstart in
  let s0 = rstart.(0) in
  let s1 = rstart.(1) in
  let s2 = rstart.(2) in
  let s3 = rstart.(3) in
  let x0 = (s0 - prev) lor l0 in
  let x1 = x0 lor (s1 - s0 - l0) lor l1 in
  let x2 = x1 lor (s2 - s1 - l1) lor l2 in
  let x3 = x2 lor (s3 - s2 - l2) lor l3 in
  let rnum = ranges.rnum in
  if rnum >= 4 && x3 <= type_7_max then
    write b 4 (encode_type_7 b.b_prev ranges)
  else if rnum >= 3 && x2 <= type_6_max then
    write b 3 (encode_type_6 b.b_prev ranges)
  else if rnum >= 2 && x1 <= type_5_max then
    write b 2 (encode_type_5 b.b_prev ranges)
  else if x0 <= type_4_max then
    write b 1 (encode_type_4 b.b_prev ranges)
  else
    other()


let write_type_0_and_8_to_13 b ranges =
  let prev = b.b_prev in
  let rlen = ranges.rlen in
  let l0 = rlen.(0) in
  let l1 = rlen.(1) in
  let l01 = l0 lor l1 in
  let l2 = rlen.(2) in
  let l012 = l01 lor l2 in
  let l3 = rlen.(3) in
  let l0123 = l012 lor l3 in
  let l4 = rlen.(4) in
  let l01234 = l0123 lor l4 in
  let l5 = rlen.(5) in
  let l012345 = l01234 lor l5 in
  let l6 = rlen.(6) in
  let l0123456 = l012345 lor l6 in
  let rstart = ranges.rstart in
  let s0 = rstart.(0) in
  let d0 = s0 - prev in
  let s1 = rstart.(1) in
  let d01 = d0 lor (s1 - s0) in
  let s2 = rstart.(2) in
  let d012 = d01 lor (s2 - s1) in
  let s3 = rstart.(3) in
  let d0123 = d012 lor (s3 - s2) in
  let s4 = rstart.(4) in
  let d01234 = d0123 lor (s4 - s3) in
  let s5 = rstart.(5) in
  let d012345 = d01234 lor (s5 - s4) in
  let s6 = rstart.(6) in
  let d0123456 = d012345 lor (s6 - s5) in
  let rnum = ranges.rnum in
  if rnum >= 7 && l0123456 = 1 && d0123456 <= type_13_max then
    write b 7 (encode_type_13 b.b_prev ranges)
  else if rnum >= 6 && l012345 = 1 && d012345 <= type_12_max then
    write b 6 (encode_type_12 b.b_prev ranges)
  else if rnum >= 5 && l01234 = 1 && d01234 <= type_11_max then
    write b 5 (encode_type_11 b.b_prev ranges)
  else if rnum >= 4 && l0123 = 1 && d0123 <= type_10_max then
    write b 4 (encode_type_10 b.b_prev ranges)
    (* TODO: also check ranges here! *)
  else if rnum >= 3 && l012 = 1 && d012 <= type_9_max then
    write b 3 (encode_type_9 b.b_prev ranges)
  else if rnum >= 2 && l01 = 1 && d01 <= type_8_max then
    write b 2 (encode_type_8 b.b_prev ranges)
  else
    (* assume rnum >= 1 && l0 = 1 *)
    write b 1 (encode_type_0 ranges)


let rec build_write_packed_1 b very_last =
  (* write another range and at least one word, which is not one of the 
     256-th words *)
  let ranges = b.b_ranges in
  if ranges.rlen.(0) > 1 then (
    (* the first range is a real range *)
    write_type_4_to_7
      b ranges
      (fun () ->
         (* fallback: *)
         if check_type_4_start b.b_prev ranges then (
           (* len is too large *)
           let w = encode_type_4_with_max_len b.b_prev ranges in
           b.b_zset.zs_data.{ b.b_zset_end } <- w;
           b.b_zset_end <- b.b_zset_end + 1;
           b.b_prev <- b.b_ranges.rstart.(0) + type_4_max - 1;
           ranges.rstart.(0) <- ranges.rstart.(0) + type_4_max;
           ranges.rlen.(0) <- ranges.rlen.(0) - type_4_max;
           build_write_packed b very_last
         ) else (
           (* can only write a type 0 *)
           let w = encode_type_0 ranges in
           b.b_zset.zs_data.{ b.b_zset_end } <- w;
           b.b_zset_end <- b.b_zset_end + 1;
           b.b_prev <- b.b_ranges.rstart.(0);
           ranges.rstart.(0) <- ranges.rstart.(0) + 1;
           ranges.rlen.(0) <- ranges.rlen.(0) - 1;
           build_write_packed b very_last
         )
      )
  ) else
    (* the first range is a single integer *)
    write_type_0_and_8_to_13 b ranges

and build_write_packed b very_last =
  (* write another range *)
  let zset = b.b_zset in
  let zset_end = b.b_zset_end in
  let ranges = b.b_ranges in
  if zset_end = Bigarray.Array1.dim zset.zs_data then
    resize_zset b.b_pool zset (2*(zset_end + 1));
  if very_last && ranges.rnum = 1 then (
    (* at the end, and we want to write a type 2 word *)
    let w = encode_type_2 ranges in
    zset.zs_data.{ zset_end } <- w;
    b.b_zset_end <- zset_end + 1;
    b.b_prev <- ranges.rstart.(0) + ranges.rlen.(0) - 1;
    ranges.rnum <- 0
  ) else
    if zset_end land 0xff = 0 then (
      if zset_end = 0 then (
        (* at the very beginning we prefer a type 1 word *)
        let w = encode_type_1 ranges in
        zset.zs_data.{ zset_end } <- w;
        b.b_zset_end <- zset_end + 1;
        b.b_prev <- ranges.rstart.(0) + ranges.rlen.(0) - 1;
        shift_ranges ranges 1;
      ) else (
        (* the 256-th word *)
        let w = encode_type_0 ranges in
        zset.zs_data.{ zset_end } <- w;
        b.b_zset_end <- zset_end + 1;
        b.b_prev <- ranges.rstart.(0);
        if ranges.rlen.(0) = 1 then
          shift_ranges ranges 1
        else (
          ranges.rstart.(0) <- ranges.rstart.(0) + 1;
          ranges.rlen.(0) <- ranges.rlen.(0) - 1;
          build_write_packed b very_last
        )
      )
    ) else
      build_write_packed_1 b very_last


let build_write_array b very_last =
  let zset = b.b_zset in
  let rnum = b.b_ranges.rnum in
  let zset_end = b.b_zset_end in
  if zset_end + rnum + rnum > Bigarray.Array1.dim zset.zs_data then
    resize_zset b.b_pool zset (2*(zset_end + rnum + rnum));
  let rstart = b.b_ranges.rstart in
  let rlen = b.b_ranges.rlen in
  for k = 0 to rnum - 1 do
    zset.zs_data.{ zset_end + k + k } <- rstart.(k);
    zset.zs_data.{ zset_end + k + k + 1 } <- rlen.(k);
  done;
  b.b_zset_end <- zset_end + (rnum lsl 1);
  b.b_prev <- rstart.(rnum-1) + rlen.(rnum-1) - 1;
  b.b_ranges.rnum <- 0


let build_write_bitset b very_last =
  let bitset = b.b_bitset in
  let rstart = b.b_ranges.rstart in
  let rlen = b.b_ranges.rlen in
  let rnum = b.b_ranges.rnum in
  for k = 0 to rnum - 1 do
    let s = rstart.(k) in
    let l = rlen.(k) in
    Bitset.fill_true bitset (s-1) l
  done;
  b.b_prev <- rstart.(rnum-1) + rlen.(rnum-1) - 1;
  b.b_ranges.rnum <- 0


let build_write_ranges b very_last =
  match b.b_pool_enctype with
    | Packed_enc ->
        build_write_packed b very_last
    | Array_enc ->
        build_write_array b very_last
    | Bitset_enc ->
        build_write_bitset b very_last


let create_ranges() =
  (* for decoding packed array we need at least 7 elements.
     For decoding bitsets we need at least 8 elements (= max number of
     ranges for 16 bits)
   *)
  { rstart = Array.make 8 0;
    rlen = Array.make 8 0;
    rnum = 0
  }


let build_begin p init_size zs_space =
  let Space sp = zs_space in
  let b_zset, b_bitset =
    if p.pool_enctype = Bitset_enc then
      bitset_zset zs_space, Bitset.create sp false
    else
      alloc_zset p init_size zs_space p.pool_enctype, dummy_bitset in
  { b_pool = p.pool;
    b_pool_enctype = p.pool_enctype;
    b_ranges = create_ranges();
    b_zset;
    b_first = 0;
    b_last = 0;
    b_count = 0;
    b_zset_end = 0;
    b_prev = 0;
    b_bitset;
  }

let build_add b int =
  if int <= b.b_last then invalid_arg "Zcontainer.Zset.build_add";
  let b_ranges = b.b_ranges in
  let rnum = b_ranges.rnum in
  if rnum = 0 then (
    (* very first addition *)
    b_ranges.rstart.(0) <- int;
    b_ranges.rlen.(0) <- 1;
    b_ranges.rnum <- 1;
    b.b_first <- int;
    b.b_count <- 1;
  ) else (
    if int = b_ranges.rstart.(rnum-1) + b_ranges.rlen.(rnum-1) then (
      (* the last range can be extended *)
      b_ranges.rlen.(rnum-1) <- b_ranges.rlen.(rnum-1) + 1
    ) else (
      (* need to start a new range *)
      if rnum = 7 then
        build_write_ranges b false;
      let rnum = b_ranges.rnum in
      b_ranges.rstart.(rnum) <- int;
      b_ranges.rlen.(rnum) <- 1;
      b_ranges.rnum <- rnum+1
    );
    b.b_count <- b.b_count + 1
  );
  b.b_last <- int
  
let build_add_range b start len =
  if start <= b.b_last || len < 1 then
    invalid_arg "Zcontainer.Zset.build_add_range";
  let b_ranges = b.b_ranges in
  let rnum = b_ranges.rnum in
  if rnum = 0 then (
    (* very first addition *)
    b_ranges.rstart.(0) <- start;
    b_ranges.rlen.(0) <- len;
    b_ranges.rnum <- 1;
    b.b_first <- start;
    b.b_count <- len;
  ) else (
    if start = b_ranges.rstart.(rnum-1) + b_ranges.rlen.(rnum-1) then (
      (* the last range can be extended *)
      b_ranges.rlen.(rnum-1) <- b_ranges.rlen.(rnum-1) + len
    ) else (
      (* need to start a new range *)
      if rnum = 7 then
        build_write_ranges b false;
      let rnum = b_ranges.rnum in
      b_ranges.rstart.(rnum) <- start;
      b_ranges.rlen.(rnum) <- len;
      b_ranges.rnum <- rnum+1
    );
    b.b_count <- b.b_count + len;
  );
  b.b_last <- start+len-1

let build_end_as_enc b =
  while b.b_ranges.rnum > 0 do
    build_write_ranges b true
  done;
  let zset = b.b_zset in
  resize_zset b.b_pool zset b.b_zset_end;
  zset.zs_count <- b.b_count;
  zset.zs_first <- b.b_first;
  zset.zs_last <- b.b_last;
  b.b_last <- max_int;      (* prevent further adds *)
  zset
  
let set_index zset =
  (* NB. works for both Packed_enc and Array_enc *)
  let size = Bigarray.Array1.dim zset.zs_data in
  let zs_index =
    Array.init
      ((size-1) / 256 + 1)
      (fun i ->
         let w = zset.zs_data.{ i lsl 8 } in
         decode_type_012_start zset.zs_first w
      ) in
  zset.zs_index <- zs_index

let build_end b =
  if b.b_last = max_int then
    invalid_arg "Zcontainer.Zset.build_end";
  let zset = build_end_as_enc b in
  if zset.zs_count = 0 then
    Zs_empty
  else if zset.zs_count = 1 then
    Zs_one(zset.zs_space, zset.zs_first)
  else (
    ( match zset.zs_enctype with
        | Bitset_enc ->
            Bitset.set_runlengths b.b_bitset;
            Zs_bitset(zset.zs_count, zset.zs_first, zset.zs_last, b.b_bitset)
        | Array_enc
        | Packed_enc ->
            set_index zset;
            Zs_enc zset
    )
  )

let empty =
  Zs_empty

let is_empty =
  function
  | Zs_empty -> true
  | _ -> false

let all p zs_space =
  let Space end_id = zs_space in
  let b = build_begin p 1 zs_space in
  build_add_range b 1 end_id;
  build_end b

let is_all =
  function
  | Zs_empty -> false
  | Zs_one(Space sp,x) -> x=1 && sp=1
  | Zs_enc zs ->
      let Space sp = zs.zs_space in
      zs.zs_first = 1 &&
        zs.zs_last = sp &&
          zs.zs_count = sp
  | Zs_bitset(count,first,last,bitset) ->
      let sp = Bitset.length bitset in
      first = 1 &&
        last = sp &&
          count = sp

let same_space z1 z2 =
  let get_space =
    function
    | Zs_empty -> assert false
    | Zs_one(space,_) -> space
    | Zs_enc zs -> zs.zs_space
    | Zs_bitset(_,_,_,bitset) -> Space(Bitset.length bitset) in
  match z1,z2 with
    | Zs_empty, Zs_empty -> true
    | Zs_empty, _ -> false  (* matter of definition *)
    | _, Zs_empty -> false
    | _ -> get_space z1 = get_space z2

let space_less_or_equal_than z1 (Space sp) =
  match z1 with
    | Zs_empty -> true
    | Zs_one(Space sp1,_) -> sp1 <= sp
    | Zs_enc zs ->
        let Space sp1 = zs.zs_space in
        sp1 <= sp
    | Zs_bitset(_,_,_,bitset) -> Bitset.length bitset <= sp


let max_space (Space n) z =
  match z with
    | Zs_empty -> Space n
    | Zs_one(Space sp,_) -> Space(max sp n)
    | Zs_enc zs ->
        let Space sp = zs.zs_space in
        Space(max sp n)
    | Zs_bitset(_,_,_,bitset) -> Space(max (Bitset.length bitset) n)


let min_space (Space n) z =
  match z with
    | Zs_empty -> Space n
    | Zs_one(Space sp,_) -> Space(min sp n)
    | Zs_enc zs ->
        let Space sp = zs.zs_space in
        Space(min sp n)
    | Zs_bitset(_,_,_,bitset) -> Space(min (Bitset.length bitset) n)


let from_array p zs_space a =
  let l = Array.length a in
  if l = 0 then
    Zs_empty
  else if l = 1 then
    Zs_one(zs_space, a.(0))
  else
    let b = build_begin p 64 zs_space in
    Array.iter
      (fun x ->
         build_add b x
      )
      a;
    build_end b
  

let count =
  function
  | Zs_empty -> 0
  | Zs_one _ -> 1
  | Zs_enc zset -> zset.zs_count
  | Zs_bitset(count,_,_,_) -> count


let first =
  function
  | Zs_empty -> raise Not_found
  | Zs_one(_,int) -> int
  | Zs_enc zset -> zset.zs_first
  | Zs_bitset(_,first,_,_) -> first


let last =
  function
  | Zs_empty -> raise Not_found
  | Zs_one(_,int) -> int
  | Zs_enc zset -> zset.zs_last
  | Zs_bitset(_,_,last,_) -> last


type iter =
    { itr_zset : zset_encoded;
      itr_bitset : Bitset.bitset;
      itr_length : int;
      mutable itr_pos : int;
      mutable itr_prev : int;
      itr_ranges : ranges;
    }

exception End_of_iteration

let iter_new itr_zset =
  { itr_zset;
    itr_bitset = dummy_bitset;
    itr_length = Bigarray.Array1.dim itr_zset.zs_data;
    itr_pos = (-1);
    itr_prev = 0;
    itr_ranges = create_ranges()
  }

let iter_new_bitset first last bitset =
  { itr_zset = dummy_bitset_zset;
    itr_bitset = bitset;
    itr_length = last;
    itr_pos = first-2;
    itr_prev = 0;
    itr_ranges = create_ranges()
  }

let iter_none() =
  let itr_zset = dummy_array_zset in
  let itr_ranges = create_ranges() in
  itr_ranges.rnum <- 0;
  { itr_zset;
    itr_bitset = dummy_bitset;
    itr_length = 0;
    itr_pos = 0;
    itr_prev = 0;
    itr_ranges;
  }

let iter_one id =
  let itr = iter_none() in
  itr.itr_ranges.rstart.(0) <- id;
  itr.itr_ranges.rlen.(0) <- 1;
  itr.itr_ranges.rnum <- 0;
  itr

let find_end_of_bitset_range bitset length rstart rlen =
  (* watch out for more 1's beyond what we already found *)
  let s = ref (rstart + rlen - 1) in
  while !s < length && Bitset.get bitset !s do
    let n = Bitset.get_rl bitset !s in
    assert (n >= 1);
    s := !s + n
  done;
  !s - rstart + 1


let rec iter_next_bitset itr p =
  (* The ID p+1 is the next ID to iterate over *)
  if p >= itr.itr_length then raise End_of_iteration;
  let bitset = itr.itr_bitset in
  let x = Bitset.get16 bitset p in
  if x=0 then (
    let rl = Bitset.get_rl itr.itr_bitset p in
    assert(rl <= 0);
    let n = -rl+1 in
    iter_next_bitset itr (p+n)
  ) else if x = 65535 then (
    let n = Bitset.get_rl itr.itr_bitset p in
    assert(n >= 1);
    let ranges = itr.itr_ranges in
    let ext =
      find_end_of_bitset_range bitset itr.itr_length (p+1) n in
    ranges.rstart.(0) <- p+1;   (* bit p of the bitset encodes ID p+1 *)
    ranges.rlen.(0) <- ext;
    ranges.rnum <- 1;
    itr.itr_prev <- p+ext;
    itr.itr_pos <- p+ext-1;
  )
  else (
    let ranges = itr.itr_ranges in
    let rstart = ranges.rstart in
    let rlen = ranges.rlen in
    rstart.(0) <- p+1;
    rlen.(0) <- 0;
    (* in the following k cannot overflow because [ranges] has 8 elements,
       and with 16 bits we cannot encode more ranges
     *)
    let k = ref 0 in
    let last = ref false in
    let x = ref x in
    for j = p to min (p+15) (Bitset.length bitset-1) do
      if !x land 1 <> 0 then (
        rlen.(!k) <- rlen.(!k) + 1;
        last := true
      ) else (
        if !last then (
          incr k;
          rlen.(!k) <- 0;
        );
        rstart.(!k) <- j+2;
          (* The ID j+1 is 0, hence the next 1 is at ID j+2 (at least) *)
        last := false;
      );
      x := !x lsr 1
    done;
    if !last then (
      let ext =
        find_end_of_bitset_range bitset itr.itr_length rstart.(!k) rlen.(!k) in
      rlen.(!k) <- ext
    ) else (
      if rlen.(!k) = 0 then (
        if !k = 0 then raise End_of_iteration;
        decr k
      )
    );
    ranges.rnum <- !k+1;
    let prev = rstart.(!k) + rlen.(!k) - 1 in
    itr.itr_prev <- prev;
    itr.itr_pos <- prev-1
  )
    

let iter_next itr =
  let zs = itr.itr_zset in
  let p = itr.itr_pos + 1 in
  if p >= itr.itr_length then raise End_of_iteration;
  let ranges = itr.itr_ranges in
  match zs.zs_enctype with
    | Packed_enc ->
        let w = zs.zs_data.{ p } in
        decode zs.zs_first zs.zs_last itr.itr_prev ranges w;
        let n = ranges.rnum in
        itr.itr_prev <- ranges.rstart.(n-1) + ranges.rlen.(n-1) - 1;
        itr.itr_pos <- p
    | Array_enc ->
        let rstart = ranges.rstart in
        let rlen = ranges.rlen in
        let data = zs.zs_data in
        let n = min 7 ((itr.itr_length - p) lsr 1) in
        for k = 0 to n-1 do
          rstart.(k) <- data.{ p + k + k };
          rlen.(k) <- data.{ p + k + k + 1 };
        done;
        ranges.rnum <- n;
        itr.itr_prev <- rstart.(n-1) + rlen.(n-1) - 1;
        itr.itr_pos <- p + n + n - 1;
    | Bitset_enc ->
        iter_next_bitset itr p

let iter_cur_start itr =
  let n = itr.itr_ranges.rnum in
  assert(n >= 1);
  itr.itr_ranges.rstart.(0)

let iter_cur_end itr =
  let n = itr.itr_ranges.rnum in
  assert(n >= 1);
  itr.itr_ranges.rstart.(n-1) + itr.itr_ranges.rlen.(n-1) - 1


let iter_seek itr x =
  (* positions at the member x or at the next word; may raise End_of_iteration
     if there is no next word. The member x is only searched to the right.
   *)
  match itr.itr_zset.zs_enctype with
    | Array_enc
    | Packed_enc ->
        if itr.itr_pos < 0 then iter_next itr;
        if x > iter_cur_end itr then (
          (* move to the right segment *)
          let zs = itr.itr_zset in
          let idx = itr.itr_pos lsr 8 in
          let idxlen = Array.length zs.zs_index in
          if idx + 1 < idxlen && x >= zs.zs_index.(idx+1) then (
            if x >= zs.zs_index.(idxlen-1) then
              itr.itr_pos <- ((idxlen-1) lsl 8) - 1
            else (
              let idx1 = ref (idx+1) in
              let val1 = ref (zs.zs_index.(!idx1)) in
              let idx2 = ref (idxlen-1) in
              let val2 = ref (zs.zs_index.(!idx2)) in
              while x >= !val1 && x < !val2 && !idx2 - !idx1 >= 2 do
                let m = !idx1 + ((!idx2 - !idx1) lsr 1) in
                let v = zs.zs_index.(m) in
                if x >= v then (
                  val1 := v;
                  idx1 := m
                ) else (
                  val2 := v;
                  idx2 := m
                )
              done;
              itr.itr_pos <- ( !idx1 lsl 8) - 1
            );
            iter_next itr
          );
          (* advance until found *)
          while x > iter_cur_end itr do
            iter_next itr
          done
        )
    | Bitset_enc ->
        iter_next_bitset itr (x-1)
        

let iter f zset =
  match zset with
    | Zs_empty ->
        ()
    | Zs_one(_,x) ->
        f x 1
    | Zs_enc enc ->
        ( try
            let i = iter_new enc in
            let r = i.itr_ranges in
            while true do
              iter_next i;
              for k = 0 to r.rnum - 1 do
                let s = r.rstart.(k) in
                let l = r.rlen.(k) in
                f s l
              done
            done
          with
            | End_of_iteration -> ()
        )
    | Zs_bitset(_,first,last,bitset) ->
        ( try
            let i = iter_new_bitset first last bitset in
            let r = i.itr_ranges in
            while true do
              iter_next i;
              for k = 0 to r.rnum - 1 do
                let s = r.rstart.(k) in
                let l = r.rlen.(k) in
                f s l
              done
            done
          with
            | End_of_iteration -> ()
        )

let dump zset =
  let buf = Buffer.create 80 in
  iter
    (fun start len ->
       if Buffer.length buf > 0 then
         Buffer.add_char buf ',';
       if len=1 then
         bprintf buf "%d" start
       else
         bprintf buf "%d-%d" start (start+len-1)
    )
    zset;
  Buffer.contents buf

exception Found

let mem_enc x z =
  try
    let i = iter_new z in
    iter_seek i x;
    let r = i.itr_ranges in
    for k = 0 to r.rnum - 1 do
      let s = r.rstart.(k) in
      let e = s + r.rlen.(k) - 1 in
      if x >= s && x <= e then raise Found
    done;
    false
  with
    | End_of_iteration -> false
    | Found -> true

let mem_bitset x bitset =
  x >= 1 && x <= Bitset.length bitset && Bitset.get bitset (x-1)


let mem x =
  function
  | Zs_empty -> false
  | Zs_one(_,x1) -> x = x1
  | Zs_enc z -> mem_enc x z
  | Zs_bitset(_,_,_,bitset) -> mem_bitset x bitset


exception Output1
exception Output2


let isect_iterable b_begin b_add_range b_end p zs_space i1 i2 =
  let b = b_begin p 64 zs_space in
  ( try
      let r1 = i1.itr_ranges in
      let r2 = i2.itr_ranges in
      iter_next i1;
      iter_next i2;
      let k1 = ref 0 in
      let k2 = ref 0 in
      while true do
        let s1 = r1.rstart.( !k1 ) in
        let e1 = s1 + r1.rlen.( !k1 ) - 1 in
        let s2 = r2.rstart.( !k2 ) in
        let e2 = s2 + r2.rlen.( !k2 ) - 1 in
        let s_out = max s1 s2 in
        let e_out = min e1 e2 in
        if s_out <= e_out then
          b_add_range b s_out (e_out - s_out + 1);
        if e1 <= e2 then (
          incr k1;
          if !k1 >= r1.rnum then (
            k1 := 0;
            iter_next i1;
            if e1 < s2 then
              iter_seek i1 s2;
          )
        );
        if e2 <= e1 then (
          incr k2;
          if !k2 >= r2.rnum then (
            k2 := 0;
            iter_next i2;
            if e2 < s1 then
              iter_seek i2 s1;
          )
        );
      done
    with
      | End_of_iteration -> ()
  );
  b_end b


let isect_iterable_and_bitset b_begin b_add_range b_end p zs_space i1 bitset =
  let b = b_begin p 64 zs_space in
  ( try
      let r1 = i1.itr_ranges in
      iter_next i1;
      let k1 = ref 0 in
      let s2_min = ref 0 in
      while true do
        let s1 = ref (r1.rstart.( !k1 )) in
        let e1 = !s1 + r1.rlen.( !k1 ) - 1 in
        (* split the range s1..e1 up by intersecting it with the bitset *)
        while !s1 <= e1 && !s2_min <= e1 do
          (* get the first bit in the range s1..e1 *)
          let s2 = 
            if !s1 <= !s2_min then
              !s2_min
            else
              !s1 + Bitset.runlength_false bitset (!s1-1) 0 in
          if s2 > Bitset.length bitset then raise End_of_iteration;
          s2_min := s2;
          if s2 <= e1 then (
            (* get the number of bits *)
            let e2 = s2 + Bitset.runlength_true bitset (s2-1) 0 - 1 in
            let e_out = min e1 e2 in
            b_add_range b s2 (e_out - s2 + 1);
            s1 := e_out + 2
          )
        done;
        incr k1;
        if !k1 >= r1.rnum then (
          k1 := 0;
          iter_next i1;
          if e1 < !s2_min then
            iter_seek i1 !s2_min
        )
      done
    with
      | End_of_iteration -> ()
  );
  b_end b


let iter_enc_or_bitset =
  function
  | Zs_enc enc -> iter_new enc
  | Zs_bitset(_,first,last,bitset) -> iter_new_bitset first last bitset
  | _ -> assert false

let space_enc_or_bitset =
  function
  | Zs_enc enc -> enc.zs_space
  | Zs_bitset(_,_,_,bitset) -> Space(Bitset.length bitset)
  | _ -> assert false


let isect p z1 z2 =
  match z1, z2 with
    | Zs_empty, _ ->
        Zs_empty
    | _, Zs_empty ->
        Zs_empty
    | Zs_one(sp1,x1), Zs_one(sp2,x2) ->
        if x1=x2 then
          Zs_one(min_space sp1 z2, x1)
        else
          Zs_empty
    | Zs_one(sp1,x1), Zs_enc enc2 ->
        if mem_enc x1 enc2 then
          Zs_one(min_space sp1 z2, x1)
        else
          Zs_empty
    | Zs_one(sp1,x1), Zs_bitset(_,_,_,bitset) ->
        if mem_bitset x1 bitset then
          Zs_one(min_space sp1 z2, x1)
        else
          Zs_empty
    | Zs_enc enc1, Zs_one(sp2,x2) ->
        if mem_enc x2 enc1 then
          Zs_one(min_space sp2 z2, x2)
        else
          Zs_empty
    | Zs_bitset(_,_,_,bitset), Zs_one(sp2,x2) ->
        if mem_bitset x2 bitset then
          Zs_one(min_space sp2 z1, x2)
        else
          Zs_empty
    | Zs_enc enc1, Zs_enc enc2 ->
        let sp = min_space enc1.zs_space z2 in
        let i1 = iter_new enc1 in
        let i2 = iter_new enc2 in
        isect_iterable
          build_begin build_add_range build_end p sp i1 i2
    | (Zs_enc _|Zs_bitset _), Zs_bitset(_,_,_,bitset) ->
        let sp = min_space (Space(Bitset.length bitset)) z1 in
        let i1 = iter_enc_or_bitset z1 in
        isect_iterable_and_bitset
          build_begin build_add_range build_end p sp i1 bitset
    | Zs_bitset(_,_,_,bitset), Zs_enc _ ->
        let sp = min_space (Space(Bitset.length bitset)) z2 in
        let i2 = iter_enc_or_bitset z2 in
        isect_iterable_and_bitset
          build_begin build_add_range build_end p sp i2 bitset


let isect_non_empty_bitset z bitset =
  let b_begin _ _ _ = () in
  let b_add_range _ _ _ = raise Found in
  let b_end _ = false in
  let sp = min_space (Space(Bitset.length bitset)) z in
  let i1 = iter_enc_or_bitset z in
  ( try
      isect_iterable_and_bitset
        b_begin b_add_range b_end ()
        sp i1 bitset
    with
      | Found -> true
  )


let isect_non_empty z1 z2 =
  match z1, z2 with
    | Zs_empty, _ ->
        false
    | _, Zs_empty ->
        false
    | Zs_one(sp1,x1), Zs_one(sp2,x2) ->
        x1 = x2
    | Zs_one(sp1,x1), Zs_enc enc2 ->
        mem_enc x1 enc2
    | Zs_one(sp1,x1), Zs_bitset(_,_,_,bitset) ->
        mem_bitset x1 bitset
    | Zs_enc enc1, Zs_one(sp2,x2) ->
        mem_enc x2 enc1
    | Zs_bitset(_,_,_,bitset), Zs_one(sp2,x2) ->
        mem_bitset x2 bitset
    | Zs_enc _, Zs_enc _ ->
        let b_begin _ _ _ = () in
        let b_add_range _ _ _ = raise Found in
        let b_end _ = false in
        let sp = min_space (space_enc_or_bitset z1) z2 in
        let i1 = iter_enc_or_bitset z1 in
        let i2 = iter_enc_or_bitset z2 in
        ( try
            isect_iterable
              b_begin b_add_range b_end ()
              sp i1 i2
          with
            | Found -> true
        )
    | (Zs_enc _ | Zs_bitset _), Zs_bitset(_,_,_,bitset) ->
        isect_non_empty_bitset z1 bitset
    | Zs_bitset(_,_,_,bitset), Zs_enc _ ->
        isect_non_empty_bitset z2 bitset


let iter_isect_bitset f z bitset =
  let b_begin _ _ _ = () in
  let b_add_range _ start len = f start len in
  let b_end _ = () in
  let sp = min_space (Space(Bitset.length bitset)) z in
  let i1 = iter_enc_or_bitset z in
  isect_iterable_and_bitset
    b_begin b_add_range b_end ()
    sp i1 bitset


let iter_isect f z1 z2 =
  match z1, z2 with
    | Zs_empty, _ ->
        ()
    | _, Zs_empty ->
        ()
    | Zs_one(sp1,x1), Zs_one(sp2,x2) ->
        if x1=x2 then f x1 1
    | Zs_one(sp1,x1), Zs_enc enc2 ->
        if mem_enc x1 enc2 then f x1 1
    | Zs_one(sp1,x1), Zs_bitset(_,_,_,bitset) ->
        if mem_bitset x1 bitset then f x1 1
    | Zs_enc enc1, Zs_one(sp2,x2) ->
        if mem_enc x2 enc1 then f x2 1
    | Zs_bitset(_,_,_,bitset), Zs_one(sp2,x2) ->
        if mem_bitset x2 bitset then f x2 1
    | Zs_enc _, Zs_enc _ ->
        let b_begin _ _ _ = () in
        let b_add_range _ start len = f start len in
        let b_end _ = () in
        let sp = min_space (space_enc_or_bitset z1) z2 in
        let i1 = iter_enc_or_bitset z1 in
        let i2 = iter_enc_or_bitset z2 in
        isect_iterable
          b_begin b_add_range b_end ()
          sp i1 i2
    | (Zs_enc _ | Zs_bitset _), Zs_bitset(_,_,_,bitset) ->
        iter_isect_bitset f z1 bitset
    | Zs_bitset(_,_,_,bitset), Zs_enc _ ->
        iter_isect_bitset f z2 bitset


let diff_iterable b_begin b_add_range b_end p zs_space i1 i2 =
  let b = b_begin p 64 zs_space in
  let r1 = i1.itr_ranges in
  let r2 = i2.itr_ranges in
  let k1 = ref 0 in
  let k2 = ref 0 in
  let s_cur = ref 1 in
  ( try
      iter_next i1;
      s_cur := r1.rstart.( !k1 );
      ( try iter_next i2 with End_of_iteration -> raise Output1 );
      while true do
        let s1 = r1.rstart.( !k1 ) in
        let e1 = s1 + r1.rlen.( !k1 ) - 1 in
        let s2 = r2.rstart.( !k2 ) in
        let e2 = s2 + r2.rlen.( !k2 ) - 1 in
        let s_isect = max s1 s2 in
        let e_isect = min e1 e2 in
        if s_isect <= e_isect then (          (* have isect *)
          let e_out = s_isect - 1 in
          if !s_cur <= e_out then             (* s_cur..just before the isect *)
            b_add_range b !s_cur (e_out - !s_cur + 1);
          s_cur := e_isect + 1;
        ) else
          if !s_cur < s2 then
            b_add_range b !s_cur (e1 - !s_cur + 1);
        if e1 <= e2 then (
          incr k1;
          if !k1 >= r1.rnum then (
            k1 := 0;
            iter_next i1;
          );
          s_cur := r1.rstart.( !k1 );
        );
        if e2 <= e1 then (
          incr k2;
          if !k2 >= r2.rnum then (
            try
              k2 := 0;
              iter_next i2;
              if e2 < s1 then
                iter_seek i2 s1;
            with End_of_iteration -> raise Output1
          )
        );
      done
    with
      | Output1 ->
          ( try
              let s1 = r1.rstart.( !k1 ) in
              let e1 = s1 + r1.rlen.( !k1 ) - 1 in
              if !s_cur <= e1 then
                b_add_range b !s_cur (e1 - !s_cur + 1);
              while true do
                incr k1;
                if !k1 >= r1.rnum then (
                  k1 := 0;
                  iter_next i1
                );
                b_add_range b r1.rstart.( !k1 ) r1.rlen.( !k1 );
              done
            with End_of_iteration -> ()
          )
      | End_of_iteration -> ()
  );
  b_end b


let build_one_enc p sp x =
  let b = build_begin p 1 sp in
  build_add b x;
  build_end_as_enc b


let diff p z1 z2 =
  match z1, z2 with
    | Zs_empty, _ ->
        Zs_empty
    | _, Zs_empty ->
        z1
    | Zs_one(sp1,x1), Zs_one(sp2,x2) ->
        if x1=x2 then
          Zs_empty
        else
          z1
    | Zs_one(sp1,x1), Zs_enc enc2 ->
        if mem_enc x1 enc2 then
          Zs_empty
        else
          z1
    | Zs_one(sp1,x1), Zs_bitset(_,_,_,bitset) ->
        if mem_bitset x1 bitset then
          Zs_empty
        else
          z1
    | Zs_enc enc1, Zs_one(sp2,x2) ->
        let enc2 = build_one_enc p sp2 x2 in
        diff_iterable
          build_begin build_add_range build_end p
          enc1.zs_space (iter_new enc1) (iter_new enc2)
    | Zs_bitset _, Zs_one(sp2,x2) ->
        let enc2 = build_one_enc p sp2 x2 in
        diff_iterable
          build_begin build_add_range build_end p
          (space_enc_or_bitset z1) (iter_enc_or_bitset z1) (iter_new enc2)
    | Zs_enc enc1, Zs_enc enc2 ->
        diff_iterable
          build_begin build_add_range build_end p
          enc1.zs_space (iter_new enc1) (iter_new enc2)
    | _ ->
        (* TODO: there could also be a special version for bitsets, as for
            isect
         *)
        let sp = space_enc_or_bitset z1 in
        let i1 = iter_enc_or_bitset z1 in
        let i2 = iter_enc_or_bitset z2 in
        diff_iterable
          build_begin build_add_range build_end p
          sp i1 i2

let union_iterable p zs_space i1 i2 =
  let b = build_begin p 64 zs_space in
  let limit = ref 1 in
  let add start len =
    let s = max start !limit in
    let e = start+len-1 in
    if e >= s then (
      limit := e+1;
      build_add_range b s (e-s+1)
    ) in
  let r1 = i1.itr_ranges in
  let r2 = i2.itr_ranges in
  let k1 = ref 0 in
  let k2 = ref 0 in
  ( try
      ( try iter_next i1 
        with End_of_iteration -> iter_next i2; raise Output2
      );
      ( try iter_next i2 with End_of_iteration -> raise Output1 );
      while true do
        let s1 = r1.rstart.( !k1 ) in
        while r2.rstart.( !k2 ) <= s1 do
          add r2.rstart.( !k2 ) r2.rlen.( !k2 );
          incr k2;
          if !k2 >= r2.rnum then (
            k2 := 0;
            ( try iter_next i2 with End_of_iteration -> raise Output1 );
          )
        done;
        let s2 = r2.rstart.( !k2 ) in
        while r1.rstart.( !k1 ) <= s2 do
          add r1.rstart.( !k1 ) r1.rlen.( !k1 );
          incr k1;
          if !k1 >= r1.rnum then (
            k1 := 0;
            ( try iter_next i1 with End_of_iteration -> raise Output2 );
          )
        done;
      done
    with
      | Output1 ->
          ( try
              while true do
                add r1.rstart.( !k1 ) r1.rlen.( !k1 );
                incr k1;
                if !k1 >= r1.rnum then (
                  k1 := 0;
                  iter_next i1
                )
              done
            with End_of_iteration -> ()
          )
      | Output2 ->
          ( try
              while true do
                add r2.rstart.( !k2 ) r2.rlen.( !k2 );
                incr k2;
                if !k2 >= r2.rnum then (
                  k2 := 0;
                  iter_next i2
                )
              done
            with End_of_iteration -> ()
          )
      | End_of_iteration -> ()
  );
  build_end b


let union p z1 z2 =
  (* TODO: implement union with bitset better:
      - copy the bitset
      - iterate over the other set, and set the bits
   *)
  match z1, z2 with
    | Zs_empty, _ ->
        z2
    | _, Zs_empty ->
        z1
    | Zs_one(sp1,x1), Zs_one(sp2,x2) ->
        if x1=x2 then
          Zs_one(max sp1 sp2, x1)
        else
          let enc1 = build_one_enc p sp1 x1 in
          let enc2 = build_one_enc p sp2 x2 in
          union_iterable p (max sp1 sp2) (iter_new enc1) (iter_new enc2)
    | Zs_one(sp1,x1), (Zs_enc _|Zs_bitset _) ->
        let enc1 = build_one_enc p sp1 x1 in
        let sp = max_space sp1 z2 in
        union_iterable p sp (iter_new enc1) (iter_enc_or_bitset z2)
    | (Zs_enc _|Zs_bitset _), Zs_one(sp2,x2) ->
        let enc2 = build_one_enc p sp2 x2 in
        let sp = max_space sp2 z1 in
        union_iterable p sp (iter_enc_or_bitset z1) (iter_new enc2)
    | (Zs_enc _|Zs_bitset _), (Zs_enc _|Zs_bitset _) ->
        let sp = max_space (space_enc_or_bitset z1) z2 in
        let i1 = iter_enc_or_bitset z1 in
        let i2 = iter_enc_or_bitset z2 in
        union_iterable p sp i1 i2

let union_many_heap p many zs_space =
  let itr0 = iter_none() in
  let itr =
    Array.map
      (function
        | Zs_empty -> itr0
        | Zs_one(_,id) -> itr0
        | Zs_enc enc -> iter_new enc
        | Zs_bitset(_,first,last,bitset) -> iter_new_bitset first last bitset
      )
      many in
  let idx = Array.map (fun _ -> 0) many in
  let cur =
    Array.mapi
      (fun i zs ->
         match zs with
           | Zs_empty -> 0
           | Zs_one(_,id) -> id
           | Zs_enc _
           | Zs_bitset _ -> itr.(i).itr_ranges.rstart.(0)
      )
      many in
  let module I =
    struct
      type t = int
      let compare i1 i2 =
        let s1 = cur.(i1) in
        let s2 = cur.(i2) in
        compare s1 s2
    end in
  let module U = Zutil.OrdUtil(I) in
  let heap = U.heap_create (Array.length many) (-1) in
  let () =
    Array.iteri
      (fun i zs ->
         if zs <> Zs_empty then
           U.heap_insert heap i
      )
      many in
  let b = build_begin p 64 (Space zs_space) in
  let limit = ref 1 in
  let add start len =
    let s = max start !limit in
    let e = start+len-1 in
    if e >= s then (
      limit := e+1;
      build_add_range b s (e-s+1)
    ) in
  while U.heap_length heap > 0 do
    let imin = U.heap_min heap in
    let itrmin = itr.(imin) in
    U.heap_delete_min heap;
    try
      let cont = ref true in
      while !cont do
        let kmin = idx.(imin) in
        let rmin = itrmin.itr_ranges in
        let start = cur.(imin) in
        let len = max 1 rmin.rlen.(kmin) in
        add start len;
        if kmin+1 >= rmin.rnum then (
          idx.(imin) <- 0;
          iter_next itrmin;  (* or End_of_iteration *)
          cur.(imin) <- rmin.rstart.(0)
        ) else (
          idx.(imin) <- kmin + 1;
          cur.(imin) <- rmin.rstart.(kmin+1)
        );
        let n = U.heap_length heap in
        cont := (n = 0) || I.compare imin (U.heap_min heap) <= 0;
      done;
      U.heap_insert heap imin
    with End_of_iteration ->
      ()
  done;
  build_end b


let union_many_bitset p many zs_space =
  let bitset = Bitset.create zs_space false in
  Array.iter
    (fun zs ->
       match zs with
         | Zs_bitset(_,first,last,bs) ->
             Bitset.union bitset bs (first-1) (last-first+1)
         | _ ->
             iter
               (fun start len ->
                  Bitset.fill_true bitset (start-1) len
               )
               zs
    )
    many;
  Bitset.set_runlengths bitset;
  if p.pool_enctype = Bitset_enc then (
    let c = Bitset.count bitset in
    assert (c > 0);
    (* because we don't call this fun when all inputs are empty *)
    let first = Bitset.runlength_false bitset 0 0 + 1 in
    let last =
      zs_space - Bitset.rev_runlength_false bitset (zs_space-1) 0 in
    Zs_bitset(c,first,last,bitset)
  )
  else
    let b = build_begin p 64 (Space zs_space) in
    Bitset.iter
      (fun start len ->
         build_add_range b (start+1) len
      )
      bitset;
    build_end b


let union_many p many =
  let many_a = Array.of_list many in
  let Space zs_space =
    Array.fold_left max_space (Space 0) many_a in
  if zs_space = 0 then
    Zs_empty
  else
    if Array.length many_a = 1 then
      many_a.(0)
    else (
      (* FIXME: The criterion when to prefer the bitset version is completely
         bogus
       *)
      let have_bitset =
        p.pool_enctype = Bitset_enc ||
          List.exists
            (function Zs_bitset _ -> true | _ -> false)
            many in
      let total =
        Array.fold_left
          (fun acc zs -> acc + count zs)
          0
          many_a in
      if have_bitset || total > zs_space/20 then
        union_many_bitset p many_a zs_space
      else
        union_many_heap p many_a zs_space
    )


let shift_space_enc p space zset offset =
  let Space sp = space in
  let size = Bigarray.Array1.dim zset.zs_data in
  let zset' = alloc_zset p size space zset.zs_enctype in
  let c = zset.zs_count in
  zset'.zs_count <- c;
  zset'.zs_first <- if c > 0 then zset.zs_first + offset else 0;
  zset'.zs_last <- if c > 0 then zset.zs_last + offset else 0;
  if c > 0 && (zset'.zs_first > sp || zset'.zs_last > sp) then
    failwith "Zcontainer.Zset.shift_space: outside space";
  let data = zset.zs_data in
  let data' = zset'.zs_data in
  Bigarray.Array1.blit data data';
  ( match zset.zs_enctype with
      | Packed_enc ->
          for k = 0 to size - 1 do
            let w = data.{k} in
            let ty = w lsr 56 in
            if ty <= 2 then (
              if ty = 0 then (
                let x' = (w land data_max) + offset in
                if x' > sp then
                  failwith "Zcontainer.Zset.shift_space: outside space";
                data'.{k} <- encode_type_0_int x'
              ) else 
                if ty = 1 then (
                  let x' =  (w land data_max) + offset in
                  if x' > sp then
                    failwith "Zcontainer.Zset.shift_space: outside space";
                  data'.{k} <- encode_type_1_int x'
                ) else (
                  assert(ty=2);
                  let x' =  (w land data_max) + offset in
                  if x' > sp then
                    failwith "Zcontainer.Zset.shift_space: outside space";
                  data'.{k} <- encode_type_2_int x'
                )
            )
          done;
      | Array_enc ->
          for k = 0 to (size/2) - 1 do
            let x' = data'.{ k lsl 1 } + offset in
            if x' > sp then
              failwith "Zcontainer.Zset.shift_space: outside space";
            data'.{ k lsl 1 } <- x'
          done
      | Bitset_enc ->
          assert false

  );
  set_index zset';
  zset'


let shift_space p space zset offset =
  let Space sp = space in
  match zset with
    | Zs_empty ->
        Zs_empty
    | Zs_one(_, x) ->
        if x + offset > sp then
          failwith "Zcontainer.Zset.shift_space: outside space";
        Zs_one(space, x+offset)
    | Zs_enc enc ->
        Zs_enc (shift_space_enc p space enc offset)
    | Zs_bitset _ ->
        (* this function is not directly exported, and we don't need bitsets
           for our library-internal application
         *)
        assert false


let byte_size =
  function
  | Zs_empty -> 0
  | Zs_one _ -> 8
  | Zs_enc enc -> Bigarray.Array1.dim enc.zs_data lsl 3
  | Zs_bitset(_,_,_,bitset) -> 4 * Bitset.length bitset


let write_file_ba fd data =
  let mem = Netsys_mem.memory_of_bigarray_1 data in
  let size = Bigarray.Array1.dim mem in
  let k = ref 0 in
  while !k < size do
    let n = Netsys_mem.mem_write fd mem !k (size - !k) in
    k := !k + n
  done


let write_file_str fd data =
  let size = String.length data in
  let k = ref 0 in
  while !k < size do
    let n = Unix.single_write fd data !k (size - !k) in
    k := !k + n
  done


let write_file fd =
  function
  | Zs_empty ->
      ()
  | Zs_one(Space sp,x) ->
      let s = String.make 8 '\000' in
      let w =
        if x = 1 then
          encode_type_1_int x
        else if x = sp then
          encode_type_2_int x
        else
          encode_type_0_int x in
      Netnumber.HO.write_int8 s 0 (Netnumber.int8_of_int w);
      write_file_str fd s
  | Zs_enc enc ->
      if enc.zs_enctype = Array_enc then
        failwith "Zset.write_file: array-encoded set cannot be written";
      write_file_ba fd enc.zs_data
  | Zs_bitset _ ->
      failwith "Zset.write_file: bitset cannot be written"



let to_data data offset =
  function
  | Zs_empty ->
      ()
  | Zs_one(Space sp,x) ->
      let w =
        if x = 1 then
          encode_type_1_int x
        else if x = sp then
          encode_type_2_int x
        else
          encode_type_0_int x in
      data.{ offset } <- w
  | Zs_enc enc ->
      if enc.zs_enctype = Array_enc then
        failwith "Zset.to_data: array-encoded set cannot be written";
      let size = Bigarray.Array1.dim enc.zs_data in
      Bigarray.Array1.blit
        enc.zs_data
        (Bigarray.Array1.sub data offset size)
  | Zs_bitset _ ->
      failwith "Zset.to_data: bitset cannot be written"

let internal_data =
  function
  | Zs_enc enc ->
      enc.zs_data
  | _ -> assert false
        

let from_data data offset size space count first last =
  if size < 0 then invalid_arg "Zcontainer.Zset.read_file";
  if count < 0 then invalid_arg "Zcontainer.Zset.read_file";
  if size=0 then
    Zs_empty
  else (
    let Space end_id = space in
    if first < 1 || first > end_id then invalid_arg "Zcontainer.Zset.read_file";
    if last < 1 || last > end_id then invalid_arg "Zcontainer.Zset.read_file";
    if first > last then invalid_arg "Zcontainer.Zset.read_file";
    if count = 1 then
      Zs_one(space, first)
    else
      let zset =
        { zs_data = Bigarray.Array1.sub data offset size;
          zs_index = [| |];
          zs_count = count;
          zs_first = first;
          zs_last = last;
          zs_space = space;
          zs_enctype = Packed_enc;
        } in
      set_index zset;
      Zs_enc zset
  )


let filter pool f set =
  match set with
    | Zs_empty ->
        Zs_empty
    | Zs_one(sp,id) ->
        if f id then set else Zs_empty
    | Zs_enc _ 
    | Zs_bitset _ ->
        let bset = build_begin pool 100 (space_enc_or_bitset set) in
        iter
          (fun start len ->
             for id = start to start + len - 1 do
               if f id then
                 build_add bset id
             done
          )
          set;
        build_end bset

let filter_range pool f set =
  (* calls [f add start len] for all (start,len) ranges. The function [f]
     can call back [add s l] to add a (s,l) range to the output, if necessary
     several times
   *)
  match set with
    | Zs_empty ->
        Zs_empty
    | Zs_one(sp,id) ->
        let bset = build_begin pool 100 sp in
        f (build_add_range bset) id 1;
        build_end bset
    | Zs_enc _
    | Zs_bitset _ ->
        let bset = build_begin pool 100 (space_enc_or_bitset set) in
        let add = build_add_range bset in
        iter
          (fun start len ->
             f add start len
          )
          set;
        build_end bset

let copy pool z =
  let do_copy =
    match z with
      | Zs_enc _ -> true
      | Zs_bitset _ -> pool.pool_enctype <> Bitset_enc
      | _ -> false in
  if do_copy then (
    let bset = build_begin pool 100 (space_enc_or_bitset z) in
    iter
      (fun start len ->
         build_add_range bset start len
      )
      z;
    build_end bset
  )
  else z

let to_array =
  function
  | Zs_empty ->
      [| |]
  | Zs_one(_, int) ->
      [| int |]
  | Zs_enc zset ->
      ( match zset.zs_enctype with
          | Packed_enc ->
              let ranges = create_ranges() in
              let c = zset.zs_count in
              let p = Array.make c 0 in
              let i = ref 0 in
              let prev = ref 0 in
              for k = 0 to Bigarray.Array1.dim zset.zs_data - 1 do
                let w = zset.zs_data.{ k } in
                decode zset.zs_first zset.zs_last !prev ranges w;
                for r = 0 to ranges.rnum-1 do
                  let s = ranges.rstart.(r) in
                  let l = ranges.rlen.(r) in
                  for q = 0 to l - 1 do
                    assert( !i < c );
                    p.( !i ) <- s + q;
                    incr i
                  done;
                  prev := s + l - 1
                done
              done;
              assert(!i = c);
              p
          | Array_enc ->
              let c = zset.zs_count in
              let p = Array.make c 0 in
              let rnum = (Bigarray.Array1.dim zset.zs_data) / 2 in
              let data = zset.zs_data in
              let i = ref 0 in
              for k = 0 to rnum - 1 do
                let s = data.{ k lsl 1 } in
                let l = data.{ k lsl 1 + 1 } in
                for q = 0 to l - 1 do
                  assert( !i < c );
                  p.( !i ) <- s + q;
                  incr i
                done;
              done;
              assert(!i = c);
              p
          | Bitset_enc -> assert false
      )
  | Zs_bitset(count,first,last,bitset) as z ->
      let p = Array.make count 0 in
      let j = ref 0 in
      iter
        (fun start len ->
           for k = start to start+len-1 do
             p.( !j ) <- k;
             incr j
           done
        )
        z;
      assert(!j = count);
      p
      

let to_range_arrays =
  function
  | Zs_empty ->
      ([| |], [| |])
  | Zs_one(_, int) ->
      ([| int |], [| 1 |])
  | Zs_enc zset ->
      ( match zset.zs_enctype with
          | Packed_enc ->
              let ranges = create_ranges() in
              let rnum = ref 0 in
              for k = 0 to Bigarray.Array1.dim zset.zs_data - 1 do
                let w = zset.zs_data.{ k } in
                rnum := !rnum + decode_rnum w
              done;
              let rstart = Array.make !rnum 0 in
              let rlen = Array.make !rnum 0 in
              let i = ref 0 in
              let prev = ref 0 in
              for k = 0 to Bigarray.Array1.dim zset.zs_data - 1 do
                let w = zset.zs_data.{ k } in
                decode zset.zs_first zset.zs_last !prev ranges w;
                let n = ranges.rnum in
                Array.blit ranges.rstart 0 rstart !i n;
                Array.blit ranges.rlen 0 rlen !i n;
                i := !i + n;
                prev := ranges.rstart.(n-1) + ranges.rlen.(n-1) - 1
              done;
              assert(!i = !rnum);
              (rstart, rlen)
          | Array_enc ->
              let rnum = (Bigarray.Array1.dim zset.zs_data) / 2 in
              let rstart = Array.make rnum 0 in
              let rlen = Array.make rnum 0 in
              let data = zset.zs_data in
              for k = 0 to rnum - 1 do
                rstart.(k) <- data.{ k lsl 1 };
                rlen.(k) <- data.{ k lsl 1 + 1 };
              done;
              (rstart, rlen)
          | Bitset_enc -> assert false
      )
  | Zs_bitset _ as z ->
      let c = ref 0 in
      iter (fun start len -> incr c) z;
      let rstart = Array.make !c 0 in
      let rlen = Array.make !c 0 in
      let i = ref 0 in
      iter
        (fun start len ->
           rstart.( !i ) <- start;
           rlen.( !i ) <- len;
           incr i
        )
        z;
      assert(!i = !c);
      (rstart, rlen)


let write_tf f zset =
  let by = Bytes.create 16 in
  let sp =
    match zset with
      | Zs_empty ->
          output_char f 'E';
          0
      | Zs_one(Space sp,_) ->
          output_char f '1';
          sp
      | Zs_enc zs ->
          ( match zs.zs_enctype with
              | Packed_enc -> output_char f 'P'
              | Array_enc -> output_char f 'A'
              | _ -> assert false
          );
          let Space sp = zs.zs_space in
          sp
      | Zs_bitset(_,_,_,bs) ->
          output_char f 'B';
          Bitset.length bs in
  let n0 = Zencoding.LenPre3.encode_length sp in
  Zencoding.LenPre3.encode_bytes by 0 n0 sp;
  output f by 0 n0;
  let p = ref 0 in
  iter
    (fun start len ->
       let d = start - !p in
       let n1 = Zencoding.LenPre3.encode_length d in
       Zencoding.LenPre3.encode_bytes by 0 n1 d;
       let n2 = Zencoding.LenPre3.encode_length len in
       Zencoding.LenPre3.encode_bytes by n1 n2 len;
       output f by 0 (n1+n2);
       p := start
    )
    zset;
  let n1 = Zencoding.LenPre3.encode_length 0 in
  Zencoding.LenPre3.encode_bytes by 0 n1 0;
  output f by 0 n1

let read_enctype_tf f =
  match input_char f with
    | 'E' -> Packed_enc
    | '1' -> Packed_enc
    | 'P' -> Packed_enc
    | 'A' -> Array_enc
    | 'B' -> Bitset_enc
    | _ -> failwith "enctype_tf"
         

let read_tf pool f =
  let by = Bytes.create 8 in
  let read_int() =
    really_input f by 0 1;
    let n = Zencoding.LenPre3.decode_bytes_length by 0 in
    really_input f by 1 (n-1);
    Zencoding.LenPre3.decode_bytes by 0 in
  let sp = read_int() in
  let b = build_begin pool 1000 (Space sp) in
  let p = ref 0 in
  let k = ref(read_int()) in
  while !k > 0 do
    let len = read_int() in
    let start = !p + !k in
    build_add_range b start len;
    p := start;
    k := read_int()
  done;
  build_end b


let persist zset =
  match zset with
    | Zs_empty ->
        let hd = Bytes.create 256 in
        Bytes.blit_string "ZSETEM00" 0 hd 0 8;
         let (alldata, name) =
           Zutil.Persist.create hd Bigarray.char Bigarray.c_layout 0 in
         name
    | Zs_one(Space n,k) ->
        let hd = Bytes.create 256 in
        Bytes.blit_string "ZSETON00" 0 hd 0 8;
        Netnumber.HO.write_int8 hd 8 (Netnumber.int8_of_int n);
        Netnumber.HO.write_int8 hd 16 (Netnumber.int8_of_int k);
         let (alldata, name) =
           Zutil.Persist.create hd Bigarray.char Bigarray.c_layout 0 in
         name
    | Zs_enc enc ->
        let hd = Bytes.create 256 in
        Bytes.blit_string "ZSETEN00" 0 hd 0 8;
        let size = Bigarray.Array1.dim enc.zs_data in
        let Space n = enc.zs_space in
        Netnumber.HO.write_int8 hd 8 (Netnumber.int8_of_int size);
        Netnumber.HO.write_int8 hd 16 (Netnumber.int8_of_int n);
        Netnumber.HO.write_int8 hd 24 (Netnumber.int8_of_int enc.zs_count);
        Netnumber.HO.write_int8 hd 32 (Netnumber.int8_of_int enc.zs_first);
        Netnumber.HO.write_int8 hd 40 (Netnumber.int8_of_int enc.zs_last);
        let (data, name) =
          Zutil.Persist.create hd Bigarray.int Bigarray.c_layout size in
        to_data data 0 zset;
        name
    | Zs_bitset(count,first,last,bs) ->
        let name =
          Zutil.Bitset.persist
            ~fill_header:(fun hd ->
              Netnumber.HO.write_int8 hd 24 (Netnumber.int8_of_int count);
              Netnumber.HO.write_int8 hd 32 (Netnumber.int8_of_int first);
              Netnumber.HO.write_int8 hd 40 (Netnumber.int8_of_int last);
            )
            bs in
        name

let unpersist name =
  Zutil.Persist.delete name

let access name =
  let hd = Zutil.Persist.access_header name in
  let magic = Bytes.sub_string hd 0 8 in
  match magic with
    | "ZSETEM00" ->
        Zs_empty
    | "ZSETON00" ->
        let n =
          Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 8) in
        let k =
          Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 16) in
        Zs_one(Space n,k)
    | "ZSETEN00" ->
        let size =
          Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 8) in
        let n =
          Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 16) in
        let count =
          Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 24) in
        let first =
          Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 32) in
        let last =
          Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 40) in
        let data =
          Zutil.Persist.access name Bigarray.int Bigarray.c_layout size in
        from_data data 0 size (Space n) count first last
    | "ZSETBS00" ->
        let count =
          Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 24) in
        let first =
          Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 32) in
        let last =
          Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 40) in
        let bs =
          Zutil.Bitset.access name hd in
        Zs_bitset(count,first,last,bs)
    | _ ->
        failwith "Zcontainer.Zset.access: bad magic"
