(* Whole tables *)

open Ztypes

type zdatabox =
  | ZdataBox : 'a ztype * 'a Zcol.zcol -> zdatabox

type ztable =
    { zt_stdrepr : stdrepr;
      zt_space : space;
      zt_dir : string option;
      zt_cols : (string,zcolbox option) Hashtbl.t;    (* the schema *)
      zt_data : (string,zdatabox option) Hashtbl.t;   (* the data *)
      zt_build : (string,bool) Hashtbl.t;             (* whether being built *)
      zt_over : ztable option;
    }

let underlying_ztable tab = tab.zt_over

let path_name tab = tab.zt_dir
      
let decompose_bundle_name s =
  try
    let j = String.rindex s '[' in
    let b = Buffer.create 10 in
    let k = ref (j+1) in
    while !k < String.length s && s.[!k] <> ']' do
      match s.[!k] with
        | '0'..'9' -> Buffer.add_char b s.[!k]; incr k
        | _ -> raise Not_found
    done;
    if !k <> String.length s - 1 || s.[!k] <> ']' then raise Not_found;
    let n = String.sub s 0 j in
    Some(n, int_of_string (Buffer.contents b))
  with
    | Not_found ->
        None

let rec contains_zcol : type t . t Zcol.zcol -> _ -> bool =
  fun zcol ztab ->
    let ZcolDescr(name,ty,_,_) = Zcol.(zcol.descr) in
    try
      match decompose_bundle_name name with
        | None ->
            ( match Hashtbl.find ztab.zt_data name with
                | Some(ZdataBox(ty1,col1)) ->
                    ( match same_ztype ty ty1 with
                        | Equal ->
                            col1 == zcol
                        | Not_equal ->
                            false
                    )
                | None -> false
            )
        | Some(bname,idx) ->
            ( match Hashtbl.find ztab.zt_data bname with
                | Some(ZdataBox(Zbundle ty1,col1)) ->
                    ( match same_ztype ty ty1 with
                        | Equal ->
                            ( match Zcol.containing_bundle zcol with
                                | None ->
                                    false
                                | Some zcol_bundle ->
                                    col1 == zcol_bundle
                            )
                        | Not_equal ->
                            false
                    )
                | _ -> false
            )
    with
      | Not_found ->
          ( match ztab.zt_over with
              | None -> false
              | Some ztab' -> contains_zcol zcol ztab'
          )


let rec contains_zcol_name name ztab =
  try
    let n =
      match decompose_bundle_name name with
        | None -> name
        | Some(n,_) -> n in
    Hashtbl.find ztab.zt_cols n <> None
  with Not_found ->
    (match ztab.zt_over with
       | None -> false
       | Some ztab' -> contains_zcol_name name ztab'
    )

let write_schema dir cols =
  let filename = Filename.concat dir "schema.json" in
  let columns_l =
    List.map
      (fun (name,col) ->
         match col with
           | None ->
               (name, `Assoc [])
           | Some(ZcolBox(name,ty,repr,zo)) ->
               let j =
                 `Assoc [ "type", `String (string_of_ztype ty);
                          "repr", `String (string_of_zrepr repr);
                          "order", `String zo.ord_name;
                        ] in
               (name, j)
      )
      cols in
  let j =
    `Assoc [ "columns", `Assoc columns_l ] in
  Yojson.Basic.to_file filename j


let jstring =
  function
  | `String s -> s
  | _ -> raise Not_found

let read_schema stdrepr dir =
  let filename = Filename.concat dir "schema.json" in
  let j = Yojson.Basic.from_file filename in
  try
    match j with
      | `Assoc l1 ->
          let columns_a =
            try List.assoc "columns" l1
            with Not_found ->
                 failwith "Missing entry: columns" in
          let columns_l =
            match columns_a with
              | `Assoc columns_l -> columns_l
              | _ -> raise Not_found in
          List.map
            (fun (name, j_a) ->
               let j_l =
                 match j_a with
                   | `Assoc j_l -> j_l
                   | _ -> raise Not_found in
               if j_l = [] then
                 (name, None)
               else
                 let type_s =
                   try jstring (List.assoc "type" j_l)
                   with Not_found ->
                     failwith "Missing entry: columns[_].type" in
                 let repr_s =
                   try jstring (List.assoc "repr" j_l)
                   with Not_found ->
                     failwith "Missing entry: columns[_].repr" in
                 let order_s =
                   try jstring (List.assoc "order" j_l)
                   with Not_found ->
                     failwith "Missing entry: columns[_].order" in
                 let ztypebox = ztypebox_of_string type_s in
                 let ZtypeBox ty1 = ztypebox in
                 let zreprbox = zreprbox_of_string stdrepr repr_s in
                 let ZreprBox(ty2, repr) = zreprbox in
                 let zo = zorder_of_string stdrepr ty1 order_s in
                 ( match same_ztype ty1 ty2 with
                     | Equal ->
                         (name, Some(ZcolBox(name, ty1, repr, zo)))
                     | Not_equal ->
                         failwith "type/order mismatch"
                 )
            )
            columns_l
      | _ ->
          raise Not_found
  with
    | Not_found ->
        failwith ("Bad format, in file: " ^ filename)
    | Failure msg ->
        failwith (msg ^ ", in file: " ^ filename)


let write_stats dir space =
  let Space sp = space in
  let filename = Filename.concat dir "stats.json" in
  let j =
    `Assoc [ "space", `Int sp ] in
  Yojson.Basic.to_file filename j


let read_stats dir =
  let filename = Filename.concat dir "stats.json" in
  let j = Yojson.Basic.from_file filename in
  match j with
    | `Assoc [ "space", `Int sp ] ->
        Space sp
    | _ ->
        failwith ("Bad format, in file: " ^ filename)


let write zt =
  match zt.zt_dir with
    | None ->
        ()
    | Some dir ->
        let cols =
          Hashtbl.fold (fun name col acc -> (name,col)::acc) zt.zt_cols [] in
        write_schema dir cols

let space_ok zt_space =
  function
  | None -> true
  | Some ztab ->
      zt_space = ztab.zt_space

let create_memory_ztable ?over zt_stdrepr zt_space =
  if not(space_ok zt_space over) then
    failwith "Zcontainer.Ztable.create_memory_ztable: overlay needs to have \
              the same space as the original table";
  { zt_stdrepr;
    zt_space; 
    zt_dir = None;
    zt_cols = Hashtbl.create 11;
    zt_data = Hashtbl.create 11;
    zt_build = Hashtbl.create 11;
    zt_over = over;
  }

let create_file_ztable ?over zt_stdrepr zt_space dir =
  if not(space_ok zt_space over) then
    failwith "Zcontainer.Ztable.create_file_ztable: overlay needs to have \
              the same space as the original table";
  Unix.mkdir dir 0o777;
  write_schema dir [];
  write_stats dir zt_space;
  { zt_stdrepr;
    zt_space; 
    zt_dir = Some dir;
    zt_cols = Hashtbl.create 11;
    zt_data = Hashtbl.create 11;
    zt_build = Hashtbl.create 11;
    zt_over = over;
  }

let from_file ?over zt_stdrepr dir =
  let space = read_stats dir in
  let cols = read_schema zt_stdrepr dir in
  let zt_cols = Hashtbl.create 11 in
  List.iter
    (fun (name,col_opt) ->
       Hashtbl.add zt_cols name col_opt
    )
    cols;
  if not(space_ok space over) then
    failwith "Zcontainer.Ztable.from_file: overlay needs to have \
              the same space as the original table";
  { zt_stdrepr;
    zt_space = space;
    zt_dir = Some dir;
    zt_cols;
    zt_data = Hashtbl.create 11;   (* lazily filled *)
    zt_build = Hashtbl.create 11;
    zt_over = over;
  }

let build_zinvcol_1 : type t . ztable -> string -> t ztype -> t zrepr ->
                           t zorder -> 
                           t Zinvcol.zinvcol_builder =
  fun zt name ty zrepr zo ->
    if Hashtbl.mem zt.zt_cols name then
      failwith ("Zcontainer.Ztable.build: column exists: " ^ name);
    let descr = ZcolDescr(name,ty,zrepr,zo) in
    let descrbox = ZcolBox(name,ty,zrepr,zo) in
    Hashtbl.replace zt.zt_cols name (Some descrbox);
    Hashtbl.replace zt.zt_build name true;
    let b =
      match zt.zt_dir with
        | None ->
            Zinvcol.create_memory_zinvcol_builder 1000 1000 zt.zt_space descr
        | Some dir ->
            Zinvcol.create_file_zinvcol_builder dir 1000 1000
                                                zt.zt_space descr in
    let on_done zinvcol =
      let zcol = Zcol.of_zinvcol descr zinvcol in
      Hashtbl.replace zt.zt_build name false;
      Hashtbl.replace zt.zt_data name (Some(ZdataBox(ty,zcol)));
      write zt in
    Zinvcol.set_b_done b on_done;
    b

let build_any_zinvcol zt name ty repr zo =
  if zt.zt_dir <> None then
    failwith "Zcontainer.Ztable.build_any_zinvcol: restricted to memory tables";
  build_zinvcol_1 zt name ty repr zo

let build_zinvcol : type t . ztable -> string -> t ztype -> string -> string ->
                         t Zinvcol.zinvcol_builder =
  fun zt name ty1 repr_name zo_name ->
    let zreprbox = zreprbox_of_string zt.zt_stdrepr repr_name in
    let ZreprBox(ty2, repr) = zreprbox in
    let zo = zorder_of_string zt.zt_stdrepr ty1 zo_name in
    ( match same_ztype ty1 ty2 with
        | Equal ->
            build_zinvcol_1 zt name ty1 repr zo
        | Not_equal ->
            failwith "Zcontainer.Ztable.build_zinvcol: \
                      type/order mismatch"
    )

let add_vector zt name dir vec =
  let dcol = Zdircol.from_vector name dir vec in
  let descr = Zdircol.(dcol.dc_type) in
  let zcol = Zcol.of_zdircol descr dcol in
  let ZcolDescr(name,ty,zrepr,zo) = descr in
  let descrbox = ZcolBox(name,ty,zrepr,zo) in
  Hashtbl.replace zt.zt_cols name (Some descrbox);
  Hashtbl.replace zt.zt_build name false;
  Hashtbl.replace zt.zt_data name (Some(ZdataBox(ty,zcol)));
  zcol

let build_zdircol_1 : type t . ztable -> string -> t ztype -> t zrepr ->
                           t zorder -> 
                           t Zdircol.zdircol_builder =
  fun zt name ty zrepr zo ->
    if Hashtbl.mem zt.zt_cols name then
      failwith ("Zcontainer.Ztable.build: column exists: " ^ name);
    let descr = ZcolDescr(name,ty,zrepr,zo) in
    let descrbox = ZcolBox(name,ty,zrepr,zo) in
    Hashtbl.replace zt.zt_cols name (Some descrbox);
    Hashtbl.replace zt.zt_build name true;
    let b =
      match zt.zt_dir with
        | None ->
            Zdircol.create_memory_zdircol_builder 1000 1000 zt.zt_space descr
        | Some dir ->
            Zdircol.create_file_zdircol_builder dir 1000 1000
                                                zt.zt_space descr in
    let on_done zdircol =
      let zcol = Zcol.of_zdircol descr zdircol in
      Hashtbl.replace zt.zt_build name false;
      Hashtbl.replace zt.zt_data name (Some(ZdataBox(ty,zcol)));
      write zt in
    Zdircol.(b.b_done <- on_done);
    b
    

let build_any_zdircol zt name ty repr zo =
  if zt.zt_dir <> None then
    failwith "Zcontainer.Ztable.build_any_zdircol: restricted to memory tables";
  build_zdircol_1 zt name ty repr zo

let build_zdircol : type t . ztable -> string -> t ztype -> string -> string ->
                         t Zdircol.zdircol_builder =
  fun zt name ty1 repr_name zo_name ->
    let zreprbox = zreprbox_of_string zt.zt_stdrepr repr_name in
    let ZreprBox(ty2, repr) = zreprbox in
    let zo = zorder_of_string zt.zt_stdrepr ty1 zo_name in
    ( match same_ztype ty1 ty2 with
        | Equal ->
            build_zdircol_1 zt name ty1 repr zo
        | Not_equal ->
            failwith "Zcontainer.Ztable.build_zdircol: \
                      type/order mismatch"
    )

let space zt = zt.zt_space

let columns zt =
  let rec cols ht zt =
    let names =
      Hashtbl.fold (fun name _ acc -> name::acc) zt.zt_cols [] in
    let names =
      List.filter (fun n -> not(Hashtbl.mem ht n)) names in
    names @
      match zt.zt_over with
        | None -> []
        | Some ztab ->
            List.iter (fun n -> Hashtbl.add ht n ()) names;
            cols ht ztab in
  cols (Hashtbl.create 7) zt


(* TODO: raise Column_not_found *)

let rec get_zcolbox1 zt name =
  match Hashtbl.find zt.zt_cols name with
    | None -> raise Not_found
    | Some zcolbox -> zcolbox
    | exception Not_found ->
        ( match zt.zt_over with
            | None -> raise Not_found
            | Some zt' -> get_zcolbox1 zt' name
        )

let bundle_zcolbox zt name k =
  let ZcolBox(n,ty,_,ord) = get_zcolbox1 zt name in
  match ty with
    | Zbundle ty1 ->
        let ord1 = extract_ord_from_bundle ty1 ord in
        ZcolBox(Printf.sprintf "%s[%d]" n k, ty1, Virtual_column, ord1)
    | _ ->
        failwith "Zcontainer.bundle_zcolbox: this column is not a bundle"

let get_zcolbox zt name =
  match decompose_bundle_name name with
    | Some(n,k) -> bundle_zcolbox zt n k
    | None -> get_zcolbox1 zt name


let rec get_zcol1 : type t . ztable -> t ztype -> string -> t Zcol.zcol =
  fun zt ty name ->
    let zcolbox = get_zcolbox1 zt name in
    let ZcolBox(_,ty',repr,zo) = zcolbox in
    match same_ztype ty ty' with
      | Equal ->
          let b =
            try Hashtbl.find zt.zt_build name
            with Not_found -> false in
          if b then
            failwith "Zcontainer.Ztable.get_zcol: column is being built";
          ( match Hashtbl.find zt.zt_data name with
              | None -> assert false
              | Some(ZdataBox(ty'',zcol)) ->
                  ( match same_ztype ty ty'' with
                      | Equal -> zcol
                      | Not_equal -> assert false
                  )
              | exception Not_found ->
                  if Hashtbl.mem zt.zt_cols name then (
                    let dir =
                      match zt.zt_dir with
                        | Some dir -> dir
                        | None -> assert false in
                    let descr = ZcolDescr(name,ty,repr,zo) in
                    let zcol =
                      Zcol.from_file dir zt.zt_space descr in
                    Hashtbl.add zt.zt_data name (Some(ZdataBox(ty,zcol)));
                    zcol
                  ) else
                    match zt.zt_over with
                      | None -> assert false
                      | Some zt' -> get_zcol1 zt' ty name
          )  
      | Not_equal ->
          failwith "Zcontainer.Ztable.get_zcol: type mismatch"

let bundle_zcol zt ty name k =
  let zcol = get_zcol1 zt (Zbundle ty) name in
  Zcol.bundle_element zcol k

let get_zcol zt ty name =
  match decompose_bundle_name name with
    | Some(n,k) -> bundle_zcol zt ty n k
    | None -> get_zcol1 zt ty name

let check_type : type t . t ztype -> zcolbox array -> bool =
  fun ty cols ->
    try
      Array.iter
        (function ZcolBox(_,ty',_,_) ->
           match same_ztype ty ty' with
             | Equal -> ()
             | Not_equal -> raise Not_found
        )
        cols;
      true
    with Not_found -> false


let merge_1 zt cols zt_inputs =
  List.iter
    (function ZcolBox(name,ty,_,_) ->
       let zt_zcolboxes = Array.map (fun zt -> get_zcolbox zt name) zt_inputs in
       if not (check_type ty zt_zcolboxes) then
         failwith("Zcontainer.Ztable.merge: types not identical for column: " ^
                    name);
       (* TODO: similar prechecks for repr, order *)
    )
    cols;
  List.iter
    (fun col ->
       let ZcolBox(name,ty,repr,zo) = col in
       (* FIXME: check that repr and zo are in stdrepr *)
       let in_zcols =
         Array.map
           (fun in_zt ->
              get_zcol in_zt ty name
           )
           zt_inputs in
       match repr with
         | Inv_multibyte _
         | SInv_multibyte _ ->
             let b = build_zinvcol_1 zt name ty repr zo in
             let in_zinvcols =
               Array.map
                 (fun in_zcol ->
                    match Zcol.content in_zcol with
                      | Zcol.Inv zinvcol -> zinvcol
                      | _ ->
                          failwith("Zcontainer.Ztable.merge: inverted column \
                                    expected, for column: " ^ name)
                 )
                 in_zcols in
             Zinvcol.merge b in_zinvcols;
             ignore(Zinvcol.build_end b)
         | Dir_multibyte _
         | Dir_fixed64 _ ->
             let b = build_zdircol_1 zt name ty repr zo in
             let in_zdircols =
               Array.map
                 (fun in_zcol ->
                    match Zcol.content in_zcol with
                      | Zcol.Dir zdircol -> zdircol
                      | _ ->
                          failwith("Zcontainer.Ztable.merge: direct column \
                                    expected, for column: " ^ name)
                 )
                 in_zcols in
             Zdircol.merge b in_zdircols;
             ignore(Zdircol.build_end b)
         | Dir_bigarray _
         | Virtual_column ->
             assert false
    )
    cols

let zcolbox stdrepr (name,tybox,repr_name,zo_name) =
  let ZtypeBox ty1 = tybox in
  let zreprbox = zreprbox_of_string stdrepr repr_name in
  let ZreprBox(ty2, repr) = zreprbox in
  let zo = zorder_of_string stdrepr ty1 zo_name in
  ( match same_ztype ty1 ty2 with
      | Equal ->
          ZcolBox(name,ty1,repr,zo)
      | Not_equal ->
          failwith "Zcontainer.Ztable.zcolbox: \
                    type/order mismatch"
  )


let merge zt cols inputs =
  let cols1 = List.map (zcolbox zt.zt_stdrepr) cols in
  merge_1 zt cols1 inputs


let remove ?(force=false) zt =
  match zt.zt_dir with
    | None -> ()
    | Some dir ->
        Hashtbl.iter
          (fun name zcolbox ->
             match zcolbox with
             | Some(ZcolBox(_,_,repr,_)) ->
                 ( match repr with
                     | Inv_multibyte _
                     | SInv_multibyte _ ->
                         Zinvcol.remove ~force dir name
                     | Dir_multibyte _
                     | Dir_fixed64 _ ->
                         Zdircol.remove ~force dir name
                     | Dir_bigarray _
                     | Virtual_column ->
                         ()
                 )
             | None ->
                 ()
          )
          zt.zt_cols;
        Hashtbl.clear zt.zt_cols;
        Hashtbl.clear zt.zt_data;
        Hashtbl.clear zt.zt_build;
        Zutil.FileUtil.rm ~force (Filename.concat dir "schema.json");
        Zutil.FileUtil.rm ~force (Filename.concat dir "stats.json");
        Zutil.FileUtil.rmdir ~force dir


let remove_column ?(force=false) zt name =
  ( match zt.zt_dir with
      | None -> ()
      | Some dir ->
          let zcolbox =
            try Hashtbl.find zt.zt_cols name
            with Not_found ->
              failwith ("Zcontainer.Ztable.remove_column: No such column") in
          write zt;
          match zcolbox with
            | None -> ()
            | Some(ZcolBox(_,_,repr,_)) ->
                match repr with
                  | Inv_multibyte _
                  | SInv_multibyte _ ->
                      Zinvcol.remove ~force dir name
                  | Dir_multibyte _
                  | Dir_fixed64 _ ->
                      Zdircol.remove ~force dir name
                  | Dir_bigarray _
                  | Virtual_column ->
                      ()
  );
  Hashtbl.replace zt.zt_cols name None;
  Hashtbl.replace zt.zt_data name None;
  Hashtbl.replace zt.zt_build name false
