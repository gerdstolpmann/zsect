open Ztypes

type _ grouping =
  | Base : 'a Zcol.zcol * direction -> 'a grouping
  | Base_some : 'a option Zcol.zcol * direction -> 'a grouping
  | Also : 'a Zcol.zcol * direction * 'b grouping -> ('a * 'b) grouping
  | Also_some : 'a option Zcol.zcol * direction * 'b grouping -> ('a * 'b) grouping

let group_by col dir =
  Base(col,dir)

let group_some_by col dir =
  Base_some(col,dir)

let also_by col dir gspec =
  Also(col,dir,gspec)

let also_some_by col dir gspec =
  Also_some(col,dir,gspec)

let revdir =
  function
  | ASC -> DESC
  | DESC -> ASC

let rec reverse : type t . t grouping -> t grouping =
  function
  | Base(col,dir) -> Base(col,revdir dir)
  | Base_some(col,dir) -> Base_some(col,revdir dir)
  | Also(col,dir,gspec) -> Also(col,revdir dir,reverse gspec)
  | Also_some(col,dir,gspec) -> Also_some(col,revdir dir,reverse gspec)

type 'a group =
  { gvalue : 'a Lazy.t;
    gset : Zset.zset Lazy.t;
    gcycled : bool;
  }

let iter_start itr dir =
  match dir with
    | ASC -> itr # go_beginning()
    | DESC -> itr # go_end()

let iter_advance itr dir =
  match dir with
    | ASC -> itr # next()
    | DESC -> itr # prev()
                  
let base_stream base pool ztab col dir =
  let inv = Zcol.invert col base in
  let itr = Zcol.iter_values inv in
  (fun gcycled ->
     iter_start itr dir;
     let gcycled = ref gcycled in
     Stream.from
       (fun _ ->
          if itr#beyond then
            None
          else (
            let cur_hv = itr#current_value_lz in
            let g =
              { gvalue = lazy (snd (Lazy.force cur_hv));
                gset = itr#current_zset_lz pool;
                gcycled = !gcycled
              } in
            iter_advance itr dir;
            gcycled := false;
            Some g
          )
       )
  )

let base_some_stream base pool ztab col dir =
  let inv = Zcol.invert col base in
  let itr = Zcol.iter_values inv in
  (fun gcycled ->
     iter_start itr dir;
     let gcycled = ref gcycled in
     let rec advance (k:int) =
       if itr#beyond then
         None
       else (
         match snd(itr#current_value) with
           | None ->
               iter_advance itr dir;
               advance k
           | Some v ->
               let g =
                 { gvalue = lazy v;
                   gset = itr#current_zset_lz pool;
                   gcycled = !gcycled
                 } in
               iter_advance itr dir;
               gcycled := false;
               Some g
       ) in
     Stream.from advance
  )

let get_group pool itr subg left_hv =
  let left_set = itr#current_zset pool in
  let right_set = Lazy.force subg.gset in
  if Zset.isect_non_empty left_set right_set then (
    let gvalue =
      lazy
        ( let (_,left) = Lazy.force left_hv in
          let right = Lazy.force subg.gvalue in
          (left,right)
        ) in
    let gset =
      lazy
        ( Zset.isect pool left_set right_set ) in
    Some
      { gvalue;
        gset;
        gcycled = subg.gcycled
      }
  ) else
    None


let rec stream_rec : type t . _ -> _ -> _ -> t grouping -> bool ->
                      t group Stream.t =
  fun base pool ztab gspec ->
    match gspec with
      | Base(col,dir) ->
          base_stream base pool ztab col dir
      | Base_some(col,dir) ->
          base_some_stream base pool ztab col dir
      | Also(col,dir,subspec) ->
          let inv = Zcol.invert col base in
          let itr = Zcol.iter_values inv in
          (fun gcycled ->
             iter_start itr dir;
             let mk_sub = stream_rec base pool ztab subspec in
             let sub = ref(mk_sub gcycled) in
             let rec advance (k:int) =
               if itr#beyond then
                 None
               else (
                 match Stream.peek !sub with
                   | Some subg ->
                       ignore(Stream.next !sub);
                       let left_hv = itr#current_value_lz in
                       ( match get_group pool itr subg left_hv with
                           | Some _ as gopt -> gopt
                           | None -> advance k
                       )
                   | None ->
                       sub := mk_sub true;
                       iter_advance itr dir;
                       advance k
                  ) in
             Stream.from advance
          )
      | Also_some(col,dir,subspec) ->
          let inv = Zcol.invert col base in
          let itr = Zcol.iter_values inv in
          (fun gcycled ->
             iter_start itr dir;
             let mk_sub = stream_rec base pool ztab subspec in
             let sub = ref(mk_sub gcycled) in
             let rec advance (k:int) =
               if itr#beyond then
                 None
               else (
                 match Stream.peek !sub with
                   | Some subg ->
                       ignore(Stream.next !sub);
                       ( match itr#current_value with
                           | h, Some v ->
                               let left_hv = lazy (h,v) in
                               ( match get_group pool itr subg left_hv with
                                   | Some _ as gopt -> gopt
                                   | None -> advance k
                               )
                           | _, None -> advance k
                       )
                   | None ->
                       sub := mk_sub true;
                       iter_advance itr dir;
                       advance k
                  ) in
             Stream.from advance
          )

let stream ?filter ?base pool ztab gspec =
  let base =
    match base with
      | None -> Zset.all pool (Ztable.space ztab)
      | Some b -> b in
  let base =
    match filter with
      | None -> base
      | Some flt ->
          let flt_set = Zfilter.zset pool ztab flt in
          Zset.isect pool flt_set base in
  stream_rec base pool ztab gspec false

let list ?filter ?base ?limit pool ztab gspec =
  let s = stream ?filter ?base pool ztab gspec in
  let rec recurse k =
    let eof =
      match limit with
        | None -> false
        | Some n -> k >= n in
    if eof then
      []
    else
      match Stream.peek s with
        | None -> []
        | Some g ->
            Stream.junk s;
            g :: recurse (k+1) in
  recurse 0

let first ?filter ?base pool ztab gspec =
  let s = stream ?filter ?base pool ztab gspec in
  match Stream.peek s with
    | None -> raise Not_found
    | Some g -> g

let last ?filter ?base pool ztab gspec =
  first ?filter ?base pool ztab (reverse gspec)

let group_value g =
  Lazy.force g.gvalue

let group_zset g =
  Lazy.force g.gset

let innermost_cycle g =
  g.gcycled
