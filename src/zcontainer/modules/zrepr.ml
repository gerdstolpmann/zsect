(* Representations *)

open Ztypes

(* Generic options:
    - 0x00 + data => Some
    - 0xff => None
 *)

let non_leaf_option_mb : type t . t ztype -> t multibyte -> t option multibyte =
  fun zt mb ->
  (match zt with
     | Zstring -> assert false   (* need to use leaf_option_mb *)
     | Zint64 -> assert false   (* need to use leaf_option_mb *)
     | Zint -> assert false   (* need to use leaf_option_mb *)
     | Zfloat -> assert false   (* need to use leaf_option_mb *)
     | Zbool -> assert false   (* need to use leaf_option_mb *)
     | _ -> ()
  );
  { mb_name = mb.mb_name ^ " option";
    mb_size = (function
                | None -> 1
                | Some x -> 1 + mb.mb_size x
              );
    mb_encode = (fun b pos ->
                 function
                  | None -> 
                      Bytes.set b pos '\xff'
                  | Some x ->
                      Bytes.set b pos '\x00';
                      mb.mb_encode b (pos+1) x
                );
    mb_decode = (fun b pos len ->
                   let c = Bytes.get b pos in
                   if c = '\xff' then
                     None
                   else (
                     assert(c = '\x00');
                     Some(mb.mb_decode b (pos+1) (len-1))
                   )
                );
  }


(* This version assumes that the repr of the wrapped value never starts with
   0xff
 *)

let leaf_option_mb : type t . t ztype -> t multibyte -> t option multibyte =
  fun zt mb ->
  (match zt with
     | Zstring -> ()
     | Zint64 -> ()
     | Zint -> ()
     | Zfloat -> ()
     | Zbool -> ()
     | _ -> assert false     (* need to use option_mb *)
  );
  { mb_name = string_of_ztype (Zoption zt);
    mb_size = (function
                | None -> 1
                | Some x -> mb.mb_size x
              );
    mb_encode = (fun b pos ->
                 function
                  | None -> 
                      Bytes.set b pos '\xff'
                  | Some x ->
                      mb.mb_encode b pos x
                );
    mb_decode = (fun b pos len ->
                   if len > 0 && Bytes.get b pos = '\xff' then
                     None
                   else (
                     Some(mb.mb_decode b pos len)
                   )
                );
  }

let option_mb : type t . t ztype -> t multibyte -> t option multibyte =
  fun zt mb ->
  match zt with
     | Zstring -> leaf_option_mb zt mb
     | Zint64 -> leaf_option_mb zt mb
     | Zint -> leaf_option_mb zt mb
     | Zfloat -> leaf_option_mb zt mb
     | _ -> non_leaf_option_mb zt mb


(* Encoding: <length(x)> <x> <y>

   The length is encoded with LenPre3.
 *)

let pair_mb : type s t . s ztype -> s multibyte -> t ztype -> t multibyte ->
                   (s * t) multibyte =
 fun zt1 mb1 zt2 mb2 ->
  { mb_name = "(" ^ mb1.mb_name ^ " * " ^ mb2.mb_name ^ ")";
    mb_size = (fun (x,y) ->
                 let lx = mb1.mb_size x in
                 let elx = Zencoding.LenPre3.encode_length lx in
                 elx + lx + mb2.mb_size y
              );
    mb_encode = (fun b pos (x,y) ->
                   let lx = mb1.mb_size x in
                   let elx = Zencoding.LenPre3.encode_length lx in
                   Zencoding.LenPre3.encode_bytes b pos elx lx;
                   mb1.mb_encode b (pos+elx) x;
                   mb2.mb_encode b (pos+elx+lx) y;
                );
    mb_decode = (fun b pos len ->
                   let elx = Zencoding.LenPre3.decode_bytes_length b pos in
                   let lx = Zencoding.LenPre3.decode_bytes b pos in
                   let x = mb1.mb_decode b (pos+elx) lx in
                   let y = mb2.mb_decode b (pos+elx+lx) (len-elx-lx) in
                   (x,y)
                );
  }

(* Encoding: <length(xn+1)> <xn> ... <length(x2+1)> <x2> <length(x1+1)> <x1> <0>

   The lengths and the final 0 are encoded with LenPre3. Note that the
   elements come in reverse order!
 *)

let list_mb : type t . t ztype -> t multibyte -> t list multibyte =
  fun zt1 mb1 ->
  { mb_name = mb1.mb_name ^ " list";
    mb_size = (fun l ->
                 List.fold_left
                   (fun acc x ->
                      let lx = mb1.mb_size x in
                      let elx = Zencoding.LenPre3.encode_length (lx+1) in
                      acc + lx + elx
                   )
                   (Zencoding.LenPre3.encode_length 0)
                   l
              );
    mb_encode = (fun b pos l ->
                   let l = List.rev l in
                   let k = ref pos in
                   List.iter
                     (fun x ->
                        let lx = mb1.mb_size x in
                        let elx = Zencoding.LenPre3.encode_length (lx+1) in
                        Zencoding.LenPre3.encode_bytes b !k elx (lx+1);
                        mb1.mb_encode b (!k+elx) x;
                        k := !k + elx + lx
                     )
                     l;
                   let elx = Zencoding.LenPre3.encode_length 0 in
                   Zencoding.LenPre3.encode_bytes b !k elx 0;
                );
    mb_decode = (fun b pos len ->
                   let l = ref [] in
                   let k = ref pos in
                   let elx = ref(Zencoding.LenPre3.decode_bytes_length b !k) in
                   let lx = ref(Zencoding.LenPre3.decode_bytes b !k) in
                   while !lx > 0 do
                     let x = mb1.mb_decode b (!k + !elx) (!lx-1) in
                     l := x :: !l;
                     k := !k + !elx + !lx - 1;
                     elx := Zencoding.LenPre3.decode_bytes_length b !k;
                     lx := Zencoding.LenPre3.decode_bytes b !k
                   done;
                   !l
                );
  }
    

(* For strings, 0xff as first char is taken as an escape char:
    - 0xff 0x00: means 0xff as payload
    - 0xff 0xff: means "none" for string option
 *)

let string_mb =
  { mb_name = "string";
    mb_size = (fun s ->
                 let l = String.length s in
                 if l >= 1 && (String.get s 0) = '\xff' then l+1 else l
              );
    mb_encode = (fun b pos s ->
                   let l = String.length s in
                   if l >= 1 && s.[0] = '\xff' then (
                     Bytes.set b pos '\xff';
                     Bytes.set b (pos+1) '\x00';
                     Bytes.blit_string s 1 b (pos+2) (l-1);
                   ) else
                     Bytes.blit_string s 0 b pos l
                );
    mb_decode = (fun s pos len ->
                   if len > 0 && Bytes.get s pos = '\xff' then (
                     match Bytes.get s (pos+1) with
                       | '\x00' -> "\xff" ^ Bytes.sub_string s (pos+2) (len-2)
                       | _ -> failwith "Zrepr.string_mb: format error"
                   ) else
                     Bytes.sub_string s pos len
                );
  }

let string_asc =
  { ord_name = "string_asc";
    ord_comp = Ord_leaf;
    ord_hash = (fun s ->
                  let l = min (String.length s) 8 in
                  let u = Bytes.make 8 '\000' in
                  Bytes.blit_string s 0 u 0 l;
                  let n8 = Netnumber.BE.read_int8 u 0 in
                  Int64.to_int
                    (Int64.shift_right_logical (Netnumber.int64_of_int8 n8) 2)
               );
    ord_cmp = String.compare;
    ord_cmp_asc = String.compare;
    ord_dir = Some ASC;
    ord_sub = [| |];
  }

let string_desc =
  { ord_name = "string_desc";
    ord_comp = Ord_leaf;
    ord_hash = (fun s ->
                  let l = String.length s in
                  let u = Bytes.make 8 '\000' in
                  Bytes.blit_string s 0 u 0 l;
                  let n8 = Netnumber.BE.read_int8 u 0 in
                  Int64.to_int
                    (Int64.logxor
                       (Int64.shift_right_logical(Netnumber.int64_of_int8 n8) 2)
                       0x3fff_ffff_ffff_ffffL
                    )
               );
    ord_cmp = (fun x y -> String.compare y x);
    ord_cmp_asc = String.compare;
    ord_dir = Some DESC;
    ord_sub = [| |];
  }

(* None is larger than any Some _ *)

let option_cmp cmp_base x_opt y_opt =
  match x_opt, y_opt with
    | None, None -> 0
    | Some _, None -> (-1)
    | None, Some _ -> 1
    | Some x, Some y -> cmp_base x y

let option_ord : type t . t ztype -> t zorder -> t option zorder =
  fun zt zord ->
  { ord_name = zord.ord_name ^ " option";
    ord_comp = Ord_option;
    ord_hash = (function
                 | None -> 0x3fff_ffff_ffff_ffff
                 | Some s -> zord.ord_hash s
               );
    ord_cmp = option_cmp zord.ord_cmp;
    ord_cmp_asc = option_cmp zord.ord_cmp_asc;
    ord_dir = None;
    ord_sub = [| ZorderBox(zt,zord) |];
  }

let list_ord = Ztypes.list_ord
let pair_ord = Ztypes.pair_ord
let bundle_ord = Ztypes.bundle_ord

let int64_f64 =
  { f64_name = "int64";
    f64_size = 1;
    f64_encode = (fun data k x -> data.{k} <- x);
    f64_decode = (fun data k -> data.{k})
  }

let int_f64 =
  { f64_name = "int";
    f64_size = 1;
    f64_encode = (fun data k x -> data.{k} <- Int64.of_int x);
    f64_decode = (fun data k -> Int64.to_int data.{k})
  }

let float_f64 =
  { f64_name = "float";
    f64_size = 1;
    f64_encode = (fun data k x -> data.{k} <- Int64.bits_of_float x);
    f64_decode = (fun data k -> Int64.float_of_bits data.{k})
  }

(* Int64 multibyte:
    - first byte is 0x0M where M are the three most-significant bits
    - followed by LenPre3-encoding of the other 61 bits
    - for negative values, the bits 0-62 are inverted

   Reserved: first byte = 0xff for "null" (and int64 option)
 *)

let int64_mb =
  { mb_name = "int64";
    mb_size = (fun x ->
                 let x1 =
                   if x >= 0L then x else 
                     Int64.logxor x 0x7fff_ffff_ffff_ffffL in
                 let x2 = 
                   Int64.to_int (Int64.logand x1 0x1fff_ffff_ffff_ffffL) in
                 Zencoding.LenPre3.encode_length x2 + 1
              );
    mb_encode = (fun b pos x ->
                   let x1 =
                     if x >= 0L then x else 
                       Int64.logxor x 0x7fff_ffff_ffff_ffffL in
                   let x2 = 
                     Int64.to_int (Int64.logand x1 0x1fff_ffff_ffff_ffffL) in
                   let len = Zencoding.LenPre3.encode_length x2 + 1 in
                   Bytes.set b pos
                     (Char.chr(Int64.to_int (Int64.shift_right_logical x1 61)));
                   Zencoding.LenPre3.encode_bytes b (pos+1) (len-1) x2;
                );
    mb_decode = (fun b pos len ->
                   let b0 = Char.code (Bytes.get b pos) in
                   if b0 > 7 then
                     invalid_arg "mb_decode";
                   let x0 = Zencoding.LenPre3.decode_bytes b (pos+1) in
                   let x1 =
                     Int64.logor
                       (Int64.of_int x0)
                       (Int64.shift_left (Int64.of_int b0) 61) in
                   if x1 >= 0L then
                     x1
                   else
                      Int64.logxor x1 0x7fff_ffff_ffff_ffffL
                )
  }

let int_mb =
  { mb_name = "int";
    mb_size = (fun x -> int64_mb.mb_size (Int64.of_int x));
    mb_encode = (fun b pos x ->
                   int64_mb.mb_encode b pos (Int64.of_int x)
                );
    mb_decode = (fun b pos len ->
                   Int64.to_int (int64_mb.mb_decode b pos len)
                );
  }


(* TODO: this should be done differently. In particular, floats where
   the right part of the mantissa is zero should be compressed, i.e.
   length = number of non-null bytes starting with the MSB, and all
   bytes beyond length are assumed to be 0.
 *)

let float_mb =
  { mb_name = "float";
    mb_size = (fun x -> int64_mb.mb_size (Int64.bits_of_float x));
    mb_encode = (fun b p x -> int64_mb.mb_encode b p (Int64.bits_of_float x));
    mb_decode = (fun b p l -> Int64.float_of_bits (int64_mb.mb_decode b p l));
  }


let bool_mb =
  { mb_name = "bool";
    mb_size = (fun _ -> 1);
    mb_encode = (fun b p x ->
      Bytes.set b p (if x then Char.chr 1 else Char.chr 0)
    );
    mb_decode = (fun b pos len ->
      if len <> 1 then invalid_arg "mb_decode";
      Char.code (Bytes.get b pos) > 0
    );
  }

let parsed parser zo zo_fallback =
  { ord_name = "parsed_" ^ zo.ord_name;
    ord_comp = Ord_leaf;
    ord_hash = (fun s ->
                  try zo.ord_hash (parser s)
                  with Failure _ -> zo_fallback.ord_hash s
               );
    ord_cmp = (fun x y ->
                 try
                   let xp = parser x in
                   try
                     let yp = parser y in
                     zo.ord_cmp xp yp
                   with Failure _ -> (-1)
                 with Failure _ ->
                   try ignore(parser y); 1
                   with Failure _ -> zo_fallback.ord_cmp x y
              );
    ord_cmp_asc = (fun x y ->
                   try
                     let xp = parser x in
                     try
                       let yp = parser y in
                       zo.ord_cmp_asc xp yp
                     with Failure _ -> (-1)
                   with Failure _ ->
                     try ignore(parser y); 1
                     with Failure _ -> zo_fallback.ord_cmp_asc x y
              );
    ord_dir = zo.ord_dir;
    ord_sub = zo.ord_sub;
  }

let int64_asc =
  { ord_name = "int64_asc";
    ord_comp = Ord_leaf;
    ord_hash = (fun x ->
                  Int64.to_int
                    (Int64.logxor
                       (Int64.shift_right_logical x 2)
                       0x2000_0000_0000_0000L
                    )
               );
    ord_cmp = Int64.compare;
    ord_cmp_asc = Int64.compare;
    ord_dir = Some ASC;
    ord_sub = [| |];
  }

let int64_desc =
  { ord_name = "int64_desc";
    ord_comp = Ord_leaf;
    ord_hash = (fun x ->
                  Int64.to_int
                    (Int64.logxor
                       (Int64.shift_right_logical x 2)
                       0x1fff_ffff_ffff_ffffL
                    )
               );
    ord_cmp = (fun x y -> Int64.compare y x);
    ord_cmp_asc = Int64.compare;
    ord_dir = Some DESC;
    ord_sub = [| |];
  }

let parsed_int64_asc = parsed Int64.of_string int64_asc string_asc
let parsed_int64_asc = parsed Int64.of_string int64_desc string_desc

let int_asc = Ztypes.int_asc

let int_desc =
  { ord_name = "int_desc";
    ord_comp = Ord_leaf;
    ord_hash = (fun x ->
                  ((x asr 1) lsr 1) lxor 0x1fff_ffff_ffff_ffff
                  (* this needs to be compatible with int64 *)
               );
    ord_cmp = (compare : int -> int -> int);
    ord_cmp_asc = (compare : int -> int -> int);
    ord_dir = Some DESC;
    ord_sub = [| |];
  }

let parsed_int_asc = parsed int_of_string int_asc string_asc
let parsed_int_asc = parsed int_of_string int_desc string_desc


let float_asc =
  { ord_name = "float_asc";
    ord_comp = Ord_leaf;
    ord_hash = (fun x ->
                  match classify_float x with
                    | FP_nan ->
                        0
                    | _ ->
                        let bits = Int64.bits_of_float x in
                        Int64.to_int
                          (Int64.logxor
                             (Int64.shift_right_logical bits 2)
                             0x2000_0000_0000_0000L
                          )
               );
    ord_cmp = Pervasives.compare;
    ord_cmp_asc = Pervasives.compare;
    ord_dir = Some ASC;
    ord_sub = [| |];
  }

let float_desc =
  { ord_name = "float_desc";
    ord_comp = Ord_leaf;
    ord_hash = (fun x ->
                  match classify_float x with
                    | FP_nan ->
                        0x3fff_ffff_ffff_ffff
                    | _ ->
                        let bits = Int64.bits_of_float x in
                        Int64.to_int
                          (Int64.logxor
                             (Int64.shift_right_logical bits 2)
                             0x1fff_ffff_ffff_ffffL
                          )
               );
    ord_cmp = (fun x y -> Pervasives.compare y x);
    ord_cmp_asc = Pervasives.compare;
    ord_dir = Some DESC;
    ord_sub = [| |];
  }


let parsed_float_asc = parsed float_of_string float_asc string_asc
let parsed_float_asc = parsed float_of_string float_desc string_desc

let bool_cmp =
  function
  | (false,false) -> 0
  | (false,true) -> (-1)
  | (true,false) -> 1
  | (true,true) -> 0


let bool_asc =
  { ord_name = "bool_asc";
    ord_comp = Ord_leaf;
    ord_hash = (fun x -> if x then 1 else 0);
    ord_cmp = (fun x y -> bool_cmp (x,y));
    ord_cmp_asc = (fun x y -> bool_cmp (x,y));
    ord_dir = Some ASC;
    ord_sub = [| |];
  }

let bool_desc =
  { ord_name = "bool_desc";
    ord_comp = Ord_leaf;
    ord_hash = (fun x -> if x then 0 else 1);
    ord_cmp = (fun x y -> bool_cmp (y,x));
    ord_cmp_asc = (fun x y -> bool_cmp (x,y));
    ord_dir = Some DESC;
    ord_sub = [| |];
  }

let strmap l =
  List.fold_left
    (fun acc (name,x) ->
       StrMap.add name x acc
    )
    StrMap.empty
    l

let zstring_opt = Zoption Zstring
let zint64_opt = Zoption Zint64
let zint_opt = Zoption Zint
let zfloat_opt = Zoption Zfloat
let zbool_opt = Zoption Zbool

let string_opt_mb = leaf_option_mb Zstring string_mb
let int64_opt_mb = leaf_option_mb Zint64 int64_mb
let int_opt_mb = leaf_option_mb Zint int_mb
let float_opt_mb = leaf_option_mb Zfloat float_mb
let bool_opt_mb = leaf_option_mb Zbool bool_mb

let string_ord =
  function
  | None
  | Some ASC ->
      string_asc
  | Some DESC ->
      string_desc

let bundle_mb zt mb =
  let zt1 = Zpair(Zint,zt) in
  let zt2 =
    list_mb
      zt1
      (pair_mb
         Zint int_mb
         zt mb) in
  { zt2 with
    mb_name = mb.mb_name ^ " bundle"
  }
                        
let stdrepr =
  let f64dict =
    [ int64_f64.f64_name, Fixed64Box(Zint64,int64_f64);
      int_f64.f64_name, Fixed64Box(Zint,int_f64);
      float_f64.f64_name, Fixed64Box(Zfloat,float_f64);
    ] in
  let mbdict =
    [ string_mb.mb_name, MultibyteBox(Zstring,string_mb);
      string_opt_mb.mb_name, MultibyteBox(zstring_opt,string_opt_mb);
      int64_mb.mb_name, MultibyteBox(Zint64,int64_mb);
      int64_opt_mb.mb_name, MultibyteBox(zint64_opt,int64_opt_mb);
      int_mb.mb_name, MultibyteBox(Zint,int_mb);
      int_opt_mb.mb_name, MultibyteBox(zint_opt,int_opt_mb);
      float_mb.mb_name, MultibyteBox(Zfloat,float_mb);
      float_opt_mb.mb_name, MultibyteBox(zfloat_opt,float_opt_mb);
      bool_mb.mb_name, MultibyteBox(Zbool,bool_mb);
      bool_opt_mb.mb_name, MultibyteBox(zbool_opt,bool_opt_mb);
    ] in
  let orddict =
    [ string_asc.ord_name, ZorderBox(Zstring,string_asc);
      string_desc.ord_name, ZorderBox(Zstring,string_desc);
      int64_asc.ord_name, ZorderBox(Zint64,int64_asc);
      int64_desc.ord_name, ZorderBox(Zint64,int64_desc);
      int_asc.ord_name, ZorderBox(Zint,int_asc);
      int_desc.ord_name, ZorderBox(Zint,int_desc);
      float_asc.ord_name, ZorderBox(Zfloat,float_asc);
      float_desc.ord_name, ZorderBox(Zfloat,float_desc);
      bool_asc.ord_name, ZorderBox(Zbool,bool_asc);
      bool_desc.ord_name, ZorderBox(Zbool,bool_desc);
    ] in
  let zorderConv : type s t . s ztype -> t ztype -> t zorder -> s zorder option =
    fun ztout zt zo ->
    match ztout, zt with
      | Zstring, Zint64 ->
          Some(parsed Int64.of_string zo (string_ord zo.ord_dir))
      | Zstring, Zfloat ->
          Some(parsed float_of_string zo (string_ord zo.ord_dir))
      | _ ->
          None in
  { f64dict = strmap f64dict;
    mbdict = strmap mbdict;
    mboption = Some {multibyteOption = option_mb};
    mbpair = Some {multibytePair = pair_mb};
    mblist = Some {multibyteList = list_mb};
    mbbundle = Some {multibyteBundle = bundle_mb};
    orddict = strmap orddict;
    ordoption = Some {zorderOption = option_ord};
    ordpair = Some {zorderPair = pair_ord};
    ordconv = {zorderConv}
  }

let is_std_float_f64 repr =
  match repr with
    | Dir_fixed64 f64 -> f64 == float_f64
    | _ -> false

let is_std_float_asc ord =
  ord == float_asc
