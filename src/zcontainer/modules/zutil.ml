open Ztypes

let debug = ref false

module OrdUtil(S:Map.OrderedType) = struct
  let safe_pred =
    function
    | 0 -> raise Not_found
    | n -> pred n

  (* comp=EQ: searches the index of x or raises Not_found
     comp=NE: if x is member raises Not_found, otherwise return an element
              that is not x (if n=0, also raise Not_found)
     comp=LT: searches the biggest index of a value < x (or Not_found)
     comp=LE: searches the biggest index of a value <= x (or Not_found)
     comp=GT: searches the smallest index of a value > x (or Not_found)
     comp=GE: searches the smallest index of a value >= x (or Not_found)
   *)

  let binary_search get n comp x =
    if n=0 then raise Not_found;
    let k1 = ref 0 in
    let x1 = ref (get 0) in
    let c1 = ref (S.compare x !x1) in
    if !c1 < 0 then
      (* searched element is before the first element *)
      match comp with
        | EQ | LT | LE -> raise Not_found
        | NE | GT | GE -> 0
    else
      let k2 = ref (n-1) in
      let x2 = ref (get !k2) in
      let c2 = S.compare x !x2 in
      if c2 >= 0 then
        (* searched element is at or after the last element *)
        match comp with
          | EQ -> if c2 <> 0 then raise Not_found; n-1
          | NE -> if c2 = 0 then (if !c1 = 0 then raise Not_found else 0)
                  else n-1
          | LT -> if c2 = 0 then safe_pred (n-1) else n-1
          | LE -> n-1
          | GT -> raise Not_found
          | GE -> if c2 <> 0 then raise Not_found; n-1
      else (
        while !k2 - !k1 >= 2 do
          let m = !k1 + ((!k2 - !k1) lsr 1) in
          let xm = get m in
          let c = S.compare x xm in
          if c >= 0 then (
            k1 := m;
            c1 := c;
            x1 := xm;
            if c = 0 then k2 := m+1;
          ) else (
            k2 := m;
            x2 := xm
          )
        done;
        (* x1 <= x < x2 *)
        match comp with
          | EQ -> if !c1 <> 0 then raise Not_found; !k1
          | NE -> if !c1 = 0 then (if c2 = 0 then raise Not_found else !k2)
                  else !k1
          | LT -> if !c1 = 0 then safe_pred !k1 else !k1
          | LE -> !k1
          | GT -> !k2
          | GE -> if !c1 = 0 then !k1 else !k2
      )

  (* this is always a min heap. Use different S to get a max heap *)

  type heap =
    { mutable hdata : S.t array;
      mutable hlength : int;
      hfiller : S.t;
    }

  let heap_create n filler =
    { hdata = Array.make n filler;
      hlength = 0;
      hfiller = filler
    }

  let heap_length heap =
    heap.hlength

  let heap_min heap =
    if heap.hlength = 0 then raise Not_found;
    heap.hdata.(0)

  let heap_snd_min heap =
    (* the second-smallest element *)
    if heap.hlength < 2 then raise Not_found;
    let h1 = heap.hdata.(1) in
    if heap.hlength = 2 then
      h1
    else
      let h2 = heap.hdata.(2) in
      if S.compare h1 h2 < 0 then
        h1
      else
        h2

  let heap_resize heap =
    let hdata = Array.make (2 * Array.length heap.hdata) heap.hfiller in
    Array.blit heap.hdata 0 hdata 0 heap.hlength;
    heap.hdata <- hdata

  let rec heap_bubble_up heap k =
    if k > 0 then (
      let hdata = heap.hdata in
      let p = (k-1)/2 in
      let xk = hdata.(k) in
      let xp = hdata.(p) in
      if S.compare xk xp < 0 then (
        hdata.(k) <- xp;
        hdata.(p) <- xk;
        heap_bubble_up heap p
      )
    )

  let heap_insert heap x =
    if heap.hlength >= Array.length heap.hdata then
      heap_resize heap;
    let k = heap.hlength in
    heap.hdata.(k) <- x;
    heap.hlength <- k + 1;
    heap_bubble_up heap k

  let rec heap_bubble_down heap k =
    let l = 2*k+1 in
    if l < heap.hlength then (
      let hdata = heap.hdata in
      let xk = hdata.(k) in
      let xl = hdata.(l) in
      if S.compare xl xk < 0 then (
        heap_bubble_down_1 heap hdata k xk l xl
      ) else
        heap_bubble_down_1 heap hdata k xk k xk
    )

  and heap_bubble_down_1 heap hdata k xk j xj =
    let r = 2*k+2 in
    if r < heap.hlength then (
      let xk = hdata.(k) in
      let xr = hdata.(r) in
      if S.compare xr xj < 0 then
        heap_bubble_down_2 heap hdata k xk r xr
      else
        heap_bubble_down_2 heap hdata k xk j xj
    ) else
      heap_bubble_down_2 heap hdata k xk j xj

  and heap_bubble_down_2 heap hdata k xk j xj =
    if j <> k then (
      hdata.(k) <- xj;
      hdata.(j) <- xk;
      heap_bubble_down heap j
    )

  let heap_delete_min heap =
    if heap.hlength < 1 then
      invalid_arg "Zcontainer.Zutil.OrdUtil.heap_delete_min";
    let x = heap.hdata.(heap.hlength - 1) in
    heap.hdata.(0) <- x;
    heap.hlength <- heap.hlength - 1;
    heap_bubble_down heap 0

  let heap_sort heap =  (* distructive *)
    let n = heap.hlength in
    let out = Array.make n heap.hfiller in
    let p = ref 0 in
    while heap.hlength > 0 do
      let x = heap_min heap in
      heap_delete_min heap;
      out.( !p ) <- x;
      incr p
    done;
    assert( !p = n );
    out

end


module ArrayUtil = struct
  type 'a ext_array =
    { mutable adata : 'a array;
      mutable alength : int;
      mutable afiller : 'a
    }

  let ext_create n x =
    { adata = Array.make n x;
      alength = 0;
      afiller = x;
    }

  let ext_resize a =
    let adata = Array.make (2 * Array.length a.adata) a.afiller in
    Array.blit a.adata 0 adata 0 a.alength;
    a.adata <- adata
    
  let ext_add a x =
    let len = a.alength in
    if len >= Array.length a.adata then
      ext_resize a;
    a.adata.(len) <- x;
    a.alength <- len+1

  let ext_data a = a.adata
  let ext_length a = a.alength
  let ext_contents a = Array.sub a.adata 0 a.alength

  (* TODO: ext_sort *)

end


module Persist = struct
  let elem_size : type s t . (s,t) Bigarray.kind -> int =
    function
    | Bigarray.Float32 -> 4
    | Bigarray.Float64 -> 8
    | Bigarray.Int8_signed -> 1
    | Bigarray.Int8_unsigned -> 1
    | Bigarray.Int16_signed -> 2
    | Bigarray.Int16_unsigned -> 2
    | Bigarray.Int32 -> 4
    | Bigarray.Int64 -> 8
    | Bigarray.Int -> 8   (* we are always on 64 bit platforms *)
    | Bigarray.Nativeint -> 8
    | Bigarray.Complex32 -> 8
    | Bigarray.Complex64 -> 16
    | Bigarray.Char -> 1

  let create header ba_kind ba_layout ba_size =
    let k = elem_size ba_kind in
    let fd, name =
      Netsys_posix.shm_create "/zsect" (ba_size * k + 256) in
    let header_ba =
      Bigarray.Array1.map_file
        fd Bigarray.Char Bigarray.c_layout true 256 in
    let data =
      Bigarray.Array1.map_file
        fd ~pos:256L ba_kind ba_layout true (ba_size * k) in
    Unix.close fd;
    Netsys_mem.blit_bytes_to_memory
      header 0 header_ba 0 (Bytes.length header);
    (data, name)

  let delete name =
    Netsys_posix.shm_unlink name

  let access_header name =
    let fd =
      Netsys_posix.shm_open
        name [Netsys_posix.SHM_O_RDONLY] 0 in
    let header_ba =
      Bigarray.Array1.map_file
        fd Bigarray.Char Bigarray.c_layout false 256 in
    Unix.close fd;
    Netsys_mem.bytes_of_memory header_ba


  let access name ba_kind ba_layout ba_size =
    let k = elem_size ba_kind in
    let fd =
      Netsys_posix.shm_open
        name [Netsys_posix.SHM_O_RDONLY] 0 in
    let data =
      Bigarray.Array1.map_file
        fd ~pos:256L ba_kind ba_layout false (ba_size * k) in
    Unix.close fd;
    data
end


module Bitset = struct
  (* WARNING: this is unsafe! *)

    open Bigarray

  type ba =
    (int, int16_unsigned_elt, c_layout) Array1.t

  type bitset =
    { size : int;
      datasize : int;
      data : ba;
      mutable runlengths : ba;
      mutable have_runlengths : bool;
    }

  let create n x0 =
    let i0 = if x0 then 65535 else 0 in
    let datasize = if n=0 then 0 else (n-1) / 16 + 1 in
    let data = Array1.create int16_unsigned c_layout datasize in
    Array1.fill data i0;
    { size = n;
      datasize;
      data;
      runlengths = Array1.create int16_unsigned c_layout 0;
      have_runlengths = false
    }

  let length bs = bs.size

  let get bs k =
    let x = Array1.(*unsafe_*)get bs.data (k lsr 4) in
    (x lsr (k land 15)) land 1 <> 0

  let get16 bs k =
    (* get 16 bits as integer *)
    let i = k lsr 4 in
    let j = k land 15 in
    if j = 0 then
      Array1.(*unsafe_*)get bs.data i
    else
      let x0 = (Array1.(*unsafe_*)get bs.data i) lsr j in
      if i = bs.datasize - 1 then
        x0
      else
        let x1 = Array1.(*unsafe_*)get bs.data (i+1) in
        (x0 lor (x1 lsl (16-j))) land 65535

  let set_true bs k =
    let i = k lsr 4 in
    let x = Array1.(*unsafe_*)get bs.data i in
    let x' = x lor (1 lsl (k land 15)) in
    Array1.unsafe_set bs.data i x'

  let set_false bs k =
    let i = k lsr 4 in
    let x = Array1.(*unsafe_*)get bs.data i in
    let x' = x land ((-1) lxor (1 lsl (k land 15))) in
    Array1.unsafe_set bs.data i x'

  let set bs k value =
    if value then set_true bs k else set_false bs k

  let fill_true bs k len =
    let fill_at i j_first j_last =
      let x = ref(Array1.(*unsafe_*)get bs.data i) in
      for j = j_first to j_last do
        x := !x lor (1 lsl j)
      done;
      Array1.unsafe_set bs.data i !x in

    let i_first = k lsr 4 in
    let i_last = (k+len-1) lsr 4 in
    let j_first = k land 15 in
    let j_last = (k+len-1) land 15 in
    if i_first = i_last then
      fill_at i_first j_first j_last
    else (
      fill_at i_first j_first 15;
      for i = i_first+1 to i_last-1 do
        Array1.unsafe_set bs.data i 65535
      done;
      fill_at i_last 0 j_last
    )

  let set_runlengths bs =
    if Array1.dim bs.runlengths <> bs.datasize then
      bs.runlengths <-  Array1.create int16_unsigned c_layout bs.datasize;
    Array1.fill bs.runlengths 1;
    bs.have_runlengths <- true;
    let last_x = ref (-1) in
    let last_rl = ref 1 in
    for i = bs.datasize - 2 downto 0 do
      let x = Array1.unsafe_get bs.data i in
      if (x = 0 || x = 65535) && x = !last_x then (
        let rl = if !last_rl = 65535 then 1 else !last_rl+1 in
        Array1.unsafe_set bs.runlengths i rl;
        last_rl := rl
      ) else
        last_rl := 1;
      last_x := x
    done

  let get_rl bs k =
    (* returns a positive number n when bit k is set. Returns a non-positive
       number -n+1 when bit k is not set. The number n is the number of same
       bits (including k) that exist at least at k and the following positions
       (but there could be more).
     *)
    let i = k lsr 4 in
    let x = Array1.(*unsafe_*)get bs.data i in
    if bs.have_runlengths && i < bs.datasize-1 then
      if x = 65535 then
        ((Array1.get bs.runlengths i) lsl 4) - (k land 15)
      else if x = 0 then
        let n = ((Array1.get bs.runlengths i) lsl 4) - (k land 15) in
        -n+1
      else
        (x lsr (k land 15)) land 1
    else
      (x lsr (k land 15)) land 1


  let rec runlength_false bs p acc =
    if p < length bs then (
      let i = p lsr 4 in
      let j = p land 15 in
      let x = Array1.get bs.data i in
      if x = 0 && bs.have_runlengths then
        let n = (Array1.get bs.runlengths i) lsl 4 - j in
        runlength_false bs (p+n) (acc+n)
      else
        let m = min (16-j) (length bs - p) in
        runlength_false_aux bs p acc m (x lsr j)
    )
    else
      acc

  and runlength_false_aux bs p acc m x =
    if m > 0 then (
      if x land 1 <> 0 then
        acc
      else
        runlength_false_aux bs (p+1) (acc+1) (m-1) (x lsr 1)
    )
    else
      runlength_false bs p acc


  let rec runlength_true bs p acc =
    if p < length bs then (
      let i = p lsr 4 in
      let j = p land 15 in
      let x = Array1.get bs.data i in
      if x = 65535 && bs.have_runlengths then
        let n = (Array1.get bs.runlengths i) lsl 4 - j in
        runlength_true bs (p+n) (acc+n)
      else
        let m = min (16-j) (length bs - p) in
        runlength_true_aux bs p acc m (x lsr j)
    )
    else
      acc

  and runlength_true_aux bs p acc m x =
    if m > 0 then (
      if x land 1 = 0 then
        acc
      else
        runlength_true_aux bs (p+1) (acc+1) (m-1) (x lsr 1)
    )
    else
      runlength_true bs p acc


  let rec rev_runlength_false bs p acc =
    (* in backward direction *)
    if p >= 15 then (
      let x = get16 bs (p-15) in
      if x = 0 then
        rev_runlength_false bs (p-16) (acc+16)
      else
        rev_runlength_false_aux bs p acc 16 x
    )
    else
      if p >= 0 then (
        if get bs p then
          acc
        else
          rev_runlength_false bs (p-1) (acc+1)
      )
      else
        acc

  and rev_runlength_false_aux bs p acc m x =
    if m > 0 then (
      if x land 32768 <> 0 then
        acc
      else
        rev_runlength_false_aux bs (p-1) (acc+1) (m-1) (x lsl 1)
    )
    else
      rev_runlength_false bs p acc


  let iter f bs =
    assert(bs.have_runlengths);
    let k = ref 0 in
    let x = ref (Array1.get bs.data 0) in
    let rl = ref (Array1.get bs.runlengths 0) in
    while !k <= bs.size-1 do
      if !rl = 1 then (
        if  (!x lsr (!k land 15)) land 1 <> 0 then
          f !k 1;
        incr k;
      ) else (
        if !x = 65535 then f !k (!rl * 16);
        k := !k + !rl * 16;
      );
      if !k land 15 = 0 then (
        let i = !k lsr 4 in
        if i < bs.datasize then (
          x := Array1.unsafe_get bs.data i;
          rl := Array1.unsafe_get bs.runlengths i
        )
      )
    done

  let union bsout bsin p len =
    assert(bsin.have_runlengths);
    bsout.have_runlengths <- false;  (* should be recalculated by caller *)
    let i1 = p lsr 4 in
    let j1 = p land 15 in
    let i2 = (p+len-1) lsr 4 in
    let j2 = (p+len-1) land 15 in
    if i1=i2 then (
      let xin = Array1.get bsin.data i1 in
      for j=j1 to j2 do
        if ((xin lsr j) land 1) <> 0 then
          set_true bsout (i1 lsl 4 + j)
      done
    ) else (
      let xin1 = Array1.get bsin.data i1 in
      for j=j1 to 15 do
        if ((xin1 lsr j) land 1) <> 0 then
          set_true bsout (i1 lsl 4 + j)
      done;
      let xin2 = Array1.get bsin.data i2 in
      for j=0 to j2 do
        if ((xin2 lsr j) land 1) <> 0 then
          set_true bsout (i2 lsl 4 + j)
      done;
      let i = ref (i1+1) in
      while !i <= i2-1 do
        let xin = Array1.unsafe_get bsin.data !i in
        if xin <> 0 then (
          Array1.unsafe_set
            bsout.data
            !i
            (Array1.unsafe_get bsout.data !i lor xin);
          incr i
        ) else
          i := !i + Array1.unsafe_get bsin.runlengths !i
      done;
    )

  let rec count_bits x acc =
    if x = 0 then
      acc
    else
      if x land 1 <> 0 then
        count_bits (x lsr 1) (acc+1)
      else
        count_bits (x lsr 1) acc


  let count bs =
    let n = ref 0 in
    for i = 0 to bs.datasize - 2 do
      n := count_bits (Array1.unsafe_get bs.data i) !n
    done;
    if bs.datasize > 0 then (
      let x = Array1.unsafe_get bs.data (bs.datasize-1) in
      let j = bs.size land 15 in
      let j = if j=0 then 16 else j in
      n := count_bits (((x lsl (16-j)) land 65535) lsr (16-j)) !n
    );
    !n

  let persist ?(fill_header = fun _ -> ()) bs =
    if not bs.have_runlengths then set_runlengths bs;
    let hd = Bytes.create 256 in
    Bytes.blit_string "ZSETBS00" 0 hd 0 8;   (* magic number *)
    Netnumber.HO.write_int8 hd 8 (Netnumber.int8_of_int bs.datasize);
    Netnumber.HO.write_int8 hd 16 (Netnumber.int8_of_int bs.size);
    fill_header hd;
    let (alldata, name) =
      Persist.create
        hd Bigarray.int16_unsigned Bigarray.c_layout (2*bs.datasize) in
    let data =
      Bigarray.Array1.sub alldata 0 bs.datasize in
    let runlengths =
      Bigarray.Array1.sub alldata bs.datasize bs.datasize in
    Bigarray.Array1.blit bs.data data;
    Bigarray.Array1.blit bs.runlengths runlengths;
    name

  let unpersist name =
    Persist.delete name

  let access name hd =
    let magic = Bytes.sub_string hd 0 8 in
    if magic <> "ZSETBS00" then failwith "Zcontainer.Zutil.access: bad magic";
    let datasize =
      Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 8) in
    let size =
      Netnumber.int_of_int8 (Netnumber.HO.read_int8 hd 16) in
    let alldata =
      Persist.access
        name Bigarray.int16_unsigned Bigarray.c_layout (2*datasize) in
    let data =
      Bigarray.Array1.sub alldata 0 datasize in
    let runlengths =
      Bigarray.Array1.sub alldata datasize datasize in
    { size; datasize; data; runlengths; have_runlengths = true }
end


module FileUtil = struct
  let encode_name s =
    let b = Buffer.create 80 in
    String.iter
      (function
        | '0'..'9'
        | 'A'..'Z'
        | 'a'..'z'
        | '_' | '-' | '+' as c ->
            Buffer.add_char b c
        | c ->
            Printf.bprintf b "=%02x" (Char.code c)
      )
      s;
    Buffer.contents b

  let rm ?(force=false) file =
    if force then
      (try Sys.remove file with Sys_error _ -> ())
    else
      Sys.remove file

  let rmdir ?(force=false) dir =
    if force then
      (try Unix.rmdir dir with Unix.Unix_error _ -> ())
    else
      Unix.rmdir dir
end
