open Ztypes

type kleene = N | F | T
  (* or better "kleen"? - the other guy's "e" was also stolen *)

type filter =
  | Colcmp : 'a Zcol.zcol * comparison * 'a -> filter
  | Colcmp_some : 'a option Zcol.zcol * comparison * 'a -> filter
  | Defined : 'a Zcol.zcol -> filter
  | Defined_some : 'a option Zcol.zcol -> filter
  | Test1 : 'a Zcol.zcol * (int -> 'a -> kleene) -> filter
  | TestID : (int -> kleene) -> filter
  | And : filter list -> filter
  | Or : filter list -> filter
  | Not : filter -> filter


type iterate =
  | Iterate : 'a Zcol.zcol *
               (comparison * 'a) option *
               (comparison * 'a) option -> iterate

type conjunction =
  { iterate : iterate StrMap.t;
    anyset : (Zset.zset -> Zset.zset) list;
    anytest : (int -> bool) list;
  }
  (* The filterset represented by [conjuction]: the AND of all
     iterations AND any other evaluation, specified as function
     taking an input set and returning a smaller set
   *)

let empty_conj =
  { iterate = StrMap.empty;
    anyset = [];
    anytest = []
  }

let is_icol col =
  match Zcol.content col with
    | Zcol.Inv _ -> true
    | _ -> false

let is_dcol col =
  match Zcol.content col with
    | Zcol.Dir _ -> true
    | _ -> false

let refers_to_icol =
  function
  | Colcmp(col,_,_) -> is_icol col
  | Colcmp_some(col,_,_) -> is_icol col
  | Defined col -> is_icol col
  | Defined_some col -> is_icol col
  | _ -> false

let refers_to_dcol =
  function
  | Colcmp(col,_,_) -> is_dcol col
  | Colcmp_some(col,_,_) -> is_dcol col
  | Defined col -> is_dcol col
  | Defined_some col -> is_dcol col
  | _ -> false

let and_lbound : type t.
                   t Zcol.zcol -> (comparison * t) -> (comparison * t) -> 
                   (comparison * t) =
  fun col (cmp1,lbound1) (cmp2,lbound2) ->
    let ZcolDescr(_,_,_,ord) = Zcol.descr col in
    match cmp1, cmp2 with
      | GE, GE
      | GE, EQ
      | GT, GT
      | GT, GE
      | GT, EQ ->
          let d = ord.ord_cmp_asc lbound1 lbound2 in
          (cmp1, if d >= 0 then lbound1 else lbound2)
      | GE, GT ->
          let d = ord.ord_cmp_asc lbound1 lbound2 in
          (cmp2, if d >= 0 then lbound1 else lbound2)
      | _ ->
          (cmp1, lbound1)


let and_lbound_opt : type t.
                   t Zcol.zcol -> (comparison * t) option -> (comparison * t) -> 
                   (comparison * t) option =
  fun col lb1opt (cmp2,lbound2) ->
    match lb1opt, cmp2 with
      | None, (EQ|GE|GT) ->
          Some (cmp2, lbound2)
      | None, _ ->
          None
      | Some(cmp1,lbound1), _ ->
          Some (and_lbound col (cmp1,lbound1) (cmp2,lbound2))


let and_ubound : type t.
                   t Zcol.zcol -> (comparison * t) -> (comparison * t) -> 
                   (comparison * t) =
  fun col (cmp1,ubound1) (cmp2,ubound2) ->
    let ZcolDescr(_,_,_,ord) = Zcol.descr col in
    match cmp1, cmp2 with
      | LE, LE
      | LE, EQ
      | LT, LT
      | LT, LE
      | LT, EQ ->
          let d = ord.ord_cmp_asc ubound1 ubound2 in
          (cmp1, if d <= 0 then ubound1 else ubound2)
      | LE, LT ->
          let d = ord.ord_cmp_asc ubound1 ubound2 in
          (cmp2, if d <= 0 then ubound1 else ubound2)
      | _ ->
          (cmp1, ubound1)


let and_ubound_opt : type t.
                   t Zcol.zcol -> (comparison * t) option -> (comparison * t) -> 
                   (comparison * t) option =
  fun col ub1opt (cmp2,ubound2) ->
    match ub1opt, cmp2 with
      | None, (EQ|LE|LT) ->
          Some (cmp2, ubound2)
      | None, _ ->
          None
      | Some(cmp1,ubound1), _ ->
          Some (and_ubound col (cmp1,ubound1) (cmp2,ubound2))

let pol_comparison pol cmp =
  if pol then
    match cmp with
      | EQ -> NE
      | NE -> EQ
      | LT -> GE
      | LE -> GT
      | GT -> LE
      | GE -> LT
  else
    cmp

let eval_cmp : type t . t Zcol.zcol -> _ -> t -> t -> bool =
  fun col cmp ->
    let ZcolDescr(_,_,_,ord) = Zcol.descr col in
    match cmp with
      | EQ -> (fun x1 x2 -> ord.ord_cmp_asc x1 x2 = 0)
      | NE -> (fun x1 x2 -> ord.ord_cmp_asc x1 x2 <> 0)
      | LT -> (fun x1 x2 -> ord.ord_cmp_asc x1 x2 < 0)
      | LE -> (fun x1 x2 -> ord.ord_cmp_asc x1 x2 <= 0)
      | GT -> (fun x1 x2 -> ord.ord_cmp_asc x1 x2 > 0)
      | GE -> (fun x1 x2 -> ord.ord_cmp_asc x1 x2 >= 0)


let icol_exclude : type t .
                     _ -> t Zinvcol.zinvcol -> t -> Zset.zset -> Zset.zset =
  fun pool icol x set ->
    let it = Zinvcol.iter icol set in
    match
      it # find EQ x  (* or Not_found *)
    with
      | () ->
          let rows_eq_x = it # current_zset pool in
          Zset.diff pool set rows_eq_x
      | exception Not_found ->
          set

let pol_apply_to_set pool col pol all set =
  if pol then
    Zset.diff pool all set
  else
    set


let icol_defined : type t .
                   _ -> t Zcol.zcol -> t Zinvcol.zinvcol -> bool -> Zset.zset ->
                       Zset.zset =
  fun pool col icol pol set ->
    let ZcolDescr(name,ty,repr,ord) = Zcol.descr col in
    match repr with
      | SInv_multibyte _ ->
          (* sparse columns are defined everywhere by definition *)
          if pol then
            Zset.empty
          else
            set
      | Inv_multibyte _ ->
          let s = Zinvcol.nonanon_zset icol set pool in
          pol_apply_to_set pool col pol set s
      | _ ->
          assert false

let icol_defined_some : type t .
                   _ -> t option Zcol.zcol -> t option Zinvcol.zinvcol -> 
                   bool -> Zset.zset ->
                   Zset.zset =
  fun pool col icol pol set ->
    let s = Zinvcol.nonnone_zset icol set pool in
    pol_apply_to_set pool col pol set s


let icol_iterate : type t .
                     _ -> t Zcol.zcol -> t Zinvcol.zinvcol -> comparison -> t ->
                     conjunction ->
                     conjunction =
  fun pool col icol cmp x conj ->
    let ZcolDescr(name,ty,repr,ord) = Zcol.descr col in
    match StrMap.find name conj.iterate with
      | Iterate(excol,lbound,ubound) ->
          let ZcolDescr(_,exty,_,_) = Zcol.descr excol in
          ( match same_ztype ty exty with
              | Equal ->
                  let lbound' =
                    and_lbound_opt col lbound (cmp,x) in
                  let ubound' =
                    and_ubound_opt col ubound (cmp,x) in
                  let iterate' =
                    StrMap.add
                      name (Iterate(col,lbound',ubound'))
                      conj.iterate in
                  { conj with iterate = iterate' }
              | Not_equal ->
                  assert false
          )
      | exception Not_found ->
          let lbound' =
            and_lbound_opt col None (cmp,x) in
          let ubound' =
            and_ubound_opt col None (cmp,x) in
          let iterate' =
            StrMap.add
              name (Iterate(col,lbound',ubound'))
              conj.iterate in
          { conj with iterate = iterate' }


let dcol_test col dcol cmp x =
  let ZcolDescr(_,_,_,ord) = Zcol.descr col in
  let get = Zdircol.value_of_id_unsafe dcol in
  match cmp with
    | EQ ->
        (fun id -> ord.ord_cmp_asc (get id) x = 0)
    | NE ->
        (fun id -> ord.ord_cmp_asc (get id) x <> 0)
    | LT ->
        (fun id -> ord.ord_cmp_asc (get id) x < 0)
    | LE ->
        (fun id -> ord.ord_cmp_asc (get id) x <= 0)
    | GT ->
        (fun id -> ord.ord_cmp_asc (get id) x > 0)
    | GE ->
        (fun id -> ord.ord_cmp_asc (get id) x >= 0)


let dcol_test_some : type t . t option Zcol.zcol ->
                              t option Zdircol.zdircol ->
                              _ -> t -> int -> bool =
  fun col dcol cmp x ->
    let ZcolDescr(_,_,_,ord) = Zcol.descr col in
    let get = Zdircol.value_of_id_unsafe dcol in
    let some_x = Some x in
    let test f id =
      match get id with
        | None -> false
        | Some _ as y -> f (ord.ord_cmp_asc y some_x) in
    match cmp with
      | EQ -> test (fun d -> d=0)
      | NE -> test (fun d -> d<>0)
      | LT -> test (fun d -> d<0)
      | LE -> test (fun d -> d<=0)
      | GT -> test (fun d -> d>0)
      | GE -> test (fun d -> d>=0)


let dcol_defined_some col dcol pol =
  let ZcolDescr(_,_,_,ord) = Zcol.descr col in
  let get = Zdircol.value_of_id_unsafe dcol in
  fun id ->
    match get id with
      | None -> pol
      | Some _ -> not pol


let col_defined_some pool col pol zset =
  (* internal use only *)
  match Zcol.content col with
    | Zcol.Inv icol ->
        icol_defined_some pool col icol pol zset
    | Zcol.Dir dcol ->
        Zset.filter
          pool
          (fun id ->
             dcol_defined_some col dcol pol id
          )
          zset


let col_test1 col f pol =
  let expected = if pol then F else T in
  match Zcol.content col with
    | Zcol.Dir dcol ->
        let get = Zdircol.value_of_id_unsafe dcol in
        (fun id -> f id (get id) = expected)
    | _ ->
        let get = Zcol.value_of_id col in
        (fun id -> f id (get id) = expected)

let col_testID f pol =
  let expected = if pol then F else T in
  (fun id -> f id = expected)

let zset ?base pool ztab flt =
  (* pol=false when we the included rows are requested.
     pol=true when the excluded rows are requested
   *)

  let check_col col =
    let ZcolDescr(name,_,_,_) = Zcol.descr col in
    if not (Ztable.contains_zcol col ztab) then
      failwith ("Zcontainer.Zfilter.zset: this column is not member of the \
                 table: " ^ name) in

  let rec gather_conj pol flt_list =
    List.fold_left
      (fun acc flt ->
         if refers_to_icol flt then
           match flt with
             | Colcmp(col,NE,x) when not pol ->
                 check_col col;
                 ( match Zcol.content col with
                     | Zcol.Inv icol ->
                         let f_any =
                           icol_exclude pool icol x in
                         { acc with anyset = acc.anyset @ [f_any] }
                     | _ ->
                         assert false
                 )
             | Colcmp(col,EQ,x) when pol ->
                 check_col col;
                 ( match Zcol.content col with
                     | Zcol.Inv icol ->
                         let f_any =
                           icol_exclude pool icol x in
                         { acc with anyset = acc.anyset @ [f_any] }
                     | _ ->
                         assert false
                 )
             | Colcmp(col,cmp,x) ->
                 check_col col;
                 let cmp = pol_comparison pol cmp in
                 ( match Zcol.content col with
                     | Zcol.Inv icol ->
                         icol_iterate pool col icol cmp x acc
                     | _ ->
                         assert false
                 )
             | Defined col ->
                 check_col col;
                 ( match Zcol.content col with
                     | Zcol.Inv icol ->
                         let f_any =
                           icol_defined pool col icol pol in
                         { acc with anyset = acc.anyset @ [f_any] }
                     | _ ->
                         assert false
                 )
             | Defined_some col ->
                 check_col col;
                 ( match Zcol.content col with
                     | Zcol.Inv icol ->
                         let f_any =
                           icol_defined_some pool col icol pol in
                         { acc with anyset = acc.anyset @ [f_any] }
                     | _ ->
                         assert false
                 )
             | _ ->
                 assert false
         else if refers_to_dcol flt then
           (* FIXME (perf): when the base set is ALL it makes sense to
              generate an [anyset] instead of [anytest]
            *)
           match flt with
             | Colcmp(col,cmp,x) ->
                 check_col col;
                 ( match Zcol.content col with
                     | Zcol.Dir dcol ->
                         let cmp = pol_comparison pol cmp in
                         let f_any =
                           dcol_test col dcol cmp x in
                         { acc with anytest = acc.anytest @ [f_any] }
                     | _ ->
                         assert false
                 )
             | Colcmp_some(col,cmp,x) ->
                 check_col col;
                 ( match Zcol.content col with
                     | Zcol.Dir dcol ->
                         let cmp = pol_comparison pol cmp in
                         let f_any =
                           dcol_test_some col dcol cmp x in
                         { acc with anytest = acc.anytest @ [f_any] }
                     | _ ->
                         assert false
                 )
             | Defined col ->
                 (* direct columns are defined everywhere *)
                 check_col col;
                 acc
             | Defined_some col ->
                 check_col col;
                 ( match Zcol.content col with
                     | Zcol.Dir dcol ->
                         let f_any =
                           dcol_defined_some col dcol pol in
                         { acc with anytest = acc.anytest @ [f_any] }
                     | _ ->
                         assert false
                 )                 
             | _ ->
                 assert false
         else
           match flt with
             | Test1(col,f) ->
                 check_col col;
                 let f_any =
                   col_test1 col f pol in
                 { acc with anytest = acc.anytest @ [f_any] }
             | TestID f ->
                 let f_any =
                   col_testID f pol in
                 { acc with anytest = acc.anytest @ [f_any] }
             | And l when not pol ->
                 gather_conj pol l
             | Or l when pol ->
                 gather_conj pol l
             | Or l when not pol ->
                 let l1 = List.map (fun flt -> gather_conj pol [flt]) l in
                 let f_any set =
                   let l2 = List.map (fun conj -> exec_conj conj set) l1 in
                   Zset.union_many pool l2 in
                 { acc with anyset = acc.anyset @ [f_any] }                 
             | And l when pol ->
                 let l1 = List.map (fun flt -> gather_conj pol [flt]) l in
                 let f_any set =
                   let l2 = List.map (fun conj -> exec_conj conj set) l1 in
                   Zset.union_many pool l2 in
                 { acc with anyset = acc.anyset @ [f_any] }                 
             | Not flt ->
                 gather_conj (not pol) [flt]
             | _ ->
                 assert false
      )
      empty_conj
      flt_list

  and exec_conj conj set =
    let set1 =
      StrMap.fold
        (fun _ iterate acc ->
           exec_conj_iterate iterate acc
        )
        conj.iterate
        set in
    let set2 =
      List.fold_left
        (fun acc f_any ->
           exec_conj_any f_any acc
        )
        set1
        conj.anyset in
    let set3 =
      if conj.anytest <> [] then
        Zset.filter
          pool
          (fun id ->
             List.for_all
               (fun f -> f id)
               conj.anytest
          )
          set2
      else
        set2 in
    set3

  and exec_conj_iterate (Iterate(col,lbound,ubound)) set =
    let iter =
      match Zcol.content col with
        | Zcol.Inv icol ->
            Zinvcol.iter ~all_values:true icol set
        | _ ->
            let inv = Zcol.invert col set in
            Zcol.iter_values inv in
    let outsets = ref [] in
    let c1 =
      match lbound with
        | None -> true
        | Some(cmp,x) ->
            match iter#find cmp x with
              | () -> true
              | exception Not_found -> false in
    let c2() =
      match ubound with
        | None -> true
        | Some(cmp,x) ->
            eval_cmp col cmp (snd iter#current_value) x in
    if c1 then (
      while not iter#beyond && c2() do
        outsets := iter#current_zset pool :: !outsets;
        iter # next()
      done
    );
    Zset.union_many pool !outsets

  and exec_conj_any f_any set =
    f_any set

  in
  let base =
    match base with
      | None ->
          Zset.all pool (Ztable.space ztab)
      | Some b ->
          b in
  exec_conj (gather_conj false [flt]) base


let colcmp_flt col cmp x =
  Colcmp(col,cmp,x)

let colcmp_some_flt col cmp x =
  Colcmp_some(col,cmp,x)

let defined_flt col =
  Defined col

let defined_some_flt col =
  Defined_some col

let colival_flt col l =
  Or
    (List.map
       (fun (x_from, x_to) ->
          And [ Colcmp(col, GE, x_from);
                Colcmp(col, LE, x_to)
              ]
       )
       l
    )

let colival_some_flt col l =
  Or
    (List.map
       (fun (x_from, x_to) ->
          And [ Colcmp_some(col, GE, x_from);
                Colcmp_some(col, LE, x_to)
              ]
       )
       l
    )

let colexpr1_flt col f =
  let g id x =
    try
      if f id x then T else F
    with
      | Not_found -> N in
  Test1(col, g)

let colexpr2_flt col1 col2 f =
  let get2 = Zcol.value_of_id col2 in
  let g id x =
    try
      let y = get2 id in
      if f id x y then T else F
    with
      | Not_found -> N in
  Test1(col1, g)

let colexpr3_flt col1 col2 col3 f =
  let get2 = Zcol.value_of_id col2 in
  let get3 = Zcol.value_of_id col3 in
  let g id x =
    try
      let y = get2 id in
      let z = get3 id in
      if f id x y z then T else F
    with
      | Not_found -> N in
  Test1(col1, g)

let rowexpr_flt ztab f =
  let g id =
    try
      if f id then T else F
    with
      | Not_found -> N in
  TestID g

let and_flt l =
  And l

let or_flt l =
  Or l

let not_flt flt =
  Not flt

