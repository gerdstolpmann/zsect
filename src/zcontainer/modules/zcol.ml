(* API to columns of all kinds *)

open Ztypes

type 'a zcontent =
  | Inv of 'a Zinvcol.zinvcol
  | Dir of 'a Zdircol.zdircol

type 'a zcol =
    { descr : 'a zcoldescr;
      content : 'a zcontent;
      bundle : (int * 'a) list zcol option;
    }

class type ['a] ziterator =
  object
    method current_pos : int
    method current_value : int * 'a
    method current_value_lz : (int * 'a) Lazy.t
    method current_zset : Zset.pool -> Zset.zset
    method current_zset_lz : Zset.pool -> Zset.zset Lazy.t
    method beyond : bool
    method next : unit -> unit
    method prev : unit -> unit
    method go_beginning : unit -> unit
    method go_end : unit -> unit
    method go : int -> unit
    method find : comparison -> 'a -> unit
    method length : int
  end

type 'a zcol_inverted =
  | IInv of 'a Zinvcol.zinvcol * Zset.zset list
  | IDir of 'a Zdircol.zdircol * Zset.zset list * 'a ziterator Lazy.t

let of_zinvcol descr zinvcol =
  { descr;
    content = Inv zinvcol;
    bundle = None;
  }

let of_zdircol descr zdircol =
  { descr;
    content = Dir zdircol;
    bundle = None;
  }

let from_file directory sp descr : 'a zcol =
  let ZcolDescr(name,zt,zr,zo) = descr in
  let content =
    match zr with
      | Inv_multibyte _ 
      | SInv_multibyte _ ->
          Inv(Zinvcol.from_file directory sp descr)
      | Dir_multibyte _ 
      | Dir_fixed64 _ ->
          Dir(Zdircol.from_file directory sp descr)
      | Dir_bigarray _ ->
          failwith "Zcontainer.Zcol.from_file: cannot load bigarrays"
      | Virtual_column ->
          failwith "Zcontainer.Zcol.from_file: cannot load virtual columns" in
  { descr; content; bundle = None; }

let value_of_id col =
  match col.content with
    | Inv zinvcol ->
        Zinvcol.value_of_id zinvcol
    | Dir zdircol ->
        Zdircol.value_of_id zdircol

let value_array_of_zset col =
  match col.content with
    | Inv zinvcol ->
        Zinvcol.value_array_of_zset zinvcol
    | Dir zdircol ->
        Zdircol.value_array_of_zset zdircol

let space col =
  match col.content with
    | Inv zinvcol ->
        Zinvcol.space zinvcol
    | Dir zdircol ->
        Zdircol.space zdircol

let descr col =
  col.descr


let content col =
  col.content
  


let invert_union (col : 'a zcol) base_zsets : 'a zcol_inverted =
  (* For Zdircol: slow.
     For Zinvcol and base_zset=ALL: just iterates over the values.
     For Zinvcol and base_zset<ALL: inverts the index (grouping)
   *)
  match col.content with
    | Inv zinvcol ->
        IInv(zinvcol, base_zsets)
    | Dir zdircol ->
        (* FIXME: this is not thread-safe! Ensure that the lazy expression
           can only be run by a single thread!
         *)
        let itr = lazy (Zdircol.iter_all zdircol) in
        IDir(zdircol, base_zsets, itr)


let invert col base_zset =
  invert_union col [base_zset]


let rows_with_value pool icol x =
  try
    let zset, base_zsets =
      match icol with
        | IInv(zinvcol, base_zsets) ->
            let ZcolDescr(name,zt,zr,zo) = Zinvcol.descr zinvcol in
            let idx = Zinvcol.find zinvcol EQ x in
            (Zinvcol.get_zset zinvcol idx, base_zsets)
        | IDir(zdircol, base_zsets, itr_lz) ->
            let itr = Lazy.force itr_lz in
            itr # find EQ x;  (* or Not_found *)
            (itr # current_zset pool, base_zsets) in
    match base_zsets with
      | [] -> Zset.empty
      | [base_zset] ->
          if Zset.same_space base_zset zset && Zset.is_all base_zset then
            zset
          else
            Zset.isect pool base_zset zset
      | _ ->
          let l = List.map (Zset.isect pool zset) base_zsets in
          Zset.union_many pool l
  with
    | Not_found -> Zset.empty


let iter_values icol =
  match icol with
    | IInv(zinvcol, base_zsets) ->
        Zinvcol.iter_union zinvcol base_zsets
    | IDir(zdircol, base_zsets, _) ->
        (* CHECK: maybe return a clone of itr when base_zset=all *)
        Zdircol.iter_union zdircol base_zsets

let bundle_element col k =
  let ZcolDescr(n,ty,_,ord) = col.descr in
  match ty with
    | Zbundle ty1 ->
        ( match col.content with
            | Inv icol ->
                let content' = Inv(Zinvcol.select icol k) in
                let descr' = ZcolDescr(Printf.sprintf "%s[%d]" n k,
                                       ty1,
                                       Virtual_column,
                                       extract_ord_from_bundle ty1 ord) in
                { content = content';
                  descr = descr';
                  bundle = Some col;
                }
            | Dir _ ->
                failwith "Zcontainer.Zcol.bundle_zcol: direct bundles not \
                          supported"
        )
    | _ ->
        failwith "Zcontainer.Zcol.bundle_zcol: not a bundle"

let bundle_indices col =
  match col.content with
    | Inv icol ->
        Zinvcol.indices icol
    | Dir _ ->
        failwith "Zcontainer.Zcol.bundle_indices: direct bundles not \
                  supported"

let containing_bundle col =
  col.bundle
