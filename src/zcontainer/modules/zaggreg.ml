open Ztypes

let selection_ht icol zset =
  let ht = Hashtbl.create 19 in
  Zset.iter
    (fun start len ->
       for id = start to start+len-1 do
         try
           let i = Zinvcol.index_of_id icol id in
           if not (Hashtbl.mem ht i) then
             Hashtbl.add ht i ()
         with Not_found -> ()
       done
    )
    zset;
  ht

let get_any_zset icol i base_zset =
  try Zinvcol.get_zset icol i
  with Zinvcol.Anonymous ->
    let pool = Zset.create_pool 1000 100 in
    Zinvcol.anon_zset icol base_zset pool


let fold : type t a . (int -> t -> a -> a) -> t Zcol.zcol -> _ -> a -> a =
  fun f col zset acc ->
    let acc = ref acc in
    let space = Zcol.space col in
    let ZcolDescr(_,ty,repr,_) = Zcol.descr col in
    match ty, Zcol.content col with
      | Zfloat, Zcol.Dir dcol when Zrepr.is_std_float_f64 repr &&
                                     Zset.space_less_or_equal_than zset space ->
          (* fast path *)
          Zset.iter
            (fun start len ->
               for id = start to start+len-1 do
                 acc := f id (Zdircol.float_of_id_unsafe dcol id) !acc
               done
            )
            zset;
          !acc
      | _, Zcol.Dir dcol ->
          let get =
            if Zset.space_less_or_equal_than zset space then
              Zdircol.value_of_id_unsafe dcol
            else
              Zdircol.value_of_id dcol in
          Zset.iter
            (fun start len ->
               for id = start to start+len-1 do
                 acc := f id (get id) !acc
               done
            )
            zset;
          !acc
      | _, Zcol.Inv icol ->
          (* cannot use the iterator here because we don't have a pool *)
          if Zset.count zset > Zinvcol.length icol then (
            for i = 0 to Zinvcol.length icol - 1 do
              let v = Zinvcol.get_value icol i in
              let z = get_any_zset icol i zset in
              Zset.iter_isect
                (fun start len ->
                   for id = start to start+len-1 do
                     acc := f id v !acc
                   done
                )
                z zset
            done; 
            !acc
          ) else (
            (* Pick only those values where we have an intersection: *)
            let ht = selection_ht icol zset in
            (* always include the last value, in the case the column is sparse: *)
            Hashtbl.replace ht (Zinvcol.length icol - 1) ();
            Hashtbl.iter
              (fun i _ ->
                 let v = Zinvcol.get_value icol i in
                 let z = get_any_zset icol i zset in
                 Zset.iter_isect
                   (fun start len ->
                    for id = start to start+len-1 do
                      acc := f id v !acc
                    done
                   )
                   z zset
              )
              ht;
            !acc
          )


let fold_some : type t a . (int -> t -> a -> a) -> t option Zcol.zcol -> _ -> a -> a =
  fun f col zset acc ->
    let acc = ref acc in
    let space = Zcol.space col in
    match Zcol.content col with
      | Zcol.Dir dcol ->
          let get =
            if Zset.space_less_or_equal_than zset space then
              Zdircol.value_of_id_unsafe dcol
            else
              Zdircol.value_of_id dcol in
          Zset.iter
            (fun start len ->
               for id = start to start+len-1 do
                 match get id with
                   | None -> ()
                   | Some v ->
                       acc := f id v !acc
               done
            )
            zset;
          !acc
      | Zcol.Inv icol ->
          (* cannot use the iterator here because we don't have a pool *)
          if Zset.count zset > Zinvcol.length icol then (
            for i = 0 to Zinvcol.length icol - 1 do
              let v_opt = Zinvcol.get_value icol i in
              match v_opt with
                | None -> ()
                | Some v ->
                    let z = get_any_zset icol i zset in
                    Zset.iter_isect
                      (fun start len ->
                         for id = start to start+len-1 do
                           acc := f id v !acc
                         done
                      )
                      z zset
            done; 
            !acc
          ) else (
            (* Pick only those values where we have an intersection: *)
            let ht = selection_ht icol zset in
            (* always include the last value, in the case the column is sparse: *)
            Hashtbl.replace ht (Zinvcol.length icol - 1) ();
            Hashtbl.iter
              (fun i _ ->
                 let v_opt = Zinvcol.get_value icol i in
                 match v_opt with
                   | None -> ()
                   | Some v ->
                       let z = get_any_zset icol i zset in
                       Zset.iter_isect
                         (fun start len ->
                            for id = start to start+len-1 do
                              acc := f id v !acc
                            done
                         )
                         z zset
              )
              ht;
            !acc
          )

let fold_values : type t a . (t -> a -> a) -> t Zcol.zcol -> _ -> a -> a =
  fun f col zset acc ->
    match Zcol.content col with
      | Zcol.Dir _ ->
          fold (fun _ v acc -> f v acc) col zset acc
      | Zcol.Inv icol ->
          let acc = ref acc in
          (* cannot use the iterator here because we don't have a pool *)
          if Zset.count zset > Zinvcol.length icol then (
            for i = 0 to Zinvcol.length icol - 1 do
              let v = Zinvcol.get_value icol i in
              let z = get_any_zset icol i zset in
              if Zset.isect_non_empty z zset then
                acc := f v !acc
            done; 
            !acc
          ) else (
            (* Pick only those values where we have an intersection: *)
            let ht = selection_ht icol zset in
            (* always include the last value, in the case the column is sparse: *)
            Hashtbl.replace ht (Zinvcol.length icol - 1) ();
            Hashtbl.iter
              (fun i _ ->
                 let z = get_any_zset icol i zset in
                 if Zset.isect_non_empty z zset then
                   let v = Zinvcol.get_value icol i in
                   acc := f v !acc
              )
              ht;
            !acc
          )

let fold_distinct_values
  : type t a . (t -> a -> a) -> t Zcol.zcol -> _ -> a -> a =
  fun f col zset acc ->
    match Zcol.content col with
      | Zcol.Dir _ ->
          let ht = Hashtbl.create 19 in
          fold
            (fun _ v acc ->
               if not(Hashtbl.mem ht v) then (
                 Hashtbl.add ht v ();
                 f v acc
               ) else acc
            )
            col zset acc
      | Zcol.Inv _ ->
          fold_values f col zset acc

let pass_on_min col =
  let ZcolDescr(_,_,_,ord) = Zcol.descr col in
  fun v acc ->
    match acc with
      | None -> Some v
      | Some a ->
          if ord.ord_cmp_asc a v < 0 then
            Some v
          else
            acc

let pass_on_max col =
  let ZcolDescr(_,_,_,ord) = Zcol.descr col in
  fun v acc ->
    match acc with
      | None -> Some v
      | Some a ->
          if ord.ord_cmp_asc a v > 0 then
            Some v
          else
            acc

let pass_on_min_some (col : _ option Zcol.zcol) =
  let ZcolDescr(_,_,_,ord) = Zcol.descr col in
  fun v_opt acc ->
    match acc with
      | None -> v_opt
      | Some a ->
          (match v_opt with
             | None -> acc
             | Some v ->
                 if ord.ord_cmp_asc acc v_opt < 0 then
                   v_opt
                 else
                   acc
          )

let pass_on_max_some col =
  let ZcolDescr(_,_,_,ord) = Zcol.descr col in
  fun v_opt acc ->
    match acc with
      | None -> v_opt
      | Some a ->
          (match v_opt with
             | None -> acc
             | Some v ->
                 if ord.ord_cmp_asc acc v_opt > 0 then
                   v_opt
                 else
                   acc
          )
  
let fold_values1 f col zset =
  let x_opt =
    fold_values f col zset None in
  match x_opt with
    | None -> raise Not_found
    | Some x -> x

let min col zset = fold_values1 (pass_on_min col) col zset

let max col zset = fold_values1 (pass_on_max col) col zset

let min_some col zset = fold_values1 (pass_on_min_some col) col zset

let max_some col zset = fold_values1 (pass_on_max_some col) col zset

let sum col zset =
  fold (fun _ x acc -> x +. acc) col zset 0.0

let sum_some col zset =
  fold_some (fun _ x acc -> x +. acc) col zset 0.0

let sum_map f col zset =
  fold (fun id x acc -> f id x +. acc) col zset 0.0

let count f col zset =
  fold (fun id x acc -> if f id x then acc+1 else acc) col zset 0

let count_some col zset =
  fold
    (fun id x acc ->
       match x with
         | None -> acc
         | Some _ -> acc+1
    )
    col zset 0

let avg col zset =
  let s = sum col zset in
  let n = Zset.count zset in
  s /. float n

let avg_some col zset =
  let s = sum_some col zset in
  let n = count_some col zset in
  s /. float n

let avg_map f col zset =
  let s = sum_map f col zset in
  let n = Zset.count zset in
  s /. float n

let sq x = x *. x

let var col zset =
  let a = avg col zset in
  let n = Zset.count zset in
  let s =
    sum_map
      (fun id x -> sq(x -. a))
      col
      zset in
  s /. float n

let var_some col zset =
  let s0 = sum_some col zset in
  let n = count_some col zset in
  let a = s0 /. float n in
  let s =
    sum_map
      (fun id x ->
         match x with
           | None -> 0.0
           | Some x0 -> sq(x0 -. a))
      col
      zset in
  s /. float n

let var_map f col zset =
  let a = avg_map f col zset in
  let n = Zset.count zset in
  let s =
    sum_map
      (fun id x -> sq(f id x -. a))
      col
      zset in
  s /. float n

let nth_smallest_and_next_map : type s a . (int -> s -> a) -> 
                                     (a -> a -> int) ->
                                     s Zcol.zcol ->
                                     _ -> int -> a list =
  fun f cmp col zset n ->
    if Zset.is_empty zset then
      []
    else (
      if n < 1 then invalid_arg "Zcontainer.Zaggreg.nth_smallest_and_next_map";
      let module MAX =
        struct
          type t = a
          let compare (x:t) (y:t) = cmp y x
        end in
      let module U = Zutil.OrdUtil(MAX) in
      let id0 = Zset.first zset in
      let x0 = f id0 (Zcol.value_of_id col id0) in
      let h = U.heap_create (n+1) x0 in  (* this is a MAX-heap *)
      fold
        (fun id x () ->
           let y = f id x in
           let hlength = U.heap_length h in
           let do_insert =
             hlength < n+1 ||
               (hlength = n+1 &&
                  cmp y (U.heap_min h) < 0
               ) in
           let do_delete =
             do_insert && hlength = n+1 in
           if do_delete then
             U.heap_delete_min h;
           if do_insert then
             U.heap_insert h y
        )
        col
        zset
        ();
      if U.heap_length h < n then
        []
      else if U.heap_length h = n then
        [U.heap_min h]
      else
        [U.heap_snd_min h; U.heap_min h]
    )

let nth_smallest_and_next col zset n =
  let ZcolDescr(_,_,_,ord) = Zcol.descr col in
  nth_smallest_and_next_map
    (fun _ x -> x)
    ord.ord_cmp_asc
    col zset n

let nth_smallest_map f cmp col zset n =
  match nth_smallest_and_next_map f cmp col zset n with
    | x :: _ -> x
    | _ -> raise Not_found

let nth_smallest col zset n =
  let ZcolDescr(_,_,_,ord) = Zcol.descr col in
  nth_smallest_map
    (fun _ x -> x)
    ord.ord_cmp_asc
    col zset n

let full_rat_quantile_map f cmp p q col zset =
  if p <= 0 || q <= 0 || p >= q then
    invalid_arg "Zcontainer.Zaggreg.full_rat_quantile_map";
  if Zset.is_empty zset then
    []
  else
    let n = Zset.count zset in
    if p > max_int / n then
      failwith "Zcontainer.Zaggreg.full_rat_quantile_map: n*p too large";
    let np = n*p in
    if np mod q = 0 then
      nth_smallest_and_next_map f cmp col zset (np/q)
    else
      [nth_smallest_map f cmp col zset (np/q + 1)]

let full_rat_quantile p q col zset =
  let ZcolDescr(_,_,_,ord) = Zcol.descr col in
  full_rat_quantile_map (fun _ x -> x) ord.ord_cmp_asc p q col zset

let rat_quantile p q col zset =
  match full_rat_quantile p q col zset with
    | [] -> raise Not_found
    | [m] -> m
    | [m1;m2] -> 0.5 *. (m1 +. m2)
    | _ -> assert false

let rat_quantile_some p q col zset =
  let pool = Zset.create_pool 1000 100 in
  let zset1 = Zfilter.col_defined_some pool col false zset in
  let f _ x_opt =
    match x_opt with
      | Some x -> x
      | None -> assert false in
  let cmp = (compare : float -> float -> int) in
  match full_rat_quantile_map f cmp p q col zset1 with
    | [] -> raise Not_found
    | [m] -> m
    | [m1;m2] -> 0.5 *. (m1 +. m2)
    | _ -> assert false

let rat_quantile_map f p q col zset =
  let cmp = (compare : float -> float -> int) in
  match full_rat_quantile_map f cmp p q col zset with
    | [] -> raise Not_found
    | [m] -> m
    | [m1;m2] -> 0.5 *. (m1 +. m2)
    | _ -> assert false

let quantile_map f p col zset =
  if p <= 0.0 || p >= 1.0 then
    invalid_arg "Zcontainer.Zaggreg.quantile";
  if Zset.is_empty zset then
    raise Not_found
  else
    let n = Zset.count zset in
    let np = float n *. p in
    let cmp = (compare : float -> float -> int) in
    if floor np = ceil np then
      match nth_smallest_and_next_map f cmp col zset (truncate np) with
        | [] -> raise Not_found
        | [m] -> m
        | [m1; m2] -> 0.5 *. (m1 +. m2)
    | _ -> assert false
    else
      nth_smallest_map f cmp col zset (truncate(ceil np))

let quantile p col zset =
  quantile_map (fun _ x -> x) p col zset

let quantile_some p col zset =
  let pool = Zset.create_pool 1000 100 in
  let zset1 = Zfilter.col_defined_some pool col false zset in
  let f _ x_opt =
    match x_opt with
      | Some x -> x
      | None -> assert false in
  quantile_map f p col zset1
  
let full_median col zset =
  full_rat_quantile 1 2 col zset

let full_median_map f cmp col zset =
  full_rat_quantile_map f cmp 1 2 col zset

let median col zset =
  rat_quantile 1 2 col zset

let median_some col zset =
  rat_quantile_some 1 2 col zset

let median_map f col zset =
  rat_quantile_map f 1 2 col zset
