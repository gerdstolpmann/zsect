(** The Zcontainer file format

Zcontainer is a format for column-oriented databases. So far, these
databases can only be created with the digester
({!Zcontainer.Zdigester}). The digester allows it to load the database
row by row. Every row gets an ID at this point. The IDs are in the
range 1..N; this range is also called the {b space}.

Once digested, the database can be opened by calling {!Ztable.from_file}.
Note that the columns of the database are read-only after they have been
created. It is possible, though, to add and delete columns to existing
tables.

There are two types of columns:
 - {b direct} columns just provide a mapping from ID to the value:
   {[ id -> value ]}
 - {b inverted} columns mainly provide a mapping from the value to the
   set of IDs where the row has this value:
   {[ value -> set(ID) ]}
   Inverted columns also store information to quickly undo the inversion,
   i.e.
   {[ id -> value ]}
   accesses are possible, too, but a little bit slower than for direct
   columns.

Columns have a {b type}. The possible types are defined by 
{!Ztypes.ztype}, and are limited to string and a few numeric types.

Columns have an {b order}. This is mainly important for inverted
access, as it is possible to iterate over the values in a column
in the pre-defined order. Note, however, that even direct columns
are ordered, because the library also includes the possibility to
invert such columns on the fly when needed.

Columns also have a {b representation}. Some types can be stored in
a fixed number of bytes (e.g. float). For these types there is the
"fixed64" representation, which allows it to store values in a
fixed multiple of 64 bit words. "fixed64" is only available for direct
columns. If the number of bytes varies, there is the "multibyte"
representation, which is available for both types of columns, but
is spacier.

There are names for the representations and orders available in this
library, see {!Zrepr.stdrepr}.
 *)

module Ztypes : sig
  (** {2 Fundamental types} *)

  module StrMap : Map.S with type key = string

  (** Equality witness for GADTs *)
  type (_,_) type_eq =
    | Equal : ('a, 'a) type_eq
    | Not_equal

  (** The space defines the range of IDs, 1..N *)
  type space = private Space of int

  val create_space : int -> space
  (** Create a new space for a given N *)

  (** The column types *)
  type _ ztype =
    | Zstring : string ztype
    | Zint64 : int64 ztype
    | Zint : int ztype
    | Zfloat : float ztype
    | Zbool : bool ztype
    | Zoption : 'a ztype -> 'a option ztype
    | Zpair : 'a ztype * 'b ztype -> ('a * 'b) ztype
    | Zlist : 'a ztype -> 'a list ztype
    | Zbundle : 'a ztype -> (int * 'a) list ztype
  (* TODO: Zcomplex *)

  (** A box for a column type (hides the type parameter) *)
  type ztypebox =
    | ZtypeBox : _ ztype -> ztypebox

  (** Fixed64 data is stored in bigarrays *)
  type fixed64_data =
      (int64, Bigarray.int64_elt, Bigarray.c_layout) Bigarray.Array1.t

  (** Defines a "fixed64" representation. Note that an implementation need
      not to check the arguments of [f64_encode] and [f64_decode] (i.e. these
      functions may be "unsafe").
   *)
  type 'a fixed64 =
      { f64_name : string;           (** User name *)
        f64_size : int;              (** size in 64 bit words *)
        f64_encode : fixed64_data -> int -> 'a -> unit; (** Store a value in a bigarray *)
        f64_decode : fixed64_data -> int -> 'a; (** Get a value from a bigarray *)
      }

  (** A box for a "fixed64" representation *)
  type fixed64box =
    | Fixed64Box : 'a ztype * 'a fixed64 -> fixed64box

  (** Defines a "multibyte" representation. Note that an implementation need
      not to check the arguments of [mb_encode] and [mb_decode] (i.e. these
      functions may be "unsafe").
   *)
  type 'a multibyte =
      { mb_name : string;            (** User name *)
        mb_size : 'a -> int;         (** Size of a value in bytes *)
        mb_encode : Bytes.t -> int -> 'a -> unit; (** Encode a value *)
        mb_decode : Bytes.t -> int -> int -> 'a;   (** Decode a value *)
      }

  (** A box for a "multibyte" representation *)
  type multibytebox =
    | MultibyteBox : 'a ztype * 'a multibyte -> multibytebox

  (** A function for construction multibyte option types *)
  type multibyteoption =
    { multibyteOption : 'a . ('a ztype -> 'a multibyte -> 'a option multibyte) }

  (** A function for construction multibyte list types *)
  type multibytelist =
    { multibyteList : 'a . ('a ztype -> 'a multibyte -> 'a list multibyte) }

  (** A function for construction multibyte bundle types *)
  type multibytebundle =
    { multibyteBundle : 'a . ('a ztype -> 'a multibyte ->
                              (int * 'a) list multibyte) }

  (** A function for construction multibyte option pairs *)
  type multibytepair =
    { multibytePair :
        'a 'b . 'a ztype -> 'a multibyte -> 'b ztype -> 'b multibyte ->
        ('a*'b) multibyte
    }

  (** Possible representations *)
  type 'a zrepr =
    | Inv_multibyte of 'a multibyte    (** Inverted multibyte *)
    | SInv_multibyte of 'a multibyte * 'a * ('a -> bool)
        (** Sparse inverted multibyte. The additional value is the anonymous
            element, and the function a test for the anonymous element.
            The anonymous element must be the largest element.
         *)
    | Dir_multibyte of 'a multibyte    (** Direct multibyte *)
    | Dir_fixed64 of 'a fixed64        (** Direct fixed64 *)
    | Dir_bigarray : ('a,'b) Bigarray.kind -> 'a zrepr  (** Direct bigarray (in-memory only) *)
    | Virtual_column  (** Column derived from another column *)

  (** A box for a representation *)
  type zreprbox =
    | ZreprBox : 'a ztype * 'a zrepr -> zreprbox

  (** Direction *)
  type direction =
    | ASC | DESC

  (** The composition says how [ord_hash] and [ord_cmp] interact *)
  type ord_composition = private
    | Ord_leaf
    | Ord_pair
    | Ord_option
    | Ord_list
    | Ord_bundle_pair
    | Ord_bundle_list
    | Ord_custom

  (** Defines an ordering function. For sorting the values in an inverted
      column, the functions [ord_hash] and [ord_cmp] are used in this
      way: Two values are first ordered by their 
      hash value (a non-negative 62 bit int), and when the hashes are equal,
      the comparison function is used.

      The function [ord_cmp_asc] is used in other contexts, e.g. for
      comparing two values extracted from the column. In this case,
      the hash function doesn't play any role.

      In order to avoid problems it is recommended that the hash function
      is chosen as being compatible with the comparison function, i.e.
      so that [h(x1) < h(x2) => x1 < x2] and [h(x1) > h(x2) => x1 > x2].
      The hash function is then mainly used as accelerator.
   *)
  type 'a zorder =
      { ord_name : string;            (** User name *)
        ord_comp : ord_composition;   (** Type of composition *)
        ord_hash : 'a -> int;         (** Get the hash *)
        ord_cmp : 'a -> 'a -> int;    (** Comparison function (asc or desc) *)
        ord_cmp_asc : 'a -> 'a -> int;    (** Comparison function (ascending) *)
        ord_dir : direction option;   (** for leaf types only *)
        ord_sub : zorderbox array;    (** the inner orderings of composed types *)
      }

  (** A box for a zorder *)
  and zorderbox =
    | ZorderBox : 'a ztype * 'a zorder -> zorderbox

  (** Generic ordering of options *)
  type zorderoption =
    { zorderOption : 'a . 'a ztype -> 'a zorder -> 'a option zorder }

  (** Generic ordering of lists *)
  type zorderlist =
    { zorderList : 'a . 'a ztype -> 'a zorder -> 'a list zorder }

  (** Generic ordering of pairs *)
  type zorderpair =
    { zorderPair : 'a 'b . 'a ztype -> 'a zorder -> 'b ztype -> 'b zorder ->
                   ('a * 'b) zorder
    }

  (** Conversion *)
  type zorderconv =
    { zorderConv : 'a 'b . 'a ztype -> 'b ztype -> 'b zorder -> 'a zorder option
    }

  type zcolname = string

  (** A column description, consisting of a name, a type, a representation,
      and an order *)
  type _ zcoldescr =
    | ZcolDescr : zcolname * 'a ztype * 'a zrepr * 'a zorder -> 'a zcoldescr

  (** A box for a column description *)
  type zcolbox =
    | ZcolBox : zcolname * 'a ztype * 'a zrepr * 'a zorder -> zcolbox

  (** A library of standard representations and standard orderings *)
  type stdrepr =
      { f64dict : fixed64box StrMap.t;
        mbdict : multibytebox StrMap.t;
        mboption : multibyteoption option;
        mbpair : multibytepair option;
        mblist : multibytelist option;
        mbbundle : multibytebundle option;
        orddict : zorderbox StrMap.t;
        ordoption : zorderoption option;
        (* ordlist : zorderlist option; *)
        ordpair : zorderpair option;
        ordconv : zorderconv;
      }

  (** Comparison selectors *)
  type comparison =
    | EQ | NE | LT | LE | GT | GE

  exception End_of_column

  val ord_custom : ord_composition
    (** returns [Ord_custom] *)

  val string_of_ztype : _ ztype -> string
    (** Get the user name of a type *)

  val string_of_zrepr : _ zrepr -> string
    (** Get the user name of a representation. Such names are composed
        of a prefix and the name of the multibyte or fixed64 representation:
         - "IM/" (for inverted multibyte)
         - "SIM/" (for inverted multibyte with sparse index)
         - "DM/" (for direct multibyte)
         - "DF64/" (for direct fixed64)

        E.g. "IM/int64" would select an inverted multibyte representation for
        int64.
     *)

  val debug_string_of_value : 'a ztype -> 'a -> string
    (** Get a string representation suitable for debug messages *)

  val string_of_zorder : _ zorder -> string
    (** Get the user name of a zorder *)

  val ztypebox_of_string : string -> ztypebox
    (** Parses the user name of a type. Raises a [Failure] on parsing error *)

  val zreprbox_of_string : stdrepr -> string -> zreprbox
    (** Parses the user name of a representation. Raises a [Failure] on
        parsing error *)

  (* TODO: zrepr_of_string : stdrepr -> 'a ztype -> string -> 'a zrepr *)

  val zorderbox_of_string : stdrepr -> ztypebox -> string -> zorderbox
    (** Parses the user name of a zorder. Raises a [Failure] on parsing error *)

  val zorder_of_string : stdrepr -> 'a ztype -> string -> 'a zorder
    (** Parses the user name of a zorder. Raises a [Failure] on parsing error *)

  val natural_zorder : stdrepr -> 'a ztype -> direction -> 'a zorder
    (** Extracts the "natural" order (ascending or descending) *)

  val same_ztype : 'a ztype -> 'b ztype -> ('a,'b) type_eq
    (** Witnesses whether two types are the same. *)

  val same_zrepr : 'a zrepr -> 'a zrepr -> bool
    (** Checks whether two representations are the same. So far, this is
        only assumed when the same multibyte or fixed64 representation is
        used
     *)

  val same_descr : 'a zcoldescr -> 'a zcoldescr -> bool
    (** Checks whether two columns descriptors are the same *)

  val json_of_value : 'a ztype -> 'a -> Yojson.Safe.json
    (** returns a JSON representation of a value *)

  val value_of_json : 'a ztype -> Yojson.Safe.json -> 'a
    (** deserialized JSON returned by [json_of_value] *)

  val null_value : 'a ztype -> 'a
    (** return a value that takes over the role of the "null" for the given
        type
     *)

  val json_of_zcolbox : zcolbox -> Yojson.Safe.json

  val zcolbox_of_json : stdrepr -> Yojson.Safe.json -> zcolbox

  end

module Zset : sig
  (** {2 Compressed sets of IDs} *)

  type pool
    (** Sets are stored in pools. Note that a pool can only be used by a
        single thread at a time. *)

  type zset
    (** A set of IDs *)

  type zset_builder
    (** Building a set of IDs *)

  type zset_data = (int, Bigarray.int_elt, Bigarray.c_layout) Bigarray.Array1.t
    (** Sets may be stored in bigarrays *)

  (** The encoding type:
       - [Array_enc] represents a set as sorted pairs of ranges [(start,len)]
       - [Packed_enc] is a compressed variant of [Array_enc] using a
         runlength encoding
       - [Bitset_enc] represents a set as bitvector.

      Packed encoding is the only choice for sets that are written to
      files. The other encodings are only available for sets in RAM.
   *)
  type zset_enctype =
    | Packed_enc
    | Array_enc
    | Bitset_enc

  exception Decode_error

  val create_pool : ?enctype:zset_enctype -> int -> int -> pool
    (** [create_pool data_size zset_size]: Creates a pool with space for
        initially [data_size] number of 64 bit words, and [zset_size] number
        of sets. Note that pools are grown as needed.

        The pool also specified the encoding type. [enctype] defaults
        to [Packed_enc]. Note that [Array_enc] is faster but consumes
        also much more memory. Also, [Array_enc] cannot be serialized.
     *)

  val default_pool : pool
    (** A read-to-use pool *)

  val bitset_pool : pool
    (** A pool that indicates that bitvectors are used to represent sets *)

  val build_begin : pool -> int -> Ztypes.space -> zset_builder
    (** [build_begin p data_size space]: Start building a new set in the
        pool [p] that is a subset of [space]. Pass in [data_size] the
        initial number of 64 bit words reserved for the set.

        For performance reasons, you should only build one set at a time
        in a pool! If you build several large sets, the sets need to be
        frequently moved inside the pool.
     *)

  val build_add : zset_builder -> int -> unit
    (** [build_add b id]: Add the [id] to the set. The [id] must be larger
        than any previously added [id].
     *)

  val build_add_range : zset_builder -> int -> int -> unit
    (** [build_add_range b start len]: Add the range of IDs [start] to
        [start+len-1] to the set.
     *)

  val build_end : zset_builder -> zset
    (** Finish the build and get the made zset *)

  val empty : zset
    (** The empty set is a constant *)

  val all : pool -> Ztypes.space -> zset
    (** Get the set of all IDs in the space *)

  val from_array : pool -> Ztypes.space -> int array -> zset
    (** Get a set from an array of IDs. The array must be sorted. *)

  val from_data : zset_data -> int -> int -> Ztypes.space -> int -> int -> int ->
                  zset
    (** [from_data data offset size space count first last]: Get a set from
        the encoded data in a bigarray [data], where the set starts at
        [offset] and consists of [size] words (64 bit). You also need to
        set the number of IDs, [count], the first ID, [first], and the
        last ID, [last]. (Note that these three numbers are not part of the
        encoding.)

        The returned set still points to the bigarray, i.e. the data is
        not copied.

        It is assumed that the bigarray uses packed encoding.
     *)

  val to_data : zset_data -> int -> zset -> unit
    (** [to_data data offset zset]: Copies the encoding of the set to the
        bigarray [data] at [offset].

        Fails when the set is array-encoded.
     *)

  val write_file : Unix.file_descr -> zset -> unit
    (** [write_file fd zset]: Write the packed encoding of the set to the file

        Fails when the set is array-encoded or a bitset.
     *)

  val write_tf : out_channel -> zset -> unit
    (** Writes the set in "transport format" to the channel. This is possible
        for all encodings.
     *)

  val read_enctype_tf : in_channel -> zset_enctype
    (** Reads the first character of a set in transport format, and returns
        the encoding represented
     *)

  val read_tf : pool -> in_channel -> zset
    (** Reads the data after the first character, and restores the set.
        The set is encoded as specified in the pool.
     *)

  val persist : zset -> string
    (** Copies the zset into shared memory. Returns the name of the
        POSIX memory segment.

        This function does not support [Array_enc] so far.
     *)

  val unpersist : string -> unit
    (** Deletes the shared memory segment *)

  val access : string -> zset
    (** Accesses the zset in the shared memory segment *)

  val byte_size : zset -> int
    (** Return the byte size of the encoding *)

  val is_empty : zset -> bool
    (** Whether the set is empty. Equivalent to [set = Zset.empty] *)

  val is_all : zset -> bool
    (** Whether the set is the set of all IDs of the space *)

  val same_space : zset -> zset -> bool
    (** Whether two sets have the same space *)

  val space_less_or_equal_than : zset -> Ztypes.space -> bool
    (** Whether the space of the set is less or equal to the passed space *)

  val max_space : Ztypes.space -> zset -> Ztypes.space
    (** [max_space space s]: returns the maximum of [space] and the space
        of [s]
     *)

  val min_space : Ztypes.space -> zset -> Ztypes.space
    (** [min_space space s]: returns the minimum of [space] and the space
        of [s]
     *)

  val count : zset -> int
    (** Return the number of IDs *)

  val first : zset -> int
    (** Return the first ID (raises [Not_found] when the set is empty) *)

  val last : zset -> int
    (** Return the last ID (raises [Not_found] when the set is empty) *)

  val to_array : zset -> int array
    (** Returns the IDs as a sorted array *)

  val to_range_arrays : zset -> int array * int array
    (** [let start,len = to_range_arrays zset]: Returns the IDs in two
        arrays [start] and [len], representing ranges.
     *)

  val dump : zset -> string
    (** return a string representation for debugging purposes, e.g.
        "5,78-91,101-199"
     *)

  val iter : (int -> int -> unit) -> zset -> unit
    (** [iter f zset]: iterate over the IDs. The function [f] is called
        as [f start len]
     *)

  val mem : int -> zset -> bool
    (** [mem id zset]: whether the ID is member of the set *)

  val copy : pool -> zset -> zset
    (** Return a copy that is allocated in the passed pool *)

  val isect : pool -> zset -> zset -> zset
    (** Intersects two sets, and allocates the result in the pool *)

  val isect_non_empty : zset -> zset -> bool
    (** Checks whether the intersection is non-empty *)

  val iter_isect : (int -> int -> unit) -> zset -> zset -> unit
    (** [iter_isect f s1 s2]: Intersects s1 and s2, and calls [f start len]
        for all ranges of the result. Behaves identically to
        [iter f (isect pool s1 s2)], but doesn't allocate the intermediate
        set.
     *)

  val diff : pool -> zset -> zset -> zset
    (** Computes the difference of two sets, and allocates the result in 
        the pool *)

  val union : pool -> zset -> zset -> zset
    (** Computes the union of two sets, and allocates the result in 
        the pool *)

  val union_many : pool -> zset list -> zset
    (** Unites many sets *)

  val filter : pool -> (int -> bool) -> zset -> zset
    (** The function is called for every ID in the input set, and only
        when it evaluates to [true] the ID is also added to the output
        set
     *)

  (**/**)

  val internal_data : zset -> zset_data  (* for testing only *)
  val shift_space : pool -> Ztypes.space -> zset -> int -> zset
end

module Zrepr : sig
  (** {2 Multibyte and fixed64 representation} *)

  val stdrepr : Ztypes.stdrepr
    (** The standard representations and orderings. A representation is the
        combination of a prefix and a type, e.g. "IM/string". The prefix
        determines the kind of column (here, IM=inverted multibyte).

        Available prefixes:
         - "IM/" means inverted multibyte
         - "SIM/" means inverted multibyte with sparse index
         - "DM/" means direct multibyte
         - "DF64/" means direct fixed64

        Available types:
         - "string"
         - "int64"
         - "float" (double precision as in OCaml)
         - "bool"
         - any option type: "t option", when t is another representation,
           e.g. "int64 option"
         - any pair type: "t1 * t2" when t1 and t2 are other representations,
           e.g. "string * float"
         - any list type: "t list" when t is another representation,
           e.g. "string list"
         - note that you can use parentheses, e.g. "(string * float) option".

        For inverted or direct multibyte any type is permitted. For sparse
        inverted multitype the type must be an option. For
        fixed64, however, there is currently a restriction to:

        Available types for fixed64:
         - "int64"
         - "int" (distinct from "int64" but uses the same representation;
           also note that this is always a 63-bit int because zcontainer
           is not supported on 32 bit platforms)
         - "float"

        So, e.g. "IM/int64" means inverted multibyte int64, and
        "DM/string * float option" is a direct multibyte representation of
        a pair of a string and of a float option.

        {b Ordering:} You can specify the ordering simply as "asc" or "desc".
        In this case, all primitive components (i.e. the strings or numbers
        included in the type) are sorted in an ascending or descending way.
        Pairs are sorted lexicographically first by the left element, then
        by the right element. For options, the [None] value is sorted after
        any [Some] value ("nulls last").

        Alternatively, you can specify the ordering in a detailed manner
        by replacing the primitive components in the type expression with
        an ordering directive. E.g. for the type "(string * float) option" the
        ordering could be given as "(string_desc * float_asc) option".
        The structure of the ordering must be compatible with the type.
        The primitive orderings needs not to comply to the primitive types,
        though. For example, you can sort a string by "int64_asc" (sorts
        by the parsed number if possible, and falls back to string sorting
        for unparseable numbers).

        Available primitive ordering directives:
         - "string_asc": The order as defined by [String.compare]
           ("natural" order)
         - "string_desc": The reverse natural order
         - "int64_asc"
         - "int64_desc"
         - "int_asc"
         - "int_desc"
         - "float_asc"
         - "float_desc"
         - "bool_asc"
         - "bool_desc"

        You cannot change the ordering of lists, which is hard-coded,
        and always ascending.
     *)

  val option_mb : 'a Ztypes.ztype -> 'a Ztypes.multibyte ->
                    'a option Ztypes.multibyte
  val option_ord : 'a Ztypes.ztype -> 'a Ztypes.zorder ->
                   'a option Ztypes.zorder

  val list_mb : 'a Ztypes.ztype -> 'a Ztypes.multibyte ->
                'a list Ztypes.multibyte
  val list_ord : 'a Ztypes.ztype -> 'a Ztypes.zorder ->
                 'a list Ztypes.zorder

  val pair_mb : 'a Ztypes.ztype -> 'a Ztypes.multibyte ->
                'b Ztypes.ztype -> 'b Ztypes.multibyte ->
                ('a * 'b) Ztypes.multibyte
  val pair_ord : 'a Ztypes.ztype -> 'a Ztypes.zorder ->
                 'b Ztypes.ztype -> 'b Ztypes.zorder ->
                 ('a * 'b) Ztypes.zorder

  val bundle_mb : 'a Ztypes.ztype -> 'a Ztypes.multibyte ->
                  (int * 'a) list Ztypes.multibyte
  val bundle_ord : 'a Ztypes.ztype -> 'a Ztypes.zorder ->
                 (int * 'a) list Ztypes.zorder

  val string_mb : string Ztypes.multibyte
  val string_opt_mb : string option Ztypes.multibyte
  val string_asc : string Ztypes.zorder
  val string_desc : string Ztypes.zorder

  val int64_f64 : int64 Ztypes.fixed64
  val int64_mb : int64 Ztypes.multibyte
  val int64_opt_mb : int64 option Ztypes.multibyte
  val int64_asc : int64 Ztypes.zorder
  val int64_desc : int64 Ztypes.zorder

  val int_f64 : int Ztypes.fixed64
  val int_mb : int Ztypes.multibyte
  val int_opt_mb : int option Ztypes.multibyte
  val int_asc : int Ztypes.zorder
  val int_desc : int Ztypes.zorder

  val float_f64 : float Ztypes.fixed64
  val float_mb : float Ztypes.multibyte
  val float_opt_mb : float option Ztypes.multibyte
  val float_asc : float Ztypes.zorder
  val float_desc : float Ztypes.zorder

  val bool_mb : bool Ztypes.multibyte
  val bool_opt_mb : bool option Ztypes.multibyte
  val bool_asc : bool Ztypes.zorder
  val bool_desc : bool Ztypes.zorder

end

module Zinvcol : sig
  (** {2 Inverted columns} *)

  (** An inverted column consists logically of a sequence of entries. Every
      entry has a value and a set of row IDs where the row has the value.

      Sparse inverted columns (SIM) have a special value, the anonymous value.
      This value is not stored in the file, and it is taken as a default
      for all rows that haven't been entered with a different value. With
      regard to the sorting order, the anonymous value is always the largest
      of the column. The following functions try to treat the anonymous
      entry like a normal entry as far as possible, but a few differences
      are still observable:
       - If you add the anonymous value with [build_add], this add is ignored.
         The anonymous value is automatically present in a sparse column,
         and the zset is implicitly determined.
       - If you try to get the set with [get_zset], this function raises
         [Anonymous]. Use [anon_zset] instead in this case.
       - The anonymous entry is even present if all rows actually have a
         non-anonymous value.
   *)


  type 'a zinvcol
    (** An inverted column *)

  type 'a zinvcol_builder
    (** Bulder for an inverted column *)

  exception Anonymous
    (** The current element is the anonymous element in a sparse column.
        Special access rules apply.
     *)

  val create_memory_zinvcol_builder : int -> int -> Ztypes.space -> 
                                      'a Ztypes.zcoldescr -> 'a zinvcol_builder
    (** [create_memory_zinvcol_builder inv_size idt_size space coltype] *)
  
  val create_file_zinvcol_builder : string -> int -> int -> Ztypes.space -> 
                                    'a Ztypes.zcoldescr -> 'a zinvcol_builder
    (** [create_file_zinvcol_builder dir inv_size idt_size space coltype] *)

  val build_add : 'a zinvcol_builder -> 'a -> Zset.zset -> unit
    (** Add a value and the set. The value must be larger than any previously
        added value. The set indicates the rows by ID where the column has
        this value.

        Note that it is not allowed to include the same ID in several added
        sets (but at present this is not checked).
     *)

  val build_end : 'a zinvcol_builder -> 'a zinvcol
    (** Finish the build and return the created column.

        Note that it is not allowed to skip over IDs (but at present this
        is not checked).
     *)

  val build_abort : _ zinvcol_builder -> unit
    (** Abort the build and delete any files *)

  val from_file : string -> Ztypes.space -> 'a Ztypes.zcoldescr -> 'a zinvcol
    (** Load a column from a directory *)

  val remove : ?force:bool -> string -> string -> unit
    (** [remove dir col_name] *)

  (* TODO: to_file : 'a zinvcol -> string -> unit *)

  val length : 'a zinvcol -> int
    (** The length is the number of (value,set) pairs. In a sparse column
        the last pair is the anonymous element.
     *)

  val get_hash : 'a zinvcol -> int -> int
    (** [get_hash col k]: Get the zorder hash for the [k]-th entry,
        [0 <= k < length].
     *)

  val get_hash_and_value : 'a zinvcol -> int -> int * 'a
    (** [get_hash_and_value col k]: Get the zorder hash and the decoded value
        for the [k]-th entry,
        [0 <= k < length].
     *)

  val get_value : 'a zinvcol -> int -> 'a
    (** [get_value col k]: Get the decoded value for the [k]-th entry,
        [0 <= k < length].
     *)

  val get_zset : 'a zinvcol -> int -> Zset.zset
    (** [get_zset col k]: Get the zset for the [k]-th entry,
        [0 <= k < length].

        This function may raise [Anonymous] when the anonymous element of
        a sparse column is accessed. The zset would have to be computed
        as for [anon_zset].
     *)

  val is_anon : 'a zinvcol -> int -> bool
     (** Whether this entry is the anonymous element of a sparse column *)

  val anon_zset : 'a zinvcol -> Zset.zset -> Zset.pool -> Zset.zset
    (** [anon_zset col base pool]: For a sparse column this function
        returns the subset of [base] where the column has the anonymous
        value.
     *)

  val find : ?hash:int -> 'a zinvcol -> Ztypes.comparison -> 'a -> int
    (** [find col comp v]: Get the index [k] of the entry for the value
        [v] and the comparison [comp]:

        - [comp=EQ]: return the exact match only
        - [comp=LT]: return the biggest [k] where the value is less than [v]
        - [comp=LE]: return the biggest [k] where the value is less or
          equal than [v]
        - [comp=GT]: return the smallest [k] where the value is greater than [v]
        - [comp=GE]: return the smallest [k] where the value is greater or
          equal than [v]
        - [comp=NE]: return any [k] when there is no exact match

       If the match cannot be found raises [Not_found].
     *)

  val index_of_id : _ zinvcol -> int -> int
    (** [index_of_id col id]: return the index [k] where the column contains the
        [id].
     *)

  val value_of_id : 'a zinvcol -> int -> 'a
    (** [value_of_id col id]: return the value of the entry where the column
        contains the [id].

        For sparse columns (SIM), [value_of_id] returns [None] for any
        unknown ID, assuming that it was from an ignored [build_add].
     *)

  val value_array_of_zset : 'a zinvcol -> Zset.zset -> 'a array
    (** Get the values for the IDs in the set. The values are obtained in
        ascending ID order and put densely into the array, which has
        [Zset.count zset] elements.
     *)

  val dump : 'a zinvcol -> ('a * int array) list
    (** Get the contents as list *)

  val dump2 : 'a zinvcol -> ('a * string) list
    (** Get the contents as list. The set is converted to a string *)

  val dump3 : (Zset.zset -> 'b) ->  'a zinvcol -> ('a * 'b) list
    (** Get the contents as list. The set is converted with the function *)

  val merge : 'a zinvcol_builder -> 'a zinvcol array -> unit
    (** [merge b cols]: Merge the columns [cols] into the builder [b].
        The builder must still be empty (nothing added to it or merged into
        it).

        The rows of the first column have the same IDs in the output.
        The rows of the following columns have IDs that are shifted by
        an offset, which is the sum of the spaces of all previous columns.

        The space of the builder must be large enough so that all input
        spaces fit in.
     *)

  (** Iterates over the column by value *)
  class type ['a] ziterator =
    object
      method current_pos : int
        (** Get the current position of the cursor from 0 to [length-1] *)

      method current_value : int * 'a
        (** Get the zorder hash and the value *)

      method current_value_lz : (int * 'a) Lazy.t
        (** Get the zorder hash and the value lazily (from the current
            position)
         *)

      method current_zset : Zset.pool -> Zset.zset
        (** Get the IDs *)

      method current_zset_lz : Zset.pool -> Zset.zset Lazy.t
        (** Get the IDs lazily (from the current position) *)

      method beyond : bool
        (** If [beyond] the current position does not point to an entry,
            and is either before the first or after the last entry
         *)

      method next : unit -> unit
        (** Go to the next entry *)

      method prev : unit -> unit
        (** Go to the previous entry *)

      method go_beginning : unit -> unit
        (** Go to the first entry *)

      method go_end : unit -> unit
        (** Go to the last entry *)

      method go : int -> unit
        (** Go to this position *)

      method find : Ztypes.comparison -> 'a -> unit
        (** [find comp v]: Go to the entry for the value
            [v] and the comparison [comp]:

        - [comp=EQ]: go to the exact match only
        - [comp=LT]: go to the last entry where the value is less than [v]
        - [comp=LE]: go to the last entry where the value is less or
          equal than [v]
        - [comp=GT]: go to the first entry where the value is greater than [v]
        - [comp=GE]: go to the first entry where the value is greater or
          equal than [v]
        - [comp=NE]: go to any entry when there is no exact match

          If the entry cannot be found raise [Not_found].
         *)

      method length : int
        (** The number of iterable positions *)
    end

  val iter_all : 'a zinvcol -> 'a ziterator
    (** Iterate over all values *)

  val iter : ?all_values:bool -> 'a zinvcol -> Zset.zset -> 'a ziterator
    (** Iterate over the values that have an intersection with the set.
        During the iteration, [current_zset] returns the intersection.

        If [all_values] is set (default: not set), the iteration also includes
        the values that do not have an intersection with the set (and
        [current_zset] is empty for these).
     *)

  val iter_union : ?all_values:bool -> 'a zinvcol -> Zset.zset list ->
                   'a ziterator
    (** [iter_union col sets]:
        Iterate over the values that have an intersection with any
        of the passed [sets].
        During the iteration, [current_zset] returns the intersection
        of the column set with the union of the [sets].

        This is the same
        as [iter col (Zset.union_many poool sets)] but faster when only
        a few values of the column are actually iterated over. It is, however,
        a lot slower if the column is completely iterated.
     *)

  val iter_mem_all : 'a list zinvcol -> 'a ziterator
    (** Only for list type: Iterates over the values that actually occur as
        elements of lists somewhere,
        i.e. this function can be used for searching the contents
        of lists. The iterator behaves different in one point: rows may be
        returned several times by the iterator (when the row has several
        list elements), and a row may also be not returned at all (when the
        row has the empty list).

        For example, when there are three rows in a list column like:
         - row 1: [["x"; "f"; "g"]]
         - row 2: [["y"; "f"; "h"]]
         - row 3: [["x"; "i"]]

        The actually occuring element values are then
        [["f";"g";"h";"i";"x";"y"]], and the iterator would run over this
        list. For each of these values the rows are returned where the
        value occurs, e.g. for "x" the set would be [{1,3}].

     *)

  val build_from_iterator : 'a zinvcol_builder -> Zset.pool -> 'a ziterator ->
                            'a zinvcol
    (** Iterate over the entries and add these to the builder *)

  val space : _ zinvcol -> Ztypes.space
    (** Get the space *)

  val descr : 'a zinvcol -> 'a Ztypes.zcoldescr
    (** Return the descriptor *)
                             
  val storage_size : _ zinvcol -> int
    (** number of bytes on disk *)

  val select : (int * 'a) list zinvcol -> int -> 'a zinvcol
    (** Select a bundle element *)

  val indices : (int * _) list zinvcol -> int list
    (** return the bundle indices *)

end


module Zdircol : sig
  (** {2 Direct columns} *)

  type 'a zdircol
    (** a direct column *)

  type 'a zdircol_builder
    (** Builder for a direct column *)

  val create_memory_zdircol_builder : int -> int -> Ztypes.space -> 
                                      'a Ztypes.zcoldescr -> 'a zdircol_builder
    (** [create_memory_zdircol_builder f64_size char_size space coltype] *)
  
  val create_file_zdircol_builder : string -> int -> int -> Ztypes.space -> 
                                    'a Ztypes.zcoldescr -> 'a zdircol_builder
    (** [create_file_zdircol_builder dir f64_size char_size space coltype] *)

  val build_add : 'a zdircol_builder -> 'a -> int
    (** [build_add b value]: Allocates a new ID for this value, stores the
        value for this ID, and returns the ID *)

  val build_end : 'a zdircol_builder -> 'a zdircol
    (** Finishes the build and returns the column *)

  val build_abort : _ zdircol_builder -> unit
    (** Aborts the build (deletes files) *)

  val from_file : string -> Ztypes.space -> 'a Ztypes.zcoldescr -> 'a zdircol
    (** Load a column from this directory *)

  val from_vector : string -> Ztypes.direction ->
                    ('a,'b,Bigarray.fortran_layout) Bigarray.Array1.t ->
                    'a zdircol
    (** [from_vector name dir vec]: Create a column from a bigarray [vec].
        The bigarray is directly used (no copy is made). The column is
        called [name] and has the ordering direction [dir].

        The function is limited to bigarrays of types that have a
        corresponding type in {!Ztypes.ztype}. At the moment, this exludes
        bigarrays of [int32], of [nativeint], and of complex numbers.
     *)

  val remove : ?force:bool -> string -> string -> unit
    (** [remove dir col_name] *)

  (* TODO: to_file : 'a zdircol -> string -> unit *)

  val length : 'a zdircol -> int
    (** The length is the number of really allocated IDs *)

  val value_of_id : 'a zdircol -> int -> 'a
    (** Returns the value of this ID. Raises [Not_found] if the ID is inside
        the space but no value has been added. If the ID is outside the space
        the function fails.
     *)

  val value_array_of_zset : 'a zdircol -> Zset.zset -> 'a array
    (** Get the values for the IDs in the set. The values are obtained in
        ascending ID order and put densely into the array, which has
        [Zset.count zset] elements.
     *)

  val iter_set : 'a zdircol -> (int -> 'a -> unit) -> Zset.zset -> unit
    (** [iter_set col f set]: iterates over the IDs in [set], and
        calls [f id x] where [x = value_of_id col id]
     *)

  (** Iterates over the column by value (inversion on the fly) *)
  class type ['a] ziterator = ['a] Zinvcol.ziterator

  val merge : 'a zdircol_builder -> 'a zdircol array -> unit
    (** [merge b cols]: Merge the columns [cols] into the builder [b].
        The builder must still be empty (nothing added to it or merged into
        it).

        The rows of the first column have the same IDs in the output.
        The rows of the following columns have IDs that are shifted by
        an offset, which is the sum of the lengths of all previous columns.

        The space of the builder must be large enough so that all input
        lengths fit in.
     *)
    (* CHECK: better use space instead of length for compatibility *)


  val iter_all : 'a zdircol -> 'a ziterator
    (** Inverts the column and accesses it by a value iterator *)

  val iter : 'a zdircol -> Zset.zset -> 'a ziterator
    (** Inverts the parts of the column selected by the zset, and accesses
        that by a value iterator *)

  val iter_union : 'a zdircol -> Zset.zset list -> 'a ziterator
    (** [iter_union col sets] does the same as
        [iter col (Zset.union_many pool sets)]
     *)


  (** Helper for converting direct data to inverted format *)
  class type ['a] accumulator =
    object
      method add : int -> 'a -> unit
        (** Add a new row with ID and value. The IDs need to be added in
            ascending order
         *)
      method finish : unit -> 'a ziterator
        (** Return an iterator allowing inverted access *)
    end

  val accumulator : 'a Ztypes.zcoldescr -> Ztypes.space -> 'a accumulator
    (** Accumulate data and make it accessible with an iterator *)

  val storage_size : _ zdircol -> int
    (** number of bytes on disk *)

  val space : _ zdircol -> Ztypes.space
    (** Get the space *)

  val descr : 'a zdircol -> 'a Ztypes.zcoldescr
    (** Return the descriptor *)
                             
end


module Zcol : sig
  (** {2 Generic read-access to columns} *)

  type 'a zcol
    (** A column *)

  type 'a zcol_inverted
    (** A column with inverted access *)

  type 'a zcontent =
    | Inv of 'a Zinvcol.zinvcol
    | Dir of 'a Zdircol.zdircol

  (** Iterates over the column by value *)
  class type ['a] ziterator = ['a] Zinvcol.ziterator

  val from_file : string -> Ztypes.space -> 'a Ztypes.zcoldescr -> 'a zcol
    (** [from_file directory space descr]: Loads the column from the
        directory *)

  val value_of_id : 'a zcol -> int -> 'a
    (** Get the value for an ID. For looking up many values of the same
        column it is advantageous to pre-evaluate the first argument and
        use the (currified) rest for looking up by ID, e.g.
        {[
let lookup = Zcol.value_of_id col
iterate (fun id -> let v = lookup id in ...)
        ]}
     *)

  val value_array_of_zset : 'a zcol -> Zset.zset -> 'a array
    (** Get the values for the IDs in the set. The values are obtained in
        ascending ID order and put densely into the array, which has
        [Zset.count zset] elements.
     *)

  val space : _ zcol -> Ztypes.space
    (** Return the space *)

  val descr : 'a zcol -> 'a Ztypes.zcoldescr
    (** Return the descriptor *)

  val content : 'a zcol -> 'a zcontent
    (** Get the underlying inverted or direct column *)

  val invert : 'a zcol -> Zset.zset -> 'a zcol_inverted
    (** Instruct to invert the column after intersecting it with the set *)

  val invert_union : 'a zcol -> Zset.zset list -> 'a zcol_inverted
    (** Invert the column after intersecting with the union of the passed
        sets (same as [invert col (Zset.union_many pool sets)]).
     *)

  val rows_with_value : Zset.pool -> 'a zcol_inverted -> 'a -> Zset.zset
    (** Get the set of IDs where the column has the passed value *)

  (* TODO:
      rows_with_value_in_interval : Zset.pool -> 'a zcol_inverted ->
                                     'a interval -> Zset.zset
   *)

  val iter_values : 'a zcol_inverted -> 'a ziterator
    (** Iterate over the inverted column by value *)

  val bundle_element : (int * 'a) list zcol -> int -> 'a zcol
    (** [bundle_element col k]: Access the k-th element from the
        bundle column [col]. The default value is [default] for rows
        where the element does not define a different value.
     *)

  val bundle_indices : (int * _) list zcol -> int list
    (** return the bundle indices *)
end


module Ztable : sig
  (** {2 Tables} *)

  type ztable
    (** A table *)

  val create_memory_ztable : ?over:ztable ->
                             Ztypes.stdrepr -> Ztypes.space -> ztable
    (** Create an in-memory table for a certain representation and a space.

        If [over] is passed, this table overlays an already existing table.
        The already existing table is not modified when columns are added
        to or removed from the overlay table. The columns in the overlay
        table hide columns with the same name in the original table. It is
        also possible to mark a column as removed in the overlay table
        while it still exists in the orignal table. It is required, though,
        that the [space] of the overlay table is the same as for the orignal
        table.

        It is undefined whether columns become visible in the overlay table
        when they are added in the original table after adding the overlay.
     *)

  val create_file_ztable : ?over:ztable ->
                           Ztypes.stdrepr -> Ztypes.space -> string -> ztable
    (** Create a file table in this directory *)

  val from_file : ?over:ztable -> Ztypes.stdrepr -> string -> ztable
    (** Load a table from this directory *)

  val path_name : ztable -> string option
    (** Get the path name or [None] if this is a memory-only ztable. In case
        of a ztable with overlays, only the path name of the topmost table
        is returned.
     *)

  val underlying_ztable : ztable -> ztable option
  (** Get the underlying ztable (the table from the [over] parameter) *)
                                                                
  val add_vector : ztable -> string -> Ztypes.direction ->
                    ('a,'b,Bigarray.fortran_layout) Bigarray.Array1.t ->
                    'a Zcol.zcol
    (** [add_vector ztab name dir vec]: Create a column from a bigarray [vec].
        The bigarray is directly used (no copy is made). The column is
        called [name] and has the ordering direction [dir].

        The function is limited to bigarrays of types that have a
        corresponding type in {!Ztypes.ztype}. At the moment, this exludes
        bigarrays of [int32], of [nativeint], and of complex numbers.
     *)

  val build_zinvcol : ztable -> string -> 'a Ztypes.ztype -> string -> string ->
                      'a Zinvcol.zinvcol_builder
    (** [build_zinvcol table col_name ty repr_name order_name]: Build a
        new inverted column
     *)

  val build_zdircol : ztable -> string -> 'a Ztypes.ztype -> string -> string ->
                      'a Zdircol.zdircol_builder
    (** [build_zdircol table col_name ty repr_name order_name]: Build a
        new direct column
     *)

  val build_any_zinvcol : ztable -> string -> 'a Ztypes.ztype -> 
                          'a Ztypes.zrepr -> 'a Ztypes.zorder -> 
                          'a Zinvcol.zinvcol_builder
    (** Build a new inverted column by passing the definitions of
        representations and zorders directly (instead by passing their names).
        This is limited to in-memory tables.
     *)

  val build_any_zdircol : ztable -> string -> 'a Ztypes.ztype -> 
                          'a Ztypes.zrepr -> 'a Ztypes.zorder ->
                          'a Zdircol.zdircol_builder
    (** Build a new direct column by passing the definitions of
        representations and zorders directly (instead by passing their names).
        This is limited to in-memory tables.
     *)

  val space : ztable -> Ztypes.space
    (** Get the space *)

  val columns : ztable -> string list
    (** Get the columns (in no particular order) *)

  val get_zcolbox : ztable -> string -> Ztypes.zcolbox
    (** Get the descriptor of a column by name, boxed. If the column is a
        bundle, the name can also have the form ["bundle[element]"] where
        [element] is an integer selecting the element from the bundle.
     *)

  val get_zcol : ztable -> 'a Ztypes.ztype -> string -> 'a Zcol.zcol
    (** Get the column. Note that you need to pass in the type for typing
        reasons. If the type is wrong, the function fails. You can get
        the type from a [get_zcolbox] call.

        If the column is a
        bundle, the name can also have the form ["bundle[element]"] where
        [element] is an integer selecting the element from the bundle.
        Note, however, that you cannot set the default value with this
        method. The default value is always {!Zcontainer.Ztypes.null_value}.
        Use [bundle_zcol] to specify a different default.
     *)

  val bundle_zcolbox : ztable -> string -> int -> Ztypes.zcolbox
     (** [bundle_zcolbox tab name k]: Get the descriptor of a bundle
         element. The [name] references the bundle. The integer [k]
         selects the element.
      *)

  val bundle_zcol : ztable -> 'a Ztypes.ztype -> string -> int ->
                    'a Zcol.zcol
     (** [bundle_zcol tab ty name k]: Get the bundle element [k]
         for the bundle column [name]. The type [ty] specifies the type of
         the element. The [default] is a value to substitute for any row
         that hasn't got a value in this element.
      *)

  val contains_zcol : _ Zcol.zcol -> ztable -> bool
    (** Whether this column is member of the table *)

  val contains_zcol_name : string -> ztable -> bool
    (** Whether a column of this name is member of the table *)

  val merge : ztable -> (string * Ztypes.ztypebox * string * string) list ->
              ztable array ->
                unit
    (** [merge out_table cols in_tables]: Merges the columns [cols] of the
        [in_tables] into the [out_table].

        The columns must not yet exist in [out_table].
     *)

  val remove : ?force:bool -> ztable -> unit
    (** Remove the directory containing a table *)

  val remove_column : ?force:bool -> ztable -> string -> unit
    (** Remove the column *)

  val decompose_bundle_name : string -> (string * int) option
    (** For a name "base[k]" (where k is an integer) this function returns
        [Some("base",k)]. If the name is not of this form, [None] is returned
     *)
end


module Zdigester : sig
  (** {2 Create a table from a sequence of rows}

      The digestion is done in several rounds. In a single round (or "stage")
      the data is kept in memory. Finally, the stages are merged into the
      output table.

      The digester stores the stages in temporary directories which are
      created side-by-side with the final table.
   *)

  type 'a parserfun =
      string -> 'a
    (** Parses a value *)

    (** A boxed version of the parser *)
  type parserbox =
    | ParserBox : 'a Ztypes.ztype * 'a parserfun -> parserbox
    | NoParserBox : 'a Ztypes.ztype -> parserbox

    (** Something to digest. The string in a [StringBox] still needs to be
        parsed, and there must be a [ParserBox] for the column. The
        [ValueBox] can be any value (together with its type).
     *)
  type foodbox =
    | StringBox : string -> foodbox
    | ValueBox : 'a Ztypes.ztype * 'a -> foodbox

  type zdigester
    (** The digester *)

  val create_memory_zdigester :
         ?max_stage_rows:int -> Ztypes.stdrepr ->
         (string * parserbox * string * string) array ->
         zdigester
    (** [create_memory_digester stdrepr cols]: Creates a new digester
        for parsing [cols]. Every column is given as tuple
        [(name, parser, repr_name, zorder_name)].

        Example:
        {[
let dg =
  create_memory_zdigester
    [| ( "col1", ParserBox(Ztypes.int64, Int64.of_string), "IM/int64",
         "int64_asc" ),
       ( "col2", ParserBox(Ztypes.float, float_of_string), "DF64/float",
         "float_asc" )
     |]
        ]}

        If you omit the parser ([NoParserBox]) you cannot use
        [build_add] but need to use [build_add_food] instead.

     *)

  val create_file_zdigester :
         ?max_stage_rows:int -> Ztypes.stdrepr ->
         (string * parserbox * string * string) array ->
         string ->
         zdigester
    (** [create_file_digester stdrepr cols dir]: Creates a new digester
        for parsing [cols]. The final table is stored in directory [dir]
        (which must not yet exist).
     *)

  val build_add : zdigester -> string array -> unit
    (** [build_add dg row]: Add another row, given as array of strings.
        For all columns there must be [ParserBox]es.
     *)

  val build_add_food : zdigester -> foodbox array -> unit
    (** [build_add_food dg row]: Add another row. The fields can be given as
        strings to be parsed ([StringBox]) or values ([ValueBox]).
     *)

  val build_abort : zdigester -> unit
    (** Abort the digester and delete files *)

  val build_end : zdigester -> Ztable.ztable
    (** Finish the digestion and get the created table *)

  val parser_of_type : 't Ztypes.ztype -> string -> 't parserfun
    (** [parser_of_type t anon]: this is the default parser for the built-in
        types:
         - string: any string is ok
         - int64: parses with [Int64.of_string]
         - int: parses with [int_of_string]
         - float: parses with [float_of_string]
         - t option: if the parsed data is equal to [anon] then [None] is
           returned, else the parser for [t] is used and the value is wrapped
           into [Some]
         - pairs s * t: a comma is used as separator. Everything before the
           comma is parsed as [s] and everything after the comma as [t].
           In case of nested pairs, the parser is right-associative, e.g.
           "a,b,c" is parsed as "a,(b,c)" (if parentheses were permitted).
     *)

  (**/**)


  type stage
  val create_stage : Ztable.ztable -> (string * parserbox * string * string) array -> stage
  val stage_add : stage -> string array -> unit
  val stage_add_food : stage -> foodbox array -> unit
  val stage_finish : stage -> unit
                                                       
end


module Zdiscretize : sig
  (** Basic discretization *)

  val equal_width_histogram : 
        ztable:Ztable.ztable ->
        name:string ->
        offset:float ->
        width:float ->
        float Zcol.zcol -> (float*float) Zcol.zcol
    (** [equal_width_histogram ... incol]: Creates a histogram from the input
        column [incol] where the width of every bucket is the same, [width].
        The histogram is added to [ztable] as a name column [name]. This
        column is of type inverted multibyte. Every output category is identified
        by a pair [(from,to)] covering the half-open interval [from <= x < to].
     *)

  val logarithmic_histogram : 
        ztable:Ztable.ztable ->
        name:string ->
        offset:float ->
        factor:int ->
        float Zcol.zcol -> (float*float) Zcol.zcol
  (** Computes a histogram where the width of the buckets grows with
      the values by a multiplicative factor. In particular, we
      ensure that the interval [x+offset .. 10 * (x+offset)]
      generates [factor] buckets.  Values less or equal than [-offset] are put
      into a single bucket [(neg_infinity,-offset)].
   *)

  val maxdiff_histogram :
        ?f_diff:(float -> float) ->
        ztable:Ztable.ztable ->
        name:string ->
        number:int ->
        float Zcol.zcol -> (float*float) Zcol.zcol
    (** [maxdiff_histogram ... incol]: Creates a histogram from the input
        column [incol] so that [number] buckets are created, and the bucket
        boundaries are put between the pairs with the largest differences.
        The histogram is added to [ztable] as a name column [name]. This
        column is of type inverted multibyte. Every output category is identified
        by a pair [(from,to)] covering the half-open interval [from <= x < to].

        Infinite numbers are handled reasonably. NaNs cause funny results,
        though.

        @param f_diff If passed, this function is applied to the differences
        before the [number] biggest differences are determined. E.g. set
        [f_diff = log].
     *)
(*
  val k_means_clustering_histogram :
        ztable:Ztable.ztable ->
        name:string ->
        number:int ->
        rnd:Random.State.t ->
        float Zcol.zcol -> (float*float) Zcol.zcol
   (** [k_means_clustering_histogram ... incol]: Creates a histogram from the
        input column [incol] by clustering the data with the k-means method.
        The histogram is added to [ztable] as a name column [name]. This
        column is of type inverted multibyte. Every output category is identified
        by a pair [(from,to)] covering the half-open interval [from <= x < to].
    *)
*)
(*
  val k_medoids_clustering_histogram :
        ztable:Ztable.ztable ->
        name:string ->
        number:int ->
        rnd:Random.State.t ->
        float Zcol.zcol -> (float*float) Zcol.zcol
   (** [k_means_clustering_histogram ... incol]: Creates a histogram from the
        input column [incol] by clustering the data with the k-medoids method.
        The histogram is added to [ztable] as a name column [name]. This
        column is of type inverted multibyte. Every output category is identified
        by a pair [(from,to)] covering the half-open interval [from <= x < to].
    *)
*)
end

(*
module Zreduce : sig
  (** Basic data reduction for categorical data *)

  val huffman_reduce :
        ztable:Ztable.ztable ->
        name:string ->
        number:int ->
        (float*float) Zcol.zcol -> (float*float) Zcol.zcol
    (** [huffman_reduce ... incol]: Reduces the number of categories in the
        input column [incol] to [number] by successively merging adjacent
        categories with the lowest counts.

        The input and output columns are histograms, defined by pairs
        [(from,to)]. The output is added to [ztable] as new column [name].
     *)

end
 *)

module Zfilter : sig
  (** Filtering queries *)

  (** Note that we implement three-valued Kleene logic: true/false/null. Because
      of this we establish the following language:
       - A row is {b included} in the filterset when the condition evaluates
         to {b true}
       - A row is {b excluded} from the filterset (or, in other words,
         included in the negated filterset) when the condition evaluates to
         {b false}
       - Otherwise, the status of the row is {b unknown}, i.e. when the
         condition evaluates to {b null}.

      For the truth tables, see
      {{:https://en.wikipedia.org/wiki/Three-valued_logic} Three-valued logic}
   *)

  open Ztypes

  (** {2 Constructing filters} *)

  type filter
    (** A filter expression representing a set of rows fulfilling some
        condition.
     *)

  val colcmp_flt : 'a Zcol.zcol -> comparison -> 'a -> filter
    (** [{ x in column | x < value}]: Compare a column with a value.

        The comparison is done according to [ord_cmp_asc]: Rows
        are included when the value fulfills the comparison. Rows
        are excluded when the value does not fulfill the comparison.
        When the row has no value for the row (which is normally not
        possible when the table was created with the digester), the status of
        the row is unknown.
     *)

  val colcmp_some_flt : 'a option Zcol.zcol -> comparison -> 'a -> filter
    (** [{ x in column | x < value}]: Compare a column with a value.

        This is a special version for columns with optional value. If
        the column is [Some x], the value [x] is compared with the [value]
        and the row is either included or excluded. If the column is [None]
        or does not have a value at all, the status of the row is unknown.
     *)

(* TODO:
   
   val colcmp_quick_float_flt : float Zdircol.zdircol -> comparison -> float -> filter
   val colcmp_quick_int64_flt : int64 Zdircol.zdircol -> comparison -> int64 -> filter

   These assume the standard representation and the standard ordering.
   Because they avoid indirect calls, these functions are somewhat faster.

   val colcmp_with_histogram_flt : float Zcol.zcol ->
                                   (float * float) Zcol.zcol ->
                                   comparison -> float -> filter

   Assume that a histogram is available for a float column.
 *)

  val colival_flt : 'a Zcol.zcol -> ('a * 'a) list -> filter
    (** [{ x in column |
        x >= from1 && x <= to1 || x >= from2 && x <= to2 || ... }]:
        Whether the column is in an interval, expressed as a list of
        [(from,to)] values. Rows where the column has a value are
        included when the value is in the interval. If the value is
        outside the interval, the row is excluded. The status of
        rows is unknown when there is no value.
     *)

  val colival_some_flt : 'a option Zcol.zcol -> ('a * 'a) list -> filter
    (** Special version for columns with optional value. If the column
        is [Some x], it is checked whether the value is in the interval,
        and the row is either included or excluded. If the column is
        [None], or if a value is completely unavailable, the status of the
        row is unknown.
     *)

  val colexpr1_flt : 'a Zcol.zcol -> (int -> 'a -> bool) -> filter
    (** [{x in column | f(x)}]:  Whether the function evaluates to true.
        If the column has a value [x] the function [f] is called for [x],
        and a [true] result means inclusion and [false] means exclusion.
        If the column does not have a value, or if the function raises
        [Not_found], the status of the row is unknown.
     *)

  val colexpr2_flt : 'a Zcol.zcol -> 'b Zcol.zcol ->
                     (int -> 'a -> 'b -> bool) -> filter
    (** [{x in column, y in column | f(x,y)}]:
        Whether the function evaluates to true *)

  val colexpr3_flt : 'a Zcol.zcol -> 'b Zcol.zcol -> 'c Zcol.zcol ->
                     (int -> 'a -> 'b -> 'c -> bool) -> filter
    (** [{x in column, y in column, z in column | f(x,y,z)}]:
        Whether the function evaluates to true *)

  val rowexpr_flt : Ztable.ztable -> (int -> bool) -> filter
    (** [{ ID in table | f(ID) }]: whether the function evaluates to true
        for the ID
     *)

  val defined_flt : 'a Zcol.zcol -> filter
    (** The set of rows where the column is defined (has a value).
        Rows are either included or excluded.

        Note that the digester ensures that columns are defined everywhere.
        When using the builder directly, though, it is possible
        to create partly defined columns.
     *)

  val defined_some_flt : 'a option Zcol.zcol -> filter
    (** The set of rows where the column is defined (has a value) and
        is not [None]. Rows are either included or excluded.
     *)

  val and_flt : filter list -> filter
    (** AND of several filters. [and_flt[]] can be used as filter that is
        always true.
     *)

  val or_flt : filter list -> filter
    (** OR of several filters. [or_flt[]] can be used as filter that is
        always false.
     *)

  val not_flt : filter -> filter
    (** Negation. *)


  (** {2 Using filters} *)


(*
  val base_flt : filter -> filter
    (** Returns the base filter, consisting of the part that can be quickly
        precomputed.
     *)

  val row_flt : filter -> filter
    (** Returns the filter part where we have to check row by row (expensive
        filter part, it makes sense to delay the processing of this part
        as much as possible)
     *)

  val precompute : Zset.pool -> filter -> filter
    (** Precomputes part sets *)
 *)

  val zset : ?base:Zset.zset ->
             Zset.pool -> Ztable.ztable -> filter -> Zset.zset
    (** Returns the set. If [base] is passed, the set is intersected
        with [base].
     *)

(*
  val mem : int -> filter -> bool
    (** whether the ID is in the filterset *)

  type bound =
    | Lower : 'a Zcol.zcol -> 'a -> bound
    | Upper : 'a Zcol.zcol -> 'a -> bound

  val bounds : filter -> bound list
    (** The iteration bounds (only for inverted columns): If [Lower(col,x0)]
        is a bound we know that all values x >= x0 (comparison according to
        [ord_cmp]). If [Upper(col,x0)]is a bound we know that all values
        x <= x0.
     *)
 *)
end

module Zgrouping : sig
  (** Grouping queries *)
    open Ztypes

    type 'a grouping

    val group_by : 'a Zcol.zcol -> direction -> 'a grouping
      (** Group by this column and set the direction *)

    val group_some_by : 'a option Zcol.zcol -> direction -> 'a grouping
      (** Special version for optional columns: only [Some] values are
          returned
       *)

    val also_by : 'a Zcol.zcol -> direction -> 'b grouping -> ('a * 'b) grouping
      (** Prepend another column to the grouping *)

    val also_some_by : 'a option Zcol.zcol -> direction -> 'b grouping -> ('a * 'b) grouping
      (** Prepend another optional column to the grouping *)

    val reverse : 'a grouping -> 'a grouping
      (** Reverse the direction *)

    type 'a group

    val stream : 
      ?filter:Zfilter.filter ->
      ?base:Zset.zset ->
      Zset.pool -> Ztable.ztable -> 'a grouping -> 'a group Stream.t
      (** Return the groups as stream. The rows given by [base] (default:
          all rows) are filtered as given by [filter] (default: do not
          filter) and then grouped as described by the [grouping].
       *)

    val list : 
      ?filter:Zfilter.filter ->
      ?base:Zset.zset ->
      ?limit:int ->
      Zset.pool -> Ztable.ztable -> 'a grouping -> 'a group list
      (** Same as list. Additionally, it is possible to [limit] the number
          of returned elements.
       *)

    val first : 
      ?filter:Zfilter.filter ->
      ?base:Zset.zset ->
      Zset.pool -> Ztable.ztable -> 'a grouping -> 'a group
      (** Return the first group *)

    val last : 
      ?filter:Zfilter.filter ->
      ?base:Zset.zset ->
      Zset.pool -> Ztable.ztable -> 'a grouping -> 'a group
      (** Return the last group *)

    val group_value : 'a group -> 'a
      (** Return the current value of the grouping column(s) *)

    val group_zset : _ group -> Zset.zset
      (** Return the set of rows of this group *)

    val innermost_cycle : _ group -> bool
      (** This flag is true when the innermost grouping has cycled around
          once (i.e. values of the innermost grouping can repeat).
       *)
end


module Zaggreg : sig
  (** Aggregations *)

  val fold : (int -> 't -> 'a -> 'a) -> 't Zcol.zcol ->
             Zset.zset -> 'a -> 'a
     (** [fold f col set acc0]: evaluates [f id x acc] for all IDs
         [id] in [set]. The value [x] is the value of the column [col] at
         the ID. The folding proceeds in an implementation-defined order.
      *)

  val fold_some  : (int -> 't -> 'a -> 'a) -> 't option Zcol.zcol ->
                   Zset.zset -> 'a -> 'a
     (** [fold_some f col set acc0]: evaluates [f id x acc] for all IDs
         [id] in [set], but excludes those IDs where the value is [None].
         The argument [x] is the [Some] value of the column [col] at
         the ID. The folding proceeds in an implementation-defined order.
      *)

  val fold_values : ('t -> 'a -> 'a) -> 't Zcol.zcol ->
             Zset.zset -> 'a -> 'a
     (** [fold_values f col set acc0]: evaluates [f x acc] for those values [x]
         of the column [col] for which the row is in [set]. The function [f]
         may be called several times for the same value [x].
         The folding proceeds in an implementation-defined order.
      *)

  val fold_distinct_values : ('t -> 'a -> 'a) -> 't Zcol.zcol ->
             Zset.zset -> 'a -> 'a
    (** Same as [fold_values] but it is guaranteed that the function [f]
        is only called once per value
     *)

  val min : 'a Zcol.zcol -> Zset.zset -> 'a
    (** Return the minimum value of the column for this zset. Raises [Not_found]
        if the zset is empty. The [ord_cmp_asc] function of the column is
        used for comparing the values.
     *)
  val min_some : 'a option Zcol.zcol -> Zset.zset -> 'a
    (** Return the minimum value of all [Some _] values of the column for this
        zset. Raises [Not_found] if the zset does not at least select one
        [Some _] value.
     *)
  val max : 'a Zcol.zcol -> Zset.zset -> 'a
    (** Same for maximum *)
  val max_some : 'a option Zcol.zcol -> Zset.zset -> 'a
    (** Same for maximum *)

  val sum : float Zcol.zcol -> Zset.zset -> float
    (** The sum of the column for all rows selected by the set *)

  val sum_some : float option Zcol.zcol -> Zset.zset -> float
      (** The sum of the [Some _] values of the column for all rows selected
          by the set *)

  val sum_map : (int -> 'a -> float) -> 'a Zcol.zcol -> Zset.zset -> float
      (** The sum of [f id x] *)

  val count : (int -> 'a -> bool) -> 'a Zcol.zcol -> Zset.zset -> int
      (** Count the rows satisfying the predicate *)

  val count_some : 'a option Zcol.zcol -> Zset.zset -> int
      (** Count the rows where the column is [Some _] *)

  val avg : float Zcol.zcol -> Zset.zset -> float
      (** Return the arithmetic mean of the column for the rows selected by
          the set *)

  val avg_some : float option Zcol.zcol -> Zset.zset -> float
      (** Return the arithmetic mean of the column for the rows selected by
          the set and where the value is [Some _] *)

  val avg_map : (int -> 'a -> float) -> 'a Zcol.zcol -> Zset.zset -> float
      (** Return the arithmetic mean of [f id x] *)

  val var : float Zcol.zcol -> Zset.zset -> float
      (** Return the arithmetic variance of the column for the rows selected by
          the set *)

  val var_some : float option Zcol.zcol -> Zset.zset -> float
      (** Return the variance of only the rows where the column has a
          [Some _] value
       *)

  val var_map : (int -> 'a -> float) -> 'a Zcol.zcol -> Zset.zset -> float
      (** Return the variance of [f id x] *)

  val nth_smallest : 'a Zcol.zcol -> Zset.zset -> int -> 'a
      (** returns the nth-smallest value after sorting the rows by the
          column. The sorting criterion is [ord_cmp_asc]. Raises [Not_found]
          if the set does not have [n] elements.

          Note that [n>=1] is required.
       *)

  val nth_smallest_map : (int -> 'a -> 'b) -> ('b -> 'b -> int) -> 
                         'a Zcol.zcol -> Zset.zset -> int -> 'b
      (** [nth_smallest_map f cmp col zset n]:
          returns the nth-smallest result of [f id x] after sorting the results
          by the passed comparison function. Raises [Not_found]
          if the set does not have [n] elements.
       *)

  val nth_smallest_and_next : 'a Zcol.zcol -> Zset.zset -> int -> 'a list
     (** returns, if possible, the n-th value [xn] and (n+1)-th value [xsucc].
         These values are returned as list [[xn;xsucc]]. If the set has only
         [n] elements, only [[xn]] is returned. If the set has fewer than
         [n] elements, the empty list is returned. This function does never
         raise [Not_found].
      *)

  val nth_smallest_and_next_map : (int -> 'a -> 'b) -> ('b -> 'b -> int) -> 
                         'a Zcol.zcol -> Zset.zset -> int -> 'b list
    (** Both variations in one function *)

  val full_median : 'a Zcol.zcol -> Zset.zset -> 'a list
    (** Returns:
         - the empty list if the set is empty
         - a list [[m]] if the set has an odd number of elements, and [m] is
           the median
         - a list [[m1;m2]] if the set has an even number of elements, and
           [m1] is the lower and [m2] is the upper median
     *)

  val full_median_map : (int -> 'a -> 'b) -> ('b -> 'b -> int) ->
                        'a Zcol.zcol -> Zset.zset -> 'b list
    (** [full_median_map f cmp col set]: Maps the elements using [f id x],
        and determines the full median using [cmp] as comparison function.
     *)

  val median : float Zcol.zcol -> Zset.zset -> float
    (** Returns the usual "single" median, taking the arithmetic mean if
        the set has an even number of elements. Raises [Not_found] if the
        set is empty.
     *)

  val median_some : float option Zcol.zcol -> Zset.zset -> float
    (** Returns the median of all [Some _] values of the column for the 
        rows selected by the set. Raises [Not_found] if no [Some _] value
        can be found.

        {b Note} that this function ignores the comparison function of the
        column and always uses the normal ordering of floats.
     *)

  val median_map : (int -> 'a -> float) -> 'a Zcol.zcol -> Zset.zset -> float
    (** Maps the elements before determining the median.

        {b Note} that this function ignores the comparison function of the
        column and always uses the normal ordering of floats.
     *)

  val full_rat_quantile : int -> int -> 'a Zcol.zcol -> Zset.zset -> 'a list
    (** [full_rat_quantile p q col set]: returns the [p/q]-th quantile. It is
        required that [p > 0], [q > 0] and [p/q < 1]. In particular, given
        [n] as the size of the set, if [n*p] is a whole multitude of [q]
        the two elements [n*p/q] and [n*p/q+1] are returned. If it is not
        a whole multitude, the single element [ceil(n*p)] is returned.
        If the set is empty the empty list is returned.

        Implementation restriction: [n*p] must be representable as [int] number.
     *)

  val full_rat_quantile_map : (int -> 'a -> 'b) -> ('b -> 'b -> int) ->
                              int -> int -> 'a Zcol.zcol -> Zset.zset -> 'b list
    (** [full_rat_quantile_map f cmp p q col set] *)

  val rat_quantile : int -> int -> float Zcol.zcol -> Zset.zset -> float
    (** [rat_quantile p q col set]: Returns the [p/q]-th quantile using the
        common definition. May raise [Not_found].
     *)

  val rat_quantile_some : int -> int -> float option Zcol.zcol -> Zset.zset ->
                          float
    (** [rat_quantile p q col set]: Returns the [p/q]-th quantile of the
        [Some _] values using the common definition. May raise [Not_found].
     *)

  val rat_quantile_map : (int -> 'a -> float) -> int -> int -> 
                         'a Zcol.zcol -> Zset.zset -> float
    (** Maps the elements before determining the median.

        {b Note} that this function ignores the comparison function of the
        column and always uses the normal ordering of floats.
     *)

  val quantile : float -> float Zcol.zcol -> Zset.zset -> float
    (** [quantile p col set]: Returns the p-th quantile. May raise [Not_found].
     *)

  val quantile_some : float -> float option Zcol.zcol -> Zset.zset -> float
    (** [quantile_some p col set]: Returns the p-th quantile of the [Some _]
        values. May raise [Not_found].
     *)

  val quantile_map : (int -> 'a -> float) ->
                     float -> 'a Zcol.zcol -> Zset.zset -> float
    (** Maps the elements before determining the median.

        {b Note} that this function ignores the comparison function of the
        column and always uses the normal ordering of floats.
     *)

end


module Zutil : sig
  (** Utilities - this is not part of the official API! *)

  module OrdUtil : functor(S:Map.OrderedType) -> sig
    val binary_search : (int -> S.t) -> int -> Ztypes.comparison -> S.t -> int
  end

  module Bitset : sig
    type bitset
    val create : int -> bool -> bitset
    val get : bitset -> int -> bool
    val fill_true : bitset -> int -> int -> unit
    val set_runlengths : bitset -> unit
    val iter : (int -> int -> unit) -> bitset -> unit
  end

end
