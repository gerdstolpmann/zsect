open Zcontainer
open Ztypes
open Zdigester
open T_frame.Util
open Printf

let get_zinvcol c =
  match Zcol.content c with
    | Zcol.Inv zinvcol -> zinvcol
    | _ -> assert false

let identity x = x

let practically_equal x y =
  compare x y = 0 ||
    x -. y < 0.000001

let same d1 d2 =
  List.length d1 = List.length d2 &&
    List.for_all2
      (fun ((x0,x1),s) ((y0,y1),t) ->
         practically_equal x0 y0 &&
           practically_equal x1 y1 &&
             s = t
      )
      d1 d2


module Maxdiff_histo = struct
    let cols =
      [| "f1", ParserBox(Zfloat, float_of_string), "DF64/float", "float_asc";
        |]

  let test_001() =
    let col_f1 =   (* ID *)
      [| 10.0;     (* 1 *)
         11.0;     (* 2 *)
         12.5;     (* 3 *)
         13.0;     (* 4 *)
         13.5;     (* 5 *)
         19.0;     (* 6 *)
         28.0;     (* 7 *)
         33.1;     (* 8 *)
         36.0;     (* 9 *)
         40.0;     (* 10 *)
         41.0;     (* 11 *)
         42.2;     (* 12 *)
         43.1      (* 13 *)
        |] in
    let dg = Zdigester.create_memory_zdigester
               ~max_stage_rows:100 Zrepr.stdrepr cols in
    Array.iter (fun f1 -> build_add dg [| string_of_float f1 |]) col_f1;
    let zt = build_end dg in
    let f1 = Ztable.get_zcol zt Zfloat "f1" in
    let h = Zdiscretize.maxdiff_histogram ~ztable:zt ~name:"h" ~number:20 f1 in
    let h_dump = Zinvcol.dump (get_zinvcol h) in
    named
      "f1"
      (same h_dump
         [ (9.5, 10.5),       [| 1 |];
           (10.5, 11.75),     [| 2 |];
           (11.75, 12.75),    [| 3 |];
           (12.75, 13.25),    [| 4 |];
           (13.25, 16.25),    [| 5 |];
           (16.25, 23.5),     [| 6 |];
           (23.5, 30.55),     [| 7 |];
           (30.55, 34.55),    [| 8 |];
           (34.55, 38.0),     [| 9 |];
           (38.0, 40.5),      [| 10 |];
           (40.5, 41.6),      [| 11 |];
           (41.6, 42.65),     [| 12 |];
           (42.65, 43.55),    [| 13 |]
         ])


  let test_002() =
    let col_f1 =   (* ID *)
      [| 10.0;     (* 1 *)
         11.0;     (* 2 *)
         12.5;     (* 3 *)
         13.0;     (* 4 *)
         13.5;     (* 5 *)
         19.0;     (* 6 *)
         28.0;     (* 7 *)
         33.1;     (* 8 *)
         36.0;     (* 9 *)
         40.0;     (* 10 *)
         41.0;     (* 11 *)
         42.2;     (* 12 *)
         43.1      (* 13 *)
        |] in
    let dg = Zdigester.create_memory_zdigester
               ~max_stage_rows:100 Zrepr.stdrepr cols in
    Array.iter (fun f1 -> build_add dg [| string_of_float f1 |]) col_f1;
    let zt = build_end dg in
    let f1 = Ztable.get_zcol zt Zfloat "f1" in
    let h = Zdiscretize.maxdiff_histogram ~ztable:zt ~name:"h" ~number:4 f1 in
    let h_dump = Zinvcol.dump (get_zinvcol h) in
    named
      "f1"
      (same h_dump
         [ (9.5, 16.25),      [| 1;2;3;4;5 |];
           (16.25, 23.5),     [| 6 |];
           (23.5, 30.55),     [| 7 |];
           (30.55, 43.55),    [| 8;9;10;11;12;13 |]
         ])


  let test_003() =
    let col_f1 =   (* ID *)
      [| 10.0;     (* 1 *)
         11.0;     (* 2 *)
         12.5;     (* 3 *)
         13.0;     (* 4 *)
         13.5;     (* 5 *)
         19.0;     (* 6 *)
         28.0;     (* 7 *)
         33.1;     (* 8 *)
         36.0;     (* 9 *)
         40.0;     (* 10 *)
         41.0;     (* 11 *)
         41.0;     (* 12 *)
         41.0;     (* 13 *)
         42.2;     (* 14 *)
         43.1      (* 15 *)
        |] in
    let dg = Zdigester.create_memory_zdigester
               ~max_stage_rows:100 Zrepr.stdrepr cols in
    Array.iter (fun f1 -> build_add dg [| string_of_float f1 |]) col_f1;
    let zt = build_end dg in
    let f1 = Ztable.get_zcol zt Zfloat "f1" in
    let h = Zdiscretize.maxdiff_histogram ~ztable:zt ~name:"h" ~number:4 f1 in
    let h_dump = Zinvcol.dump (get_zinvcol h) in
    named
      "f1"
      (same h_dump
         [ (9.5, 16.25),      [| 1;2;3;4;5 |];
           (16.25, 23.5),     [| 6 |];
           (23.5, 30.55),     [| 7 |];
           (30.55, 43.55),    [| 8;9;10;11;12;13;14;15 |]
         ])


  let test_004() =
    let col_f1 =   (* ID *)
      [| 10.0;     (* 1 *)
         11.0;     (* 2 *)
        |] in
    let dg = Zdigester.create_memory_zdigester
               ~max_stage_rows:100 Zrepr.stdrepr cols in
    Array.iter (fun f1 -> build_add dg [| string_of_float f1 |]) col_f1;
    let zt = build_end dg in
    let f1 = Ztable.get_zcol zt Zfloat "f1" in
    let h = Zdiscretize.maxdiff_histogram ~ztable:zt ~name:"h" ~number:4 f1 in
    let h_dump = Zinvcol.dump (get_zinvcol h) in
    named
      "f1"
      (same h_dump
         [ (9.5, 10.5),       [| 1 |];
           (10.5, 11.5),      [| 2 |];
         ])

  let test_005() =
    let col_f1 =   (* ID *)
      [| 10.0;     (* 1 *)
         10.0;     (* 2 *)
        |] in
    let dg = Zdigester.create_memory_zdigester
               ~max_stage_rows:100 Zrepr.stdrepr cols in
    Array.iter (fun f1 -> build_add dg [| string_of_float f1 |]) col_f1;
    let zt = build_end dg in
    let f1 = Ztable.get_zcol zt Zfloat "f1" in
    let h = Zdiscretize.maxdiff_histogram ~ztable:zt ~name:"h" ~number:4 f1 in
    let h_dump = Zinvcol.dump (get_zinvcol h) in
    named
      "f1"
      (same h_dump
         [ (9.0, 11.0),       [| 1;2 |];
         ])

  let test_006() =
    let col_f1 =   (* ID *)
      [| neg_infinity;     (* 1 *)
         10.0;             (* 2 *)
        |] in
    let dg = Zdigester.create_memory_zdigester
               ~max_stage_rows:100 Zrepr.stdrepr cols in
    Array.iter (fun f1 -> build_add dg [| string_of_float f1 |]) col_f1;
    let zt = build_end dg in
    let f1 = Ztable.get_zcol zt Zfloat "f1" in
    let h = Zdiscretize.maxdiff_histogram ~ztable:zt ~name:"h" ~number:4 f1 in
    let h_dump = Zinvcol.dump (get_zinvcol h) in
    named
      "f1"
      (same h_dump
         [ (neg_infinity, 9.0), [| 1 |];
           (9.0, 11.0),         [| 2 |];
         ])

  let test_007() =
    let col_f1 =   (* ID *)
      [| 9.0;        (* 1 *)
         infinity;   (* 2 *)
        |] in
    let dg = Zdigester.create_memory_zdigester
               ~max_stage_rows:100 Zrepr.stdrepr cols in
    Array.iter (fun f1 -> build_add dg [| string_of_float f1 |]) col_f1;
    let zt = build_end dg in
    let f1 = Ztable.get_zcol zt Zfloat "f1" in
    let h = Zdiscretize.maxdiff_histogram ~ztable:zt ~name:"h" ~number:4 f1 in
    let h_dump = Zinvcol.dump (get_zinvcol h) in
    named
      "f1"
      (same h_dump
         [ (8.0, 10.0),             [| 1 |];
          (10.0, infinity),         [| 2 |];
         ])

  let test_008() =
    let col_f1 =   (* ID *)
      [| infinity;   (* 1 *)
        |] in
    let dg = Zdigester.create_memory_zdigester
               ~max_stage_rows:100 Zrepr.stdrepr cols in
    Array.iter (fun f1 -> build_add dg [| string_of_float f1 |]) col_f1;
    let zt = build_end dg in
    let f1 = Ztable.get_zcol zt Zfloat "f1" in
    let h = Zdiscretize.maxdiff_histogram ~ztable:zt ~name:"h" ~number:4 f1 in
    let h_dump = Zinvcol.dump (get_zinvcol h) in
    named
      "f1"
      (same h_dump
         [ (neg_infinity, infinity),  [| 1 |];
         ])

  let test_009() =
    (* two numbers that are so close that there cannot be a third number
       in between
     *)
    let col_f1 =   (* ID *)
      [| 1.0;                    (* 1 *)
         1.0 +. epsilon_float;   (* 2 *)
        |] in
    let dg = Zdigester.create_memory_zdigester
               ~max_stage_rows:100 Zrepr.stdrepr cols in
    Array.iter (fun f1 -> build_add dg [| sprintf "%.20f" f1 |]) col_f1;
    let zt = build_end dg in
    let f1 = Ztable.get_zcol zt Zfloat "f1" in
    let h = Zdiscretize.maxdiff_histogram ~ztable:zt ~name:"h" ~number:4 f1 in
    let h_dump = Zinvcol.dump (get_zinvcol h) in
    named
      "f1"
      (same h_dump
         [ (0.0, 2.0), [| 1;2 |]
         ])

end


let tests =
  [ ("Maxdiff_histo.test_001", Maxdiff_histo.test_001);
    ("Maxdiff_histo.test_002", Maxdiff_histo.test_002);
    ("Maxdiff_histo.test_003", Maxdiff_histo.test_003);
    ("Maxdiff_histo.test_004", Maxdiff_histo.test_004);
    ("Maxdiff_histo.test_005", Maxdiff_histo.test_005);
    ("Maxdiff_histo.test_006", Maxdiff_histo.test_006);
    ("Maxdiff_histo.test_007", Maxdiff_histo.test_007);
    ("Maxdiff_histo.test_008", Maxdiff_histo.test_008);
    ("Maxdiff_histo.test_009", Maxdiff_histo.test_009);
  ]

           
      
