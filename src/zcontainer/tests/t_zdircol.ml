open Zcontainer
open Ztypes
open Zdircol
open T_frame.Util

module Build = struct
  let sp = Ztypes.create_space 999

  let col1 = ZcolDescr("col1",Zint64,Dir_fixed64 Zrepr.int64_f64,
                       Zrepr.int64_asc)
  let col2 = ZcolDescr("col2",Zint64,Dir_multibyte Zrepr.int64_mb,
                       Zrepr.int64_asc)
  let col3 = ZcolDescr("col3",Zfloat,Dir_fixed64 Zrepr.float_f64,
                       Zrepr.float_asc)

  let test_001() =
    (* empty column *)
    let b = create_memory_zdircol_builder 100 100 sp col1 in
    let c = build_end b in
    length c = 0

  let test_010() =
    (* col1 *)
    let b = create_memory_zdircol_builder 100 100 sp col1 in
    let id1 = build_add b 0x456L in
    let id2 = build_add b 0x123L in
    let id3 = build_add b (-0x456L) in
    let id4 = build_add b Int64.max_int in
    let c = build_end b in
    named "length" (length c = 4) &&&
      named "id1" (id1 = 1) &&&
      named "v1" (value_of_id c 1 = 0x456L) &&&
      named "id2" (id2 = 2) &&&
      named "v2" (value_of_id c 2 = 0x123L) &&&
      named "id3" (id3 = 3) &&&
      named "v3" (value_of_id c 3 = (-0x456L)) &&&
      named "id4" (id4 = 4) &&&
      named "v4" (value_of_id c 4 = Int64.max_int)


  let test_020() =
    (* col2 *)
    let b = create_memory_zdircol_builder 100 100 sp col2 in
    let id1 = build_add b 0x456L in
    let id2 = build_add b 0x123L in
    let id3 = build_add b (-0x456L) in
    let id4 = build_add b Int64.max_int in
    let c = build_end b in
    named "length" (length c = 4) &&&
      named "id1" (id1 = 1) &&&
      named "v1" (value_of_id c 1 = 0x456L) &&&
      named "id2" (id2 = 2) &&&
      named "v2" (value_of_id c 2 = 0x123L) &&&
      named "id3" (id3 = 3) &&&
      named "v3" (value_of_id c 3 = (-0x456L)) &&&
      named "id4" (id4 = 4) &&&
      named "v4" (value_of_id c 4 = Int64.max_int)

  let test_030() =
    (* col3 *)
    let b = create_memory_zdircol_builder 100 100 sp col3 in
    let id1 = build_add b 0.0 in
    let id2 = build_add b (-0.0) in
    let id3 = build_add b 3.14 in
    let id4 = build_add b (-3.14) in
    let id5 = build_add b infinity in
    let id6 = build_add b nan in
    let c = build_end b in
    named "length" (length c = 6) &&&
      named "id1" (id1 = 1) &&&
      named "v1" (value_of_id c 1 = 0.0) &&&
      named "id2" (id2 = 2) &&&
      named "v2" (value_of_id c 2 = (-0.0)) &&&
      named "id3" (id3 = 3) &&&
      named "v3" (value_of_id c 3 = 3.14) &&&
      named "id4" (id4 = 4) &&&
      named "v4" (value_of_id c 4 = (-3.14)) &&&
      named "id5" (id5 = 5) &&&
      named "v5" (compare (value_of_id c 5) infinity = 0) &&&
      named "id6" (id6 = 6) &&&
      named "v6" (compare (value_of_id c 6) nan = 0)
end


module Merge = struct
  let sp = Ztypes.create_space 999

  let col1 = ZcolDescr("col1",Zint64,Dir_fixed64 Zrepr.int64_f64,
                       Zrepr.int64_asc)
  let col2 = ZcolDescr("col2",Zint64,Dir_multibyte Zrepr.int64_mb,
                       Zrepr.int64_asc)

  let test_001() =
    let b1 = create_memory_zdircol_builder 100 100 sp col1 in
    let _ = build_add b1 0x456L in
    let _ = build_add b1 0x123L in
    let _ = build_add b1 (-0x456L) in
    let _ = build_add b1 Int64.max_int in
    let c1 = build_end b1 in
    let b2 = create_memory_zdircol_builder 100 100 sp col1 in
    let _ = build_add b2 0x763L in
    let _ = build_add b2 0x273L in
    let c2 = build_end b2 in
    let b3 = create_memory_zdircol_builder 100 100 sp col1 in
    let _ = build_add b3 (-0x10L) in
    let _ = build_add b3 Int64.min_int in
    let c3 = build_end b3 in
    let b = create_memory_zdircol_builder 100 100 sp col1 in
    merge b [| c1; c2; c3 |];
    let c = build_end b in
    named "length" (length c = 8) &&&
      named "v1" (value_of_id c 1 = 0x456L) &&&
      named "v2" (value_of_id c 2 = 0x123L) &&&
      named "v3" (value_of_id c 3 = (-0x456L)) &&&
      named "v4" (value_of_id c 4 = Int64.max_int) &&&
      named "v5" (value_of_id c 5 = 0x763L) &&&
      named "v6" (value_of_id c 6 = 0x273L) &&&
      named "v7" (value_of_id c 7 = (-0x10L)) &&&
      named "v8" (value_of_id c 8 = Int64.min_int)

  let test_002() =
    (* same with col2 (multibyte) *)
    let b1 = create_memory_zdircol_builder 100 100 sp col2 in
    let _ = build_add b1 0x456L in
    let _ = build_add b1 0x123L in
    let _ = build_add b1 (-0x456L) in
    let _ = build_add b1 Int64.max_int in
    let c1 = build_end b1 in
    let b2 = create_memory_zdircol_builder 100 100 sp col2 in
    let _ = build_add b2 0x763L in
    let _ = build_add b2 0x273L in
    let c2 = build_end b2 in
    let b3 = create_memory_zdircol_builder 100 100 sp col2 in
    let _ = build_add b3 (-0x10L) in
    let _ = build_add b3 Int64.min_int in
    let c3 = build_end b3 in
    let b = create_memory_zdircol_builder 100 100 sp col2 in
    merge b [| c1; c2; c3 |];
    let c = build_end b in
    named "length" (length c = 8) &&&
      named "v1" (value_of_id c 1 = 0x456L) &&&
      named "v2" (value_of_id c 2 = 0x123L) &&&
      named "v3" (value_of_id c 3 = (-0x456L)) &&&
      named "v4" (value_of_id c 4 = Int64.max_int) &&&
      named "v5" (value_of_id c 5 = 0x763L) &&&
      named "v6" (value_of_id c 6 = 0x273L) &&&
      named "v7" (value_of_id c 7 = (-0x10L)) &&&
      named "v8" (value_of_id c 8 = Int64.min_int)
end

let tests =
  [ ("Build.test_001", Build.test_001);
    ("Build.test_010", Build.test_010);
    ("Build.test_020", Build.test_020);
    ("Build.test_030", Build.test_030);
    
    ("Merge.test_001", Merge.test_001);
    ("Merge.test_002", Merge.test_002);
  ]
