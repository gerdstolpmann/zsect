open Printf
open T_frame.Util
open Zcontainer
open Ztypes

let atos a =
  sprintf "[%s]" (String.concat "," (List.map string_of_int (Array.to_list a)))


let shape_array a =
  let size = Array.length a in
  Array.sort Pervasives.compare a;
  (* remove dups in a... *)
  let k = ref 1 in
  for i = 1 to size-1 do
    if a.(i) <> a.( !k - 1 ) then (
      a.( !k ) <- a.(i);
      incr k
    )
  done;
  let a = Array.sub a 0 !k in
  a

let create_array seed size n =
  let rng = Random.State.make seed in
  let a = Array.make size 0 in
  let k = ref 0 in
  while !k < size do
    let b = Random.State.int rng n + 1 in
    let r = Random.State.int rng 3 in
    for j = 0 to r - 1 do
      if !k + j < size then
        a.( !k + j ) <- min (b + j) n
    done;
    k := !k + r
  done;
  shape_array a

module Encoding = struct

  let sp999 = create_space 999
  let sp999_999 = create_space 999_999
  let sp999_999_999  = create_space 999_999_999

  (* ranges *)

  let test_001 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 11; 12 |] in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_002 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 11; 12;
               20; 21; 22;
            |] in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_003 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 11; 12;
               20; 21; 22;
               30; 31; 32;
            |] in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_004 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 11; 12;
               20; 21; 22;
               30; 31; 32;
               40; 41; 42;
            |] in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_005 () =
    let p = Zset.create_pool 1024 8 in
    let a = Array.init (20*3) (fun k -> 10 * (k/3) + (k mod 3) + 1) in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_006 () =
    let p = Zset.create_pool 1024 8 in
    let a = Array.init (20*3) (fun k -> 1000 * (k/3) + (k mod 3) + 1) in
    Zset.to_array (Zset.from_array p sp999_999_999 a) = a

  let test_007 () =
    let p = Zset.create_pool 1024 8 in
    let a = Array.init (20*3) (fun k -> 10000 * (k/3) + (k mod 3) + 1) in
    Zset.to_array (Zset.from_array p sp999_999_999 a) = a

  let test_008 () =
    let p = Zset.create_pool 1024 8 in
    let a = Array.init (20*3) (fun k -> 100000 * (k/3) + (k mod 3) + 1) in
    Zset.to_array (Zset.from_array p sp999_999_999 a) = a

  let test_009 () =
    let p = Zset.create_pool 1024 8 in
    let a = Array.init (20*3) (fun k -> 1_000_000 * (k/3) + (k mod 3) + 1) in
    Zset.to_array (Zset.from_array p sp999_999_999 a) = a

  (* single integers *)

  let test_100 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 12 |] in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_101 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 12; 14 |] in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_102 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 12; 14; 16 |] in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_103 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 12; 14; 16; 18 |] in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_104 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 12; 14; 16; 18; 20 |] in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_105 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 12; 14; 16; 18; 20; 22 |] in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_106 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 12; 14; 16; 18; 20; 22; 24 |] in
    Zset.to_array (Zset.from_array p sp999 a) = a

  let test_107 () =
    let p = Zset.create_pool 1024 8 in
    let a = Array.init 50 (fun k -> 2*k+1) in
    Zset.to_array (Zset.from_array p sp999 a) = a

  (* check that every 256-th word is an absolute number *)

  let test_200() =
    let p = Zset.create_pool 1024 8 in
    let a = Array.init (255*7 + 2) (fun k -> 2*k+1) in
    let z = Zset.from_array p sp999_999 a in
    Zset.to_array z = a &&
      ( let open Zset in
        let data = internal_data z in
        named "@0" (data.{ 0 } lsr 56 = 1)
        &&& named "@256" (data.{ 256 } lsr 56 = 2)
      )

  let test_201() =
    let p = Zset.create_pool 1024 8 in
    let a =
      Array.concat
        [  Array.init (255*7 + 1) (fun k -> 2*k+1);
           [| 100_000; 100_001; 100_002 |]
        ] in
    let z = Zset.from_array p sp999_999 a in
    Zset.to_array z = a &&
      ( let open Zset in
        let data = internal_data z in
        named "@0" (data.{ 0 } lsr 56 = 1)
        &&& named "@256" (data.{ 256 } lsr 56 = 2)
      )

  (* shift_space *)

  let test_300 () =
    let p = Zset.create_pool 1024 8 in
    let a = [| 10; 11; 12;
               20; 21; 22;
               30; 31; 32;
               40; 41; 42;
            |] in
    let z1 = Zset.from_array p sp999 a in
    let z2 = Zset.shift_space p sp999 z1 5 in
    Zset.to_array z2 = (Array.map (fun i -> i+5) a)


end


module Member = struct
  let test_001 () =
    let p = Zset.create_pool 1024 8 in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 5000 50000 in
    let z1 = Zset.from_array p (create_space 50000) a1 in
    let ok = ref true in
    for k = 0 to 9 do
      let k = Array.length a1 / 10 * k + 5 in
      let x1 = a1.(k) in
      let x2 = a1.(k+1) in
      ok := !ok && named ("x1,k=" ^ string_of_int k)
                         (Zset.mem x1 z1);
      if x2 <> x1+1 then
        ok := !ok && named ("x1+1,k=" ^ string_of_int k)
                           (not(Zset.mem (x1+1) z1));
    done;
    !ok

  let test_002 () =
    let p = Zset.create_pool 1024 8 in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 50000 500000 in
    let z1 = Zset.from_array p (create_space 500000) a1 in
    let ok = ref true in
    for k = 0 to 9 do
      let k = Array.length a1 / 10 * k + 5 in
      let x1 = a1.(k) in
      let x2 = a1.(k+1) in
      ok := !ok && named ("x1,k=" ^ string_of_int k)
                         (Zset.mem x1 z1);
      if x2 <> x1+1 then
        ok := !ok && named ("x1+1,k=" ^ string_of_int k)
                           (not(Zset.mem (x1+1) z1));
    done;
    !ok
end


module Intersect = struct
  let sp999 = create_space 999

  module I = struct
    type t = int
    let compare = Pervasives.compare
  end
  module ISet = Set.Make(I)

  let check a1 a2 a =
    let s1 = Array.fold_right ISet.add a1 ISet.empty in
    let s2 = Array.fold_right ISet.add a2 ISet.empty in
    let s = Array.fold_right ISet.add a ISet.empty in
    ISet.equal s (ISet.inter s1 s2)

  let test_001 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 20; 21; 22; 23 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.isect p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_002 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 12; 13; 14; 15; 38; 39; 40; 41 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.isect p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_003 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 11; 12; 41; 42 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.isect p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_004 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 9; 10; 11; 12; 13; 14 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.isect p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_100 () =
    let p = Zset.create_pool 1024 8 in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 5000 50000 in
    let seed2 = [| 874897252; 5631563; 2534943 |] in
    let a2 = create_array seed2 5000 50000 in
    let z1 = Zset.from_array p (create_space 50000) a1 in
    let z2 = Zset.from_array p (create_space 50000) a2 in
    let z = Zset.isect p z1 z2 in
    printf "[count1=%d count2=%d count=%d] %!"
           (Zset.count z1) (Zset.count z2) (Zset.count z);
    let a = Zset.to_array z in
    check a1 a2 a


  let test_101 () =
    let p = Zset.create_pool 1024 8 in
    let seed1 = [| 8748976386; 761763; 1 |] in
    let a1 = create_array seed1 5000 50000 in
    let seed2 = [| 874897252; 5631563; 2534943; 1 |] in
    let a2_1 = Array.sub a1 100 100 in
    let a2_2 = Array.sub a1 (Array.length a1 - 200) 100 in
    let a2_3 = create_array seed2 100 50000 in
    let a2 = shape_array (Array.concat [a2_1; a2_2; a2_3]) in
    let z1 = Zset.from_array p (create_space 50000) a1 in
    let z2 = Zset.from_array p (create_space 50000) a2 in
    let z = Zset.isect p z1 z2 in
    printf "[count1=%d count2=%d count=%d] %!"
           (Zset.count z1) (Zset.count z2) (Zset.count z);
    let a = Zset.to_array z in
    check a1 a2 a
end


module Difference = struct
  let sp999 = create_space 999

  module I = struct
    type t = int
    let compare = Pervasives.compare
  end
  module ISet = Set.Make(I)

  let check a1 a2 a =
    let s1 = Array.fold_right ISet.add a1 ISet.empty in
    let s2 = Array.fold_right ISet.add a2 ISet.empty in
    let s = Array.fold_right ISet.add a ISet.empty in
    ISet.equal s (ISet.diff s1 s2)

  let test_001 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 20; 21; 22; 23 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.diff p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_002 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 12; 13; 14; 15; 38; 39; 40; 41 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.diff p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_003 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 11; 12; 41; 42 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.diff p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_004 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 9; 10; 11; 12; 13; 14 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.diff p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_005 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 14; 15; 16; 17; 18; 19; 20; 40; 41; 42; 43 |] in
    let a2 = [| 9; 10; 11; 12; 13; 14; 17; 18; 41 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.diff p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_006 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 14; 15; 16; 17; 18; 19; 20; 40; 41; 42; 43 |] in
    let a2 = [| 12 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.diff p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_007 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 14; 15; 16; 17; 18; 19; 20; 40; 41; 42; 43 |] in
    let a2 = [| 12; 15; 18; 40; 41; 42; 43 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.diff p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_100 () =
    let p = Zset.create_pool 1024 8 in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 5000 50000 in
    let seed2 = [| 874897252; 5631563; 2534943 |] in
    let a2 = create_array seed2 5000 50000 in
    let z1 = Zset.from_array p (create_space 50000) a1 in
    let z2 = Zset.from_array p (create_space 50000) a2 in
    let z = Zset.diff p z1 z2 in
    printf "[count1=%d count2=%d count=%d] %!"
           (Zset.count z1) (Zset.count z2) (Zset.count z);
    let a = Zset.to_array z in
    check a1 a2 a

  let test_101 () =
    let p = Zset.create_pool 1024 8 in
    let seed1 = [| 8748976386; 761763; 1 |] in
    let a1 = create_array seed1 5000 50000 in
    let seed2 = [| 874897252; 5631563; 2534943; 1 |] in
    let a2_1 = Array.sub a1 100 100 in
    let a2_2 = Array.sub a1 (Array.length a1 - 200) 100 in
    let a2_3 = create_array seed2 100 50000 in
    let a2 = shape_array (Array.concat [a2_1; a2_2; a2_3]) in
    let z1 = Zset.from_array p (create_space 50000) a1 in
    let z2 = Zset.from_array p (create_space 50000) a2 in
    let z = Zset.diff p z1 z2 in
    printf "[count1=%d count2=%d count=%d] %!"
           (Zset.count z1) (Zset.count z2) (Zset.count z);
    let a = Zset.to_array z in
    check a1 a2 a
end


module Union = struct
  let sp999 = create_space 999

  module I = struct
    type t = int
    let compare = Pervasives.compare
  end
  module ISet = Set.Make(I)

  let check a1 a2 a =
    let s1 = Array.fold_right ISet.add a1 ISet.empty in
    let s2 = Array.fold_right ISet.add a2 ISet.empty in
    let s = Array.fold_right ISet.add a ISet.empty in
    ISet.equal s (ISet.union s1 s2)

  let test_001 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 20; 21; 22; 23 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.union p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_002 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 12; 13; 14; 15; 38; 39; 40; 41 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.union p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_003 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 11; 12; 41; 42 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.union p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_004 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 16; 17; 18; 19; 40; 41; 42; 43 |] in
    let a2 = [| 9; 10; 11; 12; 13; 14; 15; 16; 17; |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.union p z1 z2 in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_100 () =
    let p = Zset.create_pool 1024 8 in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 5000 200000 in
    let seed2 = [| 874897252; 5631563; 2534943 |] in
    let a2 = create_array seed2 5000 200000 in
    let z1 = Zset.from_array p (create_space 200000) a1 in
    let z2 = Zset.from_array p (create_space 200000) a2 in
    let z = Zset.union p z1 z2 in
    printf "[count1=%d count2=%d count=%d] %!"
           (Zset.count z1) (Zset.count z2) (Zset.count z);
    let a = Zset.to_array z in
    check a1 a2 a
end


module Union_many = struct
  let sp999 = create_space 999

  module I = struct
    type t = int
    let compare = Pervasives.compare
  end
  module ISet = Set.Make(I)

  let check a1 a2 a =
    let s1 = Array.fold_right ISet.add a1 ISet.empty in
    let s2 = Array.fold_right ISet.add a2 ISet.empty in
    let s = Array.fold_right ISet.add a ISet.empty in
    ISet.equal s (ISet.union s1 s2)

  let check3 a1 a2 a3 a =
    let s1 = Array.fold_right ISet.add a1 ISet.empty in
    let s2 = Array.fold_right ISet.add a2 ISet.empty in
    let s3 = Array.fold_right ISet.add a3 ISet.empty in
    let s = Array.fold_right ISet.add a ISet.empty in
    ISet.equal s (ISet.union (ISet.union s1 s2) s3)

  let test_001 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 20; 21; 22; 23 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.union_many p [z1;z2] in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_002 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 12; 13; 14; 15; 38; 39; 40; 41 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.union_many p [z1;z2] in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_003 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 11; 12; 41; 42 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.union_many p [z1;z2] in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_004 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 16; 17; 18; 19; 40; 41; 42; 43 |] in
    let a2 = [| 9; 10; 11; 12; 13; 14; 15; 16; 17; |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z = Zset.union_many p [z1;z2] in
    let a = Zset.to_array z in
    check a1 a2 a

  let test_011 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 20; 21; 22; 23 |] in
    let a3 = [| 12; 13; 18; 19; 50 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z3 = Zset.from_array p sp999 a3 in
    let z = Zset.union_many p [z1;z2;z3] in
    let a = Zset.to_array z in
    check3 a1 a2 a3 a

  let test_012 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 12; 13; 14; 15; 38; 39; 40; 41 |] in
    let a3 = [| 12; 13; 18; 19; 50 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z3 = Zset.from_array p sp999 a3 in
    let z = Zset.union_many p [z1;z2;z3] in
    let a = Zset.to_array z in
    check3 a1 a2 a3 a

  let test_013 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 40; 41; 42; 43 |] in
    let a2 = [| 11; 12; 41; 42 |] in
    let a3 = [| 12; 13; 18; 19; 50 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z3 = Zset.from_array p sp999 a3 in
    let z = Zset.union_many p [z1;z2;z3] in
    let a = Zset.to_array z in
    check3 a1 a2 a3 a

  let test_014 () =
    let p = Zset.create_pool 1024 8 in
    let a1 = [| 10; 11; 12; 13; 16; 17; 18; 19; 40; 41; 42; 43 |] in
    let a2 = [| 9; 10; 11; 12; 13; 14; 15; 16; 17; |] in
    let a3 = [| 12; 13; 18; 19; 50 |] in
    let z1 = Zset.from_array p sp999 a1 in
    let z2 = Zset.from_array p sp999 a2 in
    let z3 = Zset.from_array p sp999 a3 in
    let z = Zset.union_many p [z1;z2;z3] in
    let a = Zset.to_array z in
    check3 a1 a2 a3 a

  let test_100 () =
    let p = Zset.create_pool 1024 8 in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 5000 200000 in
    let seed2 = [| 874897252; 5631563; 2534943 |] in
    let a2 = create_array seed2 5000 200000 in
    let z1 = Zset.from_array p (create_space 200000) a1 in
    let z2 = Zset.from_array p (create_space 200000) a2 in
    let z = Zset.union_many p [z1;z2] in
    printf "[count1=%d count2=%d count=%d] %!"
           (Zset.count z1) (Zset.count z2) (Zset.count z);
    let a = Zset.to_array z in
    check a1 a2 a

  let test_101 () =
    let p = Zset.create_pool 1024 8 in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 10000 200000 in
    let seed2 = [| 874897252; 5631563; 2534943 |] in
    let a2 = create_array seed2 10000 200000 in
    let seed3 = [| 874284352; 5612363; 23 |] in
    let a3 = create_array seed3 10000 200000 in
    let z1 = Zset.from_array p (create_space 200000) a1 in
    let z2 = Zset.from_array p (create_space 200000) a2 in
    let z3 = Zset.from_array p (create_space 200000) a3 in
    let z = Zset.union_many p [z1;z2;z3] in
    printf "[count1=%d count2=%d count3=%d count=%d] %!"
           (Zset.count z1) (Zset.count z2) (Zset.count z3) (Zset.count z);
    let a = Zset.to_array z in
    check3 a1 a2 a3 a

end

module Arrayenc = struct
  let test_001 () =
    let p = Zset.create_pool ~enctype:Zset.Array_enc 100 1000 in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 20000 50000 in
    let z1 = Zset.from_array p (create_space 50000) a1 in
    let a2 = Zset.to_array z1 in
    named "a2" (a2 = a1) && (
      let ok = ref true in
      for k = 0 to 9 do
        let k = Array.length a1 / 10 * k + 5 in
        let x1 = a1.(k) in
        let x2 = a1.(k+1) in
        ok := !ok && named ("x1,k=" ^ string_of_int k)
                           (Zset.mem x1 z1);
        if x2 <> x1+1 then
          ok := !ok && named ("x1+1,k=" ^ string_of_int k)
                             (not(Zset.mem (x1+1) z1));
      done;
      !ok
    )
end

module Bitset = struct
  let test_001 () =
    let p = Zset.bitset_pool in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 20000 50000 in
    let z1 = Zset.from_array p (create_space 50000) a1 in
    let a2 = Zset.to_array z1 in
    named "a2" (a2 = a1) && (
      let ok = ref true in
      for k = 0 to 9 do
        let k = Array.length a1 / 10 * k + 5 in
        let x1 = a1.(k) in
        let x2 = a1.(k+1) in
        ok := !ok && named ("x1,k=" ^ string_of_int k)
                           (Zset.mem x1 z1);
        if x2 <> x1+1 then
          ok := !ok && named ("x1+1,k=" ^ string_of_int k)
                             (not(Zset.mem (x1+1) z1));
      done;
      !ok
    ) && (
      Zset.iter (fun _ len -> if len = 0 then failwith "len=0") z1;
      true
    )


  let test_002 () =
    let p = Zset.bitset_pool in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 20 50000 in
    let z1 = Zset.from_array p (create_space 50000) a1 in
    let a2 = Zset.to_array z1 in
    named "a2" (a2 = a1) && (
      let ok = ref true in
      for k = 0 to 9 do
        let k = Array.length a1 / 10 * k in
        let x1 = a1.(k) in
        let x2 = a1.(k+1) in
        ok := !ok && named ("x1,k=" ^ string_of_int k)
                           (Zset.mem x1 z1);
        if x2 <> x1+1 then
          ok := !ok && named ("x1+1,k=" ^ string_of_int k)
                             (not(Zset.mem (x1+1) z1));
      done;
      !ok
    ) && (
      Zset.iter (fun _ len -> if len = 0 then failwith "len=0") z1;
      true
    )

  let test_003 () =
    let p = Zset.bitset_pool in
    let a1 =
      Array.concat
        [ Array.init 1000 (fun k -> 2000+k);
          Array.init 960 (fun k -> 5000+k);
          Array.init 30 (fun k -> 7000+k);
          Array.init 15 (fun k -> 11000+k);
          Array.init 3 (fun k -> 13000+k);
        ] in
    let z1 = Zset.from_array p (create_space 50000) a1 in
    let a2 = Zset.to_array z1 in
    named "a2" (a2 = a1) && (
      let ok = ref true in
      for k = 0 to 9 do
        let k = Array.length a1 / 10 * k in
        let x1 = a1.(k) in
        let x2 = a1.(k+1) in
        ok := !ok && named ("x1,k=" ^ string_of_int k)
                           (Zset.mem x1 z1);
        if x2 <> x1+1 then
          ok := !ok && named ("x1+1,k=" ^ string_of_int k)
                             (not(Zset.mem (x1+1) z1));
      done;
      !ok
    ) && (
      Zset.iter (fun _ len -> if len = 0 then failwith "len=0") z1;
      true
    )

  let test_100 () =
    let p1 = Zset.default_pool in
    let p2 = Zset.bitset_pool in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 5000 50000 in
    let seed2 = [| 874897252; 5631563; 2534943 |] in
    let a2 = create_array seed2 5000 50000 in
    let z1 = Zset.from_array p1 (create_space 50000) a1 in
    let z2 = Zset.from_array p2 (create_space 50000) a2 in
    let z = Zset.isect Zset.default_pool z1 z2 in
    printf "[count1=%d count2=%d count=%d] %!"
           (Zset.count z1) (Zset.count z2) (Zset.count z);
    let a = Zset.to_array z in
    Intersect.check a1 a2 a


  let test_101 () =
    let p1 = Zset.bitset_pool in  (* diff from test_100 *)
    let p2 = Zset.bitset_pool in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 5000 50000 in
    let seed2 = [| 874897252; 5631563; 2534943 |] in
    let a2 = create_array seed2 5000 50000 in
    let z1 = Zset.from_array p1 (create_space 50000) a1 in
    let z2 = Zset.from_array p2 (create_space 50000) a2 in
    let z = Zset.isect Zset.default_pool z1 z2 in
    printf "[count1=%d count2=%d count=%d] %!"
           (Zset.count z1) (Zset.count z2) (Zset.count z);
    let a = Zset.to_array z in
    Intersect.check a1 a2 a


  let test_102 () =
    let p1 = Zset.bitset_pool in  (* diff from test_100 *)
    let p2 = Zset.bitset_pool in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 5000 50000 in
    let seed2 = [| 874897252; 5631563; 2534943 |] in
    let a2 = create_array seed2 5000 50000 in
    let z1 = Zset.from_array p1 (create_space 50000) a1 in
    let z2 = Zset.from_array p2 (create_space 50000) a2 in
    let z = Zset.isect Zset.bitset_pool z1 z2 in  (* diff from test_101 *)
    printf "[count1=%d count2=%d count=%d] %!"
           (Zset.count z1) (Zset.count z2) (Zset.count z);
    let a = Zset.to_array z in
    Intersect.check a1 a2 a

  let test_103 () =
    let p1 = Zset.default_pool in
    let p2 = Zset.bitset_pool in
    let a1 = [| 1; 2; 3 |] in
    let a2 = [| 2; 3 |] in
    let z1 = Zset.from_array p1 (create_space 50000) a1 in
    let z2 = Zset.from_array p2 (create_space 50000) a2 in
    let z = Zset.isect Zset.bitset_pool z1 z2 in  (* diff from test_101 *)
    let a = Zset.to_array z in
    Intersect.check a1 a2 a

  let test_200 () =
    let p1 = Zset.default_pool in
    let p2 = Zset.bitset_pool in
    let seed1 = [| 8748976386; 761763; 43759475 |] in
    let a1 = create_array seed1 5000 200000 in
    let seed2 = [| 874897252; 5631563; 2534943 |] in
    let a2 = create_array seed2 5000 200000 in
    let z1 = Zset.from_array p1 (create_space 200000) a1 in
    let z2 = Zset.from_array p2 (create_space 200000) a2 in
    let z = Zset.union_many Zset.bitset_pool [z1;z2] in
    printf "[count1=%d count2=%d count=%d] %!"
           (Zset.count z1) (Zset.count z2) (Zset.count z);
    let a = Zset.to_array z in
    Union_many.check a1 a2 a &&
      named "count" (Array.length a = Zset.count z) &&
      named "first" (a.(0) = Zset.first z) &&
      named "last" (a.(Array.length a - 1) = Zset.last z)

end


let tests =
  [ ("Encoding.test_001", Encoding.test_001);
    ("Encoding.test_002", Encoding.test_002);
    ("Encoding.test_003", Encoding.test_003);
    ("Encoding.test_004", Encoding.test_004);
    ("Encoding.test_005", Encoding.test_005);
    ("Encoding.test_006", Encoding.test_006);
    ("Encoding.test_007", Encoding.test_007);
    ("Encoding.test_008", Encoding.test_008);
    ("Encoding.test_009", Encoding.test_009);

    ("Encoding.test_100", Encoding.test_100);
    ("Encoding.test_101", Encoding.test_101);
    ("Encoding.test_102", Encoding.test_102);
    ("Encoding.test_103", Encoding.test_103);
    ("Encoding.test_104", Encoding.test_104);
    ("Encoding.test_105", Encoding.test_105);
    ("Encoding.test_106", Encoding.test_106);
    ("Encoding.test_107", Encoding.test_107);

    ("Encoding.test_200", Encoding.test_200);
    ("Encoding.test_201", Encoding.test_201);

    ("Encoding.test_300", Encoding.test_300);

    ("Member.test_001", Member.test_001);
    ("Member.test_002", Member.test_002);

    ("Intersect.test_001", Intersect.test_001);
    ("Intersect.test_002", Intersect.test_002);
    ("Intersect.test_003", Intersect.test_003);
    ("Intersect.test_004", Intersect.test_004);
    ("Intersect.test_100", Intersect.test_100);
    ("Intersect.test_101", Intersect.test_101);

    ("Difference.test_001", Difference.test_001);
    ("Difference.test_002", Difference.test_002);
    ("Difference.test_003", Difference.test_003);
    ("Difference.test_004", Difference.test_004);
    ("Difference.test_005", Difference.test_005);
    ("Difference.test_006", Difference.test_006);
    ("Difference.test_007", Difference.test_007);
    ("Difference.test_100", Difference.test_100);
    ("Difference.test_101", Difference.test_101);

    ("Union.test_001", Union.test_001);
    ("Union.test_002", Union.test_002);
    ("Union.test_003", Union.test_003);
    ("Union.test_004", Union.test_004);
    ("Union.test_100", Union.test_100);

    ("Union_many.test_001", Union_many.test_001);
    ("Union_many.test_002", Union_many.test_002);
    ("Union_many.test_003", Union_many.test_003);
    ("Union_many.test_004", Union_many.test_004);
    ("Union_many.test_011", Union_many.test_011);
    ("Union_many.test_012", Union_many.test_012);
    ("Union_many.test_013", Union_many.test_013);
    ("Union_many.test_014", Union_many.test_014);
    ("Union_many.test_100", Union_many.test_100);
    ("Union_many.test_101", Union_many.test_101);

    ("Arrayenc.test_001", Arrayenc.test_001);

    ("Bitset.test_001", Bitset.test_001);
    ("Bitset.test_002", Bitset.test_002);
    ("Bitset.test_003", Bitset.test_003);
    ("Bitset.test_100", Bitset.test_100);
    ("Bitset.test_101", Bitset.test_101);
    ("Bitset.test_102", Bitset.test_102);
    ("Bitset.test_103", Bitset.test_103);
    ("Bitset.test_200", Bitset.test_200);
  ]
