open Zcontainer
open Ztypes
open T_frame.Util

module MB_string = struct

    let mb = Zrepr.string_mb
    
    let test_001() =
      (* empty string *)
      let data = "" in
      let size = mb.mb_size data in
      let b = Bytes.create size in
      mb.mb_encode b 0 data;
      let decoded = mb.mb_decode b 0 size in
      named "size" (size = 0) &&&
        named "roundtrip" (decoded = data)

    let test_002() =
      (* non-empty string *)
      let data = "something" in
      let size = mb.mb_size data in
      let b = Bytes.create size in
      mb.mb_encode b 0 data;
      let decoded = mb.mb_decode b 0 size in
      named "size" (size = 9) &&&
        named "roundtrip" (decoded = data)
end


module MB_int = struct

    let mb = Zrepr.int_mb
    
    let test_001() =
      List.for_all
        (fun data ->
           let size = mb.mb_size data in
           let b = Bytes.create size in
           mb.mb_encode b 0 data;
           let decoded = mb.mb_decode b 0 size in
           named (string_of_int data) (decoded = data)
        )
        [ 0; 42; 83786843658465; (-42); (-8793798473549875) ]
  end


module MB_int_list = struct

    let mb = Zrepr.list_mb Ztypes.Zint Zrepr.int_mb
    
    let test_001() =
      List.for_all
        (fun data ->
           let size = mb.mb_size data in
           let b = Bytes.create size in
           mb.mb_encode b 0 data;
           let decoded = mb.mb_decode b 0 size in
           named (String.concat "," (List.map string_of_int data))
                 (decoded = data)
        )
        [ [];
          [1;2;3];
          [-1;-2;-3];
          [7868365845645; 7836835684658465]
        ]
  end


module MB_string_list = struct

    let mb = Zrepr.list_mb Ztypes.Zstring Zrepr.string_mb
    
    let test_001() =
      List.for_all
        (fun data ->
           let size = mb.mb_size data in
           let b = Bytes.create size in
           mb.mb_encode b 0 data;
           let decoded = mb.mb_decode b 0 size in
           named (String.concat "," data)
                 (decoded = data)
        )
        [ [];
          [ "" ];
          [ ""; "" ];
          [ ""; "x" ];
          [ "x"; "" ];
          [ "X"; "y"; "zzzzzzzzzzzzzzzzzzzzzzzz" ]
        ]
  end
                       

let tests =
  [ ("MB_string.test_001", MB_string.test_001);
    ("MB_string.test_002", MB_string.test_002);

    ("MB_int.test_001", MB_int.test_001);

    ("MB_int_list.test_001", MB_int_list.test_001);

    ("MB_string_list.test_001", MB_string_list.test_001);
  ]
