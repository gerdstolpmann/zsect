open Zcontainer
open Ztypes
open Zdigester
open T_frame.Util

let get_zinvcol c =
  match Zcol.content c with
    | Zcol.Inv zinvcol -> zinvcol
    | _ -> assert false

module Stage = struct
  let cols1 =
    [| "i1", ParserBox(Zint64, Int64.of_string), "DF64/int64", "int64_asc";
       "i2", ParserBox(Zint64, Int64.of_string), "DF64/int64", "int64_desc";
       "f1", ParserBox(Zfloat, float_of_string), "DF64/float", "float_asc";
       "f2", ParserBox(Zfloat, float_of_string), "DF64/float", "float_desc";
      |]

  let cols2 =
    [| "i1", ParserBox(Zint64, Int64.of_string), "IM/int64", "int64_asc";
       "i2", ParserBox(Zint64, Int64.of_string), "DF64/int64", "int64_desc";
       "f1", ParserBox(Zfloat, float_of_string), "IM/float", "float_asc";
       "f2", ParserBox(Zfloat, float_of_string), "DF64/float", "float_desc";
      |]


  let parse_pair p1 p2 s =
    try
      let k = String.index s ',' in
      (p1 (String.sub s 0 k),
       p2 (String.sub s (k+1) (String.length s - k - 1))
      )
    with
      | Not_found ->
          failwith "Cannot parse pair: no comma"

  let cols3 =
    [| "p1", ParserBox(Zpair(Zint64, Zstring), 
                       parse_pair Int64.of_string (fun s -> s)),
             "DM/int64 * string", "asc";
      |]

  let cols4 =
    [| "p1", ParserBox(Zpair(Zint64, Zstring), 
                       parse_pair Int64.of_string (fun s -> s)),
             "IM/int64 * string", "asc";
      |]

  let test_001() =
    let zt = Ztable.create_memory_ztable Zrepr.stdrepr (create_space 100) in
    let st = create_stage zt cols1 in
    stage_add st [| "54"; "-13"; "3.14"; "1E99" |];
    stage_add st [| "68"; "-12"; "3.14"; "512" |];
    stage_add st [| "54"; "-13"; "2.01"; "0" |];
    stage_finish st;
    let c1 = Ztable.get_zcol zt Zint64 "i1" in
    let c2 = Ztable.get_zcol zt Zint64 "i2" in
    let c3 = Ztable.get_zcol zt Zfloat "f1" in
    let c4 = Ztable.get_zcol zt Zfloat "f2" in
    named "c1" (List.map (Zcol.value_of_id c1) [1;2;3] = [54L; 68L; 54L]) &&&
    named "c2" (List.map (Zcol.value_of_id c2) [1;2;3] = [-13L; -12L; -13L]) &&&
    named "c3" (List.map (Zcol.value_of_id c3) [1;2;3] = [3.14; 3.14; 2.01]) &&&
    named "c4" (List.map (Zcol.value_of_id c4) [1;2;3] = [1E99; 512.0; 0.0])

  let test_002() =
    let zt = Ztable.create_memory_ztable Zrepr.stdrepr (create_space 100) in
    let st = create_stage zt cols2 in
    stage_add st [| "54"; "-13"; "3.14"; "1E99" |];
    stage_add st [| "68"; "-12"; "3.14"; "512" |];
    stage_add st [| "54"; "-13"; "2.01"; "0" |];
    stage_finish st;
    let c1 = Ztable.get_zcol zt Zint64 "i1" in
    let c2 = Ztable.get_zcol zt Zint64 "i2" in
    let c3 = Ztable.get_zcol zt Zfloat "f1" in
    let c4 = Ztable.get_zcol zt Zfloat "f2" in
    let inv1 = get_zinvcol c1 in
    let inv3 = get_zinvcol c3 in
    named "c1" (List.map (Zcol.value_of_id c1) [1;2;3] = [54L; 68L; 54L]) &&&
    named "c2" (List.map (Zcol.value_of_id c2) [1;2;3] = [-13L; -12L; -13L]) &&&
    named "c3" (List.map (Zcol.value_of_id c3) [1;2;3] = [3.14; 3.14; 2.01]) &&&
    named "c4" (List.map (Zcol.value_of_id c4) [1;2;3] = [1E99; 512.0; 0.0]) &&&
    named "inv1" (Zinvcol.dump inv1 = [ 54L, [| 1; 3 |]; 68L, [| 2 |] ]) &&&
    named "inv3" (Zinvcol.dump inv3 = [ 2.01, [| 3 |]; 3.14, [| 1; 2 |] ])

  let test_003() =
    let zt = Ztable.create_memory_ztable Zrepr.stdrepr (create_space 100) in
    let st = create_stage zt cols3 in
    stage_add st [| "12,def" |];
    stage_add st [| "54,abc" |];
    stage_add st [| "105,ghi" |];
    stage_finish st;
    let c1 = Ztable.get_zcol zt (Zpair(Zint64, Zstring)) "p1" in
    named "c1" (List.map
                  (Zcol.value_of_id c1)
                  [1;2;3] =
                  [ (12L, "def"); (54L, "abc");  (105L, "ghi") ])

  let test_004() =
    let zt = Ztable.create_memory_ztable Zrepr.stdrepr (create_space 100) in
    let st = create_stage zt cols4 in
    stage_add st [| "12,def" |];
    stage_add st [| "54,abc" |];
    stage_add st [| "105,ghi" |];
    stage_add st [| "54,abc" |];
    stage_add st [| "54,def" |];
    stage_finish st;
    let c1 = Ztable.get_zcol zt (Zpair(Zint64, Zstring)) "p1" in
    let inv1 = get_zinvcol c1 in
    named "c1" (List.map
                  (Zcol.value_of_id c1)
                  [1;2;3] =
                  [ (12L, "def"); (54L, "abc");  (105L, "ghi") ]) &&&
    named "inv1" (Zinvcol.dump inv1 = [ (12L, "def"), [| 1 |];
                                        (54L, "abc"), [| 2; 4 |];
                                        (54L, "def"), [| 5 |];
                                        (105L, "ghi"), [| 3 |] ])
end

module Digest = struct
  let cols =
    [| "i1", ParserBox(Zint64, Int64.of_string), "IM/int64", "int64_asc";
       "i2", ParserBox(Zint64, Int64.of_string), "DF64/int64", "int64_desc";
       "f1", ParserBox(Zfloat, float_of_string), "IM/float", "float_asc";
       "f2", ParserBox(Zfloat, float_of_string), "DF64/float", "float_desc";
      |]

  let test_001() =
    let dg = Zdigester.create_memory_zdigester
               ~max_stage_rows:3 Zrepr.stdrepr cols in
    build_add dg [| "54"; "-13"; "3.14"; "1E99" |];
    build_add dg [| "68"; "-12"; "3.14"; "512" |];
    build_add dg [| "54"; "-13"; "2.01"; "0" |];
    build_add dg [| "54"; "-14"; "2.02"; "4" |];
    build_add dg [| "54"; "-15"; "2.02"; "8" |];
    build_add dg [| "54"; "-16"; "2.02"; "12" |];
    build_add dg [| "68"; "-17"; "2.01"; "14" |];
    build_add dg [| "54"; "-18"; "2.03"; "21" |];
    let zt = build_end dg in
    let c1 = Ztable.get_zcol zt Zint64 "i1" in
    let c2 = Ztable.get_zcol zt Zint64 "i2" in
    let c3 = Ztable.get_zcol zt Zfloat "f1" in
    let c4 = Ztable.get_zcol zt Zfloat "f2" in
    let inv1 = get_zinvcol c1 in
    let inv3 = get_zinvcol c3 in
    named "space" (Ztable.space zt = create_space 8) &&&
    named "c1" (List.map (Zcol.value_of_id c1) [1;2;8] = [54L; 68L; 54L]) &&&
    named "c2" (List.map (Zcol.value_of_id c2) [1;2;8] = [-13L; -12L; -18L]) &&&
    named "c3" (List.map (Zcol.value_of_id c3) [1;2;8] = [3.14; 3.14; 2.03]) &&&
    named "c4" (List.map (Zcol.value_of_id c4) [1;2;8] = [1E99; 512.0; 21.]) &&&
    named "inv1" (Zinvcol.dump inv1 = [ 54L, [| 1; 3; 4; 5; 6; 8 |]; 68L, [| 2; 7 |] ]) &&&
    named "inv3" (Zinvcol.dump inv3 = [ 2.01, [| 3; 7 |]; 2.02, [| 4; 5; 6 |]; 2.03, [| 8 |]; 3.14, [| 1; 2 |] ])

end


module Digest_list = struct
  let ilist = Zlist Zint
  let cols =
    [| "i1", ParserBox(Zint64, Int64.of_string), "IM/int64", "int64_asc";
       "L1", NoParserBox(ilist), "IM/int list", "asc";
       "L2", NoParserBox(ilist), "DM/int list", "asc";
      |]

  let test_001() =
    let dg = Zdigester.create_memory_zdigester
               ~max_stage_rows:3 Zrepr.stdrepr cols in
    build_add_food dg [| StringBox "54";
                         ValueBox(ilist, [1; 2]);
                         ValueBox(ilist, [3; 4]);
                        |];
    build_add_food dg [| StringBox "54";
                         ValueBox(ilist, [2; 4]);
                         ValueBox(ilist, [4]);
                        |];
    build_add_food dg [| StringBox "12";
                         ValueBox(ilist, [18; 1]);
                         ValueBox(ilist, [45; 1]);
                        |];
    build_add_food dg [| StringBox "3";
                         ValueBox(ilist, [1; 2]);
                         ValueBox(ilist, [1; 45]);
                        |];
    build_add_food dg [| StringBox "99";
                         ValueBox(ilist, []);
                         ValueBox(ilist, []);
                        |];
    let zt = build_end dg in
    let c1 = Ztable.get_zcol zt Zint64 "i1" in
    let c2 = Ztable.get_zcol zt ilist "L1" in
    let c3 = Ztable.get_zcol zt ilist "L2" in
    let inv1 = get_zinvcol c1 in
    let inv2 = get_zinvcol c2 in
    named "space" (Ztable.space zt = create_space 5) &&&
    named "c1" (List.map (Zcol.value_of_id c1) [1;3;5] = [54L;12L;99L]) &&&
    named "c2" (List.map (Zcol.value_of_id c2) [1;3;5] =
                    [ [1;2]; [18;1]; [] ]) &&&
    named "c3" (List.map (Zcol.value_of_id c3) [1;3;5] =
                    [ [3;4]; [45;1]; [] ]) &&&
    named "inv1" (Zinvcol.dump inv1 =
                    [ 3L, [| 4 |];
                      12L, [| 3 |];
                      54L, [| 1; 2 |];
                      99L, [| 5 |] ]) &&&
    named "inv2" (Zinvcol.dump inv2 =
                    [ [], [| 5 |];
                      [1; 2], [| 1; 4 |];
                      [2; 4], [| 2 |];
                      [18; 1], [| 3 |] ])

end
                 


let tests =
  [ ("Stage.test_001", Stage.test_001);
    ("Stage.test_002", Stage.test_002);
    ("Stage.test_003", Stage.test_003);
    ("Stage.test_004", Stage.test_004);

    ("Digest.test_001", Digest.test_001);

    ("Digest_list.test_001", Digest_list.test_001);
  ]
