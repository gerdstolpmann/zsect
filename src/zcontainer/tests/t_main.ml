let () =
  T_frame.Main.register "Zutil" T_zutil.tests;
  T_frame.Main.register "Zrepr" T_zrepr.tests;
  T_frame.Main.register "Zset" T_zset.tests;
  T_frame.Main.register "Zinvcol" T_zinvcol.tests;
  T_frame.Main.register "Zdircol" T_zdircol.tests;
  T_frame.Main.register "Zdigester" T_zdigester.tests;
  T_frame.Main.register "Zdiscretize" T_zdiscretize.tests;
  
  T_frame.Main.main()
