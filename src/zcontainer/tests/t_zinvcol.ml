open Zcontainer
open Ztypes
open Zinvcol
open T_frame.Util

module Build = struct
  let sp = Ztypes.create_space 999

  let is_none =
    function
    | None -> true
    | _ -> false

  let col1 = ZcolDescr("col1",Zstring,Inv_multibyte(Zrepr.string_mb),
                       Zrepr.string_asc)
  let col2 = ZcolDescr("col2",Zoption Zstring,
                       SInv_multibyte(Zrepr.string_opt_mb,
                                      None, is_none),
                       Zrepr.option_ord Zstring Zrepr.string_asc)

  let col3 = ZcolDescr("col3",Zlist Zstring,
                       Inv_multibyte(Zrepr.list_mb Zstring Zrepr.string_mb),
                       Zrepr.list_ord Zstring Zrepr.string_asc)
                      
  let test_001() =
    (* empty column *)
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    let c = build_end b in
    length c = 0

  let test_010() =
    (* column with one row. No zsets *)
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    build_add b "AA0" Zset.empty;
    let c = build_end b in
    named "length" (length c = 1) &&&
      named "val0" (get_value c 0 = "AA0") &&&
      named "set0" (Zset.is_empty (get_zset c 0))

  let test_011() =
    (* column with one row. One-elem zset *)
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    let s0_a = [| 42 |] in
    let s0 = Zset.from_array Zset.default_pool sp s0_a in
    build_add b "AA0" s0;
    let c = build_end b in
    named "length" (length c = 1) &&&
      named "val0" (get_value c 0 = "AA0") &&&
      let s0' = get_zset c 0 in
      named "set0.count" (Zset.count s0' = 1) &&&
      named "set0.first" (Zset.first s0' = 42) &&&
      named "set0.last" (Zset.last s0' = 42) &&&
      named "set0" (Zset.to_array s0' = s0_a) &&&
      named "idx0" (value_of_id c 42 = "AA0")

  let test_012() =
    (* column with one row. Multi-elem zset *)
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    let s0_a = [| 42; 80; 123; 455 |] in
    let s0 = Zset.from_array Zset.default_pool sp s0_a in
    build_add b "AA0" s0;
    let c = build_end b in
    named "length" (length c = 1) &&&
      named "val0" (get_value c 0 = "AA0") &&&
      let s0' = get_zset c 0 in
      named "set0.count" (Zset.count s0' = 4) &&&
      named "set0.first" (Zset.first s0' = 42) &&&
      named "set0.last" (Zset.last s0' = 455) &&&
      named "set0" (Zset.to_array s0' = s0_a) &&&
      named "idx0" (value_of_id c 80 = "AA0")


  let test_020() =
    (* column with several rows. No zsets *)
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    build_add b "AA0" Zset.empty;
    build_add b "AA1" Zset.empty;
    build_add b "AA2" Zset.empty;
    let c = build_end b in
    named "length" (length c = 3) &&&
      named "val0" (get_value c 0 = "AA0") &&&
      named "set0" (Zset.is_empty (get_zset c 0)) &&&
      named "val1" (get_value c 1 = "AA1") &&&
      named "set1" (Zset.is_empty (get_zset c 1)) &&&
      named "val2" (get_value c 2 = "AA2") &&&
      named "set2" (Zset.is_empty (get_zset c 2))

  let test_021() =
    (* column with several rows. One-elem zset *)
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    let s0_a = [| 42 |] in
    let s0 = Zset.from_array Zset.default_pool sp s0_a in
    let s1_a = [| 78 |] in
    let s1 = Zset.from_array Zset.default_pool sp s1_a in
    let s2_a = [| 23 |] in
    let s2 = Zset.from_array Zset.default_pool sp s2_a in
    build_add b "AA0" s0;
    build_add b "AA1" s1;
    build_add b "AA2" s2;
    let c = build_end b in
    named "length" (length c = 3) &&&
      named "val0" (get_value c 0 = "AA0") &&&
      let s0' = get_zset c 0 in
      named "set0.count" (Zset.count s0' = 1) &&&
      named "set0.first" (Zset.first s0' = 42) &&&
      named "set0.last" (Zset.last s0' = 42) &&&
      named "set0" (Zset.to_array s0' = s0_a) &&&

      named "val1" (get_value c 1 = "AA1") &&&
      let s1' = get_zset c 1 in
      named "set1.count" (Zset.count s1' = 1) &&&
      named "set1.first" (Zset.first s1' = 78) &&&
      named "set1.last" (Zset.last s1' = 78) &&&
      named "set1" (Zset.to_array s1' = s1_a) &&&

      named "val2" (get_value c 2 = "AA2") &&&
      let s2' = get_zset c 2 in
      named "set2.count" (Zset.count s2' = 1) &&&
      named "set2.first" (Zset.first s2' = 23) &&&
      named "set2.last" (Zset.last s2' = 23) &&&
      named "set2" (Zset.to_array s2' = s2_a)

  let test_022() =
    (* column with several rows. Multi-elem zset *)
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    let s0_a = [| 5; 42; 67 |] in
    let s0 = Zset.from_array Zset.default_pool sp s0_a in
    let s1_a = [| 7; 78; 144 |] in
    let s1 = Zset.from_array Zset.default_pool sp s1_a in
    let s2_a = [| 22; 23; 24 |] in
    let s2 = Zset.from_array Zset.default_pool sp s2_a in
    build_add b "AA0" s0;
    build_add b "AA1" s1;
    build_add b "AA2" s2;
    let c = build_end b in
    named "length" (length c = 3) &&&
      named "val0" (get_value c 0 = "AA0") &&&
      let s0' = get_zset c 0 in
      named "set0.count" (Zset.count s0' = 3) &&&
      named "set0.first" (Zset.first s0' = 5) &&&
      named "set0.last" (Zset.last s0' = 67) &&&
      named "set0" (Zset.to_array s0' = s0_a) &&&

      named "val1" (get_value c 1 = "AA1") &&&
      let s1' = get_zset c 1 in
      named "set1.count" (Zset.count s1' = 3) &&&
      named "set1.first" (Zset.first s1' = 7) &&&
      named "set1.last" (Zset.last s1' = 144) &&&
      named "set1" (Zset.to_array s1' = s1_a) &&&

      named "val2" (get_value c 2 = "AA2") &&&
      let s2' = get_zset c 2 in
      named "set2.count" (Zset.count s2' = 3) &&&
      named "set2.first" (Zset.first s2' = 22) &&&
      named "set2.last" (Zset.last s2' = 24) &&&
      named "set2" (Zset.to_array s2' = s2_a)

  (* Testing sparse multibyte *)

  let test_100() =
    (* empty column *)
    let b = create_memory_zinvcol_builder 100 100 sp col2 in
    let c = build_end b in
    named "length" (length c = 1) &&&
      named "val0" (get_value c 0 = None) &&&
      named "idx0" (value_of_id c 42 = None)

  let test_110() =
    (* column with one non-anon row. No zsets *)
    let b = create_memory_zinvcol_builder 100 100 sp col2 in
    build_add b (Some "AA0") Zset.empty;
    let c = build_end b in
    named "length" (length c = 2) &&&
      named "val0" (get_value c 0 = Some "AA0") &&&
      named "set0" (Zset.is_empty (get_zset c 0)) &&&
      named "val1" (get_value c 1 = None)

  let test_111() =
    (* column with one anon row. No zsets *)
    let b = create_memory_zinvcol_builder 100 100 sp col2 in
    build_add b None Zset.empty;
    let c = build_end b in
    named "length" (length c = 1) &&&
      named "val0" (get_value c 0 = None) &&&
      named "idx0" (value_of_id c 42 = None)

  let test_112() =
    (* column with one row. One-elem zset *)
    let b = create_memory_zinvcol_builder 100 100 sp col2 in
    let s0_a = [| 42 |] in
    let s0 = Zset.from_array Zset.default_pool sp s0_a in
    build_add b (Some "AA0") s0;
    let c = build_end b in
    named "length" (length c = 2) &&&
      named "val0" (get_value c 0 = Some "AA0") &&&
      let s0' = get_zset c 0 in
      named "set0.count" (Zset.count s0' = 1) &&&
      named "set0.first" (Zset.first s0' = 42) &&&
      named "set0.last" (Zset.last s0' = 42) &&&
      named "set0" (Zset.to_array s0' = s0_a) &&&
      named "idx0" (value_of_id c 42 = Some "AA0") &&&
      named "val1" (get_value c 1 = None)

  let test_113() =
    (* column with a zset that is large enough to trigger a
       hash table reconstruction *)
    let b = create_memory_zinvcol_builder 100 100 sp col2 in
    let s0_a = Array.init 150 (fun k -> 3*k + 1) in
    let s0 = Zset.from_array Zset.default_pool sp s0_a in
    build_add b (Some "AA0") s0;
    let c = build_end b in
    named "length" (length c = 2) &&&
      named "val0" (get_value c 0 = Some "AA0") &&&
      let s0' = get_zset c 0 in
      named "set0.count" (Zset.count s0' = 150) &&&
      named "set0.first" (Zset.first s0' = 1) &&&
      named "set0.last" (Zset.last s0' = 448) &&&
      named "set0" (Zset.to_array s0' = s0_a) &&&
      named "idx0" (value_of_id c 112 = Some "AA0") &&&
      named "val1" (get_value c 1 = None)

  let test_114() =
    (* two larger sets *)
    let b = create_memory_zinvcol_builder 100 100 sp col2 in
    let s0_a = Array.init 150 (fun k -> 3*k + 1) in
    let s0 = Zset.from_array Zset.default_pool sp s0_a in
    let s1_a = Array.init 110 (fun k -> 3*k + 2) in
    let s1 = Zset.from_array Zset.default_pool sp s1_a in
    build_add b (Some "AA0") s0;
    build_add b (Some "AA1") s1;
    let c = build_end b in
    named "length" (length c = 3) &&&
      named "val0" (get_value c 0 = Some "AA0") &&&
      named "val1" (get_value c 1 = Some "AA1") &&&
      let s0' = get_zset c 0 in
      named "set0.count" (Zset.count s0' = 150) &&&
      named "set0.first" (Zset.first s0' = 1) &&&
      named "set0.last" (Zset.last s0' = 448) &&&
      named "set0" (Zset.to_array s0' = s0_a) &&&
      named "idx0" (value_of_id c 112 = Some "AA0") &&&
      let s1' = get_zset c 1 in
      named "set1.count" (Zset.count s1' = 110) &&&
      named "set1.first" (Zset.first s1' = 2) &&&
      named "set1.last" (Zset.last s1' = 329) &&&
      named "set1" (Zset.to_array s1' = s1_a) &&&
      named "idx1" (value_of_id c 203 = Some "AA1") &&&
      named "idxother" (value_of_id c 450 = None) &&&
      named "val2" (get_value c 2 = None)

   (* testing lists *)

  let test_200() =
    let s0_a = [| 42 |] in
    let s0 = Zset.from_array Zset.default_pool sp s0_a in
    let b = create_memory_zinvcol_builder 100 100 sp col3 in
    build_add b ["abc"; "def"] s0;
    let c = build_end b in
    named "length" (length c = 1) &&&
      named "val0" (get_value c 0 = ["abc";"def"]) &&&
      named "set0" (Zset.to_array(get_zset c 0) = s0_a)

  let test_201() =
    let s0_a = [| 42 |] in
    let s0 = Zset.from_array Zset.default_pool sp s0_a in
    let s1_a = [| 44 |] in
    let s1 = Zset.from_array Zset.default_pool sp s1_a in
    let b = create_memory_zinvcol_builder 100 100 sp col3 in
    build_add b ["abc"; "def"] s0;
    build_add b ["abc"; "ghi"] s1;
    let c = build_end b in
    named "length" (length c = 2) &&&
      named "val0" (get_value c 0 = ["abc";"def"]) &&&
      named "set0" (Zset.to_array(get_zset c 0) = s0_a) &&&
      named "val1" (get_value c 1 = ["abc";"ghi"]) &&&
      named "set1" (Zset.to_array(get_zset c 1) = s1_a)
            
  end


module Find = struct
  let sp = Ztypes.create_space 999

  let col1 = ZcolDescr("col1",Zstring,Inv_multibyte(Zrepr.string_mb),
                       Zrepr.string_asc)

  let col3 = ZcolDescr("col3",Zlist Zstring,
                       Inv_multibyte(Zrepr.list_mb Zstring Zrepr.string_mb),
                       Zrepr.list_ord Zstring Zrepr.string_asc)
                      
  let test_001() =
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    build_add b "AA0" Zset.empty;
    build_add b "AA1" Zset.empty;
    build_add b "AA2" Zset.empty;
    build_add b "AA3" Zset.empty;
    build_add b "AA4" Zset.empty;
    build_add b "AA5" Zset.empty;
    build_add b "AA6" Zset.empty;
    build_add b "AA7" Zset.empty;
    build_add b "AA8" Zset.empty;
    build_add b "AA9" Zset.empty;
    let c = build_end b in
    named "find-GE-AA" (find c GE "AA" = 0) &&&
      named "find-GE-AA2" (find c GE "AA2" = 2) &&&
      named "find-GE-AA6" (find c GE "AA6" = 6) &&&
      named "find-GE-AA7a" (find c GE "AA7a" = 8) &&&
      named "find-GE-B00" (try ignore(find c GE "B00"); false
                        with Not_found -> true) &&&
      named "find-GE-AA" (find c GT "AA" = 0) &&&
      named "find-GT-AA2" (find c GT "AA2" = 3) &&&
      named "find-GT-AA6" (find c GT "AA6" = 7) &&&
      named "find-GT-AA7a" (find c GT "AA7a" = 8) &&&
      named "find-GT-B00" (try ignore(find c GT "B00"); false
                           with Not_found -> true) &&&
      named "find-LE-AA" (try ignore(find c LE "AA"); false
                          with Not_found -> true) &&&
      named "find-LE-AA2" (find c LE "AA2" = 2) &&&
      named "find-LE-AA6" (find c LE "AA6" = 6) &&&
      named "find-LE-AA7a" (find c LE "AA7a" = 7) &&&
      named "find-LE-B00" (find c LE "B00" = 9) &&&
      named "find-LT-AA" (try ignore(find c LT "AA"); false
                          with Not_found -> true) &&&
      named "find-LT-AA2" (find c LT "AA2" = 1) &&&
      named "find-LT-AA6" (find c LT "AA6" = 5) &&&
      named "find-LT-AA7a" (find c LT "AA7a" = 7) &&&
      named "find-LT-B00" (find c LT "B00" = 9) &&&
      named "find-EQ-AA" (try ignore(find c EQ "AA"); false
                          with Not_found -> true) &&&
      named "find-EQ-AA2" (find c EQ "AA2" = 2) &&&
      named "find-EQ-AA6" (find c EQ "AA6" = 6) &&&
      named "find-EQ-AA7a" (try ignore(find c EQ "AA7a"); false
                            with Not_found -> true) &&&
      named "find-EQ-B00" (try ignore(find c EQ "B00"); false
                           with Not_found -> true)

  let test_100() =
    let b = create_memory_zinvcol_builder 100 100 sp col3 in
    build_add b [ "AA0" ] Zset.empty;
    build_add b [ "AA0"; "AB1" ] Zset.empty;
    build_add b [ "AA0"; "AB2" ] Zset.empty;
    build_add b [ "AA1" ] Zset.empty;
    build_add b [ "AA2" ] Zset.empty;
    build_add b [ "AA2"; "AB1" ] Zset.empty;
    build_add b [ "AA2"; "AB1"; "AB2" ] Zset.empty;
    let c = build_end b in
    named "find-GE-AA" (find c GE ["AA"] = 0) &&&
    named "find-GE-AA0-AB2" (find c GE ["AA0";"AB2"] = 2) &&&
    named "find-GE-AA0-AB1A" (find c GE ["AA0";"AB1A"] = 2) &&&
    named "find-GE-AA0A" (find c GE ["AA0A"; "AA"] = 3) &&&

    named "find-GT-AA" (find c GT ["AA"] = 0) &&&
    named "find-GT-AA0-AB2" (find c GT ["AA0";"AB2"] = 3) &&&
    named "find-GT-AA0-AB3" (find c GT ["AA0";"AB3"] = 3) &&&
    named "find-GT-AA0-AB1A" (find c GT ["AA0";"AB1A"] = 2) &&&
    named "find-GT-AA0A" (find c GT ["AA0A"; "AA"] = 3) &&&
    named "find-GT-AA2-AB0" (find c GT ["AA2";"AB0"] = 5) &&&
    named "find-GT-AA3" (try ignore(find c GT ["AA3"]); false
                         with Not_found -> true) &&&

    named "find-LE-AA" (try ignore(find c LE ["AA"]); false
                        with Not_found -> true) &&&
    named "find-LE-AA0-AB2" (find c LE ["AA0";"AB2"] = 2) &&&
    named "find-LE-AA0-AB1A" (find c LE ["AA0";"AB1A"] = 1) &&&
    named "find-LE-AA0A" (find c LE ["AA0A"; "AA"] = 2) &&&
    named "find-LE-AA3" (find c LE ["AA3"] = 6) &&&

    named "find-LT-AA" (try ignore(find c LT ["AA"]); false
                        with Not_found -> true) &&&
    named "find-LT-AA0-AB2" (find c LT ["AA0";"AB2"] = 1) &&&
    named "find-LT-AA0-AB1A" (find c LT ["AA0";"AB1A"] = 1) &&&
    named "find-LT-AA2-AB0" (find c LT ["AA2";"AB0"] = 4) &&&
    named "find-LT-AA0A" (find c LT ["AA0A"; "AA"] = 2) &&&
    named "find-LT-AA3" (find c LT ["AA3"] = 6) &&&

    named "find-EQ-AA" (try ignore(find c EQ ["AA"]); false
                        with Not_found -> true) &&&
    named "find-EQ-AA0-AB2" (find c EQ ["AA0";"AB2"] = 2) &&&
    named "find-EQ-AA0-AB1A" (try ignore(find c EQ ["AA0";"AB1A"]); false
                              with Not_found -> true)

  end


module Merge = struct
  let sp1 = Ztypes.create_space 20
  let sp2 = Ztypes.create_space 20
  let sp3 = Ztypes.create_space 20
  let sp = Ztypes.create_space 50

  let col1 = ZcolDescr("col1",Zstring,Inv_multibyte(Zrepr.string_mb),
                       Zrepr.string_asc)

  let col3 = ZcolDescr("col3",Zlist Zstring,
                       Inv_multibyte(Zrepr.list_mb Zstring Zrepr.string_mb),
                       Zrepr.list_ord Zstring Zrepr.string_asc)

  let zset = Zset.from_array Zset.default_pool

  let test_001() =
    let b1 = create_memory_zinvcol_builder 100 100 sp1 col1 in
    build_add b1 "AA1" (zset sp1 [| 1; 3; 5; 7; 11; 13; 17; 19 |]);
    build_add b1 "AA3" (zset sp1 [| 2; 4; 6 |]);
    build_add b1 "AA5" (zset sp1 [| 8; 9; 10 |]);
    build_add b1 "AA7" (zset sp1 [| 12; 14; 15; 16; 18; 20 |]);
    let c1 = build_end b1 in
    let b2 = create_memory_zinvcol_builder 100 100 sp2 col1 in
    build_add b2 "AA1" (zset sp2 [| 2; 4; 6 |]);
    build_add b2 "AA2" (zset sp2 [| 1; 3; 5; 7; 11; 13; 17; 19 |]);
    build_add b2 "AA5" (zset sp2 [| 8 |]);
    build_add b2 "AA6" (zset sp2 [| 9; 10 |]);
    build_add b2 "AA7" (zset sp2 [| 12; 14; 15; 16; 18; 20 |]);
    let c2 = build_end b2 in
    let b3 = create_memory_zinvcol_builder 100 100 sp3 col1 in
    build_add b3 "AA5" (zset sp3 [| 1; 2; 3; 4; 5; 6; 7; 8; 9; 10 |]);
    let c3 = build_end b3 in
    let b =  create_memory_zinvcol_builder 100 100 sp col1 in
    merge b [| c1; c2; c3 |];
    let c = build_end b in
    let d = dump c in
    d = [ "AA1", [| 1; 3; 5; 7; 11; 13; 17; 19;
                    22; 24; 26 |];
          "AA2", [| 21; 23; 25; 27; 31; 33; 37; 39 |];
          "AA3", [| 2; 4; 6 |];
          "AA5", [| 8; 9; 10;
                    28;
                    41; 42; 43; 44; 45; 46; 47; 48; 49; 50 |];
          "AA6", [| 29; 30 |];
          "AA7", [| 12; 14; 15; 16; 18; 20;
                    32; 34; 35; 36; 38; 40 |]
        ]

  let test_100() =
    let b1 = create_memory_zinvcol_builder 100 100 sp1 col3 in
    build_add b1 ["A1"] (zset sp1 [| 1; 3; 5; 7; 11; 13; 17; 19 |]);
    build_add b1 ["A1"; "B1"] (zset sp1 [| 2; 4; 6 |]);
    build_add b1 ["A1"; "B2"] (zset sp1 [| 8; 9; 10 |]);
    build_add b1 ["A7"] (zset sp1 [| 12; 14; 15; 16; 18; 20 |]);
    let c1 = build_end b1 in
    let b2 = create_memory_zinvcol_builder 100 100 sp2 col3 in
    build_add b2 ["A1"] (zset sp2 [| 2; 4; 6 |]);
    build_add b2 ["A1";"B2"] (zset sp2 [| 1; 3; 5; 7; 11; 13; 17; 19 |]);
    build_add b2 ["A5"] (zset sp2 [| 8 |]);
    build_add b2 ["A6"] (zset sp2 [| 9; 10 |]);
    build_add b2 ["A7";"B2"] (zset sp2 [| 12; 14; 15; 16; 18; 20 |]);
    let c2 = build_end b2 in
    let b3 = create_memory_zinvcol_builder 100 100 sp3 col3 in
    build_add b3 ["A7";"B1"] (zset sp3 [| 1; 2; 3; 4; 5; 6; 7; 8; 9; 10 |]);
    let c3 = build_end b3 in
    let b =  create_memory_zinvcol_builder 100 100 sp col3 in
    merge b [| c1; c2; c3 |];
    let c = build_end b in
    let d = dump c in
    d = [ ["A1"],         [| 1; 3; 5; 7; 11; 13; 17; 19;
                             22; 24; 26 |];
          ["A1";"B1"],    [| 2; 4; 6 |];
          ["A1";"B2"],    [| 8; 9; 10;
                             21; 23; 25; 27; 31; 33; 37; 39 |];
          ["A5"],         [| 28 |];
          ["A6"],         [| 29; 30 |];
          ["A7"],         [| 12; 14; 15; 16; 18; 20 |];
          ["A7";"B1"],    [| 41; 42; 43; 44; 45; 46; 47; 48; 49; 50 |];
          ["A7";"B2"],    [| 32; 34; 35; 36; 38; 40 |]
        ]
  end

module Bundle = struct
  let col1 =
    ZcolDescr("col1",
              Zbundle Zstring,
              Inv_multibyte(Zrepr.bundle_mb Zstring Zrepr.string_mb),
              Zrepr.bundle_ord Zstring Zrepr.string_asc)

  let zset = Zset.from_array Zset.default_pool

  let print d =
    List.iter
      (fun (s,ids) ->
        Printf.printf "%s: %s\n" s (String.concat "," (List.map string_of_int (Array.to_list ids)))
      )
      d;
    flush stdout

  let test_001() =
    let sp = Ztypes.create_space 5 in
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    build_add b [100, "x"; 101, "y"] (zset sp [| 1; 2; 3 |]);
    build_add b [101, "a"; 102, "p"] (zset sp [| 4 |]);
    build_add b [102, "r"] (zset sp [| 5 |]);
    let c1 = build_end b in
    let d1 = dump c1 in
    let c100 = select c1 100 in
    let d100 = dump c100 in
    let c101 = select c1 101 in
    let d101 = dump c101 in
    let c102 = select c1 102 in
    let d102 = dump c102 in
    named "dump" (d1 = [ [100, "x"; 101, "y"], [| 1;2;3 |];
                         [101, "a"; 102, "p"], [| 4 |];
                         [102, "r"], [| 5 |] ]) &&
    named "col100" (d100 = [ "", [| 4;5 |];
                             "x", [| 1;2;3 |] ]) &&
    named "col101" (d101 = [ "", [| 5 |];
                             "a", [| 4 |];
                             "y", [| 1;2;3 |] ]) &&
    named "col102" (d102 = [ "", [| 1;2;3 |];
                             "p", [| 4 |];
                             "r", [| 5 |] ]) &&
    named "col100_values_3" (value_of_id c100 3 = "x") &&
    named "col100_values_4" (value_of_id c100 4 = "") &&
    named "col101_values_3" (value_of_id c101 3 = "y") &&
    named "col101_values_4" (value_of_id c101 4 = "a") &&
    named "col102_values_3" (value_of_id c102 3 = "") &&
    named "col102_values_4" (value_of_id c102 4 = "p")

  let test_002() =
    (* same data as in test_001: *)
    let sp = Ztypes.create_space 5 in
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    build_add b [100, "x"; 101, "y"] (zset sp [| 1; 2; 3 |]);
    build_add b [101, "a"; 102, "p"] (zset sp [| 4 |]);
    build_add b [102, "r"] (zset sp [| 5 |]);
    let c1 = build_end b in
    let c102 = select c1 102 in
    named "EQ-p" (find c102 EQ "p" = 1) &&&
    named "EQ-E" (find c102 EQ "" = 0) &&&
    named "GE-p" (find c102 GE "p" = 1) &&&
    named "GE-p1" (find c102 GE "p1" = 2) &&&
    named "GE-E" (find c102 GE "" = 0) &&&
    named "GE-q1" (find c102 GE "q1" = 2) &&&
    named "GE-r1" (try ignore(find c102 GE "r1"); false
                   with Not_found -> true) &&&
    named "GT-p" (find c102 GT "p" = 2) &&&
    named "GT-a" (find c102 GT "a" = 1) &&&
    named "LE-r" (find c102 LE "r" = 2) &&&
    named "LE-q1" (find c102 LE "q1" = 1) &&&
    named "LT-r" (find c102 LT "r" = 1) &&&
    named "LT-q1" (find c102 LT "q1" = 1) &&&
    named "NE-p" (find c102 NE "p" <> 1) &&&
    named "NE-E" (find c102 NE "" <> 0)

  let test_003() =
    (* same data as in test_001: *)
    let sp = Ztypes.create_space 5 in
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    build_add b [100, "x"; 101, "y"] (zset sp [| 1; 2; 3 |]);
    build_add b [101, "a"; 102, "p"] (zset sp [| 4 |]);
    build_add b [102, "r"] (zset sp [| 5 |]);
    let c1 = build_end b in
    let c102 = select c1 102 in
    let d102 = dump c102 in
    named "col102" (d102 = [ "", [| 1;2;3 |];
                             "p", [| 4 |];
                             "r", [| 5 |] ]) &&
    named "col102_values_3" (value_of_id c102 3 = "") &&
    named "col102_values_4" (value_of_id c102 4 = "p")

(*
  let test_004() =
    (* same data as in test_001: *)
    let sp = Ztypes.create_space 5 in
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    build_add b [100, "x"; 101, "y"] (zset sp [| 1; 2; 3 |]);
    build_add b [101, "a"; 102, "p"] (zset sp [| 4 |]);
    build_add b [102, "r"] (zset sp [| 5 |]);
    let c1 = build_end b in
    let c102 = select c1 102 in
    named "EQ-p" (find c102 EQ "p" = 0) &&&
    named "EQ-q" (try ignore(find c102 EQ "q"); false
                  with Not_found -> true) &&&
    named "GE-p" (find c102 GE "p" = 0)  &&&
    named "GE-p1" (find c102 GE "p1" = 1) &&&
    named "GE-q" (find c102 GE "q" = 1) &&&
    named "GE-q1" (find c102 GE "q1" = 1) &&&
    named "GE-r1" (try ignore(find c102 GE "r1"); false
                   with Not_found -> true) &&&
    named "GT-p" (find c102 GT "p" = 1) &&&
    named "GT-a" (find c102 GT "a" = 0) &&&
    named "LE-r" (find c102 LE "r" = 1) &&&
    named "LE-q1" (find c102 LE "q1" = 0) &&&
    named "LT-r" (find c102 LT "r" = 0) &&&
    named "LT-q1" (find c102 LT "q1" = 0) &&&
    named "NE-p" (find c102 NE "p" <> 0) &&&
    named "NE-q" (try ignore(find c102 NE "q"); true
                  with Not_found -> false)
 *)

    let test_005() =
      let sp = Ztypes.create_space 10 in
      let b = create_memory_zinvcol_builder 100 100 sp col1 in
      build_add b [100, "a"; 102, "s" ] (zset sp [| 7 |]);
      build_add b [100, "x"; 101, "y"] (zset sp [| 1; 2; 3 |]);
      build_add b [101, "a"; 102, "p"] (zset sp [| 4;6 |]);
      build_add b [101, "b"; 102, "g" ] (zset sp [| 8;9 |]);
      build_add b [102, "r"] (zset sp [| 5 |]);
      let c1 = build_end b in
      let c102 = select c1 102 in
      let flt = zset sp [| 2;3;5;6;9;10 |] in
      let itr = iter c102 flt in
      let l = ref [] in
      while not itr#beyond do
        let (_,v) = itr#current_value in
        let s = itr#current_zset Zset.default_pool in
        l := (v, Zset.to_array s) :: !l;
        itr # next()
      done;
      l := List.rev !l;
      !l = [ "", [| 2; 3; 10 |];
             "g", [| 9 |];
             "p", [| 6 |];
             "r", [| 5 |];
           ]

    let test_006() =
      (* Same data as for test_005 *)
      let sp = Ztypes.create_space 10 in
      let b = create_memory_zinvcol_builder 100 100 sp col1 in
      build_add b [100, "a"; 102, "s" ] (zset sp [| 7 |]);
      build_add b [100, "x"; 101, "y"] (zset sp [| 1; 2; 3 |]);
      build_add b [101, "a"; 102, "p"] (zset sp [| 4;6 |]);
      build_add b [101, "b"; 102, "g" ] (zset sp [| 8;9 |]);
      build_add b [102, "r"] (zset sp [| 5 |]);
      let c1 = build_end b in
      let c102 = select c1 102 in
      let flt = zset sp [| 2;3;5;6;9;10 |] in
      let itr = iter c102 flt in
      let check cmp x some_p =
        try
          itr # find cmp x;
          let p = itr # current_pos in
          some_p = Some p
        with
          | Not_found -> some_p = None in
      named "EQ-p" (check EQ "p" (Some 2)) &&&
      named "EQ-E" (check EQ "" (Some 0)) &&&
      named "EQ-s" (check EQ "s" None) &&&
      named "GE-p" (check GE "p" (Some 2)) &&&
      named "GE-E" (check GE "" (Some 0)) &&&
      named "GE-s" (check GE "s" None) &&&
      named "GT-p" (check GT "p" (Some 3)) &&&
      named "GT-E" (check GT "" (Some 1)) &&&
      named "GT-s" (check GT "s" None) &&&
      named "LE-p" (check LE "p" (Some 2)) &&&
      named "LE-E" (check LE "" (Some 0)) &&&
      named "LE-s" (check LE "s" (Some 3)) &&&
      named "LT-p" (check LT "p" (Some 1)) &&&
      named "LT-E" (check LT "" None) &&&
      named "LT-s" (check LT "s" (Some 3))

  (* testing bundle indices > 8191 *)

  let test_101() =
    let sp = Ztypes.create_space 5 in
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    build_add b [50100, "x"; 50101, "y"] (zset sp [| 1; 2; 3 |]);
    build_add b [50101, "a"; 50102, "p"] (zset sp [| 4 |]);
    build_add b [50102, "r"] (zset sp [| 5 |]);
    let c1 = build_end b in
    let d1 = dump c1 in
    let c100 = select c1 50100 in
    let d100 = dump c100 in
    let c101 = select c1 50101 in
    let d101 = dump c101 in
    let c102 = select c1 50102 in
    let d102 = dump c102 in
    named "dump" (d1 = [ [50100, "x"; 50101, "y"], [| 1;2;3 |];
                         [50101, "a"; 50102, "p"], [| 4 |];
                         [50102, "r"], [| 5 |] ]) &&
    named "col100" (d100 = [ "", [| 4;5 |];
                             "x", [| 1;2;3 |] ]) &&
    named "col101" (d101 = [ "", [| 5 |];
                             "a", [| 4 |];
                             "y", [| 1;2;3 |] ]) &&
    named "col102" (d102 = [ "", [| 1;2;3 |];
                             "p", [| 4 |];
                             "r", [| 5 |] ]) &&
    named "col100_values_3" (value_of_id c100 3 = "x") &&
    named "col100_values_4" (value_of_id c100 4 = "") &&
    named "col101_values_3" (value_of_id c101 3 = "y") &&
    named "col101_values_4" (value_of_id c101 4 = "a") &&
    named "col102_values_3" (value_of_id c102 3 = "") &&
    named "col102_values_4" (value_of_id c102 4 = "p")

  let test_102() =
    (* same data as in test_001: *)
    let sp = Ztypes.create_space 5 in
    let b = create_memory_zinvcol_builder 100 100 sp col1 in
    build_add b [50100, "x"; 50101, "y"] (zset sp [| 1; 2; 3 |]);
    build_add b [50101, "a"; 50102, "p"] (zset sp [| 4 |]);
    build_add b [50102, "r"] (zset sp [| 5 |]);
    let c1 = build_end b in
    let c102 = select c1 50102 in
    named "EQ-p" (find c102 EQ "p" = 1) &&&
    named "EQ-E" (find c102 EQ "" = 0) &&&
    named "GE-p" (find c102 GE "p" = 1) &&&
    named "GE-p1" (find c102 GE "p1" = 2) &&&
    named "GE-E" (find c102 GE "" = 0) &&&
    named "GE-q1" (find c102 GE "q1" = 2) &&&
    named "GE-r1" (try ignore(find c102 GE "r1"); false
                   with Not_found -> true) &&&
    named "GT-p" (find c102 GT "p" = 2) &&&
    named "GT-a" (find c102 GT "a" = 1) &&&
    named "LE-r" (find c102 LE "r" = 2) &&&
    named "LE-q1" (find c102 LE "q1" = 1) &&&
    named "LT-r" (find c102 LT "r" = 1) &&&
    named "LT-q1" (find c102 LT "q1" = 1) &&&
    named "NE-p" (find c102 NE "p" <> 1) &&&
    named "NE-E" (find c102 NE "" <> 0)
end


module Merge_bundle = struct
  let col1 =
    ZcolDescr("col1",
              Zbundle Zstring,
              Inv_multibyte(Zrepr.bundle_mb Zstring Zrepr.string_mb),
              Zrepr.bundle_ord Zstring Zrepr.string_asc)

  let zset = Zset.from_array Zset.default_pool

  let print1 d =
    List.iter
      (fun (l,ids) ->
        Printf.printf
          "[%s]: %s\n"
          (String.concat "; "
            (List.map (fun (id,s) -> Printf.sprintf "(%d,%s)" id s) l))
          (String.concat ","
            (List.map string_of_int (Array.to_list ids)))
      )
      d;
    flush stdout

  let print2 d =
    List.iter
      (fun (s,ids) ->
        Printf.printf "%s: %s\n" s (String.concat "," (List.map string_of_int (Array.to_list ids)))
      )
      d;
    flush stdout
          
  let test_001() =
    let sp1 = Ztypes.create_space 5 in
    let b1 = create_memory_zinvcol_builder 100 100 sp1 col1 in
    build_add b1 [100, "x"; 101, "y"] (zset sp1 [| 1; 2; 3 |]);
    build_add b1 [101, "a"; 102, "p"] (zset sp1 [| 4 |]);
    build_add b1 [102, "r"] (zset sp1 [| 5 |]);
    let c1 = build_end b1 in
    let sp2 = Ztypes.create_space 5 in
    let b2 = create_memory_zinvcol_builder 100 100 sp2 col1 in
    build_add b2 [100, "X"; 101, "y"; 103, "m"] (zset sp2 [| 1; 2; |]);
    build_add b2 [101, "a"; 102, "P"] (zset sp2 [| 4 |]);
    build_add b2 [102, "R"; 103, "n"] (zset sp2 [| 3; 5 |]);
    let c2 = build_end b2 in
    let sp3 = Ztypes.create_space 10 in
    let b3 =  create_memory_zinvcol_builder 100 100 sp3 col1 in
    merge b3 [| c1; c2 |];
    let c3 = build_end b3 in
    let d3 = dump c3 in
    let c101 = select c3 101 in
    let d101 = dump c101 in
    let c103 = select c3 103 in
    let d103 = dump c103 in
    named "d3"
          (d3 = [ [ 100, "X"; 101, "y"; 103, "m" ], [| 6; 7 |];
                  [ 100, "x"; 101, "y" ], [| 1; 2; 3 |];
                  [ 101, "a"; 102, "P" ], [| 9 |];
                  [ 101, "a"; 102, "p" ], [| 4 |];
                  [ 102, "R"; 103, "n" ], [| 8; 10 |];
                  [ 102, "r" ], [| 5 |]
          ]) &&&
    named "d101"
          (d101 = [ "", [| 5; 8; 10 |];
                    "a", [| 4; 9 |];
                    "y", [| 1; 2; 3; 6; 7 |]
          ]) &&&
    named "d103"
          (d103 = [ "", [| 1; 2; 3; 4; 5; 9 |];
                    "m", [| 6; 7 |];
                    "n", [| 8; 10 |]
                  ])

  let test_002() =
    (* tests the frequent case that the last input is incomplete *)
    let sp1 = Ztypes.create_space 5 in
    let b1 = create_memory_zinvcol_builder 100 100 sp1 col1 in
    build_add b1 [100, "x"; 101, "y"] (zset sp1 [| 1; 2; 3 |]);
    build_add b1 [101, "a"; 102, "p"] (zset sp1 [| 4 |]);
    build_add b1 [102, "r"] (zset sp1 [| 5 |]);
    let c1 = build_end b1 in
    let sp2 = Ztypes.create_space 5 in
    let b2 = create_memory_zinvcol_builder 100 100 sp2 col1 in
    build_add b2 [100, "X"; 101, "y"; 103, "m"] (zset sp2 [| 1; 2; |]);
    build_add b2 [101, "a"; 102, "P"] (zset sp2 [| 3 |]);
    let c2 = build_end b2 in
    let sp3 = Ztypes.create_space 8 in
    let b3 =  create_memory_zinvcol_builder 100 100 sp3 col1 in
    merge b3 [| c1; c2 |];
    let c3 = build_end b3 in
    let d3 = dump c3 in
    let c101 = select c3 101 in
    let d101 = dump c101 in
    let c103 = select c3 103 in
    let d103 = dump c103 in
    named "d3"
          (d3 = [ [ 100, "X"; 101, "y"; 103, "m" ], [| 6; 7 |];
                  [ 100, "x"; 101, "y" ], [| 1; 2; 3 |];
                  [ 101, "a"; 102, "P" ], [| 8 |];
                  [ 101, "a"; 102, "p" ], [| 4 |];
                  [ 102, "r" ], [| 5 |]
          ]) &&&
    named "d101"
          (d101 = [ "", [| 5 |];
                    "a", [| 4; 8 |];
                    "y", [| 1; 2; 3; 6; 7 |]
          ]) &&&
    named "d103"
          (d103 = [ "", [| 1; 2; 3; 4; 5; 8 |];
                    "m", [| 6; 7 |];
                  ])


end

let tests =
  [ ("Build.test_001", Build.test_001);
    ("Build.test_010", Build.test_010);
    ("Build.test_011", Build.test_011);
    ("Build.test_012", Build.test_012);
    ("Build.test_020", Build.test_020);
    ("Build.test_021", Build.test_021);
    ("Build.test_022", Build.test_022);

    ("Build.test_100", Build.test_100);
    ("Build.test_110", Build.test_110);
    ("Build.test_111", Build.test_111);
    ("Build.test_112", Build.test_112);
    ("Build.test_113", Build.test_113);
    ("Build.test_114", Build.test_114);

    ("Build.test_200", Build.test_200);
    ("Build.test_201", Build.test_201);

    ("Find.test_001", Find.test_001);
    ("Find.test_100", Find.test_100);

    ("Merge.test_001", Merge.test_001);
    ("Merge.test_100", Merge.test_100);

    ("Bundle.test_001", Bundle.test_001);
    ("Bundle.test_002", Bundle.test_002);
    ("Bundle.test_003", Bundle.test_003);
    (* ("Bundle.test_004", Bundle.test_004); *)
    ("Bundle.test_005", Bundle.test_005);
    ("Bundle.test_006", Bundle.test_006);
    ("Bundle.test_101", Bundle.test_101);
    ("Bundle.test_102", Bundle.test_102);

    ("Merge_bundle.test_001", Merge_bundle.test_001);
    ("Merge_bundle.test_002", Merge_bundle.test_002);
  ]
