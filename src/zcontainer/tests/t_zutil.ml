open Zcontainer
open Zutil
open Ztypes
open T_frame.Util


module TBinSearch = struct
  module OU = Zutil.OrdUtil(String)
  let test_001() =
    (* one element array *)
    let data = [| "m" |] in
    let get k = data.(k) in
    let n = Array.length data in
    named "EQ-a" (try ignore(OU.binary_search get n EQ "a"); false
                  with Not_found -> true) &&&
    named "EQ-m" (OU.binary_search get n EQ "m" = 0) &&&
    named "EQ-z" (try ignore(OU.binary_search get n EQ "z"); false
                  with Not_found -> true) &&&
    named "LE-a" (try ignore(OU.binary_search get n LE "a"); false
                  with Not_found -> true) &&&
    named "LE-m" (OU.binary_search get n LE "m" = 0) &&&
    named "LE-z" (OU.binary_search get n LE "z" = 0) &&&
    named "LT-a" (try ignore(OU.binary_search get n LT "a"); false
                  with Not_found -> true) &&&
    named "LT-m" (try ignore(OU.binary_search get n LT "m"); false
                  with Not_found -> true) &&&
    named "LT-z" (OU.binary_search get n LT "z" = 0) &&&
    named "GE-a" (OU.binary_search get n GE "a" = 0) &&&
    named "GE-m" (OU.binary_search get n GE "m" = 0) &&&
    named "GE-z" (try ignore(OU.binary_search get n GE "z"); false
                  with Not_found -> true) &&&
    named "GT-a" (OU.binary_search get n GT "a" = 0) &&&
    named "GT-m" (try ignore(OU.binary_search get n GT "m"); false
                  with Not_found -> true) &&&
    named "GT-z" (try ignore(OU.binary_search get n GT "z"); false
                  with Not_found -> true) &&&
    named "NE-a" (OU.binary_search get n NE "a" = 0) &&&
    named "NE-m" (try ignore(OU.binary_search get n NE "m"); false
                  with Not_found -> true) &&&
    named "NE-z" (OU.binary_search get n NE "z" = 0)

  let test_002() =
    (* two element array *)
    let data = [| "m"; "n" |] in
    let get k = data.(k) in
    let n = Array.length data in
    named "EQ-a" (try ignore(OU.binary_search get n EQ "a"); false
                  with Not_found -> true) &&&
    named "EQ-m" (OU.binary_search get n EQ "m" = 0) &&&
    named "EQ-n" (OU.binary_search get n EQ "n" = 1) &&&
    named "EQ-z" (try ignore(OU.binary_search get n EQ "z"); false
                  with Not_found -> true) &&&
    named "LE-a" (try ignore(OU.binary_search get n LE "a"); false
                  with Not_found -> true) &&&
    named "LE-m" (OU.binary_search get n LE "m" = 0) &&&
    named "LE-n" (OU.binary_search get n LE "n" = 1) &&&
    named "LE-z" (OU.binary_search get n LE "z" = 1) &&&
    named "LT-a" (try ignore(OU.binary_search get n LT "a"); false
                  with Not_found -> true) &&&
    named "LT-m" (try ignore(OU.binary_search get n LT "m"); false
                  with Not_found -> true) &&&
    named "LT-n" (OU.binary_search get n LT "n" = 0) &&&
    named "LT-z" (OU.binary_search get n LT "z" = 1) &&&
    named "GE-a" (OU.binary_search get n GE "a" = 0) &&&
    named "GE-m" (OU.binary_search get n GE "m" = 0) &&&
    named "GE-n" (OU.binary_search get n GE "n" = 1) &&&
    named "GE-z" (try ignore(OU.binary_search get n GE "z"); false
                  with Not_found -> true) &&&
    named "GT-a" (OU.binary_search get n GT "a" = 0) &&&
    named "GT-m" (OU.binary_search get n GT "m" = 1) &&&
    named "GT-n" (try ignore(OU.binary_search get n GT "n"); false
                  with Not_found -> true) &&&
    named "GT-z" (try ignore(OU.binary_search get n GT "z"); false
                  with Not_found -> true) &&&
    named "NE-a" (let p = OU.binary_search get n NE "a" in p>=0 && p<=1) &&&
    named "NE-m" (OU.binary_search get n NE "m" = 1) &&&
    named "NE-n" (OU.binary_search get n NE "n" = 0) &&&
    named "NE-z" (let p = OU.binary_search get n NE "z" in p>=0 && p<=1)

  let test_003() =
    (* systematic test for n >= 3 *)
    let ok = ref true in
    for n = 3 to 20 do
      let data = Array.init n (fun k -> Printf.sprintf "%03d" (2*k+1)) in
      let get k = data.(k) in
      for k = 0 to 2*n do
        let ks = Printf.sprintf "%03d" k in
        let p = Printf.sprintf "-%03d/%03d" k n in
        ok := !ok &&
          named ("EQ" ^ p)
                (if k mod 2 = 1 then
                   try OU.binary_search get n EQ ks = k/2
                   with Not_found -> false
                 else
                   try ignore(OU.binary_search get n EQ ks); false
                   with Not_found -> true);
        ok := !ok &&
          named ("GE" ^ p)
                (if k <> 2*n then
                   try OU.binary_search get n GE ks = k/2
                   with Not_found -> false
                 else
                   try ignore(OU.binary_search get n GE ks); false
                   with Not_found -> true);
        ok := !ok &&
          named ("GT" ^ p)
                (if k < 2*n-1 then
                   try OU.binary_search get n GT ks = (k+1)/2
                   with Not_found -> false
                 else
                   try ignore(OU.binary_search get n GT ks); false
                   with Not_found -> true);
        ok := !ok &&
          named ("LE" ^ p)
                (if k > 0 then
                   try OU.binary_search get n LE ks = (k-1)/2
                   with Not_found -> false
                 else
                   try ignore(OU.binary_search get n LE ks); false
                   with Not_found -> true);
        ok := !ok &&
          named ("LT" ^ p)
                (if k > 1 then
                   try OU.binary_search get n LT ks = k/2-1
                   with Not_found -> false
                 else
                   try ignore(OU.binary_search get n LT ks); false
                   with Not_found -> true);
        ok := !ok &&
          named ("NE" ^ p)
                (try let p = OU.binary_search get n NE ks in
                     p >= 0 && p < n
                 with Not_found -> false
                )

      done
    done;
    !ok

end

module TBitset = struct
  let test_001() =
    let bs = Bitset.create 5 false in
    Bitset.fill_true bs 3 1;
    Bitset.fill_true bs 4 1;
    Bitset.set_runlengths bs;
    let l = ref [] in
    Bitset.iter (fun start len -> l := (start,len) :: !l) bs;
    l := List.rev !l;
    !l = [ 3,1; 4,1 ]

end


let tests =
  [ ("TBinSearch.test_001", TBinSearch.test_001);
    ("TBinSearch.test_002", TBinSearch.test_002);
    ("TBinSearch.test_003", TBinSearch.test_003);

    ("TBitset.test_001", TBitset.test_001);
  ]
    
