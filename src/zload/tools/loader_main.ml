open Zcontainer
open Zload
open Printf

let main() =
  let data_files = ref [] in
  let header_file = ref None in
  let data_only = ref false in
  let load_script = ref None in
  let max_stage_rows = ref 1_000_000 in
  let out = ref None in

  Arg.parse
    [ "-out", Arg.String (fun s -> out := Some s),
      "<name>   Write the output into this zcontainer directory";

      "-load-script", Arg.String (fun s -> load_script := Some s),
      "<file>   Run this load script";

      "-header", Arg.String (fun s -> header_file := Some s),
      "<file>   Read the header from this file";

      "-data-only", Arg.Set data_only,
      "    The data files do not contain a header";

      "-max-stage-rows", Arg.Set_int max_stage_rows,
      "<n>    The number of rows in the temporary staging zcontainers";
    ]
    (fun s -> data_files := s :: !data_files)
    "usage: loader [options] file.arff ...";

  data_files := List.rev !data_files;

  let header =
    match !header_file with
      | None -> failwith "No -header"
      | Some header_file ->
          let f = open_in header_file in
          let h = Arff.header_of_channel ~expect_data:false f in
          close_in f;
          h in
  let hdr_atts =
    List.map (fun (p,n,_) -> (p,n)) Arff.(header.attributes) in
  
  let load_spec =
    match !load_script with
      | None -> failwith "No -load-script"
      | Some file ->
          let f = open_in file in
          let s = Loader.load_spec_of_channel f in
          close_in f;
          s in

  let dg =
    match !out with
      | None -> failwith "No -out"
      | Some file ->
          Zdigester.create_file_zdigester
            ~max_stage_rows:!max_stage_rows
            Zrepr.stdrepr
            (Loader.digester_spec_of_load_spec load_spec)
            file in
  try
    let n = ref 0 in
    let ld =
      Loader.load dg load_spec hdr_atts in
    List.iter
      (fun file ->
         let f = open_in file in
         let lexbuf = Lexing.from_channel f in
         (* Fix up filename: *)
         let lexbuf =
           let open Lexing in
           { lexbuf with
             lex_curr_p =
               { lexbuf.lex_curr_p with pos_fname = file }
           } in
         if not !data_only then (
           (* skip over the header *)
           ignore(Arff.header_of_lexbuf ~expect_data:true lexbuf);
         );
         Arff.data_of_lexbuf
           (fun fields ->
              Loader.add ld fields;
              incr n
           )
           header
           lexbuf;
         close_in f
      )
      !data_files;
    ignore(Zdigester.build_end dg);
    printf "Digested %d rows\n%!" !n
  with
    | error ->
        let bt = Printexc.get_backtrace() in
        eprintf "Backtrace: %s\n" bt;
        Zdigester.build_abort dg;
        raise error


let () =
  Printexc.record_backtrace true;
  try
    main()
  with
    | error ->
        let bt = Printexc.get_backtrace() in
        eprintf "Exception: %s\n" (Printexc.to_string error);
        eprintf "Backtrace: %s\n" bt;
        exit 2
