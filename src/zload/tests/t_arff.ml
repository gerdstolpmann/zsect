open Printf
open T_frame.Util
open Zload.Arff

let print_items items =
  printf "{";
  List.iter
    (function
      | (n,None) -> printf " %d=None" n
      | (n,Some s) -> printf "%d=Some(%S)" n s
    )
    items;
  printf "}"


let test_001() =
  let data = 
{|@relation test
@attribute a1 integer
@attribute "a2" string
@attribute a3 NUMERIC
@ATTRibute 'a4' {"1",2,3,"five"}
|} in
  let h, n = header_of_string data in
  named "n" (n = String.length data) &&
    named "rel" (h.relation = "test") &&
      named "atts" (h.attributes =
                      [ 0, "a1", Int;
                        1, "a2", String;
                        2, "a3", Numeric;
                        3, "a4", Nominal [ "1"; "2"; "3"; "five" ]
                      ])

let test_002() =
  let data = 
{|@relation test
@attribute a1 integer
@attribute "a2" string
@attribute a3 NUMERIC
@ATTRibute 'a4' {"1",2,3,"five"}

@data
|} in
  let h, n = header_of_string ~expect_data:true (data ^ "foobar\n") in
  named "n" (n = String.length data) &&
    named "rel" (h.relation = "test") &&
      named "atts" (h.attributes =
                      [ 0, "a1", Int;
                        1, "a2", String;
                        2, "a3", Numeric;
                        3, "a4", Nominal [ "1"; "2"; "3"; "five" ]
                      ])

let test_003() =
  let data = 
{|@relation test
@attribute a1@0 integer
@attribute a3@2 string
@attribute @3..@4 NUMERIC
@ATTRibute 'q'@5..@6 {"1",2,3,"five"}
|} in
  let h, n = header_of_string data in
  named "n" (n = String.length data) &&
    named "rel" (h.relation = "test") &&
      named "atts" (h.attributes =
                      [ 0, "a1", Int;
                        2, "a3", String;
                        3, "@3", Numeric;
                        4, "@4", Numeric;
                        5, "q@5", Nominal [ "1"; "2"; "3"; "five" ];
                        6, "q@6", Nominal [ "1"; "2"; "3"; "five" ];
                      ])


let test_100() =
  let data =
{|100,200,300
"100","200","300"
"100,200","300",?
|} in
  let h =
    { relation = "test";
      attributes = [ 0, "x", String;
                     1, "y", String;
                     2, "z", String ];
      attribute_ranges = [];
    } in
  let k = ref 1 in
  data_of_string
    (fun items ->
       let ok =
         match !k with
           | 1 -> items = [ 2, Some "300"; 1, Some "200"; 0, Some "100" ]
           | 2 -> items = [ 2, Some "300"; 1, Some "200"; 0, Some "100" ]
           | 3 -> items = [ 2, None; 1, Some "300"; 0, Some "100,200" ]
           | _ -> false in
       if not ok then (
         print_items items;
         failwith (sprintf "failed: %d" !k);
       );
       printf "[%d ok] %!" !k;
       incr k
    )
    h
    data;
  true


let test_101() =
  let data =
{|100,200,300
"100",'200',"300"
"100,\"200\"","300"
|} in
  let h =
    { relation = "test";
      attributes = [ 0, "x", String;
                     1, "y", String;
                     2, "z", String ];
      attribute_ranges = [];
    } in
  let k = ref 1 in
  data_of_string
    (fun items ->
       let ok =
         match !k with
           | 1 -> items = [ 2, Some "300"; 1, Some "200"; 0, Some "100" ]
           | 2 -> items = [ 2, Some "300"; 1, Some "200"; 0, Some "100" ]
           | 3 -> items = [ 1, Some "300"; 0, Some "100,\"200\"" ]
           | _ -> false in
       if not ok then (
         print_items items;
         failwith (sprintf "failed: %d" !k);
       );
       printf "[%d ok] %!" !k;
       incr k
    )
    h
    data;
  true


let test_102() =
  (* same with ranges *)
  let data =
{|100,200,300
"100",'200',"300"
"100,\"200\"","300"
|} in
  let h =
    { relation = "test";
      attributes = [];
      attribute_ranges = [ (0,2,"x",String) ]
    } in
  let k = ref 1 in
  data_of_string
    (fun items ->
       let ok =
         match !k with
           | 1 -> items = [ 2, Some "300"; 1, Some "200"; 0, Some "100" ]
           | 2 -> items = [ 2, Some "300"; 1, Some "200"; 0, Some "100" ]
           | 3 -> items = [ 1, Some "300"; 0, Some "100,\"200\"" ]
           | _ -> false in
       if not ok then (
         print_items items;
         failwith (sprintf "failed: %d" !k);
       );
       printf "[%d ok] %!" !k;
       incr k
    )
    h
    data;
  true
    

let test_200() =
  let data =
{|{ 0 100, 1 200, 2 300}
{0 "100",1 "200", 2"300"}
{0 "100,200",1"300",2 ?}
|} in
  let h =
    { relation = "test";
      attributes = [ 0, "x", String;
                     1, "y", String;
                     2, "z", String ];
      attribute_ranges = [];
    } in
  let k = ref 1 in
  data_of_string
    (fun items ->
       let ok =
         match !k with
           | 1 -> items = [ 2, Some "300"; 1, Some "200"; 0, Some "100" ]
           | 2 -> items = [ 2, Some "300"; 1, Some "200"; 0, Some "100" ]
           | 3 -> items = [ 2, None; 1, Some "300"; 0, Some "100,200" ]
           | _ -> false in
       if not ok then (
         print_items items;
         failwith (sprintf "failed: %d" !k);
       );
       printf "[%d ok] %!" !k;
       incr k
    )
    h
    data;
  true


let test_201() =
  let data =
{|{ 2 300}
{1 "200"}
{0 "100,200"}
|} in
  let h =
    { relation = "test";
      attributes = [ 0, "x", String;
                     1, "y", String;
                     2, "z", String ];
      attribute_ranges = [];
    } in
  let k = ref 1 in
  data_of_string
    (fun items ->
       let ok =
         match !k with
           | 1 -> items = [ 2, Some "300" ]
           | 2 -> items = [ 1, Some "200" ]
           | 3 -> items = [ 0, Some "100,200" ]
           | _ -> false in
       if not ok then (
         print_items items;
         failwith (sprintf "failed: %d" !k);
       );
       printf "[%d ok] %!" !k;
       incr k
    )
    h
    data;
  true


let tests =
  [ ("test_001", test_001);
    ("test_002", test_002);
    ("test_003", test_003);
    ("test_100", test_100);
    ("test_101", test_101);
    ("test_102", test_102);
    ("test_200", test_200);
    ("test_201", test_201);
  ]
