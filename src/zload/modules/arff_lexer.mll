{
type header_token =
 | H_attribute
 | H_relation
 | H_end
 | H_data
 | H_name of string
 | H_Lbrace
 | H_Rbrace
 | H_comma
 | H_newline
 | H_at of int
 | H_range of int * int
 | H_eof

type data_token =
 | D_quoted of string
 | D_unquoted of string
 | D_missing
 | D_fieldsep
 | D_recsep
 | D_space
 | D_Lbrace
 | D_Rbrace
 | D_eof

exception Scanner_error of Lexing.position * string

let scanner_error lexbuf msg =
  let pos = Lexing.lexeme_start_p lexbuf in
  raise(Scanner_error(pos,msg))
}


rule header =
  parse
  | '@' ([ 'a'-'z' 'A'-'Z' ]+ as s)
     { match String.lowercase s with
         | "attribute" -> H_attribute
         | "relation" -> H_relation
         | "end" -> H_end
         | "data" -> H_data
         | _ -> scanner_error lexbuf ("Unknown token: " ^ Lexing.lexeme lexbuf)
     }
  | '@' ( [ '0'-'9' ]+ as n )
     { H_at (int_of_string n) }
  | '@' ( [ '0'-'9' ]+ as n1 ) ".." '@' ( [ '0'-'9' ]+ as n2 )
     { H_range (int_of_string n1, int_of_string n2) }
  | [^ '{' '}' ',' '%' '"' '\'' ' ' '@' '\000'-'\032']+ as s
     { H_name s }
  | '"' ([ ^ '"' '\000'-'\031' ]+ as s) '"'
     { H_name s }
  | '\'' ([ ^ '\'' '\000'-'\031' ]+ as s) '\''
     { H_name s }
  | '{'
     { H_Lbrace }
  | '}'
     { H_Rbrace }
  | ','
     { H_comma }
  | '%' [^ '\n']* '\n'
     { Lexing.new_line lexbuf; H_newline }
  | '\r'? '\n'
     { Lexing.new_line lexbuf; H_newline }
  | [ ' ' '\t' ]+
     { header lexbuf }
  | eof
     { H_eof }
  | _ as c
     { scanner_error lexbuf ("Unknown character: '" ^ String.make 1 c ^ "'") }

and string_lit delim b =
  parse
  | '\\' (_ as c)
     { Buffer.add_char b c; string_lit delim b lexbuf }
  | '"'
     { if delim <> '"' then ( Buffer.add_char b '"'; string_lit delim b lexbuf )
     }
  | '\''
     { if delim <> '\'' then ( Buffer.add_char b '\''; string_lit delim b lexbuf )
     }
  | [^ '\\' '\'' '"' '\n' ]+ as s
     { Buffer.add_string b s; string_lit delim b lexbuf }
  | '\n'
     { Lexing.new_line lexbuf; Buffer.add_char b '\n'; string_lit delim b lexbuf }
  | '\\'
     { scanner_error lexbuf "Bad use of backslash" }
  | eof 
     { scanner_error lexbuf "Unexpected end of file" }


and csv_data =
  parse
  | [ ',' '\t' ]
     { D_fieldsep }
  | '?'
     { D_missing }
  | '"'
     { let b = Buffer.create 80 in
       string_lit '"' b lexbuf;
       D_quoted (Buffer.contents b)
     }
  | '\''
     { let b = Buffer.create 80 in
       string_lit '\'' b lexbuf;
       D_quoted (Buffer.contents b)
     }
  | [^ ',' '?' '%' '"' '\'' '\t' ' ' '\r' '\n'] [^ ',' '%' '"' '\'' '\t' ' ' '\r' '\n']* as s
     { D_unquoted s }
  | '\r'? '\n'
     { Lexing.new_line lexbuf; D_recsep }
  | [ ' ' ]+
     { D_space }
  | '%' [^ '\n']* '\n'
     { Lexing.new_line lexbuf; D_recsep }
  | eof
     { D_eof }
  | _ as c
     { scanner_error lexbuf ("Unknown character: '" ^ String.make 1 c ^ "'") }


and sparse_data =
  parse
  | [ ',' '\t' ]
     { D_fieldsep }
  | '?'
     { D_missing }
  | '"'
     { let b = Buffer.create 80 in
       string_lit '"' b lexbuf;
       D_quoted (Buffer.contents b)
     }
  | '\''
     { let b = Buffer.create 80 in
       string_lit '\'' b lexbuf;
       D_quoted (Buffer.contents b)
     }
  | [^ ',' '?' '%' '"' '\'' '\t' ' ' '\r' '\n' '{' '}' ','] [^ ',' '%' '"' '\'' '\t' ' ' '\r' '\n' '{' '}' ',']* as s
     { D_unquoted s }
  | '\r'? '\n'
     { Lexing.new_line lexbuf; D_recsep }
  | [ ' ' ]+
     { D_space }
  | '%' [^ '\n']* '\n'
     { Lexing.new_line lexbuf; D_recsep }
  | '{'
     { D_Lbrace }
  | '}'
     { D_Rbrace }
  | eof
     { D_eof }
  | _ as c
     { scanner_error lexbuf ("Unknown character: '" ^ String.make 1 c ^ "'") }
