type typ =
  | Int
  | Real
  | Numeric
  | String
  | Nominal of string list
  | Date of string

type header =
  { relation : string;
    attributes : (int * string * typ) list;
    attribute_ranges : (int * int * string * typ) list;  (* large ranges only *)
  }

exception Header_syntax of string
exception Data_syntax of string

val header_of_channel : ?expect_data:bool -> in_channel -> header
  (** Parses a header. Parsing stops either at EOF or right after
      the \@data keyword (plus newline).

      If [expect_data] is set, the \@data keyword is expected.
   *)

val header_of_string : ?expect_data:bool -> string -> header * int
  (** [let (h, len) = header_of_string data]:

      Parses a header, starting at [data.[pos]]. The function returns the
      header and the number of parsed bytes.
   *)


val header_of_lexbuf : ?expect_data:bool -> Lexing.lexbuf -> header
  (** Same for lexbufs *)

val data_of_channel : ( (int*string option)list -> unit ) -> header ->
                      in_channel -> unit
  (** [data_of_channel f h ch]: Parses the data section. For every line,
      the function [f] is called back with a list of [(p,data)] pairs.
      [p] is the position of the data item. If [data] is [None] this
      means a question mark in the ARFF file. [Some s] means that there
      the data item exists in the ARFF file. Missing fields are not
      passed to [f].
   *)

val data_of_string : ( (int*string option)list -> unit ) -> header ->
                     string -> unit
  (** Same for strings *)

val data_of_lexbuf : ( (int*string option)list -> unit ) -> header ->
                     Lexing.lexbuf -> unit
  (** Same for lexbufs *)

