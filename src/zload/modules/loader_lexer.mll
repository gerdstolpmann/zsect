{
open Loader_parser
open Printf

exception Scan_error of string

let scan_error line lexbuf msg =
  let pos = Lexing.lexeme_start_p lexbuf in
  let where =
    sprintf "Line %d, position %d: " line Lexing.(pos.pos_cnum+1) in
  raise(Scan_error(where ^ msg))
}

let ws = [ ' ' '\t' ]

rule scan line =
  parse
  | [ 'a'-'z' 'A'-'Z' '_' ] [ 'a'-'z' 'A'-'Z' '_' '0'-'9' ]* as name 
      { IDENT name }
  | '@' ( ['0'-'9']+ as n )
      { POS (int_of_string n) }
  | '@' ( ['0'-'9']+ as n1 ) "..@" ( ['0'-'9']+ as n2 )
      { let k1 = int_of_string n1 in
        let k2 = int_of_string n2 in
        RANGE ((k1, k2), (k1, k2))
      }
  | '@' ( ['0'-'9']+ as n1 ) "..@" ( ['0'-'9']+ as n2 ) ws+ "AS" ws+
    '@' ( ['0'-'9']+ as n3 ) "..@" ( ['0'-'9']+ as n4 )
      { let k1 = int_of_string n1 in
        let k2 = int_of_string n2 in
        let k3 = int_of_string n3 in
        let k4 = int_of_string n4 in
        if k2-k1 <> k4-k3 then
          scan_error line lexbuf "The range on the left side of AS has a different width than the range on the right side";
        RANGE ((k1, k2), (k3, k4))
      }
  | ['+' '-']? ['0'-'9']+ as n
      { INT (Int64.of_string n) }
  | ['+' '-']? ['0'-'9']+ ('.' ['0'-'9']*)? ['E' 'e'] ['+' '-']? ['0'-'9']+ as n
      { FLOAT (float_of_string n) }
  | ['+' '-']? ['0'-'9']+ '.' ['0'-'9']* as n
      { FLOAT (float_of_string n) }
  | ['+' '-']? ['0'-'9']* '.' ['0'-'9']+ as n
      { FLOAT (float_of_string n) }
  | '"'
      { let b = Buffer.create 80 in
        string_lit line b lexbuf;
        STRING(Buffer.contents b)
      }
  | ','
      { COMMA }
  | '('
      { LPAREN }
  | ')'
      { RPAREN }
  | [ ' ' '\t' '\r' ]+
      { scan line lexbuf }
  | eof
      { END }
  | _ 
      { scan_error line lexbuf "unexpected symbol" }


and string_lit line b =
  parse
  | '\\' (_ as c)
     { let c =
         match c with
           | 'n' -> '\n'
           | 'r' -> '\r'
           | 'b' -> '\b'
           | 't' -> '\t'
           | _ -> c in
       Buffer.add_char b c;
       string_lit line b lexbuf
     }
  | '"'
     { () }
  | [^ '\\' '"' '\n' ]+ as s
     { Buffer.add_string b s; string_lit line b lexbuf }
  | '\n'
     { Lexing.new_line lexbuf; Buffer.add_char b '\n'; string_lit line b lexbuf }
  | '\\'
     { scan_error line lexbuf "Bad use of backslash" }
  | eof 
     { scan_error line lexbuf "Unexpected end of file" }
