open Zcontainer
open Loader_types
open Printf

exception Parse_error of string

type load_spec_col =
  { outname : string;
    reprbox : Ztypes.zreprbox;
    expr : expr;
    parser : Zdigester.parserbox;
    default : Zdigester.foodbox;
    add : Zdigester.foodbox array -> string option -> unit;
    add_element : int -> Zdigester.foodbox array -> string option -> unit
  }

type load_spec = load_spec_col array

type csv_spec = (int * string) list

let line_re =
  Str.regexp
    "^[ \t]*\\([a-zA-Z0-9_]+\\)[ \t]*:[ \t]*\\([^=]*[^= ]\\)[ \t]*=\\(.*\\)$"

let parse_load_spec lines =
  let cols = ref [] in
  List.iteri
    (fun i line ->
       if line <> "" && line.[0] <> '#' then (
         if Str.string_match line_re line 0 then
           let name = Str.matched_group 1 line in
           let repr_s = Str.matched_group 2 line in
           let ex_s = Str.matched_group 3 line in
           let repr =
             try Zcontainer.Ztypes.zreprbox_of_string Zrepr.stdrepr repr_s
             with Failure _ ->
               let msg = sprintf "Line %d: cannot parse type" (i+1) in
               raise(Parse_error msg) in
           let lexbuf = Lexing.from_string ex_s in
           let expr =
             try Loader_parser.expr_phrase (Loader_lexer.scan (i+1)) lexbuf
             with
               | Parsing.Parse_error ->
                   let msg = sprintf "Line %d: cannot parse expression" (i+1) in
                   raise(Parse_error msg) in
           cols := (name, repr, expr) :: !cols
         else
           let msg = sprintf "Line %d: syntax error" (i+1) in
           raise(Loader_lexer.Scan_error msg)
       )
    )
    lines;
  List.rev !cols

let rec parser_of_type : type t . t Ztypes.ztype -> t Zdigester.parserfun =
  fun zt ->
  match zt with
  | Ztypes.Zstring -> Zdigester.parser_of_type zt ""
  | Ztypes.Zint64 -> Zdigester.parser_of_type zt ""
  | Ztypes.Zint -> Zdigester.parser_of_type zt ""
  | Ztypes.Zfloat -> Zdigester.parser_of_type zt ""
  | Ztypes.Zbool -> Zdigester.parser_of_type zt ""
  | Ztypes.Zoption zt ->
      let sub_parser = parser_of_type zt in
      (fun s -> Some(sub_parser s))
  | Ztypes.Zpair(zt1,zt2) ->
      let sub_parser1 = parser_of_type zt1 in
      let sub_parser2 = parser_of_type zt2 in
      (fun s ->
         try
           let k = String.index s ',' in
           (sub_parser1 (String.sub s 0 k),
            sub_parser2 (String.sub s (k+1) (String.length s - k - 1))
           )
         with
           | Not_found ->
               failwith "Cannot parse pair: no comma"
      )
  | Ztypes.Zlist zt1 ->
      let sub_parser1 = parser_of_type zt1 in
      let rec loop s i =
        try
          let k = String.index_from s i ',' in
          sub_parser1 (String.sub s i (k-i)) :: loop s (k+1)
        with
          | Not_found ->
              let l = String.length s in
              [ sub_parser1 (String.sub s i (l-i)) ] in
      (fun s ->
         if s = "" then [] else loop s 0
      )
  | Ztypes.Zbundle zt1 ->
      parser_of_type (Ztypes.Zlist(Ztypes.Zpair(Ztypes.Zint,zt1)))

let rec default_of_type : type t . t Ztypes.ztype -> t =
  (* the value when the field is missing in the CSV row *)
  function
  | Ztypes.Zstring -> ""
  | Ztypes.Zint64 -> 0L
  | Ztypes.Zint -> 0
  | Ztypes.Zfloat -> 0.0
  | Ztypes.Zbool -> false
  | Ztypes.Zoption zt1 -> Some(default_of_type zt1)
  | Ztypes.Zpair(zt1,zt2) -> (default_of_type zt1, default_of_type zt2)
  | Ztypes.Zlist zt1 -> []
  | Ztypes.Zbundle _ -> []
                               
let rec unknown_of_type : type t . t Ztypes.ztype -> t =
  (* the value to substitute for '?' *)
  function
  | Ztypes.Zstring -> ""
  | Ztypes.Zint64 -> 0L
  | Ztypes.Zint -> 0
  | Ztypes.Zfloat -> 0.0
  | Ztypes.Zbool -> false
  | Ztypes.Zoption zt1 -> None
  | Ztypes.Zpair(zt1,zt2) -> (unknown_of_type zt1, unknown_of_type zt2)
  | Ztypes.Zlist zt1 -> []
  | Ztypes.Zbundle zt1 -> []

let parse_field i name reprbox =
  let Ztypes.ZreprBox(ty,_) = reprbox in
  let dg_parser = 
    parser_of_type ty in
  let dg_unknown =
    unknown_of_type ty in
  let dg_default =
    default_of_type ty in
  let dg_add_field dg_row data_opt =
    match data_opt with
      | None ->
          dg_row.(i) <- Zdigester.ValueBox(ty,dg_unknown)
      | Some s ->
          dg_row.(i) <- Zdigester.StringBox s in
  let dg_add_element =
    match ty with
      | Ztypes.Zbundle ty1 ->
          let dg_unknown = unknown_of_type ty1 in
          let parse = parser_of_type ty1 in
          (fun k dg_row data_opt ->
            let v =
              match data_opt with
                | None -> dg_unknown
                | Some s -> parse s in
            match dg_row.(i) with
              | Zdigester.ValueBox(ty2,v2) ->
                  ( match Ztypes.same_ztype ty ty2 with
                      | Ztypes.Equal ->
                          dg_row.(i) <- Zdigester.ValueBox(ty, (k, v) :: v2)
                      | Ztypes.Not_equal ->
                          failwith ("Inconsistent types for column: " ^ name)
                  )
              | _ ->
                  failwith ("Inconsistent types for column: " ^ name)
          )
      | _ ->
          (fun _ _ _ -> failwith ("Column is not a bundle: " ^ name)) in
  (Zdigester.ParserBox(ty,dg_parser),
   Zdigester.ValueBox(ty,dg_default), 
   dg_add_field,
   dg_add_element
  )

let create_load_spec lines =
  let ldspec = parse_load_spec lines in
  let parsing =
    List.mapi
      (fun i (outname,reprbox,expr) ->
         let p, d, a, ae = parse_field i outname reprbox in
         { outname;
           reprbox;
           expr;
           parser = p;
           default = d;
           add = a;
           add_element = ae
         }
      )
      ldspec in
  Array.of_list parsing


let load_spec_of_string s =
  let lines = Str.split_delim (Str.regexp "[\n]") s in
  create_load_spec lines

let load_spec_of_channel ch =
  let lines = ref [] in
  try
    while true do
      lines := input_line ch :: !lines
    done;
    assert false
  with
    | End_of_file ->
        create_load_spec (List.rev !lines)

let digester_spec_of_load_spec ldspec =
  Array.map
    (fun col ->
       let Ztypes.ZreprBox(ty,repr) = col.reprbox in
       let repr_s = Ztypes.string_of_zrepr repr in
       (col.outname, col.parser, repr_s, "asc")
    )
    ldspec

type pos_mapping =
  | Col of int    (* Col index in ldspec *)
  | Col_bundle of (* Col index of bundle: *) int * (* Element: *) int

let create_col_of_pos ldspec csvcols =
  let ht = Hashtbl.create 7 in
  let ranges = ref [] in
  Array.iteri
    (fun i col ->
       match col.expr with
         | Name name ->
             let p, _ =
               try List.find (fun (p,n) -> n = name) csvcols
               with Not_found ->
                 failwith ("CSV column not found: " ^ name) in
             Hashtbl.replace ht p (Col i)
         | Pos pos ->
             Hashtbl.replace ht pos (Col i)
         | Range((pos1,pos2),(pos3,_)) ->
             if pos2 - pos1 <= 10000 then (
               for p = pos1 to pos2 do
                 Hashtbl.replace ht p (Col_bundle(i, p + (pos3-pos1)))
               done
             ) else (
               ranges := (i, pos1, pos2, pos3-pos1) :: !ranges
             )
         | _ ->
             failwith "expression still unsupported"
    )
    ldspec;
  (ht, List.rev !ranges)


type loader =
  { dg : Zdigester.zdigester;
    ldspec : load_spec;
    col_of_pos : (int,pos_mapping) Hashtbl.t;
      (* maps CSV position to the zcontainer column index *)
    col_of_pos_ranges : (int * int * int * int) list;
      (* for large ranges: (colid, from, to, offset) *)
  }

let load dg ldspec csvcols = 
  let col_of_pos, col_of_pos_ranges = create_col_of_pos ldspec csvcols in
  { dg; ldspec; col_of_pos; col_of_pos_ranges }

let add ld data =
  let ldspec = ld.ldspec in
  let col_of_pos = ld.col_of_pos in
  let row = Array.map (fun col -> col.default) ldspec in
  List.iter
    (fun (pos, item_opt) ->
       try
         let col = Hashtbl.find col_of_pos pos in
         match col with
           | Col i ->
               ldspec.(i).add row item_opt
           | Col_bundle(i,k) ->
               ldspec.(i).add_element k row item_opt
       with Not_found ->
         try
           let (i, pos1, pos2, offset) =
             List.find
               (fun (_, pos1, pos2, _) ->
                 pos >= pos1 && pos <= pos2
               )
               ld.col_of_pos_ranges in
           ldspec.(i).add_element (pos+offset) row item_opt
         with Not_found -> ()
    )
    data;
  Zdigester.build_add_food ld.dg row



