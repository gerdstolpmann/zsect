open Arff_lexer
open Printf

type typ =
  | Int
  | Real
  | Numeric
  | String
  | Nominal of string list
  | Date of string

type header =
  { relation : string;
    attributes : (int * string * typ) list;
    attribute_ranges : (int * int * string * typ) list;  (* large ranges only *)
  }

exception Header_syntax of string
exception Data_syntax of string

let header_syntax (pos:Lexing.position) msg =
  let where =
    Lexing.(sprintf "File %S, line %d, position %d: "
                    pos.pos_fname pos.pos_lnum (pos.pos_cnum-pos.pos_bol+1)) in
  raise(Header_syntax(where ^ msg))

let data_syntax (pos:Lexing.position) msg =
  let where =
    Lexing.(sprintf "File %S, line %d, position %d: "
                    pos.pos_fname pos.pos_lnum (pos.pos_cnum-pos.pos_bol+1)) in
  raise(Data_syntax(where ^ msg))

let expect_newline_tok lexbuf tok =
  match tok with
    | H_newline -> ()
    | _ ->
        header_syntax (Lexing.lexeme_start_p lexbuf) "newline expected"

let expect_newline lexbuf =
  expect_newline_tok lexbuf (header lexbuf)

let lc_name tok =
  match tok with
    | H_name n -> H_name(String.lowercase n)
    | _ -> tok


let rec parse_header lexbuf hdr nextatt =
  match header lexbuf with
    | H_relation ->
        let hdr = parse_relation lexbuf hdr in
        parse_header lexbuf hdr nextatt
    | H_attribute ->
        let hdr, nextatt = parse_attribute lexbuf hdr nextatt in
        parse_header lexbuf hdr nextatt
    | H_newline ->
        parse_header lexbuf hdr nextatt
    | H_eof ->
        (hdr, H_eof)
    | H_data ->
        expect_newline lexbuf;
        (hdr, H_data)
    | _ ->
        header_syntax  (Lexing.lexeme_start_p lexbuf) "directive expected"


and parse_relation lexbuf hdr =
  match header lexbuf with
    | H_name n ->
        expect_newline lexbuf;
        { hdr with relation = n }
    | _ ->
        header_syntax (Lexing.lexeme_start_p lexbuf) "name expected"

and parse_attribute lexbuf hdr nextatt =
  match header lexbuf with
    | H_name n ->
        ( match header lexbuf with
            | H_at pos -> 
                let t, tok = parse_type lexbuf (header lexbuf) in
                expect_newline_tok lexbuf tok;
                ( { hdr with attributes = (pos, n, t) :: hdr.attributes },
                  pos+1
                )
            | H_range(frompos,topos) ->
                if frompos < 0 || frompos > topos then
                  header_syntax (Lexing.lexeme_start_p lexbuf) "bad range";
                let t, tok = parse_type lexbuf (header lexbuf) in
                expect_newline_tok lexbuf tok;
                if topos - frompos <= 10000 then
                  let new_atts =
                    Array.to_list
                      (Array.init
                         (topos-frompos+1)
                         (fun k ->
                           let pos = frompos+k in
                           let n = n ^ "@" ^ string_of_int pos in
                           (pos, n, t)
                         )
                      ) in
                  ( { hdr with attributes = List.rev new_atts @ hdr.attributes },
                    topos+1
                  )
                else
                  ( { hdr with attribute_ranges =
                                 (frompos, topos, n, t) :: hdr.attribute_ranges },
                    topos+1
                  )
            | tok ->
                let t, tok = parse_type lexbuf tok in
                expect_newline_tok lexbuf tok;
                ( { hdr with attributes = (nextatt, n, t) :: hdr.attributes },
                  nextatt+1
                )
        )
    | H_at pos ->
        let n = "@" ^ string_of_int pos in
        let t, tok = parse_type lexbuf (header lexbuf) in
        expect_newline_tok lexbuf tok;
        ( { hdr with attributes = (pos, n, t) :: hdr.attributes },
          nextatt+1
        )
    | H_range(frompos,topos) ->
        if frompos < 0 || frompos > topos then
          header_syntax (Lexing.lexeme_start_p lexbuf) "bad range";
        let t, tok = parse_type lexbuf (header lexbuf) in
        expect_newline_tok lexbuf tok;
        if topos - frompos <= 10000 then
          let new_atts =
            Array.to_list
              (Array.init
                 (topos-frompos+1)
                 (fun k ->
                   let pos = frompos+k in
                   let n = "@" ^ string_of_int pos in
                   (pos, n, t)
                 )
              ) in
          ( { hdr with attributes = List.rev new_atts @ hdr.attributes },
            topos+1
          )
        else
          ( { hdr with attribute_ranges =
                         (frompos, topos, "", t) :: hdr.attribute_ranges },
            topos+1
          )
    | _ ->
        header_syntax (Lexing.lexeme_start_p lexbuf) "name or position expected"

and parse_type lexbuf tok =
  match lc_name tok with
    | H_name ("int"|"integer") ->
        (Int, header lexbuf)
    | H_name ("real"|"float"|"double") ->
        (Real, header lexbuf)
    | H_name ("numeric") ->
        (Numeric, header lexbuf)
    | H_name ("string") ->
        (String, header lexbuf)
    | H_name ("date") ->
        ( match header lexbuf with
            | H_name format ->
                (Date format, header lexbuf)
            | tok ->
                (Date "yyyy-MM-dd'T'HH:mm:ss", tok)
        )
    | H_Lbrace ->
        parse_nominal lexbuf []
    | _ ->
        header_syntax (Lexing.lexeme_start_p lexbuf) "type expected"


and parse_nominal lexbuf names =
  match header lexbuf with
    | H_name n ->
        ( match header lexbuf with
            | H_Rbrace ->
                let t = Nominal (List.rev (n :: names)) in
                (t, header lexbuf)
            | H_comma ->
                parse_nominal lexbuf (n :: names)
            | _ ->
                header_syntax (Lexing.lexeme_start_p lexbuf)
                              "closing brace or comma expected"
        )
    | _ ->
        header_syntax (Lexing.lexeme_start_p lexbuf) "name expected"

let empty_hdr =
  { relation = ""; attributes = []; attribute_ranges = [] }

let check_post_header_token lexbuf tok expect_data =
  match tok with
    | H_eof ->
        if expect_data then
          header_syntax (Lexing.lexeme_start_p lexbuf) "@data expected"
    | H_data ->
        ()
    | _ ->
        header_syntax (Lexing.lexeme_start_p lexbuf) "end of header expected"


let hdr_fixup h =
  { h with
    attributes = List.rev h.attributes;
    attribute_ranges = List.rev h.attribute_ranges
  }

let header_of_lexbuf ?(expect_data=false) lexbuf =
  let hdr, tok = parse_header lexbuf empty_hdr 0 in
  check_post_header_token lexbuf tok expect_data;
  hdr_fixup hdr

let header_of_channel ?(expect_data=false) ch =
  let lexbuf = Lexing.from_channel ch in
  header_of_lexbuf ~expect_data lexbuf

let header_of_string ?(expect_data=false) s =
  let lexbuf = Lexing.from_string s in
  let hdr = header_of_lexbuf ~expect_data lexbuf in
  let len = Lexing.lexeme_end lexbuf in
  (hdr, len)

let type_of_pos lexbuf hdr ht pos =
  try
    Hashtbl.find ht pos
  with
    | Not_found ->
        try
          let (_,_,_,t) =
            List.find
              (fun (frompos,topos,_,_) -> pos >= frompos && pos <= topos)
              hdr.attribute_ranges in
          t
        with
          | Not_found ->
              data_syntax (Lexing.lexeme_start_p lexbuf) "position not declared"

let check_value lexbuf t v =
  match t with
    | Int ->
        ( try ignore(int_of_string v)
          with _ ->
            data_syntax (Lexing.lexeme_start_p lexbuf) "integer expected"
        )
    | Numeric
    | Real ->
        ( try ignore(float_of_string v)
          with _ ->
            data_syntax (Lexing.lexeme_start_p lexbuf) "number expected"
        )
    | String ->
        ()
    | Nominal l ->
        if not (List.mem v l) then
          data_syntax (Lexing.lexeme_start_p lexbuf) "bad value"
    | Date _ ->
        (* TODO *)
        ()


let rec parse_dense_data f hdr ht lexbuf tok pos data =
  match tok with
    | D_missing ->
        ignore(type_of_pos lexbuf hdr ht pos);
        let data = (pos, None) :: data in
        let pos = pos+1 in
        parse_dense_next f hdr ht lexbuf pos data
    | D_fieldsep ->
        data_syntax (Lexing.lexeme_start_p lexbuf) "value expected"
    | D_recsep ->
        if pos=0 then
          parse_dense_data f hdr ht lexbuf tok pos data
        else
          data_syntax (Lexing.lexeme_start_p lexbuf) "value expected"
    | D_space ->
        parse_dense_data f hdr ht lexbuf tok pos data
    | D_eof ->
        if pos=0 then
          ()
        else
          data_syntax (Lexing.lexeme_start_p lexbuf) "value expected"
    | D_quoted s
    | D_unquoted s ->
        let t = type_of_pos lexbuf hdr ht pos in
        check_value lexbuf t s;
        let data = (pos, Some s) :: data in
        let pos = pos+1 in
        parse_dense_next f hdr ht lexbuf pos data
    | D_Lbrace
    | D_Rbrace ->
        assert false

and parse_dense_next f hdr ht lexbuf pos data =
  match csv_data lexbuf with
    | D_space ->
        parse_dense_next f hdr ht lexbuf pos data
    | D_fieldsep ->
        parse_dense_data f hdr ht lexbuf (csv_data lexbuf) pos data
    | D_recsep ->
        parse_dense_next_rec f hdr ht lexbuf D_recsep data
    | D_eof ->
        parse_dense_next_rec f hdr ht lexbuf D_eof data
    | D_missing
    | D_quoted _
    | D_unquoted _ ->
        data_syntax (Lexing.lexeme_start_p lexbuf) "unexpected data"
    | D_Lbrace
    | D_Rbrace ->
        assert false

and parse_dense_next_rec f hdr ht lexbuf last_tok data =
  f data;
  match last_tok with
    | D_recsep ->
        parse_dense_data f hdr ht lexbuf (csv_data lexbuf) 0 []
    | D_eof ->
        ()
    | _ ->
        assert false

let rec parse_sparse_start f hdr ht lexbuf tok =
  match tok with
    | D_space
    | D_recsep ->
        parse_sparse_start f hdr ht lexbuf (sparse_data lexbuf)
    | D_eof ->
        ()
    | D_Lbrace ->
        parse_sparse_entry f hdr ht lexbuf []
    | _ ->
        data_syntax (Lexing.lexeme_start_p lexbuf) "opening brace expected"

and parse_sparse_entry f hdr ht lexbuf data =
  match sparse_data lexbuf with
    | D_space
    | D_recsep ->
        parse_sparse_entry f hdr ht lexbuf data
    | D_unquoted s ->
        let n =
          try int_of_string s
          with Failure _ ->
            data_syntax (Lexing.lexeme_start_p lexbuf) "integer expected" in
        if n < 0 then
          data_syntax (Lexing.lexeme_start_p lexbuf)
                      "non-negative integer expected";
        let t = type_of_pos lexbuf hdr ht n in
        parse_sparse_value f hdr ht lexbuf data n t
    | _ ->
        data_syntax (Lexing.lexeme_start_p lexbuf) "integer expected"

and parse_sparse_value f hdr ht lexbuf data n t =
  match sparse_data lexbuf with
    | D_space
    | D_recsep ->
        parse_sparse_value f hdr ht lexbuf data n t
    | D_missing ->
        let data = (n, None) :: data in
        parse_sparse_next f hdr ht lexbuf data
    | D_quoted s
    | D_unquoted s ->
        check_value lexbuf t s;
        let data = (n, Some s) :: data in
        parse_sparse_next f hdr ht lexbuf data
    | _ ->
        data_syntax (Lexing.lexeme_start_p lexbuf) "value expected"

and parse_sparse_next f hdr ht lexbuf data =
  match sparse_data lexbuf with
    | D_space 
    | D_recsep ->
        parse_sparse_next f hdr ht lexbuf data
    | D_fieldsep ->
        parse_sparse_entry f hdr ht lexbuf data
    | D_Rbrace ->
        f data;
        parse_sparse_start f hdr ht lexbuf (sparse_data lexbuf)
    | _ ->
        data_syntax (Lexing.lexeme_start_p lexbuf)
                    "comma or closing brace expected"

let data_of_lexbuf f hdr lexbuf =
  let ht = Hashtbl.create (List.length hdr.attributes * 2) in
  List.iter
    (fun (pos,_,t) ->
       Hashtbl.replace ht pos t
    )
    hdr.attributes;
  match sparse_data lexbuf with
    | D_Lbrace ->
        parse_sparse_start f hdr ht lexbuf D_Lbrace
    | tok ->
        parse_dense_data f hdr ht lexbuf tok 0 []

let data_of_channel f hdr ch =
  data_of_lexbuf f hdr (Lexing.from_channel ch)

let data_of_string f hdr s =
  data_of_lexbuf f hdr (Lexing.from_string s)
