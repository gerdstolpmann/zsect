open Zcontainer

exception Parse_error of string

type load_spec
  (** the contents of the load file *)

type csv_spec = (int * string) list
  (** A list of positions and names describing the columns in a
     (possibly sparse) CSV file
   *)

val load_spec_of_string : string -> load_spec
  (** parse a load file given as string *)

val load_spec_of_channel : in_channel -> load_spec
  (** parse a load file given as channel *)

val digester_spec_of_load_spec : 
  load_spec -> (string * Zdigester.parserbox * string * string) array
  (** return the parser and type information needed for the digester.
      Use this array for creating the digester, e.g.
{[
let info = Loader.digester_spec_of_load_spec ldspec
let dg = Zdigester.create_memory_digester Zrepr.stdrepr info
]}
   *)

type loader
  (** The loader applied to a CSV/ARFF file *)

val load : Zdigester.zdigester ->
           load_spec ->
           csv_spec ->
           loader
  (** Create the loader *)

val add : loader -> (int * string option) list -> unit
  (** [add loader row]: add another row that is given as list of pairs
      [(pos, data)] where [pos] is the CSV/ARFF position. The data
      may be [None] meaning that the value is unknown ('?' in ARFF),
      or [Some s] when [s] is the string to parse.

      Data not present in the CSV/ARFF row is not passed as part
      of [row], i.e. these positions are skipped.

      This function just calls {!Zdigester.build_add_food} with the
      preprocessed row.
   *)


(** Note that you still need to finish the digester after all rows have
    been added:

{[
let ztab = Zdigester.build_end dg
]}

 *)
