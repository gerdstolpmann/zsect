%{
open Loader_types
%}

%token <string> IDENT
%token <int> POS
%token <(int*int)*(int*int)> RANGE
%token LPAREN RPAREN
%token COMMA END
%token <int64> INT
%token <float> FLOAT
%token <string> STRING

%start expr_phrase
%type <Loader_types.expr> expr_phrase

%%

expr_phrase:
  expr END  { $1 }
;

expr:
  IDENT LPAREN expr_list RPAREN { Call($1,$3) }
| IDENT LPAREN RPAREN { Call($1,[]) }
| IDENT { Name $1 }
| POS { Pos $1 }
| RANGE { let (r1,r2) = $1 in Range(r1, r2) }
| INT { Int $1 }
| FLOAT { Float $1 }
| STRING { String $1 }
;

expr_list:
  expr COMMA expr_list  { $1 :: $3 }
| expr { [$1] }
;

%%
