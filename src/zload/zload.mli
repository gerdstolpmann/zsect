(** Load data into zcontainers *)

(** {2 The load file} *)

(**
A load file says how the zcontainer is populated from a row-by-row input
such a csv or arff file.

Example:

{[
tcc_i : IM/string = tcc
tcc_d : DF64/float = tcc
eid : DF64/int = @5
pharma : IM/bool bundle = @1000..@1999
]}

Generally, the syntax is

{[
<zcontainer-column> : <repr>/<type> = <CSV/ARFF specification>
]}

It is allowed that the same CSV/ARFF column is loaded twice, which is
in particular useful when you want to make it available with different
representations and/or types. The notation "\@k" refers to the k-th
column in the CSV/ARFF file. The notation "\@k1..\@k2" means the bundle
of columns from k1 to k2. The type must then be a bundle, and the
column indexes are picked up as element designators in the bundle.
 *)


(** {2 Extended ARFF files} *)

(**
The {!Zload.Arff} module can generally read the ARFF files as specified
here: {{:http://weka.wikispaces.com/ARFF+%28stable+version%29}ARFF
(stable version)}.

In order to support sparse files better, the following extensions in
the ARFF header are supported:

 - [\@attribute name\@k type]: The column [name] is declared to be at the
   index [k]
 - [\@attribute \@k type]: The column without name is declared to be at the
   index [k]
 - [\@attribute \@k1..\@k2 type]: A number of columns are declared from indices
   [k1] to [k2], all without name


 *)


(** {2 Tools} *)

(**
Available tools:

 - [zload]: load CSV and/or ARFF files into zcontainer using a load file


 *)

(** {2 Modules} *)

open Zcontainer

module Loader : sig
  exception Parse_error of string

  type load_spec
    (** the contents of the load file *)

  type csv_spec = (int * string) list
    (** A list of positions and names describing the columns in a
       (possibly sparse) CSV file
     *)

  val load_spec_of_string : string -> load_spec
    (** parse a load file given as string *)

  val load_spec_of_channel : in_channel -> load_spec
    (** parse a load file given as channel *)

  val digester_spec_of_load_spec : 
    load_spec -> (string * Zdigester.parserbox * string * string) array
    (** return the parser and type information needed for the digester.
        Use this array for creating the digester, e.g.
  {[
  let info = Loader.digester_spec_of_load_spec ldspec
  let dg = Zdigester.create_memory_digester Zrepr.stdrepr info
  ]}
     *)

  type loader
    (** The loader applied to a CSV/ARFF file *)

  val load : Zdigester.zdigester ->
             load_spec ->
             csv_spec ->
             loader
    (** Create the loader *)

  val add : loader -> (int * string option) list -> unit
    (** [add loader row]: add another row that is given as list of pairs
        [(pos, data)] where [pos] is the CSV/ARFF position. The data
        may be [None] meaning that the value is unknown ('?' in ARFF),
        or [Some s] when [s] is the string to parse.

        Data not present in the CSV/ARFF row is not passed as part
        of [row], i.e. these positions are skipped.

        This function just calls {!Zdigester.build_add_food} with the
        preprocessed row.
     *)


  (** Note that you still need to finish the digester after all rows have
      been added:

  {[
  let ztab = Zdigester.build_end dg
  ]}

   *)
end


module Arff : sig
  type typ =
    | Int
    | Real
    | Numeric
    | String
    | Nominal of string list
    | Date of string

  type header =
    { relation : string;
      attributes : (int * string * typ) list;
      attribute_ranges : (int * int * string * typ) list;
    }

  exception Header_syntax of string
  exception Data_syntax of string

  val header_of_channel : ?expect_data:bool -> in_channel -> header
    (** Parses a header. Parsing stops either at EOF or right after
        the \@data keyword (plus newline).

        If [expect_data] is set, the \@data keyword is expected.
     *)

  val header_of_string : ?expect_data:bool -> string -> header * int
    (** [let (h, len) = header_of_string data]:

        Parses a header, starting at [data.[pos]]. The function returns the
        header and the number of parsed bytes.
     *)


  val header_of_lexbuf : ?expect_data:bool -> Lexing.lexbuf -> header
    (** Same for lexbufs *)

  val data_of_channel : ( (int*string option)list -> unit ) -> header ->
                        in_channel -> unit
    (** [data_of_channel f h ch]: Parses the data section. For every line,
        the function [f] is called back with a list of [(p,data)] pairs.
        [p] is the position of the data item. If [data] is [None] this
        means a question mark in the ARFF file. [Some s] means that there
        the data item exists in the ARFF file. Missing fields are not
        passed to [f].
     *)

  val data_of_string : ( (int*string option)list -> unit ) -> header ->
                       string -> unit
    (** Same for strings *)

  val data_of_lexbuf : ( (int*string option)list -> unit ) -> header ->
                       Lexing.lexbuf -> unit
    (** Same for lexbufs *)

end
