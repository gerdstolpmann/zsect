module type CONTAINER =
  sig
  end

type vnull =
  | Null_neg
  | Null_pos

type vtype =
  | TNum of vnull
  | TStr of vnull
  | TBool of vnull
  | TTup of (string * vtype) list * vnull
  | TBag of (string * vtype) list * vnull

type _ vrepr =
  | RNum : float vrepr
  | RNum_opt : float option vrepr
  | RStr : string vrepr
  | RStr_opt : string option vrepr
  | RBool : bool vrepr
  | RBool_opt : bool option vrepr
  | RTup : 'a trepr -> 'a vrepr
  | RTup_opt : 'a trepr -> 'a option vrepr
  | RBag : 'a trepr -> 'a list vrepr
  | RBag_opt : 'a trepr -> 'a list option vrepr

 and _ trepr =
   | RTup1 : 'a vrepr -> 'a trepr
   | RTup2 : 'a vrepr * 'b vrepr -> ('a * 'b) trepr
   | RTup3 : 'a vrepr * 'b vrepr * 'c vrepr -> ('a * 'b * 'c) trepr
   | RTup4 : 'a vrepr * 'b vrepr * 'c vrepr * 'd vrepr ->
             ('a * 'b * 'c * 'd) trepr
   | RTup5 : 'a vrepr * 'b vrepr * 'c vrepr * 'd vrepr * 'e vrepr ->
             ('a * 'b * 'c * 'd * 'e) trepr
   | RTupX : 'a trepr * 'b trepr * 'c trepr * 'd trepr * 'e trepr ->
             ('a * 'b * 'c * 'd * 'e) trepr


type _ tagged_value =
  | Value : 'a vrepr * 'a -> 'a tagged_value

type boxed_value =
  | ValueBox : 'a vrepr * 'a -> boxed_value

type _ tagged_tuple =
  | Tuple : 'a trepr * 'a -> 'a tagged_tuple

type boxed_tuple =
  | TupleBox : 'a trepr * 'a -> boxed_tuple

type _ tagged_bag =
  | Bag : 'a trepr * 'a -> 'a list tagged_bag

type boxed_bag =
  | BagBox: 'a trepr * 'a -> boxed_bag



type _ simple_value =
  | VNum : float -> float simple_value
  | VStr : string -> string simple_value
  | VBool : bool -> bool simple_value

type (_,_) type_eq =
  | Equal : ('a, 'a) type_eq
  | Not_equal

type 'a krepr =
    { key_repr : 'a vrepr;
      key_hash : 'a -> int;
      key_cmp : 'a -> 'a -> int;
    }

type boxed_krepr =
  | KReprBox : _ krepr -> boxed_krepr

type _ tagged_key =
  | KeyNull : unit tagged_key
  | Key : 'a krepr * 'a * int -> 'a tagged_key
      (* (repr, value, hash) *)

type boxed_key =
  | KeyNullBox : boxed_key
  | KeyBox : 'a krepr * 'a * int -> boxed_key
      (* (repr, value, hash) *)
