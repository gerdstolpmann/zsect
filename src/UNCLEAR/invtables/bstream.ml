exception End_of_stream

type 'a t =
    { mutable eof : bool;
      current : 'a Queue.t;
      generate : unit -> 'a Queue.t;
    }

let create() =
  { eof = false;
    current = Queue.create();
    generate = (fun () -> raise End_of_stream)
  }

let add bs x =
  if bs.eof then
    failwith "Bstream.add: cannot add once end is reported to caller";
  Queue.add x bs.current

let addq bs q =
  if bs.eof then
    failwith "Bstream.addq: cannot add once end is reported to caller";
  Queue.transfer (Queue.copy q) bs.current

let generate f =
  let q = Queue.create() in
  let current = Queue.create() in
  { eof = false;
    current;
    generate = 
      (fun () -> 
         assert(Queue.is_empty q);
         let x = f() in
         Queue.add x q;
         q
      )
  }

let generateq f =
  { eof = false;
    current = Queue.create();
    generate = f;
  }

let refill bs =
  try
    while Queue.is_empty bs.current do
      let q = bs.generate() in
      Queue.transfer q bs.current;
    done
  with
    | End_of_stream ->
        bs.eof <- true


let at_end bs =
  bs.eof || (
    Queue.is_empty bs.current && (
      refill bs;
      bs.eof
    )
  )

let next bs =
  if Queue.is_empty bs.current then
    refill bs;
  if not bs.eof then 
    ignore(Queue.take bs.current)

let peek bs =
  if Queue.is_empty bs.current then
    refill bs;
  if bs.eof then
    raise End_of_stream
  else
    Queue.peek bs.current

let peekq bs =
  if Queue.is_empty bs.current then
    refill bs;
  if bs.eof then
    raise End_of_stream
  else
    Queue.copy bs.current

let take bs =
  if Queue.is_empty bs.current then
    refill bs;
  if bs.eof then
    raise End_of_stream
  else
    Queue.take bs.current

let takeq bs =
  if Queue.is_empty bs.current then
    refill bs;
  if bs.eof then
    raise End_of_stream
  else
    let q = Queue.create() in
    Queue.transfer bs.current q;
    q
