(** Streams with buffer *)

exception End_of_stream

type 'a t

(** Cooked streams *)

val create : unit -> 'a t

val add : 'a t -> 'a -> unit

val addq : 'a t -> 'a Queue.t -> unit

(** Generating streams *)

val generate : (unit -> 'a) -> 'a t

val generateq : (unit -> 'a Queue.t) -> 'a t

(** Reading from streams *)

val at_end : _ t -> bool

val peek : 'a t -> 'a

val peekq : 'a t -> 'a Queue.t

val next : 'a t -> unit

val take : 'a t -> 'a

val takeq : 'a t -> 'a Queue.t
