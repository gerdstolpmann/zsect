open Types

let repr_proj11 =
  function
  | RTup1 r1 -> r1
  | _ -> assert false
                
let repr_proj12 =
  function
  | RTup2(r1,r2) -> r1
  | _ -> assert false
                
let repr_proj22 =
  function
  | RTup2(r1,r2) -> r2
  | _ -> assert false

let repr_proj13 =
  function
  | RTup3(r1,r2,r3) -> r1
  | _ -> assert false

let repr_proj23 =
  function
  | RTup3(r1,r2,r3) -> r2
  | _ -> assert false

let repr_proj33 =
  function
  | RTup3(r1,r2,r3) -> r3
  | _ -> assert false

let repr_proj14 =
  function
  | RTup4(r1,r2,r3,r4) -> r1
  | _ -> assert false

let repr_proj24 =
  function
  | RTup4(r1,r2,r3,r4) -> r2
  | _ -> assert false

let repr_proj34 =
  function
  | RTup4(r1,r2,r3,r4) -> r3
  | _ -> assert false

let repr_proj44 =
  function
  | RTup4(r1,r2,r3,r4) -> r4
  | _ -> assert false

let repr_proj15 =
  function
  | RTup5(r1,r2,r3,r4,r5) -> r1
  | _ -> assert false

let repr_proj25 =
  function
  | RTup5(r1,r2,r3,r4,r5) -> r2
  | _ -> assert false

let repr_proj35 =
  function
  | RTup5(r1,r2,r3,r4,r5) -> r3
  | _ -> assert false

let repr_proj45 =
  function
  | RTup5(r1,r2,r3,r4,r5) -> r4
  | _ -> assert false

let repr_proj55 =
  function
  | RTup5(r1,r2,r3,r4,r5) -> r5
  | _ -> assert false

let repr_proj1X =
  function
  | RTupX(r1,r2,r3,r4,r5) -> r1
  | _ -> assert false

let repr_proj2X =
  function
  | RTupX(r1,r2,r3,r4,r5) -> r2
  | _ -> assert false

let repr_proj3X =
  function
  | RTupX(r1,r2,r3,r4,r5) -> r3
  | _ -> assert false

let repr_proj4X =
  function
  | RTupX(r1,r2,r3,r4,r5) -> r4
  | _ -> assert false

let repr_proj5X =
  function
  | RTupX(r1,r2,r3,r4,r5) -> r5
  | _ -> assert false

let dest_tuple =
  function
  | RTup x -> x
  | _ -> assert false



let extract_tagged_value :
  type ta . ta tagged_value -> ta =
  fun tval ->
    match tval with
      | Value(repr, v) -> v

let extract_tagged_tuple :
  type ta . ta tagged_tuple -> ta =
  fun ttup ->
    match ttup with
      | Tuple(repr, tup) -> tup

let repr_of_tagged_tuple :
  type ta . ta tagged_tuple -> ta trepr =
  fun ttup ->
    match ttup with
      | Tuple(repr, tup) -> repr


let rec have_same_vrepr :
  type ta tb . ta vrepr -> tb vrepr -> (ta,tb) type_eq =
  fun r1 r2 ->
    match r1, r2 with
      | RNum, RNum -> Equal
      | RNum_opt, RNum_opt -> Equal
      | RStr, RStr -> Equal
      | RStr_opt, RStr_opt -> Equal
      | RBool, RBool -> Equal
      | RBool_opt, RBool_opt -> Equal
      | RTup t1, RTup t2 -> have_same_trepr t1 t2
      | RTup_opt t1, RTup_opt t2 ->
          ( match have_same_trepr t1 t2 with
              | Equal -> Equal
              | Not_equal -> Not_equal
          )
      | RBag t1, RBag t2 ->
          ( match have_same_trepr t1 t2 with
              | Equal -> Equal
              | Not_equal -> Not_equal
          )
       | RBag_opt t1, RBag_opt t2 ->
          ( match have_same_trepr t1 t2 with
              | Equal -> Equal
              | Not_equal -> Not_equal
          )
       | _ ->
           Not_equal

and have_same_trepr :
  type ta tb . ta trepr -> tb trepr -> (ta,tb) type_eq =
  fun t1 t2 ->
    match t1, t2 with
      | RTup1 r1, RTup1 r2 ->
          have_same_vrepr r1 r2
      | RTup2(r1a,r1b), RTup2(r2a,r2b) ->
          ( match have_same_vrepr r1a r2a with
              | Equal -> 
                  (match have_same_vrepr r1b r2b with
                     | Equal -> Equal
                     | Not_equal -> Not_equal
                  )
              | Not_equal -> Not_equal
          )
      | RTup3(r1a,r1b,r1c), RTup3(r2a,r2b,r2c) ->
          ( match have_same_vrepr r1a r2a with
              | Equal -> 
                  (match have_same_vrepr r1b r2b with
                     | Equal ->
                         (match have_same_vrepr r1c r2c with
                            | Equal -> Equal
                            | Not_equal -> Not_equal
                         )
                     | Not_equal -> Not_equal
                  )
              | Not_equal -> Not_equal
          )
      | RTup4(r1a,r1b,r1c,r1d), RTup4(r2a,r2b,r2c,r2d) ->
          ( match have_same_vrepr r1a r2a with
              | Equal -> 
                  (match have_same_vrepr r1b r2b with
                     | Equal ->
                         (match have_same_vrepr r1c r2c with
                            | Equal ->
                                (match have_same_vrepr r1d r2d with
                                   | Equal -> Equal
                                   | Not_equal -> Not_equal
                                )
                            | Not_equal -> Not_equal
                         )
                     | Not_equal -> Not_equal
                  )
              | Not_equal -> Not_equal
          )
      | RTup5(r1a,r1b,r1c,r1d,r1e), RTup5(r2a,r2b,r2c,r2d,r2e) ->
          ( match have_same_vrepr r1a r2a with
              | Equal -> 
                  (match have_same_vrepr r1b r2b with
                     | Equal ->
                         (match have_same_vrepr r1c r2c with
                            | Equal ->
                                (match have_same_vrepr r1d r2d with
                                   | Equal ->
                                       (match have_same_vrepr r1e r2e with
                                          | Equal -> Equal
                                          | Not_equal -> Not_equal
                                       )
                                   | Not_equal -> Not_equal
                                )
                            | Not_equal -> Not_equal
                         )
                     | Not_equal -> Not_equal
                  )
              | Not_equal -> Not_equal
          )
      | RTupX(r1a,r1b,r1c,r1d,r1e), RTupX(r2a,r2b,r2c,r2d,r2e) ->
          ( match have_same_trepr r1a r2a with
              | Equal -> 
                  (match have_same_trepr r1b r2b with
                     | Equal ->
                         (match have_same_trepr r1c r2c with
                            | Equal ->
                                (match have_same_trepr r1d r2d with
                                   | Equal ->
                                       (match have_same_trepr r1e r2e with
                                          | Equal -> Equal
                                          | Not_equal -> Not_equal
                                       )
                                   | Not_equal -> Not_equal
                                )
                            | Not_equal -> Not_equal
                         )
                     | Not_equal -> Not_equal
                  )
              | Not_equal -> Not_equal
          )
      | _ ->
          Not_equal
