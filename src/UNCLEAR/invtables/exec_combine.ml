open Types

module MakeXCo (C : CONTAINER) = struct
  module XT = Exec_types.MakeXT(C)

  let ni_of_node :
    type cur . cur XT.node -> cur XT.node_info =
    fun node ->
      match node with
        | XT.XBag(ni,_) -> ni
        | XT.XMap(ni,_,_) -> ni

end

