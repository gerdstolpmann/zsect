open Types

module MakeXT (C : CONTAINER) = struct

  (* (s,t) vexpr: The type s is the type of the current tuple. t is the type
        of the result of the expression

     (s,t) tproj: same system

     (s,t) tconstr: same system
   *)

  type (_,_) vexpr =
    | XLiteral : 'a simple_value -> (_,'a) vexpr
    | XTupProj : ('cur,'a) tproj -> ('cur,'a) vexpr
    | XTupConstr : ('cur,'a) tconstr -> ('cur,'a) vexpr
    | XCurTup : 'cur trepr -> ('cur,'cur) vexpr
    | XDefLocal : string * boxed_value * ('cur,'a) vexpr -> ('cur,'a) vexpr
    | XLocal : string * 'a vrepr -> (_,'a) vexpr

   and (_,_) tproj =
     | XProj11 : ('cur, 'a) vexpr -> ('cur, 'a) tproj
     | XProj12 : ('cur, 'a * 'b) vexpr -> ('cur, 'a) tproj
     | XProj22 : ('cur, 'a * 'b) vexpr -> ('cur, 'b) tproj
     | XProj13 : ('cur, 'a * 'b * 'c) vexpr -> ('cur, 'a) tproj
     | XProj23 : ('cur, 'a * 'b * 'c) vexpr -> ('cur, 'b) tproj
     | XProj33 : ('cur, 'a * 'b * 'c) vexpr -> ('cur, 'c) tproj
     | XProj14 : ('cur, 'a * 'b * 'c * 'd) vexpr -> ('cur, 'a) tproj
     | XProj24 : ('cur, 'a * 'b * 'c * 'd) vexpr -> ('cur, 'b) tproj
     | XProj34 : ('cur, 'a * 'b * 'c * 'd) vexpr -> ('cur, 'c) tproj
     | XProj44 : ('cur, 'a * 'b * 'c * 'd) vexpr -> ('cur, 'd) tproj
     | XProj15 : ('cur, 'a * 'b * 'c * 'd * 'e) vexpr -> ('cur, 'a) tproj
     | XProj25 : ('cur, 'a * 'b * 'c * 'd * 'e) vexpr -> ('cur, 'b) tproj
     | XProj35 : ('cur, 'a * 'b * 'c * 'd * 'e) vexpr -> ('cur, 'c) tproj
     | XProj45 : ('cur, 'a * 'b * 'c * 'd * 'e) vexpr -> ('cur, 'd) tproj
     | XProj55 : ('cur, 'a * 'b * 'c * 'd * 'e) vexpr -> ('cur, 'e) tproj
     | XProj1X : ('cur, 'a * 'b * 'c * 'd * 'e) vexpr -> ('cur, 'a) tproj
     | XProj2X : ('cur, 'a * 'b * 'c * 'd * 'e) vexpr -> ('cur, 'b) tproj
     | XProj3X : ('cur, 'a * 'b * 'c * 'd * 'e) vexpr -> ('cur, 'c) tproj
     | XProj4X : ('cur, 'a * 'b * 'c * 'd * 'e) vexpr -> ('cur, 'd) tproj
     | XProj5X : ('cur, 'a * 'b * 'c * 'd * 'e) vexpr -> ('cur, 'e) tproj

   and (_,_) tconstr =
     | XTup1 : ('cur, 'a) vexpr ->
               ('cur, 'a) tconstr
     | XTup2 : ('cur, 'a) vexpr * ('cur, 'b) vexpr ->
               ('cur, 'a * 'b) tconstr
     | XTup3 : ('cur, 'a) vexpr * ('cur, 'b) vexpr * ('cur, 'c) vexpr ->
               ('cur, 'a * 'b * 'c) tconstr
     | XTup4 : ('cur, 'a) vexpr * ('cur, 'b) vexpr * ('cur, 'c) vexpr *
                  ('cur, 'd) vexpr ->
               ('cur, 'a * 'b * 'c * 'd) tconstr
     | XTup5 : ('cur, 'a) vexpr * ('cur, 'b) vexpr * ('cur, 'c) vexpr *
                 ('cur, 'd) vexpr * ('cur, 'e) vexpr ->
               ('cur, 'a * 'b * 'c * 'd * 'e) tconstr
     | XTupX : ('cur, 'a) tconstr * ('cur, 'b) tconstr * ('cur, 'c) tconstr *
                  ('cur, 'd) tconstr * ('cur, 'e) tconstr ->
               ('cur, 'a * 'b * 'c * 'd * 'e) tconstr

  type group_by_members = boxed_bag

  type group_by_table_IDs = unit   (* TODO *)

  type axis =
    | ByMembers of group_by_members
    | ByTableIDs of group_by_table_IDs

  type invtable_type =
      { itab_dummy : unit }   (* TODO *)

  type axis_repr =
    | RByMembers : _ trepr -> axis_repr
    | RByTableIDs : invtable_type -> axis_repr

  type 'cur node_info =
    < ni_keys : boxed_krepr array option;
        (* ni_keys = None: data is ungrouped
           ni_keys = Some kreprs: data is grouped. kreprs=[||] is possible and
                     means "group by all"
         *)
      ni_axes : axis array;
        (* If empty, there is only the current tuple (but the stream can already
           be ordered by keys).
           If non-empty there is additionally grouped data, as described by
           the axes.
         *)
      ni_current : 'cur trepr;
    >

  type _ node =
    | XBag : 'a node_info * (_, 'a) tconstr list -> 'a node
    | XMap : 'a node_info * ('b, 'a) tconstr * 'b node -> 'a node

  type 'cur row =
      { row_keys : boxed_key array;
        row_axes : axis array;
        row_current : 'cur tagged_tuple;
      }

end
