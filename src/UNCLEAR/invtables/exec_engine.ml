open Types

module MakeXEng (C : CONTAINER) = struct

  module XT = Exec_types.MakeXT(C)
  module XX = Exec_expr.MakeXX(C)
  module XCo = Exec_combine.MakeXCo(C)

  type ctx =
      { ctx_dummy : unit }

  let simple_row trepr data =
    { XT.row_keys = [| |];
      XT.row_axes = [| |];
      XT.row_current = Tuple(trepr,data)
    }

  let rec execute :
    type cur . ctx -> cur XT.node -> cur XT.row Bstream.t =
    fun ctx node ->
      match node with
        | XT.XBag _ ->
            execute_XBag ctx node
        | XT.XMap _ ->
            execute_XMap ctx node

  and execute_XBag :
    type cur . ctx -> cur XT.node -> cur XT.row Bstream.t =
    fun ctx node ->
      match node with
        | XT.XBag(ni, bag) ->
            let bs = Bstream.create() in
            let expr_ctx = XX.create_ctx() in
            let trepr = ni#ni_current in
            List.iter
              (fun texpr ->
                 let vexpr = XT.XTupConstr texpr in
                 let tup = XX.entry_eval expr_ctx vexpr in
                 Bstream.add bs (simple_row trepr tup)
              )
              bag;
            bs
        | _ ->
            assert false

  and execute_XMap :
    type cur . ctx -> cur XT.node -> cur XT.row Bstream.t =
    fun ctx node ->
      match node with
        | XT.XMap(ni, texpr, n1) ->
            let bs1 = execute ctx n1 in
            let expr_ctx = XX.create_ctx() in
            let trepr = ni#ni_current in
            let vexpr = XT.XTupConstr texpr in
            Bstream.generateq
              (fun () ->
                 let q1 = Bstream.peekq bs1 in  (* or End_of_stream *)
                 let qo = Queue.create() in
                 Queue.iter
                   (fun row ->
                      let current = XT.(row.row_current) in
                      let tup =
                        XX.entry_eval_with_curtup
                          current expr_ctx vexpr in
                      let out =
                        { row with XT.row_current = Tuple(trepr,tup) } in
                      Queue.add out qo
                   )
                   q1;
                 qo
              )
        | _ ->
            assert false
end
