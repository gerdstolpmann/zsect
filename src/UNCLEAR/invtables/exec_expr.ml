open Types
open Printf

module MakeXX (C : CONTAINER) = struct
  module XT = Exec_types.MakeXT(C)
  module StrMap = Map.Make(String)

  let rec repr_of_expr :
    type cur t . (cur,t) XT.vexpr -> t vrepr =
    fun qx ->
      match qx with
        | XT.XLiteral v ->
            ( match v with
                | VNum x -> RNum
                | VStr s -> RStr
                | VBool b -> RBool
            )
        | XT.XTupProj proj_spec ->
            repr_of_tuple_project proj_spec
        | XT.XTupConstr constr ->
            RTup(repr_of_tuple_constr constr)
        | XT.XCurTup repr ->
            RTup repr
        | XT.XDefLocal(_,_,qx1) ->
            repr_of_expr qx1
        | XT.XLocal(_,repr) ->
            repr

  and repr_of_tuple_project :
    type cur t .  (cur,t) XT.tproj -> t vrepr =
    fun proj_spec ->
      match proj_spec with
        | XT.XProj11 x -> Repr.repr_proj11 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj12 x -> Repr.repr_proj12 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj22 x -> Repr.repr_proj22 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj13 x -> Repr.repr_proj13 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj23 x -> Repr.repr_proj23 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj33 x -> Repr.repr_proj33 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj14 x -> Repr.repr_proj14 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj24 x -> Repr.repr_proj24 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj34 x -> Repr.repr_proj34 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj44 x -> Repr.repr_proj44 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj15 x -> Repr.repr_proj15 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj25 x -> Repr.repr_proj25 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj35 x -> Repr.repr_proj35 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj45 x -> Repr.repr_proj45 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj55 x -> Repr.repr_proj55 (Repr.dest_tuple (repr_of_expr x))
        | XT.XProj1X x ->
            RTup(Repr.repr_proj1X (Repr.dest_tuple (repr_of_expr x)))
        | XT.XProj2X x ->
            RTup(Repr.repr_proj2X (Repr.dest_tuple (repr_of_expr x)))
        | XT.XProj3X x ->
            RTup(Repr.repr_proj3X (Repr.dest_tuple (repr_of_expr x)))
        | XT.XProj4X x ->
            RTup(Repr.repr_proj4X (Repr.dest_tuple (repr_of_expr x)))
        | XT.XProj5X x ->
            RTup(Repr.repr_proj5X (Repr.dest_tuple (repr_of_expr x)))

  and repr_of_tuple_constr :
    type cur t . (cur,t) XT.tconstr -> t trepr =
    fun constr ->
      match constr with
        | XT.XTup1 x1 ->
            RTup1(repr_of_expr x1)
        | XT.XTup2(x1,x2) ->
            RTup2(repr_of_expr x1, repr_of_expr x2)
        | XT.XTup3(x1,x2,x3) ->
            RTup3(repr_of_expr x1, repr_of_expr x2, repr_of_expr x3)
        | XT.XTup4(x1,x2,x3,x4) ->
            RTup4(repr_of_expr x1, repr_of_expr x2, repr_of_expr x3,
                  repr_of_expr x4)
        | XT.XTup5(x1,x2,x3,x4,x5) ->
            RTup5(repr_of_expr x1, repr_of_expr x2, repr_of_expr x3,
                  repr_of_expr x4, repr_of_expr x5)
        | XT.XTupX(x1,x2,x3,x4,x5) ->
            RTupX(repr_of_tuple_constr x1, repr_of_tuple_constr x2,
                  repr_of_tuple_constr x3, repr_of_tuple_constr x4,
                  repr_of_tuple_constr x5)

  let proj12 (x,_) = x
  let proj22 (_,x) = x

  let proj13 (x,_,_) = x
  let proj23 (_,x,_) = x
  let proj33 (_,_,x) = x

  let proj14 (x,_,_,_) = x
  let proj24 (_,x,_,_) = x
  let proj34 (_,_,x,_) = x
  let proj44 (_,_,_,x) = x

  let proj15 (x,_,_,_,_) = x
  let proj25 (_,x,_,_,_) = x
  let proj35 (_,_,x,_,_) = x
  let proj45 (_,_,_,x,_) = x
  let proj55 (_,_,_,_,x) = x

  type ctx =
      { locals : boxed_value StrMap.t;
      }


  type 'cur eval =
      { mutable eval : 's . ctx -> ('cur,'s) XT.vexpr -> 's }
    (* NB. 'cur is regular (i.e. the same on every level of the recursion),
       while 's can be different in every level
     *)

  let tuple_project :
    type cur ta tb . cur eval -> ctx -> (cur,ta) XT.tproj -> ta =
    fun eval ctx proj_spec ->
      match proj_spec with
        | XT.XProj11 v -> eval.eval ctx v
        | XT.XProj12 v -> proj12 (eval.eval ctx v)
        | XT.XProj22 v -> proj22 (eval.eval ctx v)
        | XT.XProj13 v -> proj13 (eval.eval ctx v)
        | XT.XProj23 v -> proj23 (eval.eval ctx v)
        | XT.XProj33 v -> proj33 (eval.eval ctx v)
        | XT.XProj14 v -> proj14 (eval.eval ctx v)
        | XT.XProj24 v -> proj24 (eval.eval ctx v)
        | XT.XProj34 v -> proj34 (eval.eval ctx v)
        | XT.XProj44 v -> proj44 (eval.eval ctx v)
        | XT.XProj15 v -> proj15 (eval.eval ctx v)
        | XT.XProj25 v -> proj25 (eval.eval ctx v)
        | XT.XProj35 v -> proj35 (eval.eval ctx v)
        | XT.XProj45 v -> proj45 (eval.eval ctx v)
        | XT.XProj55 v -> proj55 (eval.eval ctx v)
        | XT.XProj1X v -> proj15 (eval.eval ctx v)
        | XT.XProj2X v -> proj25 (eval.eval ctx v)
        | XT.XProj3X v -> proj35 (eval.eval ctx v)
        | XT.XProj4X v -> proj45 (eval.eval ctx v)
        | XT.XProj5X v -> proj55 (eval.eval ctx v)

  let rec tuple_constr :
    type cur ta . cur eval -> ctx -> (cur,ta) XT.tconstr -> ta =
    fun eval ctx constr ->
      match constr with
        | XT.XTup1 x1 -> eval.eval ctx x1
        | XT.XTup2(x1,x2) -> (eval.eval ctx x1, eval.eval ctx x2)
        | XT.XTup3(x1,x2,x3) -> (eval.eval ctx x1, eval.eval ctx x2,
                                 eval.eval ctx x3)
        | XT.XTup4(x1,x2,x3,x4) -> (eval.eval ctx x1, eval.eval ctx x2, 
                                    eval.eval ctx x3, eval.eval ctx x4)
        | XT.XTup5(x1,x2,x3,x4,x5) -> (eval.eval ctx x1, eval.eval ctx x2,
                                       eval.eval ctx x3, eval.eval ctx x4,
                                       eval.eval ctx x5)
        | XT.XTupX(x1,x2,x3,x4,x5) ->
            (tuple_constr eval ctx x1, tuple_constr eval ctx x2,
             tuple_constr eval ctx x3, tuple_constr eval ctx x4,
             tuple_constr eval ctx x5)
                                     

  let eval_generic :
    type cur t. cur eval -> ctx -> (cur,t) XT.vexpr -> t =
    fun eval ctx qx ->
      match qx with
        | XT.XLiteral v ->
            ( match v with
                | VNum x -> x
                | VStr s -> s
                | VBool b -> b
            )
        | XT.XTupProj proj_spec ->
            tuple_project eval ctx proj_spec
        | XT.XTupConstr constr ->
            tuple_constr eval ctx constr
        | XT.XDefLocal(name,boxed_value,expr) ->
            let ctx =
              { ctx with locals = StrMap.add name boxed_value ctx.locals } in
            eval.eval ctx expr
        | XT.XLocal(name,repr) ->
            let local =
              try StrMap.find name ctx.locals
              with Not_found ->
                   failwith (sprintf "Invtables.Exec_expr.eval: local not \
                                      found: %s" name) in
            ( match local with
                | ValueBox(v_repr, v) ->
                    ( match Repr.have_same_vrepr repr v_repr with
                        | Equal ->
                            (* Gives us: type_of(v) = t *)
                            v
                        | Not_equal ->
                            failwith (sprintf "Invtables.Exec_expr.eval: local
                                               has bad type: %s" name)
                    )
            )
        | _ ->
            assert false

  let eval_with_curtup :
    type cur ta tb .
           cur eval -> ctx -> cur tagged_tuple -> (cur,ta) XT.vexpr -> ta =
    fun eval ctx current qx ->
      match qx with
        | XT.XCurTup _ ->
            Repr.extract_tagged_tuple current
        | _ ->
            eval_generic eval ctx qx

  let entry_eval :
    type ta . ctx -> (_,ta) XT.vexpr -> ta =
    fun ctx qx ->
      let eval =
        { eval = (fun _ _ -> assert false) } in
      let rec eval_recursion : type ta . ctx -> (_,ta) XT.vexpr -> ta =
        fun ctx qx -> eval_generic eval ctx qx in
      eval.eval <- eval_recursion;
      eval.eval ctx qx

  let entry_eval_with_curtup :
    type cur ta . cur tagged_tuple -> ctx -> (cur,ta) XT.vexpr -> ta =
    fun current ctx qx ->
      let eval =
        { eval = (fun _ _ -> assert false) } in
      let rec eval_recursion : type ta . ctx -> (cur,ta) XT.vexpr -> ta =
        fun ctx qx -> eval_with_curtup eval ctx current qx in
      eval.eval <- eval_recursion;
      eval.eval ctx qx

  let create_ctx() =
    { locals = StrMap.empty
    }

end

