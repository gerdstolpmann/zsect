(* File: csv.ml

   Copyright (C) 2005-2009

     Richard Jones
     email: rjones@redhat.com

     Christophe Troestler
     email: Christophe.Troestler@umh.ac.be
     WWW: http://math.umh.ac.be/an/software/

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License version 2.1 or
   later as published by the Free Software Foundation, with the special
   exception on linking described in the file LICENSE.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the file
   LICENSE for more details. *)

(* MOTIVATION.  There are already several solutions to parse CSV files
   in OCaml.  They did not suit my needs however because:

   - The files I need to deal with have a header which does not always
   reflect the data structure (say the first row are the names of
   neurones but there are two columns per name).  In other words I
   want to be able to deal with heterogeneous files.

   - I do not want to read the the whole file at once (I may but I
   just want to be able to choose).  Higher order functions like fold
   are fine provided the read stops at the line an exception is raised
   (so it can be reread again).

   - For similarly encoded line, being able to specify once a decoder
   and then use a type safe version would be nice.

   - Speed is not neglected (we would like to be able to parse a
   ~2.5Mb file under 0.1 sec on my machine (2GHz Core Duo)).

   We follow the CVS format documentation available at
   http://www.creativyst.com/Doc/Articles/CSV/CSV01.htm
*)

open Printf

(* In the following we exploit that every OCaml string is terminated
   by a null byte, i.e. s[n] = '\000'. Of course, this byte is only
   accessible via String.unsafe_get.
 *)


type t = string list list

(* Specialize to int for speed *)
let max i j = if (i:int) < j then j else i


class type in_obj_channel =
object
  method input : Bytes.t -> int -> int -> int
  method close_in : unit -> unit
end

class type out_obj_channel =
object
  method output : Bytes.t -> int -> int -> int
  method close_out : unit -> unit
end

(* Specialize [min] to integers for performance reasons (> 150% faster). *)
let min x y = if (x:int) <= y then x else y

(*
 * Input
 *)

exception Failure of int * int * string

type skip = [ `None | `Empty | `Skip ]

let buffer_len = (* orig: 0x1FFF, EdgeSpring: *) 16384
  (* 4096 is the internal buffer size for in_channel, and a multiple is
     good
   *)

(* We buffer the input as this allows the be efficient while using
   very basic input channels.  The drawback is that if we want to use
   another tool, there will be data hold in the buffer.  That is why
   we allow to convert a CSV handle to an object sharing the same
   buffer.  Because if this, we actually decided to implement the CSV
   handle as an object that is coercible to a input-object.

   FIXME: This is not made for non-blocking channels.  Can we fix it? *)
type in_channel = {
  in_chan : in_obj_channel;
  mutable in_pos : int;  (* the relative position *)
  in_buf : Bytes.t;
  (* The data in the in_buf is at indexes i s.t. in0 <= i < in1.
     Invariant: 0 <= in0 ; in1 <= in_buf_len. in1 < 0 indicates a
     closed channel. *)
  in_buf_len : int;   (* length of the string in_buf *)
  mutable in0 : int;
  mutable in1 : int;
  mutable end_of_file : bool;
  (* If we encounter an End_of_file exception, we set this flag to
     avoid reading again because we do not know how the channel will
     react past an end of file.  That allows us to assume that
     reading past an end of file will keep raising End_of_file. *)
  current_field : Buffer.t; (* buffer reused to scan fields *)
  mutable record : string list; (* The current record *)
  mutable record_len : int;  (* fields in [record] *) 
  mutable input_field : int; (* the current input field. Can be > record_len
                                when fields were skipped *)
  mutable record_n : int; (* For error messages *)
  mutable record_rev : bool;  (* whether [record] is in reverse order *)
  mutable after_quote : bool;  (* temp state for parsing quoted fields *)
  mutable refills : int;  (* number of refills *)
  separator : char;
  excel_tricks : bool;
  quotes : bool;
  width : int;
  skip : skip array;
}

let of_in_obj ?(separator=',') ?(noquotes=false) ?(excel_tricks=true) 
              ?(width=0) ?(skip = [| |]) ?(buffer_size=buffer_len) in_chan =
  {
    in_chan = in_chan;
    in_pos = 0;
    in_buf = Bytes.create buffer_size;
    in_buf_len = buffer_size;
    in0 = 0;
    in1 = 0;
    end_of_file = false;
    current_field = Buffer.create 0xFF;
    record = [];
    record_len = 0;
    input_field = 0;
    record_n = 0;
    record_rev = false;
    after_quote = false;
    refills = 0;
    separator = separator;
    excel_tricks = excel_tricks;
    quotes = not noquotes;
    width;
    skip;
  }

let of_channel ?separator ?noquotes ?excel_tricks ?width ?skip ?buffer_size fh =
  of_in_obj ?separator ?noquotes ?excel_tricks ?width ?skip ?buffer_size
    (object
       val fh = fh
       method input s ofs len =
         try
           let r = Pervasives.input fh s ofs len in
           if r = 0 then raise End_of_file;
           r
         with Sys_blocked_io -> 0
       method close_in() = Pervasives.close_in fh
     end)


(* [fill_in_buf chan] refills in_buf if needed (when empty).  After
   this [in0 < in1] or [in0 = in1 = 0], the latter indicating that
   there is currently no bytes to read (for a non-blocking channel).

   @raise End_of_file if there are no more bytes to read. *)
let fill_in_buf ic =
  if ic.in0 >= ic.in1 then begin
    if ic.end_of_file then raise End_of_file;
    ic.in_pos <- ic.in_pos + ic.in1;
    try
      ic.refills <- ic.refills + 1;
      ic.in0 <- 0;
      ic.in1 <- ic.in_chan#input ic.in_buf 0 ic.in_buf_len;
    with End_of_file ->
      ic.end_of_file <- true;
      ic.in1 <- 0;
      raise End_of_file
  end


let close_in ic =
  if ic.in1 >= 0 then begin
    ic.in0 <- 0;
    ic.in1 <- -1;
    ic.in_chan#close_in(); (* may raise an exception *)
  end


let pos_in ic =
  if ic.in1 < 0 then failwith "pos_in: closed channel";
  ic.in_pos + ic.in0


let num_refills ic = ic.refills


let to_in_obj ic =
object
  val ic = ic

  method input buf ofs len =
    if ofs < 0 || len < 0 || ofs + len > Bytes.length buf
    then invalid_arg "Csv.to_in_obj#input";
    if ic.in1 < 0 then raise(Sys_error "Bad file descriptor");
    fill_in_buf ic;
    let r = min len (ic.in1 - ic.in0) in
    Bytes.blit ic.in_buf ic.in0 buf ofs r;
    ic.in0 <- ic.in0 + r;
    r

  method close_in() = close_in ic
end

(*
 * CSV input format parsing
 *)

let is_space c = c = ' ' || c = '\t' (* See documentation *)
let is_real_space c = c = ' ' (* when separator = '\t' *)

(* Given a buffer, returns its content stripped of *final* whitespace. *)
let strip_contents buf =
  let n = ref(Buffer.length buf - 1) in
  while !n >= 0 && is_space(Buffer.nth buf !n) do decr n done;
  Buffer.sub buf 0 (!n + 1)


let unsafe_sub s p n =
  if n=0 then 
    ""  (* no allocation *)
  else
    let r = Bytes.create n in
    Bytes.unsafe_blit s p r 0 n;
    Bytes.unsafe_to_string r

(* Return the substring after stripping its final space.  It is
   assumed the substring parameters are valid. *)
let strip_substring buf ofs len =
  let n = ref(ofs + len - 1) in
  while !n >= ofs && is_space(Bytes.unsafe_get buf !n) do decr n done;
  unsafe_sub buf ofs (!n - ofs + 1)


(* Skip the possible '\n' following a '\r'.  Reaching End_of_file is
   not considered an error -- just do nothing. *)
let skip_CR ic =
  try
    fill_in_buf ic;
    if Bytes.unsafe_get ic.in_buf ic.in0 = '\n' then ic.in0 <- ic.in0 + 1
  with End_of_file -> ()

let field_skip_status ic =
  if ic.input_field >= Array.length ic.skip then
    `None
  else
    Array.unsafe_get ic.skip ic.input_field


(* Unquoted field.  Read till a delimiter, a newline, or the
   end of the file.  Skip the next delimiter or newline.
   @return [true] if more fields follow, [false] if the record
   is complete. *)
let rec seek_unquoted_separator ic i =
  seek_unquoted_separator_loop ic i ic.in_buf ic.separator ' '

and seek_unquoted_separator_loop ic i in_buf sep last_c =
  let c = Bytes.unsafe_get in_buf i in
(* eprintf "loop field=%d i=%d c=%d c=%C\n" ic.input_field i (Char.code c) c;  *)
  if c = sep then
    found_unquoted_separator ic i in_buf sep c last_c true
  else
    match c with
      | '\n' | '\r' ->
          if ic.input_field+1 < ic.width then
            seek_unquoted_separator_loop ic (i+1) in_buf sep c
          else
            found_unquoted_separator ic i in_buf sep c last_c false;
      | '\000' ->
          if i >= ic.in1 then (
(* eprintf "next_chunk\n";*)
            (* End not found, need to look at the next chunk *)
            Buffer.add_subbytes ic.current_field in_buf ic.in0 (i - ic.in0);
            ic.in0 <- i;
            fill_in_buf ic; (* or raise End_of_file *)
            seek_unquoted_separator ic 0
          )
          else
            seek_unquoted_separator_loop ic (i+1) in_buf sep c
      | '"' when ic.quotes ->
          raise(Failure(ic.record_n, ic.record_len,
                        "Found quotation mark inside an unquoted field"))

      | _ ->
          seek_unquoted_separator_loop ic (i+1) in_buf sep c
            
and found_unquoted_separator ic i in_buf sep c last_c return =
  (* The field before the separator consists of:
      - the string in ic.current_field (frequently empty, though)
      - the string in in_buf from ic.in0 to i-1

     c is here the separator, last_c the char before the separator.

     return: the return value (i.e. whether more fields follow for this
     record)
   *)
  let skip_it = field_skip_status ic in
  if skip_it <> `Skip then (
    if Buffer.length ic.current_field = 0 || skip_it=`Empty then
      (* Avoid copying the string to the buffer if unnecessary *)
      ic.record <- 
        (if skip_it = `Empty then
           ""
         else if is_space last_c then
           strip_substring in_buf ic.in0 (i - ic.in0)
         else
           unsafe_sub in_buf ic.in0 (i - ic.in0)
        ) :: ic.record
    else (
      Buffer.add_subbytes ic.current_field in_buf ic.in0 (i - ic.in0);
      ic.record <- strip_contents ic.current_field :: ic.record;
      Buffer.clear ic.current_field
    );
    ic.record_len <- ic.record_len + 1;
  );
  Buffer.clear ic.current_field;
(* eprintf "field: %S\n" (List.hd ic.record); *)
  ic.input_field <- ic.input_field + 1;
  ic.in0 <- i + 1;
  if c = '\r' then skip_CR ic;
  return
  
let add_unquoted_field ic =
  seek_unquoted_separator ic ic.in0

let add_unquoted_field_eof_handler ic =
  (* What to do when End_of_file is caught in add_unquoted_field *)
  let skip_it = field_skip_status ic in
  ( match skip_it with
      | `None ->
           ic.record <- strip_contents ic.current_field :: ic.record;
           ic.record_len <- ic.record_len+1;
      | `Empty ->
           ic.record <- "" :: ic.record;
           ic.record_len <- ic.record_len+1;
      | `Skip ->
           ()
  );
  ic.input_field <- ic.input_field + 1


(* Quoted field closed.  Read past a separator or a newline and decode
   the field or raise [End_of_file].  @return [true] if more fields
   follow, [false] if the record is complete. *)
let rec seek_quoted_separator ic =
  let c = Bytes.unsafe_get ic.in_buf ic.in0 in
  if c = ic.separator || c = '\n' || c = '\r' then
    found_quoted_separator ic c
  else
    if is_space c then (
      ic.in0 <- ic.in0 + 1;
      seek_quoted_separator ic (* skip space *)
    )
    else 
      if c = '\000' && ic.in0 >= ic.in1 then (
        fill_in_buf ic; (* or raise End_of_file *)
        seek_quoted_separator ic
      )
      else
        raise(Failure(ic.record_n, ic.input_field,
                      "Non-space char after closing the quoted field"))

and found_quoted_separator ic c =
  ic.in0 <- ic.in0 + 1;
  let skip_it = field_skip_status ic in
  ( match skip_it with
      | `None ->
           ic.record <- Buffer.contents ic.current_field :: ic.record;
           ic.record_len <- ic.record_len + 1;
      | `Empty ->
           ic.record <- "" :: ic.record;
           ic.record_len <- ic.record_len + 1;
      | `Skip ->
           ()
  );
  ic.input_field <- ic.input_field + 1;
  Buffer.clear ic.current_field;
  if c = '\r' then (skip_CR ic; false) else (c = ic.separator)


let rec examine_quoted_field ic i =
  if ic.after_quote then
    examine_quoted_field_after_quote ic i ic.in_buf
  else
    examine_quoted_field_loop ic i ic.in_buf

and examine_quoted_field_loop ic i in_buf =
  let c = Bytes.unsafe_get in_buf i in
  if c = '\"' then (
    ic.after_quote <- true;
    let c1 = Bytes.unsafe_get in_buf (i+1) in
    if c1 = ic.separator && Buffer.length ic.current_field = 0 then (
      (* Fast path. NB. it is possible that c1='\000' when we are at the buffer
         end, but the logically _next_ char is the separator. This case is
         handled by the slow path.
       *)
      let skip_it = field_skip_status ic in
      ( match skip_it with
          | `None ->
               ic.record <-  unsafe_sub in_buf ic.in0 (i - ic.in0) :: ic.record;
               ic.record_len <- ic.record_len + 1;
          | `Empty ->
               ic.record <- "" :: ic.record;
               ic.record_len <- ic.record_len + 1;
          | `Skip ->
               ()
      );
      ic.input_field <- ic.input_field + 1;
      ic.in0 <- i + 2;
      true
    )
    else (
      (* Save the field so far, without the quote *)
      Buffer.add_subbytes ic.current_field in_buf ic.in0 (i - ic.in0);
      ic.in0 <- i + 1; (* skip the quote *)
      examine_quoted_field_after_quote ic ic.in0 in_buf
    )
  )
  else 
    if c = '\000' then (
      if i >= ic.in1 then (
        (* End of field not found, need to look at the next chunk *)
        Buffer.add_subbytes ic.current_field in_buf ic.in0 (i - ic.in0);
        ic.in0 <- i;
        fill_in_buf ic; (* or raise End_of_file *)
        examine_quoted_field ic 0
      )
      else
        examine_quoted_field_loop ic (i+1) in_buf
    )
    else
      examine_quoted_field_loop ic (i+1) in_buf

and examine_quoted_field_after_quote ic i in_buf =
  let c = Bytes.unsafe_get in_buf i in
  if c = '\"' then (
    (* Found adjacent quotes *)
    ic.after_quote <- false;
    (* [c] is kept so a quote will be included in the field *)
    examine_quoted_field_loop ic (i+1) in_buf
  )
  else if c = ic.separator || c = '\n' || c = '\r' then (
    found_quoted_separator ic c
  )
  else if is_space c then (
    seek_quoted_separator ic (* field already saved; in0=i;
                                after_quote=true *)
  )
  else if ic.excel_tricks && c = '0' then (
    (* Supposedly, '"' '0' means ASCII NULL *)
    ic.after_quote <- false;
    Buffer.add_char ic.current_field '\000';
    ic.in0 <- i + 1; (* skip the '0' *)
    examine_quoted_field_loop ic (i+1) in_buf
  )
  else 
    if c='\000' && i >= ic.in1 then (
      (* End of field not found, need to look at the next chunk *)
      Buffer.add_subbytes ic.current_field in_buf ic.in0 (i - ic.in0);
      ic.in0 <- i;
      fill_in_buf ic; (* or raise End_of_file *)
      examine_quoted_field ic 0
    )
    else
      raise(Failure(ic.record_n, ic.input_field, "Bad '\"' in quoted field"))
        

let add_quoted_field ic  =
  ic.after_quote <- false;
  examine_quoted_field_loop ic ic.in0 ic.in_buf


let add_quoted_field_eof_handler ic =
  (* What to do when End_of_file is caught in add_quoted_field *)
  (* Add the field even if not closed well *)
  let skip_it = field_skip_status ic in
  ( match skip_it with
      | `None -> 
           ic.record <- Buffer.contents ic.current_field :: ic.record;
           ic.record_len <- ic.record_len+1;
      |  `Empty ->
            ic.record <- "" :: ic.record;
            ic.record_len <- ic.record_len + 1;
      | `Skip ->
           ()
  );
  Buffer.clear ic.current_field;
  ic.input_field <- ic.input_field+1;
  if not ic.after_quote then
    raise(Failure(ic.record_n, ic.input_field,
                  "Quoted field closed by end of file"))



let rec skip_chars in_buf in0 in1 c0 c1 =
  (* no need to check whether in0 >= in1 - because in this case
     c='\000' but neither c0 nor c1 is '\000'
   *)
  let c = Bytes.unsafe_get in_buf in0 in
  if c = c0 || c = c1 then
    skip_chars in_buf (in0+1) in1 c0 c1
  else
    in0

let skip_spaces ic space0 space1 =
  (* Skip spaces: after this [in0] is a non-space char. *)
  let in1 = ic.in1 in
  let in0 = skip_chars ic.in_buf ic.in0 in1 space0 space1 in
  ic.in0 <- in0;
  if in0 >= in1 then (
    while ic.in0 >= ic.in1 do
      fill_in_buf ic;
      ic.in0 <- skip_chars ic.in_buf ic.in0 ic.in1 space0 space1;
    done
  )

let skip_spaces_eof_handler ic =
  (* If it is the first field, coming from [next()], the field is
     made of spaces.  If after the first, we are sure we read a
     delimiter before (but maybe the field is empty).  Thus add an
     empty field. *)
  let skip_it = field_skip_status ic in
  ( match skip_it with
      | `None 
      | `Empty ->
           ic.record <-  "" :: ic.record;
           ic.record_len <- ic.record_len+1;
      | `Skip ->
           ()
  );
  ic.input_field <- ic.input_field+1


let equal_sign_eof_handler ic =
  let skip_it = field_skip_status ic in
  ( match skip_it with
      | `None ->
           ic.record <-  "=" :: ic.record;
           ic.record_len <- ic.record_len+1;
      | `Empty ->
           ic.record <-  "" :: ic.record;
           ic.record_len <- ic.record_len+1;
      | `Skip ->
           ()
  );
  ic.input_field <- ic.input_field+1


(* We suppose to be at the beginning of a field.  Add the next field
   to [record].  @return [true] if more fields follow, [false] if
   the record is complete.

   space0, space1: two characters counting as space (may be identical chars)

   Raise  Failure if there is a format error.

   Raise End_of_line if there is no more data to read. In case of EOF,
   the caller must invoke eof_handler_ref, which is set to the right
   function for cleanup.
 *)
let rec add_next_field ic space0 space1 eof_handler_ref =
  let c = Bytes.unsafe_get ic.in_buf ic.in0 in
  if c = '\000' && ic.in0 >= ic.in1 then (
    fill_in_buf ic;
    add_next_field ic space0 space1 eof_handler_ref
  )
  else if c = '\"' && ic.quotes then (
    ic.in0 <- ic.in0 + 1;
    eof_handler_ref := `Quoted;
    add_quoted_field ic
  )
  else (
    let c =
      if c = space0 || c = space1 then (
        eof_handler_ref := `Skip_spaces;
        skip_spaces ic space0 space1;
        Bytes.unsafe_get ic.in_buf ic.in0
      )
      else c in
    (* Now, in0 < in1 or End_of_file was raised *)
    if c = '=' && ic.excel_tricks && ic.quotes then (
      ic.in0 <- ic.in0 + 1; (* mark '=' as read *)
      eof_handler_ref := `Equal_sign;
      fill_in_buf ic;
      if Bytes.unsafe_get ic.in_buf ic.in0 = '\"' then (
        (* Excel trick ="..." to prevent spaces around the field
           to be removed. *)
        ic.in0 <- ic.in0 + 1; (* skip '"' *)
        eof_handler_ref := `Quoted;
        add_quoted_field ic
      )
      else (
        Buffer.add_char ic.current_field '=';
        eof_handler_ref := `Unquoted;
        add_unquoted_field ic
      )
    )
    else (
      eof_handler_ref := `Unquoted;
      add_unquoted_field ic
    )
  )


let next_0 ic =
  if ic.in1 < 0 then raise(Sys_error "Bad file descriptor");
  fill_in_buf ic; (* or End_of_file which means no more records *)
  ic.record <- [];
  ic.record_len <- 0;
  ic.input_field <- 0;
  ic.record_n <- ic.record_n + 1; (* the current line being read *)
  ic.record_rev <- true;
  let eof_handler_ref = ref `Nothing in
  let space0 = ' ' in
  let space1 = if ic.separator = '\t' then space0 else '\t' in
  try
    let more_fields = ref true in
    while !more_fields do
      more_fields := add_next_field ic space0 space1 eof_handler_ref
    done
  with
    | End_of_file ->
         ( match !eof_handler_ref with
             | `Nothing -> ()
             | `Unquoted -> add_unquoted_field_eof_handler ic
             | `Quoted ->  add_quoted_field_eof_handler ic
             | `Skip_spaces -> skip_spaces_eof_handler ic
             | `Equal_sign -> equal_sign_eof_handler ic
         )

let next ic =
  next_0 ic;
  ic.record <- List.rev ic.record;
  ic.record_rev <- false;
  ic.record

let current_record ic = 
  if ic.record_rev then (
    ic.record <- List.rev ic.record;
    ic.record_rev <- false;
  );
  ic.record

let current_record_a ic =
  let n = ic.record_len in
  let array = Array.make n "" in

  let rec fill_fwd i l =
    match l with
      | x :: l' -> 
          if i >= n then assert false;
          Array.unsafe_set array i x; fill_fwd (i+1) l'
      | [] -> 
          if i <> n then assert false;
          () in

  let rec fill_bwd i l =
    match l with
      | x :: l' -> 
          if i < 0 then assert false;
          Array.unsafe_set array i x; fill_bwd (i-1) l'
      | [] -> 
          if i <> (-1) then assert false;
          () in

  if ic.record_rev then
    fill_bwd (n-1) ic.record
  else
    fill_fwd 0 ic.record;
  array

let next_a ic =
  next_0 ic;
  current_record_a ic


let fold_left f a0 ic =
  let a = ref a0 in
  try
    while true do
      a := f !a (next ic)
    done;
    assert false
  with End_of_file -> !a

let iter ~f ic =
  try  while true do f (next ic) done;
  with End_of_file -> ()

let input_all ic =
  List.rev(fold_left (fun l r -> r :: l) [] ic)

let fold_right f ic a0 =
  (* We to collect all records before applying [f] -- last row first. *)
  let lr = fold_left (fun l r -> r :: l) [] ic in
  List.fold_left (fun a r -> f r a) a0 lr


let load ?separator ?noquotes ?excel_tricks ?width ?skip ?buffer_size fname =
  let fh = if fname = "-" then stdin else open_in fname in
  let csv = 
    of_channel
      ?separator ?noquotes ?excel_tricks ?width ?skip 
      ?buffer_size fh in
  let t = input_all csv in
  close_in csv;
  t

let load_in ?separator ?noquotes ?excel_tricks ?width ?skip ?buffer_size ch =
  input_all
    (of_channel ?separator ?noquotes ?excel_tricks ?width ?skip ?buffer_size ch)

(* @deprecated *)
let load_rows ?separator ?noquotes ?excel_tricks ?width ?skip ?buffer_size 
              f ch =
  iter f 
    (of_channel ?separator ?noquotes ?excel_tricks ?width ?skip ?buffer_size ch)

(*
 * Output
 *)

(* FIXME: Rework this part *)
type out_channel = {
  out_chan : out_obj_channel;
  out_separator : char;
  out_separator_string : string;
  out_excel_tricks : bool;
}

let to_out_obj ?(separator=',') ?(excel_tricks=false) out_chan = {
  out_chan = out_chan;
  out_separator = separator;
  out_separator_string = String.make 1 separator;
  out_excel_tricks = excel_tricks;
}

let to_channel ?separator ?excel_tricks fh =
  to_out_obj ?separator ?excel_tricks
    (object
       val fh = fh
       method output s ofs len = output fh s ofs len; len
       method close_out () = close_out fh
     end)

let rec really_output oc s ofs len =
  let w = oc.out_chan#output s ofs len in
  if w < len then really_output oc s (ofs+w) (len-w)

let rec really_output_string oc s ofs len =
  let w = oc.out_chan#output (Bytes.unsafe_of_string s) ofs len in
  if w < len then really_output_string oc s (ofs+w) (len-w)

(* Determine whether the string s must be quoted and how many chars it
   must be extended to contain the escaped values.  Return -1 if there
   is no need to quote.  It is assumed that the string length [len]
   is > 0. *)
let must_quote separator excel_tricks s len =
  let quote = ref(is_space(String.unsafe_get s 0)
                  || is_space(String.unsafe_get s (len - 1))) in
  let n = ref 0 in
  for i = 0 to len - 1 do
    let c = String.unsafe_get s i in
    if c = separator || c = '\n' || c = '\r' then quote := true
    else if c = '"' || (excel_tricks && c = '\000') then (
      quote := true;
      incr n)
  done;
  if !quote then !n else -1

let need_excel_trick s len =
  len > 0 &&
    let c = String.unsafe_get s 0 in
    is_space c || c = '0' || is_space(String.unsafe_get s (len - 1))

(* Do some work to avoid quoting a field unless it is absolutely
   required. *)
let write_escaped oc field =
  let len = String.length field in
  if len > 0 then begin
    let use_excel_trick = oc.out_excel_tricks && need_excel_trick field len
    and n = must_quote oc.out_separator oc.out_excel_tricks field len in
    if n < 0 && not use_excel_trick then
      really_output_string oc field 0 len
    else (
      let field =
        if n <= 0 then field
        else (* There are some quotes to escape *)
          let s = Bytes.create (len + n) in
          let j = ref 0 in
          for i = 0 to len - 1 do
            let c = String.unsafe_get field i in
            if c = '"' then (
              Bytes.unsafe_set s !j '"'; incr j;
              Bytes.unsafe_set s !j '"'; incr j
            )
            else if oc.out_excel_tricks && c = '\000' then (
              Bytes.unsafe_set s !j '"'; incr j;
              Bytes.unsafe_set s !j '0'; incr j
            )
            else (Bytes.unsafe_set s !j c; incr j)
          done;
          Bytes.unsafe_to_string s
      in
      if use_excel_trick then really_output_string oc "=\"" 0 2
      else really_output_string oc "\"" 0 1;
      really_output_string oc field 0 (String.length field);
      really_output_string oc "\"" 0 1
    )
  end

let output_record oc = function
  | [] ->
      really_output_string oc "\n" 0 1
  | [f] ->
      write_escaped oc f;
      really_output_string oc "\n" 0 1
  | f :: tl ->
      write_escaped oc f;
      List.iter (fun f ->
                   really_output_string oc oc.out_separator_string 0 1;
                   write_escaped oc f;
                ) tl;
      really_output_string oc "\n" 0 1

let output_all oc t =
  List.iter (fun r -> output_record oc r) t

let print ?separator ?excel_tricks t =
  let csv = to_channel ?separator ?excel_tricks stdout in
  output_all csv t;
  flush stdout

let save_out ?separator ?excel_tricks ch t =
  let csv = to_channel ?separator ?excel_tricks ch in
  output_all csv t

let save ?separator ?excel_tricks fname t =
  let ch = open_out fname in
  let csv = to_channel ?separator ?excel_tricks ch in
  output_all csv t;
  close_out ch

(*
 * Acting on CSV data in memory
 *)

let lines = List.length

let columns csv =
  List.fold_left max 0 (List.map List.length csv)


let rec dropwhile f = function
  | [] -> []
  | x :: xs when f x -> dropwhile f xs
  | xs -> xs


let trim ?(top=true) ?(left=true) ?(right=true) ?(bottom=true) csv =
  let rec empty_row = function
    | [] -> true
    | x :: _ when x <> "" -> false
    | _ :: xs -> empty_row xs
  in
  let csv = if top then dropwhile empty_row csv else csv in
  let csv =
    if right then
      List.map (fun row ->
                  let row = List.rev row in
                  let row = dropwhile ((=) "") row in
                  let row = List.rev row in
                  row) csv
    else csv in
  let csv =
    if bottom then (
      let csv = List.rev csv in
      let csv = dropwhile empty_row csv in
      let csv = List.rev csv in
      csv
    ) else csv in

  let empty_left_cell =
    function [] -> true | x :: _ when x = "" -> true | _ -> false in
  let empty_left_col =
    List.fold_left (fun a row -> a && empty_left_cell row) true in
  let remove_left_col =
    List.map (function [] -> [] | _ :: xs -> xs) in
  let rec loop csv =
    if empty_left_col csv then
      remove_left_col csv
    else
      csv
  in

  let csv = if left then loop csv else csv in

  csv

let square csv =
  let columns = columns csv in
  List.map (
    fun row ->
      let n = List.length row in
      let row = List.rev row in
      let rec loop acc = function
        | 0 -> acc
        | i -> "" :: loop acc (i-1)
      in
      let row = loop row (columns - n) in
      List.rev row
  ) csv

let is_square csv =
  let columns = columns csv in
  List.for_all (fun row -> List.length row = columns) csv

let rec set_columns cols = function
  | [] -> []
  | r :: rs ->
      let rec loop i cells =
        if i < cols then (
          match cells with
          | [] -> "" :: loop (succ i) []
          | c :: cs -> c :: loop (succ i) cs
        )
        else []
      in
      loop 0 r :: set_columns cols rs

let rec set_rows rows csv =
  if rows > 0 then (
    match csv with
    | [] -> [] :: set_rows (pred rows) []
    | r :: rs -> r :: set_rows (pred rows) rs
  )
  else []

let set_size rows cols csv =
  set_columns cols (set_rows rows csv)

(* from extlib: *)
let rec drop n = function
  | _ :: l when n > 0 -> drop (n-1) l
  | l -> l

let sub r c rows cols csv =
  let csv = drop r csv in
  let csv = List.map (drop c) csv in
  let csv = set_rows rows csv in
  let csv = set_columns cols csv in
  csv

(* Compare two rows for semantic equality - ignoring any blank cells
 * at the end of each row.
 *)
let rec compare_row (row1 : string list) row2 =
  match row1, row2 with
  | [], [] -> 0
  | x :: xs, y :: ys ->
      let c = compare x y in
      if c <> 0 then c else compare_row xs ys
  | "" :: xs , [] ->
      compare_row xs []
  | _ :: _, [] ->
      1
  | [], "" :: ys ->
      compare_row [] ys
  | [], _ :: _ ->
      -1

(* Semantic equality for CSV files. *)
let rec compare (csv1 : t) csv2 =
  match csv1, csv2 with
  | [], [] -> 0
  | x :: xs, y :: ys ->
      let c = compare_row x y in
      if c <> 0 then c else compare xs ys
  | x :: xs, [] ->
      let c = compare_row x [] in
      if c <> 0 then c else compare xs []
  | [], y :: ys ->
      let c = compare_row [] y in
      if c <> 0 then c else compare [] ys

(* Concatenate - arrange left to right. *)
let rec concat = function
  | [] -> []
  | [csv] -> csv
  | left_csv :: csvs ->
      (* Concatenate the remaining CSV files. *)
      let right_csv = concat csvs in

      (* Set the height of the left and right CSVs to the same. *)
      let nr_rows = max (lines left_csv) (lines right_csv) in
      let left_csv = set_rows nr_rows left_csv in
      let right_csv = set_rows nr_rows right_csv in

      (* Square off the left CSV. *)
      let left_csv = square left_csv in

      (* Prepend the right CSV rows with the left CSV rows. *)
      List.map (
        fun (left_row, right_row) -> List.append left_row right_row
      ) (List.combine left_csv right_csv)

let to_array csv =
  Array.of_list (List.map Array.of_list csv)

let of_array csv =
  List.map Array.to_list (Array.to_list csv)

let associate header data =
  let nr_cols = List.length header in
  let rec trunc = function
    | 0, _ -> []
    | n, [] -> "" :: trunc (n-1, [])
    | n, (x :: xs) -> x :: trunc (n-1, xs)
  in
  List.map (
    fun row ->
      let row = trunc (nr_cols, row) in
      List.combine header row
  ) data


let save_out_readable chan csv =
  (* Escape all the strings in the CSV file first. *)
  (* XXX Why are we doing this?  I commented it out anyway.
  let csv = List.map (List.map String.escaped) csv in
  *)

  (* Find the width of each column. *)
  let widths =
    (* Don't consider rows with only a single element - typically
     * long titles.
     *)
    let csv = List.filter (function [_] -> false | _ -> true) csv in

    (* Square the CSV file - makes the next step simpler to implement. *)
    let csv = square csv in

    match csv with
      | [] -> []
      | row1 :: rest ->
          let lengths_row1 = List.map String.length row1 in
          let lengths_rest = List.map (List.map String.length) rest in
          let max2rows r1 r2 =
            let rp =
              try List.combine r1 r2
              with
                Invalid_argument "List.combine" ->
                  failwith (Printf.sprintf "Csv.save_out_readable: internal \
                              error: length r1 = %d, length r2 = %d"
                              (List.length r1) (List.length r2)) in
            List.map (fun ((a : int), (b : int)) -> max a b) rp
          in
          List.fold_left max2rows lengths_row1 lengths_rest in

  (* Print out each cell at the correct width. *)
  let rec repeat f = function
    | 0 -> ()
    | i -> f (); repeat f (i-1)
  in
  List.iter (
    function
    | [cell] ->                         (* Single column. *)
        output_string chan cell;
        output_char chan '\n'
    | row ->                            (* Other. *)
        (* Pair up each cell with its max width. *)
        let row =
          let rec loop = function
            | ([], _) -> []
            | (_, []) -> failwith "Csv.save_out_readable: internal error"
            | (cell :: cells, width :: widths) ->
                (cell, width) :: loop (cells, widths)
          in
          loop (row, widths) in
        List.iter (
          fun (cell, width) ->
            output_string chan cell;
            let n = String.length cell in
            repeat (fun () -> output_char chan ' ') (width - n + 1)
        ) row;
        output_char chan '\n'
  ) csv

let print_readable = save_out_readable stdout
