(** Support for interactive sessions *)

module Stypes : sig
  exception Incomplete_request
  exception Bad_request

  type live_object = ..

  (** The configuration of sessions *)
  module type SESSION_CONFIG =
    sig
      val stash_path : string
      (** The directory with the session stash *)
                         
      val is_interactive : bool
      (** Whether we are really interactive and want to be verbose *)
    end

  (** The stash stores data on disk so that it survives program restarts *)
  module type SESSION_STASH =
    sig
      module Config : SESSION_CONFIG
      (** The configuation of the stash *)

      type slot
      (** The stash consists of slots *)
             
      val slots : unit -> slot list
      (** List the slots available in the stash *)
                              
      val slot_index : slot -> int
      (** Get the (stable) index of the slot *)
                               
      val slot_mtime : slot -> float
      (** The time when the slot was last updated *)

      val slot_kind : slot -> string
      (** The kind of the slot is a string, and is conventionally the full
          OCaml module path of the module that can access this kind of data *)
                              
      val slot_param : slot -> Yojson.Safe.json
      (** A JSON parameter to be interpreted by the accessor module *)
                                    
      val slot_files : slot -> (string * (string * string)) list
      (** The files of the slot, given as triples [(name, (path, format))].
          The name is the logical name (e.g. "input.trainset"). The path
          is the absolute path to the file. The format is an aribitrary format
          identifier.
       *)

      val slot_objects : slot -> (string * live_object) list
      (** returns the slot objects (lists of named [live_object]). 
          Slot objects can be used to store non-persistent data in the
          slot, e.g. loaded files.
       *)

      val slot_comment : slot -> string
      (** A comment string associated with the slot *)

      val get_slot : int -> slot
      (** get slot by index *)
                                                        
      val delete_slot : slot -> unit
      (** A deleted slot does no longer exist in the file system, and any 
         further update is not written out. It is fully functional otherwise.
       *)
                                  
      val create_slot : string -> slot
      (** [create_slot kind] *)

      val copy_slot : slot -> int -> slot
      (** [copy_slot slot new_index] *)

      type slot_update
      val update : slot -> slot_update
      (** Starts the update of a slot *)

      val updated_slot : slot_update -> slot
                             
      val set_comment : slot_update -> string -> unit

      val set_param : slot_update -> Yojson.Safe.json -> unit
      (** Changes the parameter *)
                                                               
      val add_file : slot_update -> string -> string -> string
      (** [add_file update name format]: Sets the file [name] to a new file
          with [format]. The function returns the absolute name of a temporary
          file that is still empty and can be written by the caller.
       *)

      val add_object : slot_update -> string -> live_object -> unit
      (** [add_object update name obj] *)

      val delete_file : slot_update -> string -> unit
      val delete_object : slot_update -> string -> unit

      val commit : slot_update -> unit
      (** Writes the update out *)
                                 
      val abort : slot_update -> unit
      (** Cancels the update (deletes any temporary files) *)
                            
    end

  (** These are input files for algorithms. So far, the inputs are all in
      ztable format.
   *)
  module type SESSION_INPUT =
    sig
      module Config : SESSION_CONFIG
      module Stash : SESSION_STASH with module Config = Config

      type input
      val inputs : Stash.slot -> input list
      (** Get all inputs in this slot *)
      val get : Stash.slot -> string -> input
      (** Get input by logical name *)
                                       
      val input_name : input -> string
      (** Get the logical name of the input (not to be confused with the
          filename, e.g. the logical name could be "training_data" whereas
          the file name points to a zcontainer on disk with the real training
          data).
       *)
                                  
      val input_ztable : input -> Zcontainer.Ztable.ztable
      (** Get the ztable of the input *)

      val set_input : Stash.slot_update -> string -> Zcontainer.Ztable.ztable -> input
      (** Set an input to a ztable *)
      val delete_input : Stash.slot_update -> string -> unit
      (** Delete an input by logical name *)
    end

  (** Configures a slot kind *)
  module type ACCESSOR_CONFIG =
    sig
      val kind : string
        (** The accessor can process this kind of slot *)
                         
      val load : int -> unit
        (** Load the slot with this index *)

      val init : int -> unit
        (** Do any initializations for a fresh slot with this index *)
    end

  (** Basic UI functions *)
  module type CORE_UI =
    sig
      module Config : SESSION_CONFIG
      module Stash : SESSION_STASH with module Config = Config
      module Input : SESSION_INPUT with module Config = Config and
                                        module Stash = Stash
                      
      val slots : unit -> unit
        (** List on stdout which slots are available *)

      val resume : int -> unit
        (** Resume the slot with this index *)

      val create : unit -> int
        (** Create a new slot and return the index. The slot is initially
            empty *)

      val copy : int -> int -> unit
        (** Copy the slot with the existing index to the new index *)

      val current : unit -> int
        (** Return the index of the current slot. Creates a new slot if
            no slot is available yet.
         *)

      val delete : int -> unit
        (** Delete this slot *)
                              
      val current_slot : unit -> Stash.slot
        (** Return the current slot *)

      val inputs : unit -> unit
        (** List on stdout which inputs have been configured *)

      val get_input : name:string -> Input.input
        (** Get this input by name from the current slot *)
                             
      val load_ztable : name:string -> path:string -> unit
        (** [load_ztable input_name file]: Sets the input to a file. Also,
            the current slot is saved.
         *)

      val set_ztable : name:string -> Zcontainer.Ztable.ztable -> unit
        (** Set an input to a ztable, and saves the current slot *)

      val select_ztable : name:string -> unit
        (** Select a zcontainer interactively *)
                                                                 
      val get_ztable : name:string -> Zcontainer.Ztable.ztable
        (** Get an input by name *)

      val delete_input : name:string -> unit
        (** Deletes this input and saves the current slot *)

      val get_comment : unit -> string
        (** Get the comment string *)

      val set_comment : string -> unit
        (** Set the comment string *)
    end

end

module Core : sig
  module Make_Stash(SC : Stypes.SESSION_CONFIG) : Stypes.SESSION_STASH
  module Make_Input(SH : Stypes.SESSION_STASH) : Stypes.SESSION_INPUT
  module Make_UI(AC : Stypes.ACCESSOR_CONFIG)(SI : Stypes.SESSION_INPUT)
         : Stypes.CORE_UI
end


module Util :
sig
  type vector =
    (float, Bigarray.float64_elt, Bigarray.fortran_layout) Bigarray.Array1.t

  val write_vector : string -> vector -> unit
  val read_vector : string -> vector
  val write_zset : string -> Zcontainer.Zset.zset -> unit
  val read_zset : Zcontainer.Zset.pool -> string -> Zcontainer.Zset.zset

  val call_command : string -> string
  val get_line : string -> string

  val json_update : Yojson.Safe.json -> Yojson.Safe.json -> Yojson.Safe.json
                             
  end


module Config :
sig
  module Default : Stypes.SESSION_CONFIG
end
