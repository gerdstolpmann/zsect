open Zsession

module Config =
  struct
    let stash_path = "stash"
    let is_interactive = true
  end

module AccConfig =
  struct
    let kind = "testtype"
    let load _ = ()
    let init _ = ()
  end
    
module SH = Core.Make_Stash(Config)
module SI = Core.Make_Input(SH)
module UI = Core.Make_UI(AccConfig)(SI)
                   


(*
#use "topfind";;
#require "zsession";;
#directory "src/zsession/tests";;
#use "session1.ml";;

 *)
