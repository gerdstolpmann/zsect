open Printf
module U = Yojson.Safe.Util

let default_to_empty_list j =
  if j = `Null then
    `List []
  else
    j

             
module Make_Stash(SC : Stypes.SESSION_CONFIG) : Stypes.SESSION_STASH =
  struct
    module Config = SC

    type slot =
      { index : int;
        base : string;
        kind : string;
        mutable mtime : float;
        mutable comment : string;
        mutable param : Yojson.Safe.json;
        mutable files : (string * (string * string)) list;
        mutable objects : (string * Stypes.live_object) list;
        mutable exists : bool
      }

    type slot_update =
      { slot : slot;
        mutable upd_param : Yojson.Safe.json option;
        mutable upd_files : (string * (string * string)) list;
        mutable del_files : string list;
        mutable new_files : string list;
        mutable upd_objects : (string * Stypes.live_object) list;
        mutable del_objects : string list;
        mutable upd_comment : string option;
      }

    let current = Hashtbl.create 17

    let ensure_stash_exists() =
      if not (Sys.file_exists SC.stash_path) then
        ignore(Sys.command
                 (sprintf "mkdir -p %s" (Filename.quote SC.stash_path)));
      ()
                                 
    let load() =
      ensure_stash_exists();
      let files =
        Array.to_list (Sys.readdir SC.stash_path) in
      let files =
        List.filter (fun name -> Filename.check_suffix name ".slot") files in
      let files =
        List.map
          (fun name ->
             let base = Filename.chop_suffix name ".slot" in
             let index =
               try int_of_string base
               with Failure _ -> (-1) in
             (index,base)
          )
          files in
      let files =
        List.filter (fun (i,b) -> i >= 0) files in
      List.iter
        (fun (index,base) ->
           let json =
             Yojson.Safe.from_file
               (Filename.concat SC.stash_path (base ^ ".slot")) in
           let mtime = 
             json |> U.member "mtime" |> U.to_float in
           let do_read =
             try mtime > (Hashtbl.find current index).mtime
             with Not_found -> true in
           if do_read then (
             let kind = 
               json |> U.member "kind" |> U.to_string in
             let comment =
               try json |> U.member "comment" |> U.to_string
               with _ -> "" in
             let param = 
               json |> U.member "param" in
             let files =
               json |> U.member "files" |> U.to_list |>
                 (fun filejsons ->
                    List.map
                      (fun filejson -> 
                         let n = filejson |> U.member "name" |> U.to_string in
                         let p = filejson |> U.member "path" |> U.to_string in
                         let f = filejson |> U.member "format" |> U.to_string in
                         (n,(p,f))
                      )
                      filejsons
                 ) in
             let objects = [] in
             let exists = true in
             let slot =
               { index; base; comment; kind; mtime; param;
                 files; objects; exists } in
             Hashtbl.replace current index slot
           )
        )
        files

    let slots() =
      load();
      Hashtbl.fold (fun _ sl acc -> sl::acc) current []

    let slot_index sl = sl.index
    let slot_mtime sl = sl.mtime
    let slot_kind sl = sl.kind
    let slot_param sl = sl.param
    let slot_objects sl = sl.objects
    let slot_comment sl = sl.comment

    let slot_files sl =
      List.map
        (fun (n, (p, fmt)) ->
           let p = Filename.concat SC.stash_path p in
           (n, (p, (fmt)))
        )
        sl.files

    let get_slot i =
      List.find (fun sl -> sl.index = i) (slots())
                            
    let delete_slot sl =
      if sl.exists then (
        List.iter
          (fun (_,(p,_)) ->
             let fp = Filename.concat SC.stash_path p in
             try Sys.remove fp
             with Sys_error _ -> ()
          )
          sl.files;
        ( try Sys.remove (Filename.concat SC.stash_path (sl.base ^ ".slot"))
          with Sys_error _ -> ()
        );
        Hashtbl.remove current sl.index;
        sl.exists <- false
      )

    let write_slot sl ch =
      let filejsons =
        List.map
          (fun (n,(p,f)) ->
           `Assoc [ "name", `String n;
                    "path", `String p;
                    "format", `String f
                   ]
          )
          sl.files in
      let json =
        `Assoc [ "kind", `String sl.kind;
                 "mtime", `Float sl.mtime;
                 "param", sl.param;
                 "comment", `String sl.comment;
                 "files", `List filejsons
               ] in
      Yojson.Safe.to_channel ch json

    let random_id() =
      let f = open_in "/dev/urandom" in
      let b = Bytes.create 8 in
      really_input f b 0 8;
      close_in f;
      Netencoding.Base64.encode ~slash:'_' (Bytes.to_string b)

    let rec create_file k =
      try
        let fd =
          Unix.openfile
            (sprintf "%s/%d.slot" SC.stash_path k)
            [Unix.O_WRONLY; Unix.O_CREAT; Unix.O_EXCL]
            0o666 in
        (Unix.out_channel_of_descr fd, k, string_of_int k)
      with
        | Unix.Unix_error(Unix.EEXIST,_,_) ->
            create_file(k+1)
                                
    let create_slot kind =
      ensure_stash_exists();
      let ch, index, base = create_file 0 in
      try
        let mtime = Unix.time() in
        let exists = true in
        let objects = [] in
        let sl =
          { index; base; kind; mtime; param = `Null; files = []; objects;
            exists; comment = "" } in
        write_slot sl ch;
        close_out ch;
        sl
      with error ->
        close_out_noerr ch;
        raise error

    let copy_file oldp newp =
      let oldabs = sprintf "%s/%s" SC.stash_path oldp in
      let newabs = sprintf "%s/%s" SC.stash_path newp in
      let oldfd =
        Unix.openfile oldabs [Unix.O_RDONLY] 0 in
      try
        let newfd =
          Unix.openfile
            newabs [Unix.O_WRONLY; Unix.O_CREAT; Unix.O_EXCL ] 0o666 in
        try
          let bufsize = 65536 in
          let buf = Bytes.create bufsize in
          let n = ref 1 in
          while !n > 0 do
            n := Unix.read oldfd buf 0 bufsize;
            if !n > 0 then
              ignore(Unix.single_write newfd buf 0 !n)
          done;
          Unix.close newfd;
          Unix.close oldfd
        with
          | error ->
              Unix.close newfd;
              raise error
      with
        | error ->
            Unix.close oldfd;
            raise error

    let copy_slot sl i =
      ensure_stash_exists();
      if List.exists (fun sl -> sl.index = i) (slots()) then (
        if Config.is_interactive then
          eprintf "There is already a slot at index %d\n%!" i;
        raise Stypes.Bad_request
      );
      let ch, i', base = create_file i in
      try
        assert(i = i');
        let files' =
          List.map
            (fun (n,(p,f)) ->
              let id = random_id() in
              let p' = sprintf "%d.%s" i id in
              copy_file p p';
              (n,(p',f))
            )
            sl.files in
        let sl' =
          { index = i;
            base;
            kind = sl.kind;
            mtime = sl.mtime;
            param = sl.param;
            comment = sl.comment;
            files = files';
            objects = [];
            exists = true
          } in
        write_slot sl' ch;
        close_out ch;
        sl'
      with error ->
        close_out_noerr ch;
        raise error

    let update sl =
      { slot = sl;
        upd_param = None;
        del_files = [];
        upd_files = [];
        new_files = [];
        del_objects = [];
        upd_objects = [];
        upd_comment = None;
      }

    let set_param upd param =
      upd.upd_param <- Some param

    let add_file upd name format =
      let id = random_id() in
      let filename = sprintf "%s.%s" upd.slot.base id in
      let absname = sprintf "%s/%s" SC.stash_path filename in
      let ch =
        open_out_gen
          [Open_wronly; Open_creat; Open_excl]
          0o666
          absname in
      close_out ch;
      let upd_files, upd_overwritten =
        List.partition (fun (n,_) -> n <> name) upd.upd_files in
      let upd_files =
        (name, (filename, format)) :: upd_files in
      let del_files =
        List.map (fun (_,(p,_)) -> p) upd_overwritten @ upd.del_files in
      upd.del_files <- del_files;
      upd.upd_files <- upd_files;
      upd.new_files <- filename :: upd.new_files;
      absname

    let delete_file upd name =
      let upd_files, upd_to_delete =
        List.partition (fun (n,_) -> n <> name) upd.upd_files in
      let to_delete =
        List.filter (fun (n,_) -> n = name) upd.slot.files in
      let del_files =
        List.map (fun (_,(p,_)) -> p) (upd_to_delete@to_delete) @
          upd.del_files in
      upd.upd_files <- upd_files;
      upd.del_files <- del_files

    let add_object upd name obj =
      let upd_objects =
        List.filter (fun (n,_) -> n <> name) upd.upd_objects in
      upd.upd_objects <- (name,obj) :: upd_objects

    let delete_object upd name =
      upd.upd_objects <-
        List.filter (fun (n,_) -> n <> name) upd.upd_objects;
      upd.del_objects <- name :: upd.del_objects

    let rec apply_files_update files upd_files =
      match upd_files with
        | (n,_) as file :: upd_rest ->
            let files' =
              file :: List.filter (fun (n',_) -> n <> n') files in
            apply_files_update files' upd_rest
        | [] ->
            files

    let rec todel_files_update todel files upd_files =
      match files with
        | (n,(p,_)):: files' ->
            let todel' =
              if List.mem_assoc n upd_files then
                p :: todel
              else
                todel in
            todel_files_update todel' files' upd_files
        | [] ->
            todel

    let rec apply_objects_update objs upd_objs =
      match upd_objs with
        | (n,obj) :: upd_rest ->
            let objs' =
              (n,obj) :: List.filter (fun (n',_) -> n <> n') objs in
            apply_objects_update objs' upd_rest
        | [] ->
            objs

    let set_comment upd s =
      upd.upd_comment <- Some s
                                   
    let commit upd =
      ensure_stash_exists();
      let sl =
        { upd.slot with
          param = ( match upd.upd_param with
                           | None -> upd.slot.param
                           | Some kp -> kp
                       );
          files = apply_files_update upd.slot.files upd.upd_files;
          objects = apply_objects_update upd.slot.objects upd.upd_objects;
          exists = true;
          mtime = Unix.time();
          comment = ( match upd.upd_comment with
                        | None -> upd.slot.comment
                        | Some s -> s
                    );
        } in
      let todel =
        todel_files_update [] upd.slot.files upd.upd_files in
      let ch =
        open_out_gen
          [Open_wronly; Open_creat; Open_trunc]
          0o666
          (sprintf "%s/%s.slot" SC.stash_path upd.slot.base) in
      write_slot sl ch;
      close_out ch;
      upd.slot.param <- sl.param;
      upd.slot.files <- sl.files;
      upd.slot.objects <- sl.objects;
      List.iter
        (fun file ->
           try Sys.remove (sprintf "%s/%s" SC.stash_path file)
           with Sys_error _ -> ()
        )
        (upd.del_files @ todel)

    let abort upd =
      List.iter
        (fun file ->
           try Sys.remove (sprintf "%s/%s" SC.stash_path file)
           with Sys_error _ -> ()
        )
        upd.new_files;
      upd.upd_param <- None;
      upd.upd_files <- [];
      upd.del_files <- [];
      upd.new_files <- [];
      upd.upd_objects <- [];
      upd.del_objects <- [];
      upd.upd_comment <- None

    let updated_slot upd = upd.slot
  end
    

let rec paths tab = (* Not_found if there is a memory tab somewhere *)
  match Zcontainer.Ztable.path_name tab with
    | None ->
        raise Not_found
    | Some file ->
        ( match Zcontainer.Ztable.underlying_ztable tab with
            | None ->
                [file]
            | Some tab ->
                file :: paths tab
            )
          

module Make_Input(SH : Stypes.SESSION_STASH) : Stypes.SESSION_INPUT =
  struct
    module Config = SH.Config
    module Stash = SH
    type input = string * Zcontainer.Ztable.ztable
    type Stypes.live_object += I of input list

    let rec load paths =
      match paths with
        | [] -> assert false
        | [file] ->
            Zcontainer.Ztable.from_file Zcontainer.Zrepr.stdrepr file
        | file :: paths ->
            let over = load paths in
            Zcontainer.Ztable.from_file ~over Zcontainer.Zrepr.stdrepr file

    let inputs sl =
      let objects = Stash.slot_objects sl in
      try
        match List.assoc "input" objects with
          | I inlist -> inlist
          | _ -> raise Not_found
      with
        | Not_found ->
            let p = Stash.slot_param sl in
            let p = if p=`Null then `Assoc [] else p in
            let l =
              p |> U.member "input" |> default_to_empty_list |> U.to_list in
            let inlist =
              List.map
                (fun j ->
                   let name = j |> U.member "name" |> U.to_string in
                   let paths = j |> U.member "paths" |> U.to_list |>
                                 List.map U.to_string in
                   (name, load paths)
                )
                l in
            let upd = Stash.update sl in
            Stash.add_object upd "input" (I inlist);
            Stash.commit upd;
            inlist

    let get sl name =
      (name, List.assoc name (inputs sl))

    let input_name (name,_) = name
    let input_ztable (_,tab) = tab

    let set_input upd name ztab =
      let sl = Stash.updated_slot upd in
      let inlist = inputs sl in
      let inlist = List.filter (fun (n,_) -> n <> name) inlist in
      let inlist = (name,ztab) :: inlist in
      Stash.add_object upd "input" (I inlist);
      try
        let ztab_paths = paths ztab in  (* or Not_found *)
        let p = Stash.slot_param sl in
        let p = if p=`Null then `Assoc [] else p in
        let l = 
          p |> U.member "input" |> default_to_empty_list |> U.to_list in
        let l = List.filter
                  (fun j -> j |> U.member "name" |> U.to_string <> name)
                  l in
        let j =
          `Assoc [ "name", `String name;
                   "paths", `List (List.map (fun s -> `String s) ztab_paths)
                 ]  in
        let l = j :: l in
        let p =
          Util.json_update p (`Assoc ["input", `List l]) in
        Stash.set_param upd p;
        (name,ztab)
      with
        | Not_found ->
            eprintf "WARNING set_input: the table contains memory-only data \
                     that cannot be saved to disk\n%!";
            (name,ztab)

    let delete_input upd name =
      let sl = Stash.updated_slot upd in
      let inlist = inputs sl in
      let inlist = List.filter (fun (n,_) -> n <> name) inlist in
      Stash.add_object upd "input" (I inlist);
      let p = Stash.slot_param sl in
      let l =
        try p |> U.member "input" |> U.to_list
        with U.Type_error _ -> [] in
      let l = List.filter
                (fun j -> j |> U.member "name" |> U.to_string <> name)
                l in
      let p = Util.json_update p (`Assoc ["input", `List l]) in
      Stash.set_param upd p
  end
    

module Make_UI(AC : Stypes.ACCESSOR_CONFIG)(SI : Stypes.SESSION_INPUT)
              : Stypes.CORE_UI =
  struct
    module Config = SI.Config
    module Stash = SI.Stash
    module Input = SI

    let curslot = ref None

    let is_current sl =
      match !curslot with
        | None -> false
        | Some cursl -> Stash.slot_index cursl = Stash.slot_index sl
                      
    let print_slot sl =
      eprintf
        "%2d: %-20s %-7s %s\n"
        (Stash.slot_index sl)
        (Stash.slot_kind sl)
        (if Stash.slot_kind sl = AC.kind then (
           if is_current sl then
             "CURRENT"
           else
             "AVAIL"
         ) else
           "UNAVAIL"
        )
        (Netdate.mk_mail_date ~localzone:true (Stash.slot_mtime sl));
      eprintf
        "  : %s\n"
        (if Stash.slot_comment sl = "" then
           "(no comment)"
         else
           Stash.slot_comment sl
        )
                     
    let slots() =
      let l = Stash.slots() in
      let l =
        List.sort
          (fun sl1 sl2 -> Stash.slot_index sl1 - Stash.slot_index sl2)
          l in
      if l = [] then
        eprintf "No slots.\n%!"
      else (
        List.iter print_slot l;
        eprintf "%!"
      )

    let create() =
      let sl = Stash.create_slot AC.kind in
      AC.init (Stash.slot_index sl);
      curslot := Some sl;
      if Config.is_interactive then (
        eprintf "Created: ";
        print_slot sl;
        eprintf "%!"
      );
      Stash.slot_index sl

    let copy i j =
      let sl =
        try
          List.find (fun sl -> Stash.slot_index sl = i) (Stash.slots())
        with
          | Not_found ->
              if Config.is_interactive then
                eprintf "No slot with index %d. Call slots() to get a list of \
                         available slots\n%!" i;
              raise Stypes.Bad_request in
      ignore(Stash.copy_slot sl j);
      if Config.is_interactive then
        eprintf "Copied slot %d to %d\n%!" i j;
      ()


    let current() =
      match !curslot with
        | None -> create()
        | Some sl -> Stash.slot_index sl

    let rec current_slot() =
      match !curslot with
        | None -> ignore(create()); current_slot()
        | Some sl -> sl

    let resume i =
      try
        let sl =
          List.find (fun sl -> Stash.slot_index sl = i) (Stash.slots()) in
        if Stash.slot_kind sl <> AC.kind then (
          if Config.is_interactive then
            eprintf "Slot at index %d is of incompatible kind\n%!" i;
          raise Stypes.Bad_request
        );
        curslot := Some sl;
        AC.load i;
        if Config.is_interactive then (
          eprintf "Resumed: ";
          print_slot sl;
          eprintf "%!"
        );
      with
        | Not_found ->
            if Config.is_interactive then
              eprintf "No slot with index %d. Call slots() to get a list of \
                       available slots\n%!" i;
            raise Stypes.Bad_request
        
    let delete i =
      let sl =
        try
          List.find (fun sl -> Stash.slot_index sl = i) (Stash.slots())
        with
          | Not_found ->
              if Config.is_interactive then
                eprintf "No slot with index %d. Call slots() to get a list of \
                         available slots\n%!" i;
              raise Stypes.Bad_request in
      Stash.delete_slot sl;
      if Config.is_interactive then (
          eprintf "Deleted: ";
          print_slot sl;
          eprintf "%!"
      );
      match !curslot with
        | None -> ()
        | Some cursl ->
            if Stash.slot_index cursl = i then (
              curslot := None;
              if Config.is_interactive then (
                eprintf "This was the selected slot. \
                         Nothing is selected anymore.\n%!"
              )
            )

    let print_input inp =
      try
        let pl = paths (Input.input_ztable inp) in (* or Not_found *)
        List.iteri
          (fun k p ->
           eprintf
             "      %s: ztable %s\n"
             (if k = 0 then Input.input_name inp else "  (over)")
             p
          )
          pl
      with
        | Not_found ->
            eprintf
              "      %s: in-memory ztable (at least partly)\n"
              (Input.input_name inp)
        
                                                 
    let inputs() =
      let sl = current_slot() in
      let l = Input.inputs sl in
      let l =
        List.sort
          (fun inp1 inp2 ->
             String.compare (Input.input_name inp1) (Input.input_name inp2)
          )
          l in
      if l = [] then
        eprintf "No inputs.\n%!"
      else (
        List.iter print_input l;
        eprintf "%!"
      )

    let get_input ~name =
      try
        Input.get (current_slot()) name
      with
        | Not_found ->
            if Config.is_interactive then
              eprintf "No such input: %s. For a list call inputs()\n%!" name;
            raise Stypes.Bad_request

    let set_ztable ~name ztab =
      let sl = current_slot() in
      let upd = Stash.update sl in
      let inp = Input.set_input upd name ztab in
      Stash.commit upd;
      if Config.is_interactive then (
        eprintf "Setting: ";
        print_input inp;
        eprintf "%!"
      )

    let get_ztable ~name =
      let inp = get_input ~name in
      Input.input_ztable inp

    let load_ztable ~name ~path =
      let ztab = Zcontainer.Ztable.from_file Zcontainer.Zrepr.stdrepr path in
      set_ztable ~name ztab

    let select_ztable ~name =
      let text =
        Util.call_command
          "zenity --file-selection --directory \
           --title='Select zcontainer directory' 2>/dev/null" in
      let path = Util.get_line text in
      load_ztable ~name ~path
            
    let delete_input ~name =
      ignore(get_input ~name);
      let sl = current_slot() in
      let upd = Stash.update sl in
      Input.delete_input upd name;
      Stash.commit upd

    let get_comment() =
      let sl = current_slot() in
      Stash.slot_comment sl

    let set_comment s =
      let sl = current_slot() in
      let upd = Stash.update sl in
      Stash.set_comment upd s;
      Stash.commit upd
                 
end
