module Default =
  struct
    let is_interactive = true

    let stash_path =
      try Sys.getenv "ZSECT_STASH"
      with
        | Not_found ->
            try
              Unix.((getpwnam (getlogin())).pw_dir) ^ "/.zsect"
            with
              | Unix.Unix_error _ ->
                  "/tmp"
                                                
  end
