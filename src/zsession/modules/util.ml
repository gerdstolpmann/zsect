open Printf
module StrSet = Set.Make(String)
       
type vector =
  (float, Bigarray.float64_elt, Bigarray.fortran_layout) Bigarray.Array1.t

let write_vector_fd fd (vector:vector) =
  let mem = Netsys_mem.memory_of_bigarray_1 vector in
  let size = Bigarray.Array1.dim mem in
  let k = ref 0 in
  while !k < size do
    let n = Netsys_mem.mem_write fd mem !k (size - !k) in
    k := !k + n
  done

let write_vector filename vector =
  let fd =
    Unix.openfile filename [Unix.O_WRONLY; Unix.O_CREAT; Unix.O_TRUNC] 0o666 in
  try
    write_vector_fd fd vector;
    Unix.close fd
  with error -> Unix.close fd; raise error
    
let read_vector_fd fd : vector =
  let vec1 =
    Bigarray.Array1.map_file
      fd Bigarray.float64 Bigarray.fortran_layout false (-1) in
  let n = Bigarray.Array1.dim vec1 in
  let vec2 =
    Bigarray.Array1.create Bigarray.float64 Bigarray.fortran_layout n in
  Bigarray.Array1.blit vec1 vec2;
  vec2

let read_vector filename =
  let fd =
    Unix.openfile filename [Unix.O_RDONLY] 0 in
  try
    let vec = read_vector_fd fd in
    Unix.close fd;
    vec
  with error -> Unix.close fd; raise error
                                     
let write_zset filename zset =
  let f =
    open_out_gen [Open_wronly; Open_creat; Open_trunc] 0o666 filename in
  try
    Zcontainer.Zset.write_tf f zset;
    close_out f
  with error -> close_out_noerr f; raise error

let read_zset pool filename =
  let f = open_in_gen [Open_rdonly] 0 filename in
  try
    (* FIXME: restore the pool! *)
    ignore(Zcontainer.Zset.read_enctype_tf f);
    let zset = Zcontainer.Zset.read_tf pool f in
    close_in f;
    zset
  with error -> close_in f; raise error

let break_signal() =
  if !Sys.interactive then
    Sys.set_signal Sys.sigint (Sys.Signal_handle (fun _ -> raise Sys.Break))
                                  
let call_command cmd_text =
  (* FIXME: signal handling. Problem is that Ocamlnet isn't nice to the
     Sys.Break configuration of the toploop.
   *)
  if !Sys.interactive then
    Netsys_signal.register_handler
      ~keep_default:false
      ~name:"ctrl-c"
      ~signal:Sys.sigint
      ~callback:(fun _ -> ()) ();
  Netsys_signal.restore_management Sys.sigint;
  let cmd =
    Shell.cmd "/bin/sh" [ "-c"; ("exec " ^ cmd_text) ] in
  let outbuf = Buffer.create 80 in
  let outbufc = Shell.to_buffer outbuf in
  let errbuf = Buffer.create 80 in
  let errbufc = Shell.to_buffer errbuf in
  try
    Shell.call ~stdout:outbufc ~stderr:errbufc [cmd];
    break_signal();
    Buffer.contents outbuf
  with
    | Shell.Subprocess_error [ _, Unix.WEXITED n ] ->
        eprintf "Exit code %d.\n" n;
        eprintf "%s\n%!" (Buffer.contents errbuf);
        break_signal();
        raise Stypes.Bad_request

let get_line text =
  if text <> "" && text.[String.length text - 1] = '\n' then
    String.sub text 0 (String.length text - 1)
  else
    text
      
let json_update j1 j2 =
  match j1, j2 with
    | `Assoc l1, `Assoc l2 ->
        let set =
          List.fold_left (fun acc (n,_) -> StrSet.add n acc) StrSet.empty l2 in
        let l1' =
          List.filter (fun (n,_) -> not (StrSet.mem n set)) l1 in
        `Assoc(l1' @ l2)
    | _ -> j1
