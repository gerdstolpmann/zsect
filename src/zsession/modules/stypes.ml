exception Incomplete_request
exception Bad_request
            
type live_object = ..
                     
module type SESSION_CONFIG =
  sig
    val stash_path : string
    val is_interactive : bool
  end

module type SESSION_STASH =
  sig
    module Config : SESSION_CONFIG

    type slot
    val slots : unit -> slot list
    val slot_index : slot -> int
    val slot_mtime : slot -> float
    val slot_kind : slot -> string
    val slot_param : slot -> Yojson.Safe.json
    val slot_files : slot -> (string * (string * string)) list
    val slot_objects : slot -> (string * live_object) list
    val slot_comment : slot -> string
    val get_slot : int -> slot
    val delete_slot : slot -> unit
    val create_slot : string -> slot
    val copy_slot : slot -> int -> slot

    type slot_update
    val update : slot -> slot_update
    val updated_slot : slot_update -> slot
    val set_comment : slot_update -> string -> unit
    val set_param : slot_update -> Yojson.Safe.json -> unit
    val add_file : slot_update -> string -> string -> string
    val add_object : slot_update -> string -> live_object -> unit
    val delete_file : slot_update -> string -> unit
    val delete_object : slot_update -> string -> unit

    val commit : slot_update -> unit
    val abort : slot_update -> unit
  end

module type SESSION_INPUT =
  sig
    module Config : SESSION_CONFIG
    module Stash : SESSION_STASH with module Config = Config

    type input
    val inputs : Stash.slot -> input list
    val get : Stash.slot -> string -> input

    val input_name : input -> string
    val input_ztable : input -> Zcontainer.Ztable.ztable

    val set_input : Stash.slot_update -> string -> Zcontainer.Ztable.ztable -> input
    val delete_input : Stash.slot_update -> string -> unit
  end

module type ACCESSOR_CONFIG =
  sig
    val kind : string
    val load : int -> unit
    val init : int -> unit
  end

module type CORE_UI =
  sig
    module Config : SESSION_CONFIG
    module Stash : SESSION_STASH with module Config = Config
    module Input : SESSION_INPUT with module Config = Config and
                                      module Stash = Stash

    val slots : unit -> unit
    val resume : int -> unit
    val create : unit -> int
    val copy : int -> int -> unit
    val current : unit -> int
    val delete : int -> unit
    val current_slot : unit -> Stash.slot
    val inputs : unit -> unit
    val get_input : name:string -> Input.input
    val load_ztable : name:string -> path:string -> unit
    val set_ztable : name:string -> Zcontainer.Ztable.ztable -> unit
    val select_ztable : name:string -> unit
    val get_ztable : name:string -> Zcontainer.Ztable.ztable
    val delete_input : name:string -> unit
    val get_comment : unit -> string
    val set_comment : string -> unit
  end
