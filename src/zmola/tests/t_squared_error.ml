open Printf
open T_frame.Util
open Zmola
open Squared_error_types
open Squared_error
open Zcontainer

module StringSet = Squared_error.StdSet(String)
module StringMap = Squared_error.StdMap(String)
module SQE = Squared_error.Make_Squared_Error(String)(StringSet)(StringMap)

let compute_sqe l =
  let sum = List.fold_left (fun acc x -> acc+.x) 0.0 l in
  let count = List.length l in
  let avg = sum /. float count in
  List.fold_left (fun acc x -> acc +. (x -. avg) *. (x -. avg)) 0.0 l

let is_circa x y =
  (* printf "x=%f y=%f\n%!" x y; *)
  abs_float(x -. y) < 0.00001

let ( =~ ) = is_circa


let test_001() =
  let pool = Zset.default_pool in
  let space = Ztypes.create_space 10 in
  let vector =
    Vector.of_array [| 0.0; 1.0; 2.0; 3.0; 4.0; 5.0; 6.0; 7.0; 8.0; 9.0 |] in
  let sets = [ "A", Zset.from_array pool space [| 3; 4 |];
               "B", Zset.from_array pool space [| 5; 6 |];
               "C", Zset.from_array pool space [| 7; 8; 9 |];
               "D", Zset.from_array pool space [| 10 |];
             ] in
  let sqe = SQE.create vector sets in
  SQE.check sqe;
  let all_iset = SQE.all sqe in
  let a_iset = SQE.IndexSet.of_list [ "A" ] in
  let ab_iset = SQE.IndexSet.of_list [ "A"; "B" ] in
  let ac_iset = SQE.IndexSet.of_list [ "A"; "C" ] in
  let abc_iset = SQE.IndexSet.of_list [ "A"; "B"; "C" ] in
  let a_stats = SQE.stats sqe a_iset in
  let ab_stats = SQE.stats sqe ab_iset in
  let ac_stats = SQE.stats sqe ac_iset in
  let abc_stats = SQE.stats sqe abc_iset in

  named "count_all" (SQE.count sqe all_iset = 8) &&&
    named "sum_all" (SQE.sum sqe all_iset =~ 44.0) &&&
    named "avg_all" (SQE.avg sqe all_iset =~ 5.5) &&&
    named "sqe_all" (SQE.squared_error sqe all_iset =~ 42.0) &&&
    
    named "count_A" (SQE.count sqe a_iset = 2) &&&
    named "sum_A" (SQE.sum sqe a_iset =~ 5.0) &&&
    named "avg_A" (SQE.avg sqe a_iset =~ 2.5) &&&
    named "sqe_A" (SQE.squared_error sqe a_iset =~ 0.5) &&&

    named "count_AB" (SQE.count sqe ab_iset = 4) &&&
    named "sum_AB" (SQE.sum sqe ab_iset =~ 14.0) &&&
    named "avg_AB" (SQE.avg sqe ab_iset =~ 3.5) &&&
    named "sqe_AB" (SQE.squared_error sqe ab_iset =~ 5.0) &&&

    named "count_ABC" (SQE.count sqe abc_iset = 7) &&&
    named "sum_ABC" (SQE.sum sqe abc_iset =~ 35.0) &&&
    named "avg_ABC" (SQE.avg sqe abc_iset =~ 5.0) &&&
    named "sqe_ABC" (SQE.squared_error sqe abc_iset =~ 28.0) &&&

    named "a_stats" (SQE.stats_error a_stats =~ 0.5) &&&
    named "ab_stats" (SQE.stats_error ab_stats =~ 5.0) &&&
    named "ac_stats" (SQE.stats_error ac_stats =~ 26.8) &&&
    named "abc_stats" (SQE.stats_error abc_stats =~ 28.0) &&&

    named "sqe_A_AB" (SQE.stats_error (SQE.stats_add sqe a_stats "B") =~ 5.0) &&&
    named "sqe_AB_ABC" (SQE.stats_error (SQE.stats_add sqe ab_stats "C") =~ 28.0) &&&
    named "sqe_ABC_AB" (SQE.stats_error (SQE.stats_sub sqe abc_stats "C") =~ 5.0) &&&
    named "sqe_AB_AC" (SQE.stats_error (SQE.stats_update sqe ab_stats ac_iset) =~ 26.8)

let tests =
  [ ("test_001", test_001) ]

