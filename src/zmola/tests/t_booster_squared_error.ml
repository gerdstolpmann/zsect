open Printf
open T_frame.Util
open Zmola
open Types
open Zcontainer

module Basic = struct
  module DS1 =
    (* only color *)
    struct
      type dataset = Ztable.ztable
      type col_hint = { col_ordinal : bool;
                        (* col_monotonic : [`Increasing|`Decreasing] option; *)
                      }
      let ztable ztab = ztab
      let x_columns _ = [ "color", {col_ordinal=false; (*col_monotonic=None*)} ]
      let y_column _ = "x"
    end

  module DS2 =
    (* color + direction *)
    struct
      type dataset = Ztable.ztable
      type col_hint = { col_ordinal : bool;
                        (* col_monotonic : [`Increasing|`Decreasing] option; *)
                      }
      let ztable ztab = ztab
      let x_columns _ = [ "color", {col_ordinal=false; (*col_monotonic=None*)};
                          "direction", {col_ordinal=false; (*col_monotonic=None*)};
                        ]
      let y_column _ = "x"
    end

  let cols =
    [| ( "color",
         Zdigester.ParserBox(Ztypes.Zstring, (fun s -> s)),
         "IM/string",
         "string_asc"
       );
       ( "direction",
         Zdigester.ParserBox(Ztypes.Zstring, (fun s -> s)),
         "IM/string",
         "string_asc"
       );
       ( "x",
         Zdigester.ParserBox(Ztypes.Zfloat, float_of_string),
         "DF64/float",
         "float_asc"
       );
      |]

  let create_ztable1() =
    let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
    Zdigester.build_add dg [| "red";   "down"; "5.0" |];
    Zdigester.build_add dg [| "red";   "down"; "5.1" |];
    Zdigester.build_add dg [| "red";   "up";   "5.2" |];
    Zdigester.build_add dg [| "green"; "down"; "9.8" |];
    Zdigester.build_add dg [| "green"; "down"; "10.2" |];
    Zdigester.build_add dg [| "green"; "up";   "10.5" |];
    Zdigester.build_add dg [| "blue";  "up";   "20.5" |];
    Zdigester.build_add dg [| "blue";  "up";   "21.2" |];
    Zdigester.build_add dg [| "blue";  "down"; "17.8" |];
    Zdigester.build_end dg

  let colname col =
    let Ztypes.ZcolDescr(n,_,_,_) = col in
    n

  let coltype col =
    let Ztypes.ZcolDescr(_,ty,_,_) = col in
    ty

  let colval col v =
    Ztypes.debug_string_of_value (coltype col) v

  let test_001() =
    (* Take only color as x column *)
    let module L = Loss.Squared_error(DS1) in
    let module BL_step = Blearner_sqe_bisection.Step(DS1) in
    let module BL_config = struct let max_depth = 5 end in
    let module BL = Blearner.Tree_learner(BL_config)(BL_step) in
    let module EL = Booster.Tree_Gradient_Booster(L)(BL) in
    let pool = Zset.default_pool in
    let ztab = create_ztable1() in
    let trainset = Zset.all pool (Ztable.space ztab) in
    let s0 = EL.start ztab trainset in
    let s1 = EL.advance s0 in
    let m1 = EL.model s1 in
    let l1 = EL.loss s1 in
    let s2 = EL.advance s1 in
    let l2 = EL.loss s2 in
    (match m1 with
       | Inner [_, 
                Inner [ E_Or[E_ColCmp(col1,_,v1)], Leaf g1;
                        E_Or[E_ColCmp(col2,_,v2)], Leaf g2
                      ];
                E_Or[E_ColCmp(col3,_,v3)], Leaf g3
               ] ->
           named "col1" (colname col1 = "color") &&&
           named "v1" (colval col1 v1 = "\"red\"") &&&
             named "col2" (colname col2 = "color") &&&
             named "v2" (colval col2 v2 = "\"green\"") &&&
             named "col3" (colname col3 = "color") &&&
             named "v3" (colval col3 v3 = "\"blue\"") &&&
             named "l1" (l1 < 3.36)
       | _ ->
           Tree.print_tree stderr m1;
           named "m1" false
    ) &&&
      named "l2" (abs_float(l1 -. l2) < 0.00001)
        (* converges in one go *)


  let test_002() =
    (* Take color and direction as x columns *)
    let module L = Loss.Squared_error(DS2) in
    let module BL_step = Blearner_sqe_bisection.Step(DS2) in
    let module BL_config = struct let max_depth = 5 end in
    let module BL = Blearner.Tree_learner(BL_config)(BL_step) in
    let module EL = Booster.Tree_Gradient_Booster(L)(BL) in
    let pool = Zset.default_pool in
    let ztab = create_ztable1() in
    let trainset = Zset.all pool (Ztable.space ztab) in
    let s0 = EL.start ztab trainset in
    let s1 = EL.advance s0 in
    let m1 = EL.model s1 in
    let l1 = EL.loss s1 in
    let s2 = EL.advance s1 in
    let l2 = EL.loss s2 in
    (match m1 with
       | Inner [_, 
                Inner [ E_Or[E_ColCmp(col1,_,v1)],
                        Inner [ E_Or[E_ColCmp(col2,_,v2)], Leaf _;
                                E_Or[E_ColCmp(col3,_,v3)], Leaf _ ];

                        E_Or[E_ColCmp(col4,_,v4)],
                        Inner [ E_Or[E_ColCmp(col5,_,v5)], Leaf _;
                                E_Or[E_ColCmp(col6,_,v6)], Leaf _ ];

                      ];
                E_Or[E_ColCmp(col7,_,v7)],
                Inner [ E_Or[E_ColCmp(col8,_,v8)], Leaf _;
                        E_Or[E_ColCmp(col9,_,v9)], Leaf _ ];
               ] ->
           named "col1" (colname col1 = "color") &&&
           named "v1" (colval col1 v1 = "\"red\"") &&&
             named "col2" (colname col2 = "direction") &&&
             named "v2" (colval col2 v2 = "\"down\"") &&&
             named "col3" (colname col3 = "direction") &&&
             named "v3" (colval col3 v3 = "\"up\"") &&&
             named "col4" (colname col4 = "color") &&&
             named "v4" (colval col4 v4 = "\"green\"") &&&
             named "col5" (colname col5 = "direction") &&&
             named "v5" (colval col5 v5 = "\"down\"") &&&
             named "col6" (colname col6 = "direction") &&&
             named "v6" (colval col6 v6 = "\"up\"") &&&
             named "col7" (colname col7 = "color") &&&
             named "v7" (colval col7 v7 = "\"blue\"") &&&
             named "col8" (colname col8 = "direction") &&&
             named "v8" (colval col8 v8 = "\"down\"") &&&
             named "col9" (colname col9 = "direction") &&&
             named "v9" (colval col9 v9 = "\"up\"") &&&
             named "l1" (l1 < 0.17)
       | _ ->
           Tree.print_tree stderr m1;
           named "m1" false
    ) &&&
      named "l2" (abs_float(l1 -. l2) < 0.00001)
        (* converges in one go *)


  let test_003() =
    (* Take color and direction as x columns, but now limit the depth of the
       tree to 2. So two boosting steps are now needed until convergence.
     *)
    let module L = Loss.Squared_error(DS2) in
    let module BL_step = Blearner_sqe_bisection.Step(DS2) in
    let module BL_config = struct let max_depth = 2 end in (* here! *)
    let module BL = Blearner.Tree_learner(BL_config)(BL_step) in
    let module EL = Booster.Tree_Gradient_Booster(L)(BL) in
    let pool = Zset.default_pool in
    let ztab = create_ztable1() in
    let trainset = Zset.all pool (Ztable.space ztab) in
    let s0 = EL.start ztab trainset in
    let s1 = EL.advance s0 in
    let l1 = EL.loss s1 in
    let s2 = EL.advance s1 in
    let l2 = EL.loss s2 in
    let s3 = EL.advance s2 in
    let l3 = EL.loss s3 in
    named "l1" (l1 > 0.25 && l1 < 0.26) &&&
      named "l2" (l2 > 0.16 && l2 < 0.175) &&&
      named "l3" (l3 > 0.16 && l3 < 0.17)
end


module Sine = struct
  let cols =
    [| ( "x",
         Zdigester.ParserBox(Ztypes.Zint, int_of_string),
         "IM/int",
         "int_asc"
       );
       ( "y",
         Zdigester.ParserBox(Ztypes.Zfloat, float_of_string),
         "DF64/float",
         "float_asc"
       );
      |]


  let pi = 3.141592

  let error() =
    (* some normal distributed error using Box-Muller *)
    let var = 0.01 in
    let u = Random.float 1.0 in
    let v = Random.float 1.0 in
    sqrt(var) *. sqrt(-2.0 *. log u) *. cos(2.0 *. pi *. v)

  let create_data f ~enable_noise ~rounds ~size =
    for r = 1 to rounds do
      for i = 0 to size-1 do
        let k = i - size/2 in
        let x = float k *. pi /. 100.0 in
        let y = sin x +. (if enable_noise then error() else 0.0) in
        f k x y
      done;
    done


  let create_table ~enable_noise ~rounds ~size =
    let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
    create_data
      (fun k x y ->
       let dg_vec = [| string_of_int k; string_of_float y |] in
       Zdigester.build_add dg dg_vec;
      )
      ~enable_noise ~rounds ~size;
    Zdigester.build_end dg

  module DS =
    struct
      type dataset = Ztable.ztable
      type col_hint = { col_ordinal : bool;
                        (* col_monotonic : [`Increasing|`Decreasing] option;*)
                      }
      let ztable ztab = ztab
      let x_columns _ = [ "x", {col_ordinal=true; (*col_monotonic=None*)};
                        ]
      let y_column _ = "y"
    end

  module L = Loss.Squared_error(DS)
  module BL_step = Blearner_sqe_bisection.Step(DS)
  module BL_config = struct let max_depth = 3 end
  module BL = Blearner.Tree_learner(BL_config)(BL_step)
  module EL = Booster.Tree_Gradient_Booster(L)(BL)
  module F = Folder.Data_folds(DS)
  module Loop = Booster_loop.Loop(EL)

  let test_001() =
    (* Config *)
    let rate = 0.5 in
    let stages = 100 in
    (* Create the noisy dataset: *)
    let ds =
      create_table
        ~enable_noise:true
        ~rounds:5
        ~size:10 in
    (* Approximate it, yielding [total_model]: *)
    let folds = 2 in
    let pool = Zset.create_pool ~enctype:Zset.Array_enc 1000 100 in
    let fld = F.create pool folds ds in
    let total_model = ref [] in
    for d = 0 to folds-1 do
      let trainset = F.get_fold fld d in
      let valset = F.get_fold_negation fld d in
      let loopstate = Loop.create ds pool trainset valset rate in
      while Loop.stage_count loopstate < stages do
        Loop.next loopstate;
      done;
      (* Add the models in m to the total list *)
      let m = Loop.ensemble_model loopstate in
      let m2 = List.map (fun t -> Tree.multiply (1.0 /. float folds) t) m in
      total_model := m2 @ !total_model
    done;
    (* Create a noise-free dataset: *)
    let ds1 =
      create_table
        ~enable_noise:false
        ~rounds:1
        ~size:10 in
    let all1 = Zset.all pool (Ztable.space ds1) in
    let Ztypes.Space n1 = Ztable.space ds1 in
    (* Evaluate total_model for the noise-free dataset: *)
    let eval = Tree.eval_list ds1 !total_model in
    let total_approx = Vector.init n1 (fun k -> eval k) in
    (* Get the loss of the total_model relative to the noise-free dataset: *)
    let s0 = L.stage ds1 all1 total_approx in
    let loss = L.loss s0 all1 in
    (* ... and now expect loss to be very low: *)
    printf " [loss=%.6f] %!" loss;
    loss < 0.01
end


let tests =
  [ ("Basic.test_001", Basic.test_001);
    ("Basic.test_002", Basic.test_002);
    ("Basic.test_003", Basic.test_003);
    ("Sine.test_001", Sine.test_001);
  ]
