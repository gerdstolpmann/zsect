open Printf
open T_frame.Util
open Zmola
open Types
open Zcontainer

module DS1 =
  (* only color *)
  struct
    type dataset = Ztable.ztable
    type col_hint = { col_ordinal : bool;
                      (* col_monotonic : [`Increasing|`Decreasing] option;*)
                    }
    let ztable ztab = ztab
    let x_columns _ = [ "color", {col_ordinal=false; (*col_monotonic=None*)} ]
    let y_column _ = "x"
  end

module DS2 =
  (* color + direction *)
  struct
    type dataset = Ztable.ztable
    type col_hint = { col_ordinal : bool;
                      (* col_monotonic : [`Increasing|`Decreasing] option; *)
                    }
    let ztable ztab = ztab
    let x_columns _ = [ "color", {col_ordinal=false; (*col_monotonic=None*)};
                        "direction", {col_ordinal=false; (*col_monotonic=None*)};
                      ]
    let y_column _ = "x"
  end

let cols =
  [| ( "color",
       Zdigester.ParserBox(Ztypes.Zstring, (fun s -> s)),
       "IM/string",
       "string_asc"
     );
     ( "direction",
       Zdigester.ParserBox(Ztypes.Zstring, (fun s -> s)),
       "IM/string",
       "string_asc"
     );
     ( "x",
       Zdigester.ParserBox(Ztypes.Zfloat, float_of_string),
       "DF64/float",
       "float_asc"
     );
    |]

let create_ztable1() =
  let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
  Zdigester.build_add dg [| "red";   "down"; "5.0" |];
  Zdigester.build_add dg [| "red";   "down"; "5.1" |];
  Zdigester.build_add dg [| "red";   "up";   "5.2" |];
  Zdigester.build_add dg [| "green"; "down"; "9.8" |];
  Zdigester.build_add dg [| "green"; "down"; "10.2" |];
  Zdigester.build_add dg [| "green"; "up";   "10.5" |];
  Zdigester.build_add dg [| "blue";  "up";   "20.5" |];
  Zdigester.build_add dg [| "blue";  "up";   "21.2" |];
  Zdigester.build_add dg [| "blue";  "down"; "17.8" |];
  Zdigester.build_end dg

let colname col =
  let Ztypes.ZcolDescr(n,_,_,_) = col in
  n

let coltype col =
  let Ztypes.ZcolDescr(_,ty,_,_) = col in
  ty

let colval col v =
  Ztypes.debug_string_of_value (coltype col) v

let test_001() =
  (* Take only color as x column *)
  let module HC = struct let alpha = 0.3 end in
  let module L = Loss.Huber(HC)(DS1) in
  let module BL_step = Blearner_sqe_bisection.Step(DS1) in
  let module BL_config = struct let max_depth = 5 end in
  let module BL = Blearner.Tree_learner(BL_config)(BL_step) in
  let module EL = Booster.Tree_Gradient_Booster(L)(BL) in
  let pool = Zset.default_pool in
  let ztab = create_ztable1() in
  let trainset = Zset.all pool (Ztable.space ztab) in
  let s0 = EL.start ztab trainset in
  let s1 = EL.advance s0 in
  let m1 = EL.model s1 in
  let l1 = EL.loss s1 in
  (match m1 with
     | Inner [_, 
              Inner [ E_Or[E_ColCmp(col1,_,v1)], Leaf g1;
                      E_Or[E_ColCmp(col2,_,v2)], Leaf g2
                    ];
              E_Or[E_ColCmp(col3,_,v3)], Leaf g3
             ] ->
         named "col1" (colname col1 = "color") &&&
         named "v1" (colval col1 v1 = "\"red\"") &&&
           named "col2" (colname col2 = "color") &&&
           named "v2" (colval col2 v2 = "\"green\"") &&&
           named "col3" (colname col3 = "color") &&&
           named "v3" (colval col3 v3 = "\"blue\"") &&&
           named "l1" (l1 > 0.5 && l1 < 0.55)
     | _ ->
         named "m1" false
  )

let tests =
  [ ("test_001", test_001)
  ]
