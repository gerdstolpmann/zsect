open Printf
open T_frame.Util
open Zmola
open Types
open Zcontainer

module Basic = struct

  module DS1 =
    (* only color *)
    struct
      type dataset = Ztable.ztable
      type col_hint = { col_ordinal : bool;
                        (* col_monotonic : [`Increasing|`Decreasing] option; *)
                      }
      let ztable ztab = ztab
      let x_columns _ = [ "color", {col_ordinal=false; (*col_monotonic=None*)} ]
      let y_column _ = "x"
    end

  module DS2 =
    (* color + direction *)
    struct
      type dataset = Ztable.ztable
      type col_hint = { col_ordinal : bool;
                        (* col_monotonic : [`Increasing|`Decreasing] option; *)
                      }
      let ztable ztab = ztab
      let x_columns _ = [ "color", {col_ordinal=false; (*col_monotonic=None*)};
                          "direction", {col_ordinal=false; (*col_monotonic=None*)};
                        ]
      let y_column _ = "x"
    end

  let cols =
    [| ( "color",
         Zdigester.ParserBox(Ztypes.Zstring, (fun s -> s)),
         "IM/string",
         "string_asc"
       );
       ( "direction",
         Zdigester.ParserBox(Ztypes.Zstring, (fun s -> s)),
         "IM/string",
         "string_asc"
       );
       ( "x",
         Zdigester.ParserBox(Ztypes.Zint, int_of_string),
         "DF64/int",
         "int_asc"
       );
      |]

  let create_ztable1() =
    let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
    Zdigester.build_add dg [| "red";   "down"; "0" |];
    Zdigester.build_add dg [| "red";   "down"; "0" |];
    Zdigester.build_add dg [| "red";   "up";   "1" |];
    Zdigester.build_add dg [| "green"; "down"; "0" |];
    Zdigester.build_add dg [| "green"; "down"; "0" |];
    Zdigester.build_add dg [| "green"; "up";   "1" |];
    Zdigester.build_add dg [| "blue";  "up";   "1" |];
    Zdigester.build_add dg [| "blue";  "up";   "0" |];
    Zdigester.build_add dg [| "blue";  "down"; "0" |];
    Zdigester.build_end dg

  let colname col =
    let Ztypes.ZcolDescr(n,_,_,_) = Zcol.descr col in
    n

  let coltype col =
    let Ztypes.ZcolDescr(_,ty,_,_) = Zcol.descr col in
    ty

  let colval col v =
    Ztypes.debug_string_of_value (coltype col) v

  let test_001() =
    (* Take only color as x column *)
    let module BC = struct let alpha = 0.0 end in
    let module L = Loss.Bilog(BC)(DS1) in
    let module BL_step = Blearner_sqe_bisection.Step(DS1) in
    let module BL_config = struct let max_depth = 5 end in
    let module BL = Blearner.Tree_learner(BL_config)(BL_step) in
    let module EL = Booster.Tree_Gradient_Booster(L)(BL) in
    let pool = Zset.default_pool in
    let ztab = create_ztable1() in
    let trainset = Zset.all pool (Ztable.space ztab) in
    let s0 = EL.start ztab trainset in
    let l0 = EL.loss s0 in
    let s1 = EL.advance s0 in
    let l1 = EL.loss s1 in
    (* Here, s0 is already perfect! *)
    named "l0" (l0 < 5.8) &&&
      named "l1" (abs_float(l1 -. l0) < 0.001)

  let test_002() =
    (* color + directon *)
    let module BC = struct let alpha = 0.0 end in
    let module L = Loss.Bilog(BC)(DS2) in
    let module BL_step = Blearner_sqe_bisection.Step(DS2) in
    let module BL_config = struct let max_depth = 5 end in
    let module BL = Blearner.Tree_learner(BL_config)(BL_step) in
    let module EL = Booster.Tree_Gradient_Booster(L)(BL) in
    let pool = Zset.default_pool in
    let ztab = create_ztable1() in
    let trainset = Zset.all pool (Ztable.space ztab) in
    let s0 = EL.start ztab trainset in
    let l0 = EL.loss s0 in
    let s1 = EL.advance s0 in
    let l1 = EL.loss s1 in
    let s2 = EL.advance s1 in
    let l2 = EL.loss s2 in
    let s3 = EL.advance s2 in
    let l3 = EL.loss s3 in
    let ( =~ ) x y = abs_float(x -. y) < 0.1 in
    named "l0" (l0 < 5.8) &&&
      named "l1" (l1 < 2.2) &&&
      named "l2" (l2 < 1.7) &&&
      named "l3" (l3 < 1.5) &&&
      named "approx1" (L.output (EL.approximation s3 1) =~ 0.0) &&&
      named "approx2" (L.output (EL.approximation s3 2) =~ 0.0) &&&
      named "approx3" (L.output (EL.approximation s3 3) =~ 1.0) &&&
      named "approx4" (L.output (EL.approximation s3 4) =~ 0.0) &&&
      named "approx5" (L.output (EL.approximation s3 5) =~ 0.0) &&&
      named "approx6" (L.output (EL.approximation s3 6) =~ 1.0) &&&
      named "approx7" (L.output (EL.approximation s3 7) =~ 0.5) &&&
      named "approx8" (L.output (EL.approximation s3 8) =~ 0.5) &&&
      named "approx9" (L.output (EL.approximation s3 9) =~ 0.0)
end


module Shape = struct
  let cols =
    [| ( "x",
         Zdigester.ParserBox(Ztypes.Zint, int_of_string),
         "IM/int",
         "int_asc"
       );
       ( "y",
         Zdigester.ParserBox(Ztypes.Zint, int_of_string),
         "IM/int",
         "int_asc"
       );
       ( "z",
         Zdigester.ParserBox(Ztypes.Zint, int_of_string),
         "DF64/int",
         "int_asc"
       );
      |]


  let pi = 3.141592

  let error() =
    (* some normal distributed error using Box-Muller *)
    let var = 0.5 in
    let u = Random.float 1.0 in
    let v = Random.float 1.0 in
    sqrt(var) *. sqrt(-2.0 *. log u) *. cos(2.0 *. pi *. v)

  let round x =
    int_of_float(floor(x +. 0.5))

  let create_square ~enable_noise ~rounds ~size f =
    let half = size/2 in
    let limit = half*3/4 in
    for r = 1 to rounds do
      for x = -half to half do
        for y = -half to half do
          let c =
            if enable_noise then
              round (float limit +. error())
            else
              limit in
          let z_bool =
            ((x = (-c) || x = c) && y >= (-limit) && y <= limit) ||
              ((y = (-c) || y = c) && x >= (-limit) && x <= limit) in
          let z = if z_bool then 1 else 0 in
          f x y z
        done
      done
    done


  let create_table create_data =
    let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
    create_data
      (fun x y z ->
         let dg_vec = [| string_of_int x; string_of_int y; string_of_int z |] in
         Zdigester.build_add dg dg_vec;
      );
    Zdigester.build_end dg

  module DS =
    struct
      type dataset = Ztable.ztable
      type col_hint = { col_ordinal : bool;
                        (* col_monotonic : [`Increasing|`Decreasing] option;*)
                      }
      let ztable ztab = ztab
      let x_columns _ = [ "x", {col_ordinal=true; (*col_monotonic=None*)};
                          "y", {col_ordinal=true; (*col_monotonic=None*)};
                        ]
      let y_column _ = "z"
    end

  module BC = struct let alpha = 0.0 end
  module L = Loss.Bilog(BC)(DS)
  module BL_step = Blearner_sqe_bisection.Step(DS)
  module BL_config = struct let max_depth = 3 end
  module BL = Blearner.Tree_learner(BL_config)(BL_step)
  module EL = Booster.Tree_Gradient_Booster(L)(BL)
  module F = Folder.Data_folds(DS)
  module Loop = Booster_loop.Loop(EL)

  let test_001() =
    (* Config *)
    let rate = 0.5 in
    let stages = 100 in
    (* Create the noisy dataset: *)
    let ds =
      create_table
        (create_square
           ~enable_noise:true
           ~rounds:6
           ~size:10
        ) in
    (* Approximate it, yielding [total_model]: *)
    let folds = 3 in
    let pool = Zset.create_pool ~enctype:Zset.Array_enc 1000 100 in
    let fld = F.create pool folds ds in
    let total_model = ref [] in
    for d = 0 to folds-1 do
      let trainset = F.get_fold fld d in
      let valset = F.get_fold_negation fld d in
      let loopstate = Loop.create ds pool trainset valset rate in
      while Loop.stage_count loopstate < stages do
        Loop.next loopstate;
      done;
      (* Add the models in m to the total list *)
      let m = Loop.ensemble_model loopstate in
      let m2 = List.map (fun t -> Tree.multiply (1.0 /. float folds) t) m in
      total_model := m2 @ !total_model
    done;
    (* Create a noise-free dataset: *)
    let ds1 =
      create_table
        (create_square
           ~enable_noise:false
           ~rounds:1
           ~size:10
        ) in
    let Ztypes.Space n1 = Ztable.space ds1 in
    (* Evaluate total_model for the noise-free dataset: *)
    let eval = Tree.eval_list ds1 !total_model in
    let total_approx =
      Array.init n1
        (fun k -> if L.output(eval (k+1)) >= 0.5 then 1 else 0) in
    let z_col = Ztable.get_zcol ds1 Ztypes.Zint "z" in
    let get_z = Zcol.value_of_id z_col in
    let total_expected = Array.init n1 (fun k -> get_z (k+1)) in
    let mispredictions = ref 0 in
    for j = 0 to n1-1 do
      if total_approx.(j) <> total_expected.(j) then incr mispredictions;
    done;
    (* ... and now expect the mispredictions to be very low: *)
    let accuracy = float (n1 - !mispredictions) /. float n1 in
    printf " [accuracy=%.1f%%] %!" (100.0 *. accuracy);
    accuracy > 0.94
end
    

let tests =
  [ ("Basic.test_001", Basic.test_001);
    ("Basic.test_002", Basic.test_002);
    ("Shape.test_001", Shape.test_001);
  ]
