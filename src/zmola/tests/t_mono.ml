(* Monotonicity constraint *)

open Printf
open T_frame.Util
open Zmola
open Types
open Zcontainer
open Ztypes


let alist_of_htable ht =
  let keys =
    Hashtbl.fold (fun key _ acc -> key :: acc) ht [] in
  let keys =
    List.sort compare keys in
  List.flatten
    (List.map
       (fun key ->
         let values = Hashtbl.find_all ht key in
         List.map (fun v -> (key,v)) values
       )
       keys
    )


let col1 = ZcolDescr("col1", Ztypes.Zfloat, Ztypes.Virtual_column,
                     Zrepr.float_asc)
let col2 = ZcolDescr("col2", Ztypes.Zstring, Ztypes.Virtual_column,
                     Zrepr.string_asc)

let tree1 =
  Inner [ E_ColCmp(col1, LT, 5.4),
          Inner [ E_ColCmp(col2, LT, "M"),
                  Leaf 1;

                  E_ColCmp(col2, GE, "M"),
                  Leaf 2;
                ];

          E_ColCmp(col1, GE, 5.4),
          Inner [ E_ColCmp(col1, LT, 8.9),
                  Leaf 3;

                  E_ColCmp(col1, GE, 8.9),
                  Inner [ E_ColCmp(col2, LT, "B"),
                          Leaf 4;

                          E_ColCmp(col2, GE, "B"),
                          Leaf 5
                        ]
                ]
        ]

let tree1_cols = [ "col1", `Increasing ]



module Tree = struct
  let test_001() =
    match Tree.mono_bounds_of_tree tree1_cols tree1 with
      | None ->
          named "None" false
      | Some(Tree.Mono_bounds(name, zt, ord, mt, mg, mb)) ->
          named "name" (name = "col1") &&&
          named "ztype"
                (match same_ztype zt Zfloat with Equal -> true|_ -> false) &&&

          named "mono_type" (mt = `Increasing) &&&
          named "mono_group"
                (alist_of_htable mg =
                   [ 1, 0;
                     2, 0;
                     3, 1;
                     4, 2;
                     5, 2
                ]) &&&
          named "mono_bounds"
                (match same_ztype zt Zfloat with
                   | Equal ->
                       alist_of_htable mb =
                         [ 0, (None, Some 5.4);
                           1, (Some 5.4, Some 8.9);
                           2, (Some 8.9, None) ]
                   | Not_equal -> false
                )
end

(*
module Gamma = struct
  let vals1 =
    [| 2.3;  2.4;  2.2;  2.05; 2.15;
       6.7;  8.8;  6.6;  6.6;  6.05; 6.1;
       13.5; 15.6; 12.4; 15.6 |]
  let vals2 =
    [| "A";  "B";  "A";  "P";  "P";
       "A";  "C";  "P";  "Q";  "A"; "A";
       "C";  "A";  "A";  "A" |]
  let y =
    [| 10.0; 10.0; 10.0; 10.0; 10.0;
       20.0; 20.0; 20.0; 20.0; 20.0; 20.0;
       30.0; 30.0; 30.0; 30.0 |]

  let gamma approx _ subset _ =
    (* the gamma function for squared error loss *)
    let sum = ref 0.0 in
    Zset.iter
      (fun start len ->
        for id = start to start+len-1 do
          sum := !sum +. y.(id-1) -. approx.(id-1)
        done
      )
      subset;
    !sum /. float (Zset.count subset)

  let space = Ztypes.create_space 15
  let pool = Zset.default_pool

  let tree =
    Zmola.Tree.map_leaves
      (fun leaf_id ->
        match leaf_id with
          | 1 -> Zset.from_array pool space [| 1; 2; 3 |]
          | 2 -> Zset.from_array pool space [| 4; 5 |]
          | 3 -> Zset.from_array pool space [| 6; 7; 8; 9; 10; 11 |]
          | 4 -> Zset.from_array pool space [| 13; 14; 15 |]
          | 5 -> Zset.from_array pool space [| 12 |]
          | _ -> assert false
      )
      tree1

  let equal_float_arrays a1 a2 =
    Array.length a1 = Array.length a2 &&
      (try
         Array.iteri
           (fun i x1 ->
             let x2 = a2.(i) in
             if abs_float(x1 -. x2) > 0.0000001 then raise Not_found
           )
           a1;
         true
       with Not_found -> false
      )

  let test_001() =
    (* This test is constructed so that approx-y increases with col1.
       The expectation is that the special handling for monotonicity
       errors does not kick in.
     *)
    let approx =
      [|  9.9;  9.8;  9.7;  9.9;  9.6;
          18.9; 18.8; 18.7; 18.7; 18.5; 18.72;
          27.5; 27.3; 27.4; 27.5 |] in
    let gradient = Vector.make 15 0.0 in
    let (t2, gvec) =
      Booster.gamma_of_tree tree1_cols (gamma approx) () gradient tree in
    let gvec = Vector.to_array gvec in
    equal_float_arrays
      gvec
      [| 0.2; 0.2; 0.2;                      (* IDs 1 - 3 *)
         0.25; 0.25;                         (* 4 - 5 *)
         1.28; 1.28; 1.28; 1.28; 1.28; 1.28; (* 6 - 11 *)
         2.5;                                (* 12 *)
         2.6; 2.6; 2.6                       (* 13 - 15 *)
      |]

  let test_002() =
    (* Here, the approximation for IDs 4 and 5 is too much off *)
    let approx =
      [|  9.9;  9.8;  9.7;  7.0;  7.0;
          18.9; 18.8; 18.7; 18.7; 18.5; 18.72;
          28.5; 28.3; 28.4; 28.5 |] in
    let gradient = Vector.make 15 0.0 in
    let (t2, gvec) =
      Booster.gamma_of_tree tree1_cols (gamma approx) () gradient tree in
    let gvec = Vector.to_array gvec in
    equal_float_arrays
      gvec
      [| 0.2; 0.2; 0.2;                      (* IDs 1 - 3 *)
         1.28; 1.28;                         (* 4 - 5: 1.28 instead of 3 *)
         1.28; 1.28; 1.28; 1.28; 1.28; 1.28; (* 6 - 11 *)
         1.5;                                (* 12 *)
         1.6; 1.6; 1.6                       (* 13 - 15 *)
      |]

  let test_003() =
    (* Here, the approximation for ID 12 is too much off *)
    let approx =
      [|  9.9;  9.8;  9.7;  7.0;  7.0;
          18.9; 18.8; 18.7; 18.7; 18.5; 18.72;
          40.0; 28.3; 28.4; 28.5 |] in
    let gradient = Vector.make 15 0.0 in
    let (t2, gvec) =
      Booster.gamma_of_tree tree1_cols (gamma approx) () gradient tree in
    let gvec = Vector.to_array gvec in
    equal_float_arrays
      gvec
      [| -10.0; -10.0; -10.0;                (* IDs 1 - 3 *)
         -10.0; -10.0;                       (* 4 - 5: 1.28 instead of 3 *)
         -10.0; -10.0; -10.0; -10.0; -10.0; -10.0; (* 6 - 11 *)
         -10.0;                              (* 12 *)
         1.6; 1.6; 1.6                       (* 13 - 15 *)
      |]
      
end
 *)

(*
module Parabola = struct
  (* approximate one arm of a parabola, and we enable monotonicity *)

    let cols =
    [| ( "x",
         Zdigester.ParserBox(Ztypes.Zint, int_of_string),
         "IM/int",
         "int_asc"
       );
       ( "y",
         Zdigester.ParserBox(Ztypes.Zfloat, float_of_string),
         "DF64/float",
         "float_asc"
       );
      |]


  let pi = 3.141592

  let error() =
    (* some normal distributed error using Box-Muller *)
    let var = 0.01 in
    let u = Random.float 1.0 in
    let v = Random.float 1.0 in
    sqrt(var) *. sqrt(-2.0 *. log u) *. cos(2.0 *. pi *. v)

  let create_data f ~enable_noise ~rounds ~size =
    for r = 1 to rounds do
      for i = 0 to size-1 do
        let x = 8.0 *. float i /. float size in
        let y = x *. x +. (if enable_noise then error() else 0.0) in
        f i x y
      done;
    done


  let create_table ~enable_noise ~rounds ~size =
    let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
    create_data
      (fun k x y ->
       let dg_vec = [| string_of_int k; string_of_float y |] in
       Zdigester.build_add dg dg_vec;
      )
      ~enable_noise ~rounds ~size;
    Zdigester.build_end dg

  module DS =
    struct
      type dataset = Ztable.ztable
      type col_hint = { col_ordinal : bool;
                        col_monotonic : [`Increasing|`Decreasing] option;
                      }
      let ztable ztab = ztab
      let x_columns _ = [ "x", {col_ordinal=true;
                                col_monotonic=Some `Increasing};
                        ]
      let y_column _ = "y"
    end

  module L = Loss.Squared_error(DS)
  module BL_step = Blearner_sqe_bisection.Step(DS)
  module BL_config = struct let max_depth = 3 end
  module BL = Blearner.Tree_learner(BL_config)(BL_step)
  module EL = Booster.Tree_Gradient_Booster(L)(BL)
  module F = Folder.Data_folds(DS)
  module Loop = Booster_loop.Loop(EL)

  let test_001() =
    (* Config *)
    let rate = 0.1 in
    let stages = 100 in
    (* Create the noisy dataset: *)
    let ds =
      create_table
        ~enable_noise:true
        ~rounds:5
        ~size:10 in
    (* Approximate it, yielding [total_model]: *)
    let folds = 2 in
    let pool = Zset.create_pool ~enctype:Zset.Array_enc 1000 100 in
    let fld = F.create pool folds ds in
    let total_model = ref [] in
    for d = 0 to folds-1 do
      let trainset = F.get_fold fld d in
      let valset = F.get_fold_negation fld d in
      let loopstate = Loop.create ds pool trainset valset rate in
      while Loop.stage_count loopstate < stages do
        (* printf "loss=%f\n" (Loop.loss_trainset loopstate); *)
        Loop.next loopstate;
      done;
      (* Add the models in m to the total list *)
      let m = Loop.ensemble_model loopstate in
      let m2 = List.map (fun t -> Zmola.Tree.multiply (1.0 /. float folds) t) m in
      total_model := m2 @ !total_model
    done;
    (* Create a noise-free dataset: *)
    let ds1 =
      create_table
        ~enable_noise:false
        ~rounds:1
        ~size:10 in
    let all1 = Zset.all pool (Ztable.space ds1) in
    let Ztypes.Space n1 = Ztable.space ds1 in
    (* Evaluate total_model for the noise-free dataset: *)
    let eval = Zmola.Tree.eval_list ds1 !total_model in
    let total_approx = Vector.init n1 (fun k -> eval k) in
    (* Get the loss of the total_model relative to the noise-free dataset: *)
    let s0 = L.stage ds1 all1 total_approx in
    let loss = L.loss s0 all1 in
    (* ... and now expect loss to be very low: *)
    printf " [loss=%.6f] %!" loss;
    loss < 0.01
end
 *)


let tests =
  [ ("Tree.test_001",     Tree.test_001);
    (* ("Gamma.test_001",    Gamma.test_001); *)
    (* ("Gamma.test_002",    Gamma.test_002); *)
    (* ("Gamma.test_003",    Gamma.test_003); *)
    (* ("Parabola.test_001", Parabola.test_001); *)
  ]
