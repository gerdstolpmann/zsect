open Printf
open T_frame.Util
open Zmola
open Types
open Zcontainer

module DS = struct
  type dataset = Ztable.ztable
  type col_hint =
    { col_ordinal : bool;
      (* col_monotonic : [`Increasing|`Decreasing] option;*)
    }
  let ztable ztab = ztab
  let x_columns _ = []
  let y_column _ = "foo"
end

module OStep = Blearner_sqe_bisection.Ordinal_fstep(DS)
module CStep = Blearner_sqe_bisection.Categorical_fstep(DS)

let cols =
  [| ( "c1",
       Zdigester.ParserBox(Ztypes.Zstring, (fun s -> s)),
       "IM/string",
       "string_asc"
     );
     ( "c2",
       Zdigester.ParserBox(Ztypes.Zfloat, float_of_string),
       "DF64/float",
       "float_asc"
     );
    |]

let vector zcol =
  let (Ztypes.Space n) = Zcol.space zcol in
  Vector.init n (Zcol.value_of_id zcol)


let test_001() =
  let pool = Zset.default_pool in
  let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
  (* Some rows have c2 values around 10.0 some others around 20. The task
     is to recognize that the best split is at "E"
   *)
  Zdigester.build_add dg [| "A"; "9.8" |];
  Zdigester.build_add dg [| "B"; "9.9" |];
  Zdigester.build_add dg [| "C"; "10.0" |];
  Zdigester.build_add dg [| "D"; "10.1" |];
  Zdigester.build_add dg [| "E"; "19.3" |];
  Zdigester.build_add dg [| "F"; "19.7" |];
  Zdigester.build_add dg [| "G"; "19.8" |];
  Zdigester.build_add dg [| "H"; "20.4" |];
  Zdigester.build_add dg [| "I"; "21.0" |];
  Zdigester.build_add dg [| "J"; "23.0" |];
  let ztab = Zdigester.build_end dg in
  let numbers = vector (Ztable.get_zcol ztab Ztypes.Zfloat "c2") in
  let domain = Zset.all pool (Ztable.space ztab) in
  let s0 = OStep.start pool numbers domain in
  (* printf "s0 loss: %.1f\n" (OStep.base_loss s0); *)
  match OStep.feature_refine ztab "c1" s0 with
    | None ->
        named "None" false
    | Some(new_loss, [l;r]) ->
        (* printf "new loss: %.1f\n" new_loss; *)
        let expr_l = OStep.expr l in
        let expr_r = OStep.expr r in
        ( match expr_l, expr_r with
            | E_ColCmp(col, Ztypes.LT, split_l), E_ColCmp(_, Ztypes.GE, _) ->
                let Ztypes.ZcolDescr(_,ty,_,_) = col in
                ( match Ztypes.same_ztype ty Ztypes.Zstring with
                    | Ztypes.Equal ->
                        split_l = "E"
                    | _ ->
                        false
                )
            | _ ->
                false
        )
    | Some _ ->
        named "Some" false


let test_002() =
  (* Same with permuted rows *)
  let pool = Zset.default_pool in
  let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
  Zdigester.build_add dg [| "I"; "21.0" |];
  Zdigester.build_add dg [| "A"; "9.8" |];
  Zdigester.build_add dg [| "D"; "10.1" |];
  Zdigester.build_add dg [| "F"; "19.7" |];
  Zdigester.build_add dg [| "G"; "19.8" |];
  Zdigester.build_add dg [| "E"; "19.3" |];
  Zdigester.build_add dg [| "C"; "10.0" |];
  Zdigester.build_add dg [| "B"; "9.9" |];
  Zdigester.build_add dg [| "J"; "23.0" |];
  Zdigester.build_add dg [| "H"; "20.4" |];
  let ztab = Zdigester.build_end dg in
  let numbers = vector (Ztable.get_zcol ztab Ztypes.Zfloat "c2") in
  let domain = Zset.all pool (Ztable.space ztab) in
  let s0 = OStep.start pool numbers domain in
  (* printf "s0 loss: %.1f\n" (OStep.base_loss s0); *)
  match OStep.feature_refine ztab "c1" s0 with
    | None ->
        named "None" false
    | Some(new_loss, [l;r]) ->
        (* printf "new loss: %.1f\n" new_loss; *)
        let expr_l = OStep.expr l in
        let expr_r = OStep.expr r in
        ( match expr_l, expr_r with
            | E_ColCmp(col, Ztypes.LT, split_l), E_ColCmp(_, Ztypes.GE, _) ->
                let Ztypes.ZcolDescr(_,ty,_,_) = col in
                ( match Ztypes.same_ztype ty Ztypes.Zstring with
                    | Ztypes.Equal ->
                        split_l = "E"
                    | _ ->
                        false
                )
            | _ ->
                false
        )
    | Some _ ->
        named "Some" false


let extract_c1_from_expr : expr -> string =
  function
  | E_ColCmp(col, Ztypes.EQ, value) ->
      let Ztypes.ZcolDescr(_,ty,_,_) = col in
      ( match Ztypes.same_ztype ty Ztypes.Zstring with
          | Ztypes.Equal ->
              value
          | _ ->
              failwith "not a string"
      )
  | _ ->
      failwith "not an EQ clause"


let test_100() =
  let pool = Zset.default_pool in
  let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
  (* Some rows have c2 values around 10.0 some others around 20. Other than
     in test_001 the c1 values are permuted which shouldn't matter for a 
     categorical learner.
   *)
  Zdigester.build_add dg [| "a"; "9.8" |];  (* A *)
  Zdigester.build_add dg [| "b"; "23.0" |]; (* J *)
  Zdigester.build_add dg [| "c"; "9.9" |];  (* B *)
  Zdigester.build_add dg [| "d"; "10.1" |]; (* D *)
  Zdigester.build_add dg [| "e"; "20.4" |]; (* H *)
  Zdigester.build_add dg [| "f"; "19.3" |]; (* E *)
  Zdigester.build_add dg [| "g"; "19.8" |]; (* G *)
  Zdigester.build_add dg [| "h"; "21.0" |]; (* I *)
  Zdigester.build_add dg [| "i"; "19.7" |]; (* F *)
  Zdigester.build_add dg [| "j"; "10.0" |]; (* C *)
  let ztab = Zdigester.build_end dg in
  let numbers = vector (Ztable.get_zcol ztab Ztypes.Zfloat "c2") in
  let domain = Zset.all pool (Ztable.space ztab) in
  let s0 = CStep.start pool numbers domain in
  (* printf "s0 loss: %.1f\n" (CStep.base_loss s0);*)
  match CStep.feature_refine ztab "c1" s0 with
    | None ->
        named "None" false
    | Some(new_loss, [l;r]) ->
        (* printf "new loss: %.1f\n" new_loss;*)
        let expr_l = CStep.expr l in
        let expr_r = CStep.expr r in
        ( match expr_l with
            | E_Or or_list ->
                let strings =
                  List.map extract_c1_from_expr or_list in
                let strings =
                  List.sort String.compare strings in
                (* printf "string=%s\n" (String.concat "," strings);*)
                named "Lstrings" (strings = [ "a"; "c"; "d"; "j" ])
            | _ ->
                named "LOR" false
        ) &&&
          ( match expr_r with
              | E_Or or_list ->
                  let strings =
                    List.map extract_c1_from_expr or_list in
                  let strings =
                    List.sort String.compare strings in
                  (* printf "string=%s\n" (String.concat "," strings);*)
                  named "Rstrings" (strings = [ "b"; "e"; "f"; "g"; "h"; "i" ])
              | _ ->
                  named "ROR" false
          )
    | Some _ ->
        named "Some" false

  
let tests =
  [ ("test_001", test_001);
    ("test_002", test_002);
    ("test_100", test_100);
  ]
