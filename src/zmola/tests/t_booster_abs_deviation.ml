open Printf
open T_frame.Util
open Zmola
open Types
open Zcontainer

module DS1 =
  (* only color *)
  struct
    type dataset = Ztable.ztable
    type col_hint = { col_ordinal : bool;
                      (*col_monotonic : [`Increasing|`Decreasing] option; *)
                    }
    let ztable ztab = ztab
    let x_columns _ = [ "color", {col_ordinal=false; (*col_monotonic=None*)} ]
    let y_column _ = "x"
  end

module DS2 =
  (* color + direction *)
  struct
    type dataset = Ztable.ztable
    type col_hint = { col_ordinal : bool;
                      (*col_monotonic : [`Increasing|`Decreasing] option;*)
                    }
    let ztable ztab = ztab
    let x_columns _ = [ "color", {col_ordinal=false; (*col_monotonic=None*)};
                        "direction", {col_ordinal=false; (*col_monotonic=None*)};
                      ]
    let y_column _ = "x"
  end

let cols =
  [| ( "color",
       Zdigester.ParserBox(Ztypes.Zstring, (fun s -> s)),
       "IM/string",
       "string_asc"
     );
     ( "direction",
       Zdigester.ParserBox(Ztypes.Zstring, (fun s -> s)),
       "IM/string",
       "string_asc"
     );
     ( "x",
       Zdigester.ParserBox(Ztypes.Zfloat, float_of_string),
       "DF64/float",
       "float_asc"
     );
    |]

let create_ztable1() =
  let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
  Zdigester.build_add dg [| "red";   "down"; "5.0" |];
  Zdigester.build_add dg [| "red";   "down"; "5.1" |];
  Zdigester.build_add dg [| "red";   "up";   "5.2" |];
  Zdigester.build_add dg [| "green"; "down"; "9.8" |];
  Zdigester.build_add dg [| "green"; "down"; "10.2" |];
  Zdigester.build_add dg [| "green"; "up";   "10.5" |];
  Zdigester.build_add dg [| "blue";  "up";   "20.5" |];
  Zdigester.build_add dg [| "blue";  "up";   "21.2" |];
  Zdigester.build_add dg [| "blue";  "down"; "17.8" |];
  Zdigester.build_end dg

let colname col =
  let Ztypes.ZcolDescr(n,_,_,_) = col in
  n

let coltype col =
  let Ztypes.ZcolDescr(_,ty,_,_) = col in
  ty

let colval col v =
  Ztypes.debug_string_of_value (coltype col) v

let test_001() =
  (* Take only color as x column *)
  let module L = Loss.Absolute_deviation(DS1) in
  let module BL_step = Blearner_sqe_bisection.Step(DS1) in
  let module BL_config = struct let max_depth = 5 end in
  let module BL = Blearner.Tree_learner(BL_config)(BL_step) in
  let module EL = Booster.Tree_Gradient_Booster(L)(BL) in
  let pool = Zset.default_pool in
  let ztab = create_ztable1() in
  let trainset = Zset.all pool (Ztable.space ztab) in
  let s0 = EL.start ztab trainset in
  let s1 = EL.advance s0 in
  let m1 = EL.model s1 in
  let l1 = EL.loss s1 in
  let s2 = EL.advance s1 in
  let l2 = EL.loss s2 in
  (match m1 with
     | Inner [E_Or[E_ColCmp(col1,_,v1)], Leaf g3;

              _,
              Inner [ E_Or[E_ColCmp(col2,_,v2)], Leaf g1;
                      E_Or[E_ColCmp(col3,_,v3)], Leaf g2
                    ];
             ] ->
         named "col1" (colname col1 = "color") &&&
         named "v1" (colval col1 v1 = "\"red\"") &&&
           named "col2" (colname col2 = "color") &&&
           named "v2" (colval col2 v2 = "\"green\"") &&&
           named "col3" (colname col3 = "color") &&&
           named "v3" (colval col3 v3 = "\"blue\"") &&&
           named "l1" (l1 < 4.3)
     | _ ->
         named "m1" false
  ) &&&
    named "l2" (abs_float(l1 -. l2) < 0.00001)
      (* converges in one go *)


let test_002() =
  (* Take color and direction as x columns *)
  let module L = Loss.Absolute_deviation(DS2) in
  let module BL_step = Blearner_sqe_bisection.Step(DS2) in
  let module BL_config = struct let max_depth = 5 end in
  let module BL = Blearner.Tree_learner(BL_config)(BL_step) in
  let module EL = Booster.Tree_Gradient_Booster(L)(BL) in
  let pool = Zset.default_pool in
  let ztab = create_ztable1() in
  let trainset = Zset.all pool (Ztable.space ztab) in
  let s0 = EL.start ztab trainset in
  let s1 = EL.advance s0 in
  let m1 = EL.model s1 in
  let l1 = EL.loss s1 in
  let s2 = EL.advance s1 in
  let l2 = EL.loss s2 in
  (match m1 with
     | Inner [E_Or[E_ColCmp(col1,_,v1)],
              Inner [ E_Or[E_ColCmp(col2,_,v2)], Leaf _;
                      E_Or[E_ColCmp(col3,_,v3)], Leaf _ ];

              _,
              Inner [ E_Or[E_ColCmp(col4,_,v4)],
                      Inner [ E_Or[E_ColCmp(col5,_,v5)], Leaf _;
                              E_Or[E_ColCmp(col6,_,v6)], Leaf _ ];
                              
                      E_Or[E_ColCmp(col7,_,v7)],
                      Inner [ E_Or[E_ColCmp(col8,_,v8)], Leaf _;
                              E_Or[E_ColCmp(col9,_,v9)], Leaf _ ];

                    ];
             ] ->
         named "col1" (colname col1 = "color") &&&
         named "v1" (colval col1 v1 = "\"red\"") &&&
           named "col2" (colname col2 = "direction") &&&
           named "v2" (colval col2 v2 = "\"up\"") &&&
           named "col3" (colname col3 = "direction") &&&
           named "v3" (colval col3 v3 = "\"down\"") &&&
           named "col4" (colname col4 = "color") &&&
           named "v4" (colval col4 v4 = "\"green\"") &&&
           named "col5" (colname col5 = "direction") &&&
           named "v5" (colval col5 v5 = "\"down\"") &&&
           named "col6" (colname col6 = "direction") &&&
           named "v6" (colval col6 v6 = "\"up\"") &&&
           named "col7" (colname col7 = "color") &&&
           named "v7" (colval col7 v7 = "\"blue\"") &&&
           named "col8" (colname col8 = "direction") &&&
           named "v8" (colval col8 v8 = "\"up\"") &&&
           named "col9" (colname col9 = "direction") &&&
           named "v9" (colval col9 v9 = "\"down\"") &&&
           named "l1" (l1 < 1.2)
     | _ ->
         named "m1" false
  ) &&&
    named "l2" (abs_float(l1 -. l2) < 0.00001)
      (* converges in one go *)


let test_003() =
  (* Take color and direction as x columns, but now limit the depth of the
     tree to 2. So two boosting steps are now needed until convergence.
   *)
  let module L = Loss.Absolute_deviation(DS2) in
  let module BL_step = Blearner_sqe_bisection.Step(DS2) in
  let module BL_config = struct let max_depth = 2 end in (* here! *)
  let module BL = Blearner.Tree_learner(BL_config)(BL_step) in
  let module EL = Booster.Tree_Gradient_Booster(L)(BL) in
  let pool = Zset.default_pool in
  let ztab = create_ztable1() in
  let trainset = Zset.all pool (Ztable.space ztab) in
  let s0 = EL.start ztab trainset in
  let s1 = EL.advance s0 in
  let l1 = EL.loss s1 in
  let s2 = EL.advance s1 in
  let l2 = EL.loss s2 in
  let s3 = EL.advance s2 in
  let l3 = EL.loss s3 in
  let s4 = EL.advance s3 in
  let l4 = EL.loss s4 in
  named "l1" (l1 > 4.1 && l1 < 4.3) &&&
    named "l2" (l2 > 3.4 && l2 < 3.6) &&&
    named "l3" (l3 > 1.1 && l3 < 1.3) &&&
    named "l4" (l4 > 1.1 && l4 < 1.3)


let tests =
  [ ("test_001", test_001);
    ("test_002", test_002);
    ("test_003", test_003);
  ]
