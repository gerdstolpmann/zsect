open Printf
open T_frame.Util

module M = Zmola.Ivalmap.FloatIvalMap


let create l =
  List.fold_left
    (fun acc ((l,r),x) ->
      M.add (l,r) x acc
    )
    (M.zero "")
    l

let show l =
  List.iter
    (fun ((l,r),x) ->
      printf "((%f,%f),%S)\n" l r x
    )
    l;
  flush stdout;
  l
    
module Add = struct
  let test_001() =
    M.to_list (create [ ((10.0, 20.0), "10-20") ]) =
      [ ((neg_infinity, 10.0), "");
        ((10.0, 20.0), "10-20");
        ((20.0, infinity), "")
      ]

  let test_002() =
    M.to_list
      (create
         [ ((10.0, 20.0), "10-20");
           ((30.0, 40.0), "30-40");
         ]) =
      [ ((neg_infinity, 10.0), "");
        ((10.0, 20.0), "10-20");
        ((20.0, 30.0), "");
        ((30.0, 40.0), "30-40");
        ((40.0, infinity), "")
      ]

  let test_003() =
    M.to_list
      (create
         [ ((10.0, 20.0), "10-20");
           ((-30.0, 0.0), "-30-0");
         ]) =
      [ ((neg_infinity, -30.0), "");
        ((-30.0, 0.0), "-30-0");
        ((0.0, 10.0), "");
        ((10.0, 20.0), "10-20");
        ((20.0, infinity), "")
      ]
        
  let test_004() =
    M.to_list
      (create
         [ ((10.0, 40.0), "10-40");
           ((20.0, 30.0), "20-30");
         ]) =
      [ ((neg_infinity, 10.0), "");
        ((10.0, 20.0), "10-40");
        ((20.0, 30.0), "20-30");
        ((30.0, 40.0), "10-40");
        ((40.0, infinity), "")
      ]
        
  let test_005() =
    M.to_list
      (create
         [ ((10.0, 40.0), "10-40");
           ((10.0, 30.0), "10-30");
         ]) =
      [ ((neg_infinity, 10.0), "");
        ((10.0, 30.0), "10-30");
        ((30.0, 40.0), "10-40");
        ((40.0, infinity), "")
      ]

  let test_006() =
    M.to_list
      (create
         [ ((10.0, 40.0), "10-40");
           ((20.0, 40.0), "20-40");
         ]) =
      [ ((neg_infinity, 10.0), "");
        ((10.0, 20.0), "10-40");
        ((20.0, 40.0), "20-40");
        ((40.0, infinity), "")
      ]

  let test_007() =
    M.to_list
      (create
         [ ((10.0, 40.0), "10-40");
           ((20.0, 50.0), "20-50");
         ]) =
      [ ((neg_infinity, 10.0), "");
        ((10.0, 20.0), "10-40");
        ((20.0, 50.0), "20-50");
        ((50.0, infinity), "")
      ]

  let test_008() =
    M.to_list
      (create
         [ ((10.0, 40.0), "10-40");
           ((20.0, 30.0), "20-30");
           ((20.0, 50.0), "20-50");
         ]) =
      [ ((neg_infinity, 10.0), "");
        ((10.0, 20.0), "10-40");
        ((20.0, 50.0), "20-50");
        ((50.0, infinity), "")
      ]
        
  let test_009() =
    M.to_list
      (create
         [ ((20.0, 50.0), "20-50");
           ((10.0, 40.0), "10-40");
      ]) =
      [ ((neg_infinity, 10.0), "");
        ((10.0, 40.0), "10-40");
        ((40.0, 50.0), "20-50");
        ((50.0, infinity), "")
      ]
end

module Find = struct
  let test_001() =
    let m =
      create
        [ ((10.0, 20.0), "10-20");
          ((20.0, 30.0), "20-30");
          ((30.0, 40.0), "30-40")
        ] in
    named "5" (M.find 5.0 m = ((neg_infinity, 10.0), "")) &&&
    named "10" (M.find 10.0 m = ((10.0, 20.0), "10-20")) &&&
    named "15" (M.find 15.0 m = ((10.0, 20.0), "10-20")) &&&
    named "20" (M.find 20.0 m = ((20.0, 30.0), "20-30")) &&&
    named "25" (M.find 25.0 m = ((20.0, 30.0), "20-30")) &&&
    named "50" (M.find 50.0 m = ((40.0, infinity), ""))

  let test_002() =
    let m =
      create
        [ ((10.0, 20.0), "10-20");
          ((20.0, 30.0), "20-30");
          ((30.0, 40.0), "30-40")
        ] in
    named "5" (try ignore(M.find_prev 5.0 m); false with Not_found -> true) &&&
    named "10" (M.find_prev 10.0 m = ((neg_infinity, 10.0), "")) &&&
    named "15" (M.find_prev 15.0 m = ((neg_infinity, 10.0), "")) &&&
    named "20" (M.find_prev 20.0 m = ((10.0, 20.0), "10-20")) &&&
    named "25" (M.find_prev 25.0 m = ((10.0, 20.0), "10-20")) &&&
    named "50" (M.find_prev 50.0 m = ((30.0, 40.0), "30-40"))

  let test_003() =
    let m =
      create
        [ ((10.0, 20.0), "10-20");
          ((20.0, 30.0), "20-30");
          ((30.0, 40.0), "30-40")
        ] in
    named "5" (M.find_next 5.0 m = ((10.0, 20.0), "10-20")) &&&
    named "10" (M.find_next 10.0 m = ((20.0, 30.0), "20-30")) &&&
    named "15" (M.find_next 15.0 m = ((20.0, 30.0), "20-30")) &&&
    named "20" (M.find_next 20.0 m = ((30.0, 40.0), "30-40")) &&&
    named "25" (M.find_next 25.0 m = ((30.0, 40.0), "30-40")) &&&
    named "50" (try ignore(M.find_next 50.0 m); false with Not_found -> true)
end

module Merge = struct
  let test_001() =
    let m =
      M.merge
        (fun _ x y -> x ^ "+" ^ y)
        (create
           [ ((10.0, 20.0), "10-20");
             ((20.0, 30.0), "20-30");
             ((30.0, 40.0), "30-40")
           ]
        )
        (create
           [ ((15.0, 25.0), "15-25") ]
        )
        "" in
    M.to_list m =
      [ ((neg_infinity, 10.0), "+");
        ((10.0, 15.0), "10-20+");
        ((15.0, 20.0), "10-20+15-25");
        ((20.0, 25.0), "20-30+15-25");
        ((25.0, 30.0), "20-30+");
        ((30.0, 40.0), "30-40+");
        ((40.0, infinity), "+")
      ]

  let test_002() =
    let m =
      M.merge
        (fun _ x y -> x ^ "+" ^ y)
        (create
           [ ((10.0, 20.0), "10-20");
             ((20.0, 30.0), "20-30");
             ((30.0, 40.0), "30-40")
           ]
        )
        (create
           [ ((15.0, 25.0), "15-25");
             ((35.0, 45.0), "35-45");
           ]
        )
        "" in
    M.to_list m =
      [ ((neg_infinity, 10.0), "+");
        ((10.0, 15.0), "10-20+");
        ((15.0, 20.0), "10-20+15-25");
        ((20.0, 25.0), "20-30+15-25");
        ((25.0, 30.0), "20-30+");
        ((30.0, 35.0), "30-40+");
        ((35.0, 40.0), "30-40+35-45");
        ((40.0, 45.0), "+35-45");
        ((45.0, infinity), "+")
      ]
        
end


let tests =
  [ ("Add.test_001", Add.test_001);
    ("Add.test_002", Add.test_002);
    ("Add.test_003", Add.test_003);
    ("Add.test_004", Add.test_004);
    ("Add.test_005", Add.test_005);
    ("Add.test_006", Add.test_006);
    ("Add.test_007", Add.test_007);
    ("Add.test_008", Add.test_008);
    ("Add.test_009", Add.test_009);

    ("Find.test_001", Find.test_001);
    ("Find.test_002", Find.test_002);
    ("Find.test_003", Find.test_003);

    ("Merge.test_001", Merge.test_001);
    ("Merge.test_002", Merge.test_002);
  ]
