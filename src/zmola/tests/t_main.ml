let () =
  T_frame.Main.register "Squared_error" T_squared_error.tests;
  T_frame.Main.register "Blearner_sqe_bisection" T_blearner_sqe_bisection.tests;
  T_frame.Main.register "Blearner_sqe_clustering" T_blearner_sqe_clustering.tests;
  T_frame.Main.register "Booster_squared_error" T_booster_squared_error.tests;
  T_frame.Main.register "Booster_abs_deviation" T_booster_abs_deviation.tests;
  T_frame.Main.register "Booster_huber" T_booster_huber.tests;
  T_frame.Main.register "Booster_bilog" T_booster_bilog.tests;
  T_frame.Main.register "Mono" T_mono.tests;
  T_frame.Main.register "Ivalmap" T_ivalmap.tests;

  T_frame.Main.main()
