open Printf
open T_frame.Util
open Zmola
open Types
open Zcontainer

module DS = struct
  type dataset = Ztable.ztable
  type col_hint =
    { col_ordinal : bool;
      (* col_monotonic : [`Increasing|`Decreasing] option;*)
    }
  let ztable ztab = ztab
  let x_columns _ = []
  let y_column _ = "foo"
end

module OStep = Blearner_sqe_clustering.Ordinal_fstep(DS)
module CStep = Blearner_sqe_clustering.Categorical_fstep(DS)

let cols =
  [| ( "c1",
       Zdigester.ParserBox(Ztypes.Zstring, (fun s -> s)),
       "IM/string",
       "string_asc"
     );
     ( "c2",
       Zdigester.ParserBox(Ztypes.Zfloat, float_of_string),
       "DF64/float",
       "float_asc"
     );
    |]

let vector zcol =
  let (Ztypes.Space n) = Zcol.space zcol in
  Vector.init n (Zcol.value_of_id zcol)


let extract_c1_from_expr : expr -> string =
  function
  | E_ColCmp(col, _, value) ->
      let Ztypes.ZcolDescr(_,ty,_,_) = col in
      ( match Ztypes.same_ztype ty Ztypes.Zstring with
          | Ztypes.Equal ->
              value
          | _ ->
              failwith "not a string"
      )
  | _ ->
      failwith "not a column comparison clause"


let test_001() =
  let pool = Zset.default_pool in
  let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
  (* Some rows have c2 values around 10.0 some others around 20 and a third
     group have values around 30
   *)
  Zdigester.build_add dg [| "A"; "9.8" |];
  Zdigester.build_add dg [| "B"; "9.9" |];
  Zdigester.build_add dg [| "C"; "10.0" |];
  Zdigester.build_add dg [| "D"; "10.1" |];
  Zdigester.build_add dg [| "E"; "19.3" |];
  Zdigester.build_add dg [| "F"; "19.7" |];
  Zdigester.build_add dg [| "G"; "19.8" |];
  Zdigester.build_add dg [| "H"; "20.4" |];
  Zdigester.build_add dg [| "I"; "21.0" |];
  Zdigester.build_add dg [| "J"; "29.0" |];
  Zdigester.build_add dg [| "K"; "31.0" |];
  let ztab = Zdigester.build_end dg in
  let numbers = vector (Ztable.get_zcol ztab Ztypes.Zfloat "c2") in
  let domain = Zset.all pool (Ztable.space ztab) in
  let s0 = OStep.start pool numbers domain in
  (* printf "s0 loss: %.1f\n" (OStep.base_loss s0); *)
  match OStep.feature_refine ztab "c1" s0 with
    | None ->
        named "None" false
    | Some(new_loss, [sub1;sub2;sub3]) ->
        (* printf "new loss: %.1f\n" new_loss; *)
        ( match OStep.expr sub1 with
            | E_And [ E_ColCmp(_, Ztypes.GE, _) as cmp1;
                      E_ColCmp(_, Ztypes.LE, _) as cmp2;
                    ] ->
                named "A" (extract_c1_from_expr cmp1 = "A") &&&
                  named "D" (extract_c1_from_expr cmp2 = "D")
            | _ ->
                named "structure1" false
        ) &&&
          ( match OStep.expr sub2 with
              | E_And [ E_ColCmp(_, Ztypes.GE, _) as cmp1;
                        E_ColCmp(_, Ztypes.LE, _) as cmp2;
                      ] ->
                  named "E" (extract_c1_from_expr cmp1 = "E") &&&
                    named "I" (extract_c1_from_expr cmp2 = "I")
              | _ ->
                  named "structure2" false
          ) &&&
          ( match OStep.expr sub3 with
              | E_And [ E_ColCmp(_, Ztypes.GE, _) as cmp1;
                        E_ColCmp(_, Ztypes.LE, _) as cmp2;
                      ] ->
                  named "J" (extract_c1_from_expr cmp1 = "J") &&&
                    named "K" (extract_c1_from_expr cmp2 = "K")
              | _ ->
                  named "structure3" false
          )
    | Some _ ->
        named "Some" false


let get_strings =
  function
  | E_Or or_list ->
      let strings =
        List.map extract_c1_from_expr or_list in
      let strings =
        List.sort String.compare strings in
      strings
  | _ ->
      []



let test_100() =
  (* That's for categorical data. The dataset is the same except that
     we permuted the rows and relabeled c1
   *)
  let pool = Zset.default_pool in
  let dg = Zdigester.create_memory_zdigester Zrepr.stdrepr cols in
  Zdigester.build_add dg [| "a"; "31.0" |];    (* K *)
  Zdigester.build_add dg [| "b"; "9.9" |];     (* B *)
  Zdigester.build_add dg [| "c"; "10.0" |];    (* C *)
  Zdigester.build_add dg [| "d"; "19.3" |];    (* E *)
  Zdigester.build_add dg [| "e"; "29.0" |];    (* J *)
  Zdigester.build_add dg [| "f"; "9.8" |];     (* A *)
  Zdigester.build_add dg [| "g"; "19.7" |];    (* F *)
  Zdigester.build_add dg [| "h"; "20.4" |];    (* H *)
  Zdigester.build_add dg [| "i"; "19.8" |];    (* G *)
  Zdigester.build_add dg [| "j"; "10.1" |];    (* D *)
  Zdigester.build_add dg [| "k"; "21.0" |];    (* I *)
  let ztab = Zdigester.build_end dg in
  let numbers = vector (Ztable.get_zcol ztab Ztypes.Zfloat "c2") in
  let domain = Zset.all pool (Ztable.space ztab) in
  let s0 = CStep.start pool numbers domain in
  (* printf "s0 loss: %.1f\n" (CStep.base_loss s0); *)
  match CStep.feature_refine ztab "c1" s0 with
    | None ->
        named "None" false
    | Some(new_loss, [sub1;sub2;sub3]) ->
        (* printf "new loss: %.1f\n" new_loss;*)
        let expr1 = CStep.expr sub1 in
        let expr2 = CStep.expr sub2 in
        let expr3 = CStep.expr sub3 in
        named "expr2" (get_strings expr2 = [ "b"; "c"; "f"; "j" ]) &&&
          named "expr3" (get_strings expr3 = [ "d"; "g"; "h"; "i"; "k" ]) &&&
          named "expr1" (get_strings expr1 = [ "a"; "e" ])
    | Some _ ->
        named "Some" false



let tests =
  [ ("test_001", test_001);
    ("test_100", test_100);
  ]
