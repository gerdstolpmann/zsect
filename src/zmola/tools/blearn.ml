(* stand-alone base learner *)

open Zcontainer
open Zmola.Blearner_par.Proto_in_deser
open Zmola.Blearner_par.Proto_out_ser
open Zmola.Types

let get_algo_functor =
  function
  | `Bisect ->
      (module Zmola.Blearner_sqe_bisection.FStep :
         MAKE_TREE_BASELEARNER_FEATURE_STEP)
;;

let main() =
  try
    while true do
      let line = input_line stdin in
      let j = Yojson.Safe.from_string line in
      let (algo, ds, vec, zset, feature) = from_json j in
      let algo_functor = get_algo_functor algo in
      let module MAKE = (val algo_functor : MAKE_TREE_BASELEARNER_FEATURE_STEP) in
      let module BL = Zmola.Blearner_sqe_bisection.FStep(DS) in
      let step = BL.start Zset.default_pool vec zset in
      let j_out =
        match BL.feature_refine ds feature step with
          | None -> `Null
          | Some(loss, partitioning) ->
              let partitioning' =
                List.map (fun p -> (BL.domain p, BL.expr p)) partitioning in
              let j_out = to_json loss partitioning' in
              List.iter BL.finalize partitioning;
              j_out in
      Yojson.Safe.to_channel stdout j_out;
      output_string stdout "\n";
      flush stdout
    done
  with
    | End_of_file -> ()
;;
    
main()
