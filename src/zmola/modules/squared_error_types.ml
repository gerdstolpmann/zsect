(* Helper module for computing least squared errors for sets *)

open Types
open Zcontainer

(** Roughly, this is a subset of [Set.S] *)
module type SIMPLE_SET =
  sig
    type elt
    type t
    val empty: t
    val is_empty: t -> bool
    val mem: elt -> t -> bool
    val add: elt -> t -> t
    val singleton: elt -> t
    val remove: elt -> t -> t
    val iter: (elt -> unit) -> t -> unit
    val fold: (elt -> 'a -> 'a) -> t -> 'a -> 'a
    val elements : t -> elt list
    val of_list : elt list -> t
    val diff : t -> t -> t
    val union : t -> t -> t
    val filter : (elt -> bool) -> t -> t
    val min_elt : t -> elt
    val max_elt : t -> elt
    module S : Set.S with type elt=elt
    val to_set : t -> S.t
  end


module type SIMPLE_MAP =
  sig
    type key
    type 'a t
    val is_empty: 'a t -> bool
    val mem: key -> 'a t -> bool
    val iter: (key -> 'a -> unit) -> 'a t -> unit
    val fold: (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val find: key -> 'a t -> 'a
    val of_list: (key * 'a) list -> 'a t
  end
    

module type SQUARED_ERROR =
  sig
    module Index : Set.OrderedType
    module IndexSet : SIMPLE_SET with type elt = Index.t

    (* CHECK: maybe we also want the underlying algebra for the arithmetic
       as module, e.g. as a Ring : sig val (+) val ( * ) end
     *)

    type squared_errors
    type index = Index.t

    type stats
      (** count, sum, and error for an IndexSet.t *)

    val create : vector -> (index * Zset.zset) list -> squared_errors
      (** create a [squared_error] accumulator for this vector of numbers
          and a couple of indexed sets. {b The sets need to be disjoint.}
       *)

    val check : squared_errors -> unit
      (** Check whether the sets are disjoint and whether the vector is
          large enough. This check is fairly expensive!
       *)

    val permute : squared_errors -> (index -> index) -> squared_errors
      (** Create a new sqe minimizer by mapping index values.

          The returned minimizer shares the vector with the input minimizer.
       *)

    val get_zset_at : squared_errors -> index -> Zset.zset
      (** return the set identified by this index. If the index is
          not found, the empty set is returned instead.
       *)

    val get_zset : squared_errors -> IndexSet.t -> Zset.pool -> Zset.zset
      (** return the union of the zsets for this index set *)

    val get_zsets : squared_errors -> IndexSet.t -> Zset.pool -> Zset.zset list
      (** return the zsets for this index set *)

    val all : squared_errors -> IndexSet.t
      (** return the index set of all covered sets *)

    val count : squared_errors -> IndexSet.t -> int
      (** number of rows selected by these sets *)

    val sum : squared_errors -> IndexSet.t -> float
      (** the sum of the numbers selected by these sets *)

    val avg : squared_errors -> IndexSet.t -> float
      (** the arithmetic mean of the numbers selected by these sets. *)

    val squared_error : squared_errors -> IndexSet.t -> float
      (** returns the squared error [SUM(x IN S . (x - avg(S))^2)].
          Note that the arithmetic mean minimizes the squared error,
          thus this is the least squared error possible
       *)

    val stats : squared_errors -> IndexSet.t -> stats
      (** Get the full stats *)

    val stats_iset : stats -> IndexSet.t
    val stats_count : stats -> int
    val stats_sum : stats -> float
    val stats_avg : stats -> float
    val stats_error : stats -> float
      (** return the components of the stats type *)

    val stats_empty : stats
      (** stats for the empty set *)

    val stats_add : squared_errors -> stats -> index -> stats
      (** [stats_add sqe stats i]: returns the new stats record when index [i]
          is added to the index set
       *)

    val stats_sub : squared_errors -> stats -> index -> stats
      (** [stats_sub sqe stats i]: returns the new stats record when index [i]
          is removed from the index set
       *)

    val stats_update : squared_errors -> stats -> IndexSet.t -> stats
      (** [stats_update sqe stats iset]: returns a new stats record for
          [iset] by exploiting the data available in the stats record passed
          in. The closer the new [iset] is to the one covered by [stats] the
          quicker the new record can be computed.
       *)
  end
