(* Base learner generic stuff *)

open Types
open Zcontainer
open Printf


(** Combine a feature stepper for ordinal columns and a feature stepper
    for categorical columns to get a general stepper
 *)
module Ord_or_cat_fstep(OStep : TREE_BASELEARNER_FEATURE_STEP)
                       (CStep : TREE_BASELEARNER_FEATURE_STEP with module DS = OStep.DS) : 
                       TREE_BASELEARNER_FEATURE_STEP with module DS = OStep.DS =
  struct
    module DS = OStep.DS

    type wrapped_step =
      | O of OStep.base_step
      | C of CStep.base_step

    type base_step =
      { w : wrapped_step;
        depth : int;
        pool : Zset.pool;
        numbers : vector;
      }

    let start pool numbers domain =
      { w = O(OStep.start pool numbers domain);
        depth = 0;
        pool;
        numbers
      }

    let depth bs = bs.depth

    let domain bs =
      match bs.w with
        | O step -> OStep.domain step
        | C step -> CStep.domain step

    let expr bs =
      match bs.w with
        | O step -> OStep.expr step
        | C step -> CStep.expr step

    let feature bs =
      match bs.w with
        | O step -> OStep.feature step
        | C step -> CStep.feature step

    let base_loss bs =
      match bs.w with
        | O step -> OStep.base_loss step
        | C step -> CStep.base_loss step

    let feature_refine ds colname bs =
      let is_ordinal =
        List.exists
          (fun (n,col_hint) -> n = colname && DS.(col_hint.col_ordinal))
          (DS.x_columns ds) in
      if is_ordinal then
        let step =
          match bs.w with
            | O step -> step
            | C _ -> OStep.start bs.pool bs.numbers (domain bs) in
        match OStep.feature_refine ds colname step with
          | None -> None
          | Some(bloss, substeps) ->
              Some(bloss,
                   List.map
                     (fun substep ->
                      { bs with
                        w = O substep;
                        depth = bs.depth + 1
                      }
                     )
                     substeps
                  )
      else
        let step =
          match bs.w with
            | C step -> step
            | O _ -> CStep.start bs.pool bs.numbers (domain bs) in
        match CStep.feature_refine ds colname step with
          | None -> None
          | Some(bloss, substeps) ->
              Some(bloss,
                   List.map
                     (fun substep ->
                      { bs with
                        w = C substep;
                        depth = bs.depth + 1
                      }
                     )
                     substeps
                  )
    let finalize bs =
      match bs.w with
        | O step -> OStep.finalize step
        | C step -> CStep.finalize step

  end


(** Create a stepper that tries the feature stepper for all x columns
    and takes the best column
 *)
module Step(FStep : TREE_BASELEARNER_FEATURE_STEP) :
         TREE_BASELEARNER_STEP with module DS = FStep.DS =
  struct
    module DS = FStep.DS
    type base_step = FStep.base_step
    let start = FStep.start
    let depth = FStep.depth
    let feature = FStep.feature
    let domain = FStep.domain
    let expr = FStep.expr
    let base_loss = FStep.base_loss
    let finalize = FStep.finalize

    let rec exists_in_first n p l =
      (* whether there is an element x from the first n elements of l
         so that p x is true
       *)
      n > 0 &&
        match l with
          | x :: l' -> p x || exists_in_first (n-1) p l'
          | [] -> false

    let refine ds step steps_to_go enabled_x_columns =
      let x_columns =
        List.filter
          (fun (n,_) -> List.mem n enabled_x_columns)
          (DS.x_columns ds) in
      (* Invoke the base learner: *)
      let col_results =
        List.map
          (fun (colname,hint) ->
             if !Log.debug then
               eprintf "    [column=%s%!" colname;
             let t0 = Unix.gettimeofday() in
             let col_result = FStep.feature_refine ds colname step in
             let t1 = Unix.gettimeofday() in
             if !Log.debug then
               printf " %.3fseconds]\n" (t1-.t0);
             (colname,hint,col_result)
          )
          x_columns in
      let col_results =
        List.flatten
          (List.map
             (function
              | (_,_,None) -> []
              | (colname,hint,Some (loss, steps)) -> [colname,loss,steps]
             )
             col_results) in
      List.sort
        (fun (_,loss1,_) (_,loss2,_) -> compare loss1 loss2)
        col_results
  end


module type TREE_LEARNER_CONFIG = sig
    val max_depth : int
end


module Tree_learner(C:TREE_LEARNER_CONFIG)(S:TREE_BASELEARNER_STEP) :
          TREE_BASELEARNER with module DS = S.DS =
  struct
    module DS = S.DS

    type base_output =
      { model : Zset.zset tree;
        parts : Zset.zset list
      }

    type model = Zset.zset tree

    let rec map_leaves f =
      function
      | Leaf l -> Leaf(f l)
      | Inner l -> Inner(List.map (fun (e,sub) -> e,map_leaves f sub) l)

    let rec fold_leaves f tree acc =
      match tree with
        | Leaf l -> f l acc
        | Inner l ->
            List.fold_right
              (fun (_,sub) acc -> fold_leaves f sub acc) 
              l
              acc

    let learn ds stage_num pool numbers domain =
      let enabled_x_columns =
        List.map fst (DS.x_columns ds) in
      let step_accu = ref [] in
      let rec recurse step =
        if !Log.debug then
          eprintf "  [depth=%d domain_count=%d]\n"
                  (S.depth step) (Zset.count (S.domain step));
        step_accu := step :: !step_accu;
        if S.depth step >= C.max_depth then
          Leaf step
        else
          let steps_to_go = C.max_depth - S.depth step in
          match S.refine ds step steps_to_go enabled_x_columns with
            | [] ->
                Leaf step
            | (_, loss, substeps) :: other ->
                List.iter (fun (_,_,l) -> List.iter S.finalize l) other;
                Inner(List.map
                        (fun substep -> S.expr substep, recurse substep)
                        substeps
                     ) in
      try
        let step_tree = recurse(S.start pool numbers domain) in
        let model =
          map_leaves
            (fun s -> S.domain s)
            step_tree in
        let parts =
          fold_leaves (fun zset acc -> zset :: acc) model [] in
        List.iter S.finalize !step_accu;
        { model; parts }
      with
        | exn ->
            List.iter S.finalize !step_accu;
            raise exn

    let model bo = bo.model
    let partitioning bo = bo.parts
  end
