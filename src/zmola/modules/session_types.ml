module type BOOST_UI_P0 =
  sig
    include Zsession.Stypes.CORE_UI
    module DS : Types.DATASET

    val dataset_input : unit -> Input.input
    val dataset_ztable : unit -> Zcontainer.Ztable.ztable
    val dataset : unit -> DS.dataset
                                   
    val load_dataset : path:string -> unit
    val set_dataset : Zcontainer.Ztable.ztable -> unit
    val select_dataset : unit -> unit
                            
    val columns : unit -> unit
      (** List on stdout which columns are configured *)

    val get_xcolumns : unit -> string list
      (** The X columns. Bundles appear here as such *)
    val get_xcolumns_unbundled : unit -> (string * DS.col_hint) list
      (** The X columns after unbundling, i.e. for bundles the bundle members
          are substituted
       *)
    val add_xcolumns : ?ordinal:bool ->
                       (* ?monotonic:[`Increasing|`Decreasing] ->*)
                       string list -> unit
      (** Sets these columns as X features *)   

    val select_xcolumns : unit -> unit
     (** interactively edit the X features *)

    val reset_xcolumns : unit -> unit

    val set_ycolumn : string -> unit
    val select_ycolumn : unit -> unit
    val reset_ycolumn : unit -> unit
    val get_ycolumn : unit -> string option

    type loss_type = [`Abs_dev | `Squared_error | `Huber | `Bilog ]
    val get_loss : unit -> loss_type
    val set_loss : loss_type -> unit
    val reset_loss : unit -> unit
    val select_loss : unit -> unit
                                    
    val set_trainset : Zcontainer.Zset.zset -> unit
    val get_trainset : unit -> Zcontainer.Zset.zset
    val set_train_subrange : from_id:int -> to_id:int -> unit

    val set_folds : int -> unit
    val get_folds : unit -> int

    val set_learning_rate : float -> unit
    val get_learning_rate : unit -> float

    val set_tree_depth : int -> unit
    val get_tree_depth : unit -> int
                                      
    val describe : unit -> unit

    val wrap_base_stepper : (module Types.TREE_BASELEARNER_STEP_WRAPPER) ->
                            unit
      (** Set the wrapper module for the base stepper. Note that this
          setting cannot be save to the session file, and needs to be manually
          configured.
       *)

    val wrap_baselearner : (module Types.TREE_BASELEARNER_WRAPPER) -> unit
      (** Set the wrapper module for the base learner. Note that this
          setting cannot be save to the session file, and needs to be manually
          configured.
       *)

    val get_parallel : unit -> bool
    val set_parallel : bool -> unit
      (** get/set whether the parallel version of the algorithm is enabled.
          The parallel version needs the external executable zmola_blearn.
          This setting is also not saved in the session file.
       *)

(*
    val set_loss_module : (module Stypes.LOSS with module ...) -> unit
 *)

(*
    val goal : unit -> unit
    (** show the goal on stdout *)
    val set_goal : ?iterations:int -> unit -> unit
    val select_goal : unit -> unit              
 *)

(*
    val write_model : ?format:string -> string -> unit
 *)

    module type RUNTIME =
      sig
        module Loss : Types.LOSS with module DS = DS
        module Booster : Types.TREE_GRADIENT_BOOSTER with module DS = DS
        module Loop : Booster_loop.LOOP_TYPE with module DS = DS
                                              and module L = Loss
                                              and module EL = Booster

        val fold_loop : int -> Loop.state
          (** Get the loop state of the k-th fold, 0 <= k < num_folds *)

        val fold_stage : int -> Booster.stage
          (** Get the stage of the k-th fold *)

        val run : iterations:int -> ?only_fold:int -> unit -> unit

        val describe : unit -> unit
        val save_runtime : unit -> unit

        val ensemble_model : unit -> Booster.model list
      end
                              
                                                    
    module type LOSS =
      Types.LOSS with module DS = DS
                                                     
    val loss_module : unit -> (module LOSS)

    val runtime : unit -> (module RUNTIME)
      (** Get the current runtime. If not yet running, this creates a new
          runtime module
       *)

    val reset : unit -> unit
      (** Delete the current runtime *)

    val run : iterations:int -> ?only_fold:int -> unit -> (module RUNTIME)
      (** Run until all folds reach the indicated iteration *)
  end

module type BOOST_UI =
  sig
    include BOOST_UI_P0
              
    val oniteration : ((module BOOST_UI_P0) -> int -> unit) -> unit
      (** [oniteration f]: configure that [f] is called once at the beginning
          of the learning algorithm, and then every time after a learning
          step. The function [f] is called as [f mod fold] where [mod]
          is the whole boost module, and [fold] identifies the fold that
          is currently used for learning.

          There is only one such callback, i.e. later calls of [oniteration]
          overwrite earlier callbacks.

          The [oniteration] callback isn't persistent, and needs to be
          set again after a dataset has been loaded.
       *)
  end
