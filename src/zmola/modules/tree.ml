open Types
open Zcontainer
open Printf


(************************ Pretty printing ************************)

let string_of_cmp =
  function
  | Ztypes.EQ -> "="
  | Ztypes.NE -> "<>"
  | Ztypes.LT -> "<"
  | Ztypes.LE -> "<="
  | Ztypes.GT -> ">"
  | Ztypes.GE -> ">="

let cmp_of_string =
  function
  | "="  -> Ztypes.EQ
  | "<>" -> Ztypes.NE
  | "<"  -> Ztypes.LT
  | "<=" -> Ztypes.LE
  | ">"  -> Ztypes.GT
  | ">=" -> Ztypes.GE
  | _ -> failwith "Zmola.Tree.cmp_of_string"

let rec string_of_expr =
  function
  | Types.E_ColCmp(col,cmp,value) ->
      let Ztypes.ZcolDescr(name,ty,_,_) = col in
      sprintf "%s%s%s"
              name
              (string_of_cmp cmp)
              (Ztypes.debug_string_of_value ty value)
  | Types.E_And l ->
      let l = List.map string_of_expr l in
      let l = List.map (fun s -> "(" ^ s ^ ")") l in
      String.concat " && " l
  | Types.E_Or l ->
      let l = List.map string_of_expr l in
      let l = List.map (fun s -> "(" ^ s ^ ")") l in
      String.concat " || " l

let print_tree ch t =
  let rec print i =
    function
    | Types.Leaf g ->
        fprintf ch "%s%.4f\n" (String.make i ' ') g
    | Types.Inner list ->
        List.iter
          (fun (expr,sub) ->
             fprintf ch "%sIF %s\n" (String.make i ' ') (string_of_expr expr);
             print (i+3) sub
          )
          list in
  print 0 t


(*********** Mapping and evaluation **************)

let rec map_leaves f =
  function
  | Leaf x -> Leaf (f x)
  | Inner l -> Inner (List.map (fun (e,t) -> (e,map_leaves f t)) l)

let rec map_inner f =
  function
  | Leaf x -> Leaf x
  | Inner l -> Inner(f (List.map (fun (e,t) -> (e,map_inner f t)) l))

let multiply factor tree =
  map_leaves (fun x -> x *. factor) tree

let rec map_expr f t =
  let t' =
    match t with
      | E_ColCmp _ -> t
      | E_And l -> E_And(List.map (map_expr f) l)
      | E_Or l -> E_Or(List.map (map_expr f) l) in
  f t'


let rec eval_expr ztab =
  function
  | E_ColCmp(coldescr, cmp, value) ->
      let Ztypes.ZcolDescr(colname,ty,_,ord) = coldescr in
      let col = Ztable.get_zcol ztab ty colname in
      (fun id ->
         let col_value = Zcol.value_of_id col id in
         let open Ztypes in
         match cmp with
           | EQ -> ord.ord_cmp_asc col_value value = 0
           | NE -> ord.ord_cmp_asc col_value value <> 0
           | LT -> ord.ord_cmp_asc col_value value < 0
           | LE -> ord.ord_cmp_asc col_value value <= 0
           | GT -> ord.ord_cmp_asc col_value value > 0
           | GE -> ord.ord_cmp_asc col_value value >= 0
      )
  | E_And l ->
      let l' = List.map (eval_expr ztab) l in
      (fun id ->
         List.for_all
           (fun f -> f id)
           l'
      )
  | E_Or l ->
      let l' = List.map (eval_expr ztab) l in
      (fun id ->
         List.exists
           (fun f -> f id)
           l'
      )

let rec first_col_of_expr =
  function
  | E_ColCmp(coldescr, cmp, value) ->
      let Ztypes.ZcolDescr(colname,_,_,_) = coldescr in
      colname
  | E_And [] ->
      "ANY"
  | E_And(e::_) ->
      first_col_of_expr e
  | E_Or [] ->
      "NONE"
  | E_Or(e::_) ->
      first_col_of_expr e


let rec eval ztab =
  function
  | Leaf y -> 
      (fun _ -> y)
  | Inner l ->
      let l' =
        List.map
          (fun (e,t) ->
             let pe = eval_expr ztab e in
             let pt = eval ztab t in
             (pe,pt)
          )
          l in
      (fun id ->
         (* may raise Not_found, but that's ok *)
        match List.find (fun (pe,_) -> pe id) l' with
          | _, pt -> pt id
          | exception Not_found ->
              Printf.eprintf
                "WARNING: subtree not matching id=%d col=%s\n%!"
                id
                (first_col_of_expr (fst (List.hd l)));
              0.0
      )
  
let rec eval_list ztab trees =
  let f_list = List.map (eval ztab) trees in
  (fun id ->
     List.fold_left (fun acc f -> acc +. f id) 0.0 f_list
  )


let rec filter_of_expr ztab =
  function
  | E_ColCmp(coldescr, cmp, value) ->
      let Ztypes.ZcolDescr(name,ty,_,_) = coldescr in
      let zcol = Ztable.get_zcol ztab ty name in
      Zfilter.colcmp_flt zcol cmp value
  | E_And l ->
      Zfilter.and_flt (List.map (filter_of_expr ztab) l)
  | E_Or l ->
      Zfilter.or_flt (List.map (filter_of_expr ztab) l)


let rec map_zset_tree ztab pool baseset 
                      (f : 'a -> Zset.zset -> 'b)
                      (tree : 'a tree) : 'b tree =
  let rec recurse parent_filters tree =
    match tree with
      | Leaf leaf ->
          let zset =
            Zfilter.zset pool ztab (Zfilter.and_flt parent_filters) in
          let zset =
            Zset.isect pool baseset zset in
          Leaf (f leaf zset)
      | Inner list ->
          Inner
            (List.map
               (fun (expr, sub) ->
                  let flt = filter_of_expr ztab expr in
                  (expr, recurse (flt :: parent_filters) sub)
               )
               list
            ) in
  recurse [] tree


let partitions ztab pool baseset (tree : 'a tree) : (Zset.zset * 'a) list =
  (* get the partitions of the tree, paired with the labels. The partitions
     are intersected with baseset.
   *)
  let rec recurse parent_filters tree =
    match tree with
      | Leaf leaf ->
          let zset =
            Zfilter.zset
              ~base:baseset
              pool ztab (Zfilter.and_flt parent_filters) in
          [ zset, leaf ]
      | Inner list ->
          List.flatten
            (List.map
               (fun (expr, sub) ->
                  let flt = filter_of_expr ztab expr in
                  recurse (flt :: parent_filters) sub
               )
               list
            ) in
  recurse [] tree


let accumulate ztab pool (numbers:vector) baseset tree =
  (* evaluate the tree on ztab, and add the numbers in the leaves to [numbers].
     The evaluation is restricted to the IDs of baseset.
     This is an in-place update
   *)
  let parts = partitions ztab pool baseset tree in
  List.iter
    (fun (zset,x) ->
       Zset.iter
         (fun start len ->
            for k = start to start+len-1 do
              numbers.{k} <- numbers.{k} +. x
            done
         )
         zset
    )
    parts

(*********************** Monotonicity *************************)


(* Monotonicity: The function mono_bounds_of_tree looks at the
   topmost ordinal feature in the tree (in the first n levels of the tree).
   It checks whether it is a feature col with a monotonicity constraint. If so,
   it figures the intervals out into which the tree splits the space of col
   values. So e.g. if the tree starts with

   +
   |\
   | +------  col < 5.4
   | |\
   | | +----  col < 2.3
   | |        subtree with leaves 1, 2, 4, and 5
   | |
   |  \
   |   +----  col >= 2.3
   |          subtree with leaves 3 and 6
    \
     +......  col >= 5.4
              subtree with leaves 7 and 8

   it figures out that the intervals are

   - interval 1: (-infinity, 2.3)
   - interval 2: (2.3, 5.4)
   - interval 3: (5.4, infinity)

   Every leaf (identified by a leaf ID) of the tree is covered by one
   of the intervals (because it is in the subtree). Because of this, we can
   also say that the leaves are grouped by the intervals. This leads to:

   - group 1: IDs 1, 2, 4, 5
   - group 2: IDs 3, 6
   - group 3: IDs 7, 8

   Now, mono_group maps the leaf IDs to the group IDs. The table mono_bounds
   contains the interval bounds oer group ID.
 *)

(* Helpers for mono_bounds_of_tree: *)

let mono_occurring_column mono_x_columns =
  (* Whether a monotonic column occurs in the expression in a way we can
     process here
   *)
  function
  | E_ColCmp(Ztypes.ZcolDescr(n1,ty1,_,ord1), (Ztypes.LT|Ztypes.GE), _) ->
      let is_mono = List.mem_assoc n1 mono_x_columns in
      if is_mono then
        let ordbox1 = Ztypes.ZorderBox(ty1,ord1) in
        Some(n1,ordbox1)
      else
        None
  | _ -> None

let mono_is_acceptable_tree mono_x_columns =
  (* actually, we could accept here more, but so far don't need to do so
     because the base learners at most only generate this...
   *)
  function
  | Inner (_ :: _ as inner) ->
      ( match mono_occurring_column mono_x_columns (fst(List.hd inner)) with
          | Some(n1,ordbox1) ->
              let is_only_column =
                List.for_all
                  (fun (e,_) ->
                    match mono_occurring_column mono_x_columns e with
                      | Some (n,_) -> n = n1
                      | _ -> false
                  )
                  inner in
              if is_only_column then
                Some(n1,ordbox1)
              else
                None
          | None ->
              None
      )
  | _ -> None

let mono_bounds_of_tree_for : type t. _ -> _ -> t Ztypes.ztype ->
                                   t Ztypes.zorder ->
                                   int tree ->
                                   ( [`Increasing|`Decreasing] *
                                       (int, int) Hashtbl.t *
                                         (int, (t option * t option)) Hashtbl.t
                                   ) =
  fun mono_x_columns n1 ty1 ord1 idtree ->
    let mono_group = Hashtbl.create 7 in
    let mono_bounds = Hashtbl.create 7 in
    let cmp = Ztypes.(ord1.ord_cmp_asc) in

    let mono_type = List.assoc n1 mono_x_columns in

    let rec add_to_group tree grp_id =
      match tree with
        | Leaf id ->
            Hashtbl.replace mono_group id grp_id
        | Inner subs ->
            List.iter (fun (_,sub) -> add_to_group sub grp_id) subs in
    let group_id = ref 0 in
    let new_group tree lbound_opt ubound_opt =
      let grp_id = !group_id in
      incr group_id;
      add_to_group tree grp_id;
      Hashtbl.add mono_bounds grp_id (lbound_opt,ubound_opt) in
    let rec trace_bounds tree lbound_opt ubound_opt =
      match tree with
        | Leaf _ ->
            new_group tree lbound_opt ubound_opt
        | Inner [] ->
            ()
        | Inner inner ->
            ( match mono_is_acceptable_tree mono_x_columns tree with
                | Some(n,_) when n = n1 ->
                    List.iter
                      (fun (expr,sub) ->
                        match expr with
                          | E_ColCmp(Ztypes.ZcolDescr(_,ty,_,_),
                                     (Ztypes.LT | Ztypes.LE),
                                     split) ->
                              ( match Ztypes.same_ztype ty ty1 with
                                  | Ztypes.Equal ->
                                      let ubound' =
                                        match ubound_opt with
                                          | None ->
                                              split
                                          | Some u ->
                                              if cmp (split:t) u < 0
                                              then split
                                              else (u:t) in
                                      trace_bounds sub lbound_opt (Some ubound')
                                  | Ztypes.Not_equal ->
                                      assert false
                              )
                          | E_ColCmp(Ztypes.ZcolDescr(_,ty,_,_),
                                     (Ztypes.GE | Ztypes.GT),
                                     split) ->
                              ( match Ztypes.same_ztype ty ty1 with
                                  | Ztypes.Equal ->
                                      let lbound' =
                                        match lbound_opt with
                                          | None ->
                                              split
                                          | Some l ->
                                              if cmp split (l:t) >0
                                              then split
                                              else (l:t) in
                                      trace_bounds sub (Some lbound') ubound_opt
                                  | Ztypes.Not_equal ->
                                      assert false
                              )
                          | _ ->
                              assert false
                      )
                      inner
                | _ ->
                    new_group tree lbound_opt ubound_opt
            ) in
    trace_bounds idtree None None;
    (mono_type, mono_group, mono_bounds)

type mono_bounds =
  | Mono_bounds : (*name:*) string * 'a Ztypes.ztype * 'a Ztypes.zorder *
                  [`Increasing|`Decreasing] * (int, int) Hashtbl.t *
                  (int, ('a option * 'a option)) Hashtbl.t -> mono_bounds

let mono_bounds_of_tree mono_x_columns (idtree : int tree) =
  match mono_is_acceptable_tree mono_x_columns idtree with
    | None ->
        None
    | Some(n1, Ztypes.ZorderBox(ty1, ord1)) ->
        let (mono_type, mono_group, mono_bounds) =
          mono_bounds_of_tree_for mono_x_columns n1 ty1 ord1 idtree in
        Some(Mono_bounds(n1, ty1, ord1, mono_type, mono_group, mono_bounds))

(* A "mono bounds map" is an interval map from the monotonic feature to
   (approx_min,approx_max), i.e. to the range of approximations.
 *)

type 'a point =
  | Point of 'a
  | Infinity
  | Neg_infinity

module type MONO_BOUNDS_MAP =
  sig
    type t  (* type of the monotonic feature *)
    module T : Map.OrderedType with type t = t
    module P : Ivalmap.PointType with type t = T.t point
    module M : Ivalmap.S with type point = P.t
    val create_from_tree : get_gamma:(int -> float) ->
                           mono_group:(int, int) Hashtbl.t ->
                           mono_bounds:(int, (t option * t option)) Hashtbl.t->
                           (float * float) M.t
    (* Creates an M.t from a Mono_bounds value *)
    val accumulate : (float * float) M.t -> (float * float) M.t ->
                     (float * float) M.t
    (* Adds two mappings up *)
  end

module type MONO_BOUNDS_MAP_VALUE =
  sig
    include MONO_BOUNDS_MAP
    val value : (float * float) M.t
  end

module Mono_bounds_map(T:Map.OrderedType) : MONO_BOUNDS_MAP with type t = T.t =
  struct
    type t = T.t
    module T = T
    module P = struct
      type t = T.t point
      let compare x_opt y_opt =
        match (x_opt,y_opt) with
          | Neg_infinity, Neg_infinity -> 0
          | Neg_infinity, _ -> (-1)
          | Infinity, Infinity -> 0
          | Infinity, _ -> 1
          | Point x, Point y -> T.compare x y
          | Point _, Neg_infinity -> 1
          | Point _, Infinity -> (-1)
      let neg_infinity = Neg_infinity
      let infinity = Infinity
    end

    module M = Ivalmap.Make(P)

    let create_from_tree ~get_gamma ~mono_group ~mono_bounds =
      Hashtbl.fold
        (fun id grp acc ->
          let g = get_gamma id in
          let (lbound, ubound) = Hashtbl.find mono_bounds grp in
          let lbound' =
            match lbound with
              | None -> Neg_infinity
              | Some x -> Point x in
          let ubound' =
            match ubound with
              | None -> Infinity
              | Some x -> Point x in
          let _, (g_min, g_max) = M.find lbound' acc in
          let g_min = min g_min g in
          let g_max = max g_max g in
          M.add (lbound',ubound') (g_min,g_max) acc
        )
        mono_group
        (M.zero (infinity,neg_infinity))

    let accumulate m1 m2 =
      M.merge
        (fun _ (g_min1,g_max1) (g_min2,g_max2) ->
          (g_min1 +. g_min2, g_max1 +. g_max2)
        )
        m1
        m2
        (0.0, 0.0)
  end

type mono_bounds_map =
  | Mono_bounds_map : (*name:*) string * 'a Ztypes.ztype * 'a Ztypes.zorder *
                      [`Increasing|`Decreasing] *
                      (module MONO_BOUNDS_MAP_VALUE with type t = 'a) ->
                          mono_bounds_map
  | Mono_bounds_map_empty
  | Mono_bounds_map_not_applicable

let mono_bounds_map_singleton : type s . _ -> s Ztypes.ztype ->
                                     s Ztypes.zorder -> _ -> _ -> _ ->
                                     (int, s option * s option) Hashtbl.t ->
                                     mono_bounds_map =
  fun n1 ty1 ord1 mono_type get_gamma mono_group mono_bounds ->
    let module T =
      struct
        type t = s
        let compare x y = Ztypes.(ord1.ord_cmp_asc) x y
      end in
    let module MBM : MONO_BOUNDS_MAP with type t = s = Mono_bounds_map(T) in
    let module MBMV =
      struct
        include MBM
        let value = create_from_tree get_gamma mono_group mono_bounds
      end in
    Mono_bounds_map(n1, ty1, ord1, mono_type, (module MBMV))


let mono_update (mbm : mono_bounds_map)
                (mb : mono_bounds option) get_gamma =
  match mbm, mb with
    | _, None ->
        mbm
    | Mono_bounds_map_empty,
      Some(Mono_bounds(n1, ty1, ord1, mono_type, mono_group, mono_bounds)) ->
        mono_bounds_map_singleton
          n1 ty1 ord1 mono_type get_gamma mono_group mono_bounds
    | Mono_bounds_map_not_applicable, _ ->
        Mono_bounds_map_not_applicable
    | Mono_bounds_map(n1, ty1, ord1, mono_type1, mbmv1),
      Some(Mono_bounds(n2, ty2, ord2, mono_type2, mono_group, mono_bounds)) ->
        if n1<>n2 then
          Mono_bounds_map_not_applicable
            (* CHECK: or just ignore mb... *)
        else
          ( match Ztypes.same_ztype ty1 ty2 with
              | Ztypes.Equal ->
                  let module MBMV1 = (val mbmv1) in
                  let m1 = MBMV1.value in
                  let m2 =
                    MBMV1.create_from_tree get_gamma mono_group mono_bounds in
                  let m3 =
                    MBMV1.accumulate m1 m2 in
                  let module MBMV3 =
                    struct
                      include MBMV1
                      let value = m3
                    end in
                  Mono_bounds_map(n1, ty1, ord1, mono_type1, (module MBMV3))
              | Ztypes.Not_equal ->
                  assert false
          )

let get_idtree f tree =
  let node_id = ref 0 in
  let node_ht = Hashtbl.create 17 in
  let idtree =
    map_leaves
      (fun x ->
        let id = !node_id in
        incr node_id;
        let x' = f x in
        Hashtbl.add node_ht id x';
        id
      )
      tree in
  (idtree, node_ht)

let mono_update_for_tree mbm mono_x_columns (tree : float tree) =
  if mbm = Mono_bounds_map_not_applicable then
    Mono_bounds_map_not_applicable
  else (
    let idtree, node_ht = get_idtree (fun g -> g) tree in
    let get_gamma id =
      try Hashtbl.find node_ht id
      with Not_found -> assert false in
    let mb = mono_bounds_of_tree mono_x_columns idtree in
    mono_update mbm mb get_gamma
  )

let mono_bounds_map_of_model mono_x_columns (model : float tree list) =
  assert(model <> []);
  List.fold_left
    (fun acc tree ->
      mono_update_for_tree acc mono_x_columns tree
    )
    (mono_update_for_tree Mono_bounds_map_empty mono_x_columns (List.hd model))
    (List.tl model)

let mono_find : type t . _ -> t Ztypes.ztype -> t -> float * float =
  fun mbm ty x ->
    match mbm with
      | Mono_bounds_map(n1, ty1, ord1, mono_type1, mbmv1) ->
          ( match Ztypes.same_ztype ty ty1 with
              | Ztypes.Equal ->
                  let module MBMV1 = (val mbmv1) in
                  snd (MBMV1.M.find (Point x) MBMV1.value)
              | Ztypes.Not_equal ->
                  failwith "Zmola.Tree.mono_find"
          )
      | Mono_bounds_map_empty ->
          failwith "Zmola.Tree.mono_find"
      | Mono_bounds_map_not_applicable ->
          failwith "Zmola.Tree.mono_find"

(************************** JSON I/O *************************)


let json_of_coldescr col =
  let Ztypes.ZcolDescr(name,ty,repr,order) = col in
  Zcontainer.Ztypes.json_of_zcolbox(Ztypes.ZcolBox(name,ty,repr,order))

let rec json_of_expr =
  function
  | Types.E_ColCmp(col,cmp,value) ->
      let Ztypes.ZcolDescr(_,ty,_,_) = col in
      `List [ `String "ColCmp";
              json_of_coldescr col;
              `String (string_of_cmp cmp);
              Zcontainer.Ztypes.json_of_value ty value
            ]
  | Types.E_And l ->
      `List [ `String "And";
              `List (List.map json_of_expr l)
            ]
  | Types.E_Or l ->
      `List [ `String "Or";
              `List (List.map json_of_expr l)
            ]

let create_hc_ht() = Hashtbl.create 17

let rec internalize : type t . (string,string) Hashtbl.t ->
                           t Zcontainer.Ztypes.ztype ->
                           t -> t =
  fun hashcons_ht ty value ->
  let open Zcontainer.Ztypes in
  match ty with
    | Zint -> value
    | Zbool -> value
    | Zint64 -> value
    | Zfloat -> value
    | Zlist _ -> value
    | Zbundle _ -> value
    | Zpair(ty1,ty2) ->
        (internalize hashcons_ht ty1 (fst value),
         internalize hashcons_ht ty2 (snd value))
    | Zoption ty1 ->
        ( match value with
            | None -> None
            | Some v -> Some(internalize hashcons_ht ty1 v)
        )
    | Zstring->
        ( try Hashtbl.find hashcons_ht value
          with Not_found ->
            Hashtbl.add hashcons_ht value value;
            value
        )


let rec expr_of_json_1 hashcons_ht =
  function
  | `List [ `String "ColCmp"; j_coldescr; `String cmp_name; j_value ] ->
      let Zcontainer.Ztypes.ZcolBox(name,ty,repr,order) =
        Zcontainer.Ztypes.zcolbox_of_json Zcontainer.Zrepr.stdrepr j_coldescr in
      let col = Zcontainer.Ztypes.ZcolDescr(name,ty,repr,order) in
      let cmp = cmp_of_string cmp_name in
      let value = Zcontainer.Ztypes.value_of_json ty j_value in
      let value = internalize hashcons_ht ty value in
      Types.E_ColCmp(col,cmp,value)
  | `List [ `String "And"; `List jl ] ->
      let el = List.map (expr_of_json_1 hashcons_ht) jl in
      Types.E_And el
  | `List [ `String "Or"; `List jl ] ->
      let el = List.map (expr_of_json_1 hashcons_ht) jl in
      Types.E_Or el
  | _ ->
      failwith "not recognized"

let expr_of_json_hc hashcons_ht j =
  try expr_of_json_1 hashcons_ht j
  with Failure msg ->
    failwith ("Zmola.Tree.expr_of_json: bad JSON (" ^ msg ^ ")")

let expr_of_json = expr_of_json_hc (create_hc_ht())

let rec json_of_tree json_of_label t =
  match t with
    | Leaf label ->
        `List [ `String "Leaf"; json_of_label label ]
    | Inner l ->
        let jl =
          List.map
            (fun (expr,sub) ->
               `List [ json_of_expr expr; json_of_tree json_of_label sub ]
            )
            l in
        `List [ `String "Inner"; `List jl ]

let rec tree_of_json_1 hashcons_ht label_of_json j =
  match j with
    | `List [ `String "Leaf"; j_label ] ->
        Leaf (label_of_json j_label)
    | `List [ `String "Inner"; `List jl ] ->
        let tl =
          List.map
            (function
              | `List [ j_expr; j_sub ] ->
                  let expr = expr_of_json_hc hashcons_ht j_expr in
                  let sub = tree_of_json_1 hashcons_ht label_of_json j_sub in
                  (expr,sub)
              | _ ->
                  failwith "not recognized"
            )
            jl in
        Inner tl
    | _ ->
        failwith "not recognized"

let tree_of_json_hc hashcons_ht label_of_json j =
  try tree_of_json_1 hashcons_ht label_of_json j
  with Failure msg ->
    failwith("Zmola.Tree.tree_of_json: bad JSON (" ^ msg ^ ")")

let tree_of_json x = tree_of_json_hc (create_hc_ht()) x

let json_of_model m =
  json_of_tree (fun x -> `Float x) m

let model_of_json_hc hashcons_ht j =
  tree_of_json_hc
    hashcons_ht
    (function
      | `Float x -> x
      | _ -> failwith "bad label type"
    )
    j

let model_of_json = model_of_json_hc (create_hc_ht())
               
let json_of_model_list m =
  `List (List.map (json_of_tree (fun x -> `Float x)) m)
   
let model_list_of_json j =
  let ht = create_hc_ht() in
  match j with
    | `List jl ->
        List.map (model_of_json_hc ht) jl
    | _ ->
        failwith "Zmola.Tree.model_of_json_list: bad JSON (not a list)"

let load_model filename =
  let j = Yojson.Safe.from_file filename in
  model_list_of_json j

let save_model filename m =
  let j = json_of_model_list m in
  Yojson.Safe.to_file filename j
