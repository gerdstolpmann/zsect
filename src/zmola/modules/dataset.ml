(* serialization of dataset descriptors *)

open Zcontainer
open Types
module U = Yojson.Safe.Util

let rec ztab_paths ztab =
  match Ztable.path_name ztab with
    | None ->
        failwith
          "Zmola.Dataset.to_json: memory-based table cannot be serialized"
    | Some p ->
        p :: ( match Ztable.underlying_ztable ztab with
                 | None -> []
                 | Some ztab' -> ztab_paths ztab'
             )

let ztab_cache = Hashtbl.create 7

let ztab_from_file p =
  try Hashtbl.find ztab_cache p
  with Not_found ->
     let ztab = Ztable.from_file Zrepr.stdrepr p in
     Hashtbl.add ztab_cache p ztab;
     ztab

let rec path_lookup_ztab paths =
  match paths with
    | [p] ->
        ztab_from_file p
    | p :: paths' ->
        let over = path_lookup_ztab paths' in
        Ztable.from_file ~over Zrepr.stdrepr p
    | [] ->
        failwith "Zmola.Dataset.path_lookup_ztab"


module Serialize(DS:DATASET) = struct
  let ser_xcol (n,hint) =
    `List [ `String n;
            `Assoc [ "ordinal", `Bool DS.(hint.col_ordinal) ]
          ]

  let to_json (ds:DS.dataset) : Yojson.Safe.json =
    let paths = ztab_paths (DS.ztable ds) in
    `Assoc [ "paths", `List (List.map (fun s -> `String s) paths);
             "xcols", `List (List.map ser_xcol (DS.x_columns ds));
             "ycol", `String (DS.y_column ds);
           ]
end


module Deserialize = struct
  type col_hint = { col_ordinal : bool }
  type dataset =
    { ztable : Ztable.ztable;
      x_cols : (string * col_hint) list;
      y_col : string
    }
  let ztable ds = ds.ztable
  let x_columns ds = ds.x_cols
  let y_column ds = ds.y_col

  let from_json (j:Yojson.Safe.json) : dataset =
    let fail() =
      failwith "Zmola.Dataset.Deserialize.from_json: bad json" in
    let deser_xcol =
      function
      | `List [ `String n;
                `Assoc [ "ordinal", `Bool col_ordinal ]
              ] ->
          (n, { col_ordinal })
      | _ ->
          fail() in
    match j with
      | `Assoc [ "paths", `List j_paths;
                 "xcols", `List j_xcols;
                 "ycol", `String y_col
               ] ->
          let paths = List.map U.to_string j_paths in
          let ztable = path_lookup_ztab paths in
          let x_cols = List.map deser_xcol j_xcols in
          { ztable; x_cols; y_col }
      | _ ->
          fail()

end
