open Types
open Zcontainer
module U = Yojson.Safe.Util

let ( !! ) = Lazy.force

let gamma_of_tree (* mono_x_columns *) f_gamma loss gradient tree =
  (* Get the tree labeled with gamma values, and also return the vector
     of gamma values
   *)
  let n = Vector.length gradient in
  let gamma = Vector.make n 0.0 in
  (* Assign IDs to the leaves of the tree *)
  let idtree, node_ht =
    Tree.get_idtree
      (fun zset ->
        let g = f_gamma loss zset gradient in
        (zset,g)
      )
      tree in
(*
  (* For monotonic columns, we have to do some special processing here.
     We have to ensure that the gamma values grow/fall with the column
     values. We do this by successively adjusting the gamma values so
     that the monotonicity constraint holds for the tree. We group
     the zsets so that zsets with the same constrains fall into the same
     group, and start the adjustment with the largest group (in terms
     of zsets size).

     mono_group: maps the leaf ID to the group ID
     mono_bounds: maps the group ID to (lbound,ubound)

     See also the comments for Tree.mono_bounds_of_tree which go a little
     bit deeper.
   *)
  ( match Tree.mono_bounds_of_tree mono_x_columns idtree with
      | None -> ()    (* no monotonic column at hand *)
      | Some(Tree.Mono_bounds(col_name, ty, ord, mono_type,
                              mono_group, mono_bounds)) ->
          (* Compute the group weights: *)
          let group_weight = Hashtbl.create 13 in
          Hashtbl.iter
            (fun id (zset,g) ->
              let grp = Hashtbl.find mono_group id in
              let w = try Hashtbl.find group_weight grp with Not_found -> 0.0 in
              let w' = w +. abs_float g *. float (Zset.count zset) in
              Hashtbl.replace group_weight grp w'
            )
            node_ht;
          (* Progress over the groups in order of decreasing weight: *)
          (* (The weight is here the sum of the absolute gamma values.
             CHECK: is this choice reasonable? *)
          let groups =
            Hashtbl.fold (fun grp _ acc -> grp::acc) group_weight [] in
          let groups =
            List.sort
              (fun grp1 grp2 ->
                compare
                  (Hashtbl.find group_weight grp2)
                  (Hashtbl.find group_weight grp1)
              )
              groups in
          (* List.iter (fun grp -> Printf.printf "Group %d\n" grp) groups; *)
          (* Manage min/max values for gamma per group: *)
          let max_group = Hashtbl.create 13 in
          let min_group = Hashtbl.create 13 in
          Hashtbl.iter
            (fun grp _ ->
              Hashtbl.add max_group grp infinity;
              Hashtbl.add min_group grp neg_infinity
            )
            group_weight;
          (* Now do the adjustments: *)
          List.iter
            (fun grp ->
              let imposed_max = Hashtbl.find max_group grp in
              let imposed_min = Hashtbl.find min_group grp in
              (* Printf.printf "Group %d: imax=%f imin=%f\n" grp imposed_max imposed_min; *)
              let real_max = ref neg_infinity in
              let real_min = ref infinity in
              Hashtbl.iter
                (fun id grp_of_id ->
                  if grp = grp_of_id then (
                    let zset, g = Hashtbl.find node_ht id in
                    (* Printf.printf "  g=%f\n" g; *)
                    let g' = min imposed_max (max imposed_min g) in
                    real_max := max !real_max g';
                    real_min := min !real_min g';
                    Hashtbl.replace node_ht id (zset, g')
                  )
                )
                mono_group;
              (* Printf.printf "  rmax=%f rmin=%f\n" !real_max !real_min; *)
              let cur_lbound, cur_ubound = Hashtbl.find mono_bounds grp in
              Hashtbl.iter
                (fun b_grp (lbound, ubound) ->
                  let x_greater =
                    (* CHECK: we only compare the lower bound. Sufficient?
                     *)
                    match lbound, cur_lbound with
                      | None, None -> false
                      | Some _, None -> true
                      | None, Some _ -> false
                      | Some l1, Some l2 ->
                          Ztypes.(ord.ord_cmp_asc) l1 l2 > 0 in
                  let y_bounding =
                    if x_greater then
                      match mono_type with
                        | `Increasing -> `Lower
                        | `Decreasing -> `Upper
                    else
                      match mono_type with
                        | `Increasing -> `Upper
                        | `Decreasing -> `Lower in
                  (* Printf.printf "  b_grp=%d x_greater=%B " b_grp x_greater; *)
                  match y_bounding with
                    | `Lower ->
                        (* Printf.printf "lower "; *)
                        let new_min =
                          max !real_max (Hashtbl.find min_group b_grp) in
                        Hashtbl.replace
                          min_group b_grp new_min;
                    (* Printf.printf "new_min=%f\n" new_min; *)
                    | `Upper ->
                        (* Printf.printf "upper ";*)
                        let new_max =
                          min !real_min (Hashtbl.find max_group b_grp) in
                        Hashtbl.replace max_group b_grp new_max;
                (* Printf.printf "new_max=%f\n" new_max;*)
                )
                mono_bounds
            )
            groups;
  );
 *)
  (* Restore the tree, and add up gammas: *)
  let model =
    Tree.map_leaves
      (fun id ->
        let (zset,g) = Hashtbl.find node_ht id in
        Zset.iter
          (fun start len ->
            for id = start to start+len-1 do
              gamma.{id} <- g
            done
          )
          zset;
        g
      )
      idtree in
  (model, gamma)


module Tree_Gradient_Booster(L:LOSS)(B:TREE_BASELEARNER with module DS=L.DS)
       : TREE_GRADIENT_BOOSTER with module DS=L.DS and module L=L =
  struct
    module DS = L.DS
    module L = L
    module B = B

    type model = float tree

    type wrapped_loss =
      | Wrapped : 'a Ztypes.ztype * 'a L.stage -> wrapped_loss

    type stage =
      { ds : DS.dataset;
        domain : Zset.zset;
        stage_count : int;
        base_approx : vector;
        next_approx : vector Lazy.t;
        next_loss : wrapped_loss Lazy.t;
        gamma : vector;
        model : model;
        gradient : vector;
        loss_value : float Lazy.t;
      }

    let compute_next_approx base_approx gamma =
      lazy
        (Vector.mapi
           (fun i g ->
              base_approx.{i} +. g
           )
           gamma
        )


    let start ds trainset =
      let y_col_name = DS.y_column ds in
      let ztab = DS.ztable ds in
      let Ztypes.ZcolBox(_, real_y_ty, _, _) =
        Ztable.get_zcolbox ztab y_col_name in
      if not (L.input_ok real_y_ty) then
        failwith "Zmola.Booster.Tree_Gradient_Booster.start: the loss \
                  module does not support this type of y column";
      let (Ztypes.Space n) = Ztable.space (DS.ztable ds) in
      (* Note that we enforce here bitsets as the representation for the
         training set. This is a preferable choice for the base learners
         that have been written so far.
       *)
      let domain = Zset.copy Zset.bitset_pool trainset in
      let stage_count = 0 in
      let base_approx = Vector.make n 0.0 in
      let f0 = L.initial_approx ds trainset in
      let gamma = Vector.make n f0 in
      let model = Leaf f0 in
      let gradient = Vector.make n 0.0 in
      let next_approx = compute_next_approx base_approx gamma in
      let next_loss_stage = lazy(L.stage ds domain !!next_approx) in
      let next_loss = lazy(Wrapped(L.y_type, !!next_loss_stage)) in
      let loss_value = lazy (L.loss !!next_loss_stage domain) in
      { ds; domain; stage_count; base_approx; next_approx; next_loss;
        gamma; model; gradient; loss_value }

(*
    let get_mono_x_columns ds =
      List.flatten
        (List.map
           (fun (n,hints) ->
             match DS.(hints.col_monotonic) with
               | None -> []
               | Some typ -> [n, typ]
           )
           (DS.x_columns ds)
        )
 *)

    let advance s =
      (* let t0 = Unix.gettimeofday() in *)
      let n = Bigarray.Array1.dim s.base_approx in
      let pool = Zset.create_pool 1000 1000 in
      let ds = s.ds in
      let domain = s.domain in
      let stage_count = s.stage_count + 1 in
      (* First commit the gamma vector from the last round: *)
      let base_approx = !! (s.next_approx) in
      (* Get the loss data structures: *)
      let Wrapped(y_ty,loss) = !! (s.next_loss) in
      match Ztypes.same_ztype y_ty L.y_type with
        | Ztypes.Not_equal -> assert false
        | Ztypes.Equal ->
            (* Get the pseudo response (gradient vector): *)
            let y_col_name = DS.y_column ds in
            let ztab = DS.ztable ds in
            let Ztypes.ZcolBox(_, real_y_ty, _, _) =
              Ztable.get_zcolbox ztab y_col_name in
            let y_col = Ztable.get_zcol ztab real_y_ty y_col_name in
            let convert = L.input real_y_ty in
            let gradient = Vector.make n 0.0 in
            Zaggreg.fold
              (fun id y () ->
                gradient.{id} <-
                  L.gradient loss id base_approx.{id} (convert y)
              )
              y_col
              domain
              ();
            (* Influence trimming: focus on those rows that really count *)
            let effset = L.trim loss gradient pool in
            let have_trimming = Zset.count effset < Zset.count domain in
            (* Printf.printf "have_trimming=%B\n%!" have_trimming;*)
            (* let t1 = Unix.gettimeofday() in*)
            (* Fire off the base learner: *)
            let effset = Zset.copy Zset.bitset_pool effset in
            let learner = B.learn ds stage_count pool gradient effset in
            let bmodel = B.model learner in
            (* If we trimmed something, the zsets in bmodel only cover 
               effset but not domain. Need to recompute these sets
             *)
            let bmodel =
              if have_trimming then
                Tree.map_zset_tree
                  ztab pool domain (fun _ zset -> zset) bmodel
              else
                bmodel in
            (* let t2 = Unix.gettimeofday() in *)
            (* Compute the gamma values: *)
            (* let mono_x_columns = get_mono_x_columns ds in *)
            let model, gamma =
              gamma_of_tree L.gamma loss gradient bmodel in
            let next_approx = compute_next_approx base_approx gamma in
            let next_loss_stage = lazy(L.stage ds domain !!next_approx) in
            let next_loss = lazy(Wrapped(L.y_type, !!next_loss_stage)) in
            let loss_value = lazy (L.loss !!next_loss_stage domain) in
            (* let t3 = Unix.gettimeofday() in *)
            (* Printf.printf "    [booster t1=%.3f t2=%.3f t3=%.3f]\n%!"
                   (t1 -. t0) (t2 -. t1) (t3 -. t2);
             *)
            { ds; domain; stage_count; base_approx; next_approx; next_loss;
              gamma; model; gradient; loss_value }
     
    let multiply learning_rate s =
      if learning_rate = 1.0 then
        s
      else
        let gamma =
          Vector.map
            (fun g -> learning_rate *. g)
            s.gamma in
        let model =
          Tree.map_leaves
            (fun g -> learning_rate *. g)
            s.model in
        let next_approx =
          lazy
            (Vector.mapi
               (fun i g ->
                s.base_approx.{i} +. g
               )
               gamma
            ) in
        let next_loss_stage = lazy(L.stage s.ds s.domain !!next_approx) in
        let next_loss = lazy(Wrapped(L.y_type, !!next_loss_stage)) in
        let loss_value =  lazy (L.loss !!next_loss_stage s.domain) in
        { s with gamma; model; next_approx; next_loss; loss_value }

    let stage_count s = s.stage_count
    let domain s = s.domain
    let gamma s id = s.gamma.{id}
    let approximation s id = !!(s.next_approx).{id}
    let gradient s id = s.gradient.{id}
    let model s = s.model
    let loss s = Lazy.force s.loss_value

    (* ---- session support ---- *)
                            
    type Zsession.Stypes.live_object +=
       | Stage of stage
                            
    let to_stash (stash : (module Zsession.Stypes.SESSION_STASH))
                 i stage suffix =
      let module Stash = (val stash) in
      let sl = Stash.get_slot i in
      let upd = Stash.update sl in
      Stash.add_object upd ("Zmola.Booster.stage" ^ suffix) (Stage stage);
      let param = Stash.slot_param sl in
      let param = if param = `Null then `Assoc [] else param in
      let param =
        Zsession.Util.json_update
          param
          (`Assoc [ "Zmola.Booster.stage_count" ^ suffix,
                    `Int stage.stage_count]) in
      Stash.set_param upd param;
      let filename_domain =
        Stash.add_file upd ("Zmola.Booster.domain"^suffix) "zset" in
      Zsession.Util.write_zset filename_domain stage.domain;
      let filename_base_approx =
        Stash.add_file upd ("Zmola.Booster.base_approx"^suffix) "vector" in
      Zsession.Util.write_vector filename_base_approx stage.base_approx;
      let filename_gamma =
        Stash.add_file upd ("Zmola.Booster.gamma"^suffix) "vector" in
      Zsession.Util.write_vector filename_gamma stage.gamma;
      let filename_gradient =
        Stash.add_file upd ("Zmola.Booster.gradient"^suffix) "vector" in
      Zsession.Util.write_vector filename_gradient stage.gradient;
      let filename_model =
        Stash.add_file upd ("Zmola.Booster.model"^suffix) "model" in
      Yojson.Safe.to_file
        filename_model
        (Tree.json_of_model stage.model);
      Stash.commit upd

    let from_stash (stash : (module Zsession.Stypes.SESSION_STASH))
                   i ds pool suffix =
      let module Stash = (val stash) in
      let sl = Stash.get_slot i in
      let objs = Stash.slot_objects sl in
      match List.assoc ("Zmola.Booster.stage" ^ suffix) objs with
        | Stage stage ->
            stage
        | _ ->
            failwith "Zmola.Booster.from_stash"
        | exception Not_found ->
            let files = Stash.slot_files sl in
            let domain =
              match List.assoc ("Zmola.Booster.domain"^suffix) files with
                | (filename,_) -> Zsession.Util.read_zset pool filename
                | exception Not_found ->
                    failwith "Zmola.Booster.from_stash" in
            let base_approx =
              match List.assoc ("Zmola.Booster.base_approx"^suffix) files with
                | (filename,_) -> Zsession.Util.read_vector filename
                | exception Not_found ->
                    failwith "Zmola.Booster.from_stash" in
            let gamma =
              match List.assoc ("Zmola.Booster.gamma"^suffix) files with
                | (filename,_) -> Zsession.Util.read_vector filename
                | exception Not_found ->
                    failwith "Zmola.Booster.from_stash" in
            let gradient =
              match List.assoc ("Zmola.Booster.gradient"^suffix) files with
                | (filename,_) -> Zsession.Util.read_vector filename
                | exception Not_found ->
                    failwith "Zmola.Booster.from_stash" in
            let model =
              match List.assoc ("Zmola.Booster.model"^suffix) files with
                | (filename,_) ->
                    Tree.model_of_json (Yojson.Safe.from_file filename)
                | exception Not_found ->
                    failwith "Zmola.Booster.from_stash" in
            let param = Stash.slot_param sl in
            let param = if param = `Null then `Assoc [] else param in
            let stage_count =
              param |> U.member ("Zmola.Booster.stage_count"^suffix)
              |> U.to_int in
            let next_approx = compute_next_approx base_approx gamma in
            let next_loss_stage = lazy(L.stage ds domain !!next_approx) in
            let next_loss = lazy(Wrapped(L.y_type, !!next_loss_stage)) in
            let loss_value = lazy (L.loss !!next_loss_stage domain) in
            { ds; domain; stage_count; base_approx; next_approx; next_loss;
              gamma; model; gradient; loss_value }

    let stash_files suffix =
      [ "Zmola.Booster.domain"^suffix;
        "Zmola.Booster.base_approx"^suffix;
        "Zmola.Booster.gamma"^suffix;
        "Zmola.Booster.gradient"^suffix;
        "Zmola.Booster.model"^suffix
      ]
              
  end
