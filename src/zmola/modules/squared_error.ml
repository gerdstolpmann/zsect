open Types
open Squared_error_types
open Zcontainer

module Int =
struct
  type t = int
  let compare (x:t) (y:t) = compare x y
end

module IntSet = Set.Make(Int)

module Interval_Set : SIMPLE_SET with type elt = int =
struct
  type elt = int
  type t = int * int
  let empty = (0,-1)
  let is_empty ((a,b):t) = a > b
  let mem x ((a,b):t) = x >= a && x <= b

  let add x ((a,b):t) =
    if is_empty (a,b) then
      (x,x)
    else if x+1 = a then
      (x,b)
    else if b+1 = x then
      (a,x)
    else
      failwith "Interval_Set.add"

  let singleton (x:int) = (x,x)

  let norm (a,b) =
    if is_empty (a,b) then empty else (a,b)

  let remove x ((a,b):t) =
    if x = a then
      norm (a+1,b)
    else if x = b then
      norm (a,b-1)
    else if x < a || x > b then
      (a,b)
    else
      failwith "Interval_Set.remove"

  let iter f ((a,b):t) =
    for k = a to b do f k done

  let rec fold f ((a,b):t) acc =
    if is_empty (a,b) then
      acc
    else
      fold f (a+1,b) (f a acc)

  let elements s =
    let cons x l = x :: l in
    fold cons s []

  let of_list l =
    List.fold_left (fun acc x -> add x acc) empty l

  let diff (s1:t) (s2:t) =
    let (a1,b1) = s1 in
    let (a2,b2) = s2 in
    if is_empty s2 then
      s1
    else if a2 >= a1 && a2 <= b1 && b2 >= b1 then
      norm (a1,a2-1)
    else if b2 >= a1 && b2 <= b1 && a2 <= a1 then
      norm (b2+1,b1)
    else if a2 > b1 || b2 < a1 then
      s1
    else if a2 <= a1 && b2 >= b1 then
      empty
    else
      failwith "Interval_Set.diff"

  let min_elt ((a,b):t) =
    if is_empty (a,b) then raise Not_found else a

  let max_elt ((a,b):t) =
    if is_empty (a,b) then raise Not_found else b

  let filter _ =
    failwith "Interval_Set.filter: unimplemented"

  let union _ =
    failwith "Interval_Set.filter: unimplemented"

  module S = IntSet

  let to_set s =
    fold S.add s S.empty
end

module StdSet(I:Set.OrderedType) : SIMPLE_SET with type elt = I.t =
struct
  module S = Set.Make(I)
  include S
  let to_set s = s
end


module Interval_Map : SIMPLE_MAP with type key = int =
struct
  type key = int
  type 'a t =
    { data : 'a option array;
      offset : int;
    }

  let of_list l =
    if l = [] then
      { data=[| |]; offset=0}
    else
      let min_key =
        List.fold_left
          (fun acc (k,_) -> min acc k)
          (fst(List.hd l))
          (List.tl l) in
      let max_key =
        List.fold_left
          (fun acc (k,_) -> max acc k)
          (fst(List.hd l))
          (List.tl l) in
      let data = Array.make (max_key - min_key + 1) None in
      let offset = min_key in
      List.iter
        (fun (i,x) ->
           data.(i-offset) <- Some x
        )
        l;
      { data; offset }

  let is_empty m =
    Array.length m.data = 0

  let mem i m =
    (i >= m.offset && i < m.offset + Array.length m.data) &&
      m.data.(i - m.offset) <> None
      
  let find i m =
    if i < m.offset || i >= m.offset + Array.length m.data then
      raise Not_found;
    match m.data.(i - m.offset) with
      | None -> raise Not_found
      | Some x -> x

  let iter f m =
    Array.iteri
      (fun i xopt ->
         match xopt with
           | None -> ()
           | Some x -> f (i + m.offset) x
      )
      m.data

  let fold f m acc =
    let acc = ref acc in
    iter
      (fun i x -> acc := f i x !acc)
      m;
    !acc

end


module StdMap(I:Set.OrderedType) : SIMPLE_MAP with type key = I.t =
struct
  module M = Map.Make(I)
  include M
  let of_list l =
    List.fold_left
      (fun acc (key,x) -> add key x acc)
      empty
      l
end


module Make_Squared_Error
         (I:Set.OrderedType)
         (S:SIMPLE_SET with type elt = I.t)
         (M:SIMPLE_MAP with type key = I.t) :
          SQUARED_ERROR with module Index=I =
struct
  module Index = I
  module IndexSet = S
  module IndexMap = M


  type index = Index.t

  type set =
    { zset : Zset.zset;
      sum : float;         (* sum of numbers per set *)
      count : int;         (* counts per set *)
      error : float
       (* SUM(i IN S . (numbers.(i) - sum(S)/count(S))^2) *)
    }

  type squared_errors =
    { numbers : vector;
      sets : set IndexMap.t;
    }

  type stats =
    { st_iset : IndexSet.t;
      st_count : int;
      st_sum : float;
      st_error : float
    }

  let sum_of (numbers:vector) set =
    let acc = ref 0.0 in
    Zset.iter
      (fun start len ->
         for id = start to start+len-1 do
           acc := !acc +. numbers.{id}
         done
      )
      set;
    !acc

  let error_of (numbers:vector) set avg =
    let acc = ref 0.0 in
    Zset.iter
      (fun start len ->
         for id = start to start+len-1 do
           let diff = numbers.{id} -. avg in
           acc := !acc   +.   diff *. diff 
         done
      )
      set;
    !acc

  let create (numbers:vector) sets_l =
    let sets =
      List.fold_left
        (fun acc (i,zset) ->
           let sum = sum_of numbers zset in
           let count = Zset.count zset in
           let error = error_of numbers zset (sum /. float count) in
           let s = { zset; sum; count; error } in
           (i,s) :: acc
        )
        []
        sets_l in
    { numbers; sets = M.of_list sets }

  let check sqe =
    let n = Vector.length sqe.numbers in
    IndexMap.iter
      (fun i s ->
         if Zset.is_empty s.zset then
           failwith "Aliendog.Squared_error.check: \
                     found an empty set";
         if Zset.last s.zset > n then
           failwith "Aliendog.Squared_error.check: \
                     set refers to non-existing row";
         IndexMap.iter
           (fun i2 s2 ->
              if I.compare i i2 <> 0 && Zset.isect_non_empty s.zset s2.zset then
                failwith "Aliendog.Squared_error.check: \
                          sets are not pairwise disjoint"
           )
           sqe.sets
      )
      sqe.sets


  let permute sqe perm =
    let permute_map m =
      let l =
        IndexMap.fold
          (fun i x acc ->
             (perm i, x) :: acc
          )
          m
          [] in
      M.of_list l in
    { numbers = sqe.numbers;
      sets = permute_map sqe.sets;
    }

  let get_zset_at sqe i =
    try
      (IndexMap.find i sqe.sets).zset
    with
      | Not_found -> Zset.empty

  let get_zsets sqe iset pool =
    let indices = IndexSet.elements iset in
    List.map (get_zset_at sqe) indices

  let get_zset sqe iset pool =
    let sets = get_zsets sqe iset pool in
    Zset.union_many pool sets

  let all sqe =
    IndexMap.fold
      (fun i _ acc -> IndexSet.add i acc)
      sqe.sets
      IndexSet.empty

  let count_at sqe i =
    try (IndexMap.find i sqe.sets).count
    with Not_found -> 0

  let count sqe iset =
    IndexSet.fold
      (fun i acc -> acc + count_at sqe i)
      iset
      0

  let sum_at sqe i =
    try (IndexMap.find i sqe.sets).sum
    with Not_found -> 0.0

  let sum sqe iset =
    IndexSet.fold
      (fun i acc -> acc +. sum_at sqe i)
      iset
      0.0

  let avg sqe iset =
    sum sqe iset /. float (count sqe iset)

  let error_at sqe i =
    try (IndexMap.find i sqe.sets).error
    with Not_found -> 0.0

  let squared_error_add_1 sqe iset_sum iset_count iset_error i =
    let iset_count = float iset_count in
    let n = float(count_at sqe i) in
    let s = sum_at sqe i in
    let c1 = iset_sum /. iset_count in
    let c2 = (iset_sum +. s) /. (iset_count +. n) in
    let d = c2 -. c1 in
    let e2_old = iset_error  +.  iset_count *. d *. d in
    let e = c2 -. s /. n in
    let e2_new = error_at sqe i  +.  n *. e *. e in
    e2_old +. e2_new

  let squared_error sqe iset =
    let r = ref 0.0 in
    ( try
        IndexSet.iter
          (fun i0 ->
             if IndexMap.mem i0 sqe.sets then (
               let iset1 = IndexSet.remove i0 iset in
               let _, _, error =
                 IndexSet.fold
                   (fun i (acc_sum,acc_count,acc_error) ->
                      if IndexMap.mem i sqe.sets then
                        (acc_sum +. sum_at sqe i,
                         acc_count + count_at sqe i,
                         squared_error_add_1 sqe acc_sum acc_count acc_error i
                        )
                      else
                        (acc_sum,acc_count,acc_error)
                   )
                   iset1
                   (sum_at sqe i0, count_at sqe i0, error_at sqe i0) in
               r := error;
               raise Exit
             )
          )
          iset
      with Exit -> ()
    );
    !r

  let stats sqe iset =
    { st_iset = iset;
      st_count = count sqe iset;
      st_sum = sum sqe iset;
      st_error = squared_error sqe iset
    }

  let stats_at sqe i =
    { st_iset = IndexSet.singleton i;
      st_count = count_at sqe i;
      st_sum = sum_at sqe i;
      st_error = error_at sqe i;
    }

  let stats_empty =
    { st_iset = IndexSet.empty;
      st_count = 0;
      st_sum = 0.0;
      st_error = 0.0
    }

  let stats_iset st = st.st_iset
  let stats_count st = st.st_count
  let stats_sum st = st.st_sum
  let stats_avg st = st.st_sum /. float st.st_count
  let stats_error st = st.st_error

  let stats_add sqe st i =
    if IndexSet.mem i st.st_iset then
      st
    else
      if not (IndexMap.mem i sqe.sets) then
        st
      else if IndexSet.is_empty st.st_iset then
        stats_at sqe i
      else
        { st_iset = IndexSet.add i st.st_iset;
          st_count = st.st_count + count_at sqe i;
          st_sum = st.st_sum +. sum_at sqe i;
          st_error = squared_error_add_1
                       sqe st.st_sum st.st_count st.st_error i
        }

  let stats_sub sqe st i =
    if not(IndexSet.mem i st.st_iset) then
      st
    else
      if not (IndexMap.mem i sqe.sets) then
        st
      else if IndexSet.is_empty (IndexSet.remove i st.st_iset) then
        stats_empty
      else (
        let n = float(count_at sqe i) in
        let s = sum_at sqe i in
        let iset_count = float st.st_count in
        let iset_sum = st.st_sum in
        let iset_error = st.st_error in
        let c2 = iset_sum /. iset_count in
        let c1 = (iset_sum -. s) /. (iset_count -. n) in
        let d = c1 -. c2 in
        let e1_old = iset_error  +.  iset_count *. d *. d in
        let e = s /. n -. c1 in
        let e1_new = error_at sqe i  +.  n *. e *. e  in
        { st_iset = IndexSet.remove i st.st_iset;
          st_count = st.st_count - count_at sqe i;
          st_sum = st.st_sum -. sum_at sqe i;
          st_error = e1_old -. e1_new
        }
      )

  let stats_update sqe st iset2 =
    let iset1 = st.st_iset in
    let adds = IndexSet.diff iset2 iset1 in
    let subs = IndexSet.diff iset1 iset2 in
    let st' =
      IndexSet.fold
        (fun i acc ->
           stats_sub sqe acc i
        )
        subs
        st in
    let st'' =
      IndexSet.fold
        (fun i acc ->
           stats_add sqe acc i
        )
        adds
        st' in
    st''


end


(* Implementation sketch:

   type squared_errors =
     { numbers : vector;
       sets : Zset.zset IndexMap;   (* maps index -> Zset.zset *)
       sum : float IndexMap;       (* sum of numbers per set *)
       count : int IndexMap;       (* counts per set *)
       error : float IndexMap;
         (* SUM(i IN S . (numbers.(i) - sum(S)/count(S))^2) *)
     }

     The functions are now implemented as follows:

   - count: just sum up all counts selected by the IndexSet
   - sum: just sum up all sums selected by the IndexSet
   - avg = sum/count

   - squared_error_add sqe s1 e1 i: if i IN s1, just return e1.
     If s1 is empty, just return error(i).
     let s2 = s1 + set(i)
     let c1 = avg(s1) = SUM(x in S1.x) / SUM(x in S1.|x|)
     let c2 = avg(s2)
     
     Now, e2 = e2_old + e2_new
     where e2_old = SUM(x IN S1 . (x - c2)^2)
       and e2_new = SUM(x IN set(i) . (x - c2)^2)

     Reduce computation of e2_old to e1 = SUM(x IN S1 . (x - c1)^2):
     e2_old = e1 - 2*d*SUM(x in S1.x) + 2*n*c1*d + n*d^2
            = e1 + n*d^2
      where d = c2-c1
            n = count(S1)

     Note that we get SUM(x in S1.x) by folding over squared_errors.count.

     Reduce computation of e2_new to
                             error(i) = SUM(x IN set(i) . (x - avg(i))^2)
     (and error(i) can again be found in type squared_errors):

     e2_new = error(i) - 2*d*SUM(x in set(i).x) + 2*n*c(i)*d + n*d^2
            = error(i) + n*d^2
      where c(i) = avg(set(i))
        and d = c2-c(i)
        and n = count(set(i))

     We find SUM(x in set(i).x) in squared_errors.sum.

   - squared_error_sub sqe s2 e2 i: if i not IN s2, just return e2. If
     s2=set(i) return 0.
     let s1 = s2 - set(i)
     let c1 = avg(s1)
     let c2 = avg(s2)
     
     Now, e1 = e1_old - e1_new
     where e1_old = SUM(x IN S2 . (x - c1)^2)
       and e1_new = SUM(x IN set(i) . (x - c1)^2)

     e1_old and e1_new are computed the same way as above.

     e1_old = e2 + n*d^2
     e1_new = error(i) + n*d^2

   - squared_error_update: compare the two input sets and reduce to
     adds and subs.

 *)
