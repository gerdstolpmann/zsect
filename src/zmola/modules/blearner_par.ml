(* parallel base learner *)

open Zcontainer
open Types
open Printf
open Uq_engines.Operators

type algo =
  [ `Bisect ]

let num_workers = ref 8

module Proto_in_ser(DS:DATASET) = struct
  module Ser = Dataset.Serialize(DS)

  let to_json algo ds vec_name zset_name feature =
    let algo_name =
      match algo with
        | `Bisect -> "Bisect" in
    `Assoc [ "algo", `String algo_name;
             "dataset", Ser.to_json ds;
             "vector", `String vec_name;
             "set", `String zset_name;
             "feature", `String feature;
           ]
end

module Proto_in_deser = struct
  module DS = Dataset.Deserialize

  let from_json (j:Yojson.Safe.json) :
        (algo * DS.dataset * vector * Zset.zset * string) =
    let fail() =
      failwith "Zmola.Blearner_par.Proto_in_deser.from_json: bad json" in
    match j with
      | `Assoc [ "algo", `String algo_name;
                 "dataset", j_dataset;
                 "vector", `String vec_name;
                 "set", `String zset_name;
                 "feature", `String fname;
               ] ->
          let algo =
            match algo_name with
              | "Bisect" -> `Bisect
              | _ -> fail() in
          let dataset = DS.from_json j_dataset in
          let vector = Vector.access_persistent vec_name in
          let zset = Zset.access zset_name in
          (algo, dataset, vector, zset, fname)
      | _ ->
          fail()
end

module Proto_out_ser = struct
  let to_json loss (part : (Zset.zset * expr) list) : Yojson.Safe.json =
    let part_to_json (zset, expr) =
      let tmp_zset = Zset.persist zset in
      `Assoc [ "set", `String tmp_zset;
               "expr", Tree.json_of_expr expr
             ] in
    `Assoc [ "loss", `Float loss;
             "partitioning", `List (List.map part_to_json part);
           ]
end

module Proto_out_deser = struct
  let from_json pool (j:Yojson.Safe.json) : (float * (Zset.zset * string * expr) list) =
    let fail() =
      failwith "Zmola.Blearner_par.Proto_out_deser.from_json: bad json" in
    let part_from_json =
      function
      | `Assoc [ "set", `String tmp_zset;
                 "expr", j_expr
               ] ->
          let zset = Zset.access tmp_zset in
          let expr = Tree.expr_of_json j_expr in
          (zset,tmp_zset,expr)
      | _ ->
          fail() in
    match j with
      | `Assoc [ "loss", `Float loss;
                 "partitioning", `List j_parts;
               ] ->
          let parts = List.map part_from_json j_parts in
          (loss, parts)
      | _ ->
          fail()

end

module Base_step(DS:DATASET) :
          TREE_BASELEARNER_STEP with module DS = DS =
  struct
    module DS = DS
    module Ser = Proto_in_ser(DS)
    type base_step =
      { pool : Zset.pool;
        depth : int;
        vector : vector;
        vector_name : string;
        vector_count : int ref;
        domain : Zset.zset;
        domain_file : string;
        domain_count : int ref;
        feature : (string * DS.col_hint) option;
        expr : expr;
        esys : Unixqueue.event_system;
      }

    let start pool vec set =
      let (vector, vector_name) = Vector.persist vec in
      let domain_file = Zset.persist set in
      { pool;
        depth = 0;
        vector;
        vector_name;
        vector_count = ref 1;
        domain = set;
        domain_file;
        domain_count = ref 1;
        feature = None;
        expr = E_And [];
        esys = Unixqueue.create_unix_event_system()
      }

    let depth bs = bs.depth
    let feature bs =
      match bs.feature with
        | None -> failwith "feature"
        | Some (name,hint) -> (name,hint)
    let domain bs = bs.domain
    let expr bs = bs.expr
    let base_loss bs = assert false  (* TODO *)

    let jobs = Queue.create()
    (* (producer, consumer) pairs *)

    let workers = ref [| |]
    let free_workers = ref 0

    let start_workers esys =
      let w =
        Array.init
          !num_workers
          (fun _ ->
            let fd1, fd_out = Unix.pipe() in
            let stdin = Shell.from_fd fd1 in
            let fd_in, fd4 = Unix.pipe() in
            let stdout = Shell.to_fd fd4 in
            let cmd =
              Shell_sys.command
                ~arguments:[| "-c";
                              "zmola_blearn || ocamlfind zmola/zmola_blearn" |]
                ~filename:"/bin/sh"
                () in
            let e = new Shell_uq.call_engine ~stdin ~stdout [cmd] esys in
            Unix.close fd1;
            Unix.close fd4;
            let e' =
              e >> (fun state ->
                Unix.close fd_in;
                state
              ) in
            let buf =
              Uq_io.create_in_buffer (`Polldescr(`Read_write,fd_in,esys)) in
            (e', fd_in, fd_out, buf, ref false)
          ) in
      workers := w;
      free_workers := !num_workers;
      if !Log.debug then
        eprintf "Started workers\n%!"

    let stop_workers esys =
      Array.iter
        (fun (_, _, fd, _, _) ->
          Unix.close fd
        )
        !workers;
      workers := [| |];
      free_workers := 0;
      if !Log.debug then
        eprintf "Closed worker pipes\n%!"

    let find_free_worker () =
      snd
        (Array.fold_left
           (fun (i,acc) (_,_,_,_,busy) ->
             if acc >= 0 || !busy then (i+1,acc) else (i+1, i)
           )
           (0, (-1))
           !workers
        )

    let rec submit esys produce consume_e =
      if !workers = [| |] then
        start_workers esys;
      if !free_workers > 0 then (
        let w = find_free_worker() in
        assert(w >= 0);
        let (_, fd_in, fd_out, buf, busy) = !workers.(w) in
        busy := true;
        decr free_workers;
        let s = produce() ^ "\n" in
        Netsys.really_gwrite_tstr
          `Read_write fd_out (`String s) 0 (String.length s);
        if !Log.debug then
          eprintf "Request sent to worker %d\n%!" w;
        let _e =
          Uq_io.input_line_e (`Buffer_in buf)
          ++ (fun line ->
            if !Log.debug then
              eprintf "Response from worker %d\n%!" w;
            busy := false;
            incr free_workers;
            submit_next esys;
            consume_e line
          ) in
        ()
      ) else
        Queue.add (produce,consume_e) jobs

    and submit_next esys =
      assert(!free_workers > 0);
      if not (Queue.is_empty jobs) then (
        let (produce,consume_e) = Queue.take jobs in
        submit esys produce consume_e
      ) else
        if !Log.debug then
          eprintf "Job queue is empty\n%!"

    let feature_refine ds ft_name bs cont =
      let hint = List.assoc ft_name (DS.x_columns ds) in
      submit
        bs.esys
        (fun () ->
          let j_out =
            Ser.to_json `Bisect ds bs.vector_name bs.domain_file ft_name in
          Yojson.Safe.to_string j_out
        )
        (fun line ->
          let j_in = Yojson.Safe.from_string line in
          let out =
            if j_in = `Null then
              None
            else
              let (loss, partitioning) =
                Proto_out_deser.from_json Zset.default_pool j_in in
              let bs_list =
                List.map
                  (fun (dom,dom_file,expr) ->
                    incr bs.vector_count;
                    { bs with
                      depth = bs.depth + 1;
                      domain = dom;
                      domain_file = dom_file;
                      domain_count = ref 1;
                      feature = Some(ft_name,hint);
                      expr = expr;
                    }
                  )
                  partitioning in
              Some(loss, bs_list) in
          cont out;
          eps_e (`Done ()) bs.esys
        )

    let finalize bs =
      if !(bs.vector_count) > 0 then (
        decr bs.vector_count;
        if !(bs.vector_count) = 0 then (
          Vector.unpersist bs.vector_name;
          stop_workers bs.esys;
          Unixqueue.run bs.esys;
          if !Log.debug then
            eprintf "Workers all stopped\n%!"
        )
      );
      if !(bs.domain_count) > 0 then (
        decr bs.domain_count;
        if !(bs.domain_count) = 0 then
          Zset.unpersist bs.domain_file
      )

    exception Refine_exit

    let refine ds step steps_to_go enabled_x_columns =
      let refine_exit() =
        let g = Unixqueue.new_group step.esys in
        Unixqueue.once step.esys g 0.0 (fun () -> raise Refine_exit) in
      let x_columns =
        List.filter
          (fun (n,_) -> List.mem n enabled_x_columns)
          (DS.x_columns ds) in
      (* Invoke the base learner: *)
      let x_columns = Array.of_list x_columns in
      let col_results = Array.map (fun _ -> None) x_columns in
      let n = ref (Array.length x_columns) in
      Array.iteri
        (fun i (colname,hint) ->
          let t0 = Unix.gettimeofday() in
          feature_refine
            ds colname step
            (fun col_result ->
              col_results.(i) <- col_result;
              decr n;
              let t1 = Unix.gettimeofday() in
              if !Log.debug then (
                eprintf "    [column=%s%!" colname;
                eprintf " %.3fseconds]%!\n" (t1-.t0)
              );
              if !n=0 then (
                if !Log.debug then
                  eprintf "Jobs accomplished\n%!";
                refine_exit()
              )
            )
        )
        x_columns;
      let break = ref false in
      let old_sigint =
        Sys.signal
          Sys.sigint
          (Sys.Signal_handle
             (fun _ ->
               break := true;
               eprintf "[Seen SIGINT - will stop shortly]\n%!";
          )) in
      ( try
          Unixqueue.run step.esys
        with Refine_exit -> ()
      );
      Sys.set_signal Sys.sigint old_sigint;
      if !break && !Sys.interactive then (
        Array.iter
          (function
           | None -> ()
           | Some (_, bs_list) ->
               List.iter finalize bs_list
          )
          col_results;
        raise Sys.Break;
      );
      if !Log.debug then
        eprintf "Passing results back to caller\n%!";
      let col_results =
        Array.mapi
          (fun i r ->
            let (colname,hint) = x_columns.(i) in
            (colname,hint,r)
          )
          col_results in
      let col_results = Array.to_list col_results in
      let col_results =
        List.flatten
          (List.map
             (function
              | (_,_,None) -> []
              | (colname,hint,Some (loss, steps)) -> [colname,loss,steps]
             )
             col_results) in
      List.sort
        (fun (_,loss1,_) (_,loss2,_) -> compare loss1 loss2)
        col_results
  end
