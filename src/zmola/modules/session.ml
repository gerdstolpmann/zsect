open Zcontainer
open Session_types
open Printf
module U = Yojson.Safe.Util

open Zsession

module Identity_base_stepper(BS:Types.TREE_BASELEARNER_STEP)
       : Types.TREE_BASELEARNER_STEP with module DS = BS.DS =
  struct
    module DS = BS.DS
    type base_step = BS.base_step
    let start = BS.start
    let depth = BS.depth
    let feature = BS.feature
    let domain = BS.domain
    let expr = BS.expr
    let base_loss = BS.base_loss
    let refine = BS.refine
    let finalize = BS.finalize
  end

module Identity_baselearner(TL:Types.TREE_BASELEARNER)
       : Types.TREE_BASELEARNER with module DS = TL.DS =
  struct
    module DS = TL.DS
    type base_output = TL.base_output
    type model = TL.model
    let learn = TL.learn
    let model = TL.model
    let partitioning = TL.partitioning
  end

module Make_UI_P0(SC : Zsession.Stypes.SESSION_CONFIG) =
  struct
    let self = ref (None : (module BOOST_UI_P0) option)
    let load_callback = ref (fun _ -> ())

    module AC =
      struct
        let kind = "Zmola.Session"
        let load i = !load_callback i
        let init _ = ()
      end

    module SH = Zsession.Core.Make_Stash(SC)
    module SI = Zsession.Core.Make_Input(SH)
    module Core_UI = Zsession.Core.Make_UI(AC)(SI)
    include Core_UI

    type loss_type = [`Abs_dev | `Squared_error | `Huber | `Bilog ]
                                     
    type spec_col_hint =
      { col_ordinal : bool;
        (* col_monotonic : [`Increasing|`Decreasing] option; *)
      }

    let hint_tuple h =
      h.col_ordinal

              
    type spec =
      { mutable xcolumns : (string * spec_col_hint) list;
        mutable xcolumns_unbundled : (string * spec_col_hint) list option;
        mutable ycolumn : string option;
        mutable loss_type : loss_type option;
        mutable trainset : Zset.zset option;  (* None means "all" *)
        mutable folds : int;
        mutable learning_rate : float;
        mutable tree_depth : int;
      }

    let pool = Zset.default_pool   (* CHECK! *)

    let create_spec() =
      { xcolumns = [];
        xcolumns_unbundled = None;
        ycolumn = None;
        loss_type = None;
        trainset = None;
        folds = 2;
        learning_rate = 0.5;
        tree_depth = 4;
      }

    let spec = create_spec()
    let oniteration_cb =
      ref (fun _ _ -> () : (module BOOST_UI_P0) -> int -> unit)
    let wrap_base_stepper_module =
      ref (module Identity_base_stepper : Types.TREE_BASELEARNER_STEP_WRAPPER)
    let wrap_baselearner_module =
      ref (module Identity_baselearner : Types.TREE_BASELEARNER_WRAPPER)
    let enable_par =
      ref false

    let reset_all () =
      spec.xcolumns <- [];
      spec.xcolumns_unbundled <- None;
      spec.ycolumn <- None;
      spec.loss_type <- None;
      spec.trainset <- None;
      spec.folds <- 2;
      spec.learning_rate <- 0.5;
      spec.tree_depth <- 4

    let string_of_loss =
      function
      | `Squared_error -> "`Squared_error"
      | `Abs_dev -> "`Abs_dev"
      | `Huber -> "`Huber"
      | `Bilog -> "`Bilog"                          

    let loss_of_string =
      function
      | "`Squared_error" -> `Squared_error
      | "`Abs_dev" -> `Abs_dev
      | "`Huber" -> `Huber
      | "`Bilog" -> `Bilog
      | _ -> failwith "loss_of_string"
     
                    
    let dataset_ztable() =
      get_ztable "dataset"

    let get_trainset0() =
      let ztab = dataset_ztable() in
      match spec.trainset with
        | None -> Zset.all pool (Ztable.space ztab)
        | Some zset -> zset
                    
    let nullable opt f =
      match opt with
        | None -> `Null
        | Some x -> f x

(*
    let string_of_mono =
      function
      | None -> "None"
      | Some `Increasing -> "Increasing"
      | Some `Decreasing -> "Decreasing"

    let mono_of_string =
      function
      | "None" -> None
      | "Increasing" -> Some `Increasing
      | "Decreasing" -> Some `Decreasing
      | _ -> failwith "mono_of_string"
 *)

    let save_spec_u upd =
      let sl = current_slot() in
      let spec_j =
        `Assoc [ "loss_type",
                 nullable
                   spec.loss_type
                   (fun ltype -> `String (string_of_loss ltype));

                 "xcolumns",
                 `List (List.map
                          (fun (name,hint) ->
                           `List [ `String name;
                                   `Assoc [ "ordinal", `Bool hint.col_ordinal;
                                            (* "monotonic", `String (string_of_mono hint.col_monotonic) *)
                                          ]
                                 ]
                          )
                          spec.xcolumns);

                 "ycolumn",
                 nullable
                   spec.ycolumn
                   (fun name -> `String name);
                 
                 "folds", `Int spec.folds;
                 "learning_rate", `Float spec.learning_rate;
                 "tree_depth", `Int spec.tree_depth;
               ] in
      let p = Stash.slot_param sl in
      let p = if p=`Null then `Assoc [] else p in
      let p =
        Zsession.Util.json_update
          p (`Assoc ["Zmola.Session.spec", spec_j;
                     "Zmola.Session.running", `Bool false
                    ]) in
      Stash.set_param upd p;
      let trainset = get_trainset0() in
      let filename_trainset =
        Stash.add_file upd "Zmola.Session.spec.trainset" "zset" in
      Zsession.Util.write_zset filename_trainset trainset

    let save_spec() =
      let sl = current_slot() in
      let upd = Stash.update sl in
      save_spec_u upd;
      Stash.commit upd
                               
    let running = ref false

    let save_running() =
      let sl = current_slot() in
      let upd = Stash.update sl in
      let rj = `Bool !running in
      let p = Stash.slot_param sl in
      let p = if p=`Null then `Assoc [] else p in
      let p =
        Zsession.Util.json_update p (`Assoc ["Zmola.Session.running", rj]) in
      Stash.set_param upd p;
      Stash.commit upd

                      
    let check() =
      if !running then (
        if Config.is_interactive then
          eprintf "ERROR: The learner is running. Call reset() before \
                   changing parameters\n%!";
        raise Zsession.Stypes.Bad_request
      )

    let dataset_input() =
      get_input "dataset"

    (* FIXME: also ensure that [check] is called when the underlying
       function is directly called *)
                 
    let load_dataset ~path =
      check();
      load_ztable ~name:"dataset" ~path;
      reset_all();
      save_spec()

    let set_dataset ztab =
      check();
      set_ztable ~name:"dataset" ztab;
      reset_all();
      save_spec()

    let select_dataset() =
      check();
      select_ztable ~name:"dataset";
      reset_all();
      save_spec()

    let have_dataset() =
      try
        ignore(Input.get (current_slot()) "dataset"); true
      with Not_found -> false
               
    let check_dataset() =
      if not (have_dataset()) then (
        if Config.is_interactive then
          eprintf "ERROR: No dataset selected yet. \
                   Do e.g. with select_dataset()\n%!";
        raise Zsession.Stypes.Incomplete_request
      )
                                     
    let check_loss() =
      if spec.loss_type = None then (
        if Config.is_interactive then
          eprintf "ERROR: No loss function selected yet. \
                   Do e.g. with select_loss()\n%!";
        raise Zsession.Stypes.Incomplete_request
      )

    let check_columns() =
      if spec.xcolumns = [] then (
        if Config.is_interactive then
          eprintf "ERROR: You need to select the X columns first. \
                   Do e.g. with select_xcolumns()\n%!";
        raise Zsession.Stypes.Incomplete_request        
      );
      if spec.ycolumn = None then (
        if Config.is_interactive then
          eprintf "ERROR: You need to select the Y column first. \
                   Do e.g. with select_ycolumn()\n%!";
        raise Zsession.Stypes.Incomplete_request        
      )
      
    let get_xcolumns() =
      List.map fst spec.xcolumns

    let get_xcolumns_unbundled_1() =
      match spec.xcolumns_unbundled with
        | Some list ->
            list
        | None ->
            let ztab = dataset_ztable() in
            let list =
              List.flatten
                (List.map
                   (fun (name,hint) ->
                     let Ztypes.ZcolBox(_,ty,_,_) =
                       Ztable.get_zcolbox ztab name in
                     match ty with
                       | Ztypes.Zbundle ty1 ->
                           let col = Ztable.get_zcol ztab ty name in
                           let indices = Zcol.bundle_indices col in
                           List.map
                             (fun i ->
                               (sprintf "%s[%d]" name i, hint)
                             )
                             indices
                       | _ ->
                           [name,hint]
                   )
                   spec.xcolumns
                ) in
            spec.xcolumns_unbundled <- Some list;
            list
              
    let get_ycolumn() =
      spec.ycolumn
              
    module type SESSION_DATASET =
      sig
        include Types.DATASET
        val dataset : unit -> dataset
      end
        
    module DS0 =
      struct
        type col_hint = spec_col_hint =
          { col_ordinal : bool;
            (* col_monotonic : [`Increasing|`Decreasing] option; *)
          }
        type dataset =
          Ztable.ztable * (string * spec_col_hint) list * string option

        let ztable (ztab,_,_) = ztab
        let x_columns (_,x,_) = x
        let y_column (_,_,yopt) =
          match yopt with
            | None -> assert false
            | Some y -> y

        let get_xcolumns_unbundled() =
          List.map
            (fun (name,hint) ->
              let col_ordinal = hint_tuple hint in
              (name, { col_ordinal })
            )
            (get_xcolumns_unbundled_1())

        let dataset() : dataset =
          check_dataset();
          check_columns();
          (dataset_ztable(), get_xcolumns_unbundled(), get_ycolumn())
      end

    module DS1 : SESSION_DATASET
      = DS0
        
    module DS : Types.DATASET with type dataset = DS1.dataset
      = DS1
        
    let dataset() = DS1.dataset()
    let get_xcolumns_unbundled() = DS.x_columns (dataset())
        
    module type RUNTIME =
      sig
        module Loss : Types.LOSS with module DS = DS
        module Booster : Types.TREE_GRADIENT_BOOSTER with module DS = DS
        module Loop : Booster_loop.LOOP_TYPE with module DS = DS
                                              and module L = Loss
                                              and module EL = Booster

        val fold_loop : int -> Loop.state
        val fold_stage : int -> Booster.stage
        val run : iterations:int -> ?only_fold:int -> unit -> unit
        val describe : unit -> unit
        val save_runtime : unit -> unit
        val ensemble_model : unit -> Booster.model list
      end

    module type LOSS =
      Types.LOSS with module DS = DS

    let get_loss() =
      match spec.loss_type with
        | None -> raise Not_found
        | Some ltype -> ltype

    let loss_module() =
      check_dataset();
      check_loss();
      let ltype = get_loss() in
      match ltype with
        | `Squared_error ->
            let module L = Loss.Squared_error(DS) in
            (module L : LOSS)
        | `Abs_dev ->
            let module L = Loss.Absolute_deviation(DS) in
            (module L : LOSS)
        | `Huber ->
            (* FIXME: Huber needs a parameter *)
            let module BC = struct let alpha = 0.2 end in
            let module L = Loss.Huber(BC)(DS) in
            (module L : LOSS)
        | `Bilog ->
            (* FIXME: influence trimming is so far disabled *)
            let module BC = struct let alpha = 0.0 end in
            let module L = Loss.Bilog(BC)(DS) in
            (module L : LOSS)

                                      
    let is_available_x_column ztab name =
      let Ztypes.ZcolBox(name,ty,_,_) = Ztable.get_zcolbox ztab name in
      let col = Ztable.get_zcol ztab ty name in
      match Zcol.content col with
        | Zcol.Inv _ -> true
        | _ -> false

    let is_available_y_column ltype ztab name =
      let Ztypes.ZcolBox(_,ty,_,_) = Ztable.get_zcolbox ztab name in
      let module Loss = (val loss_module()) in
      Loss.input_ok ty

    let set_loss ltype =
      check();
      check_dataset();
      let ztab = dataset_ztable() in
      ( match spec.ycolumn with
          | None -> ()
          | Some y ->
              if not(is_available_y_column ltype ztab y) then (
                if Config.is_interactive then
                  eprintf "WARNING: The Y column is incompatible with this \
                           loss function.\n\
                           The Y column has been reset because of this!\n%!";
                spec.ycolumn <- None
              )
      );
      spec.loss_type <- Some ltype;
      save_spec();
      if Config.is_interactive then
        eprintf "Selected: %s\n%!" (string_of_loss ltype)

    let reset_loss () =
      check();
      spec.loss_type <- None;
      save_spec()
           
    let select_loss() =
      check();
      let text =
        Util.call_command
          "zenity --list --title='Select loss type' --text='Select loss type' \
           --column='Code' --column='Description' \
           '`Squared_error' 'minimize the sum of squared errors' \
           '`Abs_dev'       'minimize the sum of absolute deviation' \
           '`Huber'         'Huber function (`Squared_error for small, `Abs_dev for big deviations)' \
           '`Bilog'         'Binomial logistic loss (minimizing logits)'" in
      match Util.get_line text with
        | "`Squared_error" -> set_loss `Squared_error
        | "`Abs_dev" -> set_loss `Abs_dev
        | "`Huber" -> set_loss `Huber
        | "`Bilog" -> set_loss `Bilog
        | _ -> assert false

    let columns() =
      check_dataset();
      let ztab = dataset_ztable() in
      let cols = Ztable.columns ztab in
      let cols = List.sort String.compare cols in
      eprintf "Available X columns:\n%!";
      let unavail = ref 0 in
      List.iter
        (fun col ->
           if is_available_x_column ztab col then
             let Ztypes.ZcolBox(name,ty,_,_) = Ztable.get_zcolbox ztab col in
             let sel =
               try
                 let hint = List.assoc col spec.xcolumns in
                 if hint.col_ordinal then "ORD" else "CAT"
               with Not_found -> "" in
             eprintf "  %3s %-20s : %s\n" sel name (Ztypes.string_of_ztype ty)
           else
             incr unavail
        )
        cols;
      if !unavail > 0 then
        eprintf "Additionally, %d columns do not match the requirements as \
                 X column\n" !unavail;
      eprintf "Available Y columns:\n%!";
      ( match spec.loss_type with
          | None ->
              eprintf "  (select loss type first to get a list)\n"
          | Some ltype ->
              unavail := 0;
              List.iter
                (fun col ->
                   if is_available_y_column ltype ztab col then
                     let Ztypes.ZcolBox(name,ty,_,_) =
                       Ztable.get_zcolbox ztab col in
                     let sel =
                       if Some col = spec.ycolumn then "SEL" else "" in
                     eprintf "  %3s %-20s : %s\n" sel name
                             (Ztypes.string_of_ztype ty)
                   else
                     incr unavail
                )
                cols;
              if !unavail > 0 then
                eprintf "Additionally, %d columns do not match the requirements \
                         as Y column\n" !unavail;
      );
      eprintf "%!"

    let add_xcolumns0 ?(ordinal=false) names =
      check();
      check_dataset();
      let ztab = dataset_ztable() in
      let unavailable =
        List.filter (fun name -> not(is_available_x_column ztab name)) names in
      if unavailable <> [] then (
        if Config.is_interactive then
          eprintf "ERROR: These columns are incompatible: %s\n%!"
                  (String.concat ", " unavailable);
        raise Zsession.Stypes.Bad_request
      );
      let hint = { col_ordinal = ordinal; (*col_monotonic = monotonic*) } in
      let xcolumns =
        List.filter (fun (n,_) -> not(List.mem n names)) spec.xcolumns in
      let xcolumns =
        List.map (fun n -> (n, hint)) names @ xcolumns in
      spec.xcolumns <- xcolumns;
      spec.xcolumns_unbundled <- None;
      save_spec()
               
    let add_xcolumns ?ordinal names =
      add_xcolumns0 ?ordinal names;
      save_spec()
      
    let split_re = Netstring_str.regexp "[|]"
    let categorical_re = Netstring_str.regexp "\\(.*\\) (categorical)$"
    let ordinal_re = Netstring_str.regexp "\\(.*\\) (ordinal)$"
                         
    let select_xcolumns() =
      check();
      check_dataset();
      let ztab = dataset_ztable() in
      let cols = Ztable.columns ztab in
      let cols = List.sort String.compare cols in
      let cols = List.filter (is_available_x_column ztab) cols in
      let to_select =
        List.flatten
          (List.map
             (fun col ->
                let Ztypes.ZcolBox(name,ty,_,_) = Ztable.get_zcolbox ztab col in
                [ (try (List.assoc col spec.xcolumns).col_ordinal=false
                   with Not_found -> false),
                  col ^ " (categorical)",
                  Ztypes.string_of_ztype ty;

                  (try (List.assoc col spec.xcolumns).col_ordinal=true
                   with Not_found -> false),
                    col ^ " (ordinal)",
                  Ztypes.string_of_ztype ty;
                ]
             )
             cols
          ) in
      let cmd1 =
        "zenity --list --checklist --title='Select X columns' --text='Select X columns' --column='Select' --column='Column' --column='Type' --multiple " in
      let cmd2 =
        String.concat
          " "
          (List.map
             (fun (sel,name,descr) ->
                sprintf "%B %s %s"
                        sel (Filename.quote name) (Filename.quote descr)
             )
             to_select
          ) in
      let line = Util.get_line (Util.call_command (cmd1 ^ cmd2)) in
      let selected = Netstring_str.split split_re line in
      List.iter
        (fun ext_name ->
           let ordinal, name =
             match Netstring_str.string_match categorical_re ext_name 0 with
               | Some r ->
                   false, Netstring_str.matched_group r 1 ext_name
               | None ->
                   ( match Netstring_str.string_match ordinal_re ext_name 0 with
                       | Some r ->
                           true, Netstring_str.matched_group r 1 ext_name
                       | None ->
                           assert false
                   ) in
           add_xcolumns0 ~ordinal [name]
        )
        selected;
      save_spec()

    let reset_xcolumns() =
      check();
      spec.xcolumns <- [];
      spec.xcolumns_unbundled <- None;
      save_spec()  

    let set_ycolumn name =
      check();
      check_dataset();
      check_loss();
      let ztab = dataset_ztable() in
      let ltype = get_loss() in
      if not (is_available_y_column ltype ztab name) then (
        eprintf "ERROR: This column is incompatible: %s\n%!" name;
        raise Zsession.Stypes.Bad_request
      );
      spec.ycolumn <- Some name;
      save_spec()

    let reset_ycolumn() =
      check();
      spec.ycolumn <- None;
      save_spec()
          
    let select_ycolumn() =
      check();
      check_dataset();
      check_loss();
      let ztab = dataset_ztable() in
      let ltype = get_loss() in
      let cols = Ztable.columns ztab in
      let cols = List.sort String.compare cols in
      let cols = List.filter (is_available_y_column ltype ztab) cols in
      let to_select =
        List.map
          (fun col ->
             let Ztypes.ZcolBox(name,ty,_,_) = Ztable.get_zcolbox ztab col in
             ( spec.ycolumn = Some col,
               col,
               Ztypes.string_of_ztype ty;
             )
          )
          cols in
      let cmd1 =
        "zenity --list --radiolist --title='Select Y column' --text='Select Y column' --column='Select' --column='Column' --column='Type' " in
      let cmd2 =
        String.concat
          " "
          (List.map
             (fun (sel,name,descr) ->
                sprintf "%B %s %s"
                        sel (Filename.quote name) (Filename.quote descr)
             )
             to_select
          ) in
      let line = Util.get_line (Util.call_command (cmd1 ^ cmd2)) in
      set_ycolumn line

    let get_trainset() =
      check_dataset();
      get_trainset0()
      
    let set_trainset zset =
      check();
      check_dataset();
      let ztab = dataset_ztable() in
      if not (Zset.space_less_or_equal_than zset (Ztable.space ztab)) then (
        if Config.is_interactive then
          eprintf "ERROR: This set refers to rows outside the range of the \
                   dataset\n%!";
        raise Zsession.Stypes.Bad_request
      );
      spec.trainset <- Some zset;
      save_spec()

    let set_train_subrange ~from_id ~to_id =
      check();
      check_dataset();
      let ztab = dataset_ztable() in
      let Ztypes.Space n = Ztable.space ztab in
      if from_id < 1 || from_id > n || to_id < from_id || to_id > n then (
        if Config.is_interactive then
          eprintf "ERROR: This set refers to rows outside the range of the \
                   dataset\n%!";
        raise Zsession.Stypes.Bad_request
      );
      let builder = Zset.build_begin pool 100 (Ztable.space ztab) in
      Zset.build_add_range builder from_id (to_id-from_id+1);
      let zset = Zset.build_end builder in
      set_trainset zset

    let set_folds n =
      check();
      if n < 2 then (
        if Config.is_interactive then
          eprintf "ERROR: Minimum number of folds: 2\n%!";
        raise Zsession.Stypes.Bad_request
      );
      spec.folds <- n;
      save_spec()

    let get_folds() =
      spec.folds

    let set_learning_rate r =
      check();
      if r <= 0.0 || r > 1.0 then (
        if Config.is_interactive then
          eprintf "ERROR: learning rate must be > 0.0 and <= 1.0\n%!";
        raise Zsession.Stypes.Bad_request
      );
      spec.learning_rate <- r;
      save_spec()

    let get_learning_rate() =
      spec.learning_rate

    let set_tree_depth d =
      check();
      if d < 2 then (
        if Config.is_interactive then
          eprintf "ERROR: tree depth must be >= 2%!";
        raise Zsession.Stypes.Bad_request
      );
      spec.tree_depth <- d;
      save_spec()

    let get_tree_depth() =
      spec.tree_depth

    let describe_spec() =
      eprintf "INPUTS:\n";
      inputs();
      eprintf "COLUMNS:\n";
      if have_dataset() then
        columns()
      else
        eprintf "      No columns.\n";
      eprintf "LOSS:\n";
      ( match spec.loss_type with
          | None ->
              eprintf "      No loss.\n"
          | Some ltype ->
              eprintf "      Loss: %s\n" (string_of_loss ltype)
      );
      eprintf "TRAINING:\n";
      ( match spec.trainset with
          | None ->
              eprintf "      Set: ALL\n"
          | Some _ ->
              eprintf "      Set: (subset, here omitted)\n"
      );
      eprintf "      Folds: %d\n" spec.folds;
      eprintf "      Learning rate (initially): %.2f\n" spec.learning_rate;
      eprintf "      Tree depth: %d\n" spec.tree_depth;
      eprintf "%!"

    let load_spec i =
      let sl = Stash.get_slot i in
      reset_all();
      let p = Stash.slot_param sl in
      let spec_j = p |> U.member "Zmola.Session.spec" in
      if spec_j = `Null then (
        if Config.is_interactive then
          eprintf "WARNING: No content for Zmola.Session found!\n%!"
      )
      else (
        let ltype =
          spec_j |> U.member "loss_type" |> U.to_option
             (fun x ->
                x |> U.to_string |> loss_of_string) in
        let folds =
          spec_j |> U.member "folds" |> U.to_int in
        let learning_rate =
          spec_j |> U.member "learning_rate" |> U.to_float in
        let tree_depth =
          spec_j |> U.member "tree_depth" |> U.to_int in
        let xcolumns =
          spec_j |> U.member "xcolumns" |> U.to_list |>
            (fun l ->
               List.map
                 (function
                  | `List [ `String name; `Assoc props ] ->
                      let col_ordinal =
                        match List.assoc "ordinal" props with
                          | `Bool b -> b
                          | _ -> failwith "load_spec: ordinal"
                          | exception Not_found ->
                              failwith "load_spec: ordinal" in
(*
                      let col_monotonic =
                        match List.assoc "monotonic" props with
                          | `String s -> mono_of_string s
                          | _ -> failwith "load_spec: monotonic"
                          | exception Not_found -> None in
 *)
                      (name, { col_ordinal; (*col_monotonic*) })
                   | _ -> failwith "load_spec"
                 )
                 l
            ) in
        let ycolumn =
          spec_j |> U.member "ycolumn" |> U.to_option
                                            (fun x -> x |> U.to_string) in
        let files = Stash.slot_files sl in
        let trainset =
          match List.assoc "Zmola.Session.spec.trainset" files with
            | (filename,_) -> Zsession.Util.read_zset pool filename
            | exception Not_found ->
               failwith "Zmola.Session.load_spec" in
        let trainset =
          if Zset.is_all trainset then
            None
          else
            Some trainset in

        spec.xcolumns <- xcolumns;
        spec.xcolumns_unbundled <- None;
        spec.ycolumn <- ycolumn;
        spec.loss_type <- ltype;
        spec.folds <- folds;
        spec.learning_rate <- learning_rate;
        spec.tree_depth <- tree_depth;
        spec.trainset <- trainset;

        (* TODO: check that this is meaningful! *)
        
        if Config.is_interactive then
          describe_spec()
      )

    let wrap_base_stepper m =
      wrap_base_stepper_module := m

    let wrap_baselearner m =
      wrap_baselearner_module := m

    let get_parallel() =
      !enable_par

    let set_parallel b =
      enable_par := b

    module type RT_CONSTRUCTION =
      sig
        val mode : [`Create|`Load of int]
      end
              
    let current_runtime = ref (None : (module RUNTIME) option)

    module type MAKE_TREE_BASELEARNER_STEP_1 =
      functor(DS:Types.DATASET) ->
      Types.TREE_BASELEARNER_STEP with module DS = DS

    module Create_runtime(L:LOSS)(RTC:RT_CONSTRUCTION)() =
      struct
        module Loss = L
        module WBS = (val !wrap_base_stepper_module)
        module WBL = (val !wrap_baselearner_module)
        module BL_mk_step1 = Blearner_sqe_bisection.Step
        module BL_mk_step2 = Blearner_par.Base_step

        let bl_step =
          if !enable_par then
            (module BL_mk_step2 : MAKE_TREE_BASELEARNER_STEP_1)
          else
            (module BL_mk_step1)

        module BL_mk_step = (val bl_step)
        module BL_step = BL_mk_step(DS)
        module BL_step' = WBS(BL_step)
        module BL_config = struct let max_depth = get_tree_depth() end
        module BL = Blearner.Tree_learner(BL_config)(BL_step')
        module BL' = WBL(BL)
        module Booster = Booster.Tree_Gradient_Booster(Loss)(BL')
        module Loop = Booster_loop.Loop(Booster)
        module F = Folder.Data_folds(DS)

        let progress loopstate d n =
          eprintf "fold %d iter %d loss_fold=%e loss_cross=%e rate=%.2f\n%!"
                  d
                  (Loop.stage_count loopstate)
                  (Loop.loss_trainset loopstate /. float n)
                  (Loop.loss_valset loopstate /. float n)
                  (Loop.learning_rate loopstate)
                     
        let fold_states =
          match RTC.mode with
            | `Create ->
                let folds =
                  F.create
                    ~is_interactive:Config.is_interactive
                    ~base:(get_trainset())
                    pool
                    spec.folds
                    (dataset()) in
                Array.init
                  spec.folds
                  (fun fld ->
                     let foldset = F.get_fold folds fld in
                     let crossset = F.get_fold_negation folds fld in
                     Loop.create
                       (dataset()) pool foldset crossset spec.learning_rate
                  )
            | `Load i ->
                let n = Zset.count (get_trainset()) in
                Array.init
                  spec.folds
                  (fun fld ->
                     let loopstate =
                       Loop.from_stash (module Stash) i (dataset()) pool
                                       (sprintf "[%d]" fld) in
                     if Config.is_interactive then
                       progress loopstate fld n;
                     loopstate
                  )
            
        let fold_loop fld =
          if fld < 0 || fld >= Array.length fold_states then
            raise Zsession.Stypes.Bad_request;
          fold_states.(fld)
                     
        let fold_stage fld =
          let state = fold_loop fld in
          Loop.stage state

        let save_runtime () =
          let sl = current_slot() in
          Array.iteri
            (fun k st ->
               Loop.to_stash (module Stash) (Stash.slot_index sl) st
                             (sprintf "[%d]" k)
            )
            fold_states

        let run ~iterations ?only_fold () =
          let boostmod =
            match !self with Some m -> m | None -> assert false in
          let n = Zset.count (get_trainset()) in
          for d = 0 to spec.folds - 1 do
            let do_it =
              match only_fold with
                | None -> true
                | Some only_d -> d = only_d in
            if do_it then (
              let loopstate = fold_states.(d) in
              !oniteration_cb boostmod d;
              if Config.is_interactive then
                progress loopstate d n;
              while Loop.stage_count loopstate < iterations do
                Loop.next loopstate;
                save_runtime();
                !oniteration_cb boostmod d;
                if Config.is_interactive then
                  progress loopstate d n;
              done;
              if Config.is_interactive then
                eprintf "fold %d: reached target number of iterations\n%!" d
            )
          done

        let describe() =
          eprintf "RUNTIME:\n";
          let n = Zset.count (get_trainset()) in
          for d = 0 to spec.folds - 1 do
            let loopstate = fold_states.(d) in
            progress loopstate d n;
          done;
          eprintf "%!"

        let ensemble_model() =
          let n =
            float(Array.length fold_states) in
          let models1 =
            Array.map Loop.ensemble_model fold_states in
          let models2 =
            Array.map (List.map (Tree.multiply (1.0 /. n))) models1 in
          List.flatten (Array.to_list models2)
      end
      
    let runtime() =
      match !current_runtime with
        | None ->
            assert(not !running);
            check_dataset();
            check_loss();
            check_columns();
            let module RTC = struct let mode = `Create end in
            let module RT = Create_runtime((val loss_module() : LOSS))(RTC)() in
            let rt = (module RT : RUNTIME) in
            current_runtime := Some rt;
            running := true;
            save_running();
            rt
        | Some rt ->
            assert(!running);
            rt

    let load_runtime i =
      let sl = Stash.get_slot i in
      let p = Stash.slot_param sl in
      let r = p |> U.member "Zmola.Session.running" |> U.to_bool_option in
      if r = Some true then (
        check_dataset();
        check_loss();
        check_columns();
        let module RTC = struct let mode = `Load i end in
        let module RT = Create_runtime((val loss_module() : LOSS))(RTC)() in
        let rt = (module RT : RUNTIME) in
        current_runtime := Some rt;
        running := true;
      ) else (
        current_runtime := None;
        running := false
      )
      
    let reset() =
      ( match !current_runtime with
          | None -> ()
          | Some rt ->
              let module RT = (val rt) in
              let sl = current_slot() in
              let upd = Stash.update sl in
              for d = 0 to spec.folds - 1 do
                let files = RT.Loop.stash_files (sprintf "[%d]" d) in
                List.iter (Stash.delete_file upd) files
              done;
              Stash.commit upd
      );
      current_runtime := None;
      running := false;
      save_running()

    let run ~iterations ?only_fold () =
      let rt = runtime() in
      let module RT = (val rt) in
      RT.run ~iterations ?only_fold ();
      rt

    let describe() =
      describe_spec();
      match !current_runtime with
        | None ->
            eprintf "RUNTIME:\n";
            eprintf "      not running\n%!"
        | Some rt ->
            let module RT = (val rt) in
            RT.describe()
            
        
    let () =
      load_callback := (fun i -> load_spec i; load_runtime i)
              
    let oniteration f =
      oniteration_cb := f
  end


module Make_UI(SC : Zsession.Stypes.SESSION_CONFIG) : BOOST_UI =
  struct
    module P0 = Make_UI_P0(SC)
    let () =
      P0.self := Some(module P0)
    include P0
  end
    
    
module UI = Make_UI(Zsession.Config.Default)
                   
(*
#use "topfind";;
#require "zmola";;
open Zmola.Session.UI;;
 *)
