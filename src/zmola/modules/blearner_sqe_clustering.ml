(* Base learners splitting a set into several smaller sets corresponding
   to clusters
 *)

(* Clustering works generally as follows:

   Initially, all our elements are put into a single cluster. In the
   course of the algorithm, we split clusters up and merge clusters.
   Actually, a cluster is either intended for splitting - in that case
   we call it subtractive - or it is intended for merging - in that case
   we call it additive. The initial single cluster is of course subtractive.

   Roughly speaking, the subtractive clusters contain all the elements
   that are far away from each other or where we don't know much, and
   the additive clusters contain the elements that are known to be
   close to each other. We try to move elements from subtractive
   clusters to additive ones after figuring out that they are nearby.

   Now look at the distances of adjacent elements.  We call the
   relation of an element and its right neigbor an "edge".  The edges
   can be labelled with the distance between the elements.

   We iterate now over the edges in the order of increasing distance labels.
   Generally, the idea is that elements that are close to each other should
   go into the same cluster. In every iteration we take the edge, and check
   to which clusters the two elements of the edge already belong, and
   modify the clusters:

    - If both elements are members of subtractive clusters, we start a
      new additive cluster that initially only has the two elements as members.
      The original subtractive cluster is split up
      into a left and a right part containing the elements to the left
      and to the right of the edge. (These parts can be empty.)
    - If both elements are members of additive clusters, we merge
      the clusters.
    - If one element is member of an additive cluster and the other a
      member of a subtractive cluster, the element is moved from the
      latter one to the former one.

   Once we have done this for all edges, we finally end up with a single cluster
   again, which is this time an additive one. Between the starting point and
   the final point there may by up to n/2 clusters (in the worst case).
   We measure the loss for every cluster constellation and take the one
   with the minimum loss.
 *)

open Types
open Zcontainer
open Printf

module Int = Squared_error.Int

module ClusterID =
  struct
    type t = CID of < >
    let create() =
      CID(object end)
    let compare (CID c1) (CID c2) =
      Pervasives.compare c1 c2
  end

module ClusterMap = Map.Make(ClusterID)

module IntSet = Squared_error.StdSet(Int)
module IntMap = Squared_error.StdMap(Int)
module SQE = Squared_error.Make_Squared_Error(Int)(IntSet)(IntMap)
module ISet = SQE.IndexSet

type kind =
  | Additive
  | Subtractive
(* Clusters are additive or subtractive - either members are added,
   or clusters are split up
 *)

type clustering =
  { membership : ClusterID.t array;          (* the CID of the index *)
    mutable clusters : (ISet.t * kind) ClusterMap.t
  }
  (* PERF comment: storing the membership information into an array is
     not good. It is just done for simplicity.

     An alternative when we limit the number of clusters somehow is
     a Map using the left points of the cluster ranges as key. Use
     Map.split for searching.
   *)

let rec iset_fromto p q =
  if p > q then
    ISet.empty
  else
    ISet.add p (iset_fromto (p+1) q)


(* Initially, the clustering consist only of a single subtractive cluster
   and all sets are member of it.
 *)

let initial_clustering n =
  let rest = ClusterID.create() in
  let membership = Array.make n rest in
  let restset = iset_fromto 0 (n-1) in
  let clusters =
    ClusterMap.add rest (restset, Subtractive) ClusterMap.empty in
  { membership; clusters }

let copy_clustering cls =
  (* If membership were also a functional data structure, we wouldn't need
     a copy
   *)
  { membership = Array.copy cls.membership;
    clusters = cls.clusters
  }

let update_membership cls cid iset =
  ISet.iter
    (fun i ->
       cls.membership.(i) <- cid
    )
    iset

(* Refine by splitting up subtractive clusters and adding to additive clusters
 *)

let refine_clustering cls k =
  (* Consider the edge from k to ksucc=k+1. *)
  let ksucc = k+1 in
  let cid_k = cls.membership.(k) in
  let cid_ksucc = cls.membership.(ksucc) in
  let iset_k, kind_k =
    try ClusterMap.find cid_k cls.clusters
    with Not_found -> assert false in
  let iset_ksucc, kind_ksucc =
    try ClusterMap.find cid_ksucc cls.clusters
    with Not_found -> assert false in
  (* It depends on the kinds of the clusters what to do: *)
  match kind_k, kind_ksucc with
    | Subtractive, Subtractive ->
        (* Create a new additive cluster with elements k and ksucc *)
        (* printf "S/S\n"; *)
        let cid_new = ClusterID.create() in
        let iset_new = ISet.of_list [k; ksucc] in
        let kind_new = Additive in
        cls.clusters <-
          ClusterMap.add cid_new (iset_new,kind_new) cls.clusters;
        update_membership cls cid_new iset_new;
        (* remove k and ksucc from their old cluster, which must be the
           same, and split this cluster up
         *)
        assert(cid_k = cid_ksucc);
        let iset_k_new = ISet.filter (fun i -> i < k) iset_k in
        let iset_ksucc_new = ISet.filter (fun i -> i > ksucc) iset_k in
        let cid_ksucc_new = ClusterID.create() in
        cls.clusters <-
          ClusterMap.add cid_k (iset_k_new,Subtractive) cls.clusters;
        cls.clusters <-
          ClusterMap.add cid_ksucc_new (iset_ksucc_new,Subtractive) cls.clusters;
        update_membership cls cid_k iset_k_new;
        update_membership cls cid_ksucc_new iset_ksucc_new;
    | Additive, Additive ->
        (* Merge the two clusters *)
        (* printf "A/A\n"; *)
        let iset_k_new = ISet.union iset_k iset_ksucc in
        cls.clusters <-
          ClusterMap.add cid_ksucc (ISet.empty,Subtractive) cls.clusters;
        cls.clusters <-
          ClusterMap.add cid_k (iset_k_new,Additive) cls.clusters;
        update_membership cls cid_k iset_k_new
    | Additive, Subtractive ->
        (* Add ksucc to the left cluster, and remove it from its cluster *)
        (* printf "A/S\n"; *)
        let iset_k_new = ISet.add ksucc iset_k in
        let iset_ksucc_new = ISet.remove ksucc iset_ksucc in
        cls.clusters <-
          ClusterMap.add cid_k (iset_k_new,Additive) cls.clusters;
        cls.clusters <-
          ClusterMap.add cid_ksucc (iset_ksucc_new,Subtractive) cls.clusters;
        cls.membership.(ksucc) <- cid_k
    | Subtractive, Additive ->
        (* Add k to the right cluster, and remove it from its cluster *)
        (* printf "S/A\n"; *)
        let iset_k_new = ISet.remove k iset_k in
        let iset_ksucc_new = ISet.add k iset_ksucc in
        cls.clusters <-
          ClusterMap.add cid_k (iset_k_new,Subtractive) cls.clusters;
        cls.clusters <-
          ClusterMap.add cid_ksucc (iset_ksucc_new,Additive) cls.clusters;
        cls.membership.(k) <- cid_ksucc


let loss_of_clustering sqe cls =
  ClusterMap.fold
    (fun _ (iset,_) acc ->
       if ISet.is_empty iset then
         acc
       else
         SQE.squared_error sqe iset +. acc
    )
    cls.clusters
    0.0


let print_clustering cls =
  ClusterMap.iter
    (fun (ClusterID.CID id) (set,kind) ->
       printf "CID(%d): set={%s} kind=%s\n"
              (Oo.id id)
              (String.concat "," (List.map string_of_int (ISet.elements set)))
              ( match kind with
                  | Additive -> "A"
                  | Subtractive -> "S"
              )
    )
    cls.clusters


let best_clustering occurrences sqe =
  let len = Array.length occurrences in
  let avgdiffs =
    Array.map
      (fun (k,_) ->
         if k < len - 1 then
           let avg1 = SQE.avg sqe (ISet.singleton k) in
           let avg2 = SQE.avg sqe (ISet.singleton (k+1)) in
           (k, abs_float(avg1 -. avg2))
         else
           (k, infinity)
      )
      occurrences in
  Array.sort (fun (i1,d1) (i2,d2) -> compare d1 d2) avgdiffs;
(*
      Array.iter
        (fun (i,d) -> printf "@ %d: diff=%.1f\n" i d)
        avgdiffs;
 *)
  let edges = Array.map fst avgdiffs in
  let cls = ref (initial_clustering len) in
  let err_start = loss_of_clustering sqe !cls in
  let cls_min = ref !cls in
  let err_min = ref err_start in
  let edge_min = ref (-1) in
  (* FIXME: probably we don't want to always check all possibilities.
     A criterion for stopping earlier could be the number of clusters.
   *)
  for edge_idx = 0 to Array.length edges - 1 do
    let edge = edges.(edge_idx) in
    if edge < len-1 then (
      (* printf "* edge=%d\n" edge; *)
      let cls_candidate = copy_clustering !cls in
      refine_clustering cls_candidate edge;
      (* print_clustering cls_candidate; *)
      let err_candidate = loss_of_clustering sqe cls_candidate in
      if err_candidate < !err_min then (
        cls_min := cls_candidate;
        err_min := err_candidate;
        edge_min := edge;
      );
      cls := cls_candidate;
    )
  done;
  (* printf "MINIMUM after adding edge %d\n" !edge_min; *)
  (err_start, !err_min, !cls_min)


module Ordinal_fstep(DS:DATASET)
       : TREE_BASELEARNER_FEATURE_STEP with module DS=DS =
struct
  module DS = DS

  type base_step =
    { pool : Zset.pool;
      numbers : vector;
      domain : Zset.zset;
      ord : int;
      depth : int;
      expr : expr;
      feature : (string * DS.col_hint) option;
      base_loss : float Lazy.t;
    }

  let start pool numbers domain =
    (* initially we do not split the set up, hence we only have one
       partition with everything in it. The base_loss is computed for that.
     *)
    let depth = 0 in
    let expr = E_And [] in
    let base_loss =
      lazy (
          (* FIXME: this is slow. Doesn't matter for the moment as it is not
             called
           *)
          let sqe = SQE.create numbers [ 0, domain ] in
          SQE.squared_error sqe (SQE.all sqe) (* WATCH PERF! *)
        ) in
    { pool; numbers; domain; depth; expr; feature=None; base_loss; ord=0 }

  let depth step = step.depth
  let domain step = step.domain
  let expr step = step.expr
  let base_loss step = Lazy.force step.base_loss

  let feature step =
    match step.feature with
      | None -> failwith "Zmola.Blearner_sqe_clustering.feature"
      | Some nh -> nh

  exception No_refinement

  let feature_refine ds colname step =
    try
      let ztab = DS.ztable ds in
      let hints =
        try List.assoc colname (DS.x_columns ds)
        with Not_found ->
          failwith "Zmola.Blearner_sqe_clustering.feature_refine: \
                    no such column" in
      let Ztypes.ZcolBox(_,ty,_,_) = Ztable.get_zcolbox ztab colname in
      let zcol = Ztable.get_zcol ztab ty colname in
      let icol = Zcol.invert zcol step.domain in
      let itr = Zcol.iter_values icol in
      let len = itr#length in
      if len < 2 then raise No_refinement;
      let occurrences = Blearner_sqe_bisection.get_occurrences step.pool itr in
      let occurrences_l = Array.to_list occurrences in
      let sqe = SQE.create step.numbers occurrences_l in (* WATCH PERF! *)
      let (err_start, err_min, cls_min) =
        best_clustering occurrences sqe in
      if err_min = err_start then raise No_refinement;
      let get k =
        ( itr # go k;
          snd(itr # current_value)
        ) in
      let substeps =
        ClusterMap.fold
          (fun _ (iset,_) acc ->
             if ISet.is_empty iset then
               acc
             else
               let i_first = ISet.min_elt iset in
               let i_last = ISet.max_elt iset in
               let expr =
                 E_And
                   [ E_ColCmp(Zcol.descr zcol, Ztypes.GE, get i_first);
                     E_ColCmp(Zcol.descr zcol, Ztypes.LE, get i_last);
                   ] in
               let domain = SQE.get_zset sqe iset step.pool in
               let ord = Zset.first domain in
               { pool = step.pool;
                 numbers = step.numbers;
                 domain;
                 ord;
                 depth = step.depth + 1;
                 expr;
                 feature = Some(colname, hints);
                 base_loss = lazy(SQE.squared_error sqe iset)
               } :: acc
          )
          cls_min.clusters
          [] in
      let substeps =
        List.sort
          (fun s1 s2 -> compare s1.ord s2.ord)
          substeps in
      Some(err_min, substeps)
    with
      | No_refinement ->
          None

  let finalize bs = ()

end


module Categorical_fstep(DS:DATASET)
       : TREE_BASELEARNER_FEATURE_STEP with module DS=DS =
struct
  module DS = DS

  type base_step =
    { pool : Zset.pool;
      numbers : vector;
      domain : Zset.zset;
      ord : int;
      depth : int;
      expr : expr;
      feature : (string * DS.col_hint) option;
      base_loss : float Lazy.t;
    }

  let start pool numbers domain =
    (* initially we do not split the set up, hence we only have one
       partition with everything in it. The base_loss is computed for that.
     *)
    let depth = 0 in
    let expr = E_And [] in
    let base_loss =
      lazy (
          (* FIXME: this is slow. Doesn't matter for the moment as it is not
             called
           *)
          let sqe = SQE.create numbers [ 0, domain ] in
          SQE.squared_error sqe (SQE.all sqe) (* WATCH PERF! *)
        ) in
    { pool; numbers; domain; depth; expr; feature=None; base_loss; ord=0 }

  let depth step = step.depth
  let domain step = step.domain
  let expr step = step.expr
  let base_loss step = Lazy.force step.base_loss

  let feature step =
    match step.feature with
      | None -> failwith "Zmola.Blearner_sqe_clustering.feature"
      | Some nh -> nh

  exception No_refinement

  let feature_refine ds colname step =
    try
      let ztab = DS.ztable ds in
      let hints =
        try List.assoc colname (DS.x_columns ds)
        with Not_found ->
          failwith "Zmola.Blearner_sqe_clustering.feature_refine: \
                    no such column" in
      let Ztypes.ZcolBox(_,ty,_,_) = Ztable.get_zcolbox ztab colname in
      let zcol = Ztable.get_zcol ztab ty colname in
      let icol = Zcol.invert zcol step.domain in
      let itr = Zcol.iter_values icol in
      let len = itr#length in
      if len < 2 then raise No_refinement;
      let occurrences = Blearner_sqe_bisection.get_occurrences step.pool itr in
      let occurrences_l = Array.to_list occurrences in
      let sqe0 = SQE.create step.numbers occurrences_l in (* WATCH PERF! *)
      let permutation = Array.map fst occurrences in
      Array.sort
        (fun i1 i2 ->
           compare
             (SQE.avg sqe0 (ISet.singleton i1))
             (SQE.avg sqe0 (ISet.singleton i2))
        )
        permutation;
(*
      Array.iteri
        (fun i j ->
           printf "%d: %d\n" i j
        )
        permutation;
 *)
      let ipermutation =
        Blearner_sqe_bisection.invert_permutation permutation in
      let sqe = SQE.permute sqe0 (fun i -> ipermutation.(i)) in
      let (err_start, err_min, cls_min) =
        best_clustering occurrences sqe in
      if err_min = err_start then raise No_refinement;
      let get k =
        ( itr # go k;
          snd(itr # current_value)
        ) in
      let substeps =
        ClusterMap.fold
          (fun _ (iset,_) acc ->
             if ISet.is_empty iset then
               acc
             else
               let expr =
                 E_Or
                   (List.map
                      (fun i ->
                         let k = permutation.(i) in
                         E_ColCmp(Zcol.descr zcol, Ztypes.EQ, get k)
                      )
                      (ISet.elements iset)
                   ) in
               let domain = SQE.get_zset sqe iset step.pool in
               let ord = Zset.first domain in
               { pool = step.pool;
                 numbers = step.numbers;
                 domain;
                 ord;
                 depth = step.depth + 1;
                 expr;
                 feature = Some(colname, hints);
                 base_loss = lazy(SQE.squared_error sqe iset)
               } :: acc
          )
          cls_min.clusters
          [] in
      let substeps =
        List.sort
          (fun s1 s2 -> compare s1.ord s2.ord)
          substeps in
      Some(err_min, substeps)
    with
      | No_refinement ->
          None

  let finalize bs = ()

end


module FStep(DS:DATASET) =
  Blearner.Ord_or_cat_fstep(Ordinal_fstep(DS))(Categorical_fstep(DS))

module Step(DS:DATASET) =
  Blearner.Step(FStep(DS))

