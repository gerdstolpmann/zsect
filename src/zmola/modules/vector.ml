(* IMPORTANT:

   This bigarray uses FORTRAN conventions, i.e. the first index is 1!
 *)


type t =
  (float, Bigarray.float64_elt, Bigarray.fortran_layout) Bigarray.Array1.t


let length (vec:t) =
  Bigarray.Array1.dim vec

let make n x =
  let vec =
    Bigarray.Array1.create Bigarray.float64 Bigarray.fortran_layout n in
  Bigarray.Array1.fill vec x;
  vec

let init n f =
  let vec =
    Bigarray.Array1.create Bigarray.float64 Bigarray.fortran_layout n in
  for k = 1 to n do
    vec.{k} <- f k
  done;
  vec

let map f (vec:t) =
  let n = length vec in
  init n (fun k -> f vec.{k})

let mapi f (vec:t) =
  let n = length vec in
  init n (fun k -> f k vec.{k})

let of_array a =
  init
    (Array.length a)
    (fun k -> a.(k-1))

let to_array (vec:t) =
  Array.init
    (length vec)
    (fun k -> vec.{k+1})

let persist_tab =
  Hashtbl.create 7

let persist (vec:t) : t * string =
  let fd, name =
    Netsys_posix.shm_create "/zsect" (length vec * 8) in
  let vec' =
    Bigarray.Array1.map_file
      fd Bigarray.float64 Bigarray.fortran_layout true (length vec) in
  Unix.close fd;
  Bigarray.Array1.blit vec vec';
  Hashtbl.add persist_tab name ();
  (vec', name)

let unpersist name =
  Hashtbl.remove persist_tab name;
  try
    Netsys_posix.shm_unlink name
  with _ -> ()

let unpersist_all() =
  let l = Hashtbl.fold (fun n _ acc -> n::acc) persist_tab [] in
  List.iter unpersist l

let access_persistent name =
  let fd =
    Netsys_posix.shm_open
      name [Netsys_posix.SHM_O_RDONLY] 0 in
  let vec =
    Bigarray.Array1.map_file
      fd Bigarray.float64 Bigarray.fortran_layout false (-1) in
  Unix.close fd;
  vec



module Heap =
  struct
    (* an adaption of the heap implemented in zcontainer. This is always a
       min-heap.
     *)

    type vec = t
    type t =
      { mutable hdata : vec;
        mutable hlength : int;
      }

    let create n =
      { hdata = make n 0.0;
        hlength = 0;
      }

    let length heap =
      heap.hlength

    let min heap =
      if heap.hlength = 0 then raise Not_found;
      heap.hdata.{1}

    let snd_min heap =
      (* the second-smallest element *)
      if heap.hlength < 2 then raise Not_found;
      let h1 = heap.hdata.{2} in
      if heap.hlength = 2 then
        h1
      else
        let h2 = heap.hdata.{3} in
        if compare h1 h2 < 0 then
          h1
        else
          h2

    let resize heap =
      let hdata = make (2 * Bigarray.Array1.dim heap.hdata) 0.0 in
      Bigarray.Array1.blit
        (Bigarray.Array1.sub heap.hdata 1 heap.hlength)
        (Bigarray.Array1.sub hdata 1 heap.hlength);
      heap.hdata <- hdata

    let rec bubble_up heap k =
      if k > 1 then (
        let hdata = heap.hdata in
        let p = k/2 in
        let xk = hdata.{k} in
        let xp = hdata.{p} in
        if compare xk xp < 0 then (
          hdata.{k} <- xp;
          hdata.{p} <- xk;
          bubble_up heap p
        )
      )

    let insert heap x =
      if heap.hlength >= Bigarray.Array1.dim heap.hdata then
        resize heap;
      let k = heap.hlength + 1 in
      heap.hdata.{k} <- x;
      heap.hlength <- k;
      bubble_up heap k

  let rec bubble_down heap k =
    let l = 2*k in
    if l <= heap.hlength then (
      let hdata = heap.hdata in
      let xk = hdata.{k} in
      let xl = hdata.{l} in
      if compare xl xk < 0 then (
        bubble_down_1 heap hdata k xk l xl
      ) else
        bubble_down_1 heap hdata k xk k xk
    )

  and bubble_down_1 heap hdata k xk j xj =
    let r = 2*k+1 in
    if r <= heap.hlength then (
      let xk = hdata.{k} in
      let xr = hdata.{r} in
      if compare xr xj < 0 then
        bubble_down_2 heap hdata k xk r xr
      else
        bubble_down_2 heap hdata k xk j xj
    ) else
      bubble_down_2 heap hdata k xk j xj

  and bubble_down_2 heap hdata k xk j xj =
    if j <> k then (
      hdata.{k} <- xj;
      hdata.{j} <- xk;
      bubble_down heap j
    )

  let delete_min heap =
    if heap.hlength < 1 then
      invalid_arg "Zcontainer.Zutil.OrdUtil.heap_delete_min";
    let x = heap.hdata.{heap.hlength} in
    heap.hdata.{1} <- x;
    heap.hlength <- heap.hlength - 1;
    bubble_down heap 1

  let heap_sort heap =  (* distructive *)
    let n = heap.hlength in
    let out = make n 0.0 in
    let p = ref 1 in
    while heap.hlength > 0 do
      let x = min heap in
      delete_min heap;
      out.{ !p } <- x;
      incr p
    done;
    assert( !p = n+1 );
    out
  end
