(* Base learners bisecting a set into two smaller sets, and taking the
   bisection with the least squared error
 *)

open Types
open Zcontainer
open Printf

module SQE = Squared_error.Make_Squared_Error
               (Squared_error.Int)
               (Squared_error.Interval_Set)
               (Squared_error.Interval_Map)
module ISet = SQE.IndexSet


let iter_of_zcol zcol domain =
  match Zcol.content zcol with
    | Zcol.Inv icol ->
        let all_values = Zset.count domain > Zinvcol.length icol in
        Zinvcol.iter ~all_values icol domain
          (* all_values=true skips a preprocessing step that is normally
             done in the iterator, namely the identification of the values
             that are actually selected by the set. This step makes only
             sense when many values can be skipped (in particular for
             high-cardinality columns). In the following we can also process
             values that do not occur (i.e. where the corresponding set is
             empty), so we can enable or disable this step as it is best
             for performance
           *)
    | _ ->
        Zcol.iter_values (Zcol.invert zcol domain)

(* FIXME: in get_occurrences we retrieve the ordered values by running over
   the iterator itr via calling [next]. In Tree.eval_expr we use
   ord_cmp_asc for comparing values. This doesn't go together!
 *)

let get_occurrences pool itr =
  let len = itr#length in
  let occurrences0 = Array.make len (0, Zset.empty) in
  for i = 0 to len-1 do
    let zset = itr#current_zset pool in
    occurrences0.(i) <- (i,zset);
    itr # next()
  done;
  let n = ref 0 in
  Array.iter (fun (_,z) -> if not(Zset.is_empty z) then incr n) occurrences0;
  let occurrences = Array.make !n (0, Zset.empty) in
  let k = ref 0 in
  Array.iter
    (fun (i,z) ->
       if not(Zset.is_empty z) then (
         occurrences.( !k ) <- (i,z);
         incr k
       )
    )
    occurrences0;
  occurrences


let find_split sqe len =
  let stats_L = ref (SQE.stats sqe (ISet.singleton 0)) in
  let stats_R = ref (SQE.stats sqe (ISet.remove 0 (SQE.all sqe))) in
  let err_min = ref (SQE.stats_error !stats_L +. SQE.stats_error !stats_R) in
  let split_min = ref 0 in
  let stats_L_min = ref !stats_L in
  let stats_R_min = ref !stats_R in
  for split = 1 to len-2 do
    let new_st_L = SQE.stats_add sqe !stats_L split in
    let new_st_R = SQE.stats_sub sqe !stats_R split in
    let sum_err = SQE.stats_error new_st_L +. SQE.stats_error new_st_R in
    if sum_err < !err_min then (
      err_min := sum_err;
      split_min := split;
      stats_L_min := new_st_L;
      stats_R_min := new_st_R
    );
    stats_L := new_st_L;
    stats_R := new_st_R
  done;
  (!split_min, !stats_L_min, !stats_R_min, !err_min)


let invert_permutation p =
  let q = Array.map (fun _ -> 0) p in
  Array.iteri
    (fun i j ->
       q.(j) <- i
    )
    p;
  q


module Ordinal_fstep(DS:DATASET)
       : TREE_BASELEARNER_FEATURE_STEP with module DS=DS =
struct
  module DS = DS

  type base_step =
    { pool : Zset.pool;
      numbers : vector;
      domain : Zset.zset;
      depth : int;
      expr : expr;
      feature : (string * DS.col_hint) option;
      base_loss : float Lazy.t;
    }
    (* PERF: think about caching sqe, in case the same column is split
       several times in sequence
     *)

  let start pool numbers domain =
    (* initially we do not split the set up, hence we only have one
       partition with everything in it. The base_loss is computed for that.
     *)
    let depth = 0 in
    let expr = E_And [] in
    let base_loss =
      lazy (
          (* FIXME: this is slow. Doesn't matter for the moment as it is not
             called
           *)
          let sqe = SQE.create numbers [ 0, domain ] in
          SQE.squared_error sqe (SQE.all sqe) (* WATCH PERF! *)
        ) in
    { pool; numbers; domain; depth; expr; base_loss; feature=None }

  let depth step = step.depth
  let domain step = step.domain
  let expr step = step.expr
  let base_loss step = Lazy.force step.base_loss

  let feature step =
    match step.feature with
      | None -> failwith "Zmola.Blearner_sqe_bisection.feature"
      | Some nh -> nh

  exception No_refinement

  (* idea 1:
       - make Zset.union_many faster
         reduce n-way merge to 2-way merge. Avoid heap
     idea 2:
       - domain : Zset.zset list
       - no need to call union_many
       - Zcol.invert_list: takes a zset list
     idea 3:
       - domain : bitset
   *)

  let feature_refine ds colname step =
    try
      let t0 = Unix.gettimeofday() in
      let hints =
        try List.assoc colname (DS.x_columns ds)
        with Not_found ->
          failwith "Zmola.Blearner_sqe_bisection.feature_refine: \
                    no such column" in
      let ztab = DS.ztable ds in
      let Ztypes.ZcolBox(_,ty,_,_) = Ztable.get_zcolbox ztab colname in
      let zcol = Ztable.get_zcol ztab ty colname in
      let itr = iter_of_zcol zcol step.domain in
      let occurrences = get_occurrences step.pool itr in
      let len = Array.length occurrences in
      if len < 2 then raise No_refinement;
      let t1 = Unix.gettimeofday() in
      let occurrences_l = 
        Array.to_list
          (Array.mapi
             (fun i (_,z) -> (i,z))
              occurrences
          ) in
      let sqe = SQE.create step.numbers occurrences_l in (* WATCH PERF! *)
      let t2 = Unix.gettimeofday() in
      let (split_min, st_L_min, st_R_min, err_min) =
        find_split sqe len in
      let t3 = Unix.gettimeofday() in
      (* Selecting here always bitsets for the output domains. Bitsets
         are used anyway in the underlying union_many operation, so we only
         ask here to actually return the bitset. The bitset is good to have
         for any subsequent refinements.
       *)
      let domain_L = SQE.get_zset sqe (SQE.stats_iset st_L_min) Zset.bitset_pool in (* WATCH PERF! *)
      let domain_R = SQE.get_zset sqe (SQE.stats_iset st_R_min) Zset.bitset_pool in (* WATCH PERF! *)
      let t4 = Unix.gettimeofday() in
      ignore(t0 +. t1 +. t2 +. t3 +. t4);   (* avoid warning *)
(*
      Printf.printf "    [bisector t1=%.3f t2=%.3f t3=%.3f t4=%.3f]\n%!"
                    (t1 -. t0) (t2 -. t1) (t3 -. t2) (t4 -. t3);
 *)
      (* if err_min >= step.base_loss then raise No_refinement; *)
      let split_min1, _ = occurrences.(split_min) in
      let split_value =
        ( itr # go (split_min1+1);
          snd(itr # current_value)
        ) in
      let err_L_min = SQE.stats_error st_L_min in
      let err_R_min = SQE.stats_error st_R_min in
      let left =
        { pool = step.pool;
          numbers = step.numbers;
          domain = domain_L;
          depth = step.depth + 1;
          expr = E_ColCmp(Zcol.descr zcol,Ztypes.LT,split_value);
          feature = Some(colname, hints);
          base_loss = lazy err_L_min
        } in
      let right =
        { pool = step.pool;
          numbers = step.numbers;
          domain = domain_R;
          depth = step.depth + 1;
          expr = E_ColCmp(Zcol.descr zcol,Ztypes.GE,split_value);
          feature = Some(colname, hints);
          base_loss = lazy err_R_min
        } in
      Some(err_min, [left; right])
    with
      | No_refinement ->
          None

  let finalize bs = ()
end


module Categorical_fstep(DS:DATASET)
       : TREE_BASELEARNER_FEATURE_STEP with module DS=DS =
struct
  module DS = DS

  type base_step =
    { pool : Zset.pool;
      numbers : vector;
      domain : Zset.zset;
      depth : int;
      expr : expr;
      feature : (string * DS.col_hint) option;
      base_loss : float Lazy.t;
    }
    (* PERF: think about caching sqe, in case the same column is split
       several times in sequence
     *)

  let start pool numbers domain =
    (* initially we do not split the set up, hence we only have one
       partition with everything in it. The base_loss is computed for that.
     *)
    let depth = 0 in
    let expr = E_And [] in
    let base_loss =
      lazy (
          (* FIXME: this is slow. Doesn't matter for the moment as it is not
             called
           *)
          let sqe = SQE.create numbers [ 0, domain ] in
          SQE.squared_error sqe (SQE.all sqe) (* WATCH PERF! *)
        ) in
    { pool; numbers; domain; depth; expr; feature=None; base_loss }

  let depth step = step.depth
  let domain step = step.domain
  let expr step = step.expr
  let base_loss step = Lazy.force step.base_loss

  let feature step =
    match step.feature with
      | None -> failwith "Zmola.Blearner_sqe_bisection.feature"
      | Some nh -> nh

  exception No_refinement

  let rec fromto p q =
    if p > q then
      []
    else
      p :: fromto (p+1) q

  let feature_refine ds colname step =
    try
      let ztab = DS.ztable ds in
      let hints =
        try List.assoc colname (DS.x_columns ds)
        with Not_found ->
          failwith "Zmola.Blearner_sqe_bisection.feature_refine: \
                    no such column" in
      let Ztypes.ZcolBox(_,ty,_,_) = Ztable.get_zcolbox ztab colname in
      let zcol = Ztable.get_zcol ztab ty colname in
      let itr = iter_of_zcol zcol step.domain in
      let occurrences = get_occurrences step.pool itr in
      let len = Array.length occurrences in
      if len < 2 then raise No_refinement;
      let occurrences_a = 
        Array.mapi
          (fun i (_,z) -> (i,z))
          occurrences in
      let occurrences_l = Array.to_list occurrences_a in
      let sqe0 = SQE.create step.numbers occurrences_l in (* WATCH PERF! *)
      (* The difference to Ordinal is now that we do not run in natural
         order over the values, but first sort by avg
       *)
      let permutation_with_avg =
        Array.map
          (fun (i,_) -> i, SQE.avg sqe0 (ISet.singleton i))
          occurrences_a in
      Array.sort
        (fun (i1,avg1) (i2,avg2) ->
           compare avg1 avg2
        )
        permutation_with_avg;
      let permutation = Array.map fst permutation_with_avg in
      let ipermutation = invert_permutation permutation in
      let sqe = SQE.permute sqe0 (fun i -> ipermutation.(i)) in
      let (split_min, st_L_min, st_R_min, err_min) =
        find_split sqe len in
      (* if err_min >= step.base_loss then raise No_refinement; *)
      (* Also, there is no simple split point anymore. We have
         "split sets"
       *)
      let err_L_min = SQE.stats_error st_L_min in
      let err_R_min = SQE.stats_error st_R_min in
      let get k =
        ( itr # go (fst occurrences.(k));
          snd(itr # current_value)
        ) in
      let left_expr =
        E_Or
          (List.map
             (fun p ->
                let i = permutation.(p) in
                E_ColCmp(Zcol.descr zcol, Ztypes.EQ, get i)
             )
             (fromto 0 split_min)
          ) in
      let right_expr =
        E_Or
          (List.map
             (fun p ->
                let i = permutation.(p) in
                E_ColCmp(Zcol.descr zcol, Ztypes.EQ, get i)
             )
             (fromto (split_min+1) (len-1))
          ) in
      let domain_L = SQE.get_zset sqe (SQE.stats_iset st_L_min) Zset.bitset_pool in (* WATCH PERF! *)
      let domain_R = SQE.get_zset sqe (SQE.stats_iset st_R_min) Zset.bitset_pool in (* WATCH PERF! *)
      let left =
        { pool = step.pool;
          numbers = step.numbers;
          domain = domain_L;
          depth = step.depth + 1;
          expr = left_expr;
          feature = Some(colname, hints);
          base_loss = lazy err_L_min
        } in
      let right =
        { pool = step.pool;
          numbers = step.numbers;
          domain = domain_R;
          depth = step.depth + 1;
          expr = right_expr;
          feature = Some(colname, hints);
          base_loss = lazy err_R_min
        } in
      Some(err_min, [left; right])
    with
      | No_refinement ->
          None

  let finalize bs = ()
end


module FStep(DS:DATASET) =
  Blearner.Ord_or_cat_fstep(Ordinal_fstep(DS))(Categorical_fstep(DS))

module Step(DS:DATASET) =
  Blearner.Step(FStep(DS))


(* module Categorical_fstep(DS) : TREE_BASELEARNER_FEATURE_STEP

type base_step =
  { numbers : vector;
    domain : Zset.zset;
    depth : int;
    expr : expr;
    base_loss : float
  }

feature_refine: first get the column, and the list of values with corresponding
zset (by iterating over possible values). This way we get an array of
value_instance =
  { value : 'a;
    zset : Zset.zset (* the rows having this value *)
  }
In the following, the "index" is the index of a value_instance in this array.

Create now SQE = Squared_error.Make_Squared_Error(DS)(Int)
IndexSet=SQE.IndexSet

We now maintain two sets of indices, L and R. Initially
  L = IndexSet.singleton 0
  R = IndexSet.diff all L
 (where all = SQE.all)

Also, eL = SQE.squared_error L
  and eR = SQE.squared_error R
    emin = eL+eR

The loop now adds the index sets for 1, 2, ... in turn to L and removes these
sets from R. While doing that we maintain eL and eR and observe emin. Whenever
we find a new minimum eL+eR we record the index imin where this occurs.

Every loop cylce updates eL by calling SQE.squared_error_add and updates eR
by calling SQE.squared_error_sub.

The loop results in an index imin and the corresponding error emin. Now, the
function returns a list of new base_step = [ bL; bR ].

bL.domain = SQE.get_zset L.(imin)
bL.expr = <see below>
bL.base_loss = eL.(imin)

bR.domain = SQE.get_zset R.(imin)
bR.expr = <see below>
bR.base_loss = eR.(imin)

The expressions are computed from value_instance.value:
bL.expr = (LE, value_instance.(imin))
bR.expr = (GT, value_instance.(imin))
 *)

(* module Ordinal_fstep(DS) : TREE_BASELEARNER_FEATURE_STEP

Similar to Categorical_fstep, with the following difference: the value_instance
array is not sorted by value but by averages.
 *)

