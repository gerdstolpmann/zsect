module type PointType =
  sig
    type t
    val compare : t -> t -> int
    val neg_infinity : t
    val infinity : t
  end

module type S =
  sig
    type point
    type key = point * point
    type (+'a) t
    val neg_infinity : point
    val infinity : point
    val zero : 'a -> 'a t
    val add : key -> 'a -> 'a t -> 'a t
    val find : point -> 'a t -> key * 'a
    val find_prev : point -> 'a t -> key * 'a
    val find_next : point -> 'a t -> key * 'a
    val map : (key -> 'a -> 'b) -> 'a t -> 'b t
    val merge : (key -> 'a -> 'b -> 'c) -> 'a t -> 'b t -> 'c -> 'c t
    val to_list : 'a t -> (key * 'a) list
  end

module Make(P:PointType) : S with type point = P.t =
  struct

    module M = Map.Make(P)
    type point = P.t
    type key = point * point
    type 'a t = (P.t * 'a) M.t

    let neg_infinity = P.neg_infinity
    let infinity = P.infinity

    let zero z =
      M.singleton neg_infinity (infinity,z)

    let dmerge m1 m2 =
      M.merge
        (fun k y1_opt y2_opt ->
          match y1_opt, y2_opt with
            | Some _, _ -> y1_opt
            | _, Some _  -> y2_opt
            | None, None -> assert false
        )
        m1 m2

    let add (l,r) new_y m =
      if P.compare l r < 0 then (
        let m1, y_opt, m2 = M.split l m in
        let mleft, m3 =
          match y_opt with
            | Some(r1,y) ->
                (m1, M.add l (r1,y) m2)
            | None ->
                assert(not (M.is_empty m1));
                let l1, (r1,y1) = M.max_binding m1 in
                let m1' = M.add l1 (l,y1) m1 in
                let m2' = M.add l (r1,y1) m2 in
                (m1',m2') in
        let m4, y_opt, m5 = M.split r m3 in
        let mright =
          match y_opt with
            | Some(r1,y) ->
                M.add r (r1,y) m5
            | None ->
                assert(not (M.is_empty m4));
                let l1, (r1,y1) = M.max_binding m4 in
                M.add r (r1,y1) m5 in
        M.add l (r,new_y) (dmerge mleft mright)
      )
      else
        m

    let find x m =
      let m1, y_opt, m2 = M.split x m in
      match y_opt with
        | Some(r1,y) ->
            ((x,r1), y)
        | None ->
            let l1, (r1,y1) = M.max_binding m1 in
            ((l1,r1),y1)

    let find_prev x m =
      let m1, y_opt, m2 = M.split x m in
      match y_opt with
        | Some _ ->
            let l1, (r1,y1) = M.max_binding m1 in
            ((l1,r1),y1)
        | None ->
            let l2, _ = M.max_binding m1 in
            let m3 = M.remove l2 m1 in
            let l1, (r1,y1) = M.max_binding m3 in
            ((l1,r1),y1)

    let find_next x m =
      let m1, y_opt, m2 = M.split x m in
      let l1, (r1,y1) = M.min_binding m2 in
      ((l1,r1),y1)

    let map (f : key -> 'a -> 'b) (m:'a t) =
      M.mapi
        (fun l (r,y) ->
          (r, f (l,r) y)
        )
        m

    let merge (f : key -> 'a -> 'b -> 'c) (m : 'a t) (n : 'b t) (z:'c) =
      M.fold
        (fun l (r,ny) acc ->
          let m1, my_opt, m2 = M.split l m in
          let m3 =
            match my_opt with
              | Some(r1,my) ->
                  M.add l (r1,my) m2
              | None ->
                  assert(not (M.is_empty m1));
                  let l1, (r1,y1) = M.max_binding m1 in
                  let m2' = M.add l (r1,y1) m2 in
                  m2' in
          let m4, y_opt, m5 = M.split r m3 in
          let m6 =
            match y_opt with
              | Some(r1,y) ->
                  m4
              | None ->
                  assert(not (M.is_empty m4));
                  let l1, (r1,y1) = M.max_binding m4 in
                  let m4' = M.add l1 (r,y1) m4 in
                  m4' in
          dmerge
            (M.mapi
               (fun ll (rr,yy) ->
                 (rr, f (ll,rr) yy ny)
               )
               m6
            )
            acc
        )
        n
        (zero z)

    let to_list (m : 'a t) =
      List.rev
        (M.fold
           (fun l (r,y) acc ->
             ((l,r),y) :: acc
           )
           m
           []
        )

  end

module Float =
  struct
    type t = float
    let compare : t -> t -> int = Pervasives.compare
    let neg_infinity = neg_infinity
    let infinity = infinity
  end

module FloatIvalMap = Make(Float)
                          
