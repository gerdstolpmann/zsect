(** This design follows section 4.3 ("Regression trees") of Friedman's paper.
    I verified that all loss functions would fit in. For the logistic losses,
    some postprocessing of the output feature is needed to get the probabilities
    back.

    As representation for vectors I assume a pair of a float array (over all
    rows) and a zset that selects a subset of the rows. That's simple and 
    efficient but consumes some RAM. In inner loops this should probably be
    different, in particular in the base learner. We'll have to figure this
    out.
 *)

open Zcontainer

module type DATASET =
  sig
    type dataset
    type col_hint =
      { col_ordinal : bool; (** Whether we are allowed to use the ordering of the values *)
        (* col_monotonic : [`Increasing|`Decreasing] option; (** Whether to ensure monotonicity *) *)
      }
    val ztable : dataset -> Ztable.ztable
    val x_columns : dataset -> (string * col_hint) list
      (** The features we can use in tree expressions *)
    val y_column : dataset -> string
      (** The feature to predict *)
      (* CHECK: maybe give also a col_hint? *)
  end

type vector =
  (float, Bigarray.float64_elt, Bigarray.fortran_layout) Bigarray.Array1.t
  (** vec.{id} is some float number for row id *)

type vector_name = string

type expr =
  | E_ColCmp : 'a Ztypes.zcoldescr * Ztypes.comparison * 'a -> expr
      (** [E_Colcmp(col,cmp,val)] is true for all data points where the column
         [col] compares as [cmp] with the constant value [val]
       *)
  | E_And : expr list -> expr
      (** Boolean AND. [E_And[]] means true. *)
  | E_Or : expr list -> expr
      (** Boolean OR. [E_Or[]] means false. *)

type 'label tree =
  (* a tree... where the leaves are labeled *)
   | Inner of (expr * 'label tree) list
       (** The expression says whether to enter the subtree *)
   | Leaf of 'label
       (** The leaves have labels *)

module type LOSS =
  sig
    module DS : DATASET
      (** The loss is defined here only for a given training set *)

    type 't stage
      (** The loss function can depend on the stage. The type parameter
          is the type of the y column
       *)

    type y
      (** The type of the y column *)

    type p
      (** The type of the prediction. This is not necessarily the same as [y] *)

    val y_type : y Ztypes.ztype
    val p_type : p Ztypes.ztype

    val input : 'a Ztypes.ztype -> 'a -> y
      (** input conversion *)

    val input_ok : 'a Ztypes.ztype -> bool
      (** whether [input] accepts this type *)

    val initial_approx : DS.dataset -> Zset.zset -> float
      (** get the initial approximation for the training set, [F0] *)

    val gradient : y stage -> int -> float -> y -> float
      (** [gradient stage i approx y] = derived loss function for 
          data point [i], e.g. for
          - least squares: [0.5 * (y-F(x))]
          - least abs dev: [sign(y-F(x))]
       *)
      (* CHECK: generalize concept of gradient *)
    val stage : DS.dataset -> Zset.zset -> vector -> y stage
      (** [stage ds subset approximation]:
          Configure the loss function for the stage. The vector contains
          the approximation F_{m-1}
       *)

    val trim : y stage -> vector -> Zset.pool -> Zset.zset
      (** [trim stage gradients pool]:
          Do "influence trimming", i.e. return the subset of the training
          set where the data point with very low influence are deleted
       *)

    val gamma : y stage -> Zset.zset -> vector -> float
      (** [gamma stage subset gradients]: get the gamma value (the number
         in the leaves of the tree) for a given subset of the dataset
         and a given approximation F, [approx = F_(m-1)]. Gamma is the
         value that minimizes the loss [L(y, F(x)+gamma)] in the subset.
         E.g. for
          - least squares: [mean(y-F(x))]
          - least abs dev: [median(y-F(x))]

         CHECK: for L2 it might be convenient to also pass the vector of
         gradients in.

         Note: gamma is only needed for tree-based boosting, but useless
         for any generalization.
       *)
    val loss : y stage -> Zset.zset -> float
      (** [loss stage]: return the loss for the subset of the stage.

          We don't need this function for the tree-based alg, but it might be
          useful for validation.
       *)
    val output : float -> p
      (** Output conversion for a given approximation *)
  end

module type MAKE_LOSS = functor(DS:DATASET) -> LOSS with module DS = DS

(* e.g.
   module LS : MAKE_LOSS   - least squares
   module LAD : MAKE_LOSS  - least abs dev

   Implementations are straight-forward.
 *)


module type BASELEARNER =
  sig
    module DS : DATASET
    type base_output
    type model
    val learn : DS.dataset -> int -> Zset.pool -> vector -> Zset.zset -> base_output
      (** learn this vector which is filled for this set.
          The input is the
          vector of gradients for the most recent approximation.
          The int is the iteration number (stage).
       *)
      (* Possible generalization: learn covariate vectors *)
    val model : base_output -> model
      (** The model, in a fairly generic way *)
    (* CHECK: how to evaluate model? *)
  end

(** The algorithm needs base learners that approximate a given vector for
    a given input set.  We handle here only the case that the input set
    is split into a number of partitions in order to get an information gain.
    Interestingly, the algorithm doesn't need the approximated
    values per partition, but only the partitioning per se.

    When we express the partitions by a list of expressions, the model
    can also be written as decision tree.
 *)

module type TREE_BASELEARNER =
  sig
    include BASELEARNER with type model = Zset.zset tree
      (** this changes [model] to return the tree describing the learned data.
          The leaves of the
          tree do not have number labels, because the base learner only partitions
          the training set reasonably, but does not set the weight of the
          partitions
       *)
    val partitioning : base_output -> Zset.zset list
      (** return the partitioning *)
  end

(** Note that tree base learners are usually constructed from base learner
    steps (see below), where every step corresponds to the creation of
    an internal node of the tree. But for the booster only the result
    counts.
 *)

(** A baselearner designed to be used with line searching on top of it *)
module type LSEARCH_BASELEARNER =
  sig
    include BASELEARNER
    val approximation : base_output -> vector
      (** After learning, the approximation of the training set with the
          model. Note that this vector isn't needed for the tree case
          but only for line searching.

          For rows not part of the training set the approximation shall be 0.

          Note that the approximation may be multiplied with any factor.
          Only the direction counts, not how much we go into the direction.
       *)
  end

(** The booster runs the base learner multiple times, and adds the gamma
    values from LOSS to the approximation.
 *)

(* TBD: module type BOOSTER. Most functions of GRADIENT_BOOSTER still apply.
   [gradient], of course, can be removed. The big question is, however,
   what we use for LOSS, which is so far designed for gradient boosting.
 *)

module type GRADIENT_BOOSTER =
  sig
    module DS : DATASET
      (** Training dataset *)

    module L : LOSS with module DS = DS
      (** The loss function to be applied to the gradients *)

    type stage
    type model

    val stage_count : stage -> int
      (** enumeration of the stages, m = 0, 1, ... *)
    val domain : stage -> Zset.zset
      (** The domain of the training set *)
    val start : DS.dataset -> Zset.zset -> stage
      (** make an initial guess at the model, and define the training set *)
    val approximation : stage -> int -> float
      (** the approximation on this stage (F_m in Friedman's paper) *)
    val gamma : stage -> int -> float
      (** [gamma stage id]: gamma is what we add to the stage in this stage *)
      (* CHECK: need also total_gamma for all steps? *)
    val gradient : stage -> int -> float
      (** [gradient stage id]: the gradient or pseudo response (y~) *)
    val multiply : float -> stage -> stage
      (** multiply gamma by this factor (usually < 1) *)
    val advance : stage -> stage
      (** advance to a new stage (run the base learner) *)
    val model : stage -> model
      (** Get the model for this stage *)
    val loss : stage -> float
      (** Get the total loss *)
    val to_stash : (module Zsession.Stypes.SESSION_STASH) ->
                   int -> stage -> string -> unit
    val from_stash : (module Zsession.Stypes.SESSION_STASH) ->
                     int -> DS.dataset -> Zset.pool -> string -> stage
    val stash_files : string -> string list
  end

module type TREE_GRADIENT_BOOSTER =
  sig
    include GRADIENT_BOOSTER 
            with type model = float tree
      (** The model is here a tree where every leaf is labeled with the
         gamma value
       *)

    module B : TREE_BASELEARNER with module DS = DS
      (** A base learner *)
  end

module type LSEARCH_GRADIENT_BOOSTER =
  sig
    include GRADIENT_BOOSTER 
      (** leave the model abstract... *)

    module B : LSEARCH_BASELEARNER with module DS = DS
      (** A base learner *)
  end

module type MAKE_LSEARCH_GRADIENT_BOOSTER = functor(DS:DATASET) ->
                                            functor(L:LOSS with module DS=DS) ->
                                            functor(B:LSEARCH_BASELEARNER with module DS=DS) ->
                                    LSEARCH_GRADIENT_BOOSTER with module DS = DS
                                      and module L = L
                                      and module B = B
module type MAKE_TREE_BOOSTER = functor(DS:DATASET) ->
                                functor(L:LOSS with module DS=DS) ->
                                functor(B:TREE_BASELEARNER with module DS=DS) ->
                                TREE_GRADIENT_BOOSTER with module DS = DS
                                              and module L = L
                                              and module B = B

(** How to construct a base learner step-wise. The idea is that we bisect
    the training set several times and construct a tree from that. Here
    we only look at one step: split the set up once.
 *)

module type TREE_BASELEARNER_CORE =
  sig
    module DS : DATASET
    type base_step 
      (** A step in the path from the root of the tree to the terminal node *)
    val start : Zset.pool -> vector -> Zset.zset -> base_step
      (** Set the vector to learn and the training set *)
    val depth : base_step -> int
      (** how many [refine] calls were done *)
    val feature : base_step -> string * DS.col_hint
      (** the feature used for this step (only meaningful if [depth>0], i.e.
          we already refined once)
       *)
    val domain : base_step -> Zset.zset
      (** every step is associated with a domain of data points that is
         further decomposed into a partitioning of smaller data point sets.
       *)
    val expr : base_step -> expr
      (** every step is associated with an expression. The expression describes
         [domain] as model, i.e. if we filter the training set by [expr]
         we get [domain].
       *)
    val base_loss : base_step -> float
      (** the loss that is minimized by the base learner. NB. This loss
         can be different from the loss specified by the LOSS module.
         Friedman assumes here always least squares because of the ease
         of computation, but other functions are also possible. The base
         loss is hardcoded into the learner.
       *)
    val finalize : base_step -> unit
      (** This function is called when [base_step] is no longer needed *)
  end

(** This is the inner loop: do the learning step for a particular feature *)

module type TREE_BASELEARNER_FEATURE_STEP =
  sig
    include TREE_BASELEARNER_CORE
    val feature_refine : DS.dataset -> string -> base_step ->
                         (float * base_step list) option
      (** refine the domain into smaller sets, and do this for this feature.
          On success, return [Some(loss,substeps)] where [substeps] is
          a partitioning of the original domain. The [loss] is the sum
          of the losses of all substeps. The list [substeps] has at least
          two elements.
       *)
  end

(* Implementations:
    - module Ordinal_step
    - module Categorical_step
 *)

(** This is the outer loop: try all features in turn and pick the one
    with the lowest base loss:
 *)

module type TREE_BASELEARNER_STEP =
  sig
    include TREE_BASELEARNER_CORE
    val refine : DS.dataset -> base_step -> int -> string list ->
                 (string * float * base_step list) list
      (** [let options = refine ds bs steps_to_go enabled_x_cols]:

          refine the domain into smaller sets. Returns a list of options
          [(colname, loss, steps)]. The caller will pick the first option
          of the list. The function shall returns non-favored options as
          further elements of the list (which is useful for wrapping this
          function).
       *)
  end

module type TREE_BASELEARNER_STEP_WRAPPER =
  functor(BS:TREE_BASELEARNER_STEP) ->
  TREE_BASELEARNER_STEP with module DS = BS.DS
  (** Wrap an existing [TREE_BASELEARNER_STEP] module to get a new module *)

module type MAKE_TREE_BASELEARNER_FEATURE_STEP = functor(DS:DATASET) ->
                                                 TREE_BASELEARNER_FEATURE_STEP with module DS = DS

module type MAKE_TREE_BASELEARNER_STEP = functor(DS:DATASET) ->
                                         functor(BFS:TREE_BASELEARNER_FEATURE_STEP) ->
                                         TREE_BASELEARNER_STEP with module DS = DS
  (** Try all features in turn, and take the feature resulting in the lowest
     [base_loss]
   *)

module type MAKE_TREE_BASELEARNER = functor(DS:DATASET) ->
                                    functor(BS:TREE_BASELEARNER_STEP) ->
                                    TREE_BASELEARNER with module DS = DS
  (** Build a tree by applying the base steps recursively *)

module type TREE_BASELEARNER_WRAPPER =
  functor(TL:TREE_BASELEARNER) ->
  TREE_BASELEARNER with module DS = TL.DS
  (** Wrap an existing [TREE_BASELEARNER] module to get a new module *)
