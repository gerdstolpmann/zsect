open Types
open Zcontainer

module Data_folds(DS:DATASET) =
  struct

    type folds =
      { ztab : Ztable.ztable;
        pool : Zset.pool;
        base : Zset.zset;
        sets : Zset.zset array
      }

    let create ?(is_interactive=false) ?base pool n_folds ds : folds =
      let ztab = DS.ztable ds in
      let base =
        match base with
          | None -> Zset.all pool (Ztable.space ztab)
          | Some b -> b in
      let n = Zset.count base in
      let fold_size = n / n_folds + 1 in
      let fold_of_id = Array.init n (fun k -> k/fold_size) in
      let sets =
        Array.init
          n_folds
          (fun fld ->
             Zset.filter pool (fun id -> fold_of_id.(id-1) = fld) base
          ) in
      if is_interactive then
        Array.iteri
          (fun k set ->
             Printf.eprintf "fold %d: n=%d rows\n%!" k (Zset.count set)
          )
          sets;
      { ztab; pool; base; sets }

    let get_fold (fd:folds) k =
      fd.sets.(k)

    let get_fold_negation (fd:folds) k =
      Zset.diff fd.pool fd.base fd.sets.(k)
  end
