open Types
open Zcontainer
open Printf

let sign x =
  if x < 0.0 then
    (-1.0)
  else if x = 0.0 then
    0.0
  else
    1.0

(** Actually, half squared error *)
module Squared_error(DS:DATASET) 
       : LOSS with module DS=DS and type y = float and type p = float =
  struct
    module DS = DS

    let initial_approx ds zset =
      (* return the mean value of the y column for the training set *)
      let y_col =
        Ztable.get_zcol (DS.ztable ds) Ztypes.Zfloat (DS.y_column ds) in
      Zaggreg.avg y_col zset

    type 't stage =
      DS.dataset * Zset.zset * vector

    type y = float
    type p = float

    let y_type = Ztypes.Zfloat
    let p_type = Ztypes.Zfloat

    let input : type s . s Ztypes.ztype -> s -> y =
      function
      | Ztypes.Zfloat -> (fun x -> x)
      | Ztypes.Zint -> float_of_int
      | _ -> failwith "Zmola.Loss.Squared_error.input"

    let input_ok : type s . s Ztypes.ztype -> bool =
      function
      | Ztypes.Zfloat -> true
      | Ztypes.Zint -> true
      | _ -> false

    let stage ds zset numbers =
      (ds,zset,numbers)

    let trim (ds,zset,numbers) _ _ = zset
      
    let gradient stage i approx y =
      y -. approx

    let gamma ((ds,zset,numbers) : _ stage) subset _ =
      let y_col =
        Ztable.get_zcol (DS.ztable ds) Ztypes.Zfloat (DS.y_column ds) in
      let sum =
        Zaggreg.fold
          (fun id y acc ->
             acc +. y -. numbers.{id}
          )
          y_col
          subset
          0.0 in
      let count = Zset.count subset in
      sum /. float count

    let loss ((ds,zset,numbers) : _ stage) subset =
      let y_col =
        Ztable.get_zcol (DS.ztable ds) Ztypes.Zfloat (DS.y_column ds) in
      0.5 *. 
        Zaggreg.fold
          (fun id y acc ->
             let p = y -. numbers.{id} in
             acc +. p *. p
          )
          y_col
          subset
          0.0

    let output approx = approx
  end


module Absolute_deviation(DS:DATASET) 
       : LOSS with module DS=DS and type y = float and type p = float =
  struct
    module DS = DS

    type 't stage =
      DS.dataset * Zset.zset * vector

    type y = float
    type p = float

    let y_type = Ztypes.Zfloat
    let p_type = Ztypes.Zfloat

    let input : type s . s Ztypes.ztype -> s -> y =
      function
      | Ztypes.Zfloat -> (fun x -> x)
      | Ztypes.Zint -> float_of_int
      | _ -> failwith "Zmola.Loss.Absolute_deviation.input"

    let input_ok : type s . s Ztypes.ztype -> bool =
      function
      | Ztypes.Zfloat -> true
      | Ztypes.Zint -> true
      | _ -> false

    let initial_approx ds zset =
      (* return the median of the y column for the training set *)
      let y_col =
        Ztable.get_zcol (DS.ztable ds) Ztypes.Zfloat (DS.y_column ds) in
      Zaggreg.median y_col zset

    let stage ds zset numbers =
      (ds,zset,numbers)

    let trim (ds,zset,numbers) _ _ = zset

    let gradient _ _ approx y =
       sign (y -. approx)

    let gamma ((ds,zset,numbers) : _ stage) subset _ =
      let y_col =
        Ztable.get_zcol (DS.ztable ds) Ztypes.Zfloat (DS.y_column ds) in
      Zaggreg.median_map
        (fun id y ->
           y -. numbers.{id}
        )
        y_col
        subset

    let loss ((ds,zset,numbers) : _ stage) subset =
      let y_col =
        Ztable.get_zcol (DS.ztable ds) Ztypes.Zfloat (DS.y_column ds) in
      Zaggreg.fold
        (fun id y acc ->
           let p = abs_float (y -. numbers.{id}) in
           acc +. p
          )
          y_col
          subset
          0.0

    let output approx = approx
  end


module type HUBER_CONFIG =
  sig
    val alpha : float
  end


module Huber(C:HUBER_CONFIG)(DS:DATASET)
       : LOSS with module DS=DS and type y = float and type p = float =
  struct
    module DS = DS

    type 't stage =
      { ds : DS.dataset;
        zset : Zset.zset;
        approximation : vector;
        delta : float
      }

    type y = float
    type p = float

    let y_type = Ztypes.Zfloat
    let p_type = Ztypes.Zfloat

    let input : type s . s Ztypes.ztype -> s -> y =
      function
      | Ztypes.Zfloat -> (fun x -> x)
      | Ztypes.Zint -> float_of_int
      | _ -> failwith "Zmola.Loss.Huber.input"

    let input_ok : type s . s Ztypes.ztype -> bool =
      function
      | Ztypes.Zfloat -> true
      | Ztypes.Zint -> true
      | _ -> false

    let initial_approx ds zset =
      (* return the median of the y column for the training set *)
      let y_col =
        Ztable.get_zcol (DS.ztable ds) Ztypes.Zfloat (DS.y_column ds) in
      Zaggreg.median y_col zset

    let stage ds zset approximation =
      let y_col =
        Ztable.get_zcol (DS.ztable ds) Ztypes.Zfloat (DS.y_column ds) in
      let delta =
        Zaggreg.quantile_map
          (fun id y ->
             abs_float(y -. approximation.{id})
          )
          C.alpha
          y_col
          zset in
      { ds; zset; approximation; delta }

    let trim stage _ _ = stage.zset

    let gradient stage _ approx y =
      let diff = y -. approx in
      let absdiff = abs_float diff in
      if absdiff <= stage.delta then
        diff
      else
        stage.delta *. sign diff

    let gamma stage subset _ =
      let y_col =
        Ztable.get_zcol
          (DS.ztable stage.ds) Ztypes.Zfloat (DS.y_column stage.ds) in
      let r_median =
        Zaggreg.median_map
          (fun id y ->
             y -. stage.approximation.{id}
          )
          y_col
          subset in
      let n = float(Zset.count subset) in
      let sum =
        Zaggreg.fold
          (fun id y acc ->
             let approx = stage.approximation.{id} in
             let diff = y -. approx in
             let s = sign(diff -. r_median) in
             let m = min stage.delta (diff -. r_median) in
             s *. m
          )
          y_col
          subset
          0.0 in
      r_median  +.  sum /. n

    let loss stage subset =
      let y_col =
        Ztable.get_zcol
          (DS.ztable stage.ds) Ztypes.Zfloat (DS.y_column stage.ds) in
      Zaggreg.fold
        (fun id y acc ->
           let approx = stage.approximation.{id} in
           let diff = y -. approx in
           let absdiff = abs_float diff in
           if absdiff <= stage.delta then
             acc  +.  0.5 *. diff *. diff
           else
             acc  +.  stage.delta *. (absdiff -. 0.5 *. stage.delta)
          )
          y_col
          subset
          0.0

    let output approx = approx
  end


module type BILOG_CONFIG =
  sig
    val alpha : float
      (** alpha=0.0 disables influence trimming. alpha>0.0 enables it *)

    (* TODO: specify how to map arbitrary y values to {-1,1} *)
  end

module Bilog(C:BILOG_CONFIG)(DS:DATASET)
       : LOSS with module DS=DS and type y = bool and type p = float =
  struct
    (* Speed of exp and log on x86 (64 bit):
        - exp takes the same as around 7 fp mults
        - log takes the same as around 10 fp mults
       On my laptop:
        - exp: 50 Mops/sec
        - log: 33 Mops/sec
        - mult: 333 Mops/sec
     *)

    module DS = DS

    type 't stage =
      { ds : DS.dataset;
        zset : Zset.zset;
        approximation : vector;
      }

   (* Attention! We assume here y IN {0,1} whereas Friedman wrote his text
      for y IN {-1,1}
    *)

    type y = bool
    type p = float

    let y_type = Ztypes.Zbool
    let p_type = Ztypes.Zfloat

    let input : type s . s Ztypes.ztype -> s -> y =
      function
      | Ztypes.Zbool -> (fun x -> x)
      | Ztypes.Zint -> (fun x -> x <> 0)
      | _ -> failwith "Zmola.Loss.Bilog.input"

    let input_ok : type s . s Ztypes.ztype -> bool =
      function
      | Ztypes.Zbool -> true
      | Ztypes.Zint -> true
      | _ -> false

    let initial_approx ds zset =
      let y_col =
        Ztable.get_zcol (DS.ztable ds) Ztypes.Zint (DS.y_column ds) in
      let y_avg =
        Zaggreg.avg_map
          (fun i y ->
             if y <> 0 then 1.0 else -1.0
          )
          y_col
          zset in
      0.5 *. log ((1.0 +. y_avg) /. (1.0 -. y_avg))

    let stage ds zset approximation =
      { ds; zset; approximation }

    let gradient stage i approx y =
      let two_y_float =
        if y then 2.0 else -2.0 in
      let arg = two_y_float *. approx in
      if arg > 700.0 then
        0.0
      else
        if arg < (-700.0) then
          two_y_float
        else
          two_y_float /. (1.0 +. exp arg)

    let trim stage gvec pool =
      if C.alpha <= 0.0 then
        stage.zset
      else (
        (* As wlimit is an estimate anyway, we just pick a random subsample
           of the data
         *)
        let n = Vector.length stage.approximation in
        let m_target = 5000 in
        let p = float m_target /. float n in
        let subset =
          Zset.filter
            pool
            (fun id -> Random.float 1.0 < p)
            stage.zset in
        (* Compute the vector of weights: *)
        let wvec = Vector.make n 0.0 in
        Zset.iter
          (fun start len ->
             for id = start to start+len-1 do
               let g = gvec.{id} in
               let w = abs_float g *. (2.0 -. abs_float g) in
               wvec.{id} <- w;
             done
          )
          stage.zset;
        let wsum = ref 0.0 in
        Zset.iter
          (fun start len ->
             for id = start to start+len-1 do
               wsum := !wsum +. wvec.{id}
             done
          )
          subset;
        let wsum_limit = C.alpha *. !wsum in
        (* Add the weights to a heap *)
        let heap = Vector.Heap.create n in
        Zset.iter
          (fun start len ->
             for id = start to start+len-1 do
               Vector.Heap.insert heap wvec.{id}
             done
          )
          subset;
        (* Remove the weights from the heap in ascending order: *)
        let tsum = ref 0.0 in
        let wlimit = ref 0.0 in
        while !tsum < wsum_limit && Vector.Heap.length heap > 0 do
          let w = Vector.Heap.min heap in
          tsum := !tsum +. w;
          wlimit := w;
          Vector.Heap.delete_min heap
        done;
        (* Finally, we apply the criterion to stage.zset and not subset: *)
        let trimset =
          Zset.filter pool (fun id -> wvec.{id} >= !wlimit) stage.zset in
        printf "[trim orig=%d trimmed=%d]\n%!"
               (Zset.count stage.zset) (Zset.count trimset);
        trimset
      )

    let gamma stage subset gradients =
      let y_col =
        Ztable.get_zcol
          (DS.ztable stage.ds) Ztypes.Zint (DS.y_column stage.ds) in
      let sum1 =
        Zaggreg.sum_map
          (fun i _ ->
             gradients.{i}
          )
          y_col
          subset in
      let sum2 =
        Zaggreg.sum_map
          (fun i _ ->
             let g = abs_float gradients.{i} in
             g *. (2.0 -. g)
          )
          y_col
          subset in
      let g =
        sum1 /. sum2 in
      ( match classify_float g with
          | FP_nan -> 
              (* This can only happen when all gradients are 0, i.e. when
                 there isn't any change. So don't add anything to the
                 approximation
               *)
              0.0
          | _ ->
              (* We can still return large values or even infinity. This
                 means that there are gradients close to 2.0 or -2.0. In
                 other words we have diverged. This is caused by the Newton
                 step (which is kind of unpredictable).
               *)
              g
      )

    let loss stage subset =
      let y_col =
        Ztable.get_zcol
          (DS.ztable stage.ds) Ztypes.Zint (DS.y_column stage.ds) in
      Zaggreg.sum_map
        (fun i y ->
           let minus_two_y_float =
             if y <> 0 then -2.0 else 2.0 in
           let arg = minus_two_y_float *. stage.approximation.{i} in
           if arg < 10.0 then
             log(1.0 +. exp arg)
           else
             arg
        )
        y_col
        subset

    let output approx =
      1.0 /. (1.0 +. exp (-2.0 *. approx))
  end
