open Types
open Zcontainer
module U = Yojson.Safe.Util

module type LOOP_TYPE =
  sig
    module DS : DATASET
    module EL : TREE_GRADIENT_BOOSTER with module DS = DS
    module L : LOSS with module DS = DS

    type state
    val trainset : state -> Zset.zset
    val valset : state -> Zset.zset
    val stage : state -> EL.stage
    val ensemble_model : state -> EL.model list
    val stage_count : state -> int
    val learning_rate : state -> float
    val loss_trainset : state -> float
    val loss_valset : state -> float

    val create : DS.dataset -> Zset.pool -> Zset.zset -> Zset.zset -> float ->
                 state

    val next : state -> unit
    val check : state -> unit

    val to_stash : (module Zsession.Stypes.SESSION_STASH) -> int -> state ->
                   string -> unit
    val from_stash : (module Zsession.Stypes.SESSION_STASH) -> int ->
                     DS.dataset -> Zset.pool -> string -> state
    val stash_files : string -> string list
  end
             
module Loop(EL:TREE_GRADIENT_BOOSTER) :
         LOOP_TYPE with module DS = EL.DS
                    and module L = EL.L
                    and module EL = EL =
  struct
    module DS = EL.DS
    module L = EL.L
    module EL = EL

    type state =
      { ds : DS.dataset;
        pool : Zset.pool;
        trainset : Zset.zset;
        valset : Zset.zset;
        mutable learning_rate : float;
        mutable stage : EL.stage;
        mutable ensemble_model : EL.model list;
        mutable old_stage : EL.stage;
        mutable old_ensemble_model : EL.model list;
        mutable val_approx : vector;
      }

    let trainset state = state.trainset
    let valset state = state.valset
    let stage state = state.stage
    let ensemble_model state = List.rev state.ensemble_model
    let stage_count state = EL.stage_count state.stage
    let learning_rate state = state.learning_rate

    let loss_trainset state = EL.loss state.stage

    let loss_valset state =
      let s_valset = L.stage state.ds state.valset state.val_approx in
      L.loss s_valset state.valset

    let create ds pool trainset valset learning_rate =
      let ztab = DS.ztable ds in
      let Ztypes.Space n = Ztable.space ztab in
      let stage = EL.start ds trainset in
      let model = EL.model stage in
      let ensemble_model = [model] in
      let old_stage = stage in
      let old_ensemble_model = ensemble_model in
      let val_approx = Vector.make n 0.0 in
      Tree.accumulate ztab pool val_approx valset model;
      { ds; pool; trainset; valset; learning_rate;
        stage; ensemble_model;
        old_stage;  old_ensemble_model;
        val_approx;
      }

    let rec next state =
      let ztab = DS.ztable state.ds in
      let next_stage =
        EL.multiply state.learning_rate (EL.advance state.stage) in
      let next_loss = EL.loss next_stage in
      if next_loss >= 2.0 *. (loss_trainset state) then (
        let m = EL.model state.stage in
        state.learning_rate <- 0.25 *. state.learning_rate;
        state.stage <- state.old_stage;
        if state.ensemble_model != state.old_ensemble_model then (
          let m_neg = Tree.map_leaves (fun d -> -.d) m in
          Tree.accumulate ztab state.pool state.val_approx state.valset m_neg
        );
        state.ensemble_model <- state.old_ensemble_model;
        next state
      ) else (
        state.old_stage <- state.stage;
        state.old_ensemble_model <- state.ensemble_model;
        let next_model = EL.model next_stage in
        state.stage <- next_stage;
        state.ensemble_model <- next_model :: state.ensemble_model;
        Tree.accumulate ztab state.pool state.val_approx state.valset next_model
      )

    let check state =
      (* compare ensemble_model with the approximation *)
      let ztab = DS.ztable state.ds in
      let eval = Tree.eval_list ztab state.ensemble_model in
      let approx = EL.approximation state.stage in
      let error = ref 0.0 in
      let square x = x *. x in
      Zset.iter
        (fun start len ->
           for id = start to start+len-1 do
             let y1 = eval id in
             let y2 = approx id in
             error := !error +. square (y1 -. y2)
           done
        )
        state.trainset;
      Printf.printf "model error: %e\n%!" !error

    (* ---- session support ---- *)
                            
    type Zsession.Stypes.live_object +=
       | State of state
                            

    let to_stash (stash : (module Zsession.Stypes.SESSION_STASH))
                 i state suffix =
      let module Stash = (val stash) in
      (* By saving stage we implicitly also save trainset (=stage.domain) *)
      EL.to_stash stash i state.stage (".current" ^ suffix);
      EL.to_stash stash i state.old_stage (".old" ^ suffix);
      (* Save the remaining fields: *)
      let sl = Stash.get_slot i in
      let upd = Stash.update sl in
      Stash.add_object upd ("Zmola.Booster_loop.state"^suffix) (State state);
      let param = Stash.slot_param sl in
      let param = if param = `Null then `Assoc [] else param in
      let param =
        Zsession.Util.json_update
          param
          (`Assoc [ "Zmola.Booster_loop.learning_rate"^suffix,
                    `Float state.learning_rate]) in
      Stash.set_param upd param;
      
      let filename_valset =
        Stash.add_file upd ("Zmola.Booster_loop.valset"^suffix) "zset" in
      Zsession.Util.write_zset filename_valset state.valset;
      let filename_val_approx =
        Stash.add_file upd ("Zmola.Booster_loop.val_approx"^suffix) "vector" in
      Zsession.Util.write_vector filename_val_approx state.val_approx;
      let filename_model =
        Stash.add_file upd ("Zmola.Booster_loop.model.current"^suffix)
                       "model list" in
      Yojson.Safe.to_file
        filename_model
        (Tree.json_of_model_list state.ensemble_model);
      let filename_old_model =
        Stash.add_file upd ("Zmola.Booster_loop.model.old"^suffix)
                       "model list" in
      Yojson.Safe.to_file
        filename_old_model
        (Tree.json_of_model_list state.old_ensemble_model);
      Stash.commit upd


    let from_stash (stash : (module Zsession.Stypes.SESSION_STASH))
                   i ds pool suffix =
      let module Stash = (val stash) in
      let sl = Stash.get_slot i in
      let objs = Stash.slot_objects sl in
      match List.assoc ("Zmola.Booster_loop.state"^suffix) objs with
        | State state ->
            state
        | _ ->
            failwith "Zmola.Booster_loop.from_stash"
        | exception Not_found ->
            let stage = EL.from_stash stash i ds pool (".current"^suffix) in
            let old_stage = EL.from_stash stash i ds pool (".old"^suffix) in
            let param = Stash.slot_param sl in
            let param = if param = `Null then `Assoc [] else param in
            let files = Stash.slot_files sl in
            let learning_rate =
              param |> U.member ("Zmola.Booster_loop.learning_rate"^suffix) |>
                U.to_float in
            let trainset = EL.domain stage in
            let valset =
              match List.assoc ("Zmola.Booster_loop.valset"^suffix) files with
                | (filename,_) -> Zsession.Util.read_zset pool filename
                | exception Not_found ->
                            failwith "Zmola.Booster_loop.from_stash" in
            let val_approx =
              match List.assoc ("Zmola.Booster_loop.val_approx"^suffix) files
              with
                | (filename,_) -> Zsession.Util.read_vector filename
                | exception Not_found ->
                            failwith "Zmola.Booster_loop.from_stash" in
            let ensemble_model =
              match List.assoc ("Zmola.Booster_loop.model.current"^suffix)
                               files
              with
                | (filename,_) ->
                    Tree.model_list_of_json (Yojson.Safe.from_file filename)
                | exception Not_found ->
                            failwith "Zmola.Booster_loop.from_stash" in
            let old_ensemble_model =
              match List.assoc ("Zmola.Booster_loop.model.old"^suffix) files
              with
                | (filename,_) ->
                    Tree.model_list_of_json (Yojson.Safe.from_file filename)
                | exception Not_found ->
                            failwith "Zmola.Booster_loop.from_stash" in
            { ds; pool; trainset; valset; learning_rate;
              stage; ensemble_model;
              old_stage;  old_ensemble_model;
              val_approx;
            }
                            
    let stash_files suffix =
      EL.stash_files  (".current"^suffix) @
        EL.stash_files  (".old"^suffix) @
          [ "Zmola.Booster_loop.valset"^suffix;
            "Zmola.Booster_loop.val_approx"^suffix;
            "Zmola.Booster_loop.model.current"^suffix;
            "Zmola.Booster_loop.model.old"^suffix
          ]

              
  end
